package conexion;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import org.apache.log4j.Logger;

public class Conexion {
	public static Conexion instancia;
	private Connection connection;
	private Logger log = Logger.getLogger(Conexion.class);
	private String ip, puerto, bdd, usuario, contrasenia;

	public static Conexion getConexion() {
		if (instancia == null)
			instancia = new Conexion();

		return instancia;
	}

	public boolean probarConexion(String ip, String puerto, String bdd, String usuario, String contrasenia) {
		try {
			this.ip = ip;
			this.puerto = puerto;
			this.bdd = bdd;
			this.usuario = usuario;
			this.contrasenia = contrasenia;

			Class.forName("com.mysql.jdbc.Driver");

			this.connection = DriverManager.getConnection("jdbc:mysql://"+ip+":"+puerto+"/"+bdd, usuario, contrasenia);
			log.info("Conexión exitosa al probar conexión");
			return true;
		} catch (Exception e) {
			log.error("Conexión fallida al intentar probar conexión");
			return false;
		}
	}

	private Conexion() {
		try {
			leerProperties();
			Class.forName("com.mysql.jdbc.Driver");
			this.connection = DriverManager.getConnection("jdbc:mysql://"+ip+":"+puerto+"/"+bdd, usuario, contrasenia);
			log.info("Conexión exitosa al intentar conectar");
		} catch (Exception e) {
			log.error("Conexión fallida al intentar conectar");
		}
	}

	public Connection getSQLConexion() {
		return this.connection;
	}

	public void cerrarConexion() {
		if (this.connection != null) {
			try {
				this.connection.close();
				log.info("Conexión cerrada");
			} catch (SQLException e) {
				log.error("Error al CERRAR la conexión!", e);
			}
		}
		instancia = null;
	}
	
	public static boolean comprobarConexion() {
		if(instancia!=null) {
			if(instancia.getSQLConexion() == null) {
				instancia=null;
			}
			else {
				instancia.cerrarConexion();
				instancia=null;
			}
		}
		return getConexion().getSQLConexion() != null;	
	}

	private void leerProperties() {
		Properties properties = new Properties();
		try {
			FileReader fileReader = new FileReader("BDD.properties");
			properties.load(fileReader);
		} catch (IOException e) {
			e.printStackTrace();
		}

		this.ip = properties.getProperty("host");
		this.puerto = properties.getProperty("puerto");
		this.bdd = properties.getProperty("baseDatos");
		this.usuario = properties.getProperty("usuario");
		this.contrasenia = properties.getProperty("contrasenia");
	}
}