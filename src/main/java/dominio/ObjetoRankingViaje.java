package dominio;

import java.math.BigDecimal;

public class ObjetoRankingViaje {
	private Viaje viaje;
	private BigDecimal monto;

	public ObjetoRankingViaje(Viaje viaje, BigDecimal monto) {
		this.viaje = viaje;
		this.monto = monto;
	}

	public Viaje getViaje() {
		return this.viaje;
	}

	public BigDecimal getMonto() {
		return this.monto;
	}

}
