package dominio;

import java.time.LocalDate;
import java.util.Objects;
import java.util.Random;

public class Usuario implements Comparable<Object> {
	private int id;
	private String dni, nombre, apellido, usuario, contraseña, email;
	private Estado estado;
	private UsuarioRol rol;
	private LocalDate fechaCreacion, nacimiento;
	private Contacto contacto;
	private Local local;

	public Usuario(String dni, String nombre, String apellido, String usuario, LocalDate nacimiento, String email,
			String rol) {
		this.dni = dni;
		this.nombre = nombre;
		this.apellido = apellido;
		this.usuario = usuario;
		this.fechaCreacion = LocalDate.now();
		this.email = email;
		this.local = null;

		generarContraseña();

		setRol(rol);
		this.estado = Estado.HABILITADO;
		this.nacimiento = nacimiento;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Contacto getContacto() {
		return contacto;
	}

	public void setContacto(Contacto contacto) {
		this.contacto = contacto;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public LocalDate getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(LocalDate fecha) {
		this.fechaCreacion = fecha;
	}

	public String getContraseña() {
		return this.contraseña;
	}

	public void setContraseña(String contraseña) {
		this.contraseña = contraseña;
	}

	public String getUsuario() {
		return this.usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public LocalDate getNacimiento() {
		return nacimiento;
	}

	public void setNacimiento(LocalDate nacimiento) {
		this.nacimiento = nacimiento;
	}

	public void generarContraseña() {
		Random r = new Random();
		int entre = 10000000;
		int hasta = 99999999;
		int resultado = r.nextInt(hasta - entre) + entre;

		String contraseña = "" + resultado;

		this.contraseña = contraseña;
	}

	public String getRol() {
		return rol.name();
	}

	public void setRol(String rol) {
		this.rol = UsuarioRol.valueOf(rol);
	}

	public String getEstado() {
		return estado.toString();
	}

	public void setEstado(String estado) {
		this.estado = Estado.valueOf(estado);
	}

	public void setLocal(Local local) {
		this.local = local;
	}

	@Override
	public String toString() {
		return usuario;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		Usuario usuario1 = (Usuario) o;
		return Objects.equals(dni, usuario1.dni) && Objects.equals(usuario, usuario1.usuario);
	}

	@Override
	public int hashCode() {
		return Objects.hash(dni, usuario);
	}

	public Local getLocal() {
		return this.local;
	}

	@Override
	public int compareTo(Object arg0) {
		return arg0.toString().compareTo(this.usuario);
	}

}
