package dominio;

import java.util.Objects;

public class Local implements Comparable<Object> {
	private int id;
	private String nombre, direccion, localidad, provincia, detalle;
	private Estado estado;
	private Gasto gasto;

	public Local(String nombre, String direccion, String localidad, String provincia, String detalle) {
		this.nombre = nombre;
		this.direccion = direccion;
		this.localidad = localidad;
		this.provincia = provincia;
		this.detalle = detalle;
		this.estado = Estado.HABILITADO;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDireccion() {
		return direccion;
	}

	public String getLocalidad() {
		return localidad;
	}

	public String getProvincia() {
		return provincia;
	}

	public String getDetalle() {
		return detalle;
	}

	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}

	public String getEstado() {
		return estado.toString();
	}

	public void setEstado(String estado) {
		this.estado = Estado.valueOf(estado);
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}

	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	public Gasto getGasto() {
		return gasto;
	}

	public void setGasto(Gasto gasto) {
		this.gasto = gasto;
	}

	@Override
	public String toString() {
		return this.nombre;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		Local local = (Local) o;
		return Objects.equals(nombre, local.nombre) && Objects.equals(direccion, local.direccion)
				&& Objects.equals(localidad, local.localidad) && Objects.equals(provincia, local.provincia)
				&& Objects.equals(detalle, local.detalle);
	}

	@Override
	public int hashCode() {
		return Objects.hash(nombre, direccion, localidad, provincia, detalle);
	}

	@Override
	public int compareTo(Object arg0) {
		return arg0.toString().compareTo(this.nombre);
	}
}
