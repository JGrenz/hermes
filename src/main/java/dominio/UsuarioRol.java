package dominio;

public enum UsuarioRol {
	CLIENTE {
		public String toString() {
			return "CLIENTE";
		}
	},
	COORDINADOR {
		public String toString() {
			return "COORDINADOR";
		}
	},
	DUEÑO {
		public String toString() {
			return "DUEÑO";
		}
	},
	PERSONAL_ADMINISTRATIVO {
		public String toString() {
			return "PERSONAL_ADMINISTRATIVO";
		}
	},
	ADMINISTRADOR {
		public String toString() {
			return "ADMINISTRADOR";
		}
	}
}
