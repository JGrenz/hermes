package dominio;

import java.util.Objects;

public class MedioTransporte {

	private int id;
	private String nombreMedioDeTransporte;
	private Estado estado;

	public MedioTransporte(String nombreDelMedioDeTransporte) {
		this.nombreMedioDeTransporte = nombreDelMedioDeTransporte;
		estado = Estado.HABILITADO;
	}

	public String getNombre() {
		return nombreMedioDeTransporte;
	}

	public void setNombre(String nombreMedioDeTransporte) {
		this.nombreMedioDeTransporte = nombreMedioDeTransporte;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEstado() {
		return estado.toString();
	}

	public void setEstado(String estado) {
		this.estado = Estado.valueOf(estado);
	}

	@Override
	public String toString() {
		return nombreMedioDeTransporte;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		MedioTransporte that = (MedioTransporte) o;
		return Objects.equals(nombreMedioDeTransporte, that.nombreMedioDeTransporte);
	}

	@Override
	public int hashCode() {
		return Objects.hash(nombreMedioDeTransporte);
	}
}
