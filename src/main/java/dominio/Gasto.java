package dominio;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

public class Gasto {
	private enum tipoGasto {
		SERVICIO, SUELDO, CONSUMOS_VARIOS, DEVOLUCION
	}

	private enum estadoGasto {
		HABILITADO, INHABILITADO
	};

	private int id;
	private BigDecimal monto;
	private LocalDateTime fechaCreacion;
	private LocalDate fechaGasto;
	private Enum gasto;
	private Enum estado;
	private String codigoReferencia;
	private Local local;

	public Gasto(BigDecimal monto, String tipoDeGasto, Local local, LocalDate fechaGasto) {
		this.monto = monto;
		this.fechaCreacion = LocalDateTime.now();
		this.local = local;
		this.gasto = tipoGasto.valueOf(tipoDeGasto);
		this.estado = estadoGasto.valueOf("HABILITADO");
		this.fechaGasto = fechaGasto;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public LocalDateTime getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(LocalDateTime fecha) {
		this.fechaCreacion = fecha;
	}

	public BigDecimal getMonto() {
		return monto;
	}

	public String getTipoGasto() {
		return gasto.toString();
	}

	public void setTipoGasto(String gasto) {
		this.gasto = tipoGasto.valueOf(gasto);
	}

	public String getEstado() {
		return estado.toString();
	}

	public void setEstado(String estado) {
		this.estado = estadoGasto.valueOf(estado);
	}

	public Local getLocal() {
		return local;
	}

	public void setLocal(Local local) {
		this.local = local;
	}

	public void setMonto(BigDecimal monto) {
		this.monto = monto;
	}

	public String getCodigoReferencia() {
		return codigoReferencia;
	}

	public void setCodigoReferencia(String codigoReferencia) {
		this.codigoReferencia = codigoReferencia;
	}
	
	public LocalDate getFechaGasto() {
		return fechaGasto;
	}

	public void setFechaGasto(LocalDate fechaGasto) {
		this.fechaGasto = fechaGasto;
	}

	@Override
	public String toString() {
		return codigoReferencia;
	}
}
