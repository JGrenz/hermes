package dominio;

import java.math.BigDecimal;
import java.time.LocalDate;

public class Devolucion {

	private int id;
	private Usuario cliente;
	private BigDecimal monto;
	private LocalDate fechaCreacion;
	private int porcentajeDevolucion;

	public Devolucion(Usuario cliente, BigDecimal monto, int porcentajeDevolucion) {
		this.cliente = cliente;
		this.monto = monto;
		this.porcentajeDevolucion = porcentajeDevolucion;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public BigDecimal getMonto() {
		return monto;
	}

	public void setMonto(BigDecimal monto) {
		this.monto = monto;
	}

	public LocalDate getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(LocalDate fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public int getPorcentajeDevolucion() {
		return porcentajeDevolucion;
	}

	public void setPorcentajeDevolucion(int porcentajeDevolucion) {
		this.porcentajeDevolucion = porcentajeDevolucion;
	}

	public Usuario getCliente() {
		return cliente;
	}

}
