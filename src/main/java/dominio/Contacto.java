package dominio;

public class Contacto {
	private int id;
	private String telefono1, telefono2, telefono3, telefono4, telefono5;

	public Contacto(String telefono1, String telefono2, String telefono3, String telefono4, String telefono5) {
		this.telefono1 = telefono1;
		this.telefono2 = telefono2;
		this.telefono3 = telefono3;
		this.telefono4 = telefono4;
		this.telefono5 = telefono5;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTelefono1() {
		return telefono1;
	}

	public void setTelefono1(String telefono1) {
		this.telefono1 = telefono1;
	}

	public String getTelefono2() {
		return telefono2;
	}

	public void setTelefono2(String telefono2) {
		this.telefono2 = telefono2;
	}

	public String getTelefono3() {
		return telefono3;
	}

	public void setTelefono3(String telefono3) {
		this.telefono3 = telefono3;
	}

	public String getTelefono4() {
		return telefono4;
	}

	public void setTelefono4(String telefono4) {
		this.telefono4 = telefono4;
	}

	public String getTelefono5() {
		return telefono5;
	}

	public void setTelefono5(String telefono5) {
		this.telefono5 = telefono5;
	}

}
