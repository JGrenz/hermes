package dominio;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Operacion {

	private int id;
	private BigDecimal monto;
//	private String codReferencia;
	private Pasaje pasaje;
	private Usuario cliente, responsable;
	private Pago pago;
	private LocalDateTime fechaCreacion;

	public Operacion(BigDecimal montoAbonado, Pago pago, Pasaje pasaje, Usuario cliente, Usuario responsable) {
		this.monto = montoAbonado;
		this.pago = pago;
		this.pasaje = pasaje;
		this.cliente = cliente;
		this.responsable = responsable;
//		this.codReferencia = MetodosDominio.generarCodigoReferenia();
	}

//	public String getCodReferencia() {
//		return codReferencia;
//	}

	public BigDecimal getMonto() {
		return monto;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Pasaje getPasaje() {
		return pasaje;
	}

	public Usuario getCliente() {
		return cliente;
	}

	public Usuario getResponsable() {
		return responsable;
	}

	public Pago getPago() {
		return pago;
	}

	public LocalDateTime getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(LocalDateTime fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

//	public void setCodigoReferencia(String codReferencia) {
//		this.codReferencia = codReferencia;
//	}

	@Override
	public String toString() {
		DateTimeFormatter formatoDeFechaHora = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
		String fechaDeCreacionConFormato = fechaCreacion.format(formatoDeFechaHora);
		return "" + fechaDeCreacionConFormato;
	}
}
