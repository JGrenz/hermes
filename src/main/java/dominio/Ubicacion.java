package dominio;

import java.util.List;
import java.util.Objects;

public class Ubicacion {

	private int id;
	private String ciudad, provincia, pais, continente, detalle;
	private List<String> tags;
	private Estado estado;

	public Ubicacion(String ciudad, String provincia, String pais, String continente, String detalle) {
		this.ciudad = ciudad;
		this.provincia = provincia;
		this.pais = pais;
		this.continente = continente;
		this.detalle = detalle;
		this.estado = Estado.HABILITADO;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public void setContinente(String continente) {
		this.continente = continente;
	}

	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}

	public String getProvincia() {
		return provincia;
	}

	public String getPais() {
		return pais;
	}

	public String getContinente() {
		return continente;
	}

	public String getDetalle() {
		return detalle;
	}

	public List<String> getTags() {
		return tags;
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}

	public void agregarTags(String tag) {
		this.tags.add(tag);
	}

	public String getEstado() {
		return this.estado.toString();
	}

	public void setEstado(String estado) {
		this.estado = Estado.valueOf(estado);
	}

	@Override
	public String toString() {
		return ciudad;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		Ubicacion ubicacion = (Ubicacion) o;
		return Objects.equals(ciudad, ubicacion.ciudad) && Objects.equals(provincia, ubicacion.provincia)
				&& Objects.equals(pais, ubicacion.pais) && Objects.equals(continente, ubicacion.continente);
	}

	@Override
	public int hashCode() {
		return Objects.hash(ciudad, provincia, pais, continente);
	}
}
