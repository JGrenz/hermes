package dominio;

import static java.time.temporal.ChronoUnit.DAYS;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import javax.mail.MessagingException;
import javax.swing.JOptionPane;

import com.lowagie.text.DocumentException;

import controladores.ControladorLogin;
import modelo.AccesoDatos;

public class Notificador {
	private LocalDate fechaActual;
	private LocalTime horaActual;
	private ArrayList<Pasaje> pasajes;
	private ArrayList<Notificacion> notificaciones;
	private ArrayList<Oferta> ofertas;
	private ArrayList<Viaje> viajes;
	private AccesoDatos accesoDatos;
	private Mail mail;
	private GeneradorPDF generadorPDF;
	private boolean primerIngreso;

	public Notificador(AccesoDatos accesoDatos) {
		this.accesoDatos = accesoDatos;
		this.fechaActual = LocalDate.now();
		this.horaActual = LocalTime.now();
		
		this.pasajes = (ArrayList<Pasaje>) accesoDatos.findAllPasajesDisponibles();

		this.mail = Mail.getInstance(accesoDatos);
		this.generadorPDF = new GeneradorPDF();
		this.primerIngreso = true;

		realizarNotificaciones();
	}

	private void realizarNotificaciones() {
		try {
			int segundo = 1000;
			int minuto = segundo * 60;
			int hora = minuto * 60;
			int medioDia = hora * 12;

//			vencerOfertas(medioDia);
//            chequearPopUps(minuto);
//            notificacionesReserva(medioDia);

			while (true) {
				chequearPopUps();
				notificacionesReserva(); // tambien hace pasaje.setEstadoPasaje("VENCIDO");

				vencerOfertas();

				finalizarViaje();

				Thread.sleep(minuto);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (DocumentException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		} catch (com.itextpdf.text.DocumentException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private void finalizarViaje() {
		this.viajes = (ArrayList<Viaje>) accesoDatos.obtenerViajes();

		for (Viaje viaje : viajes) {
			if (viaje.getEstadoViaje().equals("ABIERTO")) {
				if (diasQueFaltan(viaje.getFechaSalida()) + 1 < 1) {
					viaje.setEstado("FINALIZADO");
					accesoDatos.editarViaje(viaje);
					finalizarPasajes(viaje);
				}
			}
		}
	}

	private void finalizarPasajes(Viaje viaje) {
		ArrayList<Pasaje> pasajesDeViaje = (ArrayList<Pasaje>) accesoDatos.findByViaje(viaje);

		for (Pasaje pasaje : pasajesDeViaje) {
			if (pasaje.getEstadoPasaje().equals("PAGADO")) {
				pasaje.setEstadoPasaje("FINALIZADO");
				accesoDatos.updatePasaje(pasaje);
			}
		}
	}

	private void vencerOfertas() {
		horaActual = LocalTime.now();

		this.ofertas = (ArrayList<Oferta>) accesoDatos.obtenerOfertas();

		for (Oferta oferta : ofertas) {
			if (diasQueFaltan(oferta.getFechaVencimiento()) < 1) {
				oferta.setEstado("INHABILITADO");
				accesoDatos.updateOferta(oferta);
			}
		}
	}

	private void chequearPopUps() {
		horaActual = LocalTime.now();

		this.notificaciones = (ArrayList<Notificacion>) accesoDatos.findAllNotificaciones();

		if (ControladorLogin.id_logeado() != null) {
			for (Notificacion notificacion : notificaciones) {
				if (fechaActual.equals(notificacion.getFechaVencimiento().toLocalDate())
						&& horaActual.getHour() == notificacion.getHoraVencimiento().getHour()
						&& horaActual.getMinute() == notificacion.getHoraVencimiento().getMinute()
						&& ControladorLogin.id_logeado().equals(notificacion.getEvento().getResponsable())
						&& !notificacion.getEvento().getDescripcionEvento().getEstado().equals("FINALIZADO")) {

					JOptionPane.showMessageDialog(null, notificacion.getContenido(), "Recordatorio de evento",
							JOptionPane.WARNING_MESSAGE);
				}
			}
		}
	}

	private void notificacionesReserva()
			throws MessagingException, com.itextpdf.text.DocumentException, DocumentException, IOException {
		fechaActual = LocalDate.now();
		if (primerIngreso || !fechaActual.equals(LocalDate.now())) {
			buscarViajesReservadosNotificar();
			enviarVoucher();

			primerIngreso = false;
		}
	}

//	private void chequearPopUps(int tiempo) throws InterruptedException {
//		while (true) {
//			horaActual = LocalTime.now();
//
//			this.notificaciones = (ArrayList<Notificacion>) accesoDatos.findAllNotificaciones();
//
//			if (ControladorLogin.id_logeado() != null) {
//
//				for (Notificacion notificacion : notificaciones) {
//					if (fechaActual.equals(notificacion.getFechaVencimiento().toLocalDate())
//							&& horaActual.getHour() == notificacion.getHoraVencimiento().getHour()
//							&& horaActual.getMinute() == notificacion.getHoraVencimiento().getMinute()
//							&& ControladorLogin.id_logeado().equals(notificacion.getEvento().getResponsable())) {
//
//						JOptionPane.showMessageDialog(null, notificacion.getContenido(), notificacion.getTitulo(),
//								JOptionPane.WARNING_MESSAGE);
//					}
//
//				}
//			}
//			Thread.sleep(tiempo);
//		}
//	}

//	private void vencerOfertas(int medioDia) throws InterruptedException {
//		while (true) {
//			horaActual = LocalTime.now();
//
//			this.ofertas = (ArrayList<Oferta>) accesoDatos.obtenerOfertas();
//
//			for (Oferta oferta : ofertas) {
//				if (diasQueFaltan(oferta.getFechaVencimiento()) < 1 ) {
//					oferta.setEstado("INHABILITADO");
//					accesoDatos.updateOferta(oferta);
//				}
//			}
//			Thread.sleep(medioDia);
//		}
//	}

//    private void notificacionesReserva(int tiempo) throws IOException, DocumentException, MessagingException,
//            com.itextpdf.text.DocumentException, InterruptedException {
//        while (true) {
//            fechaActual = LocalDate.now();
//            if (primerIngreso || !fechaActual.equals(LocalDate.now())) {
//                buscarViajesReservadosNotificar();
//                enviarVoucher();
//
//                primerIngreso = false;
//            }
//            Thread.sleep(tiempo);
//        }
//    }

	private void buscarViajesReservadosNotificar() {
		for (Pasaje pasaje : pasajes) {
			double porcentajeAbonado = calcularPorcentajeAbonado(pasaje);

			condicionesDeNotificacion(pasaje, porcentajeAbonado);
		}
	}

	private double calcularPorcentajeAbonado(Pasaje pasaje) {
		BigDecimal precioViajeConImpuesto = pasaje.getViaje().getPrecio().add(pasaje.getViaje().getImpuesto());

		return clacularPorcentajeAbonado(precioViajeConImpuesto, montoAcumulado(pasaje));
	}

	public BigDecimal montoAcumulado(Pasaje pasaje) {
		List<Operacion> operaciones = accesoDatos.findOperacionesByPasaje(pasaje);
		BigDecimal montoRetorno = new BigDecimal(0);

		for (Operacion operacion : operaciones)
			montoRetorno = montoRetorno.add(operacion.getMonto());

		montoRetorno = montoRetorno.setScale(2, RoundingMode.HALF_UP);
		return montoRetorno;
	}

	private double clacularPorcentajeAbonado(BigDecimal precio, BigDecimal montoTotal) {
		BigDecimal precioPorCien = montoTotal.multiply(new BigDecimal(100));
		BigDecimal divididoTotal = precioPorCien.divide(precio, 2, RoundingMode.HALF_UP);

		return Double.parseDouble(divididoTotal.toString());
	}

	private void condicionesDeNotificacion(Pasaje pasaje, double porcentajeAbonado) {
		LocalDate fechaCreacionPasaje = pasaje.getFecha_creacion().toLocalDate();

		notificarPagoPendiente(pasaje, porcentajeAbonado, fechaCreacionPasaje);

		notificarCancelacion(pasaje, porcentajeAbonado, fechaCreacionPasaje);
	}

	private void notificarPagoPendiente(Pasaje pasaje, double porcentajeAbonado, LocalDate fechaCreacionPasaje) {
		if (diasQueFaltan(fechaCreacionPasaje) == -5 && porcentajeAbonado <= 30) {
			redactarMailFaltaPago(pasaje);
		}
	}

	private long diasQueFaltan(LocalDate fecha) {
		return DAYS.between(fechaActual, fecha);
	}

	private void notificarCancelacion(Pasaje pasaje, double porcentajeAbonado, LocalDate fechaCreacionPasaje) {
		if (diasQueFaltan(fechaCreacionPasaje) == -7 && porcentajeAbonado <= 30) {
			redactarMailCancelacionPasaje(pasaje);
			pasaje.setEstadoPasaje("VENCIDO");
			accesoDatos.updatePasaje(pasaje);
		}
	}

	private void redactarMailFaltaPago(Pasaje pasaje) {
		String direccion = pasaje.getCliente().getEmail();
		String asunto = "Recordatorio de pago de reserva";
		String cuerpo = "Estimade " + pasaje.getCliente().getApellido() + " " + pasaje.getCliente().getNombre()
				+ ", se le recuerda que aún tiene pendiente el pago del viaje con destino hacia "
				+ pasaje.getViaje().getUbicacionDestino().getCiudad() + ", "
				+ pasaje.getViaje().getUbicacionDestino().getProvincia() + ", "
				+ pasaje.getViaje().getUbicacionDestino().getPais() + ". Si no efectúa el pago antes de "
				+ pasaje.getViaje().getFechaSalida() + ", se le retendrá el 100% de lo abonado y perderá el viaje.";

		this.mail.enviarConGMail(direccion, asunto, cuerpo);
	}

	private void redactarMailCancelacionPasaje(Pasaje pasaje) {
		String direccion = pasaje.getCliente().getEmail();
		String asunto = "Cancelación de pago de reserva";
		String cuerpo = "Estimade " + pasaje.getCliente().getApellido() + " " + pasaje.getCliente().getNombre()
				+ ", se le informa que su viaje con destino hacia "
				+ pasaje.getViaje().getUbicacionDestino().getCiudad() + ", "
				+ pasaje.getViaje().getUbicacionDestino().getProvincia() + ", "
				+ pasaje.getViaje().getUbicacionDestino().getPais() + " fue cancelado por falta de pago.";

		this.mail.enviarConGMail(direccion, asunto, cuerpo);
	}

	public boolean verificarCambioDeFecha() {
		if (fechaActual != LocalDate.now()) {
			fechaActual = LocalDate.now();
			return true;
		}
		return false;
	}

	public void enviarVoucher()
			throws IOException, DocumentException, MessagingException, com.itextpdf.text.DocumentException {
		List<Pasaje> pasajes = accesoDatos.findAllPasajesDisponibles();
		for (Pasaje pasaje : pasajes) {
			if (cumpleCondicionesDeEnvioVoucher(pasaje)) {
				List<Acompañante> acompañantes = accesoDatos.obtenerAcompañantesDePasaje(pasaje);

				generadorPDF.generar("Voucher", pasaje, acompañantes);

				redactarMailVoucher(pasaje);
			}
		}
	}

	private boolean cumpleCondicionesDeEnvioVoucher(Pasaje pasaje) {
		return diasQueFaltan(pasaje.getViaje().getFechaSalida()) == 2 && pasaje.getViaje().fueConfirmado()
				&& pasaje.getViaje().getEstadoViaje().equals("ABIERTO");
	}

	private void redactarMailVoucher(Pasaje pasaje) throws MessagingException {
		String asunto = "Voucher";
		String cuerpo = "Estimade " + pasaje.getCliente().getApellido() + " " + pasaje.getCliente().getNombre()
				+ ", se le deja adjunto el voucher que deberá presentar ante las autoridades del viaje.\n\n\n"
				+ "Le deseamos un buen viaje.";

		mail.enviarConGMail(pasaje.getCliente().getEmail(), asunto, cuerpo, "Voucher.pdf");
	}
}
