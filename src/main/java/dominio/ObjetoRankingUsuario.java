package dominio;

import java.math.BigDecimal;

public class ObjetoRankingUsuario {
	Usuario creador;
	BigDecimal totalRecaudado;
	int cantidadDePasajes;

	public ObjetoRankingUsuario(Usuario creador, BigDecimal totalRecaudado, int cantidadDePasajes) {
		this.creador = creador;
		this.totalRecaudado = totalRecaudado;
		this.cantidadDePasajes = cantidadDePasajes;
	}

	public Usuario getCreador() {
		return creador;
	}

	public BigDecimal getTotalRecaudado() {
		return totalRecaudado;
	}

	public int getCantidadDePasajes() {
		return cantidadDePasajes;
	}

}
