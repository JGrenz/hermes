package dominio.reportes;

import java.awt.Image;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import dominio.Operacion;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;

public class ReporteIngresosLocal {
	private JasperReport reporte;
	private JasperViewer reporteViewer;
	private JasperPrint reporteLleno;
	private Logger log = Logger.getLogger(ReporteIngresosLocal.class);

	public ReporteIngresosLocal(List<Operacion> operaciones, String tipoReporte, String local, String rangoFechaDesde,
			String rangoFechaHasta, Image imagen) {
		Map<String, Object> parametersMap = new HashMap<String, Object>();
		parametersMap.put("Fecha", new SimpleDateFormat("dd/MM/yyyy").format(new Date()));
		parametersMap.put("Local", local);
		parametersMap.put("rangoFechaDesde", rangoFechaDesde);
		parametersMap.put("rangoFechaHasta", rangoFechaHasta);
		parametersMap.put("imagen", imagen);

		if (tipoReporte.equals("viaje")) {
			try {
				this.reporte = (JasperReport) JRLoader
						.loadObjectFromFile("reportes" + File.separator + "ReporteIngresosViajeLocal.jasper");
				this.reporteLleno = JasperFillManager.fillReport(this.reporte, parametersMap,
						new JRBeanCollectionDataSource(operaciones));
				log.info("Se cargó correctamente el reporte");
			} catch (JRException ex) {
				log.error("Ocurrió un error mientras se cargaba el archivo ReporteIngresosViajeLocal.Jasper", ex);
			}
		} else {
			try {
				this.reporte = (JasperReport) JRLoader
						.loadObjectFromFile("reportes" + File.separator + "ReporteIngresosClienteLocal.jasper");
				this.reporteLleno = JasperFillManager.fillReport(this.reporte, parametersMap,
						new JRBeanCollectionDataSource(operaciones));
				log.info("Se cargó correctamente el reporte");
			} catch (JRException ex) {
				log.error("Ocurrió un error mientras se cargaba el archivo ReporteIngresosClienteLocal.Jasper", ex);
			}
		}
	}

	public void mostrar() {
		this.reporteViewer = new JasperViewer(this.reporteLleno, false);
		this.reporteViewer.setVisible(true);
	}

}