package dominio;

import java.time.LocalDateTime;
import java.util.Objects;

public class Evento {

	private int id;
	private String motivo, nombre, telefono, mail;
	private Usuario responsable, creador;
	private Usuario cliente;
	private LocalDateTime fechaCreacion;
	private DescripcionEvento descripcionEvento;

	public Evento(Usuario creador, Usuario responsable) {
		this.motivo = "";
		this.responsable = responsable;
		this.creador = creador;
		this.fechaCreacion = LocalDateTime.now();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMotivo() {
		return motivo;
	}

	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

	public Usuario getResponsable() {
		return responsable;
	}

	public LocalDateTime getFecha_creacion() {
		return fechaCreacion;
	}

	public void setFecha_creacion(LocalDateTime fecha_creacion) {
		this.fechaCreacion = fecha_creacion;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public Usuario getCliente() {
		return cliente;
	}

	public void setCliente(Usuario cliente) {
		this.cliente = cliente;
	}

	public Usuario getCreador() {
		return creador;
	}

	public void setResponsable(Usuario empleado) {
		this.responsable = empleado;
	}

	public DescripcionEvento getDescripcionEvento() {
		return descripcionEvento;
	}

	public void setDescripcionEvento(DescripcionEvento descripcionEvento) {
		this.descripcionEvento = descripcionEvento;
	}

	@Override
	public String toString() {
		return descripcionEvento.getTitulo();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		Evento evento = (Evento) o;
		return id == evento.id && Objects.equals(creador, evento.creador);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, creador);
	}
}
