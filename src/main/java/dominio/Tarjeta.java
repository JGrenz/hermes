package dominio;

import java.time.LocalDate;

public class Tarjeta extends Pago {

	private LocalDate fechaVencimiento;
	private String numero, nombreTitular, tipoTarjeta, empresaTarjeta;

	public Tarjeta(LocalDate fechaVencimiento, String numero, String nombreTitular, String tipoTarjeta,
			String empresaTarjeta) {
		this.fechaVencimiento = fechaVencimiento;
		this.numero = numero;
		this.nombreTitular = nombreTitular;
		this.tipoTarjeta = tipoTarjeta;
		this.empresaTarjeta = empresaTarjeta;
	}

	public LocalDate getFechaVencimiento() {
		return fechaVencimiento;
	}

	public String getNumero() {
		return numero;
	}

	public String getNombreTitular() {
		return nombreTitular;
	}

	public String getEmpresa() {
		return empresaTarjeta;
	}

	public String getTipo() {
		return tipoTarjeta;
	}
}
