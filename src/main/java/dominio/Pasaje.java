package dominio;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class Pasaje {

	private enum EstadoPasaje {
		PAGADO, RESERVADO, CANCELADO, VENCIDO, FINALIZADO
	}

	private int id;
	private String codigo;
	private Viaje viaje;
	private Usuario cliente, responsable;
	private Local local;
	private LocalDateTime fecha_creacion;
	private LocalDate fecha_reserva;
	private EstadoPasaje estadoPasaje;
	private int descuento;

	public Pasaje(Viaje viaje, Usuario cliente, String estadoPasaje, Usuario responsable, Local local) {
//		this.codigo = MetodosDominio.generarCodigoReferenia();
		this.viaje = viaje;
		this.cliente = cliente;
		this.responsable = responsable;
		this.estadoPasaje = EstadoPasaje.valueOf(estadoPasaje);
		this.local = local;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public Viaje getViaje() {
		return viaje;
	}

	public Usuario getCliente() {
		return cliente;
	}

	public LocalDateTime getFecha_creacion() {
		return fecha_creacion;
	}

	public String getEstadoPasaje() {
		return estadoPasaje.toString();
	}

	public void setFechaCreacion(LocalDateTime fecha_creacion) {
		this.fecha_creacion = fecha_creacion;
	}

	public LocalDate getFecha_reserva() {
		return fecha_reserva;
	}

	public void setFechaReserva(LocalDate fecha_reserva) {
		this.fecha_reserva = fecha_reserva;
	}

	public void setEstadoPasaje(String estadoPasaje) {
		this.estadoPasaje = EstadoPasaje.valueOf(estadoPasaje);
	}

	public int getDescuento() {
		return descuento;
	}

	public void setDescuento(int descuento) {
		this.descuento = descuento;
	}

	public Usuario getResponsable() {
		return responsable;
	}

	public Local getLocal() {
		return local;
	}

	@Override
	public String toString() {
		return codigo;
	}
}
