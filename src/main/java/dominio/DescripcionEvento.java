package dominio;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class DescripcionEvento {
	private enum EstadoEvento {
		ABIERTO, FINALIZADO
	}

	private enum TipoEvento {
		CONSULTA, RECLAMO, OTRO
	}

	private int id;
	private LocalDate fechaResolucionReal;
	private LocalDateTime fechaCreacion, fechaResolucionEstimada;
	private String titulo, descripcion;
	private EstadoEvento estado;
	private TipoEvento tipo;

	public DescripcionEvento(String titulo, String descripcion, String tipo, LocalDateTime fechaResolucionEstimada) {
		this.titulo = titulo;
		this.descripcion = descripcion;
		this.tipo = TipoEvento.valueOf(tipo);
		this.fechaResolucionEstimada = fechaResolucionEstimada;
		this.estado = EstadoEvento.valueOf("ABIERTO");
		this.fechaCreacion = LocalDateTime.now();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public LocalDateTime getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(LocalDateTime fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public LocalDateTime getFechaResolucionEstimada() {
		return fechaResolucionEstimada;
	}

	public void setFechaResolucionEstimada(LocalDateTime fechaResolucionEstimada) {
		this.fechaResolucionEstimada = fechaResolucionEstimada;
	}

	public LocalDate getFechaResolucionReal() {
		return fechaResolucionReal;
	}

	public void setFechaResolucionReal(LocalDate fechaResolucionReal) {
		this.fechaResolucionReal = fechaResolucionReal;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

//    public Evento getEvento() {
//        return evento;
//    }
//
//    public void setEvento(Evento evento) {
//        this.evento = evento;
//    }

	public String getEstado() {
		return estado.toString();
	}

	public void setEstado(String estado) {
		this.estado = EstadoEvento.valueOf(estado);
	}

	public String getTipo() {
		return tipo.toString();
	}

	public void setTipo(TipoEvento tipo) {
		this.tipo = tipo;
	}

}
