package dominio;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Objects;

public class Oferta {
	public enum EstadoOferta {
		HABILITADO, INHABILITADO
	}

	private int id;
	private String titulo;
	private String codigoReferencia;
	private int porcentajeDescuento;
	private EstadoOferta estado;
	private LocalDateTime fechaCreacion;
	private LocalDate fechaVencimiento;

	public Oferta(String titulo, int porcentajeDescuento, LocalDate fechaVencimiento) {
		this.titulo = titulo;
		this.porcentajeDescuento = porcentajeDescuento;
//        this.codigoReferencia = MetodosDominio.generarCodigoReferenia();
		this.fechaCreacion = LocalDateTime.now();
		this.fechaVencimiento = fechaVencimiento;

		setEstado("HABILITADO");
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public String getCodigoReferencia() {
		return codigoReferencia;
	}

	public void setCodigoReferencia(String codigoReferencia) {
		this.codigoReferencia = codigoReferencia;
	}

	public int getPorcentajeDescuento() {
		return porcentajeDescuento;
	}

	public String getEstado() {
		return estado.toString();
	}

	public void setEstado(String estado) {
		this.estado = EstadoOferta.valueOf(estado);
	}

	public LocalDateTime getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(LocalDateTime fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public LocalDate getFechaVencimiento() {
		return fechaVencimiento;
	}

	public void setFechaVencimiento(LocalDate fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public void setPorcentajeDescuento(int porcentajeDescuento) {
		this.porcentajeDescuento = porcentajeDescuento;
	}

	public void setEstado(EstadoOferta estado) {
		this.estado = estado;
	}

	@Override
	public String toString() {
		return codigoReferencia;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		Oferta oferta = (Oferta) o;
		return id == oferta.id && porcentajeDescuento == oferta.porcentajeDescuento
				&& Objects.equals(codigoReferencia, oferta.codigoReferencia) && estado == oferta.estado
				&& Objects.equals(fechaCreacion, oferta.fechaCreacion);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, titulo, codigoReferencia, porcentajeDescuento, estado, fechaCreacion, fechaVencimiento);
	}
}
