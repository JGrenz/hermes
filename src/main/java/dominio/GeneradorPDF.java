package dominio;

import com.itextpdf.text.Image;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfWriter;
import com.sun.mail.imap.Rights;

import javax.annotation.Resources;
import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class GeneradorPDF {
	private static com.itextpdf.text.Font fuenteTItulo = new com.itextpdf.text.Font(
			com.itextpdf.text.Font.FontFamily.TIMES_ROMAN, 25, com.itextpdf.text.Font.BOLD);
	private static com.itextpdf.text.Font fuenteResaltadaNormal = new com.itextpdf.text.Font(
			com.itextpdf.text.Font.FontFamily.TIMES_ROMAN, 12, com.itextpdf.text.Font.ITALIC, BaseColor.GRAY);
	private static com.itextpdf.text.Font fuenteResaltadaNegrita = new com.itextpdf.text.Font(
			com.itextpdf.text.Font.FontFamily.TIMES_ROMAN, 12, com.itextpdf.text.Font.BOLD);
	private static com.itextpdf.text.Font fuenteSubtitulo = new com.itextpdf.text.Font(
			com.itextpdf.text.Font.FontFamily.TIMES_ROMAN, 18, com.itextpdf.text.Font.BOLD);
	private static com.itextpdf.text.Font fuenteDetalle = new com.itextpdf.text.Font(
			com.itextpdf.text.Font.FontFamily.TIMES_ROMAN, 8, com.itextpdf.text.Font.ITALIC);

	public static void generar(String nombreArchivo, Pasaje pasaje, java.util.List<Acompañante> acompañantes)
			throws IOException, com.itextpdf.text.DocumentException {
		String carpetaTempURL = System.getProperty("java.io.tmpdir");
		com.itextpdf.text.Document pdf = new com.itextpdf.text.Document();
		PdfWriter.getInstance(pdf, new FileOutputStream(carpetaTempURL + nombreArchivo + ".pdf"));
		pdf.open();
		agregarMetaData(pdf);
		crearPDF(pdf, pasaje, acompañantes);
		pdf.close();
	}

	private static void agregarMetaData(com.itextpdf.text.Document document) {
		document.addTitle("Voucher de viaje");
		document.addSubject("Voucher de viaje");
		document.addKeywords("Voucher, PDF, Viaje, Hermes");
		document.addAuthor("Empresa Hermes");
		document.addCreator("Empresa Hermes");
	}

	private static void crearPDF(com.itextpdf.text.Document pdf, Pasaje pasaje,
			java.util.List<Acompañante> acompañantes) throws com.itextpdf.text.DocumentException, IOException {
		crearTitulo(pdf);

		crearContenido(pdf, pasaje, acompañantes);
	}

	public static void crearImagen(com.itextpdf.text.Document pdf)
			throws IOException, com.itextpdf.text.DocumentException {
		BufferedImage bufferedImage = ImageIO.read(Resources.class.getResource("/imagenes/hermespersonajepdf.png"));
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageIO.write(bufferedImage, "png", baos);
		Image imagen = Image.getInstance(baos.toByteArray());

		imagen.scalePercent(25);
		imagen.setAbsolutePosition(PageSize.A4.getWidth() - 100, PageSize.A4.getHeight() - 100);

		pdf.add(imagen);
	}

	private static void crearTitulo(com.itextpdf.text.Document pdf)
			throws com.itextpdf.text.DocumentException, IOException {
		com.itextpdf.text.Paragraph titulo = new com.itextpdf.text.Paragraph();
		String formattedLocalDate = horaFechaActual();

		titulo.add(new com.itextpdf.text.Paragraph(
				"No olvidar presentar este documento ante las autoridades del viaje.", fuenteDetalle));
		titulo.add(new com.itextpdf.text.Paragraph("Reporte generado: " + formattedLocalDate, fuenteDetalle));

		crearImagen(pdf);

		com.itextpdf.text.Paragraph textoTitulo = new com.itextpdf.text.Paragraph("Voucher", fuenteTItulo);
		textoTitulo.setAlignment(1);
		titulo.add(textoTitulo);

		saltarLinea(titulo, 2);
		pdf.add(titulo);
	}

	private static String horaFechaActual() {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
		return LocalDateTime.now().format(formatter);
	}

	private static void saltarLinea(com.itextpdf.text.Paragraph paragraph, int numeroLineas) {
		for (int i = 0; i < numeroLineas; i++) {
			paragraph.add(new com.itextpdf.text.Paragraph(" "));
		}
	}

	private static void crearContenido(com.itextpdf.text.Document pdf, Pasaje pasaje,
			java.util.List<Acompañante> acompañantes) throws com.itextpdf.text.DocumentException {
		crearDatosDeViaje(pdf, pasaje);

		crearDatosDeCliente(pdf, pasaje.getCliente());

		if (acompañantes.size() > 0) {
			crearDatosDeAcompañante(pdf, acompañantes);
		}
	}

	private static void crearDatosDeViaje(com.itextpdf.text.Document document, Pasaje pasaje)
			throws com.itextpdf.text.DocumentException {
		com.itextpdf.text.Paragraph datosViaje = new com.itextpdf.text.Paragraph("Datos de viaje: ", fuenteSubtitulo);
		saltarLinea(datosViaje, 1);

		agregarFrase(datosViaje, "Codigo de referencia: ", pasaje.getCodigo());

		BigDecimal precioFinal = pasaje.getViaje().getPrecio().add(pasaje.getViaje().getImpuesto());
		agregarFrase(datosViaje, "Precio: ", "$" + precioFinal);

		Ubicacion ubicacionOrigen = pasaje.getViaje().getUbicacionOrigen();
		agregarFrase(datosViaje, "Origen: ", "" + ubicacionOrigen.getCiudad() + ", " + ubicacionOrigen.getProvincia()
				+ ", " + ubicacionOrigen.getPais());

		Ubicacion ubicacionDestino = pasaje.getViaje().getUbicacionDestino();
		agregarFrase(datosViaje, "Destino: ", "" + ubicacionDestino.getCiudad() + ", " + ubicacionDestino.getProvincia()
				+ ", " + ubicacionDestino.getPais());

		agregarFrase(datosViaje, "Hora de salida: ", "" + pasaje.getViaje().getHoraSalida());

		agregarFrase(datosViaje, "Transporte: ", "" + pasaje.getViaje().getMedioTransporte());

		saltarLinea(datosViaje, 2);
		document.add(datosViaje);
	}

	private static void agregarFrase(com.itextpdf.text.Paragraph datosViaje, String textoNegrita, String textoNormal) {
		Phrase horaSalida2 = new Phrase();

		horaSalida2.add(new Chunk(textoNegrita, fuenteResaltadaNegrita));
		horaSalida2.add(new Chunk(textoNormal, fuenteResaltadaNormal));
		horaSalida2.add(Chunk.NEWLINE);

		datosViaje.add(horaSalida2);
	}

	private static void crearDatosDeCliente(com.itextpdf.text.Document pdf, Usuario cliente) throws DocumentException {
		com.itextpdf.text.Paragraph datosDeCliente = new com.itextpdf.text.Paragraph("Datos de viaje: ",
				fuenteSubtitulo);
		saltarLinea(datosDeCliente, 1);

		agregarFrase(datosDeCliente, "Apellido: ", cliente.getApellido());
		agregarFrase(datosDeCliente, "Nombre: ", cliente.getNombre());
		agregarFrase(datosDeCliente, "DNI: ", cliente.getDni());

		pdf.add(datosDeCliente);
	}

	private static void crearDatosDeAcompañante(Document pdf, java.util.List<Acompañante> acompañantes) {
		com.itextpdf.text.Paragraph datosDeAcompañante = new Paragraph("Datos de acompañante: ", fuenteSubtitulo);
		saltarLinea(datosDeAcompañante, 1);

		for (int i = 0; i < acompañantes.size(); i++) {
			agregarFrase(datosDeAcompañante, "Apellido" + i + ": ", acompañantes.get(i).getApellido());
			agregarFrase(datosDeAcompañante, "Nombre" + i + ": ", acompañantes.get(i).getNombre());
			agregarFrase(datosDeAcompañante, "DNI:" + i + ": ", acompañantes.get(i).getDni());
		}
	}

	public static void abrir(String nombreArchivo) {
		try {
			File path = new File(nombreArchivo + ".pdf");
			Desktop.getDesktop().open(path);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void crear(String nombreArchivo, String comprobante) throws IOException, DocumentException {
		com.itextpdf.text.Document pdf = new com.itextpdf.text.Document();
		try {
			PdfWriter.getInstance(pdf, new FileOutputStream(nombreArchivo + ".pdf"));
		} catch (DocumentException e) {
			JOptionPane.showMessageDialog(null,
					"El archivo se encuentra abierto, por favor cierrelo y vuelva a intentar para poder generar el pdf.");
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			JOptionPane.showMessageDialog(null,
					"El archivo se encuentra abierto, por favor cierrelo y vuelva a intentar para poder generar el pdf.",
					"¡Advertencia!", JOptionPane.WARNING_MESSAGE);
			e.printStackTrace();
		}
		pdf.open();
		generarPDFConString(pdf, comprobante);
		pdf.close();
	}

	private static void generarPDFConString(Document pdf, String comprobante) throws IOException, DocumentException {
		crearTituloComprobante(pdf);

		com.itextpdf.text.Paragraph datosComprobante = new com.itextpdf.text.Paragraph(" ", fuenteSubtitulo);

		boolean textoNormal = false;
		String textoNegrita = "";
		String textoComun = "";

		for (int i = 0; i < comprobante.length(); i++) {
			if (comprobante.charAt(i) != '\n') {
				if (!textoNormal) {
					textoNegrita = textoNegrita + comprobante.charAt(i);
					if (comprobante.charAt(i) == ':') {
						textoNormal = true;
					}
				} else {
					textoComun = textoComun + comprobante.charAt(i);
				}
			} else {
				agregarFrase(datosComprobante, textoNegrita, textoComun);
				textoNegrita = "";
				textoComun = "";
				textoNormal = false;
			}
		}

		pdf.add(datosComprobante);
	}

	private static void agregarDato(Document pdf, String texto) throws DocumentException {
		com.itextpdf.text.Paragraph dato = new com.itextpdf.text.Paragraph(texto, fuenteResaltadaNormal);

		pdf.add(dato);
	}

	private static void crearTituloComprobante(Document pdf) throws IOException, DocumentException {
		com.itextpdf.text.Paragraph titulo = new com.itextpdf.text.Paragraph();
		String formattedLocalDate = horaFechaActual();

		titulo.add(new com.itextpdf.text.Paragraph("Reporte generado: " + formattedLocalDate, fuenteDetalle));

		crearImagen(pdf);

		com.itextpdf.text.Paragraph textoTitulo = new com.itextpdf.text.Paragraph("Comprobante", fuenteTItulo);
		textoTitulo.setAlignment(1);
		titulo.add(textoTitulo);

		saltarLinea(titulo, 2);
		pdf.add(titulo);
	}
}
