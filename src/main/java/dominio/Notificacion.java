package dominio;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class Notificacion {
	private enum EstadoNotificacion {
		ABIERTO, FINALIZADO
	}

	private int id;
	private LocalDate fechaCreacion;
	private LocalDateTime fechaVencimiento;
	private LocalTime horaVencimiento;
	private String titulo, contenido;
	private EstadoNotificacion estado;
	private Evento evento;

	public Notificacion(LocalTime horaVencimiento, String titulo, String contenido, Evento evento) {
		this.fechaVencimiento = evento.getDescripcionEvento().getFechaResolucionEstimada();
		this.horaVencimiento = horaVencimiento;
		this.titulo = titulo;
		this.contenido = contenido;
		this.estado = EstadoNotificacion.valueOf("ABIERTO");
		this.evento = evento;
		this.fechaCreacion = LocalDate.now();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public LocalDate getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(LocalDate fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public LocalDateTime getFechaVencimiento() {
		return fechaVencimiento;
	}

	public void setFechaVencimiento(LocalDateTime fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}

	public LocalTime getHoraVencimiento() {
		return horaVencimiento;
	}

	public void setHoraVencimiento(LocalTime horaVencimiento) {
		this.horaVencimiento = horaVencimiento;
	}

	public String getEstado() {
		return estado.toString();
	}

	public void setEstado(String estado) {
		this.estado = EstadoNotificacion.valueOf(estado);
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getContenido() {
		return contenido;
	}

	public void setContenido(String contenido) {
		this.contenido = contenido;
	}

	public Evento getEvento() {
		return evento;
	}

	public void setEvento(Evento evento) {
		this.evento = evento;
	}

	@Override
	public String toString() {
		return "Notificacion{" + "id=" + id + ", fechaCreacion=" + fechaCreacion + ", fechaVencimiento="
				+ fechaVencimiento + ", horaVencimiento=" + horaVencimiento + ", titulo='" + titulo + '\''
				+ ", contenido='" + contenido + '\'' + ", estado=" + estado + ", evento=" + evento + '}';
	}

}
