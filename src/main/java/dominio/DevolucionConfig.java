package dominio;

import java.time.LocalDateTime;

public class DevolucionConfig {

	private int id;
	private Usuario creador;
	private int porcentaje, desde, hasta;
	private LocalDateTime fechaCreacion;
	private Estado estado;

	public DevolucionConfig(Usuario creador, int porcentaje, int desde, int hasta) {
		this.creador = creador;
		this.porcentaje = porcentaje;
		this.desde = desde;
		this.hasta = hasta;
		estado = Estado.HABILITADO;
		this.fechaCreacion = LocalDateTime.now();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Usuario getCreador() {
		return creador;
	}

	public void setCreador(Usuario creador) {
		this.creador = creador;
	}

	public int getPorcentaje() {
		return porcentaje;
	}

	public void setPorcentaje(int porcentaje) {
		this.porcentaje = porcentaje;
	}

	public int getDesde() {
		return desde;
	}

	public void setDesde(int desde) {
		this.desde = desde;
	}

	public int getHasta() {
		return hasta;
	}

	public void setHasta(int hasta) {
		this.hasta = hasta;
	}

	public LocalDateTime getFechaCreacion() {
		return fechaCreacion;
	}
	
	public void setFechaCreacion(LocalDateTime fecha) {
		this.fechaCreacion = fecha;
	}
	
	public String getEstado(){
		return estado.toString();
	}

	public void setEstado(String estado){
		this.estado = Estado.valueOf(estado);
	}
	
	@Override
	public String toString() {
		return ""+id;
	}
}
