package dominio;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.UUID;

public class MetodosDominio<T> {

	private static String generarCodigoReferenia() {
		UUID uuid = UUID.randomUUID();

		String codigoCompleto = uuid.toString();

		String codigoSinGuiones = codigoCompleto.replaceAll("-", "");

		String subStringCodigo = codigoSinGuiones.substring(0, 5);

		return subStringCodigo;
	}

	public static String generarCodigoReferenia(List<?> lista)
			throws InvocationTargetException, IllegalAccessException {
		String codigoReferencia = generarCodigoReferenia();

		for (int i = 0; i < lista.size(); i++) {
			Method[] metodos = lista.get(i).getClass().getMethods();

			for (Method metodo : metodos) {

				if (metodo.getName().equals("getCodigoReferencia")) {
					String codigoReferenciaEnLista = (String) metodo.invoke(lista.get(i), null);

					if (codigoReferencia.equals(codigoReferenciaEnLista)) {
						codigoReferencia = generarCodigoReferenia();
						i = 0;
					}
				}
			}
		}
		return codigoReferencia;
	}

}
