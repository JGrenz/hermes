package dominio;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;

public class Viaje implements Comparable<Object> {
	private enum EstadoViaje {
		ABIERTO, CANCELADO, FINALIZADO
	}

	private int id;
	private Ubicacion ubicacionOrigen, ubicacionDestino;
	private LocalDate fechaSalida;
	private LocalTime horaSalida;
	private Duration horasDeViaje;
	private int capacidad;
	private MedioTransporte medioTransporte;
	private EstadoViaje estadoViaje;
	private String codigoReferencia;

	private BigDecimal precio, impuesto;
	private boolean confirmado;
	private Oferta oferta;

	public Viaje(Ubicacion ubicacionSalida, Ubicacion ubicacionLlegada, LocalDate fechaSalida, LocalTime horaSalida,
			Duration horasDeViaje, int capacidad, MedioTransporte medioTransporte, BigDecimal precio,
			BigDecimal impuesto) {
		this.ubicacionOrigen = ubicacionSalida;
		this.ubicacionDestino = ubicacionLlegada;
		this.fechaSalida = fechaSalida;
		this.horaSalida = horaSalida;
		this.horasDeViaje = horasDeViaje;
		this.capacidad = capacidad;
		this.medioTransporte = medioTransporte;
		this.precio = precio;
		this.impuesto = impuesto;
		this.confirmado = false;
		this.estadoViaje = EstadoViaje.ABIERTO;
//        this.codigoReferencia = MetodosDominio.generarCodigoReferenia();
	}

	public BigDecimal getImpuesto() {
		return impuesto;
	}

	public void setImpuesto(BigDecimal impuesto) {
		this.impuesto = impuesto;
	}

	public void setPrecio(BigDecimal precio) {
		this.precio = precio;
	}

	public MedioTransporte getMedioTransporte() {
		return medioTransporte;
	}

	public void setMedioTransporte(MedioTransporte medioTransporte) {
		this.medioTransporte = medioTransporte;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Ubicacion getUbicacionOrigen() {
		return ubicacionOrigen;
	}

	public Ubicacion getUbicacionDestino() {
		return ubicacionDestino;
	}

	public void setUbicacionOrigen(Ubicacion ubicacionOrigen) {
		this.ubicacionOrigen = ubicacionOrigen;
	}

	public void setUbicacionDestino(Ubicacion ubicacionDestino) {
		this.ubicacionDestino = ubicacionDestino;
	}

	public LocalDate getFechaSalida() {
		return fechaSalida;
	}

	public void setFechaSalida(LocalDate nuevaFecha) {
		this.fechaSalida = nuevaFecha;
	}

	public LocalTime getHoraSalida() {
		return horaSalida;
	}

	public Duration getHorasDeViaje() {
		return horasDeViaje;
	}

	public int getCapacidad() {
		return capacidad;
	}

	public String getEstadoViaje() {
		return estadoViaje.toString();
	}

	public String getCodigoReferencia() {
		return codigoReferencia;
	}

	public void confirmar() {
		this.confirmado = true;
	}

	public boolean fueConfirmado() {
		return this.confirmado;
	}

	public BigDecimal getPrecio() {
		return precio;
	}

	public void setHoraSalida(LocalTime horaSalida) {
		this.horaSalida = horaSalida;
	}

	public void setEstado(String estado) {
		this.estadoViaje = EstadoViaje.valueOf(estado);
	}

	public void setCodigoReferencia(String codigoReferencia) {
		this.codigoReferencia = codigoReferencia;
	}

	public void setCapacidad(int capacidad) {
		this.capacidad = capacidad;
	}

	public void setHorasDeViaje(Duration horasDeViaje) {
		this.horasDeViaje = horasDeViaje;
	}

	public Oferta getOferta() {
		return oferta;
	}

	public void setOferta(Oferta oferta) {
		this.oferta = oferta;
	}

	@Override
	public String toString() {
		return this.codigoReferencia;
	}

	@Override
	public int compareTo(Object arg0) {
		return arg0.toString().compareTo(codigoReferencia);
	}
}
