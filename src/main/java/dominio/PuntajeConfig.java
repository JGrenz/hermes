package dominio;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class PuntajeConfig {

	private int id;
	private BigDecimal valorEnPrecio;
	private int valorEnPuntos;
	private BigDecimal valorUnitario;
	private int añosVencimiento;
	private int mesesVencimiento;
	private Usuario creador;
	private LocalDateTime fechaCreacion;

	public PuntajeConfig(BigDecimal valorEnPrecio, int valorEnPuntos, BigDecimal valorUnitarioPunto,
			int añosVencimiento, int mesesVencimiento, Usuario creador) {
		this.valorEnPrecio = valorEnPrecio;
		this.valorEnPuntos = valorEnPuntos;
		this.valorUnitario = valorUnitarioPunto;
		this.añosVencimiento = añosVencimiento;
		this.mesesVencimiento = mesesVencimiento;
		this.creador = creador;
		this.fechaCreacion = LocalDateTime.now();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public BigDecimal getValorEnPrecio() {
		return valorEnPrecio;
	}

	public void setValorEnPrecio(BigDecimal valorEnPrecio) {
		this.valorEnPrecio = valorEnPrecio;
	}

	public int getValorEnPuntos() {
		return valorEnPuntos;
	}

	public void setValorEnPuntos(int valorEnPuntos) {
		this.valorEnPuntos = valorEnPuntos;
	}

	public BigDecimal getValorUnitario() {
		return valorUnitario;
	}

	public void setValorUnitario(BigDecimal valorUnitario) {
		this.valorUnitario = valorUnitario;
	}

	public int getAñosVencimiento() {
		return añosVencimiento;
	}

	public void setAñosVencimiento(int añosVencimiento) {
		this.añosVencimiento = añosVencimiento;
	}

	public int getMesesVencimiento() {
		return mesesVencimiento;
	}

	public void setMesesVencimiento(int mesesVencimiento) {
		this.mesesVencimiento = mesesVencimiento;
	}

	public Usuario getCreador() {
		return creador;
	}

	public void setCreador(Usuario creador) {
		this.creador = creador;
	}

	public LocalDateTime getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(LocalDateTime fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
}
