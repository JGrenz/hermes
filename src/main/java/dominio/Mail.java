package dominio;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.swing.*;

import modelo.AccesoDatos;

public class Mail {
	private int id;
	private String direccion, contraseña, remitente, firma;
	private static Mail INSTANCE;

	public static Mail getInstance(AccesoDatos accesoDatos) {
		List<Mail> mails = accesoDatos.findAllMails();

		if (INSTANCE == null) {
			if (mails.size() > 0) {
				INSTANCE = mails.get(0);
			} else {
				INSTANCE = new Mail("", "", "", "");
			}
		}
		return INSTANCE;
	}

	public Mail(String direccion, String contraseña, String remitente, String firma) {
		this.direccion = direccion;
		this.contraseña = contraseña;
		this.remitente = remitente;
		this.firma = firma;
		INSTANCE = this;
	}

	public void enviarConGMail(String destinatario, String asunto, String cuerpoMail) {
		Properties properties = System.getProperties();
		properties.put("mail.smtp.host", "smtp.gmail.com");
		properties.put("mail.smtp.user", direccion);
		properties.put("mail.smtp.clave", contraseña);
		properties.put("mail.smtp.auth", "true"); // Usar autenticación mediante usuario y clave
		properties.put("mail.smtp.starttls.enable", "true"); // Para conectar de manera segura al servidor SMTP
		properties.put("mail.smtp.port", "587");

		Session session = Session.getDefaultInstance(properties);
		MimeMessage message = new MimeMessage(session);

		try {
			cuerpoMail = cuerpoMail + "\n\n\n\n";

			message.setFrom(new InternetAddress(direccion, remitente));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(destinatario));
			message.setSubject(asunto);
			message.setText(cuerpoMail + firma);

			Transport transport = session.getTransport("smtp");
			transport.connect("smtp.gmail.com", direccion, contraseña);
			transport.sendMessage(message, message.getAllRecipients());
			transport.close();
		} catch (MessagingException | UnsupportedEncodingException me) {
			me.printStackTrace();
		}
	}

	public void enviarConGMail(String destinatario, String asunto, String cuerpoMail, String archivoAdjunto)
			throws MessagingException {
		Properties properties = System.getProperties();
		properties.put("mail.smtp.host", "smtp.gmail.com");
		properties.put("mail.smtp.user", direccion);
		properties.put("mail.smtp.clave", contraseña);
		properties.put("mail.smtp.auth", "true"); // Usar autenticación mediante usuario y clave
		properties.put("mail.smtp.starttls.enable", "true"); // Para conectar de manera segura al servidor SMTP
		properties.put("mail.smtp.port", "587");

		Session session = Session.getDefaultInstance(properties);
		MimeMessage message = new MimeMessage(session);

		BodyPart adjunto = new MimeBodyPart();
		String carpetaTempURL = System.getProperty("java.io.tmpdir");
		adjunto.setDataHandler(new DataHandler(new FileDataSource(carpetaTempURL + archivoAdjunto)));
		adjunto.setFileName("Voucher.pdf");

		cuerpoMail = cuerpoMail + "\n\n\n\n";

		BodyPart texto = new MimeBodyPart();
		texto.setText(cuerpoMail + firma);

		MimeMultipart multiParte = new MimeMultipart();
		multiParte.addBodyPart(texto);
		multiParte.addBodyPart(adjunto);

		try {


			message.setFrom(new InternetAddress(direccion, remitente));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(destinatario));
			message.setSubject(asunto);
			message.setContent(multiParte);

			Transport transport = session.getTransport("smtp");
			transport.connect("smtp.gmail.com", direccion, contraseña);
			transport.sendMessage(message, message.getAllRecipients());
			transport.close();
		} catch (MessagingException | UnsupportedEncodingException me) {
			JOptionPane.showMessageDialog(null, "No se pudo enviar el mail, intente nuevamente más tarde.",
					"¡Advertencia!", JOptionPane.WARNING_MESSAGE);
			me.printStackTrace();
		}
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getContraseña() {
		return contraseña;
	}

	public void setContraseña(String contraseña) {
		this.contraseña = contraseña;
	}

	public String getRemitente() {
		return remitente;
	}

	public void setRemitente(String remitente) {
		this.remitente = remitente;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirma() {
		return firma;
	}

	public void setFirma(String firma) {
		this.firma = firma;
	}
}