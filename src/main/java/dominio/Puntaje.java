package dominio;

import java.time.LocalDate;

public class Puntaje {
	private int id;
	private int cantidad;
	private LocalDate vencimiento;
	private Operacion operacion;

	public Puntaje(int cantidad, LocalDate vencimiento, Operacion operacion) {
		this.cantidad = cantidad;
		this.vencimiento = vencimiento;
		this.operacion = operacion;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public LocalDate getVencimiento() {
		return vencimiento;
	}

	public void setVencimiento(LocalDate vencimiento) {
		this.vencimiento = vencimiento;
	}

	public Operacion getOperacion() {
		return operacion;
	}

	public void setOperacion(Operacion operacion) {
		this.operacion = operacion;
	}
}
