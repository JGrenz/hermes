package dao;

import dominio.DescripcionEvento;
import dominio.Evento;

import java.util.List;

public interface DescripcionEventoDAO {
    boolean insert(DescripcionEvento descripcion, int idEvento);

    boolean update(DescripcionEvento descripcion);

    List<DescripcionEvento> findByEvento(Evento evento);

}
