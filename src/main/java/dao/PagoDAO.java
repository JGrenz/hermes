package dao;

import dominio.Pago;

public interface PagoDAO {

    int insert(Pago pago);
    
    Pago findById(int id);
}
