package dao;

import java.util.List;

import dominio.Puntaje;
import dominio.PuntajeConfig;
import dominio.Usuario;

public interface PuntajeDAO {
	
	boolean insertPuntaje(Puntaje puntaje);
	
	List<Puntaje> buscarPorCliente(Usuario cliente);

	PuntajeConfig datosPuntajeConfig();
	
	boolean updatePuntajeConfig(PuntajeConfig puntajeConfiguracion);

	boolean restarPuntaje(Puntaje puntaje);
}
