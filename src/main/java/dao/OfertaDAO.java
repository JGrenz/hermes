package dao;

import dominio.Oferta;

import java.util.List;

public interface OfertaDAO {

    boolean insert(Oferta oferta);

    Oferta findById(int id);

    Oferta findByTItulo(String titulo);

    Oferta findByCodigoReferencia(String codigoReferencia);

    boolean update(Oferta oferta);

    List<Oferta> findAll();


}
