package dao;

import dominio.Mail;

import java.util.List;

public interface MailDAO {
    boolean insert(Mail mail);

    boolean update(Mail mail);

    Mail findById(int id);

    List<Mail> findAll();

	boolean updateContraseña(Mail mail);
}
