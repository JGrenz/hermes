package dao;

import java.util.List;

import dominio.Acompañante;
import dominio.Pasaje;

public interface AcompañanteDAO {
	
	boolean insert(Acompañante acompañante);

	List<Acompañante> findAll();

	Acompañante findById(int id);

	Acompañante findByDNI(String dni);

	List<Acompañante> findByPasaje(Pasaje pasaje);

	boolean update(Acompañante acompañante);
}
