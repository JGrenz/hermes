package dao;

import java.util.List;

import dominio.Devolucion;
import dominio.DevolucionConfig;

public interface DevolucionDAO {

	boolean insertDevolucion(Devolucion devolucion);

	boolean insertDevolucionConfig(DevolucionConfig devolucionConfig);

	boolean update(DevolucionConfig devolucionConfig);

	List<DevolucionConfig> findAll();

	DevolucionConfig datosDevolucionConfig(int cantidadDias);
}
