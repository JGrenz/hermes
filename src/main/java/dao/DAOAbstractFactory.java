
package dao;

public interface DAOAbstractFactory {
	
	MedioTransporteDAO crearMedioTransporteDAO();
	
	OperacionDAO crearOperacionDAO();
	
	PasajeDAO crearPasajeDAO();
	
	UbicacionDAO crearUbicacionDAO();
	
	ViajeDAO crearViajeDAO();
	
	UsuarioDAO crearUsuarioDAO();

	PagoDAO crearPagoDAO();

	LocalDAO crearLocalDAO();

	PaisContinenteDAO crearPaisContinenteDAO();

	AcompañanteDAO crearAcompañanteDAO();

	MailDAO crearMailDAO();
	
	PasajeroDAO crearPasajeroDAO();

	EventoDAO crearEventoDAO();
	
	PuntajeDAO crearPuntajeDAO();
	
	DevolucionDAO crearDevolucionDAO();

	OfertaDAO crearOfertaDAO();

	NotificacionDAO crearNotificacionDAO();

    GastoDAO crearGastoDAO();

    DescripcionEventoDAO crearDescripcionDAO();
}
