package dao;

import dominio.MedioTransporte;

import java.util.List;

public interface MedioTransporteDAO {
	
	boolean insert(MedioTransporte medioTrasporte);
	
	MedioTransporte findById(int id);
	
	MedioTransporte findByName(String nombre);

	List<MedioTransporte> findAll();

	boolean update(MedioTransporte medioTransporte);
	
	boolean update(MedioTransporte medioTransporte, String estado);
	
	boolean enUso(MedioTransporte medioTranpsorte);
}
