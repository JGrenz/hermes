package dao;

import java.util.List;
import dominio.Ubicacion;

public interface UbicacionDAO {

	boolean insert(Ubicacion ubicacion);
	
	boolean update(Ubicacion ubicacion);
	
	List<Ubicacion> findAll();

	Ubicacion findById(int id);

	List<Ubicacion> findByCiudad(String ciudad);

	List<Ubicacion> findByProvincia(String provincia);

	List<Ubicacion> findByPais(String pais);

	List<Ubicacion> findByContinente(String continente);

	List<Ubicacion> findByTags(List<String> tags);
	
	List<Ubicacion> findByEstado(String estado);
	
	boolean enUso(Ubicacion ubicacion);

	boolean update(Ubicacion ubicacion, String estado);
}
