package dao;

import java.util.List;

import dominio.Oferta;
import dominio.Viaje;

public interface ViajeDAO {
	
	boolean insert(Viaje viaje);
	
	boolean update(Viaje viaje);

	boolean darDeBaja(Viaje viaje);
	
	List<Viaje> findAll();

	Viaje findById(int id);

	Viaje findByCOdigoReferencia(String codigoReferencia);
	
	List<Viaje> findByEstado(String estado);
	
	List<Viaje> findByDisponibles();
	
	List<Viaje> findByOferta(Oferta oferta);
	
	boolean updateOferta(Viaje viaje);
}
