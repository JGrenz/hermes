package dao;

import dominio.Local;

import java.util.List;

public interface LocalDAO {

    boolean insert(Local local);

    boolean update(Local local);

    Local findByNombre(String nombre);

    Local findById(int id);

    List<Local> findAll();

    boolean update(Local local, String estado);
}
