package dao;

import dominio.Gasto;

import java.util.List;

public interface GastoDAO {

    boolean insert(Gasto gasto);

    boolean update(Gasto gasto);

    Gasto findByCodigoReferencia();

    List<Gasto> findAll();

	boolean cancelarGasto(Gasto gasto);
}
