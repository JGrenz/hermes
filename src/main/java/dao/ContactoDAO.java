package dao;

import dominio.Contacto;

public interface ContactoDAO {

    int insert(Contacto contacto);

    boolean update (Contacto contacto);

}
