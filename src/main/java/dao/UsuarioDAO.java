package dao;

import dominio.Usuario;

import java.util.List;

public interface UsuarioDAO {
	
	boolean insert(Usuario usuario);
	
	boolean update(Usuario usuario);

	List<Usuario> findAll();

	Usuario findById(int id);

	Usuario findByDNI(int DNI);

	Usuario findByNombreUSuario(String usuario);

	List<Usuario> findByNombreYApellido(String nombre, String apellido);

	int contarUsuariosSimilares(String nombreUsuario);
	
	boolean verificarExisteUsuario(String nombreUsuario);
	
	boolean update (Usuario usuario, String estado);
}
