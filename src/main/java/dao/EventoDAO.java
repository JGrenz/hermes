package dao;

import java.util.List;

import dominio.DescripcionEvento;
import dominio.Evento;

public interface EventoDAO {
	
	boolean insert(Evento evento, DescripcionEvento descripcionEvento);
	
	boolean update(Evento evento);
	
//	boolean finalizar(Evento evento);

	Evento findEventoById(int id);
	
	List<Evento> findAll();
	
	List<Evento> findByResponsable(int id);
	
	List<Evento> findByTipo(String tipo);
	
	List<Evento> findByNombreYApellido(String nombre, String apellido);
}
