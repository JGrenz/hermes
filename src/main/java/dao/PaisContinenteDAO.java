package dao;

import java.util.List;
import java.util.Locale;

public interface PaisContinenteDAO {

    List<String> finAllContinentes();

    List<Locale> findAllPaises(String continente);

}
