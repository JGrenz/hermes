package dao;

import dominio.Acompañante;
import dominio.ObjetoRankingViaje;
import dominio.ObjetoRankingUsuario;
import dominio.Operacion;
import dominio.Pasaje;

import java.time.LocalDateTime;
import java.util.List;

public interface OperacionDAO {
	
	boolean insert(Operacion operacion);

	boolean insert(Operacion operacion, List<Acompañante> lista_acompañantes);

	List<Operacion> findAll();

	List<Operacion> findByPasaje(Pasaje pasaje);
	
	List<ObjetoRankingUsuario> findRankingEmpleados(LocalDateTime desde, LocalDateTime hasta);
	
	List<ObjetoRankingViaje> findRankingViajes(LocalDateTime desde, LocalDateTime hasta);
	
	List<ObjetoRankingUsuario> findRankingClientes(LocalDateTime desde, LocalDateTime hasta);

}
