package dao;


import dominio.Acompañante;
import dominio.Pasaje;

public interface PasajeroDAO {
	
	boolean insert(Pasaje pasaje, Acompañante acompañante);
	
	int countByPasaje(Pasaje pasaje);

}
