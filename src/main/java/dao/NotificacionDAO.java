package dao;

import dominio.Evento;
import dominio.Notificacion;

import java.util.List;

public interface NotificacionDAO {
    boolean insert(Notificacion notificiacion);

    boolean update(Notificacion notificacion);

    Notificacion findByFKEvento(Evento evetno);

    List<Notificacion> findAll();

}
