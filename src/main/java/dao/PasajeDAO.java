package dao;

import java.util.List;

import dominio.Pasaje;
import dominio.Usuario;
import dominio.Viaje;

public interface PasajeDAO {
	
	int insert(Pasaje pasaje);
	
	boolean update(Pasaje pasaje);
	
	boolean updateReserva(Pasaje pasaje);
	
	List<Pasaje> findAll();
	
	List<Pasaje> findAllDisponibles();

	Pasaje findById(int id);

	Pasaje findByCodigoReferencia(String codigoReferencia);

	List<Pasaje> findByCliente(Usuario cliente);

	List<Pasaje> findByViaje(Viaje viaje);
	
	int countByDisponibles(Viaje viaje);
	
	boolean cancelarPasajesDelCliente(Usuario usuario);
	
	boolean tienePasajes(Usuario cliente);
}
