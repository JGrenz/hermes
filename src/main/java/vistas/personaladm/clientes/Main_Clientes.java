package vistas.personaladm.clientes;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import vistas.TextPrompt;

public class Main_Clientes {

	private static Main_Clientes INSTANCE;
	private JPanel frmMainClientes;
	private JTable tablaClientes, tablaPasajes;
	private DefaultTableModel modelClientes, modelPasajes;
	private String[] nombreColumnasCliente = { "Nombre", "Apellido", "DNI", "Nacimiento", "Usuario", "Email",
			"Telefono principal", "Estado" };
	private String[] nombreColumnasPasaje = { "Fecha de creacion", "Codigo de referencia", "Origen", "destino",
			"Fecha y hora de salida", "Fecha y hora de llegada", "Transporte", "Precio del viaje", "Monto abonado",
			"Estado" };
	private JTextField txtUsuario, txtNombre, txtApellido, txtDNI;
	private JLabel lblBusquedaPor, telefono1, telefono2, telefono3, telefono4, telefono5, lblFiltrado;
	private JButton btnAgregar, btnBorrar, btnEditar, btnBuscarCliente;
	private JPanel panelBusqueda, panelNorte, panelSur, panelTelefonos;
	private JRadioButton radioTodos, radioHabilitados, radioInhabilitados;
	private ButtonGroup filtro;

	public static Main_Clientes getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new Main_Clientes();
		}
		return INSTANCE;
	}

	public Main_Clientes() {

		frmMainClientes = new JPanel();

		filtro = new ButtonGroup();
		GridBagLayout gbl_frmMainClientes = new GridBagLayout();
		gbl_frmMainClientes.columnWidths = new int[] { 1137, 0 };
		gbl_frmMainClientes.rowHeights = new int[] { 69, 200, 427, 0 };
		gbl_frmMainClientes.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gbl_frmMainClientes.rowWeights = new double[] { 0.0, 0.0, 1.0, Double.MIN_VALUE };
		frmMainClientes.setLayout(gbl_frmMainClientes);

		panelNorte = new JPanel();
		GridBagConstraints gbc_panelNorte = new GridBagConstraints();
		gbc_panelNorte.fill = GridBagConstraints.BOTH;
		gbc_panelNorte.insets = new Insets(0, 0, 5, 0);
		gbc_panelNorte.gridx = 0;
		gbc_panelNorte.gridy = 0;
		frmMainClientes.add(panelNorte, gbc_panelNorte);

		btnAgregar = new JButton("Agregar");
		btnAgregar.setIcon(new ImageIcon(Main_Clientes.class.getResource("/imagenes/iconos/clienteAgregar.png")));
		panelNorte.add(btnAgregar);

		btnBorrar = new JButton("Habilitar / Inhabilitar");
		btnBorrar.setIcon(new ImageIcon(Main_Clientes.class.getResource("/imagenes/iconos/clienteHabInhab.png")));
		panelNorte.add(btnBorrar);

		btnEditar = new JButton("Editar");
		btnEditar.setIcon(new ImageIcon(Main_Clientes.class.getResource("/imagenes/iconos/clienteEditar.png")));
		panelNorte.add(btnEditar);

		panelBusqueda = new JPanel();
		panelBusqueda.setBounds(10, 51, 658, 143);
		GridBagConstraints gbc_panelBusqueda = new GridBagConstraints();
		gbc_panelBusqueda.fill = GridBagConstraints.BOTH;
		gbc_panelBusqueda.insets = new Insets(0, 0, 5, 0);
		gbc_panelBusqueda.gridx = 0;
		gbc_panelBusqueda.gridy = 1;
		frmMainClientes.add(panelBusqueda, gbc_panelBusqueda);
		GridBagLayout gbl_opcionesDeBusqueda = new GridBagLayout();
		gbl_opcionesDeBusqueda.columnWidths = new int[] { 127, 53, 145, 131, 127, 0 };
		gbl_opcionesDeBusqueda.rowHeights = new int[] { 30, 25, 25, 26, 0 };
		gbl_opcionesDeBusqueda.columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		gbl_opcionesDeBusqueda.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		panelBusqueda.setLayout(gbl_opcionesDeBusqueda);

		lblFiltrado = new JLabel("Filtrar por:");
		lblFiltrado.setPreferredSize(new Dimension(0, 30));
		lblFiltrado.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_lblFiltrado = new GridBagConstraints();
		gbc_lblFiltrado.fill = GridBagConstraints.BOTH;
		gbc_lblFiltrado.insets = new Insets(0, 0, 5, 5);
		gbc_lblFiltrado.gridx = 0;
		gbc_lblFiltrado.gridy = 0;
		panelBusqueda.add(lblFiltrado, gbc_lblFiltrado);

		lblBusquedaPor = new JLabel("Búsqueda por:");
		lblBusquedaPor.setPreferredSize(new Dimension(0, 30));
		lblBusquedaPor.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_lblBusquedaPor = new GridBagConstraints();
		gbc_lblBusquedaPor.fill = GridBagConstraints.BOTH;
		gbc_lblBusquedaPor.insets = new Insets(0, 0, 5, 5);
		gbc_lblBusquedaPor.gridx = 2;
		gbc_lblBusquedaPor.gridy = 0;
		panelBusqueda.add(lblBusquedaPor, gbc_lblBusquedaPor);

		btnBuscarCliente = new JButton("Buscar");
		btnBuscarCliente.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_btnBuscarCliente = new GridBagConstraints();
		gbc_btnBuscarCliente.fill = GridBagConstraints.BOTH;
		gbc_btnBuscarCliente.insets = new Insets(0, 0, 5, 5);
		gbc_btnBuscarCliente.gridx = 3;
		gbc_btnBuscarCliente.gridy = 0;
		panelBusqueda.add(btnBuscarCliente, gbc_btnBuscarCliente);

		radioTodos = new JRadioButton("Todos");
		radioTodos.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_rdbtnTodos = new GridBagConstraints();
		gbc_rdbtnTodos.fill = GridBagConstraints.BOTH;
		gbc_rdbtnTodos.insets = new Insets(0, 0, 5, 5);
		gbc_rdbtnTodos.gridx = 0;
		gbc_rdbtnTodos.gridy = 1;
		panelBusqueda.add(radioTodos, gbc_rdbtnTodos);
		filtro.add(radioTodos);

		JLabel lblNombreYApellido = new JLabel("Nombre y apellido");
		lblNombreYApellido.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_lblNombreYApellido = new GridBagConstraints();
		gbc_lblNombreYApellido.fill = GridBagConstraints.BOTH;
		gbc_lblNombreYApellido.insets = new Insets(0, 0, 5, 5);
		gbc_lblNombreYApellido.gridx = 2;
		gbc_lblNombreYApellido.gridy = 1;
		panelBusqueda.add(lblNombreYApellido, gbc_lblNombreYApellido);

		txtNombre = new JTextField();
		txtNombre.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_txtNombre = new GridBagConstraints();
		gbc_txtNombre.fill = GridBagConstraints.BOTH;
		gbc_txtNombre.insets = new Insets(0, 0, 5, 5);
		gbc_txtNombre.gridx = 3;
		gbc_txtNombre.gridy = 1;
		panelBusqueda.add(txtNombre, gbc_txtNombre);
		txtNombre.setColumns(10);
		TextPrompt phNombre = new TextPrompt("Ingrese nombre...", txtNombre);
		phNombre.setText("Ingrese nombre...");
		phNombre.changeAlpha(0.75f);
		phNombre.changeStyle(Font.ITALIC);

		txtApellido = new JTextField();
		txtApellido.setPreferredSize(new Dimension(0, 30));
		txtApellido.setColumns(10);
		GridBagConstraints gbc_txtApellido = new GridBagConstraints();
		gbc_txtApellido.fill = GridBagConstraints.BOTH;
		gbc_txtApellido.insets = new Insets(0, 0, 5, 0);
		gbc_txtApellido.gridx = 4;
		gbc_txtApellido.gridy = 1;
		panelBusqueda.add(txtApellido, gbc_txtApellido);
		TextPrompt phApellido = new TextPrompt("Ingrese apellido...", txtApellido);
		phApellido.setText("Ingrese apellido...");
		phApellido.changeAlpha(0.75f);
		phApellido.changeStyle(Font.ITALIC);

		radioHabilitados = new JRadioButton("Habilitados");
		radioHabilitados.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_rdbtnHabilitados = new GridBagConstraints();
		gbc_rdbtnHabilitados.fill = GridBagConstraints.BOTH;
		gbc_rdbtnHabilitados.insets = new Insets(0, 0, 5, 5);
		gbc_rdbtnHabilitados.gridx = 0;
		gbc_rdbtnHabilitados.gridy = 2;
		panelBusqueda.add(radioHabilitados, gbc_rdbtnHabilitados);
		radioHabilitados.setSelected(true);
		filtro.add(radioHabilitados);

		JLabel lblUsuario = new JLabel("Usuario");
		lblUsuario.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_lblUsuario = new GridBagConstraints();
		gbc_lblUsuario.fill = GridBagConstraints.BOTH;
		gbc_lblUsuario.insets = new Insets(0, 0, 5, 5);
		gbc_lblUsuario.gridx = 2;
		gbc_lblUsuario.gridy = 2;
		panelBusqueda.add(lblUsuario, gbc_lblUsuario);

		txtUsuario = new JTextField();
		txtUsuario.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_txtUsuario = new GridBagConstraints();
		gbc_txtUsuario.fill = GridBagConstraints.BOTH;
		gbc_txtUsuario.insets = new Insets(0, 0, 5, 0);
		gbc_txtUsuario.gridwidth = 2;
		gbc_txtUsuario.gridx = 3;
		gbc_txtUsuario.gridy = 2;
		panelBusqueda.add(txtUsuario, gbc_txtUsuario);
		txtUsuario.setColumns(10);
		TextPrompt phUsuario = new TextPrompt("Ingrese usuario...", txtUsuario);
		phUsuario.setText("Ingrese usuario...");
		phUsuario.changeAlpha(0.75f);
		phUsuario.changeStyle(Font.ITALIC);

		radioInhabilitados = new JRadioButton("Inhabilitados");
		radioInhabilitados.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_rdbtnInhabilitados = new GridBagConstraints();
		gbc_rdbtnInhabilitados.fill = GridBagConstraints.BOTH;
		gbc_rdbtnInhabilitados.insets = new Insets(0, 0, 0, 5);
		gbc_rdbtnInhabilitados.gridx = 0;
		gbc_rdbtnInhabilitados.gridy = 3;
		panelBusqueda.add(radioInhabilitados, gbc_rdbtnInhabilitados);
		filtro.add(radioInhabilitados);

		JLabel lblDNI = new JLabel("DNI");
		lblDNI.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_lblDNI = new GridBagConstraints();
		gbc_lblDNI.fill = GridBagConstraints.BOTH;
		gbc_lblDNI.insets = new Insets(0, 0, 0, 5);
		gbc_lblDNI.gridx = 2;
		gbc_lblDNI.gridy = 3;
		panelBusqueda.add(lblDNI, gbc_lblDNI);

		txtDNI = new JTextField();
		txtDNI.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_txtDNI = new GridBagConstraints();
		gbc_txtDNI.fill = GridBagConstraints.BOTH;
		gbc_txtDNI.gridwidth = 2;
		gbc_txtDNI.gridx = 3;
		gbc_txtDNI.gridy = 3;
		panelBusqueda.add(txtDNI, gbc_txtDNI);
		txtDNI.setColumns(10);
		TextPrompt phDNI = new TextPrompt("Ingrese DNI...", txtDNI);
		phDNI.setText("Ingrese DNI...");
		phDNI.changeAlpha(0.75f);
		phDNI.changeStyle(Font.ITALIC);

		panelSur = new JPanel();
		GridBagConstraints gbc_panelSur = new GridBagConstraints();
		gbc_panelSur.fill = GridBagConstraints.BOTH;
		gbc_panelSur.gridx = 0;
		gbc_panelSur.gridy = 2;
		frmMainClientes.add(panelSur, gbc_panelSur);
		GridBagLayout gbl_panelSur = new GridBagLayout();
		gbl_panelSur.columnWidths = new int[] { 894, 0, 0 };
		gbl_panelSur.rowHeights = new int[] { 30, 100, 30, 120, 0 };
		gbl_panelSur.columnWeights = new double[] { 1.0, 0.0, Double.MIN_VALUE };
		gbl_panelSur.rowWeights = new double[] { 0.0, 1.0, 0.0, 1.0, Double.MIN_VALUE };
		panelSur.setLayout(gbl_panelSur);

		JLabel lblClientes = new JLabel("Clientes");
		GridBagConstraints gbc_lblClientes = new GridBagConstraints();
		gbc_lblClientes.fill = GridBagConstraints.BOTH;
		gbc_lblClientes.insets = new Insets(0, 5, 5, 5);
		gbc_lblClientes.gridx = 0;
		gbc_lblClientes.gridy = 0;
		panelSur.add(lblClientes, gbc_lblClientes);

		JScrollPane spClientes = new JScrollPane();
		GridBagConstraints gbc_spClientes = new GridBagConstraints();
		gbc_spClientes.fill = GridBagConstraints.BOTH;
		gbc_spClientes.insets = new Insets(0, 0, 5, 5);
		gbc_spClientes.gridx = 0;
		gbc_spClientes.gridy = 1;
		panelSur.add(spClientes, gbc_spClientes);
		modelClientes = new DefaultTableModel(null, nombreColumnasCliente) {
			private static final long serialVersionUID = 1L;

			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		tablaClientes = new JTable(modelClientes);
		tablaClientes.getTableHeader().setReorderingAllowed(false);
		spClientes.setViewportView(tablaClientes);

		panelTelefonos = new JPanel();
		GridBagConstraints gbc_panelTelefonos = new GridBagConstraints();
		gbc_panelTelefonos.fill = GridBagConstraints.BOTH;
		gbc_panelTelefonos.insets = new Insets(0, 0, 5, 0);
		gbc_panelTelefonos.gridx = 1;
		gbc_panelTelefonos.gridy = 1;
		panelSur.add(panelTelefonos, gbc_panelTelefonos);
		GridBagLayout gbl_panelTelefonos = new GridBagLayout();
		gbl_panelTelefonos.columnWidths = new int[] { 100, 100, 0 };
		gbl_panelTelefonos.rowHeights = new int[] { 30, 30, 30, 30, 30, 0 };
		gbl_panelTelefonos.columnWeights = new double[] { 0.0, 0.0, Double.MIN_VALUE };
		gbl_panelTelefonos.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		panelTelefonos.setLayout(gbl_panelTelefonos);

		JLabel lblTelefono1 = new JLabel("Teléfono 1:");
		lblTelefono1.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_lblTelefono1 = new GridBagConstraints();
		gbc_lblTelefono1.fill = GridBagConstraints.BOTH;
		gbc_lblTelefono1.insets = new Insets(0, 0, 5, 5);
		gbc_lblTelefono1.gridx = 0;
		gbc_lblTelefono1.gridy = 0;
		panelTelefonos.add(lblTelefono1, gbc_lblTelefono1);

		telefono1 = new JLabel("");
		telefono1.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_telefono1 = new GridBagConstraints();
		gbc_telefono1.fill = GridBagConstraints.BOTH;
		gbc_telefono1.insets = new Insets(0, 0, 5, 0);
		gbc_telefono1.gridx = 1;
		gbc_telefono1.gridy = 0;
		panelTelefonos.add(telefono1, gbc_telefono1);

		JLabel lblTelefono2 = new JLabel("Teléfono 2:");
		lblTelefono2.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_lblTelefono2 = new GridBagConstraints();
		gbc_lblTelefono2.fill = GridBagConstraints.BOTH;
		gbc_lblTelefono2.insets = new Insets(0, 0, 5, 5);
		gbc_lblTelefono2.gridx = 0;
		gbc_lblTelefono2.gridy = 1;
		panelTelefonos.add(lblTelefono2, gbc_lblTelefono2);

		telefono2 = new JLabel("");
		telefono2.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_telefono2 = new GridBagConstraints();
		gbc_telefono2.fill = GridBagConstraints.BOTH;
		gbc_telefono2.insets = new Insets(0, 0, 5, 0);
		gbc_telefono2.gridx = 1;
		gbc_telefono2.gridy = 1;
		panelTelefonos.add(telefono2, gbc_telefono2);

		JLabel lblTelefono3 = new JLabel("Teléfono 3:");
		lblTelefono3.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_lblTelefono3 = new GridBagConstraints();
		gbc_lblTelefono3.fill = GridBagConstraints.BOTH;
		gbc_lblTelefono3.insets = new Insets(0, 0, 5, 5);
		gbc_lblTelefono3.gridx = 0;
		gbc_lblTelefono3.gridy = 2;
		panelTelefonos.add(lblTelefono3, gbc_lblTelefono3);

		telefono3 = new JLabel("");
		telefono3.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_telefono3 = new GridBagConstraints();
		gbc_telefono3.fill = GridBagConstraints.BOTH;
		gbc_telefono3.insets = new Insets(0, 0, 5, 0);
		gbc_telefono3.gridx = 1;
		gbc_telefono3.gridy = 2;
		panelTelefonos.add(telefono3, gbc_telefono3);

		JLabel lblTelefono4 = new JLabel("Teléfono 4:");
		lblTelefono4.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_lblTelefono4 = new GridBagConstraints();
		gbc_lblTelefono4.fill = GridBagConstraints.BOTH;
		gbc_lblTelefono4.insets = new Insets(0, 0, 5, 5);
		gbc_lblTelefono4.gridx = 0;
		gbc_lblTelefono4.gridy = 3;
		panelTelefonos.add(lblTelefono4, gbc_lblTelefono4);

		telefono4 = new JLabel("");
		telefono4.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_telefono4 = new GridBagConstraints();
		gbc_telefono4.fill = GridBagConstraints.BOTH;
		gbc_telefono4.insets = new Insets(0, 0, 5, 0);
		gbc_telefono4.gridx = 1;
		gbc_telefono4.gridy = 3;
		panelTelefonos.add(telefono4, gbc_telefono4);

		JLabel lblTelefono5 = new JLabel("Teléfono 5:");
		lblTelefono5.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_lblTelefono5 = new GridBagConstraints();
		gbc_lblTelefono5.fill = GridBagConstraints.BOTH;
		gbc_lblTelefono5.insets = new Insets(0, 0, 0, 5);
		gbc_lblTelefono5.gridx = 0;
		gbc_lblTelefono5.gridy = 4;
		panelTelefonos.add(lblTelefono5, gbc_lblTelefono5);

		telefono5 = new JLabel("");
		telefono5.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_telefono5 = new GridBagConstraints();
		gbc_telefono5.fill = GridBagConstraints.BOTH;
		gbc_telefono5.gridx = 1;
		gbc_telefono5.gridy = 4;
		panelTelefonos.add(telefono5, gbc_telefono5);

		JLabel lblPasajesDeCliente = new JLabel("Pasajes de cliente seleccionado");
		GridBagConstraints gbc_lblPasajesDeCliente = new GridBagConstraints();
		gbc_lblPasajesDeCliente.fill = GridBagConstraints.BOTH;
		gbc_lblPasajesDeCliente.insets = new Insets(0, 5, 5, 5);
		gbc_lblPasajesDeCliente.gridx = 0;
		gbc_lblPasajesDeCliente.gridy = 2;
		panelSur.add(lblPasajesDeCliente, gbc_lblPasajesDeCliente);

		JScrollPane spPasajes = new JScrollPane();
		GridBagConstraints gbc_spPasajes = new GridBagConstraints();
		gbc_spPasajes.insets = new Insets(0, 0, 0, 5);
		gbc_spPasajes.fill = GridBagConstraints.BOTH;
		gbc_spPasajes.gridx = 0;
		gbc_spPasajes.gridy = 3;
		panelSur.add(spPasajes, gbc_spPasajes);
		modelPasajes = new DefaultTableModel(null, nombreColumnasPasaje) {
			private static final long serialVersionUID = 1L;

			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		tablaPasajes = new JTable(modelPasajes);
		tablaPasajes.getTableHeader().setReorderingAllowed(false);
		TableRowSorter<TableModel> sorter = new TableRowSorter<TableModel>(tablaPasajes.getModel());
//		List <RowSorter.SortKey> sortKeys = new ArrayList<RowSorter.SortKey>();
//		sortKeys.add(new RowSorter.SortKey(0, SortOrder.DESCENDING));
//		sorter.setSortKeys(sortKeys);
		tablaPasajes.setRowSorter(sorter);
		spPasajes.setViewportView(tablaPasajes);

		this.frmMainClientes.setVisible(false);
	}

	public void mostrarVentana() {
		this.frmMainClientes.setVisible(true);
	}

	public JButton getBtnAgregar() {
		return btnAgregar;
	}

	public JButton getBtnBorrar() {
		return btnBorrar;
	}

	public JButton getBtnEditar() {
		return btnEditar;
	}

	public DefaultTableModel getModelCliente() {
		return modelClientes;
	}

	public JTable getTablaClientes() {
		return tablaClientes;
	}

	public DefaultTableModel getModelPasaje() {
		return modelPasajes;
	}

	public JTable getTablaPasaje() {
		return tablaPasajes;
	}

	public String[] getNombreColumnasClientes() {
		return nombreColumnasCliente;
	}

	public String[] getNombreColumnasPasajes() {
		return nombreColumnasPasaje;
	}

	public JTextField getTxtUsuario() {
		return txtUsuario;
	}

	public JTextField getTxtDNI() {
		return txtDNI;
	}

	public JTextField getTxtNombre() {
		return txtNombre;
	}

	public JTextField getTxtApellido() {
		return txtApellido;
	}

	public JButton getBtnBuscarCliente() {
		return btnBuscarCliente;
	}

	public JRadioButton getRdbtnTodos() {
		return radioTodos;
	}

	public JRadioButton getRdbtnHabilitados() {
		return radioHabilitados;
	}

	public JRadioButton getRdbtnInhabilitados() {
		return radioInhabilitados;
	}

	public JLabel getTelefono1() {
		return telefono1;
	}

	public JLabel getTelefono2() {
		return telefono2;
	}

	public JLabel getTelefono3() {
		return telefono3;
	}

	public JLabel getTelefono4() {
		return telefono4;
	}

	public JLabel getTelefono5() {
		return telefono5;
	}

	public JPanel getPanel() {
		return frmMainClientes;
	}
}
