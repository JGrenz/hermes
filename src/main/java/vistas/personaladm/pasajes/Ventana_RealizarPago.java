package vistas.personaladm.pasajes;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import vistas.ValidacionCampos;

import com.toedter.calendar.JYearChooser;

public class Ventana_RealizarPago extends JDialog {

	private static final long serialVersionUID = 1L;
	private static Ventana_RealizarPago INSTANCE;
	private JDialog frmRealizarPago;
	private JPanel contentPane, panelTarjeta, panelPuntos;
	private JButton btnConfirmar;
	private JTextField txtMonto, txtMontoYaAbonado, txtPrecioViaje, txtNumero;
	private ButtonGroup grupoFormaPago;
	private JRadioButton radioEfectivo, radioTarjeta, radioPuntos;
	private JComboBox<String> comboBoxTipo, comboBoxEmpresa;
	private JYearChooser añoTarjeta;
	private JSpinner mesTarjeta;
	private JTextField txtPuntosAcumulados, txtPuntosAUtilizar;

	public static Ventana_RealizarPago getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new Ventana_RealizarPago();
		}
		return INSTANCE;
	}

	private Ventana_RealizarPago() {

		frmRealizarPago = new JDialog();
		frmRealizarPago.setIconImage(Toolkit.getDefaultToolkit()
				.getImage(Ventana_RealizarPago.class.getResource("/imagenes/hermesicono.png")));
		frmRealizarPago.setModalityType(ModalityType.APPLICATION_MODAL);
		frmRealizarPago.setResizable(false);
		frmRealizarPago.setTitle("Hermes - Realizar pago");
		frmRealizarPago.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmRealizarPago.setBounds(100, 100, 361, 568);
		frmRealizarPago.setLocationRelativeTo(null);

		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		frmRealizarPago.setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 355, 539);
		contentPane.add(panel);
		panel.setLayout(null);

		JLabel lblFormaDePago = new JLabel("Forma de pago");
		lblFormaDePago.setHorizontalAlignment(SwingConstants.CENTER);
		lblFormaDePago.setBounds(125, 11, 105, 30);
		panel.add(lblFormaDePago);

		radioEfectivo = new JRadioButton("Efectivo");
		radioEfectivo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (radioEfectivo.isSelected()) {
					setPagoEfectivo();
				}
			}
		});
		radioEfectivo.setHorizontalAlignment(SwingConstants.CENTER);
		radioEfectivo.setBounds(10, 50, 105, 25);
		radioEfectivo.setSelected(true);
		panel.add(radioEfectivo);

		radioTarjeta = new JRadioButton("Tarjeta");
		radioTarjeta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (radioTarjeta.isSelected()) {
					setPagoTarjeta();
				}
			}
		});
		radioTarjeta.setHorizontalAlignment(SwingConstants.CENTER);
		radioTarjeta.setBounds(125, 50, 105, 25);
		panel.add(radioTarjeta);

		radioPuntos = new JRadioButton("Puntos");
		radioPuntos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (radioPuntos.isSelected()) {
					setPagoPuntos();
				}
			}
		});
		radioPuntos.setHorizontalAlignment(SwingConstants.CENTER);
		radioPuntos.setBounds(240, 50, 105, 25);
		panel.add(radioPuntos);

		grupoFormaPago = new ButtonGroup();
		grupoFormaPago.add(radioEfectivo);
		grupoFormaPago.add(radioTarjeta);
		grupoFormaPago.add(radioPuntos);
		
		panelTarjeta = new JPanel();
		panelTarjeta.setBounds(10, 82, 335, 178);
		panel.add(panelTarjeta);
		GridBagLayout gbl_panelTarjeta = new GridBagLayout();
		gbl_panelTarjeta.columnWidths = new int[] { 50, 50, 50, 0 };
		gbl_panelTarjeta.rowHeights = new int[] { 30, 30, 30, 30, 0 };
		gbl_panelTarjeta.columnWeights = new double[] { 1.0, 1.0, 1.0, Double.MIN_VALUE };
		gbl_panelTarjeta.rowWeights = new double[] { 1.0, 1.0, 1.0, 1.0, Double.MIN_VALUE };
		panelTarjeta.setLayout(gbl_panelTarjeta);

		JLabel lblTipo = new JLabel("Tipo:");
		GridBagConstraints gbc_lblTipo = new GridBagConstraints();
		gbc_lblTipo.fill = GridBagConstraints.BOTH;
		gbc_lblTipo.insets = new Insets(0, 0, 5, 5);
		gbc_lblTipo.gridx = 0;
		gbc_lblTipo.gridy = 0;
		panelTarjeta.add(lblTipo, gbc_lblTipo);
		lblTipo.setHorizontalAlignment(SwingConstants.CENTER);

		comboBoxTipo = new JComboBox<String>();
		GridBagConstraints gbc_comboBoxTipo = new GridBagConstraints();
		gbc_comboBoxTipo.fill = GridBagConstraints.BOTH;
		gbc_comboBoxTipo.gridwidth = 2;
		gbc_comboBoxTipo.insets = new Insets(0, 0, 5, 0);
		gbc_comboBoxTipo.gridx = 1;
		gbc_comboBoxTipo.gridy = 0;
		panelTarjeta.add(comboBoxTipo, gbc_comboBoxTipo);
		comboBoxTipo.addItem("Crédito");
		comboBoxTipo.addItem("Débito");

		JLabel lblMarca = new JLabel("Marca:");
		GridBagConstraints gbc_lblMarca = new GridBagConstraints();
		gbc_lblMarca.fill = GridBagConstraints.BOTH;
		gbc_lblMarca.insets = new Insets(0, 0, 5, 5);
		gbc_lblMarca.gridx = 0;
		gbc_lblMarca.gridy = 1;
		panelTarjeta.add(lblMarca, gbc_lblMarca);
		lblMarca.setHorizontalAlignment(SwingConstants.CENTER);

		comboBoxEmpresa = new JComboBox<String>();
		GridBagConstraints gbc_comboBoxEmpresa = new GridBagConstraints();
		gbc_comboBoxEmpresa.gridwidth = 2;
		gbc_comboBoxEmpresa.fill = GridBagConstraints.BOTH;
		gbc_comboBoxEmpresa.insets = new Insets(0, 0, 5, 0);
		gbc_comboBoxEmpresa.gridx = 1;
		gbc_comboBoxEmpresa.gridy = 1;
		panelTarjeta.add(comboBoxEmpresa, gbc_comboBoxEmpresa);
		comboBoxEmpresa.addItem("Visa");
		comboBoxEmpresa.addItem("AmericanExpress");
		comboBoxEmpresa.addItem("MasterCard");

		JLabel lblNumero = new JLabel("Número:");
		GridBagConstraints gbc_lblNumero = new GridBagConstraints();
		gbc_lblNumero.fill = GridBagConstraints.BOTH;
		gbc_lblNumero.insets = new Insets(0, 0, 5, 5);
		gbc_lblNumero.gridx = 0;
		gbc_lblNumero.gridy = 2;
		panelTarjeta.add(lblNumero, gbc_lblNumero);
		lblNumero.setHorizontalAlignment(SwingConstants.CENTER);

		txtNumero = new JTextField();
		GridBagConstraints gbc_txtNumero = new GridBagConstraints();
		gbc_txtNumero.fill = GridBagConstraints.BOTH;
		gbc_txtNumero.insets = new Insets(0, 0, 5, 0);
		gbc_txtNumero.gridwidth = 2;
		gbc_txtNumero.gridx = 1;
		gbc_txtNumero.gridy = 2;
		panelTarjeta.add(txtNumero, gbc_txtNumero);
		txtNumero.setColumns(10);

		JLabel lblVto = new JLabel("Vencimiento:");
		GridBagConstraints gbc_lblVto = new GridBagConstraints();
		gbc_lblVto.fill = GridBagConstraints.BOTH;
		gbc_lblVto.insets = new Insets(0, 0, 0, 5);
		gbc_lblVto.gridx = 0;
		gbc_lblVto.gridy = 3;
		panelTarjeta.add(lblVto, gbc_lblVto);
		lblVto.setHorizontalAlignment(SwingConstants.CENTER);

		mesTarjeta = new JSpinner();
		GridBagConstraints gbc_mesTarjeta = new GridBagConstraints();
		gbc_mesTarjeta.fill = GridBagConstraints.BOTH;
		gbc_mesTarjeta.insets = new Insets(0, 0, 0, 5);
		gbc_mesTarjeta.gridx = 1;
		gbc_mesTarjeta.gridy = 3;
		panelTarjeta.add(mesTarjeta, gbc_mesTarjeta);
		mesTarjeta.setModel(new SpinnerNumberModel(LocalDate.now().getMonthValue(), 1, 12, 1));

		añoTarjeta = new JYearChooser();
		GridBagConstraints gbc_añoTarjeta = new GridBagConstraints();
		gbc_añoTarjeta.fill = GridBagConstraints.BOTH;
		gbc_añoTarjeta.gridx = 2;
		gbc_añoTarjeta.gridy = 3;
		panelTarjeta.add(añoTarjeta, gbc_añoTarjeta);
		
		panelPuntos = new JPanel();
		panelPuntos.setBounds(10, 82, 335, 178);
		panel.add(panelPuntos);
		GridBagLayout gbl_panelPuntos = new GridBagLayout();
		gbl_panelPuntos.columnWidths = new int[] { 126, 50 };
		gbl_panelPuntos.rowHeights = new int[] { 30, 30, 30, 30, 0 };
		gbl_panelPuntos.columnWeights = new double[] { 0.0, 1.0 };
		gbl_panelPuntos.rowWeights = new double[] { 1.0, 1.0, 1.0, 1.0, Double.MIN_VALUE };
		panelPuntos.setLayout(gbl_panelPuntos);
		
		JLabel lblPuntosAcumulados = new JLabel("Puntos acumulados:");
		lblPuntosAcumulados.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_lblPuntosAcumulados = new GridBagConstraints();
		gbc_lblPuntosAcumulados.fill = GridBagConstraints.BOTH;
		gbc_lblPuntosAcumulados.insets = new Insets(0, 0, 5, 5);
		gbc_lblPuntosAcumulados.gridx = 0;
		gbc_lblPuntosAcumulados.gridy = 0;
		panelPuntos.add(lblPuntosAcumulados, gbc_lblPuntosAcumulados);
		
		txtPuntosAcumulados = new JTextField();
		txtPuntosAcumulados.setEditable(false);
		GridBagConstraints gbc_txtPuntosAcumulados = new GridBagConstraints();
		gbc_txtPuntosAcumulados.insets = new Insets(0, 0, 5, 0);
		gbc_txtPuntosAcumulados.fill = GridBagConstraints.BOTH;
		gbc_txtPuntosAcumulados.gridx = 1;
		gbc_txtPuntosAcumulados.gridy = 0;
		panelPuntos.add(txtPuntosAcumulados, gbc_txtPuntosAcumulados);
		txtPuntosAcumulados.setColumns(10);
		
		JLabel lblPuntosAUtilizar = new JLabel("Puntos a utilizar:");
		lblPuntosAUtilizar.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_lblPuntosAUtilizar = new GridBagConstraints();
		gbc_lblPuntosAUtilizar.fill = GridBagConstraints.BOTH;
		gbc_lblPuntosAUtilizar.insets = new Insets(0, 0, 5, 5);
		gbc_lblPuntosAUtilizar.gridx = 0;
		gbc_lblPuntosAUtilizar.gridy = 1;
		panelPuntos.add(lblPuntosAUtilizar, gbc_lblPuntosAUtilizar);
		
		txtPuntosAUtilizar = new JTextField();
		GridBagConstraints gbc_txtPuntosAUtilizar = new GridBagConstraints();
		gbc_txtPuntosAUtilizar.insets = new Insets(0, 0, 5, 0);
		gbc_txtPuntosAUtilizar.fill = GridBagConstraints.BOTH;
		gbc_txtPuntosAUtilizar.gridx = 1;
		gbc_txtPuntosAUtilizar.gridy = 1;
		panelPuntos.add(txtPuntosAUtilizar, gbc_txtPuntosAUtilizar);
		txtPuntosAUtilizar.setColumns(10);
		
		JLabel lblPrecioDelViaje = new JLabel("Precio del viaje:");
		lblPrecioDelViaje.setHorizontalAlignment(SwingConstants.CENTER);
		lblPrecioDelViaje.setBounds(10, 265, 110, 30);
		panel.add(lblPrecioDelViaje);

		JLabel lblMontoYaAbonado = new JLabel("Monto abonado:");
		lblMontoYaAbonado.setHorizontalAlignment(SwingConstants.CENTER);
		lblMontoYaAbonado.setBounds(10, 300, 110, 30);
		panel.add(lblMontoYaAbonado);
		
		JLabel lblMonto = new JLabel("Monto a pagar:");
		lblMonto.setHorizontalAlignment(SwingConstants.CENTER);
		lblMonto.setBounds(10, 335, 110, 30);
		panel.add(lblMonto);
		
		txtPrecioViaje = new JTextField();
		txtPrecioViaje.setEditable(false);
		txtPrecioViaje.setColumns(10);
		txtPrecioViaje.setBounds(130, 265, 215, 30);
		panel.add(txtPrecioViaje);

		txtMontoYaAbonado = new JTextField();
		txtMontoYaAbonado.setEditable(false);
		txtMontoYaAbonado.setColumns(10);
		txtMontoYaAbonado.setBounds(130, 300, 215, 30);
		panel.add(txtMontoYaAbonado);
		
		txtMonto = new JTextField();
		txtMonto.setColumns(10);
		txtMonto.setBounds(130, 335, 215, 30);
		panel.add(txtMonto);

		btnConfirmar = new JButton("Confirmar");
		btnConfirmar.setBounds(219, 505, 126, 23);
		panel.add(btnConfirmar);
		
		setPagoEfectivo();
		
		this.frmRealizarPago.setVisible(false);
	}

	public void mostrarVentana() {
		this.frmRealizarPago.setVisible(true);
	}

	public String getTipoTarjeta() {
		return comboBoxTipo.getSelectedItem().toString();
	}

	public String getEmpresaTarjeta() {
		return comboBoxEmpresa.getSelectedItem().toString();
	}

	public JTextField getNumeroTarjeta() {
		return txtNumero;
	}

	public LocalDate getVtoTarjeta() {
		LocalDate vtoTarjeta = null;
		String mes = mesTarjeta.getValue().toString();
		String año = Integer.toString(añoTarjeta.getValue());
		String fecha = año + "/" + mes;
		SimpleDateFormat formato = new SimpleDateFormat("yyyy/MM");
		try {
			Date vto = formato.parse(fecha);
			vtoTarjeta = vto.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return vtoTarjeta;
	}

	public JComboBox<String> getComboBoxEmpresaTarjeta() {
		return comboBoxEmpresa;
	}

	public JComboBox<String> getComboBoxTipoTarjeta() {
		return comboBoxTipo;
	}

	public JRadioButton getBtnEfectivo() {
		return radioEfectivo;
	}

	public JRadioButton getBtnTarjeta() {
		return radioTarjeta;
	}

	public JRadioButton getBtnPunto() {
		return radioPuntos;
	}

	public JTextField getMonto() {
		return txtMonto;
	}
	
	public JTextField getMontoAbonado() {
		return txtMontoYaAbonado;
	}

	public JTextField getTxtPrecioViaje() {
		return txtPrecioViaje;
	}

	public JButton getBtnConfirmar() {
		return btnConfirmar;
	}

	public void cerrar() {
		limpiarCampos();
		this.frmRealizarPago.dispose();
	}
	
	public void limpiarCampos() {
		this.comboBoxEmpresa.setSelectedIndex(0);
		this.comboBoxTipo.setSelectedIndex(0);
		this.txtNumero.setText(null);
		this.txtMontoYaAbonado.setText(null);
		this.txtMonto.setText(null);
		this.txtPuntosAcumulados.setText(null);
		this.txtPuntosAUtilizar.setText(null);
		this.mesTarjeta.setValue(LocalDate.now().getMonthValue());
		this.añoTarjeta.setYear(LocalDate.now().getYear());
	}

	private boolean camposVacios() {
		if (txtMonto.getText().isEmpty()) {
			JOptionPane.showMessageDialog(null, "No pueden quedar campos vacíos.");
			return true;
		}
		if (radioTarjeta.isSelected()) {
			if (txtNumero.getText().isEmpty()
					|| mesTarjeta.getValue() == null || añoTarjeta == null
					//|| comboBoxTipo.getSelectedIndex() == -1 || comboBoxEmpresa.getSelectedIndex() == -1
					) {
				JOptionPane.showMessageDialog(null, "No pueden quedar campos vacíos.");
				return true;
			}
		}
		if (radioPuntos.isSelected()) {
			if (txtPuntosAUtilizar.getText().isEmpty()) {
				JOptionPane.showMessageDialog(null, "No pueden quedar campos vacíos.");
				return true;
			}
		}
		return false;
	}
	
	private boolean camposCorrectos() {
		ValidacionCampos validador = new ValidacionCampos();
		if(!validador.esMonto(txtMonto.getText())){
			return false;
		}
		if (radioTarjeta.isSelected()) {
			if(!validador.esNumeroTarjeta(txtNumero.getText())) {
					return false;
			}
		}
		if (radioPuntos.isSelected()) {
			if(!validador.esNumerico(txtPuntosAUtilizar.getText())) {
					return false;
			}
		}
		return true;
	}
	
	public boolean verificarCampo() {
		if (!camposVacios() && camposCorrectos())
			return true;
		else
			return false;
	}
	
	
	
	private void setPagoEfectivo() {
		panelTarjeta.setVisible(false);
		panelPuntos.setVisible(false);
		txtMonto.setEditable(true);
		txtMonto.setText("");
	}

	private void setPagoTarjeta() {
		panelTarjeta.setVisible(true);
		panelPuntos.setVisible(false);
		txtMonto.setEditable(true);
		txtMonto.setText("");
	}

	private void setPagoPuntos() {
		panelTarjeta.setVisible(false);
		panelPuntos.setVisible(true);
		txtMonto.setEditable(false);
		txtMonto.setText("");
	}
	
	public JTextField getTxtPuntosAcumulados() {
		return txtPuntosAcumulados;
	}
	
	public JTextField getTxtPuntosAUtilizar() {
		return txtPuntosAUtilizar;
	}
}
