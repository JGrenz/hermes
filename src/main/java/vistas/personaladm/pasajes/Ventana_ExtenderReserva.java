package vistas.personaladm.pasajes;

import java.awt.Toolkit;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import com.toedter.calendar.JDateChooser;

public class Ventana_ExtenderReserva extends JDialog {

	private static final long serialVersionUID = 1L;
	private static Ventana_ExtenderReserva INSTANCE;
	private JDialog frmExtenderReserva;
	private JPanel contentPane;
	private JButton btnConfirmar;
	private JDateChooser txtFechaReserva;

	public static Ventana_ExtenderReserva getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new Ventana_ExtenderReserva();
		}
		return INSTANCE;
	}

	private Ventana_ExtenderReserva() {

		frmExtenderReserva = new JDialog();
		frmExtenderReserva.setIconImage(Toolkit.getDefaultToolkit()
				.getImage(Ventana_ExtenderReserva.class.getResource("/imagenes/hermesicono.png")));
		frmExtenderReserva.setModalityType(ModalityType.APPLICATION_MODAL);
		frmExtenderReserva.setResizable(false);
		frmExtenderReserva.setTitle("Hermes - Extender reserva");
		frmExtenderReserva.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmExtenderReserva.setBounds(100, 100, 425, 183);
		frmExtenderReserva.setLocationRelativeTo(null);

		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		frmExtenderReserva.setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 419, 154);
		contentPane.add(panel);
		panel.setLayout(null);

		JLabel lblFecha = new JLabel("Nueva fecha de reserva");
		lblFecha.setHorizontalAlignment(SwingConstants.CENTER);
		lblFecha.setBounds(10, 11, 152, 30);
		panel.add(lblFecha);
		
		txtFechaReserva = new JDateChooser();
		Date fechaReserva = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(fechaReserva);
		c.add(Calendar.DATE, 7);
		Date fechaReservaMas7 = c.getTime();
		txtFechaReserva.setMinSelectableDate(fechaReservaMas7);
		txtFechaReserva.setBounds(172, 11, 237, 30);
		panel.add(txtFechaReserva);

		btnConfirmar = new JButton("Confirmar");
		btnConfirmar.setBounds(283, 120, 126, 23);
		panel.add(btnConfirmar);

		this.frmExtenderReserva.setVisible(false);
	}

	public void mostrarVentana() {
		this.frmExtenderReserva.setVisible(true);
	}

	public JButton getBtnConfirmar() {
		return btnConfirmar;
	}

	public void cerrar() {
		this.frmExtenderReserva.dispose();
	}

	public JDateChooser getReservaChooser() {
		return txtFechaReserva;
	}
	
	public LocalDate getTxtFechaReserva() {
		Date date = txtFechaReserva.getDate();
		return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
	}
}