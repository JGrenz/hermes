package vistas.personaladm.pasajes;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import vistas.ValidacionCampos;

import com.toedter.calendar.JDateChooser;
import com.toedter.calendar.JTextFieldDateEditor;
import com.toedter.calendar.JYearChooser;

public class Ventana_AgregarPasaje extends JDialog {

	private static final long serialVersionUID = 1L;
	private static Ventana_AgregarPasaje INSTANCE;
	private JDialog frmAgregarPasaje;
	private JPanel contentPane, panel, panelViajes, panelClientes, panelTarjeta, panelPuntos;
	private JButton btnConfirmar, btnBuscarViaje, btnBuscarCliente, btnSeleccionarViaje, btnSeleccionarCliente,
			btnAgregarAcompañante;
	private JTextField txtCliente, txtViaje, txtPrecioViaje, txtMonto, txtNumero, txtPrecioAcompañante;
	private JComboBox<String> cbPais, cbContinente;
	private JYearChooser añoTarjeta;
	private JSpinner mesTarjeta;
	private ButtonGroup grupoFormaPago;
	private JRadioButton radioEfectivo, radioTarjeta, radioPuntos;
	private JComboBox<String> comboBoxTipo, comboBoxEmpresa;
	private String[] columnasDeViajes = { "Codigo Referencia", "Origen", "Destino", "Fecha de salida", "Hora de salida",
			"Horas de viaje", "Medio de transporte", "Precio", "Cupos disponibles" };
	private String[] columnasDeClientes = { "Usuario", "Nombre", "Apellido", "DNI" };
	private String[] columnasDeAcompañantes = { "Nombre", "Apellido", "DNI", "Fecha nacimiento" };
	private DefaultTableModel modelViajes, modelClientes, modelAcompañantes, modelBuscarAcompañante;
	private JTable tablaViajes, tablaClientes, tablaAcompañantes;
	private JDateChooser txtFechaDesde, txtFechaHasta, txtNacimientoAcompañante, txtFechaReserva;
	private JTextField txtCiudad, txtProvincia, txtPrecioMaximo, txtPrecioMinimo;
	private JTextField txtNombre, txtApellido, txtDNI, txtUsuario;
	private JTextField txtNombreAcompañante, txtApellidoAcompañante, txtDNIAcompañante, txtTelefonoAcompañante,
			txtEmailAcompañante;
	private JButton btnPanelViaje, btnPanelCliente;
	private JPanel panelAgregarAcompañante;
	private JTextField txtDNIBuscarAcompañante;
	private JButton btnBuscarAcompañante, btnPanelAgregarAcompañante, btnLimpiarCampos, btnQuitarAcompañante;
	private JTextField txtPuntosAcumulados, txtPuntosAUtilizar;
	private JTextField txtDescuento;
	private JTextField txtPrecioDescuento;

	public static Ventana_AgregarPasaje getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new Ventana_AgregarPasaje();
		}
		return INSTANCE;
	}

	private Ventana_AgregarPasaje() {

		frmAgregarPasaje = new JDialog();
		frmAgregarPasaje.setIconImage(Toolkit.getDefaultToolkit()
				.getImage(Ventana_AgregarPasaje.class.getResource("/imagenes/hermesicono.png")));
		frmAgregarPasaje.setModalityType(ModalityType.APPLICATION_MODAL);
		frmAgregarPasaje.setResizable(false);
		frmAgregarPasaje.setTitle("Hermes - Emitir pasaje");
		frmAgregarPasaje.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmAgregarPasaje.setBounds(100, 100, 893, 663);
		frmAgregarPasaje.setLocationRelativeTo(null);

		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		frmAgregarPasaje.setContentPane(contentPane);
		contentPane.setLayout(null);

		panel = new JPanel();
		panel.setBounds(0, 0, 887, 634);
		contentPane.add(panel);
		panel.setLayout(null);

		/*************************************
		 * PANEL DE BUSQUEDA DE CLIENTES
		 *************************************************/

		panelClientes = new JPanel();
		panelClientes.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		panelClientes.setBounds(-887, 83, 887, 456);
		panel.add(panelClientes);
		panelClientes.setLayout(null);

		JScrollPane spClientes = new JScrollPane();
		spClientes.setBounds(10, 100, 867, 304);
		panelClientes.add(spClientes);
		modelClientes = new DefaultTableModel(null, columnasDeClientes) {
			private static final long serialVersionUID = 1L;

			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		tablaClientes = new JTable(modelClientes);
		tablaClientes.getTableHeader().setReorderingAllowed(false);
		spClientes.setViewportView(tablaClientes);

		JLabel lblNombre = new JLabel("Nombre:");
		lblNombre.setHorizontalAlignment(SwingConstants.CENTER);
		lblNombre.setBounds(10, 15, 70, 30);
		panelClientes.add(lblNombre);

		JLabel lblApellido = new JLabel("Apellido:");
		lblApellido.setHorizontalAlignment(SwingConstants.CENTER);
		lblApellido.setBounds(215, 15, 70, 30);
		panelClientes.add(lblApellido);

		JLabel lblDNI = new JLabel("DNI:");
		lblDNI.setHorizontalAlignment(SwingConstants.CENTER);
		lblDNI.setBounds(420, 15, 70, 30);
		panelClientes.add(lblDNI);

		JLabel lblUsuario = new JLabel("Usuario:");
		lblUsuario.setHorizontalAlignment(SwingConstants.CENTER);
		lblUsuario.setBounds(625, 15, 70, 30);
		panelClientes.add(lblUsuario);

		txtNombre = new JTextField();
		txtNombre.setBounds(90, 15, 115, 30);
		panelClientes.add(txtNombre);
		txtNombre.setColumns(10);

		txtApellido = new JTextField();
		txtApellido.setBounds(295, 15, 115, 30);
		panelClientes.add(txtApellido);
		txtApellido.setColumns(10);

		txtDNI = new JTextField();
		txtDNI.setBounds(500, 15, 115, 30);
		panelClientes.add(txtDNI);
		txtDNI.setColumns(10);

		txtUsuario = new JTextField();
		txtUsuario.setBounds(705, 15, 115, 30);
		panelClientes.add(txtUsuario);
		txtUsuario.setColumns(10);

		btnSeleccionarCliente = new JButton("Seleccionar cliente");
		btnSeleccionarCliente.setBounds(724, 415, 153, 30);
		panelClientes.add(btnSeleccionarCliente);

		btnBuscarCliente = new JButton("Buscar cliente");
		btnBuscarCliente.setBounds(724, 59, 153, 30);
		panelClientes.add(btnBuscarCliente);
		modelBuscarAcompañante = new DefaultTableModel(null, columnasDeAcompañantes) {
			private static final long serialVersionUID = 1L;

			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};

		/*************************************
		 * PANEL DE BUSQUEDA DE VIAJES
		 *************************************************/

		panelViajes = new JPanel();
		panelViajes.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		panelViajes.setBounds(-887, 83, 887, 456);
		panel.add(panelViajes);
		panelViajes.setLayout(null);

		JScrollPane spViajes = new JScrollPane();
		spViajes.setBounds(10, 197, 867, 199);
		panelViajes.add(spViajes);
		modelViajes = new DefaultTableModel(null, columnasDeViajes) {
			private static final long serialVersionUID = 1L;

			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		tablaViajes = new JTable(modelViajes);
		tablaViajes.getTableHeader().setReorderingAllowed(false);
		tablaViajes.removeColumn(tablaViajes.getColumnModel().getColumn(0));
		spViajes.setViewportView(tablaViajes);

		JLabel lblRangoDeFechas = new JLabel("Rango de Fecha");
		lblRangoDeFechas.setBounds(275, 107, 125, 30);
		panelViajes.add(lblRangoDeFechas);

		JLabel lblDesde = new JLabel("Desde");
		lblDesde.setHorizontalAlignment(SwingConstants.CENTER);
		lblDesde.setBounds(275, 142, 45, 30);
		panelViajes.add(lblDesde);

		JLabel lblHasta = new JLabel("hasta");
		lblHasta.setHorizontalAlignment(SwingConstants.CENTER);
		lblHasta.setBounds(455, 142, 45, 30);
		panelViajes.add(lblHasta);

		JLabel lblCiudad = new JLabel("Ciudad:");
		lblCiudad.setBounds(10, 51, 70, 30);
		panelViajes.add(lblCiudad);

		JLabel lblProvincia = new JLabel("Provincia:");
		lblProvincia.setBounds(10, 86, 70, 30);
		panelViajes.add(lblProvincia);

		JLabel lblPais = new JLabel("Pais:");
		lblPais.setBounds(10, 121, 70, 30);
		panelViajes.add(lblPais);

		JLabel lblContinente = new JLabel("Continente:");
		lblContinente.setBounds(10, 156, 70, 30);
		panelViajes.add(lblContinente);

		JLabel lblRangoDePrecio = new JLabel("Rango de Precio");
		lblRangoDePrecio.setBounds(275, 19, 96, 25);
		panelViajes.add(lblRangoDePrecio);

		JLabel lblGuion = new JLabel("--");
		lblGuion.setHorizontalAlignment(SwingConstants.CENTER);
		lblGuion.setBounds(380, 51, 25, 30);
		panelViajes.add(lblGuion);

		txtFechaDesde = new JDateChooser();
		txtFechaDesde.setMinSelectableDate(new Date());
		JTextFieldDateEditor editorMinimo = (JTextFieldDateEditor) txtFechaDesde.getDateEditor();
		editorMinimo.setEditable(false);
		txtFechaDesde.setBounds(325, 142, 125, 30);
		txtFechaDesde.setDate(new Date());
		panelViajes.add(txtFechaDesde);

		txtFechaHasta = new JDateChooser();
		txtFechaHasta.setMinSelectableDate(new Date());
		txtFechaHasta.setBounds(505, 142, 125, 30);
		panelViajes.add(txtFechaHasta);

		txtCiudad = new JTextField();
		txtCiudad.setBounds(90, 51, 153, 30);
		panelViajes.add(txtCiudad);
		txtCiudad.setColumns(10);

		txtProvincia = new JTextField();
		txtProvincia.setBounds(90, 86, 153, 30);
		panelViajes.add(txtProvincia);
		txtProvincia.setColumns(10);

		cbPais = new JComboBox<String>();
		cbPais.setBounds(90, 121, 153, 30);
		panelViajes.add(cbPais);

		cbContinente = new JComboBox<String>();
		cbContinente.setBounds(90, 156, 153, 30);
		panelViajes.add(cbContinente);

		txtPrecioMaximo = new JTextField();
		txtPrecioMaximo.setColumns(10);
		txtPrecioMaximo.setBounds(410, 51, 100, 30);
		panelViajes.add(txtPrecioMaximo);

		txtPrecioMinimo = new JTextField();
		txtPrecioMinimo.setColumns(10);
		txtPrecioMinimo.setBounds(275, 51, 100, 30);
		panelViajes.add(txtPrecioMinimo);

		btnBuscarViaje = new JButton("Buscar viaje");
		btnBuscarViaje.setBounds(724, 161, 153, 25);
		panelViajes.add(btnBuscarViaje);

		btnSeleccionarViaje = new JButton("Seleccionar viaje");
		btnSeleccionarViaje.setBounds(724, 407, 153, 30);
		panelViajes.add(btnSeleccionarViaje);

		JLabel lblFiltrarPor = new JLabel("Filtrar por:");
		lblFiltrarPor.setBounds(22, 13, 96, 25);
		panelViajes.add(lblFiltrarPor);

		panelAgregarAcompañante = new JPanel();
		panelAgregarAcompañante.setBounds(887, 213, 421, 371);
		panel.add(panelAgregarAcompañante);
		panelAgregarAcompañante.setEnabled(false);
		panelAgregarAcompañante.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		GridBagLayout gbl_panelAcompañante = new GridBagLayout();
		gbl_panelAcompañante.columnWidths = new int[] { 78, 50, 138, 0 };
		gbl_panelAcompañante.rowHeights = new int[] { 30, 30, 30, 30, 30, 30, 30, 30, 30, 23, 0 };
		gbl_panelAcompañante.columnWeights = new double[] { 1.0, 0.0, 1.0, Double.MIN_VALUE };
		gbl_panelAcompañante.rowWeights = new double[] { 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0,
				Double.MIN_VALUE };
		panelAgregarAcompañante.setLayout(gbl_panelAcompañante);

		JLabel lblAcompañanteCuadro = new JLabel("Agregar acompañante");
		lblAcompañanteCuadro.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_lblAcompañanteCuadro = new GridBagConstraints();
		gbc_lblAcompañanteCuadro.fill = GridBagConstraints.BOTH;
		gbc_lblAcompañanteCuadro.insets = new Insets(5, 5, 5, 0);
		gbc_lblAcompañanteCuadro.gridx = 0;
		gbc_lblAcompañanteCuadro.gridy = 0;
		panelAgregarAcompañante.add(lblAcompañanteCuadro, gbc_lblAcompañanteCuadro);

		JLabel lblDNIBuscarAcompañante = new JLabel("DNI:");
		GridBagConstraints gbc_lblDNIBuscarAcompañante = new GridBagConstraints();
		gbc_lblDNIBuscarAcompañante.fill = GridBagConstraints.BOTH;
		gbc_lblDNIBuscarAcompañante.insets = new Insets(0, 0, 5, 5);
		gbc_lblDNIBuscarAcompañante.gridx = 0;
		gbc_lblDNIBuscarAcompañante.gridy = 1;
		panelAgregarAcompañante.add(lblDNIBuscarAcompañante, gbc_lblDNIBuscarAcompañante);
		lblDNIBuscarAcompañante.setHorizontalAlignment(SwingConstants.CENTER);

		txtDNIBuscarAcompañante = new JTextField();
		GridBagConstraints gbc_txtDNIBuscarAcompañante = new GridBagConstraints();
		gbc_txtDNIBuscarAcompañante.gridwidth = 2;
		gbc_txtDNIBuscarAcompañante.fill = GridBagConstraints.BOTH;
		gbc_txtDNIBuscarAcompañante.insets = new Insets(0, 0, 5, 0);
		gbc_txtDNIBuscarAcompañante.gridx = 1;
		gbc_txtDNIBuscarAcompañante.gridy = 1;
		panelAgregarAcompañante.add(txtDNIBuscarAcompañante, gbc_txtDNIBuscarAcompañante);
		txtDNIBuscarAcompañante.setColumns(10);

		btnBuscarAcompañante = new JButton("Buscar acompañante");
		GridBagConstraints gbc_btnBuscarAcompañante = new GridBagConstraints();
		gbc_btnBuscarAcompañante.fill = GridBagConstraints.BOTH;
		gbc_btnBuscarAcompañante.insets = new Insets(0, 0, 5, 0);
		gbc_btnBuscarAcompañante.gridx = 2;
		gbc_btnBuscarAcompañante.gridy = 2;
		panelAgregarAcompañante.add(btnBuscarAcompañante, gbc_btnBuscarAcompañante);

		JLabel lblNombreAcompañante = new JLabel("Nombre:");
		lblNombreAcompañante.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_lblNombreAcompañante = new GridBagConstraints();
		gbc_lblNombreAcompañante.fill = GridBagConstraints.BOTH;
		gbc_lblNombreAcompañante.insets = new Insets(0, 5, 5, 5);
		gbc_lblNombreAcompañante.gridx = 0;
		gbc_lblNombreAcompañante.gridy = 3;
		panelAgregarAcompañante.add(lblNombreAcompañante, gbc_lblNombreAcompañante);

		txtNombreAcompañante = new JTextField();
		GridBagConstraints gbc_txtNombreAcompañante = new GridBagConstraints();
		gbc_txtNombreAcompañante.gridwidth = 2;
		gbc_txtNombreAcompañante.fill = GridBagConstraints.BOTH;
		gbc_txtNombreAcompañante.insets = new Insets(0, 0, 5, 0);
		gbc_txtNombreAcompañante.gridx = 1;
		gbc_txtNombreAcompañante.gridy = 3;
		panelAgregarAcompañante.add(txtNombreAcompañante, gbc_txtNombreAcompañante);
		txtNombreAcompañante.setColumns(10);

		JLabel lblApellidoAcompañante = new JLabel("Apellido:");
		lblApellidoAcompañante.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_lblApellidoAcompañante = new GridBagConstraints();
		gbc_lblApellidoAcompañante.fill = GridBagConstraints.BOTH;
		gbc_lblApellidoAcompañante.insets = new Insets(0, 5, 5, 5);
		gbc_lblApellidoAcompañante.gridx = 0;
		gbc_lblApellidoAcompañante.gridy = 4;
		panelAgregarAcompañante.add(lblApellidoAcompañante, gbc_lblApellidoAcompañante);

		txtApellidoAcompañante = new JTextField();
		txtApellidoAcompañante.setColumns(10);
		GridBagConstraints gbc_txtApellidoAcompañante = new GridBagConstraints();
		gbc_txtApellidoAcompañante.gridwidth = 2;
		gbc_txtApellidoAcompañante.fill = GridBagConstraints.BOTH;
		gbc_txtApellidoAcompañante.insets = new Insets(0, 0, 5, 0);
		gbc_txtApellidoAcompañante.gridx = 1;
		gbc_txtApellidoAcompañante.gridy = 4;
		panelAgregarAcompañante.add(txtApellidoAcompañante, gbc_txtApellidoAcompañante);

		JLabel lblDniAcompañante = new JLabel("DNI:");
		lblDniAcompañante.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_lblDniAcompañante = new GridBagConstraints();
		gbc_lblDniAcompañante.fill = GridBagConstraints.BOTH;
		gbc_lblDniAcompañante.insets = new Insets(0, 5, 5, 5);
		gbc_lblDniAcompañante.gridx = 0;
		gbc_lblDniAcompañante.gridy = 5;
		panelAgregarAcompañante.add(lblDniAcompañante, gbc_lblDniAcompañante);

		txtDNIAcompañante = new JTextField();
		txtDNIAcompañante.setColumns(10);
		GridBagConstraints gbc_txtDNIAcompañante = new GridBagConstraints();
		gbc_txtDNIAcompañante.gridwidth = 2;
		gbc_txtDNIAcompañante.fill = GridBagConstraints.BOTH;
		gbc_txtDNIAcompañante.insets = new Insets(0, 0, 5, 0);
		gbc_txtDNIAcompañante.gridx = 1;
		gbc_txtDNIAcompañante.gridy = 5;
		panelAgregarAcompañante.add(txtDNIAcompañante, gbc_txtDNIAcompañante);

		JLabel lblNacimientoAcompañante = new JLabel("Nacimiento:");
		lblNacimientoAcompañante.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_lblNacimientoAcompañante = new GridBagConstraints();
		gbc_lblNacimientoAcompañante.fill = GridBagConstraints.BOTH;
		gbc_lblNacimientoAcompañante.insets = new Insets(0, 5, 5, 5);
		gbc_lblNacimientoAcompañante.gridx = 0;
		gbc_lblNacimientoAcompañante.gridy = 6;
		panelAgregarAcompañante.add(lblNacimientoAcompañante, gbc_lblNacimientoAcompañante);

		txtNacimientoAcompañante = new JDateChooser();
		txtNacimientoAcompañante.setMaxSelectableDate(new Date());
		JTextFieldDateEditor editor = (JTextFieldDateEditor) txtNacimientoAcompañante.getDateEditor();
		editor.setEditable(false);
		GridBagConstraints gbc_txtNacimientoAcompañante = new GridBagConstraints();
		gbc_txtNacimientoAcompañante.gridwidth = 2;
		gbc_txtNacimientoAcompañante.fill = GridBagConstraints.BOTH;
		gbc_txtNacimientoAcompañante.insets = new Insets(0, 0, 5, 0);
		gbc_txtNacimientoAcompañante.gridx = 1;
		gbc_txtNacimientoAcompañante.gridy = 6;
		panelAgregarAcompañante.add(txtNacimientoAcompañante, gbc_txtNacimientoAcompañante);

		JLabel lblTelefono = new JLabel("Telefono:");
		lblTelefono.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_lblTelefono = new GridBagConstraints();
		gbc_lblTelefono.fill = GridBagConstraints.BOTH;
		gbc_lblTelefono.insets = new Insets(0, 5, 5, 5);
		gbc_lblTelefono.gridx = 0;
		gbc_lblTelefono.gridy = 7;
		panelAgregarAcompañante.add(lblTelefono, gbc_lblTelefono);

		txtTelefonoAcompañante = new JTextField();
		txtTelefonoAcompañante.setColumns(10);
		GridBagConstraints gbc_txtTelefonoAcompañante = new GridBagConstraints();
		gbc_txtTelefonoAcompañante.gridwidth = 2;
		gbc_txtTelefonoAcompañante.fill = GridBagConstraints.BOTH;
		gbc_txtTelefonoAcompañante.insets = new Insets(0, 0, 5, 0);
		gbc_txtTelefonoAcompañante.gridx = 1;
		gbc_txtTelefonoAcompañante.gridy = 7;
		panelAgregarAcompañante.add(txtTelefonoAcompañante, gbc_txtTelefonoAcompañante);

		JLabel lblEmail = new JLabel("Email:");
		lblEmail.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_lblEmail = new GridBagConstraints();
		gbc_lblEmail.fill = GridBagConstraints.BOTH;
		gbc_lblEmail.insets = new Insets(0, 5, 5, 5);
		gbc_lblEmail.gridx = 0;
		gbc_lblEmail.gridy = 8;
		panelAgregarAcompañante.add(lblEmail, gbc_lblEmail);

		txtEmailAcompañante = new JTextField();
		txtEmailAcompañante.setColumns(10);
		GridBagConstraints gbc_txtEmailAcompañante = new GridBagConstraints();
		gbc_txtEmailAcompañante.gridwidth = 2;
		gbc_txtEmailAcompañante.fill = GridBagConstraints.BOTH;
		gbc_txtEmailAcompañante.insets = new Insets(0, 0, 5, 0);
		gbc_txtEmailAcompañante.gridx = 1;
		gbc_txtEmailAcompañante.gridy = 8;
		panelAgregarAcompañante.add(txtEmailAcompañante, gbc_txtEmailAcompañante);

		btnLimpiarCampos = new JButton("Limpiar campos");
		GridBagConstraints gbc_btnLimpiarCampos = new GridBagConstraints();
		gbc_btnLimpiarCampos.fill = GridBagConstraints.BOTH;
		gbc_btnLimpiarCampos.insets = new Insets(0, 5, 0, 5);
		gbc_btnLimpiarCampos.gridx = 0;
		gbc_btnLimpiarCampos.gridy = 9;
		panelAgregarAcompañante.add(btnLimpiarCampos, gbc_btnLimpiarCampos);

		btnAgregarAcompañante = new JButton("Agregar acompañante");
		GridBagConstraints gbc_btnAgregarAcompañante = new GridBagConstraints();
		gbc_btnAgregarAcompañante.fill = GridBagConstraints.BOTH;
		gbc_btnAgregarAcompañante.gridx = 2;
		gbc_btnAgregarAcompañante.gridy = 9;
		panelAgregarAcompañante.add(btnAgregarAcompañante, gbc_btnAgregarAcompañante);

		panelTarjeta = new JPanel();
		panelTarjeta.setBounds(71, 158, 335, 178);
		panel.add(panelTarjeta);
		GridBagLayout gbl_panelTarjeta = new GridBagLayout();
		gbl_panelTarjeta.columnWidths = new int[] { 50, 50, 50, 0 };
		gbl_panelTarjeta.rowHeights = new int[] { 30, 30, 30, 30, 0 };
		gbl_panelTarjeta.columnWeights = new double[] { 1.0, 1.0, 1.0, Double.MIN_VALUE };
		gbl_panelTarjeta.rowWeights = new double[] { 1.0, 1.0, 1.0, 1.0, Double.MIN_VALUE };
		panelTarjeta.setLayout(gbl_panelTarjeta);

		JLabel lblTipo = new JLabel("Tipo:");
		GridBagConstraints gbc_lblTipo = new GridBagConstraints();
		gbc_lblTipo.fill = GridBagConstraints.BOTH;
		gbc_lblTipo.insets = new Insets(0, 0, 5, 5);
		gbc_lblTipo.gridx = 0;
		gbc_lblTipo.gridy = 0;
		panelTarjeta.add(lblTipo, gbc_lblTipo);
		lblTipo.setHorizontalAlignment(SwingConstants.CENTER);

		comboBoxTipo = new JComboBox<String>();
		GridBagConstraints gbc_comboBoxTipo = new GridBagConstraints();
		gbc_comboBoxTipo.fill = GridBagConstraints.BOTH;
		gbc_comboBoxTipo.gridwidth = 2;
		gbc_comboBoxTipo.insets = new Insets(0, 0, 5, 0);
		gbc_comboBoxTipo.gridx = 1;
		gbc_comboBoxTipo.gridy = 0;
		panelTarjeta.add(comboBoxTipo, gbc_comboBoxTipo);
		comboBoxTipo.addItem("Crédito");
		comboBoxTipo.addItem("Débito");

		JLabel lblMarca = new JLabel("Marca:");
		GridBagConstraints gbc_lblMarca = new GridBagConstraints();
		gbc_lblMarca.fill = GridBagConstraints.BOTH;
		gbc_lblMarca.insets = new Insets(0, 0, 5, 5);
		gbc_lblMarca.gridx = 0;
		gbc_lblMarca.gridy = 1;
		panelTarjeta.add(lblMarca, gbc_lblMarca);
		lblMarca.setHorizontalAlignment(SwingConstants.CENTER);

		comboBoxEmpresa = new JComboBox<String>();
		GridBagConstraints gbc_comboBoxEmpresa = new GridBagConstraints();
		gbc_comboBoxEmpresa.gridwidth = 2;
		gbc_comboBoxEmpresa.fill = GridBagConstraints.BOTH;
		gbc_comboBoxEmpresa.insets = new Insets(0, 0, 5, 0);
		gbc_comboBoxEmpresa.gridx = 1;
		gbc_comboBoxEmpresa.gridy = 1;
		panelTarjeta.add(comboBoxEmpresa, gbc_comboBoxEmpresa);
		comboBoxEmpresa.addItem("Visa");
		comboBoxEmpresa.addItem("AmericanExpress");
		comboBoxEmpresa.addItem("MasterCard");

		JLabel lblNumero = new JLabel("Número:");
		GridBagConstraints gbc_lblNumero = new GridBagConstraints();
		gbc_lblNumero.fill = GridBagConstraints.BOTH;
		gbc_lblNumero.insets = new Insets(0, 0, 5, 5);
		gbc_lblNumero.gridx = 0;
		gbc_lblNumero.gridy = 2;
		panelTarjeta.add(lblNumero, gbc_lblNumero);
		lblNumero.setHorizontalAlignment(SwingConstants.CENTER);

		txtNumero = new JTextField();
		GridBagConstraints gbc_txtNumero = new GridBagConstraints();
		gbc_txtNumero.fill = GridBagConstraints.BOTH;
		gbc_txtNumero.insets = new Insets(0, 0, 5, 0);
		gbc_txtNumero.gridwidth = 2;
		gbc_txtNumero.gridx = 1;
		gbc_txtNumero.gridy = 2;
		panelTarjeta.add(txtNumero, gbc_txtNumero);
		txtNumero.setColumns(10);

		JLabel lblVto = new JLabel("Vencimiento:");
		GridBagConstraints gbc_lblVto = new GridBagConstraints();
		gbc_lblVto.fill = GridBagConstraints.BOTH;
		gbc_lblVto.insets = new Insets(0, 0, 0, 5);
		gbc_lblVto.gridx = 0;
		gbc_lblVto.gridy = 3;
		panelTarjeta.add(lblVto, gbc_lblVto);
		lblVto.setHorizontalAlignment(SwingConstants.CENTER);

		mesTarjeta = new JSpinner();
		GridBagConstraints gbc_mesTarjeta = new GridBagConstraints();
		gbc_mesTarjeta.fill = GridBagConstraints.BOTH;
		gbc_mesTarjeta.insets = new Insets(0, 0, 0, 5);
		gbc_mesTarjeta.gridx = 1;
		gbc_mesTarjeta.gridy = 3;
		panelTarjeta.add(mesTarjeta, gbc_mesTarjeta);
		mesTarjeta.setModel(new SpinnerNumberModel(LocalDate.now().getMonthValue(), 1, 12, 1));

		añoTarjeta = new JYearChooser();
		GridBagConstraints gbc_añoTarjeta = new GridBagConstraints();
		gbc_añoTarjeta.fill = GridBagConstraints.BOTH;
		gbc_añoTarjeta.gridx = 2;
		gbc_añoTarjeta.gridy = 3;
		panelTarjeta.add(añoTarjeta, gbc_añoTarjeta);

		panelPuntos = new JPanel();
		panelPuntos.setBounds(71, 158, 335, 178);
		panel.add(panelPuntos);
		GridBagLayout gbl_panelPuntos = new GridBagLayout();
		gbl_panelPuntos.columnWidths = new int[] { 126, 50 };
		gbl_panelPuntos.rowHeights = new int[] { 30, 30, 30, 30, 0 };
		gbl_panelPuntos.columnWeights = new double[] { 0.0, 1.0 };
		gbl_panelPuntos.rowWeights = new double[] { 1.0, 1.0, 1.0, 1.0, Double.MIN_VALUE };
		panelPuntos.setLayout(gbl_panelPuntos);

		JLabel lblPuntosAcumulados = new JLabel("Puntos acumulados:");
		lblPuntosAcumulados.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_lblPuntosAcumulados = new GridBagConstraints();
		gbc_lblPuntosAcumulados.fill = GridBagConstraints.BOTH;
		gbc_lblPuntosAcumulados.insets = new Insets(0, 0, 5, 5);
		gbc_lblPuntosAcumulados.gridx = 0;
		gbc_lblPuntosAcumulados.gridy = 0;
		panelPuntos.add(lblPuntosAcumulados, gbc_lblPuntosAcumulados);

		txtPuntosAcumulados = new JTextField();
		txtPuntosAcumulados.setEditable(false);
		GridBagConstraints gbc_txtPuntosAcumulados = new GridBagConstraints();
		gbc_txtPuntosAcumulados.insets = new Insets(0, 0, 5, 0);
		gbc_txtPuntosAcumulados.fill = GridBagConstraints.BOTH;
		gbc_txtPuntosAcumulados.gridx = 1;
		gbc_txtPuntosAcumulados.gridy = 0;
		panelPuntos.add(txtPuntosAcumulados, gbc_txtPuntosAcumulados);
		txtPuntosAcumulados.setColumns(10);

		JLabel lblPuntosAUtilizar = new JLabel("Puntos a utilizar:");
		lblPuntosAUtilizar.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_lblPuntosAUtilizar = new GridBagConstraints();
		gbc_lblPuntosAUtilizar.fill = GridBagConstraints.BOTH;
		gbc_lblPuntosAUtilizar.insets = new Insets(0, 0, 5, 5);
		gbc_lblPuntosAUtilizar.gridx = 0;
		gbc_lblPuntosAUtilizar.gridy = 1;
		panelPuntos.add(lblPuntosAUtilizar, gbc_lblPuntosAUtilizar);

		txtPuntosAUtilizar = new JTextField();
		GridBagConstraints gbc_txtPuntosAUtilizar = new GridBagConstraints();
		gbc_txtPuntosAUtilizar.insets = new Insets(0, 0, 5, 0);
		gbc_txtPuntosAUtilizar.fill = GridBagConstraints.BOTH;
		gbc_txtPuntosAUtilizar.gridx = 1;
		gbc_txtPuntosAUtilizar.gridy = 1;
		panelPuntos.add(txtPuntosAUtilizar, gbc_txtPuntosAUtilizar);
		txtPuntosAUtilizar.setColumns(10);

		JLabel lblCliente = new JLabel("Cliente:");
		lblCliente.setHorizontalAlignment(SwingConstants.CENTER);
		lblCliente.setBounds(198, 11, 60, 30);
		panel.add(lblCliente);

		JLabel lblViaje = new JLabel("Viaje:");
		lblViaje.setHorizontalAlignment(SwingConstants.CENTER);
		lblViaje.setBounds(198, 46, 60, 30);
		panel.add(lblViaje);

		JLabel lblFormaDePago = new JLabel("Forma de pago");
		lblFormaDePago.setHorizontalAlignment(SwingConstants.CENTER);
		lblFormaDePago.setBounds(176, 87, 105, 30);
		panel.add(lblFormaDePago);

		JLabel lblPrecio = new JLabel("Precio del viaje:");
		lblPrecio.setHorizontalAlignment(SwingConstants.CENTER);
		lblPrecio.setBounds(71, 347, 130, 30);
		panel.add(lblPrecio);

		JLabel lblMonto = new JLabel("Monto a pagar:");
		lblMonto.setHorizontalAlignment(SwingConstants.CENTER);
		lblMonto.setBounds(71, 488, 130, 30);
		panel.add(lblMonto);

		txtCliente = new JTextField();
		txtCliente.setEditable(false);
		txtCliente.setBounds(268, 11, 118, 30);
		panel.add(txtCliente);

		txtViaje = new JTextField();
		txtViaje.setEditable(false);
		txtViaje.setBounds(268, 46, 118, 30);
		panel.add(txtViaje);

		radioEfectivo = new JRadioButton("Efectivo");
		radioEfectivo.setSelected(true);
		radioEfectivo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (radioEfectivo.isSelected()) {
					setPagoEfectivo();
				}
			}
		});
		radioEfectivo.setHorizontalAlignment(SwingConstants.CENTER);
		radioEfectivo.setBounds(61, 126, 105, 25);
		panel.add(radioEfectivo);

		radioTarjeta = new JRadioButton("Tarjeta");
		radioTarjeta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (radioTarjeta.isSelected()) {
					setPagoTarjeta();
				}
			}
		});
		radioTarjeta.setHorizontalAlignment(SwingConstants.CENTER);
		radioTarjeta.setBounds(176, 126, 105, 25);
		panel.add(radioTarjeta);

		radioPuntos = new JRadioButton("Puntos");
		radioPuntos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (radioPuntos.isSelected()) {
					setPagoPuntos();
				}
			}
		});
		radioPuntos.setHorizontalAlignment(SwingConstants.CENTER);
		radioPuntos.setBounds(291, 126, 105, 25);
		panel.add(radioPuntos);

		grupoFormaPago = new ButtonGroup();
		grupoFormaPago.add(radioEfectivo);
		grupoFormaPago.add(radioTarjeta);
		grupoFormaPago.add(radioPuntos);

		txtPrecioViaje = new JTextField();
		txtPrecioViaje.setEditable(false);
		txtPrecioViaje.setColumns(10);
		txtPrecioViaje.setBounds(211, 347, 175, 30);
		panel.add(txtPrecioViaje);

		txtMonto = new JTextField();
		txtMonto.setColumns(10);
		txtMonto.setBounds(211, 488, 175, 30);
		panel.add(txtMonto);

		txtFechaReserva = new JDateChooser();
		JTextFieldDateEditor editorMin = (JTextFieldDateEditor) txtFechaReserva.getDateEditor();
		editorMin.setEditable(false);
		Date fechaReserva = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(fechaReserva);
		c.add(Calendar.DATE, 7);
		Date fechaReservaMas7 = c.getTime();
		txtFechaReserva.setMinSelectableDate(fechaReservaMas7);
		txtFechaReserva.setDate(fechaReservaMas7);
		txtFechaReserva.setBounds(211, 523, 175, 30);
		panel.add(txtFechaReserva);

		btnConfirmar = new JButton("Confirmar");
		btnConfirmar.setBounds(260, 594, 126, 30);
		panel.add(btnConfirmar);

		btnPanelCliente = new JButton("Seleccionar cliente");
		btnPanelCliente.setBounds(32, 11, 156, 30);
		panel.add(btnPanelCliente);

		btnPanelViaje = new JButton("Seleccionar viaje");
		btnPanelViaje.setBounds(32, 46, 156, 30);
		panel.add(btnPanelViaje);

		JLabel lblAcompañante = new JLabel("Acompañantes");
		lblAcompañante.setBounds(463, 6, 105, 30);
		panel.add(lblAcompañante);

		JScrollPane spAcompañantesViaje = new JScrollPane();
		spAcompañantesViaje.setBounds(463, 47, 414, 114);
		panel.add(spAcompañantesViaje);
		modelAcompañantes = new DefaultTableModel(null, columnasDeAcompañantes) {
			private static final long serialVersionUID = 1L;

			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		tablaAcompañantes = new JTable(modelAcompañantes);
		tablaAcompañantes.getTableHeader().setReorderingAllowed(false);
		spAcompañantesViaje.setViewportView(tablaAcompañantes);

		btnPanelAgregarAcompañante = new JButton("Agregar acompañante");
		btnPanelAgregarAcompañante.setBounds(721, 172, 156, 30);
		panel.add(btnPanelAgregarAcompañante);

		JLabel lblPrecioAcompañante = new JLabel("Precio con acompañante(s):");
		lblPrecioAcompañante.setHorizontalAlignment(SwingConstants.CENTER);
		lblPrecioAcompañante.setBounds(45, 453, 156, 30);
		panel.add(lblPrecioAcompañante);

		txtPrecioAcompañante = new JTextField();
		txtPrecioAcompañante.setText("0");
		txtPrecioAcompañante.setEditable(false);
		txtPrecioAcompañante.setColumns(10);
		txtPrecioAcompañante.setBounds(211, 453, 175, 30);
		panel.add(txtPrecioAcompañante);

		JLabel lblVencimientoDeReserva = new JLabel("Vto de reserva:");
		lblVencimientoDeReserva.setHorizontalAlignment(SwingConstants.CENTER);
		lblVencimientoDeReserva.setBounds(71, 523, 130, 30);
		panel.add(lblVencimientoDeReserva);

		JLabel lblDescuento = new JLabel("Descuento (%):");
		lblDescuento.setHorizontalAlignment(SwingConstants.CENTER);
		lblDescuento.setBounds(71, 382, 130, 30);
		panel.add(lblDescuento);

		txtDescuento = new JTextField();
		txtDescuento.setEditable(false);
		txtDescuento.setColumns(10);
		txtDescuento.setBounds(211, 382, 175, 30);
		panel.add(txtDescuento);

		JLabel lblPrecioConDescuento = new JLabel("Precio con descuento:");
		lblPrecioConDescuento.setHorizontalAlignment(SwingConstants.CENTER);
		lblPrecioConDescuento.setBounds(71, 417, 130, 30);
		panel.add(lblPrecioConDescuento);

		txtPrecioDescuento = new JTextField();
		txtPrecioDescuento.setEditable(false);
		txtPrecioDescuento.setColumns(10);
		txtPrecioDescuento.setBounds(211, 417, 175, 30);
		panel.add(txtPrecioDescuento);

		setPagoEfectivo();
		txtMonto.setText("0");

		btnQuitarAcompañante = new JButton("Quitar acompañante");
		btnQuitarAcompañante.setBounds(721, 11, 156, 27);
		panel.add(btnQuitarAcompañante);

		this.frmAgregarPasaje.setVisible(false);
	}

	public void mostrarVentana() {
		this.frmAgregarPasaje.setVisible(true);
	}

	public JPanel getPanel() {
		return panel;
	}

	public JTextField getCliente() {
		return txtCliente;
	}

	public JTextField getViaje() {
		return txtViaje;
	}

	public String getTipoTarjeta() {
		return comboBoxTipo.getSelectedItem().toString();
	}

	public String getEmpresaTarjeta() {
		return comboBoxEmpresa.getSelectedItem().toString();
	}

	public JTextField getNumeroTarjeta() {
		return txtNumero;
	}

	public LocalDate getVtoTarjeta() {
		LocalDate vtoTarjeta = null;
		String mes = mesTarjeta.getValue().toString();
		String año = Integer.toString(añoTarjeta.getValue());
		String fecha = año + "/" + mes;
		SimpleDateFormat formato = new SimpleDateFormat("yyyy/MM");
		try {
			Date vto = formato.parse(fecha);
			vtoTarjeta = vto.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return vtoTarjeta;
	}

	public JTextField getPrecioViaje() {
		return txtPrecioViaje;
	}

	public JTextField getPrecioAcompañantes() {
		return txtPrecioAcompañante;
	}

	public JTextField getMonto() {
		return txtMonto;
	}

	public JComboBox<String> getComboBoxEmpresaTarjeta() {
		return comboBoxEmpresa;
	}

	public JComboBox<String> getComboBoxTipoTarjeta() {
		return comboBoxTipo;
	}

	public JRadioButton getBtnEfectivo() {
		return radioEfectivo;
	}

	public JRadioButton getBtnTarjeta() {
		return radioTarjeta;
	}

	public JRadioButton getBtnPunto() {
		return radioPuntos;
	}

	public JButton getBtnConfirmar() {
		return btnConfirmar;
	}

	public void cerrar() {
		limpiarCamposVentanaPasaje();
		this.frmAgregarPasaje.dispose();
	}

	public void limpiarCamposVentanaPasaje() {
		this.txtPrecioViaje.setText(null);
		this.txtCliente.setText(null);
		this.txtViaje.setText(null);
		this.txtNumero.setText(null);
		this.txtMonto.setText("0");
		this.txtDescuento.setText("0");
		this.txtPrecioDescuento.setText("0");
		this.txtPrecioAcompañante.setText("0");
		this.comboBoxEmpresa.setSelectedIndex(-1);
		this.comboBoxTipo.setSelectedIndex(-1);
		this.añoTarjeta.setYear(LocalDate.now().getYear());
		this.mesTarjeta.setValue(LocalDate.now().getMonthValue());
	}

	private boolean camposVacios() {
		if (txtMonto.getText().isEmpty() || txtCliente.getText().isEmpty() || txtViaje.getText().isEmpty()) {
			JOptionPane.showMessageDialog(null, "No pueden quedar campos vacíos.");
			return true;
		}
		if (radioTarjeta.isSelected()) {
			if (txtNumero.getText().isEmpty() || mesTarjeta.getValue() == null || añoTarjeta == null
					|| comboBoxTipo.getSelectedIndex() == -1 || comboBoxEmpresa.getSelectedIndex() == -1) {
				JOptionPane.showMessageDialog(null, "No pueden quedar campos vacíos.");
				return true;
			}
		}
		if (radioPuntos.isSelected()) {
			if (txtPuntosAUtilizar.getText().isEmpty()) {
				JOptionPane.showMessageDialog(null, "No pueden quedar campos vacíos.");
				return true;
			}
		}
		/*
		 * if (checkExtenderReserva.isSelected()){ if (txtFechaReserva.getDate() ==
		 * null){ JOptionPane.showMessageDialog(null,
		 * "No pueden quedar campos vacíos."); return true; } }
		 */
		return false;
	}

	private boolean camposCorrectos() {
		ValidacionCampos validador = new ValidacionCampos();
		if (!validador.esMonto(txtMonto.getText())) {
			return false;
		}
		if (radioTarjeta.isSelected()) {
			if (!validador.esNumeroTarjeta(txtNumero.getText())) {
				return false;
			}
		}

		if (radioPuntos.isSelected()) {
			if (!validador.esNumerico(txtPuntosAUtilizar.getText())) {
				return false;
			}
		}

		return true;
	}

	public boolean verificarCampo() {
		if (!camposVacios() && camposCorrectos())
			return true;
		else
			return false;
	}

	private boolean camposVaciosAcompañante() {
		if (txtNombreAcompañante.getText().equals("") || txtApellidoAcompañante.getText().equals("")
				|| txtDNIAcompañante.getText().equals("") || txtNacimientoAcompañante.getDate() == null) {
			JOptionPane.showMessageDialog(null,
					"¡Los datos: nombre, apellido, dni y fecha de nacimiento, no pueden quedar vacios!", "¡Error!",
					JOptionPane.ERROR_MESSAGE);
			return true;
		}
		return false;
	}

	private boolean camposCorrectosAcompañante() {
		ValidacionCampos validador = new ValidacionCampos();
		if (!validador.esPalabra(txtNombreAcompañante.getText())
				|| !validador.esPalabra(txtApellidoAcompañante.getText())
				|| !validador.esNumerico(txtDNIAcompañante.getText())) {
			return false;
		}
		return true;
	}

	public boolean verificarCampoAcompañante() {
		if (camposVaciosAcompañante() || !camposCorrectosAcompañante()) {
			return false;
		} else {
			return true;
		}
	}

	private void setPagoEfectivo() {
		panelTarjeta.setVisible(false);
		panelPuntos.setVisible(false);
		txtMonto.setEditable(true);
		txtMonto.setText("");
	}

	private void setPagoTarjeta() {
		panelTarjeta.setVisible(true);
		panelPuntos.setVisible(false);
		txtMonto.setEditable(true);
		txtMonto.setText("");
	}

	private void setPagoPuntos() {
		panelTarjeta.setVisible(false);
		panelPuntos.setVisible(true);
		txtMonto.setEditable(false);
		txtMonto.setText("");
	}

	public JTable getTablaClientes() {
		return tablaClientes;
	}

	public JButton getBtnPanelViajes() {
		return btnPanelViaje;
	}

	public JPanel getPanelViajes() {
		return panelViajes;
	}

	public JTable getTablaViajes() {
		return tablaViajes;
	}

	public String[] getColumnasDeViajes() {
		return columnasDeViajes;
	}

	public DefaultTableModel getModelViajes() {
		return modelViajes;
	}

	public JButton getBtnBuscarViaje() {
		return btnBuscarViaje;
	}

	public JButton getBtnSeleccionarViaje() {
		return btnSeleccionarViaje;
	}

	public JDateChooser getTxtFechaDesde() {
		return txtFechaDesde;
	}

	public JDateChooser getTxtFechaHasta() {
		return txtFechaHasta;
	}

	public JTextField getTxtCiudad() {
		return txtCiudad;
	}

	public JTextField getTxtProvincia() {
		return txtProvincia;
	}

	public JComboBox<String> getCBPais() {
		return cbPais;
	}

	public JComboBox<String> getCBContinente() {
		return cbContinente;
	}

	public JTextField getTxtPrecioMaximo() {
		return txtPrecioMaximo;
	}

	public JTextField getTxtPrecioMinimo() {
		return txtPrecioMinimo;
	}

	public JButton getBtnPanelClientes() {
		return btnPanelCliente;
	}

	public JPanel getPanelClientes() {
		return panelClientes;
	}

	public DefaultTableModel getModelClientes() {
		return modelClientes;
	}

	public String[] getColumnasDeClientes() {
		return columnasDeClientes;
	}

	public JTextField getTxtNombre() {
		return txtNombre;
	}

	public JTextField getTxtApellido() {
		return txtApellido;
	}

	public JTextField getTxtDNI() {
		return txtDNI;
	}

	public JTextField getTxtUsuario() {
		return txtUsuario;
	}

	public JButton getBtnBuscarCliente() {
		return btnBuscarCliente;
	}

	public JButton getBtnSeleccionarCliente() {
		return btnSeleccionarCliente;
	}

	public JTextField getTxtNombreAcompañante() {
		return txtNombreAcompañante;
	}

	public JTextField getTxtApellidoAcompañante() {
		return txtApellidoAcompañante;
	}

	public JTextField getTxtDNIAcompañante() {
		return txtDNIAcompañante;
	}

	public JTextField getTxtTelefonoAcompañante() {
		return txtTelefonoAcompañante;
	}

	public JTextField getTxtEmailAcompañante() {
		return txtEmailAcompañante;
	}

	public LocalDate getTxtNacimiento() {
		Date date = txtNacimientoAcompañante.getDate();
		return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
	}

	public JButton getBtnAgregarAcompañante() {
		return btnAgregarAcompañante;
	}

	public JDateChooser getNacimiento() {
		return txtNacimientoAcompañante;
	}

	public void habilitarBotonesAcompañantes(boolean bool) {
		this.btnPanelAgregarAcompañante.setEnabled(bool);
	}

	public JButton getBtnPanelAgregarAcompañante() {
		return btnPanelAgregarAcompañante;
	}

	public DefaultTableModel getModelAcompañante() {
		return modelAcompañantes;
	}

	public JTable getTablaAcompañantes() {
		return tablaAcompañantes;
	}

	public JPanel getPanelAgregarAcompañante() {
		return panelAgregarAcompañante;
	}

	public String[] getColumnasDeAcompañantes() {
		return columnasDeAcompañantes;
	}

	public DefaultTableModel getModelBuscarAcompañante() {
		return modelBuscarAcompañante;
	}

	public JButton getBtnBuscarAcompañante() {
		return btnBuscarAcompañante;
	}

	public JButton getBtnLimpiarCampos() {
		return btnLimpiarCampos;
	}
	
	public JButton getBtnQuitarAcompañante() {
		return btnQuitarAcompañante;
	}

	public JTextField getTxtDNIAcompañanteBuscar() {
		return txtDNIBuscarAcompañante;
	}

	public JDateChooser getReservaChooser() {
		return txtFechaReserva;
	}

	public LocalDate getTxtFechaReserva() {
		Date date = txtFechaReserva.getDate();
		return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
	}

	public JTextField getTxtPuntosAcumulados() {
		return txtPuntosAcumulados;
	}

	public JTextField getTxtPuntosAUtilizar() {
		return txtPuntosAUtilizar;
	}

	public JTextField getTxtDescuento() {
		return txtDescuento;
	}

	public JTextField getTxtPrecioDescuento() {
		return txtPrecioDescuento;
	}

	public JDateChooser getVToRserva() {
		return txtFechaReserva;
	}

	public JButton getQuitarAcompañante() {
		return btnQuitarAcompañante;
	}
}
