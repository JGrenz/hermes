package vistas.personaladm.pasajes;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.RowSorter;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import vistas.TextPrompt;

public class Main_Pasajes {

	private static Main_Pasajes INSTANCE;
	private JPanel frmMainVentas;
	private JTable tablaPasajes, tablaOperaciones;
	private DefaultTableModel modelPasajes, modelOperaciones;
	private String[] nombreColumnas = { "Codigo refencia", "Cliente", "DNI", "Viaje", "Ubicacion", "Fecha de viaje",
			"Abonado", "Estado", "Fecha de reserva" };
	private String[] nombreColumnasOperacion = { "Método de pago", "Fecha creación", "Monto abonado",
			"Responsable" };
	private JButton btnAgregar, btnPago, btnCancelar, btnRealizarBusqueda, btnExtenderReserva;
	private JPanel panelBusqueda, panelNorte, panelSur;
	private JRadioButton radioPagado, radioReservado, radioCancelado, radioVencido, radioTodos;
	private ButtonGroup filtro;
	private JTextField txtUsuario, txtCodigo, txtDNI, txtNombre, txtApellido;
	private JButton btnGenerarComprobante;

	public static Main_Pasajes getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new Main_Pasajes();
		}
		return INSTANCE;
	}

	public Main_Pasajes() {

		frmMainVentas = new JPanel();

		filtro = new ButtonGroup();
		GridBagLayout gbl_frmMainVentas = new GridBagLayout();
		gbl_frmMainVentas.columnWidths = new int[] { 1204, 0 };
		gbl_frmMainVentas.rowHeights = new int[] { 69, 233, 427, 0 };
		gbl_frmMainVentas.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gbl_frmMainVentas.rowWeights = new double[] { 0.0, 0.0, 1.0, Double.MIN_VALUE };
		frmMainVentas.setLayout(gbl_frmMainVentas);

		panelNorte = new JPanel();
		GridBagConstraints gbc_panelNorte = new GridBagConstraints();
		gbc_panelNorte.fill = GridBagConstraints.BOTH;
		gbc_panelNorte.insets = new Insets(0, 0, 5, 0);
		gbc_panelNorte.gridx = 0;
		gbc_panelNorte.gridy = 0;
		frmMainVentas.add(panelNorte, gbc_panelNorte);

		btnAgregar = new JButton("Crear pasaje");
		btnAgregar.setIcon(new ImageIcon(Main_Pasajes.class.getResource("/imagenes/iconos/pasajeAgregar.png")));
		panelNorte.add(btnAgregar);

		btnCancelar = new JButton("Cancelar pasaje");
		btnCancelar.setIcon(new ImageIcon(Main_Pasajes.class.getResource("/imagenes/iconos/pasajeCancelar.png")));
		panelNorte.add(btnCancelar);

		btnPago = new JButton("Realizar pago");
		btnPago.setIcon(new ImageIcon(Main_Pasajes.class.getResource("/imagenes/iconos/realizarPago.png")));
		panelNorte.add(btnPago);

		btnExtenderReserva = new JButton("Extender reserva");
		btnExtenderReserva.setIcon(new ImageIcon(Main_Pasajes.class.getResource("/imagenes/iconos/extenderReserva.png")));
		panelNorte.add(btnExtenderReserva);

		panelBusqueda = new JPanel();
		panelBusqueda.setBounds(10, 51, 667, 225);
		GridBagConstraints gbc_panelBusqueda = new GridBagConstraints();
		gbc_panelBusqueda.fill = GridBagConstraints.BOTH;
		gbc_panelBusqueda.insets = new Insets(0, 0, 5, 0);
		gbc_panelBusqueda.gridx = 0;
		gbc_panelBusqueda.gridy = 1;
		frmMainVentas.add(panelBusqueda, gbc_panelBusqueda);
		GridBagLayout gbl_panelBusqueda = new GridBagLayout();
		gbl_panelBusqueda.columnWidths = new int[] { 127, 58, 157, 130, 130, 0 };
		gbl_panelBusqueda.rowHeights = new int[] { 30, 33, 30, 30, 31, 25, 0 };
		gbl_panelBusqueda.columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		gbl_panelBusqueda.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		panelBusqueda.setLayout(gbl_panelBusqueda);

		JLabel lblFiltrarPor = new JLabel("Filtrar por:");
		lblFiltrarPor.setPreferredSize(new Dimension(0, 30));
		lblFiltrarPor.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_lblFiltrarPor = new GridBagConstraints();
		gbc_lblFiltrarPor.fill = GridBagConstraints.BOTH;
		gbc_lblFiltrarPor.insets = new Insets(0, 0, 5, 5);
		gbc_lblFiltrarPor.gridx = 0;
		gbc_lblFiltrarPor.gridy = 0;
		panelBusqueda.add(lblFiltrarPor, gbc_lblFiltrarPor);

		JLabel lblBusquedaPor = new JLabel("Búsqueda por:");
		lblBusquedaPor.setPreferredSize(new Dimension(0, 30));
		lblBusquedaPor.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_lblBusquedaPor = new GridBagConstraints();
		gbc_lblBusquedaPor.fill = GridBagConstraints.BOTH;
		gbc_lblBusquedaPor.insets = new Insets(0, 0, 5, 5);
		gbc_lblBusquedaPor.gridx = 2;
		gbc_lblBusquedaPor.gridy = 0;
		panelBusqueda.add(lblBusquedaPor, gbc_lblBusquedaPor);

		btnRealizarBusqueda = new JButton("Buscar");
		btnRealizarBusqueda.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_btnRealizarBusqueda = new GridBagConstraints();
		gbc_btnRealizarBusqueda.fill = GridBagConstraints.BOTH;
		gbc_btnRealizarBusqueda.insets = new Insets(0, 0, 5, 5);
		gbc_btnRealizarBusqueda.gridx = 3;
		gbc_btnRealizarBusqueda.gridy = 0;
		panelBusqueda.add(btnRealizarBusqueda, gbc_btnRealizarBusqueda);

		radioTodos = new JRadioButton("Todos");
		radioTodos.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_radioTodos = new GridBagConstraints();
		gbc_radioTodos.fill = GridBagConstraints.BOTH;
		gbc_radioTodos.insets = new Insets(0, 0, 5, 5);
		gbc_radioTodos.gridx = 0;
		gbc_radioTodos.gridy = 1;
		panelBusqueda.add(radioTodos, gbc_radioTodos);
		filtro.add(radioTodos);

		JLabel lblUsuario = new JLabel("Usuario");
		lblUsuario.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_radioUsuario = new GridBagConstraints();
		gbc_radioUsuario.fill = GridBagConstraints.BOTH;
		gbc_radioUsuario.insets = new Insets(0, 0, 5, 5);
		gbc_radioUsuario.gridx = 2;
		gbc_radioUsuario.gridy = 1;
		panelBusqueda.add(lblUsuario, gbc_radioUsuario);

		txtUsuario = new JTextField();
		txtUsuario.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_txtUsuario = new GridBagConstraints();
		gbc_txtUsuario.fill = GridBagConstraints.BOTH;
		gbc_txtUsuario.insets = new Insets(0, 0, 5, 0);
		gbc_txtUsuario.gridwidth = 2;
		gbc_txtUsuario.gridx = 3;
		gbc_txtUsuario.gridy = 1;
		panelBusqueda.add(txtUsuario, gbc_txtUsuario);
		txtUsuario.setColumns(10);
		TextPrompt phUsuario = new TextPrompt("Ingrese usuario...", txtUsuario);
		phUsuario.setText("Ingrese usuario...");
		phUsuario.changeAlpha(0.75f);
		phUsuario.changeStyle(Font.ITALIC);

		radioPagado = new JRadioButton("Pagado");
		radioPagado.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_radioPagado = new GridBagConstraints();
		gbc_radioPagado.fill = GridBagConstraints.BOTH;
		gbc_radioPagado.insets = new Insets(0, 0, 5, 5);
		gbc_radioPagado.gridx = 0;
		gbc_radioPagado.gridy = 2;
		panelBusqueda.add(radioPagado, gbc_radioPagado);
		filtro.add(radioPagado);

		JLabel lblCodigoReferencia = new JLabel("Codigo de referencia");
		lblCodigoReferencia.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_radioCodigoReferencia = new GridBagConstraints();
		gbc_radioCodigoReferencia.fill = GridBagConstraints.BOTH;
		gbc_radioCodigoReferencia.insets = new Insets(0, 0, 5, 5);
		gbc_radioCodigoReferencia.gridx = 2;
		gbc_radioCodigoReferencia.gridy = 2;
		panelBusqueda.add(lblCodigoReferencia, gbc_radioCodigoReferencia);

		txtCodigo = new JTextField();
		txtCodigo.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_txtCodigo = new GridBagConstraints();
		gbc_txtCodigo.fill = GridBagConstraints.BOTH;
		gbc_txtCodigo.insets = new Insets(0, 0, 5, 0);
		gbc_txtCodigo.gridwidth = 2;
		gbc_txtCodigo.gridx = 3;
		gbc_txtCodigo.gridy = 2;
		panelBusqueda.add(txtCodigo, gbc_txtCodigo);
		txtCodigo.setColumns(10);
		TextPrompt phCodigo = new TextPrompt("Ingrese codigo de referencia", txtCodigo);
		phCodigo.setText("Ingrese codigo de referencia...");
		phCodigo.changeAlpha(0.75f);
		phCodigo.changeStyle(Font.ITALIC);

		radioReservado = new JRadioButton("Reservado");
		radioReservado.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_radioReservado = new GridBagConstraints();
		gbc_radioReservado.fill = GridBagConstraints.BOTH;
		gbc_radioReservado.insets = new Insets(0, 0, 5, 5);
		gbc_radioReservado.gridx = 0;
		gbc_radioReservado.gridy = 3;
		panelBusqueda.add(radioReservado, gbc_radioReservado);
		radioReservado.setSelected(true);
		filtro.add(radioReservado);

		JLabel lblDni = new JLabel("DNI");
		lblDni.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_radioDni = new GridBagConstraints();
		gbc_radioDni.fill = GridBagConstraints.BOTH;
		gbc_radioDni.insets = new Insets(0, 0, 5, 5);
		gbc_radioDni.gridx = 2;
		gbc_radioDni.gridy = 3;
		panelBusqueda.add(lblDni, gbc_radioDni);

		txtDNI = new JTextField();
		txtDNI.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_txtDNI = new GridBagConstraints();
		gbc_txtDNI.fill = GridBagConstraints.BOTH;
		gbc_txtDNI.insets = new Insets(0, 0, 5, 0);
		gbc_txtDNI.gridwidth = 2;
		gbc_txtDNI.gridx = 3;
		gbc_txtDNI.gridy = 3;
		panelBusqueda.add(txtDNI, gbc_txtDNI);
		txtDNI.setColumns(10);
		TextPrompt phDni = new TextPrompt("Ingrese dni...", txtDNI);
		phDni.setText("Ingrese dni...");
		phDni.changeAlpha(0.75f);
		phDni.changeStyle(Font.ITALIC);

		radioCancelado = new JRadioButton("Cancelado");
		radioCancelado.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_radioCancelado = new GridBagConstraints();
		gbc_radioCancelado.fill = GridBagConstraints.BOTH;
		gbc_radioCancelado.insets = new Insets(0, 0, 5, 5);
		gbc_radioCancelado.gridx = 0;
		gbc_radioCancelado.gridy = 4;
		panelBusqueda.add(radioCancelado, gbc_radioCancelado);
		filtro.add(radioCancelado);

		JLabel lblNombreYApellido = new JLabel("Nombre y Apellido");
		lblNombreYApellido.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_radioNombreYApellido = new GridBagConstraints();
		gbc_radioNombreYApellido.fill = GridBagConstraints.BOTH;
		gbc_radioNombreYApellido.insets = new Insets(0, 0, 5, 5);
		gbc_radioNombreYApellido.gridx = 2;
		gbc_radioNombreYApellido.gridy = 4;
		panelBusqueda.add(lblNombreYApellido, gbc_radioNombreYApellido);

		txtNombre = new JTextField();
		txtNombre.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_txtNombre = new GridBagConstraints();
		gbc_txtNombre.fill = GridBagConstraints.BOTH;
		gbc_txtNombre.insets = new Insets(0, 0, 5, 5);
		gbc_txtNombre.gridx = 3;
		gbc_txtNombre.gridy = 4;
		panelBusqueda.add(txtNombre, gbc_txtNombre);
		txtNombre.setColumns(10);
		TextPrompt phNombre = new TextPrompt("Ingrese nombre...", txtNombre);
		phNombre.setText("Ingrese nombre...");
		phNombre.changeAlpha(0.75f);
		phNombre.changeStyle(Font.ITALIC);

		txtApellido = new JTextField();
		txtApellido.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_txtApellido = new GridBagConstraints();
		gbc_txtApellido.fill = GridBagConstraints.BOTH;
		gbc_txtApellido.insets = new Insets(0, 0, 5, 0);
		gbc_txtApellido.gridx = 4;
		gbc_txtApellido.gridy = 4;
		panelBusqueda.add(txtApellido, gbc_txtApellido);
		txtApellido.setColumns(10);
		TextPrompt phApellido = new TextPrompt("Ingrese apellido...", txtApellido);
		phApellido.setText("Ingrese apellido...");
		phApellido.changeAlpha(0.75f);
		phApellido.changeStyle(Font.ITALIC);

		radioVencido = new JRadioButton("Vencido");
		radioVencido.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_radioVencido = new GridBagConstraints();
		gbc_radioVencido.fill = GridBagConstraints.BOTH;
		gbc_radioVencido.insets = new Insets(0, 0, 0, 5);
		gbc_radioVencido.gridx = 0;
		gbc_radioVencido.gridy = 5;
		panelBusqueda.add(radioVencido, gbc_radioVencido);
		filtro.add(radioVencido);

		panelSur = new JPanel();
		GridBagConstraints gbc_panelSur = new GridBagConstraints();
		gbc_panelSur.fill = GridBagConstraints.BOTH;
		gbc_panelSur.gridx = 0;
		gbc_panelSur.gridy = 2;
		frmMainVentas.add(panelSur, gbc_panelSur);
		GridBagLayout gbl_panelSur = new GridBagLayout();
		gbl_panelSur.columnWidths = new int[] { 267, 0, 0 };
		gbl_panelSur.rowHeights = new int[] { 25, 120, 25, 100 };
		gbl_panelSur.columnWeights = new double[] { 0.0, 0.0, 1.0 };
		gbl_panelSur.rowWeights = new double[] { 0.0, 1.0, 0.0, 1.0 };
		panelSur.setLayout(gbl_panelSur);

		JLabel lblPasajes = new JLabel("Pasajes:");
		GridBagConstraints gbc_lblPasajes = new GridBagConstraints();
		gbc_lblPasajes.fill = GridBagConstraints.BOTH;
		gbc_lblPasajes.insets = new Insets(0, 5, 5, 5);
		gbc_lblPasajes.gridx = 0;
		gbc_lblPasajes.gridy = 0;
		panelSur.add(lblPasajes, gbc_lblPasajes);
		
		JScrollPane spVentas = new JScrollPane();
		modelPasajes = new DefaultTableModel(null, nombreColumnas) {
			private static final long serialVersionUID = 1L;

			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		GridBagConstraints gbc_spVentas = new GridBagConstraints();
		gbc_spVentas.gridwidth = 3;
		gbc_spVentas.insets = new Insets(0, 0, 5, 0);
		gbc_spVentas.fill = GridBagConstraints.BOTH;
		gbc_spVentas.gridx = 0;
		gbc_spVentas.gridy = 1;
		panelSur.add(spVentas, gbc_spVentas);
		tablaPasajes = new JTable(modelPasajes);
		RowSorter<TableModel> sorter = new TableRowSorter<TableModel>(modelPasajes);
		tablaPasajes.setRowSorter(sorter);
		tablaPasajes.getTableHeader().setReorderingAllowed(false);
		spVentas.setViewportView(tablaPasajes);

		JLabel lblOperacionesDePasaje = new JLabel("Operaciones de pasaje seleccionado:");
		GridBagConstraints gbc_lblOperacionesDePasaje = new GridBagConstraints();
		gbc_lblOperacionesDePasaje.fill = GridBagConstraints.BOTH;
		gbc_lblOperacionesDePasaje.insets = new Insets(0, 5, 5, 5);
		gbc_lblOperacionesDePasaje.gridx = 0;
		gbc_lblOperacionesDePasaje.gridy = 2;
		panelSur.add(lblOperacionesDePasaje, gbc_lblOperacionesDePasaje);
		
		JScrollPane spOperaciones = new JScrollPane();
		modelOperaciones = new DefaultTableModel(null, nombreColumnasOperacion) {
			private static final long serialVersionUID = 1L;

			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		
		btnGenerarComprobante = new JButton("Generar comprobante");
		GridBagConstraints gbc_btnGenerarComprobante = new GridBagConstraints();
		gbc_btnGenerarComprobante.fill = GridBagConstraints.BOTH;
		gbc_btnGenerarComprobante.insets = new Insets(0, 0, 5, 5);
		gbc_btnGenerarComprobante.gridx = 1;
		gbc_btnGenerarComprobante.gridy = 2;
		panelSur.add(btnGenerarComprobante, gbc_btnGenerarComprobante);
		GridBagConstraints gbc_spOperaciones = new GridBagConstraints();
		gbc_spOperaciones.gridwidth = 3;
		gbc_spOperaciones.fill = GridBagConstraints.BOTH;
		gbc_spOperaciones.gridx = 0;
		gbc_spOperaciones.gridy = 3;
		panelSur.add(spOperaciones, gbc_spOperaciones);
		tablaOperaciones = new JTable(modelOperaciones);
		tablaOperaciones.getTableHeader().setReorderingAllowed(false);
		spOperaciones.setViewportView(tablaOperaciones);

		this.frmMainVentas.setVisible(false);
	}

	public void mostrarVentana() {
		this.frmMainVentas.setVisible(true);
	}

	public JButton getBtnAgregar() {
		return btnAgregar;
	}

	public JButton getBtnCancelar() {
		return btnCancelar;
	}

	public JButton getBtnRealizarBusqueda() {
		return btnRealizarBusqueda;
	}

	public JButton getBtnPago() {
		return btnPago;
	}

	public JButton getBtnReserva() {
		return btnExtenderReserva;
	}
	
	public JButton getBtnGenerarComprobante() {
		return btnGenerarComprobante;
	}

	public DefaultTableModel getModelPasajes() {
		return modelPasajes;
	}

	public JTable getTablaPasajes() {
		return tablaPasajes;
	}

	public String[] getNombreColumnas() {
		return nombreColumnas;
	}
	
	public DefaultTableModel getModelOperaciones() {
		return modelOperaciones;
	}
	
	public JTable getTablaOperaciones() {
		return tablaOperaciones;
	}
	
	public String[] getNombreColumnasOperaciones() {
		return nombreColumnasOperacion;
	}

	public JTextField getTxtCodigo() {
		return txtCodigo;
	}

	public JTextField getTxtUsuario() {
		return txtUsuario;
	}

	public JRadioButton getRadioTodos() {
		return radioTodos;
	}

	public JRadioButton getRadioPagado() {
		return radioPagado;
	}

	public JRadioButton getRadioReservado() {
		return radioReservado;
	}

	public JRadioButton getRadioCancelado() {
		return radioCancelado;
	}

	public JRadioButton getRadioVencido() {
		return radioVencido;
	}

	public JTextField getTxtDNI() {
		return txtDNI;
	}

	public JTextField getTxtNombre() {
		return txtNombre;
	}

	public JTextField getTxtApellido() {
		return txtApellido;
	}

	public JPanel getPanel() {
		return frmMainVentas;
	}
	
}
