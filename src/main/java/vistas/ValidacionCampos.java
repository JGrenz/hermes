package vistas;

import java.math.BigDecimal;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JOptionPane;

public class ValidacionCampos {

	public ValidacionCampos() {
	}

	public boolean esDNI(String dni, String campo) {
		return false;
	}

	public boolean esMail(String email) {
		Pattern pattern = Pattern.compile("^[_a-z0-9-]+(\\.[_a-z0-9-]+)*@[a-z0-9-]+(\\.[a-z0-9-]+)*(\\.[a-z]{2,4})$");

		Matcher mather = pattern.matcher(email);

		if (mather.find() == true)
			return true;
		else
			JOptionPane.showMessageDialog(null, "El formato de mail que intenta ingresar no es válido.", "¡Error!",
					JOptionPane.ERROR_MESSAGE);
		return false;
	}

	public boolean esNumerico(String cadena) {
		for (int i = 0; i <= cadena.length() - 1; i++)
			if (!Character.isDigit(cadena.charAt(i))) {
				JOptionPane.showMessageDialog(null, "El número que intenta ingresar no es válido.", "¡Error!",
						JOptionPane.ERROR_MESSAGE);
				return false;
			}
		return true;
	}

	public boolean esMonto(String cadena) {
		try {
			@SuppressWarnings("unused")
			BigDecimal monto = new BigDecimal(cadena);
			return true;
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "El monto que intenta ingresar no es válido.", "¡Error!",
					JOptionPane.ERROR_MESSAGE);
			return false;
		}
	}

	public boolean esPalabra(String cadena) {
		for (int i = 0; i <= cadena.length() - 1; i++)
			if (!Character.isLetter(cadena.charAt(i)) && !Character.isSpaceChar(cadena.charAt(i))) {
				JOptionPane.showMessageDialog(null, "La palabra que intenta ingresar no es válida.", "¡Error!",
						JOptionPane.ERROR_MESSAGE);
				return false;
			}
		return true;
	}

	public boolean esClave(String clave) {
		if (clave.length() > 5) {
			return true;
		} else {
			JOptionPane.showMessageDialog(null,
					"La clave que intenta ingresar no es válida, debe tener al menos 6 caracteres.", "¡Error!",
					JOptionPane.ERROR_MESSAGE);
			return false;
		}
	}

	public boolean esTelefono(String cadena) {

		if (cadena.equals("")) {
			return true;
		} else if (cadena.length() >= 8) {
			if (esNumerico(cadena)) {
				return true;
			} else
				return false;
		}

		else
			JOptionPane.showMessageDialog(null, "La cantidad de digitos debe ser mayor a 8.", "¡Error!",
					JOptionPane.ERROR_MESSAGE);
		return false;
	}

	public boolean esNumeroTarjeta(String cadena) {
		if (esNumerico(cadena)) {
			if (cadena.length() != 16) {
				JOptionPane.showMessageDialog(null, "La cantidad requerida de dígitos es 16.", "¡Error!",
						JOptionPane.ERROR_MESSAGE);
				return false;
			}
			return true;
		} else {
			JOptionPane.showMessageDialog(null, "El número que intenta ingresar no es válido.", "¡Error!",
					JOptionPane.ERROR_MESSAGE);
			return false;
		}

	}
}
