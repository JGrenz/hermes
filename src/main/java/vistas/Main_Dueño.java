package vistas;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Paint;
import java.awt.Toolkit;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

public class Main_Dueño {

	private static Main_Dueño INSTANCE;
	private JFrame frmMainDueño;
	private JPanel panelOeste, panelCentro;
	private JButton btnReportes, btnDesconectar;
	private JButton btnPerfilDeUsuario;

	public static Main_Dueño getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new Main_Dueño();
		}
		return INSTANCE;
	}

	@SuppressWarnings("serial")
	public Main_Dueño() {

		frmMainDueño = new JFrame();
		frmMainDueño.setIconImage(
				Toolkit.getDefaultToolkit().getImage(Main_Dueño.class.getResource("/imagenes/hermesicono.png")));
		frmMainDueño.setTitle("Hermes - Dueño");
		frmMainDueño.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmMainDueño.setExtendedState(frmMainDueño.getExtendedState() | JFrame.MAXIMIZED_BOTH);
		frmMainDueño.getContentPane().setLayout(new BorderLayout(0, 0));

		panelOeste = new JPanel() {
			protected void paintComponent(Graphics g) {
				if (g instanceof Graphics2D) {
					final int R = 255;
					final int G = 153;
					final int B = 153;
					Paint p = new GradientPaint(0.0f, 0.0f, new Color(R, G, B, 255), getWidth(), 0/*getHeight()*/,
							new Color(R, G, B, 0), true);
					Graphics2D g2d = (Graphics2D) g;
					g2d.setPaint(p);
					g2d.fillRect(0, 0, getWidth(), getHeight());
				} else {
					super.paintComponent(g);
				}
			}
		};
		panelOeste.setBackground(new Color(0,0,0,0));
		frmMainDueño.getContentPane().add(panelOeste, BorderLayout.WEST);

		panelCentro = new JPanel();
		frmMainDueño.getContentPane().add(panelCentro);
		panelCentro.setLayout(new CardLayout(0, 0));
		GridBagLayout gbl_panelOeste = new GridBagLayout();
		gbl_panelOeste.columnWidths = new int[] { 71, 0 };
		gbl_panelOeste.rowHeights = new int[] { 23, 0, 0 };
		gbl_panelOeste.columnWeights = new double[] { 0.0, Double.MIN_VALUE };
		gbl_panelOeste.rowWeights = new double[] { 0.0, 0.0, Double.MIN_VALUE };
		panelOeste.setLayout(gbl_panelOeste);

		btnReportes = new JButton("Reportes");
		btnReportes.setIcon(new ImageIcon(Main_Dueño.class.getResource("/imagenes/iconos/reporte.png")));
		btnReportes.setHorizontalAlignment(SwingConstants.LEADING);
		GridBagConstraints gbc_btnReportes = new GridBagConstraints();
		gbc_btnReportes.fill = GridBagConstraints.BOTH;
		gbc_btnReportes.insets = new Insets(10, 10, 5, 10);
		gbc_btnReportes.gridx = 0;
		gbc_btnReportes.gridy = 0;
		panelOeste.add(btnReportes, gbc_btnReportes);
		
		btnPerfilDeUsuario = new JButton("Perfil de usuario");
		btnPerfilDeUsuario.setIcon(new ImageIcon(Main_Dueño.class.getResource("/imagenes/iconos/perfilUsuario.png")));
		btnPerfilDeUsuario.setHorizontalAlignment(SwingConstants.LEADING);
		GridBagConstraints gbc_btnPerfilDeUsuario = new GridBagConstraints();
		gbc_btnPerfilDeUsuario.fill = GridBagConstraints.BOTH;
		gbc_btnPerfilDeUsuario.insets = new Insets(5, 10, 5, 10);
		gbc_btnPerfilDeUsuario.gridx = 0;
		gbc_btnPerfilDeUsuario.gridy = 1;
		panelOeste.add(btnPerfilDeUsuario, gbc_btnPerfilDeUsuario);
		
		btnDesconectar = new JButton("Desconectar");
		btnDesconectar.setHorizontalAlignment(SwingConstants.LEADING);
		btnDesconectar.setIcon(new ImageIcon(Main_Dueño.class.getResource("/imagenes/iconos/desconectar.png")));
		GridBagConstraints gbc_btnDesconectar = new GridBagConstraints();
		gbc_btnDesconectar.fill = GridBagConstraints.BOTH;
		gbc_btnDesconectar.insets = new Insets(5, 10, 10, 10);
		gbc_btnDesconectar.gridx = 0;
		gbc_btnDesconectar.gridy = 3;
		panelOeste.add(btnDesconectar, gbc_btnDesconectar);

		this.frmMainDueño.setVisible(false);
	}

	public void mostrarVentana() {
		this.frmMainDueño.setVisible(true);
	}

	public JButton getBtnReportes() {
		return btnReportes;
	}
	
	public JButton getBtnDesconectar() {
		return btnDesconectar;
	}

	public void cerrarVentana() {
		this.frmMainDueño.dispose();
	}

	public JPanel getPanelCentro() {
		return panelCentro;
	}

	public JButton getBtnPerfilDeUsuario() {
		return btnPerfilDeUsuario;
	}
}
