package vistas;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Paint;
import java.awt.Toolkit;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

public class Main_PersonalAdm {

	private static Main_PersonalAdm INSTANCE;
	private JFrame frmMainPersonalAdm;
	private JButton btnClientes, btnPasajes, btnEventos, btnDesconectar;
	private JPanel panelOeste, panelCentro;
	private JButton btnPerfilDeUsuario;

	public static Main_PersonalAdm getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new Main_PersonalAdm();
		}
		return INSTANCE;
	}

	@SuppressWarnings("serial")
	public Main_PersonalAdm() {

		frmMainPersonalAdm = new JFrame();
		frmMainPersonalAdm.setIconImage(
				Toolkit.getDefaultToolkit().getImage(Main_PersonalAdm.class.getResource("/imagenes/hermesicono.png")));
		frmMainPersonalAdm.setTitle("Hermes - Personal Administrativo");
		frmMainPersonalAdm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmMainPersonalAdm.setExtendedState(frmMainPersonalAdm.getExtendedState() | JFrame.MAXIMIZED_BOTH);
		frmMainPersonalAdm.getContentPane().setLayout(new BorderLayout(0, 0));

		panelOeste = new JPanel() {
			protected void paintComponent(Graphics g) {
				if (g instanceof Graphics2D) {
					final int R = 204;
					final int G = 153;
					final int B = 255;
					Paint p = new GradientPaint(0.0f, 0.0f, new Color(R, G, B, 255), getWidth(), 0/*getHeight()*/,
							new Color(R, G, B, 0), true);
					Graphics2D g2d = (Graphics2D) g;
					g2d.setPaint(p);
					g2d.fillRect(0, 0, getWidth(), getHeight());
				} else {
					super.paintComponent(g);
				}
			}
		};
		panelOeste.setBackground(new Color(0,0,0,0));
		frmMainPersonalAdm.getContentPane().add(panelOeste, BorderLayout.WEST);

		panelCentro = new JPanel();
		frmMainPersonalAdm.getContentPane().add(panelCentro);
		panelCentro.setLayout(new CardLayout(0, 0));
		GridBagLayout gbl_panelOeste = new GridBagLayout();
		gbl_panelOeste.columnWidths = new int[] { 71, 0 };
		gbl_panelOeste.rowHeights = new int[] { 23, 0, 0, 0, 0 };
		gbl_panelOeste.columnWeights = new double[] { 0.0, Double.MIN_VALUE };
		gbl_panelOeste.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		panelOeste.setLayout(gbl_panelOeste);

		btnClientes = new JButton("Clientes");
		btnClientes.setHorizontalAlignment(SwingConstants.LEADING);
		btnClientes.setIcon(new ImageIcon(Main_PersonalAdm.class.getResource("/imagenes/iconos/cliente.png")));
		GridBagConstraints gbc_btnClientes = new GridBagConstraints();
		gbc_btnClientes.fill = GridBagConstraints.BOTH;
		gbc_btnClientes.insets = new Insets(10, 10, 5, 10);
		gbc_btnClientes.gridx = 0;
		gbc_btnClientes.gridy = 0;
		panelOeste.add(btnClientes, gbc_btnClientes);

		btnPasajes = new JButton("Pasajes");
		btnPasajes.setHorizontalAlignment(SwingConstants.LEADING);
		btnPasajes.setIcon(new ImageIcon(Main_PersonalAdm.class.getResource("/imagenes/iconos/pasaje.png")));
		GridBagConstraints gbc_btnPasajes = new GridBagConstraints();
		gbc_btnPasajes.fill = GridBagConstraints.BOTH;
		gbc_btnPasajes.insets = new Insets(5, 10, 5, 10);
		gbc_btnPasajes.gridx = 0;
		gbc_btnPasajes.gridy = 1;
		panelOeste.add(btnPasajes, gbc_btnPasajes);

		btnEventos = new JButton("Eventos");
		btnEventos.setHorizontalAlignment(SwingConstants.LEADING);
		btnEventos.setIcon(new ImageIcon(Main_PersonalAdm.class.getResource("/imagenes/iconos/evento.png")));
		GridBagConstraints gbc_btnEventos = new GridBagConstraints();
		gbc_btnEventos.fill = GridBagConstraints.BOTH;
		gbc_btnEventos.insets = new Insets(5, 10, 5, 10);
		gbc_btnEventos.gridx = 0;
		gbc_btnEventos.gridy = 2;
		panelOeste.add(btnEventos, gbc_btnEventos);
		
		btnPerfilDeUsuario = new JButton("Perfil de usuario");
		btnPerfilDeUsuario.setIcon(new ImageIcon(Main_PersonalAdm.class.getResource("/imagenes/iconos/perfilUsuario.png")));
		btnPerfilDeUsuario.setHorizontalAlignment(SwingConstants.LEADING);
		GridBagConstraints gbc_btnPerfilDeUsuario = new GridBagConstraints();
		gbc_btnPerfilDeUsuario.fill = GridBagConstraints.BOTH;
		gbc_btnPerfilDeUsuario.insets = new Insets(5, 10, 5, 10);
		gbc_btnPerfilDeUsuario.gridx = 0;
		gbc_btnPerfilDeUsuario.gridy = 3;
		panelOeste.add(btnPerfilDeUsuario, gbc_btnPerfilDeUsuario);
		
		btnDesconectar = new JButton("Desconectar");
		btnDesconectar.setHorizontalAlignment(SwingConstants.LEADING);
		btnDesconectar.setIcon(new ImageIcon(Main_PersonalAdm.class.getResource("/imagenes/iconos/desconectar.png")));
		GridBagConstraints gbc_btnDesconectar = new GridBagConstraints();
		gbc_btnDesconectar.fill = GridBagConstraints.BOTH;
		gbc_btnDesconectar.insets = new Insets(5, 10, 10, 10);
		gbc_btnDesconectar.gridx = 0;
		gbc_btnDesconectar.gridy = 5;
		panelOeste.add(btnDesconectar, gbc_btnDesconectar);

		this.frmMainPersonalAdm.setVisible(false);
	}

	public void mostrarVentana() {
		this.frmMainPersonalAdm.setVisible(true);
	}

	public JButton getBtnClientes() {
		return btnClientes;
	}

	public JButton getBtnVentas() {
		return btnPasajes;
	}

	public JButton getBtnEventos() {
		return btnEventos;
	}
	
	public JButton getBtnDesconectar() {
		return btnDesconectar;
	}

	public void cerrarVentana() {
		this.frmMainPersonalAdm.dispose();
	}

	public JPanel getPanelCentro() {
		return panelCentro;
	}

	public JButton getBtnPerfilDeUsuario() {
		return btnPerfilDeUsuario;
	}
}
