package vistas.coordinador.ubicaciones;

import java.awt.Dialog.ModalityType;
import java.awt.Toolkit;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import vistas.ValidacionCampos;

public class Ventana_AgregarUbicacion {
	
	private static Ventana_AgregarUbicacion INSTANCE;
	private JDialog frmAgregarUbicacion;
	private JPanel contentPane;
	private JButton btnConfirmar, btnAgrego, btnElimino;
	private JTextField txtDestino, txtProvincia, txtDetalle;
	private JComboBox<String> comboBoxContinente;
	private JComboBox<String> comboBoxPais;
	private JLabel lblTags;
	private JList<String> listadoTags, listadoTagsAgregados;
	private DefaultListModel<String> lm1, lm2;

	public static Ventana_AgregarUbicacion getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new Ventana_AgregarUbicacion();
		}
		return INSTANCE;
	}

	private Ventana_AgregarUbicacion() {

		frmAgregarUbicacion = new JDialog();
		frmAgregarUbicacion.setIconImage(Toolkit.getDefaultToolkit().getImage(Ventana_AgregarUbicacion.class.getResource("/imagenes/hermesicono.png")));
		frmAgregarUbicacion.setModalityType(ModalityType.APPLICATION_MODAL);
		frmAgregarUbicacion.setResizable(false);
		frmAgregarUbicacion.setTitle("Hermes - Agregar ubicación");
		frmAgregarUbicacion.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmAgregarUbicacion.setBounds(100, 100, 627, 809);
		frmAgregarUbicacion.setLocationRelativeTo(null);
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		frmAgregarUbicacion.setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 621, 780);
		contentPane.add(panel);
		panel.setLayout(null);

		JLabel lblContinente = new JLabel("Continente:");
		lblContinente.setHorizontalAlignment(SwingConstants.CENTER);
		lblContinente.setBounds(10, 15, 199, 30);
		panel.add(lblContinente);

		JLabel lblPais = new JLabel("Pais:");
		lblPais.setHorizontalAlignment(SwingConstants.CENTER);
		lblPais.setBounds(10, 50, 199, 30);
		panel.add(lblPais);

		JLabel lblProvincia = new JLabel("Provincia:");
		lblProvincia.setHorizontalAlignment(SwingConstants.CENTER);
		lblProvincia.setBounds(10, 85, 199, 30);
		panel.add(lblProvincia);

		JLabel lblUbicacion = new JLabel("Nombre:");
		lblUbicacion.setHorizontalAlignment(SwingConstants.CENTER);
		lblUbicacion.setBounds(10, 120, 199, 30);
		panel.add(lblUbicacion);

		JLabel lblDetalle = new JLabel("Detalle:");
		lblDetalle.setHorizontalAlignment(SwingConstants.CENTER);
		lblDetalle.setBounds(10, 155, 199, 30);
		panel.add(lblDetalle);

		comboBoxContinente = new JComboBox<String>();
		comboBoxContinente.setBounds(219, 15, 279, 30);
		panel.add(comboBoxContinente);

		comboBoxPais = new JComboBox<String>();
		comboBoxPais.setBounds(219, 50, 279, 30);
		panel.add(comboBoxPais);

		txtProvincia = new JTextField();
		txtProvincia.setColumns(10);
		txtProvincia.setBounds(219, 85, 279, 30);
		panel.add(txtProvincia);

		txtDestino = new JTextField();
		txtDestino.setBounds(219, 120, 279, 30);
		panel.add(txtDestino);
		txtDestino.setColumns(10);

		txtDetalle = new JTextField();
		txtDetalle.setColumns(10);
		txtDetalle.setBounds(219, 155, 279, 30);
		panel.add(txtDetalle);

		btnConfirmar = new JButton("Confirmar");
		btnConfirmar.setBounds(485, 746, 126, 23);
		panel.add(btnConfirmar);
		
		lblTags = new JLabel("Listado de tags");
		lblTags.setHorizontalAlignment(SwingConstants.CENTER);
		lblTags.setBounds(58, 249, 150, 30);
		panel.add(lblTags);
		
		String[] listaTags = {"Playa","Montaña","Ciudad","Bar","Monumento","Comercial","Bosque","Nieve","Selva"};
		listadoTags = new JList<String>();
		listadoTags.setModel(new DefaultListModel<String>());
		lm1 = (DefaultListModel<String>) listadoTags.getModel();
		for (String tag : listaTags) {
			lm1.addElement(tag);
		}
		listadoTags.setModel(lm1);
		listadoTags.setBounds(10, 231, 199, 301);
		JScrollPane pane = new JScrollPane(listadoTags);
		pane.setBounds(10, 283, 246, 350);
		panel.add(pane);
		
		listadoTagsAgregados = new JList<String>();
		listadoTagsAgregados.setModel(new DefaultListModel<String>());
		lm2 = (DefaultListModel<String>) listadoTagsAgregados.getModel();
		listadoTagsAgregados.setBounds(10, 231, 199, 301);
		JScrollPane pane2 = new JScrollPane(listadoTagsAgregados);
		pane2.setBounds(365, 283, 246, 350);
		panel.add(pane2);

		JLabel lblTagsAgregados = new JLabel("Tags agregados");
		lblTagsAgregados.setHorizontalAlignment(SwingConstants.CENTER);
		lblTagsAgregados.setBounds(413, 249, 150, 30);
		panel.add(lblTagsAgregados);
		
		btnAgrego = new JButton("→");
		btnAgrego.setBounds(287, 403, 45, 45);
		panel.add(btnAgrego);
		
		btnElimino = new JButton("←");
		btnElimino.setBounds(287, 468, 45, 45);
		panel.add(btnElimino);
		
		this.frmAgregarUbicacion.setVisible(false);
	}

	public void mostrarVentana() {
		this.frmAgregarUbicacion.setVisible(true);
	}
	
	public JComboBox<String> getContinentes() {
		return comboBoxContinente;
	}
	
	public JComboBox<String> getPaises() {
		return comboBoxPais;
	}
	
	public JTextField getTxtProvincia() {
		return txtProvincia;
	}
	
	public JTextField getTxtDestino() {
		return txtDestino;
	}

	public JTextField getTxtDetalle() {
		return txtDetalle;
	}

	public JButton getBtnAgregarTag() {
		return btnAgrego;
	}
	
	public JButton getBtnEliminarTag() {
		return btnElimino;
	}
	
	public JButton getBtnConfirmar() {
		return btnConfirmar;
	}

	public JList<String> getListadoTags() {
		return listadoTags;
	}
	
	public DefaultListModel<String> getModelTags(){
		return lm1;
	}

	public JList<String> getListadoTagsAgregados() {
		return listadoTagsAgregados;
	}
	
	public DefaultListModel<String> getModelTagsAgregados(){
		return lm2;
	}

	public void cerrar() {
		this.txtProvincia.setText(null);
		this.txtDestino.setText(null);
		this.txtDetalle.setText(null);
		this.comboBoxContinente.setSelectedIndex(-1);
		this.comboBoxPais.setSelectedIndex(-1);
		this.frmAgregarUbicacion.dispose();
	}

	private boolean camposVacios() {
		if (txtProvincia.getText().isEmpty() || txtDestino.getText().isEmpty()
			|| comboBoxContinente.getSelectedIndex() == -1 || comboBoxPais.getSelectedIndex() == -1)
		{
			JOptionPane.showMessageDialog(null, "Los campos Continente, Pais, Provincia y Nombre no pueden quedar vacíos.");
			return true;
		}
		else
			return false;
	}
	
	private boolean camposCorrectos() {
		ValidacionCampos validador = new ValidacionCampos();
		if(!validador.esPalabra(txtProvincia.getText()) || !validador.esPalabra(txtDestino.getText()))
			return false;
		else
			return true;
	}
	
	public boolean verificarCampo() {
		if (!camposVacios() && camposCorrectos())
			return true;
		else
			return false;
	}
}
