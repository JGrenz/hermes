package vistas.coordinador.ubicaciones;

import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;

import vistas.TextPrompt;

public class Main_Ubicaciones {

	private static Main_Ubicaciones INSTANCE;
	private JPanel frmMainUbicaciones;
	private JTable tablaUbicaciones;
	private DefaultTableModel modelUbicaciones;
	private String[] nombreColumnas = { "Ciudad", "Provincia", "Pais", "Continente", "Detalle", "Tags", "Estado" };
	private JButton btnAgregar, btnEditar, btnEstado, btnRealizarBusqueda;
	private JTextField txtCiudad, txtProvincia, txtPais, txtContinente, txtTag;
	private JRadioButton radioTodos, radioHabilitados, radioInhabilitados;
	private ButtonGroup filtro;
	private JPanel panelBusqueda, panelTabla, panelABM;

	public static Main_Ubicaciones getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new Main_Ubicaciones();
		}
		return INSTANCE;
	}

	public Main_Ubicaciones() {

		frmMainUbicaciones = new JPanel();

		filtro = new ButtonGroup();
		GridBagLayout gbl_frmMainUbicaciones = new GridBagLayout();
		gbl_frmMainUbicaciones.columnWidths = new int[] { 975, 0 };
		gbl_frmMainUbicaciones.rowHeights = new int[] { 69, 182, 429, 0 };
		gbl_frmMainUbicaciones.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gbl_frmMainUbicaciones.rowWeights = new double[] { 0.0, 0.0, 1.0, Double.MIN_VALUE };
		frmMainUbicaciones.setLayout(gbl_frmMainUbicaciones);

		panelABM = new JPanel();
		GridBagConstraints gbc_panelABM = new GridBagConstraints();
		gbc_panelABM.fill = GridBagConstraints.BOTH;
		gbc_panelABM.insets = new Insets(0, 0, 5, 0);
		gbc_panelABM.gridx = 0;
		gbc_panelABM.gridy = 0;
		frmMainUbicaciones.add(panelABM, gbc_panelABM);

		btnAgregar = new JButton("Agregar");
		btnAgregar.setIcon(new ImageIcon(Main_Ubicaciones.class.getResource("/imagenes/iconos/ubicacionAgregar.png")));
		panelABM.add(btnAgregar);

		btnEstado = new JButton("Habilitar / Inhabilitar");
		btnEstado.setIcon(new ImageIcon(Main_Ubicaciones.class.getResource("/imagenes/iconos/ubicacionHabInhab.png")));
		panelABM.add(btnEstado);

		btnEditar = new JButton("Editar");
		btnEditar.setIcon(new ImageIcon(Main_Ubicaciones.class.getResource("/imagenes/iconos/ubicacionEditar.png")));
		panelABM.add(btnEditar);

		panelBusqueda = new JPanel();
		GridBagConstraints gbc_panelBusqueda = new GridBagConstraints();
		gbc_panelBusqueda.fill = GridBagConstraints.BOTH;
		gbc_panelBusqueda.insets = new Insets(0, 0, 5, 0);
		gbc_panelBusqueda.gridx = 0;
		gbc_panelBusqueda.gridy = 1;
		frmMainUbicaciones.add(panelBusqueda, gbc_panelBusqueda);
		GridBagLayout gbl_panelBusqueda = new GridBagLayout();
		gbl_panelBusqueda.columnWidths = new int[] { 127, 58, 145, 100, 109, 0 };
		gbl_panelBusqueda.rowHeights = new int[] { 30, 25, 25, 26, 25, 25, 0 };
		gbl_panelBusqueda.columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		gbl_panelBusqueda.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		panelBusqueda.setLayout(gbl_panelBusqueda);

		JLabel lblFiltrarPor = new JLabel("Filtrar por:");
		lblFiltrarPor.setPreferredSize(new Dimension(0, 30));
		lblFiltrarPor.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_lblFiltrarPor = new GridBagConstraints();
		gbc_lblFiltrarPor.fill = GridBagConstraints.BOTH;
		gbc_lblFiltrarPor.insets = new Insets(0, 0, 5, 5);
		gbc_lblFiltrarPor.gridx = 0;
		gbc_lblFiltrarPor.gridy = 0;
		panelBusqueda.add(lblFiltrarPor, gbc_lblFiltrarPor);

		JLabel lblBusquedaPor = new JLabel("Búsqueda por:");
		lblBusquedaPor.setPreferredSize(new Dimension(0, 30));
		lblBusquedaPor.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_lblBusquedaPor = new GridBagConstraints();
		gbc_lblBusquedaPor.fill = GridBagConstraints.BOTH;
		gbc_lblBusquedaPor.insets = new Insets(0, 0, 5, 5);
		gbc_lblBusquedaPor.gridx = 2;
		gbc_lblBusquedaPor.gridy = 0;
		panelBusqueda.add(lblBusquedaPor, gbc_lblBusquedaPor);

		btnRealizarBusqueda = new JButton("Buscar");
		btnRealizarBusqueda.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_btnRealizarBusqueda = new GridBagConstraints();
		gbc_btnRealizarBusqueda.fill = GridBagConstraints.BOTH;
		gbc_btnRealizarBusqueda.insets = new Insets(0, 0, 5, 5);
		gbc_btnRealizarBusqueda.gridx = 3;
		gbc_btnRealizarBusqueda.gridy = 0;
		panelBusqueda.add(btnRealizarBusqueda, gbc_btnRealizarBusqueda);

		radioTodos = new JRadioButton("Todos");
		radioTodos.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_radioTodos = new GridBagConstraints();
		gbc_radioTodos.fill = GridBagConstraints.BOTH;
		gbc_radioTodos.insets = new Insets(0, 0, 5, 5);
		gbc_radioTodos.gridx = 0;
		gbc_radioTodos.gridy = 1;
		panelBusqueda.add(radioTodos, gbc_radioTodos);
		filtro.add(radioTodos);

		JLabel lblCiudad = new JLabel("Ciudad");
		lblCiudad.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_lblCiudad = new GridBagConstraints();
		gbc_lblCiudad.fill = GridBagConstraints.BOTH;
		gbc_lblCiudad.insets = new Insets(0, 0, 5, 5);
		gbc_lblCiudad.gridx = 2;
		gbc_lblCiudad.gridy = 1;
		panelBusqueda.add(lblCiudad, gbc_lblCiudad);

		txtCiudad = new JTextField();
		txtCiudad.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_txtCiudad = new GridBagConstraints();
		gbc_txtCiudad.gridwidth = 2;
		gbc_txtCiudad.fill = GridBagConstraints.BOTH;
		gbc_txtCiudad.insets = new Insets(0, 0, 5, 5);
		gbc_txtCiudad.gridx = 3;
		gbc_txtCiudad.gridy = 1;
		panelBusqueda.add(txtCiudad, gbc_txtCiudad);
		txtCiudad.setColumns(10);
		TextPrompt phCiudad = new TextPrompt("Ingrese ciudad...", txtCiudad);
		phCiudad.setText("Ingrese ciudad...");
		phCiudad.changeAlpha(0.75f);
		phCiudad.changeStyle(Font.ITALIC);

		radioHabilitados = new JRadioButton("Habilitados");
		radioHabilitados.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_radioHabilitados = new GridBagConstraints();
		gbc_radioHabilitados.fill = GridBagConstraints.BOTH;
		gbc_radioHabilitados.insets = new Insets(0, 0, 5, 5);
		gbc_radioHabilitados.gridx = 0;
		gbc_radioHabilitados.gridy = 2;
		panelBusqueda.add(radioHabilitados, gbc_radioHabilitados);
		radioHabilitados.setSelected(true);
		filtro.add(radioHabilitados);

		JLabel lblProvincia = new JLabel("Provincia / estado");
		lblProvincia.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_lblProvincia = new GridBagConstraints();
		gbc_lblProvincia.fill = GridBagConstraints.BOTH;
		gbc_lblProvincia.insets = new Insets(0, 0, 5, 5);
		gbc_lblProvincia.gridx = 2;
		gbc_lblProvincia.gridy = 2;
		panelBusqueda.add(lblProvincia, gbc_lblProvincia);

		txtProvincia = new JTextField();
		txtProvincia.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_txtProvincia = new GridBagConstraints();
		gbc_txtProvincia.gridwidth = 2;
		gbc_txtProvincia.fill = GridBagConstraints.BOTH;
		gbc_txtProvincia.insets = new Insets(0, 0, 5, 5);
		gbc_txtProvincia.gridx = 3;
		gbc_txtProvincia.gridy = 2;
		panelBusqueda.add(txtProvincia, gbc_txtProvincia);
		txtProvincia.setColumns(10);
		TextPrompt phProvincia = new TextPrompt("Ingrese provincia/estado...", txtProvincia);
		phProvincia.setText("Ingrese provincia/estado...");
		phProvincia.changeAlpha(0.75f);
		phProvincia.changeStyle(Font.ITALIC);

		radioInhabilitados = new JRadioButton("Inhabilitados");
		radioInhabilitados.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_radioInhabilitados = new GridBagConstraints();
		gbc_radioInhabilitados.fill = GridBagConstraints.BOTH;
		gbc_radioInhabilitados.insets = new Insets(0, 0, 5, 5);
		gbc_radioInhabilitados.gridx = 0;
		gbc_radioInhabilitados.gridy = 3;
		panelBusqueda.add(radioInhabilitados, gbc_radioInhabilitados);
		filtro.add(radioInhabilitados);

		JLabel lblPais = new JLabel("Pais");
		lblPais.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_lblPais = new GridBagConstraints();
		gbc_lblPais.fill = GridBagConstraints.BOTH;
		gbc_lblPais.insets = new Insets(0, 0, 5, 5);
		gbc_lblPais.gridx = 2;
		gbc_lblPais.gridy = 3;
		panelBusqueda.add(lblPais, gbc_lblPais);

		txtPais = new JTextField();
		txtPais.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_txtPais = new GridBagConstraints();
		gbc_txtPais.gridwidth = 2;
		gbc_txtPais.fill = GridBagConstraints.BOTH;
		gbc_txtPais.insets = new Insets(0, 0, 5, 5);
		gbc_txtPais.gridx = 3;
		gbc_txtPais.gridy = 3;
		panelBusqueda.add(txtPais, gbc_txtPais);
		txtPais.setColumns(10);
		TextPrompt phPais = new TextPrompt("Ingrese pais...", txtPais);
		phPais.setText("Ingrese pais...");
		phPais.changeAlpha(0.75f);
		phPais.changeStyle(Font.ITALIC);

		JLabel lblContinente = new JLabel("Continente");
		lblContinente.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_lblContinente = new GridBagConstraints();
		gbc_lblContinente.fill = GridBagConstraints.BOTH;
		gbc_lblContinente.insets = new Insets(0, 0, 5, 5);
		gbc_lblContinente.gridx = 2;
		gbc_lblContinente.gridy = 4;
		panelBusqueda.add(lblContinente, gbc_lblContinente);

		txtContinente = new JTextField();
		txtContinente.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_txtContinente = new GridBagConstraints();
		gbc_txtContinente.gridwidth = 2;
		gbc_txtContinente.fill = GridBagConstraints.BOTH;
		gbc_txtContinente.insets = new Insets(0, 0, 5, 5);
		gbc_txtContinente.gridx = 3;
		gbc_txtContinente.gridy = 4;
		panelBusqueda.add(txtContinente, gbc_txtContinente);
		txtContinente.setColumns(10);
		TextPrompt phContinente = new TextPrompt("Ingrese continente...", txtContinente);
		phContinente.setText("Ingrese continente...");
		phContinente.changeAlpha(0.75f);
		phContinente.changeStyle(Font.ITALIC);

		JLabel lblTag = new JLabel("Tag");
		lblTag.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_lblTag = new GridBagConstraints();
		gbc_lblTag.fill = GridBagConstraints.BOTH;
		gbc_lblTag.insets = new Insets(0, 0, 0, 5);
		gbc_lblTag.gridx = 2;
		gbc_lblTag.gridy = 5;
		panelBusqueda.add(lblTag, gbc_lblTag);

		txtTag = new JTextField();
		txtTag.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_txtTag = new GridBagConstraints();
		gbc_txtTag.gridwidth = 2;
		gbc_txtTag.insets = new Insets(0, 0, 0, 5);
		gbc_txtTag.fill = GridBagConstraints.BOTH;
		gbc_txtTag.gridx = 3;
		gbc_txtTag.gridy = 5;
		panelBusqueda.add(txtTag, gbc_txtTag);
		txtTag.setColumns(10);
		TextPrompt phTag = new TextPrompt("Ingrese tag...", txtTag);
		phTag.setText("Ingrese tag...");
		phTag.changeAlpha(0.75f);
		phTag.changeStyle(Font.ITALIC);

		panelTabla = new JPanel();
		GridBagConstraints gbc_panelTabla = new GridBagConstraints();
		gbc_panelTabla.fill = GridBagConstraints.BOTH;
		gbc_panelTabla.gridx = 0;
		gbc_panelTabla.gridy = 2;
		frmMainUbicaciones.add(panelTabla, gbc_panelTabla);
		panelTabla.setLayout(new CardLayout(0, 0));

		JScrollPane spUbicaciones = new JScrollPane();
		panelTabla.add(spUbicaciones, "name_98763112356739");
		modelUbicaciones = new DefaultTableModel(null, nombreColumnas) {
			private static final long serialVersionUID = 1L;

			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		tablaUbicaciones = new JTable(modelUbicaciones);
		tablaUbicaciones.getTableHeader().setReorderingAllowed(false);
		spUbicaciones.setViewportView(tablaUbicaciones);

		this.frmMainUbicaciones.setVisible(false);
	}

	public void mostrarVentana() {
		this.frmMainUbicaciones.setVisible(true);
	}

	public JButton getBtnAgregar() {
		return btnAgregar;
	}

	public JButton getBtnEstado() {
		return btnEstado;
	}

	public JButton getBtnEditar() {
		return btnEditar;
	}

	public DefaultTableModel getModelUbicaciones() {
		return modelUbicaciones;
	}

	public JTable getTablaUbicaciones() {
		return tablaUbicaciones;
	}

	public String[] getNombreColumnas() {
		return nombreColumnas;
	}

	public JButton getBtnRealizarBusqueda() {
		return btnRealizarBusqueda;
	}

	public JTextField getBarraBusquedaCiudad() {
		return txtCiudad;
	}

	public JTextField getBarraBusquedaProvincia() {
		return txtProvincia;
	}

	public JTextField getBarraBusquedaPais() {
		return txtPais;
	}

	public JTextField getBarraBusquedaContinente() {
		return txtContinente;
	}

	public JTextField getBarraBusquedaTag() {
		return txtTag;
	}

	public JRadioButton getRadioTodos() {
		return radioTodos;
	}

	public JRadioButton getRadioHabilitados() {
		return radioHabilitados;
	}

	public JRadioButton getRadioInhabilitados() {
		return radioInhabilitados;
	}

	public JTextField getTxtCiudad() {
		return txtCiudad;
	}

	public JTextField getTxtProvincia() {
		return txtProvincia;
	}

	public JTextField getTxtPais() {
		return txtPais;
	}

	public JTextField getTxtContinente() {
		return txtContinente;
	}

	public JTextField getTxtTag() {
		return txtTag;
	}

	public void mostrarBarraBusquedaPorCiudad() {
		btnRealizarBusqueda.setVisible(true);
		txtCiudad.setVisible(true);
		txtProvincia.setVisible(false);
		txtPais.setVisible(false);
		txtContinente.setVisible(false);
		txtTag.setVisible(false);
		txtProvincia.setText("");
		txtPais.setText("");
		txtContinente.setText("");
		txtTag.setText("");
	}

	public void mostrarBarraBusquedaPorProvincia() {
		btnRealizarBusqueda.setVisible(true);
		txtCiudad.setVisible(false);
		txtProvincia.setVisible(true);
		txtPais.setVisible(false);
		txtContinente.setVisible(false);
		txtTag.setVisible(false);
		txtCiudad.setText("");
		txtPais.setText("");
		txtContinente.setText("");
		txtTag.setText("");
	}

	public void mostrarBarraBusquedaPorPais() {
		btnRealizarBusqueda.setVisible(true);
		txtCiudad.setVisible(false);
		txtProvincia.setVisible(false);
		txtPais.setVisible(true);
		txtContinente.setVisible(false);
		txtTag.setVisible(false);
		txtCiudad.setText("");
		txtProvincia.setText("");
		txtContinente.setText("");
		txtTag.setText("");
	}

	public void mostrarBarraBusquedaPorContinente() {
		btnRealizarBusqueda.setVisible(true);
		txtCiudad.setVisible(false);
		txtProvincia.setVisible(false);
		txtPais.setVisible(false);
		txtContinente.setVisible(true);
		txtTag.setVisible(false);
		txtCiudad.setText("");
		txtProvincia.setText("");
		txtPais.setText("");
		txtTag.setText("");
	}

	public void mostrarBarraBusquedaPorTag() {
		btnRealizarBusqueda.setVisible(true);
		txtCiudad.setVisible(false);
		txtProvincia.setVisible(false);
		txtPais.setVisible(false);
		txtContinente.setVisible(false);
		txtTag.setVisible(true);
		txtCiudad.setText("");
		txtProvincia.setText("");
		txtPais.setText("");
		txtContinente.setText("");
	}

	public JPanel getPanel() {
		return frmMainUbicaciones;
	}
}
