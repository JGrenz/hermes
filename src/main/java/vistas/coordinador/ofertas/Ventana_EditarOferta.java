package vistas.coordinador.ofertas;

import java.awt.Dialog.ModalityType;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import com.toedter.calendar.JDateChooser;
import com.toedter.calendar.JTextFieldDateEditor;

import vistas.ValidacionCampos;

public class Ventana_EditarOferta {

    private static Ventana_EditarOferta INSTANCE;
    private JDialog frmEditarOferta;
    private JPanel contentPane;
    private JTextField txtTitulo, txtDescuento;
    private JDateChooser txtVencimiento;
    private JButton btnConfirmar;

    public static Ventana_EditarOferta getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new Ventana_EditarOferta();
        }
        return INSTANCE;
    }

    private Ventana_EditarOferta() {

        frmEditarOferta = new JDialog();
        frmEditarOferta.setIconImage(Toolkit.getDefaultToolkit()
                .getImage(Ventana_EditarOferta.class.getResource("/imagenes/hermesicono.png")));
        frmEditarOferta.setModalityType(ModalityType.APPLICATION_MODAL);
        frmEditarOferta.setResizable(false);
        frmEditarOferta.setTitle("Hermes - Editar oferta");
        frmEditarOferta.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frmEditarOferta.setBounds(100, 100, 376, 222);
        frmEditarOferta.setLocationRelativeTo(null);

        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        frmEditarOferta.setContentPane(contentPane);
        contentPane.setLayout(null);

        JPanel panel = new JPanel();
        panel.setBounds(10, 11, 350, 171);
        contentPane.add(panel);
        GridBagLayout gbl_panel = new GridBagLayout();
        gbl_panel.columnWidths = new int[]{133, 79, 0, 0};
        gbl_panel.rowHeights = new int[]{30, 30, 30, 0, 30, 0};
        gbl_panel.columnWeights = new double[]{0.0, 0.0, 1.0, Double.MIN_VALUE};
        gbl_panel.rowWeights = new double[]{0.0, 0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
        panel.setLayout(gbl_panel);

        JLabel lblTitulo = new JLabel("Titulo:");
        lblTitulo.setHorizontalAlignment(SwingConstants.CENTER);
        GridBagConstraints gbc_lblTitulo = new GridBagConstraints();
        gbc_lblTitulo.fill = GridBagConstraints.BOTH;
        gbc_lblTitulo.insets = new Insets(5, 5, 0, 5);
        gbc_lblTitulo.gridx = 0;
        gbc_lblTitulo.gridy = 0;
        panel.add(lblTitulo, gbc_lblTitulo);

        txtTitulo = new JTextField();
        GridBagConstraints gbc_txtTitulo = new GridBagConstraints();
        gbc_txtTitulo.gridwidth = 2;
        gbc_txtTitulo.fill = GridBagConstraints.BOTH;
        gbc_txtTitulo.insets = new Insets(5, 0, 0, 5);
        gbc_txtTitulo.gridx = 1;
        gbc_txtTitulo.gridy = 0;
        panel.add(txtTitulo, gbc_txtTitulo);
        txtTitulo.setColumns(10);

        JLabel lblDeDescuento = new JLabel("% de descuento:");
        lblDeDescuento.setHorizontalAlignment(SwingConstants.CENTER);
        GridBagConstraints gbc_lblDeDescuento = new GridBagConstraints();
        gbc_lblDeDescuento.fill = GridBagConstraints.BOTH;
        gbc_lblDeDescuento.insets = new Insets(5, 5, 0, 5);
        gbc_lblDeDescuento.gridx = 0;
        gbc_lblDeDescuento.gridy = 1;
        panel.add(lblDeDescuento, gbc_lblDeDescuento);

        txtDescuento = new JTextField();
        txtDescuento.setColumns(10);
        GridBagConstraints gbc_txtDescuento = new GridBagConstraints();
        gbc_txtDescuento.gridwidth = 2;
        gbc_txtDescuento.insets = new Insets(5, 0, 0, 5);
        gbc_txtDescuento.fill = GridBagConstraints.BOTH;
        gbc_txtDescuento.gridx = 1;
        gbc_txtDescuento.gridy = 1;
        panel.add(txtDescuento, gbc_txtDescuento);

        JLabel lblVencimiento = new JLabel("Vencimiento:");
        lblVencimiento.setHorizontalAlignment(SwingConstants.CENTER);
        GridBagConstraints gbc_lblVencimiento = new GridBagConstraints();
        gbc_lblVencimiento.fill = GridBagConstraints.BOTH;
        gbc_lblVencimiento.insets = new Insets(5, 5, 0, 5);
        gbc_lblVencimiento.gridx = 0;
        gbc_lblVencimiento.gridy = 2;
        panel.add(lblVencimiento, gbc_lblVencimiento);

        txtVencimiento = new JDateChooser();
        txtVencimiento.setPreferredSize(new Dimension(0, 30));
        txtVencimiento.setMinSelectableDate(new Date());
        JTextFieldDateEditor editor = (JTextFieldDateEditor) txtVencimiento.getDateEditor();
        GridBagConstraints gbc_txtVencimiento = new GridBagConstraints();
        gbc_txtVencimiento.gridwidth = 2;
        gbc_txtVencimiento.fill = GridBagConstraints.BOTH;
        gbc_txtVencimiento.insets = new Insets(5, 0, 0, 5);
        gbc_txtVencimiento.gridx = 1;
        gbc_txtVencimiento.gridy = 2;
        panel.add(txtVencimiento, gbc_txtVencimiento);
        editor.setEditable(false);

        btnConfirmar = new JButton("Confirmar");
        GridBagConstraints gbc_btnConfirmar = new GridBagConstraints();
        gbc_btnConfirmar.fill = GridBagConstraints.BOTH;
        gbc_btnConfirmar.insets = new Insets(0, 0, 5, 5);
        gbc_btnConfirmar.gridx = 2;
        gbc_btnConfirmar.gridy = 4;
        panel.add(btnConfirmar, gbc_btnConfirmar);

        this.frmEditarOferta.setVisible(false);
    }

    public void mostrarVentana() {
        this.frmEditarOferta.setVisible(true);
    }

    public JTextField getTxtTitulo() {
        return txtTitulo;
    }

    public JTextField getTxtDescuento() {
        return txtDescuento;
    }

    public LocalDate getTxtVencimiento() {
        Date date = txtVencimiento.getDate();
        return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    }

    public JButton getBtnConfirmar() {
        return btnConfirmar;
    }

    public void cerrar() {
        this.txtTitulo.setText(null);
        this.txtDescuento.setText(null);
        this.txtVencimiento.setDate(null);
        this.frmEditarOferta.dispose();
    }

	private boolean camposVacios() {
		if (txtTitulo.getText().equals("") || txtDescuento.getText().equals("") || txtVencimiento.getDate() == null) {
			JOptionPane.showMessageDialog(null, "No pueden quedar campos vacíos.");
			return true;
		} else
			return false;
	}

	private boolean camposCorrectos() {
		ValidacionCampos validador = new ValidacionCampos();
		if (!validador.esNumerico(txtDescuento.getText())) {
			return false;				
		} else {
			int descuento = Integer.parseInt(txtDescuento.getText());
			if(descuento > 99) {
				JOptionPane.showMessageDialog(null, "El descuento no puede ser mayor al 99%.");
				return false;				
			}
		}
		return true;
	}

	public boolean verificarCampo() {
		if (!camposVacios() && camposCorrectos())
			return true;
		else
			return false;
	}

    public void setTxtTitulo(String titulo) {
        this.txtTitulo.setText(titulo);
    }

    public void setPorcentajeDescuento(int porcentajeDescuento) {
        this.txtDescuento.setText("" + porcentajeDescuento);
    }

    public void setFechaVencimiento(LocalDate fechaVencimiento) {
        java.util.Date fechaVencimientoNueva = java.sql.Date.valueOf(fechaVencimiento);

        this.txtVencimiento.setDate(fechaVencimientoNueva);
    }
}
