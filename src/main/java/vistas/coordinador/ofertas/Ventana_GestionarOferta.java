package vistas.coordinador.ofertas;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import vistas.TextPrompt;

public class Ventana_GestionarOferta extends JDialog {

	private static final long serialVersionUID = 1L;
	private static Ventana_GestionarOferta INSTANCE;
	private JDialog frmGestionarOferta;
	private JPanel panel, panelBusqueda, panelSur;
	private JButton btnConfirmar, btnRealizarBusqueda, btnAsignarOferta, btnDesasignarOferta;
	private JTextField txtDescuento, txtOferta;
	private JTextField txtBusquedaCodigo, txtCiudadDestino, txtProvinciaDestino;
	private JComboBox<String> cbPaisDestino, cbContinenteDestino;
	private JTable tablaViajes, tablaViajesAsignados;
	private DefaultTableModel modelViajes, modelViajesAsignados;
	private String[] nombreColumnas = { "Código de referencia", "Origen", "Destino", "Fecha de salida", "Medio de transporte",
			"Precio", "Impuesto" };

	public static Ventana_GestionarOferta getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new Ventana_GestionarOferta();
		}
		return INSTANCE;
	}

	private Ventana_GestionarOferta() {

		frmGestionarOferta = new JDialog();
		frmGestionarOferta.setIconImage(Toolkit.getDefaultToolkit()
				.getImage(Ventana_GestionarOferta.class.getResource("/imagenes/hermesicono.png")));
		frmGestionarOferta.setModalityType(ModalityType.APPLICATION_MODAL);
		frmGestionarOferta.setResizable(false);
		frmGestionarOferta.setTitle("Hermes - Gestionar oferta");
		frmGestionarOferta.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmGestionarOferta.setBounds(100, 100, 948, 764);
		frmGestionarOferta.setLocationRelativeTo(null);

		panel = new JPanel();
		panel.setBorder(new EmptyBorder(5, 5, 5, 5));
		frmGestionarOferta.setContentPane(panel);
		panel.setLayout(null);

		JLabel lblOferta = new JLabel("Oferta:");
		lblOferta.setHorizontalAlignment(SwingConstants.CENTER);
		lblOferta.setBounds(10, 10, 126, 30);
		panel.add(lblOferta);

		txtOferta = new JTextField();
		txtOferta.setEditable(false);
		txtOferta.setBounds(146, 10, 188, 30);
		panel.add(txtOferta);

		JLabel lblDescuento = new JLabel("Descuento:");
		lblDescuento.setHorizontalAlignment(SwingConstants.CENTER);
		lblDescuento.setBounds(10, 45, 126, 30);
		panel.add(lblDescuento);

		txtDescuento = new JTextField();
		txtDescuento.setEditable(false);
		txtDescuento.setBounds(146, 45, 188, 30);
		panel.add(txtDescuento);

		panelBusqueda = new JPanel();
		panelBusqueda.setBounds(10, 86, 690, 150);
		GridBagConstraints gbc_panelBusqueda = new GridBagConstraints();
		gbc_panelBusqueda.fill = GridBagConstraints.BOTH;
		gbc_panelBusqueda.insets = new Insets(0, 0, 5, 0);
		gbc_panelBusqueda.gridx = 0;
		gbc_panelBusqueda.gridy = 1;
		panel.add(panelBusqueda, gbc_panelBusqueda);
		GridBagLayout gbl_panelBusqueda = new GridBagLayout();
		gbl_panelBusqueda.columnWidths = new int[] { 120, 210, 30, 120, 210 };
		gbl_panelBusqueda.rowHeights = new int[] { 30, 30, 30, 30 };
		gbl_panelBusqueda.columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0 };
		gbl_panelBusqueda.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0 };
		panelBusqueda.setLayout(gbl_panelBusqueda);

		JLabel lblBusquedaPor = new JLabel("Búsqueda por:");
		lblBusquedaPor.setPreferredSize(new Dimension(0, 30));
		lblBusquedaPor.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_lblBusquedaPor = new GridBagConstraints();
		gbc_lblBusquedaPor.fill = GridBagConstraints.BOTH;
		gbc_lblBusquedaPor.insets = new Insets(0, 5, 5, 5);
		gbc_lblBusquedaPor.gridx = 0;
		gbc_lblBusquedaPor.gridy = 0;
		panelBusqueda.add(lblBusquedaPor, gbc_lblBusquedaPor);

		btnRealizarBusqueda = new JButton("Buscar");
		btnRealizarBusqueda.setPreferredSize(new Dimension(0, 30));
		btnRealizarBusqueda.setBounds(498, 0, 123, 29);
		GridBagConstraints gbc_btnRealizarBusqueda = new GridBagConstraints();
		gbc_btnRealizarBusqueda.insets = new Insets(0, 0, 5, 5);
		gbc_btnRealizarBusqueda.gridx = 4;
		gbc_btnRealizarBusqueda.gridy = 0;
		gbc_btnRealizarBusqueda.fill = GridBagConstraints.BOTH;
		panelBusqueda.add(btnRealizarBusqueda, gbc_btnRealizarBusqueda);

		JLabel lblCodigoReferencia = new JLabel("Codigo de referencia");
		lblCodigoReferencia.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_lblCodigoReferencia = new GridBagConstraints();
		gbc_lblCodigoReferencia.fill = GridBagConstraints.BOTH;
		gbc_lblCodigoReferencia.insets = new Insets(0, 5, 5, 5);
		gbc_lblCodigoReferencia.gridx = 0;
		gbc_lblCodigoReferencia.gridy = 1;
		panelBusqueda.add(lblCodigoReferencia, gbc_lblCodigoReferencia);

		txtBusquedaCodigo = new JTextField();
		txtBusquedaCodigo.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_txtBusquedaCodigo = new GridBagConstraints();
		gbc_txtBusquedaCodigo.insets = new Insets(0, 0, 5, 5);
		gbc_txtBusquedaCodigo.fill = GridBagConstraints.BOTH;
		gbc_txtBusquedaCodigo.gridx = 1;
		gbc_txtBusquedaCodigo.gridy = 1;
		panelBusqueda.add(txtBusquedaCodigo, gbc_txtBusquedaCodigo);
		txtBusquedaCodigo.setColumns(10);
		TextPrompt phCodigo = new TextPrompt("Ingrese codigo de referencia", txtBusquedaCodigo);
		phCodigo.setPreferredSize(new Dimension(0, 30));
		phCodigo.setText("Ingrese codigo de referencia...");
		phCodigo.changeAlpha(0.75f);
		phCodigo.changeStyle(Font.ITALIC);

		JLabel radioTipo = new JLabel("Tipo de evento");
		radioTipo.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_radioTipo = new GridBagConstraints();
		gbc_radioTipo.fill = GridBagConstraints.BOTH;
		gbc_radioTipo.insets = new Insets(0, 0, 5, 5);
		gbc_radioTipo.gridx = 5;
		gbc_radioTipo.gridy = 2;

		JLabel lblCiudadDestino = new JLabel("Ciudad destino");
		lblCiudadDestino.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_lblCiudadDestino = new GridBagConstraints();
		gbc_lblCiudadDestino.fill = GridBagConstraints.BOTH;
		gbc_lblCiudadDestino.insets = new Insets(0, 5, 5, 5);
		gbc_lblCiudadDestino.gridx = 0;
		gbc_lblCiudadDestino.gridy = 2;
		panelBusqueda.add(lblCiudadDestino, gbc_lblCiudadDestino);

		txtCiudadDestino = new JTextField();
		GridBagConstraints gbc_txtCiudadDestino = new GridBagConstraints();
		gbc_txtCiudadDestino.insets = new Insets(0, 0, 5, 5);
		gbc_txtCiudadDestino.fill = GridBagConstraints.BOTH;
		gbc_txtCiudadDestino.gridx = 1;
		gbc_txtCiudadDestino.gridy = 2;
		panelBusqueda.add(txtCiudadDestino, gbc_txtCiudadDestino);
		txtCiudadDestino.setColumns(10);
		TextPrompt phCiudadDestino = new TextPrompt("Ingrese ciudad destino...", txtCiudadDestino);
		phCiudadDestino.setText("Ingrese ciudad destino...");
		phCiudadDestino.changeAlpha(0.75f);
		phCiudadDestino.changeStyle(Font.ITALIC);

		JLabel lblPaisDestino = new JLabel("Pais destino");
		lblPaisDestino.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_lblPaisDestino = new GridBagConstraints();
		gbc_lblPaisDestino.fill = GridBagConstraints.BOTH;
		gbc_lblPaisDestino.insets = new Insets(0, 0, 5, 5);
		gbc_lblPaisDestino.gridx = 3;
		gbc_lblPaisDestino.gridy = 2;
		panelBusqueda.add(lblPaisDestino, gbc_lblPaisDestino);

		cbPaisDestino = new JComboBox<String>();
		cbPaisDestino.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_cbPaisDestino = new GridBagConstraints();
		gbc_cbPaisDestino.insets = new Insets(0, 0, 5, 5);
		gbc_cbPaisDestino.fill = GridBagConstraints.BOTH;
		gbc_cbPaisDestino.gridx = 4;
		gbc_cbPaisDestino.gridy = 2;
		panelBusqueda.add(cbPaisDestino, gbc_cbPaisDestino);

		JLabel lblProvinciaDestino = new JLabel("Provincia destino");
		lblProvinciaDestino.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_lblProvinciaDestino = new GridBagConstraints();
		gbc_lblProvinciaDestino.fill = GridBagConstraints.BOTH;
		gbc_lblProvinciaDestino.insets = new Insets(0, 5, 0, 5);
		gbc_lblProvinciaDestino.gridx = 0;
		gbc_lblProvinciaDestino.gridy = 3;
		panelBusqueda.add(lblProvinciaDestino, gbc_lblProvinciaDestino);

		txtProvinciaDestino = new JTextField();
		GridBagConstraints gbc_txtProvinciaDestino = new GridBagConstraints();
		gbc_txtProvinciaDestino.insets = new Insets(0, 0, 5, 5);
		gbc_txtProvinciaDestino.fill = GridBagConstraints.BOTH;
		gbc_txtProvinciaDestino.gridx = 1;
		gbc_txtProvinciaDestino.gridy = 3;
		panelBusqueda.add(txtProvinciaDestino, gbc_txtProvinciaDestino);
		txtProvinciaDestino.setColumns(10);
		TextPrompt phProvinciaDestino = new TextPrompt("Ingrese provincia destino...", txtProvinciaDestino);
		phProvinciaDestino.setText("Ingrese provincia destino...");
		phProvinciaDestino.changeAlpha(0.75f);
		phProvinciaDestino.changeStyle(Font.ITALIC);

		JLabel lblContinenteDestino = new JLabel("Continente destino");
		lblContinenteDestino.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_lblContinenteDestino = new GridBagConstraints();
		gbc_lblContinenteDestino.fill = GridBagConstraints.BOTH;
		gbc_lblContinenteDestino.insets = new Insets(0, 0, 0, 5);
		gbc_lblContinenteDestino.gridx = 3;
		gbc_lblContinenteDestino.gridy = 3;
		panelBusqueda.add(lblContinenteDestino, gbc_lblContinenteDestino);

		cbContinenteDestino = new JComboBox<String>();
		cbContinenteDestino.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_cbContinenteDestino = new GridBagConstraints();
		gbc_cbContinenteDestino.fill = GridBagConstraints.BOTH;
		gbc_cbContinenteDestino.insets = new Insets(0, 0, 5, 5);
		gbc_cbContinenteDestino.gridx = 4;
		gbc_cbContinenteDestino.gridy = 3;
		panelBusqueda.add(cbContinenteDestino, gbc_cbContinenteDestino);

		panelSur = new JPanel();
		panelSur.setSize(922, 436);
		panelSur.setLocation(10, 247);
		panelSur.setLayout(null);
		GridBagConstraints gbc_panelSur = new GridBagConstraints();
		gbc_panelSur.fill = GridBagConstraints.BOTH;
		gbc_panelSur.gridx = 0;
		gbc_panelSur.gridy = 2;
		panel.add(panelSur, gbc_panelSur);

		JScrollPane spViajes = new JScrollPane();
		modelViajes = new DefaultTableModel(null, nombreColumnas) {
			private static final long serialVersionUID = 1L;

			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		spViajes.setBounds(0, 0, 776, 209);
		panelSur.add(spViajes);
		tablaViajes = new JTable(modelViajes);
		tablaViajes.getTableHeader().setReorderingAllowed(false);
		spViajes.setViewportView(tablaViajes);

		JScrollPane spViajesAsignados = new JScrollPane();
		modelViajesAsignados = new DefaultTableModel(null, nombreColumnas) {
			private static final long serialVersionUID = 1L;

			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		spViajesAsignados.setBounds(0, 220, 776, 216);
		panelSur.add(spViajesAsignados);
		tablaViajesAsignados = new JTable(modelViajesAsignados);
		tablaViajesAsignados.getTableHeader().setReorderingAllowed(false);
		spViajesAsignados.setViewportView(tablaViajesAsignados);
		
		btnAsignarOferta = new JButton("Asignar oferta");
		btnAsignarOferta.setBounds(786, 11, 126, 30);
		panelSur.add(btnAsignarOferta);
		
		btnDesasignarOferta = new JButton("Desasignar oferta");
		btnDesasignarOferta.setBounds(786, 229, 126, 30);
		panelSur.add(btnDesasignarOferta);

		btnConfirmar = new JButton("Confirmar");
		btnConfirmar.setBounds(806, 694, 126, 30);
		panel.add(btnConfirmar);

		this.frmGestionarOferta.setVisible(false);
	}

	public void mostrarVentana() {
		this.frmGestionarOferta.setVisible(true);
	}

	public JTextField getTxtTituloOferta() {
		return txtOferta;
	}

	public JTextField getTxtDescuentoOferta() {
		return txtDescuento;
	}

	public JButton getBtnConfirmar() {
		return btnConfirmar;
	}

	public void cerrar() {
		this.txtDescuento.setText(null);
		this.txtOferta.setText(null);
		this.txtBusquedaCodigo.setText(null);
		this.txtCiudadDestino.setText(null);
		this.cbPaisDestino.setSelectedIndex(0);
		this.cbContinenteDestino.setSelectedIndex(0);
		this.frmGestionarOferta.dispose();
	}

	public boolean verificarCampo() {
		return true;
	}

	public JTable getTablaViajes() {
		return tablaViajes;
	}

	public DefaultTableModel getModelViajes() {
		return modelViajes;
	}

	public String[] getColumnasDeViajes() {
		return nombreColumnas;
	}

	public JButton getBtnBuscarViaje() {
		return btnRealizarBusqueda;
	}
	
	public JButton getBtnAsignarOferta() {
		return btnAsignarOferta;
	}
	
	public JButton getBtnDesasignarOferta() {
		return btnDesasignarOferta;
	}
	
	public JTextField getTxtCiudad() {
		return txtCiudadDestino;
	}
	
	public JTextField getTxtProvincia() {
		return txtProvinciaDestino;
	}
	
	public JComboBox<String> getCBPais() {
		return cbPaisDestino;
	}
	
	public JComboBox<String> getCBContinente() {
		return cbContinenteDestino;
	}
	
	public DefaultTableModel getModelViajesAsignados() {
		return modelViajesAsignados;
	}
	
	public JTable getTablaViajesAsignados() {
		return tablaViajesAsignados;
	}
}
