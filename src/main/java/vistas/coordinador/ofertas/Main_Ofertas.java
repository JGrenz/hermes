package vistas.coordinador.ofertas;

import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;

import vistas.TextPrompt;
import javax.swing.ImageIcon;

public class Main_Ofertas {

	private static Main_Ofertas INSTANCE;
	private JPanel frmMainOfertas;
	private JTable tablaOfertas;
	private DefaultTableModel modelOfertas;
	private String[] nombreColumnas = { "Codigo refencia", "Titulo", "% de descuento", "Fecha de creación", "Fecha de vencimiento", "Estado" };
	private JButton btnAgregar, btnHabInhab, btnEditar, btnRealizarBusqueda, btnAsignarOferta;
	private JPanel panelBusqueda, panelNorte, panelSur;
	private JRadioButton radioHabilitado, radioInhabilitado, radioTodos;
	private ButtonGroup filtro;
	private JTextField txtTitulo, txtCodigo;

	public static Main_Ofertas getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new Main_Ofertas();
		}
		return INSTANCE;
	}

	public Main_Ofertas() {

		frmMainOfertas = new JPanel();

		filtro = new ButtonGroup();
		
		GridBagLayout gbl_frmMainOfertas = new GridBagLayout();
		gbl_frmMainOfertas.columnWidths = new int[] { 1204, 0 };
		gbl_frmMainOfertas.rowHeights = new int[] { 69, 233, 427, 0 };
		gbl_frmMainOfertas.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gbl_frmMainOfertas.rowWeights = new double[] { 0.0, 0.0, 1.0, Double.MIN_VALUE };
		frmMainOfertas.setLayout(gbl_frmMainOfertas);

		panelNorte = new JPanel();
		GridBagConstraints gbc_panelNorte = new GridBagConstraints();
		gbc_panelNorte.fill = GridBagConstraints.BOTH;
		gbc_panelNorte.insets = new Insets(0, 0, 5, 0);
		gbc_panelNorte.gridx = 0;
		gbc_panelNorte.gridy = 0;
		frmMainOfertas.add(panelNorte, gbc_panelNorte);

		btnAgregar = new JButton("Crear");
		btnAgregar.setIcon(new ImageIcon(Main_Ofertas.class.getResource("/imagenes/iconos/ofertaAgregar.png")));
		panelNorte.add(btnAgregar);
		
		btnAsignarOferta = new JButton("Asignar oferta");
		btnAsignarOferta.setIcon(new ImageIcon(Main_Ofertas.class.getResource("/imagenes/iconos/ofertaAsignar.png")));
		panelNorte.add(btnAsignarOferta);

		btnEditar = new JButton("Editar");
		btnEditar.setIcon(new ImageIcon(Main_Ofertas.class.getResource("/imagenes/iconos/ofertaEditar.png")));
		panelNorte.add(btnEditar);

		btnHabInhab = new JButton("Habilitar / Inhabilitar");
		btnHabInhab.setIcon(new ImageIcon(Main_Ofertas.class.getResource("/imagenes/iconos/ofertaHabInhab.png")));
		panelNorte.add(btnHabInhab);

		panelBusqueda = new JPanel();
		panelBusqueda.setBounds(10, 51, 667, 225);
		GridBagConstraints gbc_panelBusqueda = new GridBagConstraints();
		gbc_panelBusqueda.fill = GridBagConstraints.BOTH;
		gbc_panelBusqueda.insets = new Insets(0, 0, 5, 0);
		gbc_panelBusqueda.gridx = 0;
		gbc_panelBusqueda.gridy = 1;
		frmMainOfertas.add(panelBusqueda, gbc_panelBusqueda);
		GridBagLayout gbl_panelBusqueda = new GridBagLayout();
		gbl_panelBusqueda.columnWidths = new int[] { 127, 58, 157, 130, 130, 0 };
		gbl_panelBusqueda.rowHeights = new int[] { 30, 33, 30, 30, 31, 25, 0 };
		gbl_panelBusqueda.columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		gbl_panelBusqueda.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		panelBusqueda.setLayout(gbl_panelBusqueda);

		JLabel lblFiltrarPor = new JLabel("Filtrar por:");
		lblFiltrarPor.setPreferredSize(new Dimension(0, 30));
		lblFiltrarPor.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_lblFiltrarPor = new GridBagConstraints();
		gbc_lblFiltrarPor.fill = GridBagConstraints.BOTH;
		gbc_lblFiltrarPor.insets = new Insets(0, 0, 5, 5);
		gbc_lblFiltrarPor.gridx = 0;
		gbc_lblFiltrarPor.gridy = 0;
		panelBusqueda.add(lblFiltrarPor, gbc_lblFiltrarPor);

		JLabel lblBusquedaPor = new JLabel("Búsqueda por:");
		lblBusquedaPor.setPreferredSize(new Dimension(0, 30));
		lblBusquedaPor.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_lblBusquedaPor = new GridBagConstraints();
		gbc_lblBusquedaPor.fill = GridBagConstraints.BOTH;
		gbc_lblBusquedaPor.insets = new Insets(0, 0, 5, 5);
		gbc_lblBusquedaPor.gridx = 2;
		gbc_lblBusquedaPor.gridy = 0;
		panelBusqueda.add(lblBusquedaPor, gbc_lblBusquedaPor);

		btnRealizarBusqueda = new JButton("Buscar");
		btnRealizarBusqueda.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_btnRealizarBusqueda = new GridBagConstraints();
		gbc_btnRealizarBusqueda.fill = GridBagConstraints.BOTH;
		gbc_btnRealizarBusqueda.insets = new Insets(0, 0, 5, 5);
		gbc_btnRealizarBusqueda.gridx = 3;
		gbc_btnRealizarBusqueda.gridy = 0;
		panelBusqueda.add(btnRealizarBusqueda, gbc_btnRealizarBusqueda);

		radioTodos = new JRadioButton("Todos");
		radioTodos.setSelected(true);
		radioTodos.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_radioTodos = new GridBagConstraints();
		gbc_radioTodos.fill = GridBagConstraints.BOTH;
		gbc_radioTodos.insets = new Insets(0, 0, 5, 5);
		gbc_radioTodos.gridx = 0;
		gbc_radioTodos.gridy = 1;
		panelBusqueda.add(radioTodos, gbc_radioTodos);
		filtro.add(radioTodos);

		JLabel lblTitulo = new JLabel("Titulo");
		lblTitulo.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_lblTitulo = new GridBagConstraints();
		gbc_lblTitulo.fill = GridBagConstraints.BOTH;
		gbc_lblTitulo.insets = new Insets(0, 0, 5, 5);
		gbc_lblTitulo.gridx = 2;
		gbc_lblTitulo.gridy = 1;
		panelBusqueda.add(lblTitulo, gbc_lblTitulo);

		txtTitulo = new JTextField();
		txtTitulo.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_txtTitulo = new GridBagConstraints();
		gbc_txtTitulo.fill = GridBagConstraints.BOTH;
		gbc_txtTitulo.insets = new Insets(0, 0, 5, 0);
		gbc_txtTitulo.gridwidth = 2;
		gbc_txtTitulo.gridx = 3;
		gbc_txtTitulo.gridy = 1;
		panelBusqueda.add(txtTitulo, gbc_txtTitulo);
		txtTitulo.setColumns(10);
		TextPrompt phTitulo = new TextPrompt("Ingrese titulo...", txtTitulo);
		phTitulo.setText("Ingrese titulo...");
		phTitulo.changeAlpha(0.75f);
		phTitulo.changeStyle(Font.ITALIC);

		radioHabilitado = new JRadioButton("Habilitado");
		radioHabilitado.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_radioHabilitado = new GridBagConstraints();
		gbc_radioHabilitado.fill = GridBagConstraints.BOTH;
		gbc_radioHabilitado.insets = new Insets(0, 0, 5, 5);
		gbc_radioHabilitado.gridx = 0;
		gbc_radioHabilitado.gridy = 2;
		panelBusqueda.add(radioHabilitado, gbc_radioHabilitado);
		filtro.add(radioHabilitado);

		JLabel lblCodigoReferencia = new JLabel("Codigo de referencia");
		lblCodigoReferencia.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_radioCodigoReferencia = new GridBagConstraints();
		gbc_radioCodigoReferencia.fill = GridBagConstraints.BOTH;
		gbc_radioCodigoReferencia.insets = new Insets(0, 0, 5, 5);
		gbc_radioCodigoReferencia.gridx = 2;
		gbc_radioCodigoReferencia.gridy = 2;
		panelBusqueda.add(lblCodigoReferencia, gbc_radioCodigoReferencia);

		txtCodigo = new JTextField();
		txtCodigo.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_txtCodigo = new GridBagConstraints();
		gbc_txtCodigo.fill = GridBagConstraints.BOTH;
		gbc_txtCodigo.insets = new Insets(0, 0, 5, 0);
		gbc_txtCodigo.gridwidth = 2;
		gbc_txtCodigo.gridx = 3;
		gbc_txtCodigo.gridy = 2;
		panelBusqueda.add(txtCodigo, gbc_txtCodigo);
		txtCodigo.setColumns(10);
		TextPrompt phCodigo = new TextPrompt("Ingrese codigo de referencia", txtCodigo);
		phCodigo.setText("Ingrese codigo de referencia...");
		phCodigo.changeAlpha(0.75f);
		phCodigo.changeStyle(Font.ITALIC);

		radioInhabilitado = new JRadioButton("Inhabilitado");
		radioInhabilitado.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_radioInhabilitado = new GridBagConstraints();
		gbc_radioInhabilitado.fill = GridBagConstraints.BOTH;
		gbc_radioInhabilitado.insets = new Insets(0, 0, 5, 5);
		gbc_radioInhabilitado.gridx = 0;
		gbc_radioInhabilitado.gridy = 3;
		panelBusqueda.add(radioInhabilitado, gbc_radioInhabilitado);
		radioInhabilitado.setSelected(true);
		filtro.add(radioInhabilitado);

		panelSur = new JPanel();
		GridBagConstraints gbc_panelSur = new GridBagConstraints();
		gbc_panelSur.fill = GridBagConstraints.BOTH;
		gbc_panelSur.gridx = 0;
		gbc_panelSur.gridy = 2;
		frmMainOfertas.add(panelSur, gbc_panelSur);
		panelSur.setLayout(new CardLayout(0, 0));

		JScrollPane spOfertas = new JScrollPane();
		panelSur.add(spOfertas, "name_233556751073910");
		modelOfertas = new DefaultTableModel(null, nombreColumnas) {
			private static final long serialVersionUID = 1L;

			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		tablaOfertas = new JTable(modelOfertas);
		tablaOfertas.getTableHeader().setReorderingAllowed(false);
		spOfertas.setViewportView(tablaOfertas);

		this.frmMainOfertas.setVisible(false);
	}

	public void mostrarVentana() {
		this.frmMainOfertas.setVisible(true);
	}

	public JButton getBtnAgregar() {
		return btnAgregar;
	}
	
	public JButton getBtnAsignar() {
		return btnAsignarOferta;
	}

	public JButton getBtnEditar() {
		return btnEditar;
	}
	
	public JButton getBtnHabInhab() {
		return btnHabInhab;
	}

	public JButton getBtnRealizarBusqueda() {
		return btnRealizarBusqueda;
	}

	public DefaultTableModel getModelOfertas() {
		return modelOfertas;
	}

	public JTable getTablaOfertas() {
		return tablaOfertas;
	}

	public String[] getNombreColumnas() {
		return nombreColumnas;
	}
	
	public JTextField getTxtTitulo() {
		return txtTitulo;
	}

	public JTextField getTxtCodigo() {
		return txtCodigo;
	}

	public JRadioButton getRadioTodos() {
		return radioTodos;
	}

	public JRadioButton getRadioHabilitado() {
		return radioHabilitado;
	}

	public JRadioButton getRadioInhabilitado() {
		return radioInhabilitado;
	}

	public JPanel getPanel() {
		return frmMainOfertas;
	}
}
