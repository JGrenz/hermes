package vistas.coordinador.transporte;

import java.awt.Dialog.ModalityType;
import java.awt.Font;
import java.awt.Toolkit;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import vistas.TextPrompt;

public class Ventana_AgregarTransporte {
	
	private static Ventana_AgregarTransporte INSTANCE;
	private JDialog frmAgregarTransporte;
	private JPanel contentPane;
	private JTextField txtTransporte;
	private JButton btnConfirmar;

	public static Ventana_AgregarTransporte getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new Ventana_AgregarTransporte();
		}
		return INSTANCE;
	}

	private Ventana_AgregarTransporte() {
		
		frmAgregarTransporte = new JDialog();
		frmAgregarTransporte.setIconImage(Toolkit.getDefaultToolkit().getImage(Ventana_AgregarTransporte.class.getResource("/imagenes/hermesicono.png")));
		frmAgregarTransporte.setModalityType(ModalityType.APPLICATION_MODAL);
		frmAgregarTransporte.setResizable(false);
		frmAgregarTransporte.setTitle("Hermes - Agregar medio de transporte");
		frmAgregarTransporte.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmAgregarTransporte.setBounds(100, 100, 436, 150);
		frmAgregarTransporte.setLocationRelativeTo(null);
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		frmAgregarTransporte.setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 430, 121);
		contentPane.add(panel);
		panel.setLayout(null);

		JLabel lblTransporte = new JLabel("Tipo de transporte:");
		lblTransporte.setBounds(10, 15, 133, 30);
		panel.add(lblTransporte);

		txtTransporte = new JTextField();
		txtTransporte.setBounds(192, 15, 228, 30);
		TextPrompt phNombre = new TextPrompt("Ingrese nombre...", txtTransporte);
		phNombre.changeAlpha(0.75f);
		phNombre.changeStyle(Font.ITALIC);
		panel.add(txtTransporte);
		txtTransporte.setColumns(10);

		btnConfirmar = new JButton("Confirmar");
		btnConfirmar.setBounds(294, 87, 126, 23);
		panel.add(btnConfirmar);

		this.frmAgregarTransporte.setVisible(false);
	}

	public void mostrarVentana() {
		this.frmAgregarTransporte.setVisible(true);
	}

	public JTextField getTxtTransporte() {
		return txtTransporte;
	}

	public JButton getBtnConfirmar() {
		return btnConfirmar;
	}

	public void cerrar() {
		this.txtTransporte.setText(null);
		this.frmAgregarTransporte.dispose();
	}
	

}
