package vistas.coordinador.transporte;

import java.awt.Dialog.ModalityType;
import java.awt.Toolkit;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class Ventana_EditarTransporte {
	
	private static Ventana_EditarTransporte INSTANCE;
	private JDialog frmEditarTransporte;
	private JPanel contentPane;
	private JTextField txtTransporte;
	private JButton btnConfirmar;

	public static Ventana_EditarTransporte getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new Ventana_EditarTransporte();
		}
		return INSTANCE;
	}

	private Ventana_EditarTransporte() {
		
		frmEditarTransporte = new JDialog();
		frmEditarTransporte.setIconImage(Toolkit.getDefaultToolkit().getImage(Ventana_EditarTransporte.class.getResource("/imagenes/hermesicono.png")));
		frmEditarTransporte.setModalityType(ModalityType.APPLICATION_MODAL);
		frmEditarTransporte.setResizable(false);
		frmEditarTransporte.setTitle("Hermes - Editar medio de transporte");
		frmEditarTransporte.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmEditarTransporte.setBounds(100, 100, 436, 150);
		frmEditarTransporte.setLocationRelativeTo(null);
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		frmEditarTransporte.setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 430, 121);
		contentPane.add(panel);
		panel.setLayout(null);

		JLabel lblTransporte = new JLabel("Tipo de transporte:");
		lblTransporte.setBounds(10, 15, 133, 30);
		panel.add(lblTransporte);

		txtTransporte = new JTextField();
		txtTransporte.setBounds(192, 15, 228, 30);
		panel.add(txtTransporte);
		txtTransporte.setColumns(10);

		btnConfirmar = new JButton("Confirmar");
		btnConfirmar.setBounds(294, 87, 126, 23);
		panel.add(btnConfirmar);

		this.frmEditarTransporte.setVisible(false);
	}

	public void mostrarVentana() {
		this.frmEditarTransporte.setVisible(true);
	}

	public JTextField getTxtTransporte() {
		return txtTransporte;
	}

	public JButton getBtnConfirmar() {
		return btnConfirmar;
	}

	public void cerrar() {
		this.frmEditarTransporte.dispose();
	}
	

}
