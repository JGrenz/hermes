package vistas.coordinador.transporte;

import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;

import vistas.TextPrompt;

public class Main_Transporte {

	private static Main_Transporte INSTANCE;
	private JPanel frmMainTransporte;
	private JTable tablaTransporte;
	private DefaultTableModel modelTransporte;
	private String[] nombreColumnas = { "Medios de transporte", "Estado" };
	private JButton btnAgregar, btnEditar, btnEstado;
	private JTextField txtNombre;
	private JButton btnBuscar;
	private JRadioButton radioHabilitados, radioInhabilitados, radioTodos;
	private ButtonGroup filtroBusqueda;
	private JLabel lblNombre;
	private JPanel panelNorte;
	private JPanel panelSur;

	public static Main_Transporte getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new Main_Transporte();
		}
		return INSTANCE;
	}

	public Main_Transporte() {

		frmMainTransporte = new JPanel();

		filtroBusqueda = new ButtonGroup();
		
		GridBagLayout gbl_frmMainTransporte = new GridBagLayout();
		gbl_frmMainTransporte.columnWidths = new int[] { 850, 0 };
		gbl_frmMainTransporte.rowHeights = new int[] { 69, 133, 427, 0 };
		gbl_frmMainTransporte.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gbl_frmMainTransporte.rowWeights = new double[] { 0.0, 0.0, 1.0, Double.MIN_VALUE };
		frmMainTransporte.setLayout(gbl_frmMainTransporte);

		panelNorte = new JPanel();
		GridBagConstraints gbc_panelNorte = new GridBagConstraints();
		gbc_panelNorte.fill = GridBagConstraints.BOTH;
		gbc_panelNorte.insets = new Insets(0, 0, 5, 0);
		gbc_panelNorte.gridx = 0;
		gbc_panelNorte.gridy = 0;
		frmMainTransporte.add(panelNorte, gbc_panelNorte);

		btnAgregar = new JButton("Agregar");
		btnAgregar.setIcon(new ImageIcon(Main_Transporte.class.getResource("/imagenes/iconos/transporteAgregar.png")));
		panelNorte.add(btnAgregar);

		btnEstado = new JButton("Habilitar / Inhabilitar");
		btnEstado.setIcon(new ImageIcon(Main_Transporte.class.getResource("/imagenes/iconos/transporteHabInhab.png")));
		panelNorte.add(btnEstado);

		btnEditar = new JButton("Editar");
		btnEditar.setIcon(new ImageIcon(Main_Transporte.class.getResource("/imagenes/iconos/transporteEditar.png")));
		panelNorte.add(btnEditar);

		JPanel panelBusqueda = new JPanel();
		panelBusqueda.setBounds(10, 51, 478, 154);
		GridBagConstraints gbc_panelBusqueda = new GridBagConstraints();
		gbc_panelBusqueda.fill = GridBagConstraints.BOTH;
		gbc_panelBusqueda.insets = new Insets(0, 0, 5, 0);
		gbc_panelBusqueda.gridx = 0;
		gbc_panelBusqueda.gridy = 1;
		frmMainTransporte.add(panelBusqueda, gbc_panelBusqueda);
		GridBagLayout gbl_panelBusqueda = new GridBagLayout();
		gbl_panelBusqueda.columnWidths = new int[] { 127, 46, 127, 158, 0 };
		gbl_panelBusqueda.rowHeights = new int[] { 30, 25, 25, 25, 0 };
		gbl_panelBusqueda.columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		gbl_panelBusqueda.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		panelBusqueda.setLayout(gbl_panelBusqueda);

		JLabel lblFiltrarPor = new JLabel("Filtrar por:");
		lblFiltrarPor.setPreferredSize(new Dimension(0, 30));
		lblFiltrarPor.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_lblFiltrarPor = new GridBagConstraints();
		gbc_lblFiltrarPor.fill = GridBagConstraints.BOTH;
		gbc_lblFiltrarPor.insets = new Insets(0, 0, 5, 5);
		gbc_lblFiltrarPor.gridx = 0;
		gbc_lblFiltrarPor.gridy = 0;
		panelBusqueda.add(lblFiltrarPor, gbc_lblFiltrarPor);

		btnBuscar = new JButton("Buscar");
		btnBuscar.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_btnBuscar = new GridBagConstraints();
		gbc_btnBuscar.fill = GridBagConstraints.BOTH;
		gbc_btnBuscar.insets = new Insets(0, 0, 5, 0);
		gbc_btnBuscar.gridx = 2;
		gbc_btnBuscar.gridy = 0;
		panelBusqueda.add(btnBuscar, gbc_btnBuscar);

		radioTodos = new JRadioButton("Todos");
		radioTodos.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_radioTodos = new GridBagConstraints();
		gbc_radioTodos.fill = GridBagConstraints.BOTH;
		gbc_radioTodos.insets = new Insets(0, 0, 5, 5);
		gbc_radioTodos.gridx = 0;
		gbc_radioTodos.gridy = 1;
		panelBusqueda.add(radioTodos, gbc_radioTodos);
		radioTodos.setVisible(true);
		filtroBusqueda.add(radioTodos);

		JLabel lblNombre = new JLabel("Buscar por nombre:");
		lblNombre.setPreferredSize(new Dimension(0, 30));
		lblNombre.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_lblNombre = new GridBagConstraints();
		gbc_lblNombre.fill = GridBagConstraints.BOTH;
		gbc_lblNombre.insets = new Insets(0, 0, 5, 5);
		gbc_lblNombre.gridx = 2;
		gbc_lblNombre.gridy = 1;
		panelBusqueda.add(lblNombre, gbc_lblNombre);
		lblNombre.setVisible(true);

		txtNombre = new JTextField();
		txtNombre.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_txtBarradebusqueda = new GridBagConstraints();
		gbc_txtBarradebusqueda.fill = GridBagConstraints.BOTH;
		gbc_txtBarradebusqueda.insets = new Insets(0, 0, 5, 0);
		gbc_txtBarradebusqueda.gridx = 3;
		gbc_txtBarradebusqueda.gridy = 1;
		panelBusqueda.add(txtNombre, gbc_txtBarradebusqueda);
		TextPrompt phNombre = new TextPrompt("Ingrese nombre...", txtNombre);
		phNombre.changeAlpha(0.75f);
		phNombre.changeStyle(Font.ITALIC);
		txtNombre.setColumns(10);
		txtNombre.setVisible(true);

		radioHabilitados = new JRadioButton("Habilitados");
		radioHabilitados.setPreferredSize(new Dimension(0, 30));
		radioHabilitados.setSelected(true);
		GridBagConstraints gbc_radioHabilitados = new GridBagConstraints();
		gbc_radioHabilitados.fill = GridBagConstraints.BOTH;
		gbc_radioHabilitados.insets = new Insets(0, 0, 5, 5);
		gbc_radioHabilitados.gridx = 0;
		gbc_radioHabilitados.gridy = 2;
		panelBusqueda.add(radioHabilitados, gbc_radioHabilitados);
		radioHabilitados.setVisible(true);
		filtroBusqueda.add(radioHabilitados);

		radioInhabilitados = new JRadioButton("Inhabilitados");
		radioInhabilitados.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_radioInhabilitados = new GridBagConstraints();
		gbc_radioInhabilitados.fill = GridBagConstraints.BOTH;
		gbc_radioInhabilitados.insets = new Insets(0, 0, 0, 5);
		gbc_radioInhabilitados.gridx = 0;
		gbc_radioInhabilitados.gridy = 3;
		panelBusqueda.add(radioInhabilitados, gbc_radioInhabilitados);
		radioInhabilitados.setVisible(true);
		filtroBusqueda.add(radioInhabilitados);

		panelSur = new JPanel();
		GridBagConstraints gbc_panelSur = new GridBagConstraints();
		gbc_panelSur.fill = GridBagConstraints.BOTH;
		gbc_panelSur.gridx = 0;
		gbc_panelSur.gridy = 2;
		frmMainTransporte.add(panelSur, gbc_panelSur);
		panelSur.setLayout(new CardLayout(0, 0));

		JScrollPane spTransporte = new JScrollPane();
		panelSur.add(spTransporte, "name_97847517912846");
		modelTransporte = new DefaultTableModel(null, nombreColumnas) {
			private static final long serialVersionUID = 1L;

			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		tablaTransporte = new JTable(modelTransporte);
		tablaTransporte.getTableHeader().setReorderingAllowed(false);
		spTransporte.setViewportView(tablaTransporte);

		this.frmMainTransporte.setVisible(false);
	}

	public void mostrarVentana() {
		this.frmMainTransporte.setVisible(true);
	}

	public JButton getBtnAgregar() {
		return btnAgregar;
	}

	public JButton getBtnEstado() {
		return btnEstado;
	}

	public JButton getBtnEditar() {
		return btnEditar;
	}

	public DefaultTableModel getModelTransporte() {
		return modelTransporte;
	}

	public JTable getTablaTransporte() {
		return tablaTransporte;
	}

	public String[] getNombreColumnas() {
		return nombreColumnas;
	}

	public JButton getBtnBuscar() {
		return btnBuscar;
	}

	public JRadioButton getRdbtnTodos() {
		return radioTodos;
	}

	public JRadioButton getRdbtnHabilitados() {
		return radioHabilitados;
	}

	public JRadioButton getRdbtnInhabilitados() {
		return radioInhabilitados;
	}

	public ButtonGroup getFiltroBusqueda() {
		return filtroBusqueda;
	}

	public JLabel getLblNombre() {
		return lblNombre;
	}

	public JTextField getTxtBarradebusqueda() {
		return txtNombre;
	}

	public JTable getTabla() {
		return tablaTransporte;
	}

	public JPanel getPanel() {
		return frmMainTransporte;
	}
}