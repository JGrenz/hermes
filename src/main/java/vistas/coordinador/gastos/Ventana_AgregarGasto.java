package vistas.coordinador.gastos;

import java.awt.Dialog.ModalityType;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import com.toedter.calendar.JDateChooser;
import com.toedter.calendar.JTextFieldDateEditor;

import vistas.TextPrompt;
import vistas.ValidacionCampos;

public class Ventana_AgregarGasto {

	private static Ventana_AgregarGasto INSTANCE;
	private JDialog frmAgregarGasto;
	private JPanel contentPane;
	private JTextField txtMonto;
	private JButton btnConfirmar;
	private JComboBox<String> cbTipoGasto;
	private JDateChooser txtFechaGasto;

	public static Ventana_AgregarGasto getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new Ventana_AgregarGasto();
		}
		return INSTANCE;
	}

	private Ventana_AgregarGasto() {

		frmAgregarGasto = new JDialog();
		frmAgregarGasto.setIconImage(Toolkit.getDefaultToolkit()
				.getImage(Ventana_AgregarGasto.class.getResource("/imagenes/hermesicono.png")));
		frmAgregarGasto.setModalityType(ModalityType.APPLICATION_MODAL);
		frmAgregarGasto.setResizable(false);
		frmAgregarGasto.setTitle("Hermes - Agregar gasto");
		frmAgregarGasto.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmAgregarGasto.setBounds(100, 100, 376, 194);
		frmAgregarGasto.setLocationRelativeTo(null);

		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		frmAgregarGasto.setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(10, 11, 350, 143);
		contentPane.add(panel);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[] { 133, 79, 0, 0 };
		gbl_panel.rowHeights = new int[] { 30, 30, 30, 0, 30, 0 };
		gbl_panel.columnWeights = new double[] { 0.0, 1.0, 1.0, Double.MIN_VALUE };
		gbl_panel.rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE };
		panel.setLayout(gbl_panel);

		JLabel lblTipoGasto = new JLabel("Tipo de gasto:");
		lblTipoGasto.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_lblTipoGasto = new GridBagConstraints();
		gbc_lblTipoGasto.fill = GridBagConstraints.BOTH;
		gbc_lblTipoGasto.insets = new Insets(5, 5, 5, 5);
		gbc_lblTipoGasto.gridx = 0;
		gbc_lblTipoGasto.gridy = 0;
		panel.add(lblTipoGasto, gbc_lblTipoGasto);

		cbTipoGasto = new JComboBox<String>();
		GridBagConstraints gbc_cbTipoGasto = new GridBagConstraints();
		gbc_cbTipoGasto.gridwidth = 2;
		gbc_cbTipoGasto.insets = new Insets(5, 0, 5, 5);
		gbc_cbTipoGasto.fill = GridBagConstraints.BOTH;
		gbc_cbTipoGasto.gridx = 1;
		gbc_cbTipoGasto.gridy = 0;
		panel.add(cbTipoGasto, gbc_cbTipoGasto);
		cbTipoGasto.addItem("SERVICIO");
		cbTipoGasto.addItem("SUELDO");
		cbTipoGasto.addItem("CONSUMOS_VARIOS");

		JLabel lblMonto = new JLabel("Monto:");
		lblMonto.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_lblMonto = new GridBagConstraints();
		gbc_lblMonto.fill = GridBagConstraints.BOTH;
		gbc_lblMonto.insets = new Insets(5, 5, 5, 5);
		gbc_lblMonto.gridx = 0;
		gbc_lblMonto.gridy = 1;
		panel.add(lblMonto, gbc_lblMonto);

		txtMonto = new JTextField();
		TextPrompt phMonto = new TextPrompt("Ingrese % de descuento...", txtMonto);
		phMonto.setText("Ingrese monto de gasto...");
		phMonto.changeAlpha(0.75f);
		phMonto.changeStyle(Font.ITALIC);
		txtMonto.setColumns(10);
		GridBagConstraints gbc_txtMonto = new GridBagConstraints();
		gbc_txtMonto.gridwidth = 2;
		gbc_txtMonto.insets = new Insets(0, 0, 5, 5);
		gbc_txtMonto.fill = GridBagConstraints.BOTH;
		gbc_txtMonto.gridx = 1;
		gbc_txtMonto.gridy = 1;
		panel.add(txtMonto, gbc_txtMonto);

		JLabel lblFechaDeGasto = new JLabel("Fecha de gasto:");
		lblFechaDeGasto.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_lblFechaDeGasto = new GridBagConstraints();
		gbc_lblFechaDeGasto.fill = GridBagConstraints.BOTH;
		gbc_lblFechaDeGasto.insets = new Insets(0, 0, 5, 5);
		gbc_lblFechaDeGasto.gridx = 0;
		gbc_lblFechaDeGasto.gridy = 2;
		panel.add(lblFechaDeGasto, gbc_lblFechaDeGasto);

		txtFechaGasto = new JDateChooser(new Date());
		txtFechaGasto.setMaxSelectableDate(new Date());
		JTextFieldDateEditor editor = (JTextFieldDateEditor) txtFechaGasto.getDateEditor();
		editor.setEditable(false);
		GridBagConstraints gbc_txtFechaGasto = new GridBagConstraints();
		gbc_txtFechaGasto.gridwidth = 2;
		gbc_txtFechaGasto.fill = GridBagConstraints.BOTH;
		gbc_txtFechaGasto.insets = new Insets(0, 0, 5, 5);
		gbc_txtFechaGasto.gridx = 1;
		gbc_txtFechaGasto.gridy = 2;
		panel.add(txtFechaGasto, gbc_txtFechaGasto);

		btnConfirmar = new JButton("Confirmar");
		GridBagConstraints gbc_btnConfirmar = new GridBagConstraints();
		gbc_btnConfirmar.fill = GridBagConstraints.BOTH;
		gbc_btnConfirmar.gridx = 2;
		gbc_btnConfirmar.gridy = 4;
		panel.add(btnConfirmar, gbc_btnConfirmar);

		this.frmAgregarGasto.setVisible(false);
	}

	public void mostrarVentana() {
		this.frmAgregarGasto.setVisible(true);
	}

	public JTextField getTxtMonto() {
		return txtMonto;
	}

	public JComboBox<String> getCBTipo() {
		return cbTipoGasto;
	}

	public JButton getBtnConfirmar() {
		return btnConfirmar;
	}

	public void cerrar() {
		this.txtMonto.setText(null);
		this.frmAgregarGasto.dispose();
	}

	public boolean verificarCampo() {
		ValidacionCampos validador = new ValidacionCampos();
		if (txtMonto.getText().equals("")) {
			JOptionPane.showMessageDialog(null, "No pueden quedar campos vacíos.");
			return false;
		} else if (!validador.esMonto(txtMonto.getText())) {
			return false;
		}
		return true;
	}

	public LocalDate getTxtFechaPago() {
		Date date = txtFechaGasto.getDate();
		return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
	}

	public JDateChooser getChooserFechaPago() {
		return txtFechaGasto;
	}
}
