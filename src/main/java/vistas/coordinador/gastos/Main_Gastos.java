package vistas.coordinador.gastos;

import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.Date;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;

import com.toedter.calendar.JDateChooser;

import vistas.TextPrompt;

public class Main_Gastos {

	private static Main_Gastos INSTANCE;
	private JPanel frmMainGastos, panelBusqueda, panelABM, panelTabla;
	private JTable tablaGastos;
	private DefaultTableModel modelGastos;
	private String[] nombreColumnas = { "Código de referencia", "Monto", "Tipo de gasto", "Estado", "Local", "Fecha de Gasto", "Fecha de creación" };
	private JButton btnAgregar, btnInhabilitar, btnRealizarBusqueda, btnEditar;
	private JRadioButton radioHabilitado, radioInhabilitado, radioTodos;
	private ButtonGroup filtro;
	private JTextField txtCodigoRef;
	private JComboBox<String> cbTipoGasto, cbLocal;
	private JTextField txtPrecioDesde, txtPrecioHasta;
	private JDateChooser txtFechaDesde, txtFechaHasta;

	public static Main_Gastos getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new Main_Gastos();
		}
		return INSTANCE;
	}

	public Main_Gastos() {

		frmMainGastos = new JPanel();
		GridBagLayout gbl_frmMainEventos = new GridBagLayout();
		gbl_frmMainEventos.columnWidths = new int[] { 102, 0 };
		gbl_frmMainEventos.rowHeights = new int[] { 10, 177, 59, 0 };
		gbl_frmMainEventos.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gbl_frmMainEventos.rowWeights = new double[] { 0.0, 0.0, 1.0, Double.MIN_VALUE };
		frmMainGastos.setLayout(gbl_frmMainEventos);

		filtro = new ButtonGroup();

		panelABM = new JPanel();
		GridBagConstraints gbc_panelABM = new GridBagConstraints();
		gbc_panelABM.fill = GridBagConstraints.BOTH;
		gbc_panelABM.insets = new Insets(0, 0, 5, 0);
		gbc_panelABM.gridx = 0;
		gbc_panelABM.gridy = 0;
		frmMainGastos.add(panelABM, gbc_panelABM);

		btnAgregar = new JButton("Crear");
		btnAgregar.setIcon(new ImageIcon(Main_Gastos.class.getResource("/imagenes/iconos/gastosAgregar.png")));
		panelABM.add(btnAgregar);

		btnEditar = new JButton("Editar");
		btnEditar.setIcon(new ImageIcon(Main_Gastos.class.getResource("/imagenes/iconos/gastosEditar.png")));
		panelABM.add(btnEditar);

		btnInhabilitar = new JButton("Inhabilitar");
		btnInhabilitar.setIcon(new ImageIcon(Main_Gastos.class.getResource("/imagenes/iconos/gastosCancelar.png")));
		panelABM.add(btnInhabilitar);

		panelBusqueda = new JPanel();
		panelBusqueda.setBounds(10, 51, 972, 191);
		GridBagConstraints gbc_panelBusqueda = new GridBagConstraints();
		gbc_panelBusqueda.fill = GridBagConstraints.BOTH;
		gbc_panelBusqueda.insets = new Insets(0, 0, 5, 0);
		gbc_panelBusqueda.gridx = 0;
		gbc_panelBusqueda.gridy = 1;
		frmMainGastos.add(panelBusqueda, gbc_panelBusqueda);
		GridBagLayout gbl_panelBusqueda = new GridBagLayout();
		gbl_panelBusqueda.columnWidths = new int[] { 127, 30, 157, 140, 0, 140, 30, 120, 160, 0 };
		gbl_panelBusqueda.rowHeights = new int[] { 30, 33, 30, 30, 0 };
		gbl_panelBusqueda.columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		gbl_panelBusqueda.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		panelBusqueda.setLayout(gbl_panelBusqueda);

		JLabel lblFiltrarPor = new JLabel("Filtrar por:");
		lblFiltrarPor.setPreferredSize(new Dimension(0, 30));
		lblFiltrarPor.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_lblFiltrarPor = new GridBagConstraints();
		gbc_lblFiltrarPor.fill = GridBagConstraints.BOTH;
		gbc_lblFiltrarPor.insets = new Insets(0, 0, 5, 5);
		gbc_lblFiltrarPor.gridx = 0;
		gbc_lblFiltrarPor.gridy = 0;
		panelBusqueda.add(lblFiltrarPor, gbc_lblFiltrarPor);

		JLabel lblBusquedaPor = new JLabel("Búsqueda por:");
		lblBusquedaPor.setPreferredSize(new Dimension(0, 30));
		lblBusquedaPor.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_lblBusquedaPor = new GridBagConstraints();
		gbc_lblBusquedaPor.fill = GridBagConstraints.BOTH;
		gbc_lblBusquedaPor.insets = new Insets(0, 0, 5, 5);
		gbc_lblBusquedaPor.gridx = 2;
		gbc_lblBusquedaPor.gridy = 0;
		panelBusqueda.add(lblBusquedaPor, gbc_lblBusquedaPor);

		radioTodos = new JRadioButton("Todos");
		radioTodos.setSelected(true);
		radioTodos.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_radioTodos = new GridBagConstraints();
		gbc_radioTodos.fill = GridBagConstraints.BOTH;
		gbc_radioTodos.insets = new Insets(0, 0, 5, 5);
		gbc_radioTodos.gridx = 0;
		gbc_radioTodos.gridy = 1;
		panelBusqueda.add(radioTodos, gbc_radioTodos);
		filtro.add(radioTodos);

		JLabel lblCodigo = new JLabel("Código de referencia");
		lblCodigo.setHorizontalAlignment(SwingConstants.CENTER);
		lblCodigo.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_lblCodigo = new GridBagConstraints();
		gbc_lblCodigo.fill = GridBagConstraints.BOTH;
		gbc_lblCodigo.insets = new Insets(0, 0, 5, 5);
		gbc_lblCodigo.gridx = 2;
		gbc_lblCodigo.gridy = 1;
		panelBusqueda.add(lblCodigo, gbc_lblCodigo);

		txtCodigoRef = new JTextField();
		txtCodigoRef.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_txtCodigoRef = new GridBagConstraints();
		gbc_txtCodigoRef.fill = GridBagConstraints.BOTH;
		gbc_txtCodigoRef.insets = new Insets(0, 0, 5, 5);
		gbc_txtCodigoRef.gridwidth = 3;
		gbc_txtCodigoRef.gridx = 3;
		gbc_txtCodigoRef.gridy = 1;
		panelBusqueda.add(txtCodigoRef, gbc_txtCodigoRef);
		txtCodigoRef.setColumns(10);
		TextPrompt phCodigo = new TextPrompt("Ingrese usuario...", txtCodigoRef);
		phCodigo.setText("Ingrese código de referencia...");
		phCodigo.changeAlpha(0.75f);
		phCodigo.changeStyle(Font.ITALIC);

		JLabel radioTipo = new JLabel("Tipo de gasto");
		radioTipo.setHorizontalAlignment(SwingConstants.CENTER);
		radioTipo.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_radioTipo = new GridBagConstraints();
		gbc_radioTipo.fill = GridBagConstraints.BOTH;
		gbc_radioTipo.insets = new Insets(0, 0, 5, 5);
		gbc_radioTipo.gridx = 7;
		gbc_radioTipo.gridy = 1;
		panelBusqueda.add(radioTipo, gbc_radioTipo);

		cbTipoGasto = new JComboBox<String>();
		cbTipoGasto.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_cbTipoGasto = new GridBagConstraints();
		gbc_cbTipoGasto.fill = GridBagConstraints.BOTH;
		gbc_cbTipoGasto.insets = new Insets(0, 0, 5, 0);
		gbc_cbTipoGasto.gridx = 8;
		gbc_cbTipoGasto.gridy = 1;
		panelBusqueda.add(cbTipoGasto, gbc_cbTipoGasto);
		cbTipoGasto.addItem("Seleccionar tipo");
		cbTipoGasto.addItem("Servicio");
		cbTipoGasto.addItem("Sueldo");
		cbTipoGasto.addItem("Consumos_varios");
		cbTipoGasto.addItem("Devolucion");

		radioHabilitado = new JRadioButton("Habilitado");
		radioHabilitado.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_radioHabilitado = new GridBagConstraints();
		gbc_radioHabilitado.fill = GridBagConstraints.BOTH;
		gbc_radioHabilitado.insets = new Insets(0, 0, 5, 5);
		gbc_radioHabilitado.gridx = 0;
		gbc_radioHabilitado.gridy = 2;
		panelBusqueda.add(radioHabilitado, gbc_radioHabilitado);
		filtro.add(radioHabilitado);

		JLabel lblRangoFechas = new JLabel("Rango de fechas");
		lblRangoFechas.setHorizontalAlignment(SwingConstants.CENTER);
		lblRangoFechas.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_lblRangoFechas = new GridBagConstraints();
		gbc_lblRangoFechas.fill = GridBagConstraints.BOTH;
		gbc_lblRangoFechas.insets = new Insets(0, 0, 5, 5);
		gbc_lblRangoFechas.gridx = 2;
		gbc_lblRangoFechas.gridy = 2;
		panelBusqueda.add(lblRangoFechas, gbc_lblRangoFechas);
		
		txtFechaDesde = new JDateChooser();
		GridBagConstraints gbc_txtFechaDesde = new GridBagConstraints();
		gbc_txtFechaDesde.fill = GridBagConstraints.BOTH;
		gbc_txtFechaDesde.insets = new Insets(0, 0, 5, 5);
		gbc_txtFechaDesde.gridx = 3;
		gbc_txtFechaDesde.gridy = 2;
		panelBusqueda.add(txtFechaDesde, gbc_txtFechaDesde);
		
		txtFechaHasta = new JDateChooser();
		txtFechaHasta.setMaxSelectableDate(new Date());
		GridBagConstraints gbc_txtFechaHasta = new GridBagConstraints();
		gbc_txtFechaHasta.fill = GridBagConstraints.BOTH;
		gbc_txtFechaHasta.insets = new Insets(0, 0, 5, 5);
		gbc_txtFechaHasta.gridx = 5;
		gbc_txtFechaHasta.gridy = 2;
		panelBusqueda.add(txtFechaHasta, gbc_txtFechaHasta);
		
		JLabel lblGuionFecha = new JLabel("-");
		GridBagConstraints gbc_lblGuionFecha = new GridBagConstraints();
		gbc_lblGuionFecha.fill = GridBagConstraints.BOTH;
		gbc_lblGuionFecha.insets = new Insets(0, 0, 5, 5);
		gbc_lblGuionFecha.gridx = 4;
		gbc_lblGuionFecha.gridy = 2;
		panelBusqueda.add(lblGuionFecha, gbc_lblGuionFecha);
		
		JLabel lblLocal = new JLabel("Local");
		lblLocal.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_lblLocal = new GridBagConstraints();
		gbc_lblLocal.fill = GridBagConstraints.BOTH;
		gbc_lblLocal.insets = new Insets(0, 0, 5, 5);
		gbc_lblLocal.gridx = 7;
		gbc_lblLocal.gridy = 2;
		panelBusqueda.add(lblLocal, gbc_lblLocal);
		
		cbLocal = new JComboBox<String>();
		GridBagConstraints gbc_cbLocal = new GridBagConstraints();
		gbc_cbLocal.insets = new Insets(0, 0, 5, 0);
		gbc_cbLocal.fill = GridBagConstraints.BOTH;
		gbc_cbLocal.gridx = 8;
		gbc_cbLocal.gridy = 2;
		panelBusqueda.add(cbLocal, gbc_cbLocal);

		radioInhabilitado = new JRadioButton("Inhabilitado");
		radioInhabilitado.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_radioInhabilitado = new GridBagConstraints();
		gbc_radioInhabilitado.fill = GridBagConstraints.BOTH;
		gbc_radioInhabilitado.insets = new Insets(0, 0, 0, 5);
		gbc_radioInhabilitado.gridx = 0;
		gbc_radioInhabilitado.gridy = 3;
		panelBusqueda.add(radioInhabilitado, gbc_radioInhabilitado);
		radioInhabilitado.setSelected(true);
		filtro.add(radioInhabilitado);

		JLabel lblRangoImporte = new JLabel("Rango de importe");
		lblRangoImporte.setHorizontalAlignment(SwingConstants.CENTER);
		lblRangoImporte.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_lblRangoImporte = new GridBagConstraints();
		gbc_lblRangoImporte.fill = GridBagConstraints.BOTH;
		gbc_lblRangoImporte.insets = new Insets(0, 0, 0, 5);
		gbc_lblRangoImporte.gridx = 2;
		gbc_lblRangoImporte.gridy = 3;
		panelBusqueda.add(lblRangoImporte, gbc_lblRangoImporte);

		btnRealizarBusqueda = new JButton("Buscar");
		btnRealizarBusqueda.setPreferredSize(new Dimension(0, 30));
		btnRealizarBusqueda.setBounds(498, 0, 123, 29);
		GridBagConstraints gbc_btnRealizarBusqueda = new GridBagConstraints();
		gbc_btnRealizarBusqueda.insets = new Insets(0, 0, 5, 5);
		gbc_btnRealizarBusqueda.gridx = 3;
		gbc_btnRealizarBusqueda.gridy = 0;
		gbc_btnRealizarBusqueda.fill = GridBagConstraints.BOTH;
		panelBusqueda.add(btnRealizarBusqueda, gbc_btnRealizarBusqueda);
		
		txtPrecioDesde = new JTextField();
		GridBagConstraints gbc_txtPrecioDesde = new GridBagConstraints();
		gbc_txtPrecioDesde.insets = new Insets(0, 0, 0, 5);
		gbc_txtPrecioDesde.fill = GridBagConstraints.BOTH;
		gbc_txtPrecioDesde.gridx = 3;
		gbc_txtPrecioDesde.gridy = 3;
		panelBusqueda.add(txtPrecioDesde, gbc_txtPrecioDesde);
		TextPrompt phPrecioDesde = new TextPrompt("Ingrese un precio...", txtPrecioDesde);
		phPrecioDesde.changeAlpha(0.75f);
		phPrecioDesde.changeStyle(Font.ITALIC);
		txtPrecioDesde.setColumns(10);
		
		JLabel lblGuionImporte = new JLabel("-");
		GridBagConstraints gbc_lblGuionImporte = new GridBagConstraints();
		gbc_lblGuionImporte.fill = GridBagConstraints.BOTH;
		gbc_lblGuionImporte.insets = new Insets(0, 0, 0, 5);
		gbc_lblGuionImporte.gridx = 4;
		gbc_lblGuionImporte.gridy = 3;
		panelBusqueda.add(lblGuionImporte, gbc_lblGuionImporte);
		
		txtPrecioHasta = new JTextField();
		GridBagConstraints gbc_txtPrecioHasta = new GridBagConstraints();
		gbc_txtPrecioHasta.insets = new Insets(0, 0, 0, 5);
		gbc_txtPrecioHasta.fill = GridBagConstraints.BOTH;
		gbc_txtPrecioHasta.gridx = 5;
		gbc_txtPrecioHasta.gridy = 3;
		panelBusqueda.add(txtPrecioHasta, gbc_txtPrecioHasta);
		TextPrompt phPrecioHasta = new TextPrompt("Ingrese un precio...", txtPrecioHasta);
		phPrecioHasta.changeAlpha(0.75f);
		phPrecioHasta.changeStyle(Font.ITALIC);
		txtPrecioHasta.setColumns(10);

		panelTabla = new JPanel();
		GridBagConstraints gbc_panelTabla = new GridBagConstraints();
		gbc_panelTabla.fill = GridBagConstraints.BOTH;
		gbc_panelTabla.gridx = 0;
		gbc_panelTabla.gridy = 2;
		frmMainGastos.add(panelTabla, gbc_panelTabla);
		panelTabla.setLayout(new CardLayout(0, 0));

		JScrollPane spGastos = new JScrollPane();
		panelTabla.add(spGastos, "name_262031167953311");
		modelGastos = new DefaultTableModel(null, nombreColumnas) {
			private static final long serialVersionUID = 1L;

			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		tablaGastos = new JTable(modelGastos);
		tablaGastos.getTableHeader().setReorderingAllowed(false);
		spGastos.setViewportView(tablaGastos);

		this.frmMainGastos.setVisible(false);
	}

	public void mostrarVentana() {
		this.frmMainGastos.setVisible(true);
	}

	public JButton getBtnAgregar() {
		return btnAgregar;
	}

	public JButton getBtnInhabilitar() {
		return btnInhabilitar;
	}

	public JButton getBtnEditar() {
		return btnEditar;
	}

	public JButton getBtnRealizarBusqueda() {
		return btnRealizarBusqueda;
	}

	public DefaultTableModel getModelGastos() {
		return modelGastos;
	}

	public JTable getTablaGastos() {
		return tablaGastos;
	}

	public String[] getNombreColumnas() {
		return nombreColumnas;
	}

	public JTextField getTxtCodigoReferencia() {
		return txtCodigoRef;
	}
	
	public JTextField getTxtImporteDesde() {
		return txtPrecioDesde;
	}
	
	public JTextField getTxtImporteHasta() {
		return txtPrecioHasta;
	}

	public JComboBox<String> getCBTipoEvento() {
		return cbTipoGasto;
	}
	
	public JComboBox<String> getCBLocal() {
		return cbLocal;
	}

	public JRadioButton getRadioTodos() {
		return radioTodos;
	}

	public JRadioButton getRadioHabilitados() {
		return radioHabilitado;
	}

	public JRadioButton getRadioInhabilitados() {
		return radioInhabilitado;
	}

	public JPanel getPanel() {
		return frmMainGastos;
	}
	
	public JDateChooser getTxtFechaDesde() {
		return txtFechaDesde;
	}
	
	public JDateChooser getTxtFechaHasta() {
		return txtFechaHasta;
	}
}
