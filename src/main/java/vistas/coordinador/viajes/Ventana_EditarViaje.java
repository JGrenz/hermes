package vistas.coordinador.viajes;

import java.awt.Dialog.ModalityType;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SpinnerDateModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.DefaultTableModel;

import com.toedter.calendar.JDateChooser;
import com.toedter.calendar.JTextFieldDateEditor;

import dominio.MedioTransporte;
import vistas.TextPrompt;
import vistas.ValidacionCampos;

public class Ventana_EditarViaje {
	
	private static Ventana_EditarViaje INSTANCE;
	private JDialog frmEditarViaje;
	private JPanel contentPane, panelCampos, panelUbicacion;
	private JButton btnConfirmar;
	private JDateChooser txtFechaSalida;
	private JComboBox<MedioTransporte> comboBoxTransporte;
	private JSpinner horaSalida, minutoSalida, horaViaje, minutoViaje;
	private JTextField txtOrigen, txtDestino, txtPrecio, txtImpuesto, txtFechaLlegada, txtPrecioTotal, txtCapacidad;
	private JTextField txtCiudad, txtProvincia, txtPais, txtContinente, txtTag;
	private JTable tablaUbicaciones;
	private DefaultTableModel modelUbicaciones;
	private String[] nombreColumnas = { "Ciudad", "Provincia", "Pais", "Continente", "Tags" };
	private JButton btnSeleccionarDestino, btnSeleccionarOrigen, btnBuscarUbicacion;

	public static Ventana_EditarViaje getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new Ventana_EditarViaje();
		}
		return INSTANCE;
	}

	private Ventana_EditarViaje() {

		frmEditarViaje = new JDialog();
		frmEditarViaje.setIconImage(Toolkit.getDefaultToolkit()
				.getImage(Ventana_AgregarViaje.class.getResource("/imagenes/hermesicono.png")));
		frmEditarViaje.setModalityType(ModalityType.APPLICATION_MODAL);
		frmEditarViaje.setResizable(false);
		frmEditarViaje.setTitle("Hermes - Editar viaje");
		frmEditarViaje.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmEditarViaje.setBounds(100, 100, 929, 538);
		frmEditarViaje.setLocationRelativeTo(null);

		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		frmEditarViaje.setContentPane(contentPane);
		contentPane.setLayout(null);

		panelCampos = new JPanel();
		panelCampos.setBounds(0, 0, 391, 509);
		contentPane.add(panelCampos);
		GridBagLayout gbl_panelCampos = new GridBagLayout();
		gbl_panelCampos.columnWidths = new int[] { 30, 133, 110, 110, 0 };
		gbl_panelCampos.rowHeights = new int[] { 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 80, 23, 30, 0 };
		gbl_panelCampos.columnWeights = new double[] { 0.0, 0.0, 1.0, 1.0, Double.MIN_VALUE };
		gbl_panelCampos.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0,
				0.0, 0.0, Double.MIN_VALUE };
		panelCampos.setLayout(gbl_panelCampos);

		JLabel lblOrigen = new JLabel("Origen:");
		GridBagConstraints gbc_lblOrigen = new GridBagConstraints();
		gbc_lblOrigen.fill = GridBagConstraints.BOTH;
		gbc_lblOrigen.insets = new Insets(0, 0, 5, 5);
		gbc_lblOrigen.gridx = 1;
		gbc_lblOrigen.gridy = 1;
		panelCampos.add(lblOrigen, gbc_lblOrigen);

		txtOrigen = new JTextField();
		txtOrigen.setEditable(false);
		GridBagConstraints gbc_txtOrigen = new GridBagConstraints();
		gbc_txtOrigen.fill = GridBagConstraints.BOTH;
		gbc_txtOrigen.insets = new Insets(0, 0, 5, 0);
		gbc_txtOrigen.gridwidth = 2;
		gbc_txtOrigen.gridx = 2;
		gbc_txtOrigen.gridy = 1;
		panelCampos.add(txtOrigen, gbc_txtOrigen);

		JLabel lblDestino = new JLabel("Destino:");
		GridBagConstraints gbc_lblDestino = new GridBagConstraints();
		gbc_lblDestino.fill = GridBagConstraints.BOTH;
		gbc_lblDestino.insets = new Insets(0, 0, 5, 5);
		gbc_lblDestino.gridx = 1;
		gbc_lblDestino.gridy = 2;
		panelCampos.add(lblDestino, gbc_lblDestino);

		txtDestino = new JTextField();
		txtDestino.setEditable(false);
		GridBagConstraints gbc_txtDestino = new GridBagConstraints();
		gbc_txtDestino.fill = GridBagConstraints.BOTH;
		gbc_txtDestino.insets = new Insets(0, 0, 5, 0);
		gbc_txtDestino.gridwidth = 2;
		gbc_txtDestino.gridx = 2;
		gbc_txtDestino.gridy = 2;
		panelCampos.add(txtDestino, gbc_txtDestino);

		JLabel lblTransporte = new JLabel("Transporte:");
		GridBagConstraints gbc_lblTransporte;
		gbc_lblTransporte = new GridBagConstraints();
		gbc_lblTransporte.fill = GridBagConstraints.BOTH;
		gbc_lblTransporte.insets = new Insets(0, 0, 5, 5);
		gbc_lblTransporte.gridx = 1;
		gbc_lblTransporte.gridy = 3;
		panelCampos.add(lblTransporte, gbc_lblTransporte);

		comboBoxTransporte = new JComboBox<MedioTransporte>();
		GridBagConstraints gbc_comboBoxTransporte = new GridBagConstraints();
		gbc_comboBoxTransporte.fill = GridBagConstraints.BOTH;
		gbc_comboBoxTransporte.insets = new Insets(0, 0, 5, 0);
		gbc_comboBoxTransporte.gridwidth = 2;
		gbc_comboBoxTransporte.gridx = 2;
		gbc_comboBoxTransporte.gridy = 3;
		panelCampos.add(comboBoxTransporte, gbc_comboBoxTransporte);

		JLabel lblCapacidad = new JLabel("Capacidad de pasajes:");
		GridBagConstraints gbc_lblCapacidad = new GridBagConstraints();
		gbc_lblCapacidad.fill = GridBagConstraints.BOTH;
		gbc_lblCapacidad.insets = new Insets(0, 0, 5, 5);
		gbc_lblCapacidad.gridx = 1;
		gbc_lblCapacidad.gridy = 4;
		panelCampos.add(lblCapacidad, gbc_lblCapacidad);

		txtCapacidad = new JTextField();
		txtCapacidad.setColumns(10);
		GridBagConstraints gbc_txtCapacidad = new GridBagConstraints();
		gbc_txtCapacidad.fill = GridBagConstraints.BOTH;
		gbc_txtCapacidad.insets = new Insets(0, 0, 5, 0);
		gbc_txtCapacidad.gridwidth = 2;
		gbc_txtCapacidad.gridx = 2;
		gbc_txtCapacidad.gridy = 4;
		panelCampos.add(txtCapacidad, gbc_txtCapacidad);

		JLabel lblFechaSalida = new JLabel("Fecha de salida:");
		GridBagConstraints gbc_lblFechaSalida = new GridBagConstraints();
		gbc_lblFechaSalida.fill = GridBagConstraints.BOTH;
		gbc_lblFechaSalida.insets = new Insets(0, 0, 5, 5);
		gbc_lblFechaSalida.gridx = 1;
		gbc_lblFechaSalida.gridy = 5;
		panelCampos.add(lblFechaSalida, gbc_lblFechaSalida);

		txtFechaSalida = new JDateChooser();
		txtFechaSalida.setMinSelectableDate(new Date());
		JTextFieldDateEditor editor = (JTextFieldDateEditor) txtFechaSalida.getDateEditor();
		editor.setEditable(false);
		GridBagConstraints gbc_txtFechaSalida = new GridBagConstraints();
		gbc_txtFechaSalida.fill = GridBagConstraints.BOTH;
		gbc_txtFechaSalida.insets = new Insets(0, 0, 5, 0);
		gbc_txtFechaSalida.gridwidth = 2;
		gbc_txtFechaSalida.gridx = 2;
		gbc_txtFechaSalida.gridy = 5;
		panelCampos.add(txtFechaSalida, gbc_txtFechaSalida);

		JLabel lblHoraSalida = new JLabel("Hora de salida:");
		GridBagConstraints gbc_lblHoraSalida = new GridBagConstraints();
		gbc_lblHoraSalida.fill = GridBagConstraints.BOTH;
		gbc_lblHoraSalida.insets = new Insets(0, 0, 5, 5);
		gbc_lblHoraSalida.gridx = 1;
		gbc_lblHoraSalida.gridy = 6;
		panelCampos.add(lblHoraSalida, gbc_lblHoraSalida);

		horaSalida = new JSpinner(new SpinnerDateModel());
		horaSalida.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				String formatDateTime = calculadorfechaViaje();
				txtFechaLlegada.setText(formatDateTime.toString());
			}
		});
		JSpinner.DateEditor horaSEditor = new JSpinner.DateEditor(horaSalida, "HH");
		horaSalida.setEditor(horaSEditor);
		GridBagConstraints gbc_horaSalida = new GridBagConstraints();
		gbc_horaSalida.fill = GridBagConstraints.BOTH;
		gbc_horaSalida.insets = new Insets(0, 0, 5, 5);
		gbc_horaSalida.gridx = 2;
		gbc_horaSalida.gridy = 6;
		panelCampos.add(horaSalida, gbc_horaSalida);

		minutoSalida = new JSpinner(new SpinnerDateModel());
		minutoSalida.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				String formatDateTime = calculadorfechaViaje();
				txtFechaLlegada.setText(formatDateTime.toString());
			}
		});
		JSpinner.DateEditor minutoSEditor = new JSpinner.DateEditor(minutoSalida, "mm");
		minutoSalida.setEditor(minutoSEditor);
		GridBagConstraints gbc_minutoSalida = new GridBagConstraints();
		gbc_minutoSalida.fill = GridBagConstraints.BOTH;
		gbc_minutoSalida.insets = new Insets(0, 0, 5, 0);
		gbc_minutoSalida.gridx = 3;
		gbc_minutoSalida.gridy = 6;
		panelCampos.add(minutoSalida, gbc_minutoSalida);

		JLabel lblHorasViaje = new JLabel("Horas de viaje:");
		GridBagConstraints gbc_lblHorasViaje = new GridBagConstraints();
		gbc_lblHorasViaje.fill = GridBagConstraints.BOTH;
		gbc_lblHorasViaje.insets = new Insets(0, 0, 5, 5);
		gbc_lblHorasViaje.gridx = 1;
		gbc_lblHorasViaje.gridy = 7;
		panelCampos.add(lblHorasViaje, gbc_lblHorasViaje);

		horaViaje = new JSpinner();
		horaViaje.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				String formatDateTime = calculadorfechaViaje();
				txtFechaLlegada.setText(formatDateTime.toString());
			}
		});
		horaViaje.setModel(new SpinnerNumberModel(new Integer(0), new Integer(0), null, new Integer(1)));
		GridBagConstraints gbc_horaViaje = new GridBagConstraints();
		gbc_horaViaje.fill = GridBagConstraints.BOTH;
		gbc_horaViaje.insets = new Insets(0, 0, 5, 5);
		gbc_horaViaje.gridx = 2;
		gbc_horaViaje.gridy = 7;
		panelCampos.add(horaViaje, gbc_horaViaje);

		minutoViaje = new JSpinner();
		minutoViaje.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				String formatDateTime = calculadorfechaViaje();
				txtFechaLlegada.setText(formatDateTime.toString());
			}
		});
		minutoViaje.setModel(new SpinnerNumberModel(0, 0, 59, 1));
		GridBagConstraints gbc_minutoViaje = new GridBagConstraints();
		gbc_minutoViaje.fill = GridBagConstraints.BOTH;
		gbc_minutoViaje.insets = new Insets(0, 0, 5, 0);
		gbc_minutoViaje.gridx = 3;
		gbc_minutoViaje.gridy = 7;
		panelCampos.add(minutoViaje, gbc_minutoViaje);

		JLabel lblFechaDeViaje = new JLabel("Fecha de llegada:");
		GridBagConstraints gbc_lblFechaDeViaje = new GridBagConstraints();
		gbc_lblFechaDeViaje.fill = GridBagConstraints.BOTH;
		gbc_lblFechaDeViaje.insets = new Insets(0, 0, 5, 5);
		gbc_lblFechaDeViaje.gridx = 1;
		gbc_lblFechaDeViaje.gridy = 8;
		panelCampos.add(lblFechaDeViaje, gbc_lblFechaDeViaje);

		txtFechaLlegada = new JTextField();
		txtFechaLlegada.setEditable(false);
		GridBagConstraints gbc_txtFechaLlegada = new GridBagConstraints();
		gbc_txtFechaLlegada.fill = GridBagConstraints.BOTH;
		gbc_txtFechaLlegada.insets = new Insets(0, 0, 5, 0);
		gbc_txtFechaLlegada.gridwidth = 2;
		gbc_txtFechaLlegada.gridx = 2;
		gbc_txtFechaLlegada.gridy = 8;
		panelCampos.add(txtFechaLlegada, gbc_txtFechaLlegada);
		txtFechaLlegada.setColumns(10);

		JLabel lblPrecio = new JLabel("Precio:");
		GridBagConstraints gbc_lblPrecio = new GridBagConstraints();
		gbc_lblPrecio.fill = GridBagConstraints.BOTH;
		gbc_lblPrecio.insets = new Insets(0, 0, 5, 5);
		gbc_lblPrecio.gridx = 1;
		gbc_lblPrecio.gridy = 9;
		panelCampos.add(lblPrecio, gbc_lblPrecio);

		txtPrecio = new JTextField();
		txtPrecio.setText("0");
		txtPrecio.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				esNumero(e, txtPrecio);
			}

			@Override
			public void keyReleased(KeyEvent arg0) {
				calcularPrecio();
			}
		});
		txtPrecio.setColumns(10);
		GridBagConstraints gbc_txtPrecio = new GridBagConstraints();
		gbc_txtPrecio.fill = GridBagConstraints.BOTH;
		gbc_txtPrecio.insets = new Insets(0, 0, 5, 0);
		gbc_txtPrecio.gridwidth = 2;
		gbc_txtPrecio.gridx = 2;
		gbc_txtPrecio.gridy = 9;
		panelCampos.add(txtPrecio, gbc_txtPrecio);

		JLabel lblImpuesto = new JLabel("Impuesto:");
		GridBagConstraints gbc_lblImpuesto = new GridBagConstraints();
		gbc_lblImpuesto.fill = GridBagConstraints.BOTH;
		gbc_lblImpuesto.insets = new Insets(0, 0, 5, 5);
		gbc_lblImpuesto.gridx = 1;
		gbc_lblImpuesto.gridy = 10;
		panelCampos.add(lblImpuesto, gbc_lblImpuesto);

		txtImpuesto = new JTextField();
		txtImpuesto.setText("0");
		txtImpuesto.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				esNumero(e, txtImpuesto);
			}

			@Override
			public void keyReleased(KeyEvent arg0) {
				calcularPrecio();
			}
		});
		txtImpuesto.setColumns(10);
		GridBagConstraints gbc_txtImpuesto = new GridBagConstraints();
		gbc_txtImpuesto.fill = GridBagConstraints.BOTH;
		gbc_txtImpuesto.insets = new Insets(0, 0, 5, 0);
		gbc_txtImpuesto.gridwidth = 2;
		gbc_txtImpuesto.gridx = 2;
		gbc_txtImpuesto.gridy = 10;
		panelCampos.add(txtImpuesto, gbc_txtImpuesto);

		JLabel lblTotal = new JLabel("Total:");
		GridBagConstraints gbc_lblTotal = new GridBagConstraints();
		gbc_lblTotal.fill = GridBagConstraints.BOTH;
		gbc_lblTotal.insets = new Insets(0, 0, 5, 5);
		gbc_lblTotal.gridx = 1;
		gbc_lblTotal.gridy = 11;
		panelCampos.add(lblTotal, gbc_lblTotal);

		txtPrecioTotal = new JTextField();
		txtPrecioTotal.setEditable(false);
		GridBagConstraints gbc_txtPrecioTotal = new GridBagConstraints();
		gbc_txtPrecioTotal.fill = GridBagConstraints.BOTH;
		gbc_txtPrecioTotal.insets = new Insets(0, 0, 5, 0);
		gbc_txtPrecioTotal.gridwidth = 2;
		gbc_txtPrecioTotal.gridx = 2;
		gbc_txtPrecioTotal.gridy = 11;
		panelCampos.add(txtPrecioTotal, gbc_txtPrecioTotal);
		txtPrecioTotal.setColumns(10);

		btnConfirmar = new JButton("Confirmar");
		GridBagConstraints gbc_btnConfirmar = new GridBagConstraints();
		gbc_btnConfirmar.insets = new Insets(0, 0, 5, 0);
		gbc_btnConfirmar.fill = GridBagConstraints.BOTH;
		gbc_btnConfirmar.gridx = 3;
		gbc_btnConfirmar.gridy = 13;
		panelCampos.add(btnConfirmar, gbc_btnConfirmar);

		panelUbicacion = new JPanel();
		panelUbicacion.setBounds(401, 0, 522, 509);
		contentPane.add(panelUbicacion);
		GridBagLayout gbl_panelUbicacion = new GridBagLayout();
		gbl_panelUbicacion.columnWidths = new int[] { 30, 100, 0, 100, 0 };
		gbl_panelUbicacion.rowHeights = new int[] { 30, 30, 30, 30, 30, 30, 30, 30, 150, 30, 30, 0 };
		gbl_panelUbicacion.columnWeights = new double[] { 0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE };
		gbl_panelUbicacion.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
				Double.MIN_VALUE };
		panelUbicacion.setLayout(gbl_panelUbicacion);

		JLabel lblBuscarUbicacion = new JLabel("Buscar ubicación por:");
		GridBagConstraints gbc_lblBuscarUbicacion = new GridBagConstraints();
		gbc_lblBuscarUbicacion.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblBuscarUbicacion.insets = new Insets(0, 0, 5, 5);
		gbc_lblBuscarUbicacion.gridx = 1;
		gbc_lblBuscarUbicacion.gridy = 1;
		panelUbicacion.add(lblBuscarUbicacion, gbc_lblBuscarUbicacion);

		JLabel lblCiudad = new JLabel("Ciudad");
		lblCiudad.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_lblCiudad = new GridBagConstraints();
		gbc_lblCiudad.fill = GridBagConstraints.BOTH;
		gbc_lblCiudad.insets = new Insets(0, 0, 5, 5);
		gbc_lblCiudad.gridx = 1;
		gbc_lblCiudad.gridy = 2;
		panelUbicacion.add(lblCiudad, gbc_lblCiudad);

		txtCiudad = new JTextField();
		txtCiudad.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_txtCiudad = new GridBagConstraints();
		gbc_txtCiudad.fill = GridBagConstraints.BOTH;
		gbc_txtCiudad.insets = new Insets(0, 0, 5, 5);
		gbc_txtCiudad.gridx = 2;
		gbc_txtCiudad.gridy = 2;
		panelUbicacion.add(txtCiudad, gbc_txtCiudad);
		txtCiudad.setColumns(10);
		TextPrompt phCiudad = new TextPrompt("Ingrese ciudad...", txtCiudad);
		phCiudad.setText("Ingrese ciudad...");
		phCiudad.changeAlpha(0.75f);
		phCiudad.changeStyle(Font.ITALIC);

		JLabel lblProvincia = new JLabel("Provincia / estado");
		lblProvincia.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_lblProvincia = new GridBagConstraints();
		gbc_lblProvincia.fill = GridBagConstraints.BOTH;
		gbc_lblProvincia.insets = new Insets(0, 0, 5, 5);
		gbc_lblProvincia.gridx = 1;
		gbc_lblProvincia.gridy = 3;
		panelUbicacion.add(lblProvincia, gbc_lblProvincia);

		txtProvincia = new JTextField();
		txtProvincia.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_txtProvincia = new GridBagConstraints();
		gbc_txtProvincia.fill = GridBagConstraints.BOTH;
		gbc_txtProvincia.insets = new Insets(0, 0, 5, 5);
		gbc_txtProvincia.gridx = 2;
		gbc_txtProvincia.gridy = 3;
		panelUbicacion.add(txtProvincia, gbc_txtProvincia);
		txtProvincia.setColumns(10);
		TextPrompt phProvincia = new TextPrompt("Ingrese provincia/estado...", txtProvincia);
		phProvincia.setText("Ingrese provincia/estado...");
		phProvincia.changeAlpha(0.75f);
		phProvincia.changeStyle(Font.ITALIC);

		JLabel lblPais = new JLabel("Pais");
		lblPais.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_lblPais = new GridBagConstraints();
		gbc_lblPais.fill = GridBagConstraints.BOTH;
		gbc_lblPais.insets = new Insets(0, 0, 5, 5);
		gbc_lblPais.gridx = 1;
		gbc_lblPais.gridy = 4;
		panelUbicacion.add(lblPais, gbc_lblPais);

		txtPais = new JTextField();
		txtPais.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_txtPais = new GridBagConstraints();
		gbc_txtPais.fill = GridBagConstraints.BOTH;
		gbc_txtPais.insets = new Insets(0, 0, 5, 5);
		gbc_txtPais.gridx = 2;
		gbc_txtPais.gridy = 4;
		panelUbicacion.add(txtPais, gbc_txtPais);
		txtPais.setColumns(10);
		TextPrompt phPais = new TextPrompt("Ingrese pais...", txtPais);
		phPais.setText("Ingrese pais...");
		phPais.changeAlpha(0.75f);
		phPais.changeStyle(Font.ITALIC);

		JLabel lblContinente = new JLabel("Continente");
		lblContinente.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_lblContinente = new GridBagConstraints();
		gbc_lblContinente.fill = GridBagConstraints.BOTH;
		gbc_lblContinente.insets = new Insets(0, 0, 5, 5);
		gbc_lblContinente.gridx = 1;
		gbc_lblContinente.gridy = 5;
		panelUbicacion.add(lblContinente, gbc_lblContinente);

		txtContinente = new JTextField();
		txtContinente.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_txtContinente = new GridBagConstraints();
		gbc_txtContinente.fill = GridBagConstraints.BOTH;
		gbc_txtContinente.insets = new Insets(0, 0, 5, 5);
		gbc_txtContinente.gridx = 2;
		gbc_txtContinente.gridy = 5;
		panelUbicacion.add(txtContinente, gbc_txtContinente);
		txtContinente.setColumns(10);
		TextPrompt phContinente = new TextPrompt("Ingrese continente...", txtContinente);
		phContinente.setText("Ingrese continente...");
		phContinente.changeAlpha(0.75f);
		phContinente.changeStyle(Font.ITALIC);

		JLabel lblTag = new JLabel("Tag");
		lblTag.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_lblTag = new GridBagConstraints();
		gbc_lblTag.fill = GridBagConstraints.BOTH;
		gbc_lblTag.insets = new Insets(0, 0, 5, 5);
		gbc_lblTag.gridx = 1;
		gbc_lblTag.gridy = 6;
		panelUbicacion.add(lblTag, gbc_lblTag);

		txtTag = new JTextField();
		txtTag.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_txtTag = new GridBagConstraints();
		gbc_txtTag.insets = new Insets(0, 0, 5, 5);
		gbc_txtTag.fill = GridBagConstraints.BOTH;
		gbc_txtTag.gridx = 2;
		gbc_txtTag.gridy = 6;
		panelUbicacion.add(txtTag, gbc_txtTag);
		txtTag.setColumns(10);
		TextPrompt phTag = new TextPrompt("Ingrese tag...", txtTag);
		phTag.setText("Ingrese tag...");
		phTag.changeAlpha(0.75f);
		phTag.changeStyle(Font.ITALIC);
		
		btnBuscarUbicacion = new JButton("Buscar ubicación");
		GridBagConstraints gbc_btnBuscarUbicacin = new GridBagConstraints();
		gbc_btnBuscarUbicacin.fill = GridBagConstraints.BOTH;
		gbc_btnBuscarUbicacin.insets = new Insets(0, 0, 5, 10);
		gbc_btnBuscarUbicacin.gridx = 3;
		gbc_btnBuscarUbicacin.gridy = 7;
		panelUbicacion.add(btnBuscarUbicacion, gbc_btnBuscarUbicacin);

		JScrollPane spUbicaciones = new JScrollPane();
		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.gridwidth = 3;
		gbc_scrollPane.insets = new Insets(0, 0, 5, 10);
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.gridx = 1;
		gbc_scrollPane.gridy = 8;
		panelUbicacion.add(spUbicaciones, gbc_scrollPane);
		modelUbicaciones = new DefaultTableModel(null, nombreColumnas) {
			private static final long serialVersionUID = 1L;

			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		tablaUbicaciones = new JTable(modelUbicaciones);
		tablaUbicaciones.getTableHeader().setReorderingAllowed(false);
		spUbicaciones.setViewportView(tablaUbicaciones);

		btnSeleccionarOrigen = new JButton("Seleccionar origen");
		GridBagConstraints gbc_btnSeleccionarOrigen = new GridBagConstraints();
		gbc_btnSeleccionarOrigen.fill = GridBagConstraints.BOTH;
		gbc_btnSeleccionarOrigen.insets = new Insets(0, 0, 5, 10);
		gbc_btnSeleccionarOrigen.gridx = 3;
		gbc_btnSeleccionarOrigen.gridy = 9;
		panelUbicacion.add(btnSeleccionarOrigen, gbc_btnSeleccionarOrigen);

		btnSeleccionarDestino = new JButton("Seleccionar destino");
		GridBagConstraints gbc_btnSeleccionarDestino = new GridBagConstraints();
		gbc_btnSeleccionarDestino.fill = GridBagConstraints.BOTH;
		gbc_btnSeleccionarDestino.insets = new Insets(0, 0, 5, 10);
		gbc_btnSeleccionarDestino.gridx = 3;
		gbc_btnSeleccionarDestino.gridy = 10;
		panelUbicacion.add(btnSeleccionarDestino, gbc_btnSeleccionarDestino);

		this.frmEditarViaje.setVisible(false);
	}
	
	public void mostrarVentana() {
		this.frmEditarViaje.setVisible(true);
	}

	public JButton getBtnConfirmar() {
		return btnConfirmar;
	}

	public JTextField getTxtOrigen() {
		return txtOrigen;
	}

	public JTextField getTxtDestino() {
		return txtDestino;
	}

	public MedioTransporte getTxtTransporte() {
		return (MedioTransporte) comboBoxTransporte.getSelectedItem();
	}

	public JComboBox<MedioTransporte> getComboBoxTransporte() {
		return comboBoxTransporte;
	}

	public JTextField getTxtCapacidad() {
		return txtCapacidad;
	}
	
	public int getCapacidad() {
		String nCapacidad = txtCapacidad.getText();
		int nCapacidad2 = Integer.parseInt(nCapacidad);
		return nCapacidad2;
	}

	public JDateChooser getChooserFechaSalida() {
		return txtFechaSalida;
	}
	
	public LocalDate getFechaSalida() {
		Date date = txtFechaSalida.getDate();
		return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
	}
	
	public JSpinner getSpinnerHoraSalida() {
		return horaSalida;
	}
	
	public JSpinner getSpinnerMinutoSalida() {
		return minutoSalida;
	}

	public JSpinner getHoraViaje() {
		return horaViaje;
	}
	
	public JSpinner getMinutoViaje() {
		return minutoViaje;
	}
	
	public LocalTime getHoraSalida() {
		String hora = new SimpleDateFormat("HH").format(horaSalida.getValue());
		String minuto = new SimpleDateFormat("mm").format(minutoSalida.getValue());
		LocalTime salida = LocalTime.parse(hora + ":" + minuto);

		return salida;
	}

	public Duration getCantidadHorasViaje() {
		Duration cantidadHorasDeViaje = Duration
				.parse("PT" + horaViaje.getValue().toString() + "H" + minutoViaje.getValue().toString() + "M");
		return cantidadHorasDeViaje;
	}

	private String calculadorfechaViaje() {
		LocalTime hora = getHoraSalida();
		LocalDate fecha = getFechaSalida();

		Duration duracionViaje = getCantidadHorasViaje();

		LocalDateTime fechasalida = fecha.atTime(hora);

		fechasalida = fechasalida.plus(duracionViaje.getSeconds(), ChronoUnit.SECONDS);

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");

		String formatDateTime = fechasalida.format(formatter);
		return formatDateTime;
	}

	public JTextField getTxtFechaViaje() {
		return txtFechaLlegada;
	}

	public JTextField getTxtPrecio() {
		return txtPrecio;
	}
	
	public JTextField getTxtImpuesto() {
		return txtImpuesto;
	}
	
	public BigDecimal getPrecio() {
		BigDecimal precio = new BigDecimal(txtPrecio.getText());
		return precio;
	}

	public BigDecimal getImpuesto() {
		BigDecimal impuesto = new BigDecimal(txtImpuesto.getText());
		return impuesto;
	}

	public JTextField getTxtPrecioTotal() {
		return txtPrecioTotal;
	}

	public void cerrar() {
		this.frmEditarViaje.dispose();
		this.txtDestino.setText(null);
		this.txtOrigen.setText(null);
		this.comboBoxTransporte.setSelectedIndex(-1);
		this.txtCapacidad.setText(null);
		this.txtFechaSalida.setDate(null);
		this.txtImpuesto.setText(null);
		this.txtPrecio.setText(null);
	}

	private boolean camposVacios() {
		if (txtCapacidad.getText().equals("") || txtPrecio.getText().equals("")
			//|| comboBoxOrigen.getSelectedIndex() == -1 || comboBoxDestino.getSelectedIndex() == -1
			|| comboBoxTransporte.getSelectedIndex() == -1 || horaSalida == null || txtFechaSalida.getDate() == null)
		{
			JOptionPane.showMessageDialog(null, "No pueden quedar campos vacíos.");
			return true;
		}
		else
			return false;
	}
	
	private boolean camposCorrectos() {
		ValidacionCampos validador = new ValidacionCampos();
		if(!validador.esNumerico(txtCapacidad.getText()) || !validador.esNumerico(txtPrecio.getText()))
			return false;
		else
			return true;
	}
	
	public boolean verificarCampo() {
		if (!camposVacios() && camposCorrectos())
			return true;
		else
			return false;
	}

	private void esNumero(KeyEvent e, JTextField campo) {
		char caracter = e.getKeyChar();
		if (((caracter < '0') || (caracter > '9')) && (caracter != KeyEvent.VK_BACK_SPACE)
				&& (caracter != '-' || campo.getText().contains("-"))
				&& (caracter != '.' || campo.getText().contains("."))) {
			e.consume();
			JOptionPane.showMessageDialog(null, "Solo se admiten numeros en este campo.", "¡Error!",
					JOptionPane.ERROR_MESSAGE);
		}
	}

	private void calcularPrecio() {
		BigDecimal precio = getPrecio();
		BigDecimal impuesto = getImpuesto();

		String total = precio.add(impuesto).toString();

		txtPrecioTotal.setText(total);
	}

	public void mostrarEdicionParcial() {
		txtImpuesto.setEnabled(false);
		txtPrecio.setEnabled(false);
		txtCapacidad.setEnabled(false);
		txtPrecioTotal.setEnabled(false);
		txtDestino.setEnabled(false);
		txtOrigen.setEnabled(false);
		comboBoxTransporte.setEnabled(false);
		txtCiudad.setEnabled(false);
		txtProvincia.setEnabled(false);
		txtPais.setEnabled(false);
		txtContinente.setEnabled(false);
		txtTag.setEnabled(false);
		btnBuscarUbicacion.setEnabled(false);
		btnSeleccionarDestino.setEnabled(false);
		btnSeleccionarOrigen.setEnabled(false);
		tablaUbicaciones.setEnabled(false);
    }

	public void mostrarEdicionTotal() {
		txtImpuesto.setEnabled(true);
		txtPrecio.setEnabled(true);
		txtCapacidad.setEnabled(true);
		txtPrecioTotal.setEnabled(true);
		txtDestino.setEnabled(true);
		txtOrigen.setEnabled(true);
		comboBoxTransporte.setEnabled(true);
		txtCiudad.setEnabled(true);
		txtProvincia.setEnabled(true);
		txtPais.setEnabled(true);
		txtContinente.setEnabled(true);
		txtTag.setEnabled(true);
		btnBuscarUbicacion.setEnabled(true);
		btnSeleccionarDestino.setEnabled(true);
		btnSeleccionarOrigen.setEnabled(true);
		tablaUbicaciones.setEnabled(true);
	}

	public JButton getBtnBuscarUbicacion() {
		return btnBuscarUbicacion;
	}
	
	public JButton getBtnSeleccionarOrigen() {
		return btnSeleccionarOrigen;
	}
	
	public JButton getBtnSeleccionarDestino() {
		return btnSeleccionarDestino;
	}
	
	public JTable getTablaUbicaciones() {
		return tablaUbicaciones;
	}

	public String[] getColumnasDeUbicacion() {
		return nombreColumnas;
	}

	public DefaultTableModel getModelUbicacion() {
		return modelUbicaciones;
	}
	
	public JTextField getTxtBuscarCiudad() {
		return txtCiudad;
	}
	
	public JTextField getTxtBuscarProvincia() {
		return txtProvincia;
	}
	
	public JTextField getTxtBuscarPais() {
		return txtPais;
	}
	
	public JTextField getTxtBuscarContinente() {
		return txtContinente;
	}
	
	public JTextField getTxtBuscarTag() {
		return txtTag;
	}
}
