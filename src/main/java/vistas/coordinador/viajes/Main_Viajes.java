package vistas.coordinador.viajes;

import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;

import vistas.TextPrompt;

public class Main_Viajes {

	private static Main_Viajes INSTANCE;
	private JPanel frmMainViajes;
	private JTable tablaViajes;
	private DefaultTableModel modelViajes;
	private String[] nombreColumnas = { "Código de referencia", "Origen", "Destino", "Fecha de salida",
			"Hora de salida", "Horas de viaje", "Capacidad", "Medio de transporte", "Precio", "Impuesto", "Estado" };
	private JButton btnAgregar, btnEditar, btnCancelar, btnRealizarBusqueda;
	private ButtonGroup filtro;
	private JRadioButton radioAbierto, radioFinalizado, radioTodos, radioCancelado;
	private JTextField txtCiudadOrigen, txtBusquedaCodigo, txtProvinciaOrigen, txtCiudadDestino, txtProvinciaDestino;
	private JComboBox<String> cbPaisOrigen, cbContinenteOrigen, cbPaisDestino, cbContinenteDestino;
	private JPanel panelNorte, panelSur, panelBusqueda;

	public static Main_Viajes getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new Main_Viajes();
		}
		return INSTANCE;
	}

	public Main_Viajes() {

		filtro = new ButtonGroup();

		frmMainViajes = new JPanel();
		GridBagLayout gbl_frmMainViajes = new GridBagLayout();
		gbl_frmMainViajes.columnWidths = new int[] { 202, 0 };
		gbl_frmMainViajes.rowHeights = new int[] { 69, 123, 427, 0 };
		gbl_frmMainViajes.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gbl_frmMainViajes.rowWeights = new double[] { 0.0, 0.0, 1.0, Double.MIN_VALUE };
		frmMainViajes.setLayout(gbl_frmMainViajes);

		panelNorte = new JPanel();
		GridBagConstraints gbc_panelNorte = new GridBagConstraints();
		gbc_panelNorte.fill = GridBagConstraints.BOTH;
		gbc_panelNorte.insets = new Insets(0, 0, 5, 0);
		gbc_panelNorte.gridx = 0;
		gbc_panelNorte.gridy = 0;
		frmMainViajes.add(panelNorte, gbc_panelNorte);

		btnAgregar = new JButton("Agregar");
		btnAgregar.setIcon(new ImageIcon(Main_Viajes.class.getResource("/imagenes/iconos/viajeAgregar.png")));
		panelNorte.add(btnAgregar);

		btnCancelar = new JButton("Cancelar");
		btnCancelar.setIcon(new ImageIcon(Main_Viajes.class.getResource("/imagenes/iconos/viajeHabInhab.png")));
		panelNorte.add(btnCancelar);

		btnEditar = new JButton("Editar");
		btnEditar.setIcon(new ImageIcon(Main_Viajes.class.getResource("/imagenes/iconos/viajeEditar.png")));
		panelNorte.add(btnEditar);

		panelBusqueda = new JPanel();
		panelBusqueda.setBounds(398, 0, 926, 52);
		GridBagConstraints gbc_panelBusqueda = new GridBagConstraints();
		gbc_panelBusqueda.fill = GridBagConstraints.BOTH;
		gbc_panelBusqueda.insets = new Insets(0, 0, 5, 0);
		gbc_panelBusqueda.gridx = 0;
		gbc_panelBusqueda.gridy = 1;
		frmMainViajes.add(panelBusqueda, gbc_panelBusqueda);
		GridBagLayout gbl_panelBusqueda = new GridBagLayout();
		gbl_panelBusqueda.columnWidths = new int[] { 130, 30, 120, 210, 30, 120, 210, 0 };
		gbl_panelBusqueda.rowHeights = new int[] { 30, 30, 30, 30, 30, 30 };
		gbl_panelBusqueda.columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0 };
		gbl_panelBusqueda.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
		panelBusqueda.setLayout(gbl_panelBusqueda);

		JLabel lblFiltrarPor = new JLabel("Filtrar por:");
		lblFiltrarPor.setPreferredSize(new Dimension(0, 30));
		lblFiltrarPor.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_lblFiltrarPor = new GridBagConstraints();
		gbc_lblFiltrarPor.fill = GridBagConstraints.BOTH;
		gbc_lblFiltrarPor.insets = new Insets(0, 0, 5, 5);
		gbc_lblFiltrarPor.gridx = 0;
		gbc_lblFiltrarPor.gridy = 0;
		panelBusqueda.add(lblFiltrarPor, gbc_lblFiltrarPor);

		JLabel lblBusquedaPor = new JLabel("Búsqueda por:");
		lblBusquedaPor.setPreferredSize(new Dimension(0, 30));
		lblBusquedaPor.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_lblBusquedaPor = new GridBagConstraints();
		gbc_lblBusquedaPor.fill = GridBagConstraints.BOTH;
		gbc_lblBusquedaPor.insets = new Insets(0, 0, 5, 5);
		gbc_lblBusquedaPor.gridx = 2;
		gbc_lblBusquedaPor.gridy = 0;
		panelBusqueda.add(lblBusquedaPor, gbc_lblBusquedaPor);

		radioTodos = new JRadioButton("Todos");
		radioTodos.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_radioTodos = new GridBagConstraints();
		gbc_radioTodos.fill = GridBagConstraints.BOTH;
		gbc_radioTodos.insets = new Insets(0, 0, 5, 5);
		gbc_radioTodos.gridx = 0;
		gbc_radioTodos.gridy = 1;
		panelBusqueda.add(radioTodos, gbc_radioTodos);
		filtro.add(radioTodos);

		JLabel radioCodigoReferencia = new JLabel("Codigo de referencia");
		radioCodigoReferencia.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_radioCodigoReferencia = new GridBagConstraints();
		gbc_radioCodigoReferencia.fill = GridBagConstraints.BOTH;
		gbc_radioCodigoReferencia.insets = new Insets(0, 0, 5, 5);
		gbc_radioCodigoReferencia.gridx = 2;
		gbc_radioCodigoReferencia.gridy = 1;
		panelBusqueda.add(radioCodigoReferencia, gbc_radioCodigoReferencia);

		txtBusquedaCodigo = new JTextField();
		txtBusquedaCodigo.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_txtBusquedaCodigo = new GridBagConstraints();
		gbc_txtBusquedaCodigo.insets = new Insets(0, 0, 5, 5);
		gbc_txtBusquedaCodigo.fill = GridBagConstraints.BOTH;
		gbc_txtBusquedaCodigo.gridx = 3;
		gbc_txtBusquedaCodigo.gridy = 1;
		panelBusqueda.add(txtBusquedaCodigo, gbc_txtBusquedaCodigo);
		txtBusquedaCodigo.setColumns(10);
		TextPrompt phCodigo = new TextPrompt("Ingrese codigo de referencia", txtBusquedaCodigo);
		phCodigo.setPreferredSize(new Dimension(0, 30));
		phCodigo.setText("Ingrese codigo de referencia...");
		phCodigo.changeAlpha(0.75f);
		phCodigo.changeStyle(Font.ITALIC);

		radioAbierto = new JRadioButton("Abierto");
		radioAbierto.setSelected(true);
		radioAbierto.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_radioAbierto = new GridBagConstraints();
		gbc_radioAbierto.fill = GridBagConstraints.BOTH;
		gbc_radioAbierto.insets = new Insets(0, 0, 5, 5);
		gbc_radioAbierto.gridx = 0;
		gbc_radioAbierto.gridy = 2;
		panelBusqueda.add(radioAbierto, gbc_radioAbierto);
		filtro.add(radioAbierto);

		JLabel lblCiudadOrigen = new JLabel("Ciudad origen");
		lblCiudadOrigen.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_lblCiudadOrigen = new GridBagConstraints();
		gbc_lblCiudadOrigen.fill = GridBagConstraints.BOTH;
		gbc_lblCiudadOrigen.insets = new Insets(0, 0, 5, 5);
		gbc_lblCiudadOrigen.gridx = 2;
		gbc_lblCiudadOrigen.gridy = 2;
		panelBusqueda.add(lblCiudadOrigen, gbc_lblCiudadOrigen);

		JLabel radioTipo = new JLabel("Tipo de evento");
		radioTipo.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_radioTipo = new GridBagConstraints();
		gbc_radioTipo.fill = GridBagConstraints.BOTH;
		gbc_radioTipo.insets = new Insets(0, 0, 5, 5);
		gbc_radioTipo.gridx = 5;
		gbc_radioTipo.gridy = 2;

		txtCiudadOrigen = new JTextField();
		txtCiudadOrigen.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_txtCiudadOrigen = new GridBagConstraints();
		gbc_txtCiudadOrigen.fill = GridBagConstraints.BOTH;
		gbc_txtCiudadOrigen.insets = new Insets(0, 0, 5, 5);
		gbc_txtCiudadOrigen.gridx = 3;
		gbc_txtCiudadOrigen.gridy = 2;
		panelBusqueda.add(txtCiudadOrigen, gbc_txtCiudadOrigen);
		txtCiudadOrigen.setColumns(10);
		TextPrompt phCiudadOrigen = new TextPrompt("Ingrese ciudad origen...", txtCiudadOrigen);
		phCiudadOrigen.setText("Ingrese ciudad origen...");
		phCiudadOrigen.changeAlpha(0.75f);
		phCiudadOrigen.changeStyle(Font.ITALIC);

		JLabel lblCiudadDestino = new JLabel("Ciudad destino");
		lblCiudadDestino.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_lblCiudadDestino = new GridBagConstraints();
		gbc_lblCiudadDestino.fill = GridBagConstraints.BOTH;
		gbc_lblCiudadDestino.insets = new Insets(0, 0, 5, 5);
		gbc_lblCiudadDestino.gridx = 5;
		gbc_lblCiudadDestino.gridy = 2;
		panelBusqueda.add(lblCiudadDestino, gbc_lblCiudadDestino);

		txtCiudadDestino = new JTextField();
		GridBagConstraints gbc_txtCiudadDestino = new GridBagConstraints();
		gbc_txtCiudadDestino.insets = new Insets(0, 0, 5, 5);
		gbc_txtCiudadDestino.fill = GridBagConstraints.BOTH;
		gbc_txtCiudadDestino.gridx = 6;
		gbc_txtCiudadDestino.gridy = 2;
		panelBusqueda.add(txtCiudadDestino, gbc_txtCiudadDestino);
		txtCiudadDestino.setColumns(10);
		TextPrompt phCiudadDestino = new TextPrompt("Ingrese ciudad destino...", txtCiudadDestino);
		phCiudadDestino.setText("Ingrese ciudad destino...");
		phCiudadDestino.changeAlpha(0.75f);
		phCiudadDestino.changeStyle(Font.ITALIC);

		radioFinalizado = new JRadioButton("Finalizado");
		radioFinalizado.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_radioFinalizado = new GridBagConstraints();
		gbc_radioFinalizado.fill = GridBagConstraints.BOTH;
		gbc_radioFinalizado.insets = new Insets(0, 0, 5, 5);
		gbc_radioFinalizado.gridx = 0;
		gbc_radioFinalizado.gridy = 3;
		panelBusqueda.add(radioFinalizado, gbc_radioFinalizado);
		radioFinalizado.setSelected(true);
		filtro.add(radioFinalizado);

		JLabel lblProvinciaOrigen = new JLabel("Provincia origen");
		lblProvinciaOrigen.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_lblProvinciaOrigen = new GridBagConstraints();
		gbc_lblProvinciaOrigen.fill = GridBagConstraints.BOTH;
		gbc_lblProvinciaOrigen.insets = new Insets(0, 0, 5, 5);
		gbc_lblProvinciaOrigen.gridx = 2;
		gbc_lblProvinciaOrigen.gridy = 3;
		panelBusqueda.add(lblProvinciaOrigen, gbc_lblProvinciaOrigen);

		txtProvinciaOrigen = new JTextField();
		txtProvinciaOrigen.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_txtProvinciaOrigen = new GridBagConstraints();
		gbc_txtProvinciaOrigen.fill = GridBagConstraints.BOTH;
		gbc_txtProvinciaOrigen.insets = new Insets(0, 0, 5, 5);
		gbc_txtProvinciaOrigen.gridx = 3;
		gbc_txtProvinciaOrigen.gridy = 3;
		panelBusqueda.add(txtProvinciaOrigen, gbc_txtProvinciaOrigen);
		txtProvinciaOrigen.setColumns(10);
		TextPrompt phProvinciaOrigen = new TextPrompt("Ingrese provincia origen...", txtProvinciaOrigen);
		phProvinciaOrigen.setText("Ingrese provincia...");
		phProvinciaOrigen.changeAlpha(0.75f);
		phProvinciaOrigen.changeStyle(Font.ITALIC);

		JLabel lblProvinciaDestino = new JLabel("Provincia destino");
		lblProvinciaDestino.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_lblProvinciaDestino = new GridBagConstraints();
		gbc_lblProvinciaDestino.fill = GridBagConstraints.BOTH;
		gbc_lblProvinciaDestino.insets = new Insets(0, 0, 5, 5);
		gbc_lblProvinciaDestino.gridx = 5;
		gbc_lblProvinciaDestino.gridy = 3;
		panelBusqueda.add(lblProvinciaDestino, gbc_lblProvinciaDestino);

		txtProvinciaDestino = new JTextField();
		GridBagConstraints gbc_txtProvinciaDestino = new GridBagConstraints();
		gbc_txtProvinciaDestino.insets = new Insets(0, 0, 5, 5);
		gbc_txtProvinciaDestino.fill = GridBagConstraints.BOTH;
		gbc_txtProvinciaDestino.gridx = 6;
		gbc_txtProvinciaDestino.gridy = 3;
		panelBusqueda.add(txtProvinciaDestino, gbc_txtProvinciaDestino);
		txtProvinciaDestino.setColumns(10);
		TextPrompt phProvinciaDestino = new TextPrompt("Ingrese provincia destino...", txtProvinciaDestino);
		phProvinciaDestino.setText("Ingrese provincia destino...");
		phProvinciaDestino.changeAlpha(0.75f);
		phProvinciaDestino.changeStyle(Font.ITALIC);

		radioCancelado = new JRadioButton("Cancelado");
		GridBagConstraints gbc_radioCancelado = new GridBagConstraints();
		gbc_radioCancelado.fill = GridBagConstraints.BOTH;
		gbc_radioCancelado.insets = new Insets(0, 0, 5, 5);
		gbc_radioCancelado.gridx = 0;
		gbc_radioCancelado.gridy = 4;
		panelBusqueda.add(radioCancelado, gbc_radioCancelado);
		filtro.add(radioCancelado);

		JLabel lblPaisOrigen = new JLabel("Pais origen");
		lblPaisOrigen.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_lblPaisOrigen = new GridBagConstraints();
		gbc_lblPaisOrigen.fill = GridBagConstraints.BOTH;
		gbc_lblPaisOrigen.insets = new Insets(0, 0, 5, 5);
		gbc_lblPaisOrigen.gridx = 2;
		gbc_lblPaisOrigen.gridy = 4;
		panelBusqueda.add(lblPaisOrigen, gbc_lblPaisOrigen);

		btnRealizarBusqueda = new JButton("Buscar");
		btnRealizarBusqueda.setPreferredSize(new Dimension(0, 30));
		btnRealizarBusqueda.setBounds(498, 0, 123, 29);
		GridBagConstraints gbc_btnRealizarBusqueda = new GridBagConstraints();
		gbc_btnRealizarBusqueda.insets = new Insets(0, 0, 5, 5);
		gbc_btnRealizarBusqueda.gridx = 3;
		gbc_btnRealizarBusqueda.gridy = 0;
		gbc_btnRealizarBusqueda.fill = GridBagConstraints.BOTH;
		panelBusqueda.add(btnRealizarBusqueda, gbc_btnRealizarBusqueda);

		cbPaisOrigen = new JComboBox<String>();
		cbPaisOrigen.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_cbPaisOrigen = new GridBagConstraints();
		gbc_cbPaisOrigen.fill = GridBagConstraints.BOTH;
		gbc_cbPaisOrigen.insets = new Insets(0, 0, 5, 5);
		gbc_cbPaisOrigen.gridx = 3;
		gbc_cbPaisOrigen.gridy = 4;
		panelBusqueda.add(cbPaisOrigen, gbc_cbPaisOrigen);

		JLabel lblPaisDestino = new JLabel("Pais destino");
		lblPaisDestino.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_lblPaisDestino = new GridBagConstraints();
		gbc_lblPaisDestino.fill = GridBagConstraints.BOTH;
		gbc_lblPaisDestino.insets = new Insets(0, 0, 5, 5);
		gbc_lblPaisDestino.gridx = 5;
		gbc_lblPaisDestino.gridy = 4;
		panelBusqueda.add(lblPaisDestino, gbc_lblPaisDestino);

		cbPaisDestino = new JComboBox<String>();
		cbPaisDestino.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_cbPaisDestino = new GridBagConstraints();
		gbc_cbPaisDestino.insets = new Insets(0, 0, 5, 5);
		gbc_cbPaisDestino.fill = GridBagConstraints.BOTH;
		gbc_cbPaisDestino.gridx = 6;
		gbc_cbPaisDestino.gridy = 4;
		panelBusqueda.add(cbPaisDestino, gbc_cbPaisDestino);

		JLabel lblContinenteOrigen = new JLabel("Continente origen");
		lblContinenteOrigen.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_lblContinenteOrigen = new GridBagConstraints();
		gbc_lblContinenteOrigen.fill = GridBagConstraints.BOTH;
		gbc_lblContinenteOrigen.insets = new Insets(0, 0, 0, 5);
		gbc_lblContinenteOrigen.gridx = 2;
		gbc_lblContinenteOrigen.gridy = 5;
		panelBusqueda.add(lblContinenteOrigen, gbc_lblContinenteOrigen);

		cbContinenteOrigen = new JComboBox<String>();
		cbContinenteOrigen.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_cbContinente = new GridBagConstraints();
		gbc_cbContinente.insets = new Insets(0, 0, 0, 5);
		gbc_cbContinente.fill = GridBagConstraints.BOTH;
		gbc_cbContinente.gridx = 3;
		gbc_cbContinente.gridy = 5;
		panelBusqueda.add(cbContinenteOrigen, gbc_cbContinente);

		JLabel lblContinenteDestino = new JLabel("Continente destino");
		lblContinenteDestino.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_lblContinenteDestino = new GridBagConstraints();
		gbc_lblContinenteDestino.fill = GridBagConstraints.BOTH;
		gbc_lblContinenteDestino.insets = new Insets(0, 0, 0, 5);
		gbc_lblContinenteDestino.gridx = 5;
		gbc_lblContinenteDestino.gridy = 5;
		panelBusqueda.add(lblContinenteDestino, gbc_lblContinenteDestino);

		cbContinenteDestino = new JComboBox<String>();
		cbContinenteDestino.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_cbContinenteDestino = new GridBagConstraints();
		gbc_cbContinenteDestino.insets = new Insets(0, 0, 0, 5);
		gbc_cbContinenteDestino.fill = GridBagConstraints.BOTH;
		gbc_cbContinenteDestino.gridx = 6;
		gbc_cbContinenteDestino.gridy = 5;
		panelBusqueda.add(cbContinenteDestino, gbc_cbContinenteDestino);

		panelSur = new JPanel();
		GridBagConstraints gbc_panelSur = new GridBagConstraints();
		gbc_panelSur.fill = GridBagConstraints.BOTH;
		gbc_panelSur.gridx = 0;
		gbc_panelSur.gridy = 2;
		frmMainViajes.add(panelSur, gbc_panelSur);
		panelSur.setLayout(new CardLayout(0, 0));

		JScrollPane spViajes = new JScrollPane();
		panelSur.add(spViajes, "name_99553900724405");
		modelViajes = new DefaultTableModel(null, nombreColumnas) {
			private static final long serialVersionUID = 1L;

			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		tablaViajes = new JTable(modelViajes);
		tablaViajes.getTableHeader().setReorderingAllowed(false);
		spViajes.setViewportView(tablaViajes);

		this.frmMainViajes.setVisible(false);
	}

	public void mostrarVentana() {
		this.frmMainViajes.setVisible(true);
	}

	public JButton getBtnAgregar() {
		return btnAgregar;
	}

	public JButton getBtnBorrar() {
		return btnCancelar;
	}

	public JButton getBtnEditar() {
		return btnEditar;
	}

	public JButton getBtnRealizarBusqueda() {
		return btnRealizarBusqueda;
	}

	public DefaultTableModel getModelViajes() {
		return modelViajes;
	}

	public JTable getTablaViajes() {
		return tablaViajes;
	}

	public String[] getNombreColumnas() {
		return nombreColumnas;
	}

	public JPanel getPanel() {
		return frmMainViajes;
	}

	public JRadioButton getRadioAbierto() {
		return radioAbierto;
	}

	public JRadioButton getRadioFinalizado() {
		return radioFinalizado;
	}

	public JRadioButton getRadioTodos() {
		return radioTodos;
	}

	public JTextField getTxtCiudadOrigen() {
		return txtCiudadOrigen;
	}

	public JTextField getTxtBusquedaCodigo() {
		return txtBusquedaCodigo;
	}

	public JTextField getTxtProvinciaOrigen() {
		return txtProvinciaOrigen;
	}

	public JTextField getTxtCiudadDestino() {
		return txtCiudadDestino;
	}

	public JTextField getTxtProvinciaDestino() {
		return txtProvinciaDestino;
	}

	public JComboBox<String> getCbPaisOrigen() {
		return cbPaisOrigen;
	}

	public JComboBox<String> getCbContinenteOrigen() {
		return cbContinenteOrigen;
	}

	public JComboBox<String> getCbPaisDestino() {
		return cbPaisDestino;
	}

	public JComboBox<String> getCbContinenteDestino() {
		return cbContinenteDestino;
	}

	public JRadioButton getRadioCancelado() {
		return radioCancelado;
	}
}
