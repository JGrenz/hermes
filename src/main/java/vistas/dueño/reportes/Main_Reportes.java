package vistas.dueño.reportes;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.SwingConstants;

import com.toedter.calendar.JDateChooser;

public class Main_Reportes {

	private static Main_Reportes INSTANCE;
	private JPanel frmMainReportes, panel;
	private JRadioButton radioEmpleados, radioPasajes, radioRankingDeVentas, radioEgresos, radioIngresos;
	private JRadioButton radioLocal, radioGeneralEmpresa;
	private JDateChooser txtFechaHasta, txtFechaDesde;
	private JComboBox<String> cbLocales, cbTipoPasajes, cbTipoEgresos, cbTipoIngresos;
	private ButtonGroup buttonGroupReportes, buttonGroupAgrupacion;
	private JButton btnConfirmar;
	private JLabel lblRangoDeFechas, lblDesde, lblHasta;

	public static Main_Reportes getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new Main_Reportes();
		}
		return INSTANCE;
	}

	public Main_Reportes() {

		buttonGroupReportes = new ButtonGroup();
		buttonGroupAgrupacion = new ButtonGroup();

		frmMainReportes = new JPanel();

		GridBagLayout gbl_frmMainReportes = new GridBagLayout();
		gbl_frmMainReportes.columnWidths = new int[] { 0, 0 };
		gbl_frmMainReportes.rowHeights = new int[] { 0, 0 };
		gbl_frmMainReportes.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gbl_frmMainReportes.rowWeights = new double[] { 1.0, Double.MIN_VALUE };
		frmMainReportes.setLayout(gbl_frmMainReportes);

		panel = new JPanel();
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.fill = GridBagConstraints.BOTH;
		gbc_panel.gridx = 0;
		gbc_panel.gridy = 0;
		frmMainReportes.add(panel, gbc_panel);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[] { 30, 0, 30, 0, 30, 30, 30, 0, 150, 30, 30, 0 };
		gbl_panel.rowHeights = new int[] { 30, 30, 30, 30, 30, 30, 30, 30, 30, 0, 0 };
		gbl_panel.columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
				Double.MIN_VALUE };
		gbl_panel.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		panel.setLayout(gbl_panel);

		JLabel lblGeneracinDeReportes = new JLabel("Generación de reportes");
		GridBagConstraints gbc_lblGeneracinDeReportes = new GridBagConstraints();
		gbc_lblGeneracinDeReportes.insets = new Insets(0, 0, 5, 5);
		gbc_lblGeneracinDeReportes.fill = GridBagConstraints.BOTH;
		gbc_lblGeneracinDeReportes.gridx = 1;
		gbc_lblGeneracinDeReportes.gridy = 1;
		panel.add(lblGeneracinDeReportes, gbc_lblGeneracinDeReportes);

		JLabel lblReporteDe = new JLabel("Reporte de:");
		lblReporteDe.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_lblReporteDe = new GridBagConstraints();
		gbc_lblReporteDe.fill = GridBagConstraints.BOTH;
		gbc_lblReporteDe.insets = new Insets(0, 0, 5, 5);
		gbc_lblReporteDe.gridx = 1;
		gbc_lblReporteDe.gridy = 3;
		panel.add(lblReporteDe, gbc_lblReporteDe);

		JLabel lblAgruparPor = new JLabel("Agrupar por:");
		lblAgruparPor.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_lblAgruparPor = new GridBagConstraints();
		gbc_lblAgruparPor.fill = GridBagConstraints.BOTH;
		gbc_lblAgruparPor.insets = new Insets(0, 0, 5, 5);
		gbc_lblAgruparPor.gridx = 3;
		gbc_lblAgruparPor.gridy = 3;
		panel.add(lblAgruparPor, gbc_lblAgruparPor);

		lblRangoDeFechas = new JLabel("Rango de fechas:");
		lblRangoDeFechas.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_lblRangoDeFechas = new GridBagConstraints();
		gbc_lblRangoDeFechas.fill = GridBagConstraints.BOTH;
		gbc_lblRangoDeFechas.insets = new Insets(0, 0, 5, 5);
		gbc_lblRangoDeFechas.gridx = 7;
		gbc_lblRangoDeFechas.gridy = 3;
		panel.add(lblRangoDeFechas, gbc_lblRangoDeFechas);

		radioEmpleados = new JRadioButton("Empleados");
		radioEmpleados.setSelected(true);
		buttonGroupReportes.add(radioEmpleados);
		radioEmpleados.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (radioEmpleados.isSelected()) {
					noMostrarCB();
					mostrarRangosFechas(false);
				}
			}
		});
		GridBagConstraints gbc_radioEmpleados = new GridBagConstraints();
		gbc_radioEmpleados.fill = GridBagConstraints.BOTH;
		gbc_radioEmpleados.insets = new Insets(0, 0, 5, 5);
		gbc_radioEmpleados.gridx = 1;
		gbc_radioEmpleados.gridy = 4;
		panel.add(radioEmpleados, gbc_radioEmpleados);

		radioLocal = new JRadioButton("Local");
		radioLocal.setSelected(true);
		buttonGroupAgrupacion.add(radioLocal);
		radioLocal.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (radioLocal.isSelected()) {
					cbLocales.setEnabled(true);
				}
			}
		});
		GridBagConstraints gbc_rdbtnPorLocal = new GridBagConstraints();
		gbc_rdbtnPorLocal.fill = GridBagConstraints.BOTH;
		gbc_rdbtnPorLocal.insets = new Insets(0, 0, 5, 5);
		gbc_rdbtnPorLocal.gridx = 3;
		gbc_rdbtnPorLocal.gridy = 4;
		panel.add(radioLocal, gbc_rdbtnPorLocal);

		cbLocales = new JComboBox<String>();
		GridBagConstraints gbc_cbLocales = new GridBagConstraints();
		gbc_cbLocales.insets = new Insets(0, 0, 5, 5);
		gbc_cbLocales.fill = GridBagConstraints.BOTH;
		gbc_cbLocales.gridx = 5;
		gbc_cbLocales.gridy = 4;
		panel.add(cbLocales, gbc_cbLocales);
		
		lblDesde = new JLabel("Desde:");
		lblDesde.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_lblDesde = new GridBagConstraints();
		gbc_lblDesde.fill = GridBagConstraints.BOTH;
		gbc_lblDesde.insets = new Insets(0, 0, 5, 5);
		gbc_lblDesde.gridx = 7;
		gbc_lblDesde.gridy = 4;
		panel.add(lblDesde, gbc_lblDesde);

		txtFechaDesde = new JDateChooser();
		txtFechaDesde.setMaxSelectableDate(new Date());
		GridBagConstraints gbc_txtFechaDesde = new GridBagConstraints();
		gbc_txtFechaDesde.fill = GridBagConstraints.BOTH;
		gbc_txtFechaDesde.insets = new Insets(0, 0, 5, 5);
		gbc_txtFechaDesde.gridx = 8;
		gbc_txtFechaDesde.gridy = 4;
		panel.add(txtFechaDesde, gbc_txtFechaDesde);

		cbTipoPasajes = new JComboBox<String>();
		GridBagConstraints gbc_cbTipoPasajes = new GridBagConstraints();
		gbc_cbTipoPasajes.insets = new Insets(0, 0, 5, 0);
		gbc_cbTipoPasajes.fill = GridBagConstraints.BOTH;
		gbc_cbTipoPasajes.gridx = 10;
		gbc_cbTipoPasajes.gridy = 4;
		panel.add(cbTipoPasajes, gbc_cbTipoPasajes);
		cbTipoPasajes.addItem("Ambos");
		cbTipoPasajes.addItem("Reservado");
		cbTipoPasajes.addItem("Pagado");

		cbTipoEgresos = new JComboBox<String>();
		GridBagConstraints gbc_cbTipoEgresos = new GridBagConstraints();
		gbc_cbTipoEgresos.insets = new Insets(0, 0, 5, 0);
		gbc_cbTipoEgresos.fill = GridBagConstraints.BOTH;
		gbc_cbTipoEgresos.gridx = 10;
		gbc_cbTipoEgresos.gridy = 4;
		panel.add(cbTipoEgresos, gbc_cbTipoEgresos);
		cbTipoEgresos.addItem("Todos");
		cbTipoEgresos.addItem("Servicio");
		cbTipoEgresos.addItem("Sueldo");
		cbTipoEgresos.addItem("Consumos_varios");
		cbTipoEgresos.addItem("Devolucion");

		cbTipoIngresos = new JComboBox<String>();
		GridBagConstraints gbc_cbTipoIngresos = new GridBagConstraints();
		gbc_cbTipoIngresos.insets = new Insets(0, 0, 5, 0);
		gbc_cbTipoIngresos.fill = GridBagConstraints.BOTH;
		gbc_cbTipoIngresos.gridx = 10;
		gbc_cbTipoIngresos.gridy = 4;
		panel.add(cbTipoIngresos, gbc_cbTipoIngresos);
		cbTipoIngresos.addItem("Viaje");
		cbTipoIngresos.addItem("Cliente");

		radioRankingDeVentas = new JRadioButton("Ranking de ventas");
		buttonGroupReportes.add(radioRankingDeVentas);
		radioRankingDeVentas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (radioRankingDeVentas.isSelected()) {
					noMostrarCB();
					mostrarRangosFechas(true);
				}
			}
		});
		GridBagConstraints gbc_radioRankingDeVentas = new GridBagConstraints();
		gbc_radioRankingDeVentas.fill = GridBagConstraints.BOTH;
		gbc_radioRankingDeVentas.insets = new Insets(0, 0, 5, 5);
		gbc_radioRankingDeVentas.gridx = 1;
		gbc_radioRankingDeVentas.gridy = 5;
		panel.add(radioRankingDeVentas, gbc_radioRankingDeVentas);

		radioGeneralEmpresa = new JRadioButton("General empresa");
		buttonGroupAgrupacion.add(radioGeneralEmpresa);
		radioGeneralEmpresa.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (radioGeneralEmpresa.isSelected()) {
					cbLocales.setEnabled(false);
				}
			}
		});
		GridBagConstraints gbc_rdbtnGeneralEmpresa = new GridBagConstraints();
		gbc_rdbtnGeneralEmpresa.fill = GridBagConstraints.BOTH;
		gbc_rdbtnGeneralEmpresa.insets = new Insets(0, 0, 5, 5);
		gbc_rdbtnGeneralEmpresa.gridx = 3;
		gbc_rdbtnGeneralEmpresa.gridy = 5;
		panel.add(radioGeneralEmpresa, gbc_rdbtnGeneralEmpresa);
		
		lblHasta = new JLabel("Hasta:");
		lblHasta.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_lblHasta = new GridBagConstraints();
		gbc_lblHasta.fill = GridBagConstraints.BOTH;
		gbc_lblHasta.insets = new Insets(0, 0, 5, 5);
		gbc_lblHasta.gridx = 7;
		gbc_lblHasta.gridy = 5;
		panel.add(lblHasta, gbc_lblHasta);

		txtFechaHasta = new JDateChooser();
		txtFechaHasta.setDate(new Date());
		txtFechaHasta.setMaxSelectableDate(new Date());
		GridBagConstraints gbc_txtFechaHasta = new GridBagConstraints();
		gbc_txtFechaHasta.fill = GridBagConstraints.BOTH;
		gbc_txtFechaHasta.insets = new Insets(0, 0, 5, 5);
		gbc_txtFechaHasta.gridx = 8;
		gbc_txtFechaHasta.gridy = 5;
		panel.add(txtFechaHasta, gbc_txtFechaHasta);

		radioEgresos = new JRadioButton("Egresos");
		buttonGroupReportes.add(radioEgresos);
		radioEgresos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (radioEgresos.isSelected()) {
					mostrarCBEgresos();
					mostrarRangosFechas(true);
				}
			}
		});
		GridBagConstraints gbc_radioEgresos = new GridBagConstraints();
		gbc_radioEgresos.fill = GridBagConstraints.BOTH;
		gbc_radioEgresos.insets = new Insets(0, 0, 5, 5);
		gbc_radioEgresos.gridx = 1;
		gbc_radioEgresos.gridy = 6;
		panel.add(radioEgresos, gbc_radioEgresos);

		radioIngresos = new JRadioButton("Ingresos");
		buttonGroupReportes.add(radioIngresos);
		radioIngresos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (radioIngresos.isSelected()) {
					mostrarCBIngresos();
					mostrarRangosFechas(true);
				}
			}
		});
		GridBagConstraints gbc_radioIngresos = new GridBagConstraints();
		gbc_radioIngresos.insets = new Insets(0, 0, 5, 5);
		gbc_radioIngresos.fill = GridBagConstraints.BOTH;
		gbc_radioIngresos.gridx = 1;
		gbc_radioIngresos.gridy = 7;
		panel.add(radioIngresos, gbc_radioIngresos);

		radioPasajes = new JRadioButton("Pasajes");
		buttonGroupReportes.add(radioPasajes);
		radioPasajes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (radioPasajes.isSelected()) {
					mostrarCBPasajes();
					mostrarRangosFechas(true);
				}
			}
		});
		GridBagConstraints gbc_radioPasajes = new GridBagConstraints();
		gbc_radioPasajes.insets = new Insets(0, 0, 5, 5);
		gbc_radioPasajes.fill = GridBagConstraints.BOTH;
		gbc_radioPasajes.gridx = 1;
		gbc_radioPasajes.gridy = 8;
		panel.add(radioPasajes, gbc_radioPasajes);

		btnConfirmar = new JButton("Confirmar");
		GridBagConstraints gbc_btnConfirmar = new GridBagConstraints();
		gbc_btnConfirmar.insets = new Insets(0, 0, 0, 5);
		gbc_btnConfirmar.fill = GridBagConstraints.BOTH;
		gbc_btnConfirmar.gridx = 8;
		gbc_btnConfirmar.gridy = 9;
		panel.add(btnConfirmar, gbc_btnConfirmar);

		noMostrarCB();
		mostrarRangosFechas(false);
		this.frmMainReportes.setVisible(false);
	}

	public void mostrarVentana() {
		this.frmMainReportes.setVisible(true);
	}

	public JPanel getPanel() {
		return frmMainReportes;
	}

	public JButton getBtnConfirmar() {
		return btnConfirmar;
	}

	public JRadioButton getRadioEmpleados() {
		return radioEmpleados;
	}

	public JRadioButton getRadioPasajes() {
		return radioPasajes;
	}

	public JRadioButton getRadioRankingDeVentas() {
		return radioRankingDeVentas;
	}

	public JRadioButton getRadioEgresos() {
		return radioEgresos;
	}

	public JRadioButton getRadioIngresos() {
		return radioIngresos;
	}

	public JRadioButton getRadioLocal() {
		return radioLocal;
	}

	public JRadioButton getRadioGeneralEmpresaa() {
		return radioGeneralEmpresa;
	}

	public JDateChooser getTxtDesde() {
		return txtFechaDesde;
	}

	public JDateChooser getTxtHasta() {
		return txtFechaHasta;
	}

	public JComboBox<String> getCBLocales() {
		return cbLocales;
	}

	public JComboBox<String> getCBTipoPasajes(){
		return cbTipoPasajes;
	}
	
	public JComboBox<String> getCBTipoEgresos(){
		return cbTipoEgresos;
	}
	
	public JComboBox<String> getCBTipoIngresos(){
		return cbTipoIngresos;
	}

	public void mostrarCBPasajes() {
		cbTipoEgresos.setVisible(false);
		cbTipoIngresos.setVisible(false);
		cbTipoPasajes.setVisible(true);
	}

	public void mostrarCBEgresos() {
		cbTipoEgresos.setVisible(true);
		cbTipoIngresos.setVisible(false);
		cbTipoPasajes.setVisible(false);
	}

	public void mostrarCBIngresos() {
		cbTipoEgresos.setVisible(false);
		cbTipoIngresos.setVisible(true);
		cbTipoPasajes.setVisible(false);
	}

	public void noMostrarCB() {
		cbTipoEgresos.setVisible(false);
		cbTipoIngresos.setVisible(false);
		cbTipoPasajes.setVisible(false);
	}
	
	public void mostrarRangosFechas(boolean bool) {
		lblRangoDeFechas.setEnabled(bool);
		lblDesde.setEnabled(bool);
		lblHasta.setEnabled(bool);
		txtFechaHasta.setEnabled(bool);
		txtFechaDesde.setEnabled(bool);
	}
}