package vistas.perfil;

import java.awt.Dialog.ModalityType;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import com.toedter.calendar.JDateChooser;
import com.toedter.calendar.JTextFieldDateEditor;

import vistas.ValidacionCampos;

public class Ventana_EditarUsuario {

	private static Ventana_EditarUsuario INSTANCE;
	private JDialog frmEditarCliente;
	private JPanel contentPane;
	private JTextField txtNombre, txtApellido, txtDni, txtEmail, txtUsuario, txtTelefono1, txtTelefono2, txtTelefono3,
			txtTelefono4, txtTelefono5;
	private JDateChooser txtNacimiento;
	private JButton btnConfirmar, btnContraseña;

	public static Ventana_EditarUsuario getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new Ventana_EditarUsuario();
		}
		return INSTANCE;
	}

	private Ventana_EditarUsuario() {

		frmEditarCliente = new JDialog();
		frmEditarCliente.setIconImage(Toolkit.getDefaultToolkit()
				.getImage(Ventana_EditarUsuario.class.getResource("/imagenes/hermesicono.png")));
		frmEditarCliente.setModalityType(ModalityType.APPLICATION_MODAL);
		frmEditarCliente.setResizable(false);
		frmEditarCliente.setTitle("Hermes - Editar cliente");
		frmEditarCliente.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmEditarCliente.setBounds(100, 100, 937, 384);
		frmEditarCliente.setLocationRelativeTo(null);

		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		frmEditarCliente.setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[] { 492, 308, 0, 0 };
		gbl_contentPane.rowHeights = new int[] { 216, 30, 30, 30, 0 };
		gbl_contentPane.columnWeights = new double[] { 1.0, 0.0, 0.0, Double.MIN_VALUE };
		gbl_contentPane.rowWeights = new double[] { 0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE };
		contentPane.setLayout(gbl_contentPane);

		JPanel panelDatos = new JPanel();
		GridBagConstraints gbc_panelDatos = new GridBagConstraints();
		gbc_panelDatos.gridheight = 2;
		gbc_panelDatos.fill = GridBagConstraints.BOTH;
		gbc_panelDatos.insets = new Insets(0, 0, 5, 5);
		gbc_panelDatos.gridx = 0;
		gbc_panelDatos.gridy = 0;
		contentPane.add(panelDatos, gbc_panelDatos);
		GridBagLayout gbl_panelDatos = new GridBagLayout();
		gbl_panelDatos.columnWidths = new int[] { 133, 267, 0 };
		gbl_panelDatos.rowHeights = new int[] { 30, 30, 30, 30, 30, 30, 30, 0 };
		gbl_panelDatos.columnWeights = new double[] { 1.0, 1.0, Double.MIN_VALUE };
		gbl_panelDatos.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		panelDatos.setLayout(gbl_panelDatos);

		
		JLabel lblNombre = new JLabel("Nombre:");
		lblNombre.setPreferredSize(new Dimension(0, 30));
		lblNombre.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_lblNombre = new GridBagConstraints();
		gbc_lblNombre.fill = GridBagConstraints.BOTH;
		gbc_lblNombre.insets = new Insets(5, 0, 5, 5);
		gbc_lblNombre.gridx = 0;
		gbc_lblNombre.gridy = 0;
		panelDatos.add(lblNombre, gbc_lblNombre);

		txtNombre = new JTextField();
		txtNombre.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_txtNombre = new GridBagConstraints();
		gbc_txtNombre.fill = GridBagConstraints.BOTH;
		gbc_txtNombre.insets = new Insets(5, 0, 5, 0);
		gbc_txtNombre.gridx = 1;
		gbc_txtNombre.gridy = 0;
		panelDatos.add(txtNombre, gbc_txtNombre);
		txtNombre.setColumns(10);

		JLabel lblApellido = new JLabel("Apellido:");
		lblApellido.setPreferredSize(new Dimension(0, 30));
		lblApellido.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_lblApellido = new GridBagConstraints();
		gbc_lblApellido.fill = GridBagConstraints.BOTH;
		gbc_lblApellido.insets = new Insets(5, 5, 5, 5);
		gbc_lblApellido.gridx = 0;
		gbc_lblApellido.gridy = 1;
		panelDatos.add(lblApellido, gbc_lblApellido);
		
		txtApellido = new JTextField();
		txtApellido.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_txtApellido = new GridBagConstraints();
		gbc_txtApellido.fill = GridBagConstraints.BOTH;
		gbc_txtApellido.insets = new Insets(5, 0, 5, 0);
		gbc_txtApellido.gridx = 1;
		gbc_txtApellido.gridy = 1;
		panelDatos.add(txtApellido, gbc_txtApellido);
		txtApellido.setColumns(10);

		JLabel lblDni = new JLabel("DNI:");
		lblDni.setPreferredSize(new Dimension(0, 30));
		lblDni.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_lblDni = new GridBagConstraints();
		gbc_lblDni.fill = GridBagConstraints.BOTH;
		gbc_lblDni.insets = new Insets(5, 5, 5, 5);
		gbc_lblDni.gridx = 0;
		gbc_lblDni.gridy = 2;
		panelDatos.add(lblDni, gbc_lblDni);

		txtDni = new JTextField();
		txtDni.setPreferredSize(new Dimension(0, 30));
		txtDni.setColumns(10);
		GridBagConstraints gbc_txtDni = new GridBagConstraints();
		gbc_txtDni.fill = GridBagConstraints.BOTH;
		gbc_txtDni.insets = new Insets(5, 0, 5, 0);
		gbc_txtDni.gridx = 1;
		gbc_txtDni.gridy = 2;
		panelDatos.add(txtDni, gbc_txtDni);

		JLabel lblEmail = new JLabel("E-Mail:");
		lblEmail.setPreferredSize(new Dimension(0, 30));
		lblEmail.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_lblEmail = new GridBagConstraints();
		gbc_lblEmail.fill = GridBagConstraints.BOTH;
		gbc_lblEmail.insets = new Insets(5, 5, 5, 5);
		gbc_lblEmail.gridx = 0;
		gbc_lblEmail.gridy = 3;
		panelDatos.add(lblEmail, gbc_lblEmail);

		txtEmail = new JTextField();
		txtEmail.setPreferredSize(new Dimension(0, 30));
		txtEmail.setColumns(10);
		GridBagConstraints gbc_txtEmail = new GridBagConstraints();
		gbc_txtEmail.fill = GridBagConstraints.BOTH;
		gbc_txtEmail.insets = new Insets(5, 0, 5, 0);
		gbc_txtEmail.gridx = 1;
		gbc_txtEmail.gridy = 3;
		panelDatos.add(txtEmail, gbc_txtEmail);

		JLabel lblNacimiento = new JLabel("Fecha de nacimiento:");
		lblNacimiento.setPreferredSize(new Dimension(0, 30));
		lblNacimiento.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_lblNacimiento = new GridBagConstraints();
		gbc_lblNacimiento.fill = GridBagConstraints.BOTH;
		gbc_lblNacimiento.insets = new Insets(5, 5, 5, 5);
		gbc_lblNacimiento.gridx = 0;
		gbc_lblNacimiento.gridy = 4;
		panelDatos.add(lblNacimiento, gbc_lblNacimiento);

		txtNacimiento = new JDateChooser();
		txtNacimiento.setPreferredSize(new Dimension(0, 30));
		txtNacimiento.setMaxSelectableDate(new Date());
		JTextFieldDateEditor editor = (JTextFieldDateEditor) txtNacimiento.getDateEditor();
		editor.setEditable(false);
		GridBagConstraints gbc_txtNacimiento = new GridBagConstraints();
		gbc_txtNacimiento.fill = GridBagConstraints.BOTH;
		gbc_txtNacimiento.insets = new Insets(5, 0, 5, 0);
		gbc_txtNacimiento.gridx = 1;
		gbc_txtNacimiento.gridy = 4;
		panelDatos.add(txtNacimiento, gbc_txtNacimiento);

		JLabel lblUsuario = new JLabel("Usuario:");
		lblUsuario.setPreferredSize(new Dimension(0, 30));
		lblUsuario.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_lblUsuario = new GridBagConstraints();
		gbc_lblUsuario.fill = GridBagConstraints.BOTH;
		gbc_lblUsuario.insets = new Insets(5, 5, 5, 5);
		gbc_lblUsuario.gridx = 0;
		gbc_lblUsuario.gridy = 5;
		panelDatos.add(lblUsuario, gbc_lblUsuario);

		txtUsuario = new JTextField();
		txtUsuario.setPreferredSize(new Dimension(0, 30));
		txtUsuario.setColumns(10);
		GridBagConstraints gbc_txtUsuario = new GridBagConstraints();
		gbc_txtUsuario.fill = GridBagConstraints.BOTH;
		gbc_txtUsuario.insets = new Insets(5, 0, 5, 0);
		gbc_txtUsuario.gridx = 1;
		gbc_txtUsuario.gridy = 5;
		panelDatos.add(txtUsuario, gbc_txtUsuario);

		btnContraseña = new JButton("Editar contraseña");
		btnContraseña.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_btnContraseña = new GridBagConstraints();
		gbc_btnContraseña.fill = GridBagConstraints.BOTH;
		gbc_btnContraseña.insets = new Insets(5, 5, 0, 5);
		gbc_btnContraseña.gridx = 0;
		gbc_btnContraseña.gridy = 6;
		panelDatos.add(btnContraseña, gbc_btnContraseña);

		JPanel panelTelefonos = new JPanel();
		GridBagConstraints gbc_panelTelefonos = new GridBagConstraints();
		gbc_panelTelefonos.gridwidth = 2;
		gbc_panelTelefonos.fill = GridBagConstraints.BOTH;
		gbc_panelTelefonos.insets = new Insets(0, 0, 5, 0);
		gbc_panelTelefonos.gridx = 1;
		gbc_panelTelefonos.gridy = 0;
		contentPane.add(panelTelefonos, gbc_panelTelefonos);
		GridBagLayout gbl_panelTelefonos = new GridBagLayout();
		gbl_panelTelefonos.columnWidths = new int[] { 133, 267, 0 };
		gbl_panelTelefonos.rowHeights = new int[] { 30, 30, 30, 30, 30, 0 };
		gbl_panelTelefonos.columnWeights = new double[] { 1.0, 1.0, Double.MIN_VALUE };
		gbl_panelTelefonos.rowWeights = new double[] { 1.0, 1.0, 1.0, 1.0, 1.0, Double.MIN_VALUE };
		panelTelefonos.setLayout(gbl_panelTelefonos);

		JLabel lblTelefono1 = new JLabel("Teléfono 1:");
		lblTelefono1.setPreferredSize(new Dimension(0, 30));
		lblTelefono1.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_lblTelefono1 = new GridBagConstraints();
		gbc_lblTelefono1.fill = GridBagConstraints.BOTH;
		gbc_lblTelefono1.insets = new Insets(5, 5, 5, 5);
		gbc_lblTelefono1.gridx = 0;
		gbc_lblTelefono1.gridy = 0;
		panelTelefonos.add(lblTelefono1, gbc_lblTelefono1);

		txtTelefono1 = new JTextField();
		txtTelefono1.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_txtTelefono1 = new GridBagConstraints();
		gbc_txtTelefono1.fill = GridBagConstraints.BOTH;
		gbc_txtTelefono1.insets = new Insets(5, 5, 5, 5);
		gbc_txtTelefono1.gridx = 1;
		gbc_txtTelefono1.gridy = 0;
		panelTelefonos.add(txtTelefono1, gbc_txtTelefono1);
		txtTelefono1.setColumns(10);

		JLabel lblTelefono2 = new JLabel("Teléfono 2:");
		lblTelefono2.setPreferredSize(new Dimension(0, 30));
		lblTelefono2.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_lblTelefono2 = new GridBagConstraints();
		gbc_lblTelefono2.fill = GridBagConstraints.BOTH;
		gbc_lblTelefono2.insets = new Insets(5, 5, 5, 5);
		gbc_lblTelefono2.gridx = 0;
		gbc_lblTelefono2.gridy = 1;
		panelTelefonos.add(lblTelefono2, gbc_lblTelefono2);

		txtTelefono2 = new JTextField();
		txtTelefono2.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_txtTelefono2 = new GridBagConstraints();
		gbc_txtTelefono2.fill = GridBagConstraints.BOTH;
		gbc_txtTelefono2.insets = new Insets(5, 5, 5, 5);
		gbc_txtTelefono2.gridx = 1;
		gbc_txtTelefono2.gridy = 1;
		panelTelefonos.add(txtTelefono2, gbc_txtTelefono2);
		txtTelefono2.setColumns(10);

		JLabel lblTelefono3 = new JLabel("Teléfono 3:");
		lblTelefono3.setPreferredSize(new Dimension(0, 30));
		lblTelefono3.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_lblTelefono3 = new GridBagConstraints();
		gbc_lblTelefono3.fill = GridBagConstraints.BOTH;
		gbc_lblTelefono3.insets = new Insets(5, 5, 5, 5);
		gbc_lblTelefono3.gridx = 0;
		gbc_lblTelefono3.gridy = 2;
		panelTelefonos.add(lblTelefono3, gbc_lblTelefono3);

		txtTelefono3 = new JTextField();
		txtTelefono3.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_txtTelefono3 = new GridBagConstraints();
		gbc_txtTelefono3.fill = GridBagConstraints.BOTH;
		gbc_txtTelefono3.insets = new Insets(5, 5, 5, 5);
		gbc_txtTelefono3.gridx = 1;
		gbc_txtTelefono3.gridy = 2;
		panelTelefonos.add(txtTelefono3, gbc_txtTelefono3);
		txtTelefono3.setColumns(10);

		JLabel lblTelefono4 = new JLabel("Teléfono 4:");
		lblTelefono4.setPreferredSize(new Dimension(0, 30));
		lblTelefono4.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_lblTelefono4 = new GridBagConstraints();
		gbc_lblTelefono4.fill = GridBagConstraints.BOTH;
		gbc_lblTelefono4.insets = new Insets(5, 5, 5, 5);
		gbc_lblTelefono4.gridx = 0;
		gbc_lblTelefono4.gridy = 3;
		panelTelefonos.add(lblTelefono4, gbc_lblTelefono4);

		txtTelefono4 = new JTextField();
		txtTelefono4.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_txtTelefono4 = new GridBagConstraints();
		gbc_txtTelefono4.fill = GridBagConstraints.BOTH;
		gbc_txtTelefono4.insets = new Insets(5, 5, 5, 5);
		gbc_txtTelefono4.gridx = 1;
		gbc_txtTelefono4.gridy = 3;
		panelTelefonos.add(txtTelefono4, gbc_txtTelefono4);
		txtTelefono4.setColumns(10);

		JLabel lblTelefono5 = new JLabel("Teléfono 5:");
		lblTelefono5.setPreferredSize(new Dimension(0, 30));
		lblTelefono5.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_lblTelefono5 = new GridBagConstraints();
		gbc_lblTelefono5.fill = GridBagConstraints.BOTH;
		gbc_lblTelefono5.insets = new Insets(5, 5, 5, 5);
		gbc_lblTelefono5.gridx = 0;
		gbc_lblTelefono5.gridy = 4;
		panelTelefonos.add(lblTelefono5, gbc_lblTelefono5);

		txtTelefono5 = new JTextField();
		txtTelefono5.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_txtTelefono5 = new GridBagConstraints();
		gbc_txtTelefono5.fill = GridBagConstraints.BOTH;
		gbc_txtTelefono5.insets = new Insets(5, 5, 5, 5);
		gbc_txtTelefono5.gridx = 1;
		gbc_txtTelefono5.gridy = 4;
		panelTelefonos.add(txtTelefono5, gbc_txtTelefono5);
		txtTelefono5.setColumns(10);

		btnConfirmar = new JButton("Confirmar");
		btnConfirmar.setPreferredSize(new Dimension(100, 30));
		GridBagConstraints gbc_btnConfirmar = new GridBagConstraints();
		gbc_btnConfirmar.fill = GridBagConstraints.BOTH;
		gbc_btnConfirmar.insets = new Insets(5, 5, 0, 0);
		gbc_btnConfirmar.gridx = 2;
		gbc_btnConfirmar.gridy = 3;
		contentPane.add(btnConfirmar, gbc_btnConfirmar);

		this.frmEditarCliente.setVisible(false);
	}

	public void mostrarVentana() {
		this.frmEditarCliente.setVisible(true);
	}

	public JTextField getTelefono1() {
		return txtTelefono1;
	}

	public JTextField getTelefono2() {
		return txtTelefono2;
	}

	public JTextField getTelefono3() {
		return txtTelefono3;
	}

	public JTextField getTelefono4() {
		return txtTelefono4;
	}

	public JTextField getTelefono5() {
		return txtTelefono5;
	}

	public JTextField getTxtNombre() {
		return txtNombre;
	}

	public JTextField getTxtApellido() {
		return txtApellido;
	}

	public JTextField getTxtDni() {
		return txtDni;
	}

	public JTextField getTxtEmail() {
		return txtEmail;
	}
	
	public JDateChooser getNacimientoChooser() {
		return txtNacimiento;
	}

	public JTextField getTxtUsuario() {
		return txtUsuario;
	}

	public LocalDate getTxtNacimiento() {
		Date date = txtNacimiento.getDate();
		return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
	}

	public JButton getBtnConfirmar() {
		return btnConfirmar;
	}
	
	public JButton getBtnContraseña() {
		return btnContraseña;
	}

	public void cerrar() {
		this.txtNombre.setText(null);
		this.txtApellido.setText(null);
		this.txtDni.setText(null);
		this.txtEmail.setText(null);
		this.txtUsuario.setText(null);
		this.frmEditarCliente.dispose();
	}

	private boolean camposVacios() {
		if (txtNombre.getText().isEmpty() || txtApellido.getText().isEmpty() || txtDni.getText().isEmpty() ||
			txtEmail.getText().isEmpty() || txtUsuario.getText().isEmpty())
		{
			JOptionPane.showMessageDialog(null, "Los campos Nombre, Apellido, DNI, Email y Usuario son obligatorios.");
			return true;
		}
		else
			return false;
	}
	
	private boolean camposCorrectos() {
		ValidacionCampos validador = new ValidacionCampos();
		if(!validador.esPalabra(txtNombre.getText()) || !validador.esPalabra(txtApellido.getText())
			|| !validador.esNumerico(txtDni.getText()) || !validador.esMail(txtEmail.getText()) 
			|| !validador.esTelefono(txtTelefono1.getText()) || !validador.esTelefono(txtTelefono2.getText())
			|| !validador.esTelefono(txtTelefono3.getText()) || !validador.esTelefono(txtTelefono4.getText())
			|| !validador.esTelefono(txtTelefono5.getText()))
			return false;
		
		else
			return true;
	}
	
	public boolean verificarCampo() {
		if (!camposVacios() && camposCorrectos())
			return true;
		else
			return false;
	}

}
