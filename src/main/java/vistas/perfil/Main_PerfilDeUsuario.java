package vistas.perfil;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.RowSorter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

public class Main_PerfilDeUsuario {

	private static Main_PerfilDeUsuario INSTANCE;
	private JPanel frmMainClientes;
	private JTable tablaPasajes;
	private DefaultTableModel modelPasajes;
	private String[] nombreColumnasPasaje = { "Codigo de referencia", "Origen", "Destino", "Fecha y hora de salida",
			"Fecha y hora de llegada", "Transporte", "Precio del viaje", "Monto abonado", "Estado" };
	private JButton btnEditar;
	private JPanel panelBusqueda, panelNorte, panelSur, panelDatos;
	private JTextField txtNombre, txtApellido, txtDNI, txtUsuario, txtEmail, txtNacimiento, txtPuntosAcumulados, telefono1, telefono2, telefono3, telefono4, telefono5;
	private JLabel lblPuntosAcumulados, lblHistorialDePasajes;

	public static Main_PerfilDeUsuario getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new Main_PerfilDeUsuario();
		}
		return INSTANCE;
	}

	public Main_PerfilDeUsuario() {

		frmMainClientes = new JPanel();

		GridBagLayout gbl_frmMainClientes = new GridBagLayout();
		gbl_frmMainClientes.columnWidths = new int[] { 1137, 0 };
		gbl_frmMainClientes.rowHeights = new int[] { 69, 200, 427, 0 };
		gbl_frmMainClientes.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gbl_frmMainClientes.rowWeights = new double[] { 0.0, 0.0, 0.0, Double.MIN_VALUE };
		frmMainClientes.setLayout(gbl_frmMainClientes);

		panelNorte = new JPanel();
		GridBagConstraints gbc_panelNorte = new GridBagConstraints();
		gbc_panelNorte.fill = GridBagConstraints.BOTH;
		gbc_panelNorte.insets = new Insets(0, 0, 5, 0);
		gbc_panelNorte.gridx = 0;
		gbc_panelNorte.gridy = 0;
		frmMainClientes.add(panelNorte, gbc_panelNorte);

		btnEditar = new JButton("Editar");
		btnEditar.setIcon(
				new ImageIcon(Main_PerfilDeUsuario.class.getResource("/imagenes/iconos/perfilUsuarioEditar.png")));
		panelNorte.add(btnEditar);

		panelBusqueda = new JPanel();
		panelBusqueda.setBounds(10, 51, 658, 143);
		GridBagConstraints gbc_panelBusqueda = new GridBagConstraints();
		gbc_panelBusqueda.fill = GridBagConstraints.BOTH;
		gbc_panelBusqueda.insets = new Insets(0, 0, 5, 0);
		gbc_panelBusqueda.gridx = 0;
		gbc_panelBusqueda.gridy = 1;
		frmMainClientes.add(panelBusqueda, gbc_panelBusqueda);
		panelBusqueda.setLayout(new BorderLayout(0, 0));

		panelDatos = new JPanel();
		panelBusqueda.add(panelDatos, BorderLayout.CENTER);
		GridBagLayout gbl_panelDatos = new GridBagLayout();
		gbl_panelDatos.columnWidths = new int[] { 30, 0, 30, 188, 30, 30, 89, 156, 30, 0, 127, 130, 0 };
		gbl_panelDatos.rowHeights = new int[] { 30, 30, 30, 30, 30, 0 };
		gbl_panelDatos.columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
				Double.MIN_VALUE };
		gbl_panelDatos.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		panelDatos.setLayout(gbl_panelDatos);

		JLabel lblNombre = new JLabel("Nombre:");
		GridBagConstraints gbc_lblNombre = new GridBagConstraints();
		gbc_lblNombre.fill = GridBagConstraints.BOTH;
		gbc_lblNombre.insets = new Insets(0, 0, 5, 5);
		gbc_lblNombre.gridx = 1;
		gbc_lblNombre.gridy = 0;
		panelDatos.add(lblNombre, gbc_lblNombre);

		txtNombre = new JTextField();
		txtNombre.setEditable(false);
		GridBagConstraints gbc_txtNombre = new GridBagConstraints();
		gbc_txtNombre.insets = new Insets(0, 0, 5, 5);
		gbc_txtNombre.fill = GridBagConstraints.BOTH;
		gbc_txtNombre.gridx = 3;
		gbc_txtNombre.gridy = 0;
		panelDatos.add(txtNombre, gbc_txtNombre);
		txtNombre.setColumns(10);

		JLabel lblTelefono1 = new JLabel("Teléfono 1:");
		GridBagConstraints gbc_lblTelefono1 = new GridBagConstraints();
		gbc_lblTelefono1.fill = GridBagConstraints.BOTH;
		gbc_lblTelefono1.insets = new Insets(0, 0, 5, 5);
		gbc_lblTelefono1.gridx = 6;
		gbc_lblTelefono1.gridy = 0;
		panelDatos.add(lblTelefono1, gbc_lblTelefono1);
		lblTelefono1.setPreferredSize(new Dimension(0, 30));

		telefono1 = new JTextField();
		telefono1.setEditable(false);
		GridBagConstraints gbc_telefono1 = new GridBagConstraints();
		gbc_telefono1.fill = GridBagConstraints.BOTH;
		gbc_telefono1.insets = new Insets(0, 0, 5, 5);
		gbc_telefono1.gridx = 7;
		gbc_telefono1.gridy = 0;
		panelDatos.add(telefono1, gbc_telefono1);
		telefono1.setPreferredSize(new Dimension(0, 30));

		JLabel lblUsuario = new JLabel("Usuario:");
		GridBagConstraints gbc_lblUsuario = new GridBagConstraints();
		gbc_lblUsuario.fill = GridBagConstraints.BOTH;
		gbc_lblUsuario.insets = new Insets(0, 0, 5, 5);
		gbc_lblUsuario.gridx = 10;
		gbc_lblUsuario.gridy = 0;
		panelDatos.add(lblUsuario, gbc_lblUsuario);

		txtUsuario = new JTextField();
		txtUsuario.setEditable(false);
		GridBagConstraints gbc_txtUsuario = new GridBagConstraints();
		gbc_txtUsuario.insets = new Insets(0, 0, 5, 0);
		gbc_txtUsuario.fill = GridBagConstraints.BOTH;
		gbc_txtUsuario.gridx = 11;
		gbc_txtUsuario.gridy = 0;
		panelDatos.add(txtUsuario, gbc_txtUsuario);
		txtUsuario.setColumns(10);

		JLabel lblApellido = new JLabel("Apellido:");
		GridBagConstraints gbc_lblApellido = new GridBagConstraints();
		gbc_lblApellido.fill = GridBagConstraints.BOTH;
		gbc_lblApellido.insets = new Insets(0, 0, 5, 5);
		gbc_lblApellido.gridx = 1;
		gbc_lblApellido.gridy = 1;
		panelDatos.add(lblApellido, gbc_lblApellido);

		txtApellido = new JTextField();
		txtApellido.setEditable(false);
		GridBagConstraints gbc_txtApellido = new GridBagConstraints();
		gbc_txtApellido.insets = new Insets(0, 0, 5, 5);
		gbc_txtApellido.fill = GridBagConstraints.BOTH;
		gbc_txtApellido.gridx = 3;
		gbc_txtApellido.gridy = 1;
		panelDatos.add(txtApellido, gbc_txtApellido);
		txtApellido.setColumns(10);

		JLabel lblTelefono2 = new JLabel("Teléfono 2:");
		GridBagConstraints gbc_lblTelefono2 = new GridBagConstraints();
		gbc_lblTelefono2.fill = GridBagConstraints.BOTH;
		gbc_lblTelefono2.insets = new Insets(0, 0, 5, 5);
		gbc_lblTelefono2.gridx = 6;
		gbc_lblTelefono2.gridy = 1;
		panelDatos.add(lblTelefono2, gbc_lblTelefono2);
		lblTelefono2.setPreferredSize(new Dimension(0, 30));

		telefono2 = new JTextField();
		telefono2.setEditable(false);
		GridBagConstraints gbc_telefono2 = new GridBagConstraints();
		gbc_telefono2.fill = GridBagConstraints.BOTH;
		gbc_telefono2.insets = new Insets(0, 0, 5, 5);
		gbc_telefono2.gridx = 7;
		gbc_telefono2.gridy = 1;
		panelDatos.add(telefono2, gbc_telefono2);
		telefono2.setPreferredSize(new Dimension(0, 30));

		lblPuntosAcumulados = new JLabel("Puntos acumulados:");
		GridBagConstraints gbc_lblPuntosAcumulados = new GridBagConstraints();
		gbc_lblPuntosAcumulados.fill = GridBagConstraints.BOTH;
		gbc_lblPuntosAcumulados.insets = new Insets(0, 0, 5, 5);
		gbc_lblPuntosAcumulados.gridx = 10;
		gbc_lblPuntosAcumulados.gridy = 1;
		panelDatos.add(lblPuntosAcumulados, gbc_lblPuntosAcumulados);
		lblPuntosAcumulados.setVisible(false);

		txtPuntosAcumulados = new JTextField();
		txtPuntosAcumulados.setEditable(false);
		txtPuntosAcumulados.setColumns(10);
		GridBagConstraints gbc_txtPuntosAcumulados = new GridBagConstraints();
		gbc_txtPuntosAcumulados.insets = new Insets(0, 0, 5, 0);
		gbc_txtPuntosAcumulados.fill = GridBagConstraints.BOTH;
		gbc_txtPuntosAcumulados.gridx = 11;
		gbc_txtPuntosAcumulados.gridy = 1;
		panelDatos.add(txtPuntosAcumulados, gbc_txtPuntosAcumulados);
		txtPuntosAcumulados.setVisible(false);

		JLabel lblDni = new JLabel("DNI:");
		GridBagConstraints gbc_lblDni = new GridBagConstraints();
		gbc_lblDni.fill = GridBagConstraints.BOTH;
		gbc_lblDni.insets = new Insets(0, 0, 5, 5);
		gbc_lblDni.gridx = 1;
		gbc_lblDni.gridy = 2;
		panelDatos.add(lblDni, gbc_lblDni);

		txtDNI = new JTextField();
		txtDNI.setEditable(false);
		GridBagConstraints gbc_txtDNI = new GridBagConstraints();
		gbc_txtDNI.insets = new Insets(0, 0, 5, 5);
		gbc_txtDNI.fill = GridBagConstraints.BOTH;
		gbc_txtDNI.gridx = 3;
		gbc_txtDNI.gridy = 2;
		panelDatos.add(txtDNI, gbc_txtDNI);
		txtDNI.setColumns(10);

		JLabel lblTelefono3 = new JLabel("Teléfono 3:");
		GridBagConstraints gbc_lblTelefono3 = new GridBagConstraints();
		gbc_lblTelefono3.fill = GridBagConstraints.BOTH;
		gbc_lblTelefono3.insets = new Insets(0, 0, 5, 5);
		gbc_lblTelefono3.gridx = 6;
		gbc_lblTelefono3.gridy = 2;
		panelDatos.add(lblTelefono3, gbc_lblTelefono3);
		lblTelefono3.setPreferredSize(new Dimension(0, 30));

		telefono3 = new JTextField();
		telefono3.setEditable(false);
		GridBagConstraints gbc_telefono3 = new GridBagConstraints();
		gbc_telefono3.fill = GridBagConstraints.BOTH;
		gbc_telefono3.insets = new Insets(0, 0, 5, 5);
		gbc_telefono3.gridx = 7;
		gbc_telefono3.gridy = 2;
		panelDatos.add(telefono3, gbc_telefono3);
		telefono3.setPreferredSize(new Dimension(0, 30));

		JLabel lblFechaNacimiento = new JLabel("Fecha nacimiento:");
		GridBagConstraints gbc_lblFechaNacimiento = new GridBagConstraints();
		gbc_lblFechaNacimiento.fill = GridBagConstraints.BOTH;
		gbc_lblFechaNacimiento.insets = new Insets(0, 0, 5, 5);
		gbc_lblFechaNacimiento.gridx = 1;
		gbc_lblFechaNacimiento.gridy = 3;
		panelDatos.add(lblFechaNacimiento, gbc_lblFechaNacimiento);

		txtNacimiento = new JTextField();
		txtNacimiento.setEditable(false);
		txtNacimiento.setColumns(10);
		GridBagConstraints gbc_txtNacimiento = new GridBagConstraints();
		gbc_txtNacimiento.insets = new Insets(0, 0, 5, 5);
		gbc_txtNacimiento.fill = GridBagConstraints.BOTH;
		gbc_txtNacimiento.gridx = 3;
		gbc_txtNacimiento.gridy = 3;
		panelDatos.add(txtNacimiento, gbc_txtNacimiento);

		JLabel lblTelefono4 = new JLabel("Teléfono 4:");
		GridBagConstraints gbc_lblTelefono4 = new GridBagConstraints();
		gbc_lblTelefono4.fill = GridBagConstraints.BOTH;
		gbc_lblTelefono4.insets = new Insets(0, 0, 5, 5);
		gbc_lblTelefono4.gridx = 6;
		gbc_lblTelefono4.gridy = 3;
		panelDatos.add(lblTelefono4, gbc_lblTelefono4);
		lblTelefono4.setPreferredSize(new Dimension(0, 30));

		telefono4 = new JTextField();
		telefono4.setEditable(false);
		GridBagConstraints gbc_telefono4 = new GridBagConstraints();
		gbc_telefono4.fill = GridBagConstraints.BOTH;
		gbc_telefono4.insets = new Insets(0, 0, 5, 5);
		gbc_telefono4.gridx = 7;
		gbc_telefono4.gridy = 3;
		panelDatos.add(telefono4, gbc_telefono4);
		telefono4.setPreferredSize(new Dimension(0, 30));

		JLabel lblEmail = new JLabel("Email:");
		GridBagConstraints gbc_lblEmail = new GridBagConstraints();
		gbc_lblEmail.insets = new Insets(0, 0, 0, 5);
		gbc_lblEmail.fill = GridBagConstraints.BOTH;
		gbc_lblEmail.gridx = 1;
		gbc_lblEmail.gridy = 4;
		panelDatos.add(lblEmail, gbc_lblEmail);

		txtEmail = new JTextField();
		txtEmail.setEditable(false);
		GridBagConstraints gbc_txtEmail = new GridBagConstraints();
		gbc_txtEmail.fill = GridBagConstraints.BOTH;
		gbc_txtEmail.insets = new Insets(0, 0, 0, 5);
		gbc_txtEmail.gridx = 3;
		gbc_txtEmail.gridy = 4;
		panelDatos.add(txtEmail, gbc_txtEmail);
		txtEmail.setColumns(10);

		JLabel lblTelefono5 = new JLabel("Teléfono 5:");
		GridBagConstraints gbc_lblTelefono5 = new GridBagConstraints();
		gbc_lblTelefono5.fill = GridBagConstraints.BOTH;
		gbc_lblTelefono5.insets = new Insets(0, 0, 0, 5);
		gbc_lblTelefono5.gridx = 6;
		gbc_lblTelefono5.gridy = 4;
		panelDatos.add(lblTelefono5, gbc_lblTelefono5);
		lblTelefono5.setPreferredSize(new Dimension(0, 30));

		telefono5 = new JTextField();
		telefono5.setEditable(false);
		GridBagConstraints gbc_telefono5 = new GridBagConstraints();
		gbc_telefono5.fill = GridBagConstraints.BOTH;
		gbc_telefono5.insets = new Insets(0, 0, 0, 5);
		gbc_telefono5.gridx = 7;
		gbc_telefono5.gridy = 4;
		panelDatos.add(telefono5, gbc_telefono5);
		telefono5.setPreferredSize(new Dimension(0, 30));

		panelSur = new JPanel();
		GridBagConstraints gbc_panelSur = new GridBagConstraints();
		gbc_panelSur.fill = GridBagConstraints.BOTH;
		gbc_panelSur.gridx = 0;
		gbc_panelSur.gridy = 2;
		frmMainClientes.add(panelSur, gbc_panelSur);
		GridBagLayout gbl_panelSur = new GridBagLayout();
		gbl_panelSur.columnWidths = new int[] { 894, 0 };
		gbl_panelSur.rowHeights = new int[] { 30, 30, 0, 0 };
		gbl_panelSur.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gbl_panelSur.rowWeights = new double[] { 0.0, 0.0, 1.0, Double.MIN_VALUE };
		panelSur.setLayout(gbl_panelSur);
		panelSur.setVisible(false);
		
		lblHistorialDePasajes = new JLabel("Historial de pasajes");
		GridBagConstraints gbc_lblHistorialDePasajes = new GridBagConstraints();
		gbc_lblHistorialDePasajes.fill = GridBagConstraints.VERTICAL;
		gbc_lblHistorialDePasajes.anchor = GridBagConstraints.WEST;
		gbc_lblHistorialDePasajes.insets = new Insets(0, 5, 5, 0);
		gbc_lblHistorialDePasajes.gridx = 0;
		gbc_lblHistorialDePasajes.gridy = 0;
		panelSur.add(lblHistorialDePasajes, gbc_lblHistorialDePasajes);
		lblHistorialDePasajes.setVisible(false);

		JScrollPane spPasajes = new JScrollPane();
		GridBagConstraints gbc_spPasajes = new GridBagConstraints();
		gbc_spPasajes.gridheight = 2;
		gbc_spPasajes.fill = GridBagConstraints.BOTH;
		gbc_spPasajes.gridx = 0;
		gbc_spPasajes.gridy = 1;
		panelSur.add(spPasajes, gbc_spPasajes);
		modelPasajes = new DefaultTableModel(null, nombreColumnasPasaje) {
			private static final long serialVersionUID = 1L;

			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		tablaPasajes = new JTable(modelPasajes);
		RowSorter<TableModel> sorter = new TableRowSorter<TableModel>(modelPasajes);
		tablaPasajes.setRowSorter(sorter);
		tablaPasajes.getTableHeader().setReorderingAllowed(false);
		spPasajes.setViewportView(tablaPasajes);

		this.frmMainClientes.setVisible(false);
	}

	public void mostrarVentana() {
		this.frmMainClientes.setVisible(true);
	}

	public JButton getBtnEditar() {
		return btnEditar;
	}

	public JTextField getTxtNombre() {
		return txtNombre;
	}

	public JTextField getTxtApellido() {
		return txtApellido;
	}

	public JTextField getTxtDni() {
		return txtDNI;
	}

	public JTextField getTxtEmail() {
		return txtEmail;
	}

	public JTextField getNacimiento() {
		return txtNacimiento;
	}

	public JTextField getTxtUsuario() {
		return txtUsuario;
	}

	public JTextField getTelefono1() {
		return telefono1;
	}

	public JTextField getTelefono2() {
		return telefono2;
	}

	public JTextField getTelefono3() {
		return telefono3;
	}

	public JTextField getTelefono4() {
		return telefono4;
	}

	public JTextField getTelefono5() {
		return telefono5;
	}
	
	public JTextField getTxtPuntos() {
		return txtPuntosAcumulados;
	}

	public DefaultTableModel getModelPasaje() {
		return modelPasajes;
	}

	public JTable getTablaPasaje() {
		return tablaPasajes;
	}

	public String[] getNombreColumnasPasajes() {
		return nombreColumnasPasaje;
	}

	public JPanel getPanel() {
		return frmMainClientes;
	}
	
	public JPanel getPanelPasajes() {
		return panelSur;
	}
	
	public JLabel getLblPuntos() {
		return lblPuntosAcumulados;
	}
	
	public JLabel getLblHistorial() {
		return lblHistorialDePasajes;
	}
}
