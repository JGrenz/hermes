package vistas.administrador.empleados;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;

import vistas.TextPrompt;
import java.awt.Dimension;
import javax.swing.ImageIcon;
import java.awt.FlowLayout;

public class Main_Empleados {

	private static Main_Empleados INSTANCE;
	private JPanel frmMainEmpleados;
	private JTable tablaEmpleados;
	private DefaultTableModel modelEmpleados;
	private String[] nombreColumnas = { "Nombre", "Apellido", "DNI", "Nacimiento", "Usuario", "Email",
			"Telefono principal", "Rol", "Estado" };
	private JButton btnAgregar, btnBorrar, btnEditar, btnBuscarEmpleado;
	private JTextField txtUsuario, txtNombre, txtApellido, txtDNI;
	private ButtonGroup filtro;
	private JLabel lblBusquedaPor, telefono1, telefono2, telefono3, telefono4, telefono5;
	private JRadioButton radioTodos, radioHabilitados, radioInhabilitados;
	private JComboBox<String> cboxRol;
	private JPanel panelABM, panelTabla, panelBusqueda, panelTelefonos;

	public static Main_Empleados getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new Main_Empleados();
		}
		return INSTANCE;
	}

	public Main_Empleados() {
		frmMainEmpleados = new JPanel();

		filtro = new ButtonGroup();

		GridBagLayout gbl_frmMainEmpleados = new GridBagLayout();
		gbl_frmMainEmpleados.columnWidths = new int[] { 1072, 0 };
		gbl_frmMainEmpleados.rowHeights = new int[] { 69, 182, 427, 0 };
		gbl_frmMainEmpleados.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gbl_frmMainEmpleados.rowWeights = new double[] { 0.0, 0.0, 1.0, Double.MIN_VALUE };
		frmMainEmpleados.setLayout(gbl_frmMainEmpleados);

		panelABM = new JPanel();
		GridBagConstraints gbc_panelABM = new GridBagConstraints();
		gbc_panelABM.fill = GridBagConstraints.BOTH;
		gbc_panelABM.insets = new Insets(0, 0, 5, 0);
		gbc_panelABM.gridx = 0;
		gbc_panelABM.gridy = 0;
		frmMainEmpleados.add(panelABM, gbc_panelABM);
		panelABM.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		btnAgregar = new JButton("Agregar");
		btnAgregar.setIcon(new ImageIcon(Main_Empleados.class.getResource("/imagenes/iconos/empleadoAgregar.png")));
		panelABM.add(btnAgregar);

		btnBorrar = new JButton("Habilitar / Inhabilitar");
		btnBorrar.setIcon(new ImageIcon(Main_Empleados.class.getResource("/imagenes/iconos/empleadoHabInhab.png")));
		panelABM.add(btnBorrar);

		btnEditar = new JButton("Editar");
		btnEditar.setIcon(new ImageIcon(Main_Empleados.class.getResource("/imagenes/iconos/empleadoEditar.png")));
		panelABM.add(btnEditar);

		panelBusqueda = new JPanel();
		GridBagConstraints gbc_panelBusqueda = new GridBagConstraints();
		gbc_panelBusqueda.fill = GridBagConstraints.BOTH;
		gbc_panelBusqueda.insets = new Insets(0, 0, 5, 0);
		gbc_panelBusqueda.gridx = 0;
		gbc_panelBusqueda.gridy = 1;
		frmMainEmpleados.add(panelBusqueda, gbc_panelBusqueda);
		GridBagLayout gbl_panelBusqueda = new GridBagLayout();
		gbl_panelBusqueda.columnWidths = new int[] { 127, 198, 98, 145, 123, 127, 0 };
		gbl_panelBusqueda.rowHeights = new int[] { 30, 25, 25, 26, 0 };
		gbl_panelBusqueda.columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		gbl_panelBusqueda.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		panelBusqueda.setLayout(gbl_panelBusqueda);

		JLabel lblEstado = new JLabel("Estado:");
		lblEstado.setPreferredSize(new Dimension(0, 30));
		lblEstado.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_lblEstado = new GridBagConstraints();
		gbc_lblEstado.fill = GridBagConstraints.BOTH;
		gbc_lblEstado.insets = new Insets(0, 0, 5, 5);
		gbc_lblEstado.gridx = 0;
		gbc_lblEstado.gridy = 0;
		panelBusqueda.add(lblEstado, gbc_lblEstado);

		JLabel lblRol = new JLabel("Rol:");
		lblRol.setPreferredSize(new Dimension(0, 30));
		lblRol.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_lblRol = new GridBagConstraints();
		gbc_lblRol.fill = GridBagConstraints.BOTH;
		gbc_lblRol.insets = new Insets(0, 0, 5, 5);
		gbc_lblRol.gridx = 1;
		gbc_lblRol.gridy = 0;
		panelBusqueda.add(lblRol, gbc_lblRol);

		lblBusquedaPor = new JLabel("Búsqueda por:");
		lblBusquedaPor.setPreferredSize(new Dimension(0, 30));
		lblBusquedaPor.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_lblBusquedaPor = new GridBagConstraints();
		gbc_lblBusquedaPor.fill = GridBagConstraints.BOTH;
		gbc_lblBusquedaPor.insets = new Insets(0, 0, 5, 5);
		gbc_lblBusquedaPor.gridx = 3;
		gbc_lblBusquedaPor.gridy = 0;
		panelBusqueda.add(lblBusquedaPor, gbc_lblBusquedaPor);

		btnBuscarEmpleado = new JButton("Buscar");
		btnBuscarEmpleado.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_btnBuscarEmpleado = new GridBagConstraints();
		gbc_btnBuscarEmpleado.fill = GridBagConstraints.BOTH;
		gbc_btnBuscarEmpleado.insets = new Insets(0, 0, 5, 5);
		gbc_btnBuscarEmpleado.gridx = 4;
		gbc_btnBuscarEmpleado.gridy = 0;
		panelBusqueda.add(btnBuscarEmpleado, gbc_btnBuscarEmpleado);

		radioTodos = new JRadioButton("Todos");
		radioTodos.setPreferredSize(new Dimension(0, 30));
		radioTodos.setHorizontalAlignment(SwingConstants.LEFT);
		GridBagConstraints gbc_rdbtnTodos = new GridBagConstraints();
		gbc_rdbtnTodos.fill = GridBagConstraints.BOTH;
		gbc_rdbtnTodos.insets = new Insets(0, 0, 5, 5);
		gbc_rdbtnTodos.gridx = 0;
		gbc_rdbtnTodos.gridy = 1;
		panelBusqueda.add(radioTodos, gbc_rdbtnTodos);
		filtro.add(radioTodos);

		cboxRol = new JComboBox<String>();
		cboxRol.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_cboxRol = new GridBagConstraints();
		gbc_cboxRol.fill = GridBagConstraints.BOTH;
		gbc_cboxRol.insets = new Insets(0, 0, 5, 5);
		gbc_cboxRol.gridx = 1;
		gbc_cboxRol.gridy = 1;
		panelBusqueda.add(cboxRol, gbc_cboxRol);

		JLabel radioNombreYApellido = new JLabel("Nombre y apellido");
		radioNombreYApellido.setPreferredSize(new Dimension(0, 30));
		radioNombreYApellido.setHorizontalAlignment(SwingConstants.LEFT);
		GridBagConstraints gbc_radioNombreYApellido = new GridBagConstraints();
		gbc_radioNombreYApellido.fill = GridBagConstraints.BOTH;
		gbc_radioNombreYApellido.insets = new Insets(0, 0, 5, 5);
		gbc_radioNombreYApellido.gridx = 3;
		gbc_radioNombreYApellido.gridy = 1;
		panelBusqueda.add(radioNombreYApellido, gbc_radioNombreYApellido);

		txtNombre = new JTextField();
		txtNombre.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_txtNombre = new GridBagConstraints();
		gbc_txtNombre.fill = GridBagConstraints.BOTH;
		gbc_txtNombre.insets = new Insets(0, 0, 5, 5);
		gbc_txtNombre.gridx = 4;
		gbc_txtNombre.gridy = 1;
		panelBusqueda.add(txtNombre, gbc_txtNombre);
		txtNombre.setColumns(10);
		TextPrompt phNombre = new TextPrompt("Ingrese nombre...", txtNombre);
		phNombre.setText("Ingrese nombre...");
		phNombre.changeAlpha(0.75f);
		phNombre.changeStyle(Font.ITALIC);

		txtApellido = new JTextField();
		txtApellido.setPreferredSize(new Dimension(0, 30));
		txtApellido.setColumns(10);
		GridBagConstraints gbc_txtApellido = new GridBagConstraints();
		gbc_txtApellido.fill = GridBagConstraints.BOTH;
		gbc_txtApellido.insets = new Insets(0, 0, 5, 0);
		gbc_txtApellido.gridx = 5;
		gbc_txtApellido.gridy = 1;
		panelBusqueda.add(txtApellido, gbc_txtApellido);
		TextPrompt phApellido = new TextPrompt("Ingrese apellido...", txtApellido);
		phApellido.setText("Ingrese apellido...");
		phApellido.changeAlpha(0.75f);
		phApellido.changeStyle(Font.ITALIC);

		radioHabilitados = new JRadioButton("Habilitados");
		radioHabilitados.setPreferredSize(new Dimension(0, 30));
		radioHabilitados.setHorizontalAlignment(SwingConstants.LEFT);
		GridBagConstraints gbc_rdbtnHabilitados = new GridBagConstraints();
		gbc_rdbtnHabilitados.fill = GridBagConstraints.BOTH;
		gbc_rdbtnHabilitados.insets = new Insets(0, 0, 5, 5);
		gbc_rdbtnHabilitados.gridx = 0;
		gbc_rdbtnHabilitados.gridy = 2;
		panelBusqueda.add(radioHabilitados, gbc_rdbtnHabilitados);
		radioHabilitados.setSelected(true);
		filtro.add(radioHabilitados);

		JLabel lblUsuario = new JLabel("Usuario");
		lblUsuario.setPreferredSize(new Dimension(0, 30));
		lblUsuario.setHorizontalAlignment(SwingConstants.LEFT);
		GridBagConstraints gbc_lblUsuario = new GridBagConstraints();
		gbc_lblUsuario.fill = GridBagConstraints.BOTH;
		gbc_lblUsuario.insets = new Insets(0, 0, 5, 5);
		gbc_lblUsuario.gridx = 3;
		gbc_lblUsuario.gridy = 2;
		panelBusqueda.add(lblUsuario, gbc_lblUsuario);

		txtUsuario = new JTextField();
		txtUsuario.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_txtUsuario = new GridBagConstraints();
		gbc_txtUsuario.fill = GridBagConstraints.BOTH;
		gbc_txtUsuario.insets = new Insets(0, 0, 5, 0);
		gbc_txtUsuario.gridwidth = 2;
		gbc_txtUsuario.gridx = 4;
		gbc_txtUsuario.gridy = 2;
		panelBusqueda.add(txtUsuario, gbc_txtUsuario);
		txtUsuario.setColumns(10);
		TextPrompt phUsuario = new TextPrompt("Ingrese usuario...", txtUsuario);
		phUsuario.setText("Ingrese usuario...");
		phUsuario.changeAlpha(0.75f);
		phUsuario.changeStyle(Font.ITALIC);

		radioInhabilitados = new JRadioButton("Inhabilitados");
		radioInhabilitados.setPreferredSize(new Dimension(0, 30));
		radioInhabilitados.setHorizontalAlignment(SwingConstants.LEFT);
		GridBagConstraints gbc_rdbtnInhabilitados = new GridBagConstraints();
		gbc_rdbtnInhabilitados.fill = GridBagConstraints.BOTH;
		gbc_rdbtnInhabilitados.insets = new Insets(0, 0, 0, 5);
		gbc_rdbtnInhabilitados.gridx = 0;
		gbc_rdbtnInhabilitados.gridy = 3;
		panelBusqueda.add(radioInhabilitados, gbc_rdbtnInhabilitados);
		filtro.add(radioInhabilitados);

		JLabel lblDNI = new JLabel("DNI");
		lblDNI.setPreferredSize(new Dimension(0, 30));
		lblDNI.setHorizontalAlignment(SwingConstants.LEFT);
		GridBagConstraints gbc_lblDNI = new GridBagConstraints();
		gbc_lblDNI.fill = GridBagConstraints.BOTH;
		gbc_lblDNI.insets = new Insets(0, 0, 0, 5);
		gbc_lblDNI.gridx = 3;
		gbc_lblDNI.gridy = 3;
		panelBusqueda.add(lblDNI, gbc_lblDNI);

		txtDNI = new JTextField();
		txtDNI.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_txtDNI = new GridBagConstraints();
		gbc_txtDNI.fill = GridBagConstraints.BOTH;
		gbc_txtDNI.gridwidth = 2;
		gbc_txtDNI.gridx = 4;
		gbc_txtDNI.gridy = 3;
		panelBusqueda.add(txtDNI, gbc_txtDNI);
		txtDNI.setColumns(10);
		TextPrompt phDNI = new TextPrompt("Ingrese DNI...", txtDNI);
		phDNI.setText("Ingrese DNI...");
		phDNI.changeAlpha(0.75f);
		phDNI.changeStyle(Font.ITALIC);

		panelTabla = new JPanel();
		GridBagConstraints gbc_panelTabla = new GridBagConstraints();
		gbc_panelTabla.fill = GridBagConstraints.BOTH;
		gbc_panelTabla.gridx = 0;
		gbc_panelTabla.gridy = 2;
		frmMainEmpleados.add(panelTabla, gbc_panelTabla);
		panelTabla.setLayout(new BorderLayout(0, 0));

		JScrollPane spEmpleados = new JScrollPane();
		panelTabla.add(spEmpleados, BorderLayout.CENTER);
		modelEmpleados = new DefaultTableModel(null, nombreColumnas) {
			private static final long serialVersionUID = 1L;

			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		tablaEmpleados = new JTable(modelEmpleados);
		tablaEmpleados.getTableHeader().setReorderingAllowed(false);
		spEmpleados.setViewportView(tablaEmpleados);

		panelTelefonos = new JPanel();
		panelTabla.add(panelTelefonos, BorderLayout.EAST);
		GridBagLayout gbl_panelTelefonos = new GridBagLayout();
		gbl_panelTelefonos.columnWidths = new int[] { 100, 100, 0 };
		gbl_panelTelefonos.rowHeights = new int[] { 30, 30, 30, 30, 30, 0 };
		gbl_panelTelefonos.columnWeights = new double[] { 1.0, 1.0, Double.MIN_VALUE };
		gbl_panelTelefonos.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		panelTelefonos.setLayout(gbl_panelTelefonos);

		JLabel lblTelefono1 = new JLabel("Teléfono 1:");
		lblTelefono1.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_lblTelefono1 = new GridBagConstraints();
		gbc_lblTelefono1.fill = GridBagConstraints.BOTH;
		gbc_lblTelefono1.insets = new Insets(0, 0, 5, 5);
		gbc_lblTelefono1.gridx = 0;
		gbc_lblTelefono1.gridy = 0;
		panelTelefonos.add(lblTelefono1, gbc_lblTelefono1);

		telefono1 = new JLabel("");
		telefono1.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_telefono1 = new GridBagConstraints();
		gbc_telefono1.fill = GridBagConstraints.BOTH;
		gbc_telefono1.insets = new Insets(0, 0, 5, 0);
		gbc_telefono1.gridx = 1;
		gbc_telefono1.gridy = 0;
		panelTelefonos.add(telefono1, gbc_telefono1);

		JLabel lblTelefono2 = new JLabel("Teléfono 2:");
		lblTelefono2.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_lblTelefono2 = new GridBagConstraints();
		gbc_lblTelefono2.fill = GridBagConstraints.BOTH;
		gbc_lblTelefono2.insets = new Insets(0, 0, 5, 5);
		gbc_lblTelefono2.gridx = 0;
		gbc_lblTelefono2.gridy = 1;
		panelTelefonos.add(lblTelefono2, gbc_lblTelefono2);

		telefono2 = new JLabel("");
		telefono2.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_telefono2 = new GridBagConstraints();
		gbc_telefono2.fill = GridBagConstraints.BOTH;
		gbc_telefono2.insets = new Insets(0, 0, 5, 0);
		gbc_telefono2.gridx = 1;
		gbc_telefono2.gridy = 1;
		panelTelefonos.add(telefono2, gbc_telefono2);

		JLabel lblTelefono3 = new JLabel("Teléfono 3:");
		lblTelefono3.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_lblTelefono3 = new GridBagConstraints();
		gbc_lblTelefono3.fill = GridBagConstraints.BOTH;
		gbc_lblTelefono3.insets = new Insets(0, 0, 5, 5);
		gbc_lblTelefono3.gridx = 0;
		gbc_lblTelefono3.gridy = 2;
		panelTelefonos.add(lblTelefono3, gbc_lblTelefono3);

		telefono3 = new JLabel("");
		telefono3.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_telefono3 = new GridBagConstraints();
		gbc_telefono3.fill = GridBagConstraints.BOTH;
		gbc_telefono3.insets = new Insets(0, 0, 5, 0);
		gbc_telefono3.gridx = 1;
		gbc_telefono3.gridy = 2;
		panelTelefonos.add(telefono3, gbc_telefono3);

		JLabel lblTelefono4 = new JLabel("Teléfono 4:");
		lblTelefono4.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_lblTelefono4 = new GridBagConstraints();
		gbc_lblTelefono4.fill = GridBagConstraints.BOTH;
		gbc_lblTelefono4.insets = new Insets(0, 0, 5, 5);
		gbc_lblTelefono4.gridx = 0;
		gbc_lblTelefono4.gridy = 3;
		panelTelefonos.add(lblTelefono4, gbc_lblTelefono4);

		telefono4 = new JLabel("");
		telefono4.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_telefono4 = new GridBagConstraints();
		gbc_telefono4.fill = GridBagConstraints.BOTH;
		gbc_telefono4.insets = new Insets(0, 0, 5, 0);
		gbc_telefono4.gridx = 1;
		gbc_telefono4.gridy = 3;
		panelTelefonos.add(telefono4, gbc_telefono4);

		JLabel lblTelefono5 = new JLabel("Teléfono 5:");
		lblTelefono5.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_lblTelefono5 = new GridBagConstraints();
		gbc_lblTelefono5.fill = GridBagConstraints.BOTH;
		gbc_lblTelefono5.insets = new Insets(0, 0, 0, 5);
		gbc_lblTelefono5.gridx = 0;
		gbc_lblTelefono5.gridy = 4;
		panelTelefonos.add(lblTelefono5, gbc_lblTelefono5);

		telefono5 = new JLabel("");
		telefono5.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_telefono5 = new GridBagConstraints();
		gbc_telefono5.fill = GridBagConstraints.BOTH;
		gbc_telefono5.gridx = 1;
		gbc_telefono5.gridy = 4;
		panelTelefonos.add(telefono5, gbc_telefono5);

		this.frmMainEmpleados.setVisible(true);
	}

	public void mostrarVentana() {
		this.frmMainEmpleados.setVisible(true);
	}

	public JButton getBtnAgregar() {
		return btnAgregar;
	}

	public JButton getBtnBorrar() {
		return btnBorrar;
	}

	public JButton getBtnEditar() {
		return btnEditar;
	}

	public DefaultTableModel getModelEmpleado() {
		return modelEmpleados;
	}

	public JTable getTablaEmpleado() {
		return tablaEmpleados;
	}

	public String[] getNombreColumnas() {
		return nombreColumnas;
	}

	public JTextField getTxtUsuario() {
		return txtUsuario;
	}

	public JTextField getTxtDNI() {
		return txtDNI;
	}

	public JTextField getTxtNombre() {
		return txtNombre;
	}

	public JTextField getTxtApellido() {
		return txtApellido;
	}

	public JButton getBtnBuscarEmpleado() {
		return btnBuscarEmpleado;
	}

	public JRadioButton getRdbtnTodos() {
		return radioTodos;
	}

	public JRadioButton getRdbtnHabilitados() {
		return radioHabilitados;
	}

	public JRadioButton getRdbtnInhabilitados() {
		return radioInhabilitados;
	}

	public JComboBox<String> getCboxRol() {
		return cboxRol;
	}

	public void mostrarBarraBusquedaPorUsuario() {
		btnBuscarEmpleado.setVisible(true);
		txtUsuario.setVisible(true);

		txtNombre.setVisible(false);
		txtApellido.setVisible(false);
		txtDNI.setVisible(false);
		txtNombre.setText("");
		txtApellido.setText("");
		txtDNI.setText("");
	}

	public void mostrarBusquedaPorNombreYApellido() {
		btnBuscarEmpleado.setVisible(true);
		txtNombre.setVisible(true);
		txtApellido.setVisible(true);

		txtUsuario.setVisible(false);
		txtDNI.setVisible(false);
		txtUsuario.setText("");
		txtDNI.setText("");
	}

	public void mostrarBusquedaPorDNI() {
		btnBuscarEmpleado.setVisible(true);
		txtDNI.setVisible(true);

		txtUsuario.setVisible(false);
		txtNombre.setVisible(false);
		txtApellido.setVisible(false);
		txtNombre.setText("");
		txtApellido.setText("");
		txtUsuario.setText("");
	}

	public JLabel getTelefono1() {
		return telefono1;
	}

	public JLabel getTelefono2() {
		return telefono2;
	}

	public JLabel getTelefono3() {
		return telefono3;
	}

	public JLabel getTelefono4() {
		return telefono4;
	}

	public JLabel getTelefono5() {
		return telefono5;
	}

	public JPanel getPanel() {
		return frmMainEmpleados;
	}

}