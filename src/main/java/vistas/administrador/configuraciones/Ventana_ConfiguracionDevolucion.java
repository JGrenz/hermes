package vistas.administrador.configuraciones;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JButton;
import java.awt.Insets;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import java.awt.Font;

public class Ventana_ConfiguracionDevolucion {

	private static Ventana_ConfiguracionDevolucion INSTANCE;
	private JPanel frmMainConfigDevolucion, panelDevolucion;
	private JButton btnAgregar, btnEditar, btnHabInhab;
	private JTable tablaConfig;
	private DefaultTableModel modelConfig;
	private String[] nombreColumnas = {"Id", "Desde", "Hasta", "% de retención", "Fecha creación", "Creador", "Estado"};
	
	public static Ventana_ConfiguracionDevolucion getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new Ventana_ConfiguracionDevolucion();
		}
		return INSTANCE;
	}

	public Ventana_ConfiguracionDevolucion() {
		frmMainConfigDevolucion = new JPanel();
		
		GridBagLayout gbl_frmMainConfigDevolucion = new GridBagLayout();
		gbl_frmMainConfigDevolucion.columnWidths = new int[] { 0, 0 };
		gbl_frmMainConfigDevolucion.rowHeights = new int[] { 0, 0 };
		gbl_frmMainConfigDevolucion.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gbl_frmMainConfigDevolucion.rowWeights = new double[] { 1.0, Double.MIN_VALUE };
		frmMainConfigDevolucion.setLayout(gbl_frmMainConfigDevolucion);

		panelDevolucion = new JPanel();
		GridBagConstraints gbc_panelDevolucion = new GridBagConstraints();
		gbc_panelDevolucion.fill = GridBagConstraints.BOTH;
		gbc_panelDevolucion.gridx = 0;
		gbc_panelDevolucion.gridy = 0;
		frmMainConfigDevolucion.add(panelDevolucion, gbc_panelDevolucion);
		GridBagLayout gbl_panelDevolucion = new GridBagLayout();
		gbl_panelDevolucion.columnWidths = new int[]{30, 200, 0, 0, 0, 0, 0, 30, 0};
		gbl_panelDevolucion.rowHeights = new int[]{30, 30, 30, 100, 0, 30, 0};
		gbl_panelDevolucion.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
		gbl_panelDevolucion.rowWeights = new double[]{0.0, 0.0, 0.0, 1.0, 1.0, 0.0, Double.MIN_VALUE};
		panelDevolucion.setLayout(gbl_panelDevolucion);
		
		JLabel lblDevolucion = new JLabel("Configuración de devolución");
		lblDevolucion.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblDevolucion.setPreferredSize(new Dimension(117, 30));
		GridBagConstraints gbc_lblDevolucion = new GridBagConstraints();
		gbc_lblDevolucion.insets = new Insets(0, 0, 5, 5);
		gbc_lblDevolucion.fill = GridBagConstraints.BOTH;
		gbc_lblDevolucion.gridx = 1;
		gbc_lblDevolucion.gridy = 1;
		panelDevolucion.add(lblDevolucion, gbc_lblDevolucion);
		
		btnAgregar = new JButton("Agregar");
		GridBagConstraints gbc_btnAgregar = new GridBagConstraints();
		gbc_btnAgregar.fill = GridBagConstraints.BOTH;
		gbc_btnAgregar.insets = new Insets(0, 0, 5, 5);
		gbc_btnAgregar.gridx = 3;
		gbc_btnAgregar.gridy = 1;
		panelDevolucion.add(btnAgregar, gbc_btnAgregar);
		
		btnEditar = new JButton("Editar");
		GridBagConstraints gbc_btnEditar = new GridBagConstraints();
		gbc_btnEditar.fill = GridBagConstraints.BOTH;
		gbc_btnEditar.insets = new Insets(0, 0, 5, 5);
		gbc_btnEditar.gridx = 4;
		gbc_btnEditar.gridy = 1;
		panelDevolucion.add(btnEditar, gbc_btnEditar);
		
		btnHabInhab = new JButton("Habilitar / Inhabilitar");
		GridBagConstraints gbc_btnHabilitarInhabilitar = new GridBagConstraints();
		gbc_btnHabilitarInhabilitar.insets = new Insets(0, 0, 5, 5);
		gbc_btnHabilitarInhabilitar.fill = GridBagConstraints.BOTH;
		gbc_btnHabilitarInhabilitar.gridx = 5;
		gbc_btnHabilitarInhabilitar.gridy = 1;
		panelDevolucion.add(btnHabInhab, gbc_btnHabilitarInhabilitar);
		
		JScrollPane spConfig = new JScrollPane();
		GridBagConstraints gbc_spConfig = new GridBagConstraints();
		gbc_spConfig.gridwidth = 6;
		gbc_spConfig.insets = new Insets(0, 0, 5, 5);
		gbc_spConfig.fill = GridBagConstraints.BOTH;
		gbc_spConfig.gridx = 1;
		gbc_spConfig.gridy = 3;
		panelDevolucion.add(spConfig, gbc_spConfig);
		modelConfig = new DefaultTableModel(null, nombreColumnas) {
			private static final long serialVersionUID = 1L;

			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		tablaConfig = new JTable(modelConfig);
		tablaConfig.getTableHeader().setReorderingAllowed(false);
		tablaConfig.removeColumn(tablaConfig.getColumnModel().getColumn(0));
		spConfig.setViewportView(tablaConfig);

		this.frmMainConfigDevolucion.setVisible(true);
	}
	
	public void mostrarVentana() {
		this.frmMainConfigDevolucion.setVisible(true);
	}

	public JPanel getPanel() {
		return frmMainConfigDevolucion;
	}
	
	public JButton getBtnAgregar() {
		return btnAgregar;
	}
	
	public JButton getBtnEditar() {
		return btnEditar;
	}
	
	public JButton getBtnHabInhab() {
		return btnHabInhab;
	}
	
	public DefaultTableModel getModelConfig() {
		return modelConfig;
	}

	public JTable getTablaConfig() {
		return tablaConfig;
	}

	public String[] getNombreColumnas() {
		return nombreColumnas;
	}
}