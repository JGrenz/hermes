package vistas.administrador.configuraciones;

import java.awt.Dialog.ModalityType;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import vistas.TextPrompt;
import vistas.ValidacionCampos;

public class Ventana_AgregarConfigDevolucion {

	private static Ventana_AgregarConfigDevolucion INSTANCE;
	private JDialog frmAgregarConfigDevolucion;
	private JPanel contentPane;
	private JButton btnConfirmar;
	private JTextField txtRangoInicial, txtRangoFinal, txtPorcentaje;

	public static Ventana_AgregarConfigDevolucion getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new Ventana_AgregarConfigDevolucion();
		}
		return INSTANCE;
	}

	public Ventana_AgregarConfigDevolucion() {

		frmAgregarConfigDevolucion = new JDialog();
		frmAgregarConfigDevolucion.setIconImage(Toolkit.getDefaultToolkit()
				.getImage(Ventana_AgregarConfigDevolucion.class.getResource("/imagenes/hermesicono.png")));
		frmAgregarConfigDevolucion.setModalityType(ModalityType.APPLICATION_MODAL);
		frmAgregarConfigDevolucion.setResizable(false);
		frmAgregarConfigDevolucion.setTitle("Hermes - Agregar configuracion de devolución");
		frmAgregarConfigDevolucion.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmAgregarConfigDevolucion.setBounds(100, 100, 410, 240);
		frmAgregarConfigDevolucion.setLocationRelativeTo(null);

		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		frmAgregarConfigDevolucion.setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 404, 211);
		contentPane.add(panel);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[] { 30, 0, 155, 0, 0 };
		gbl_panel.rowHeights = new int[] { 30, 30, 30, 30, 30, 30, 0 };
		gbl_panel.columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		gbl_panel.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		panel.setLayout(gbl_panel);

		JLabel lblRangoFechaInicial = new JLabel("Rango de fecha inicial");
		GridBagConstraints gbc_lblRangoFechaInicial = new GridBagConstraints();
		gbc_lblRangoFechaInicial.fill = GridBagConstraints.BOTH;
		gbc_lblRangoFechaInicial.insets = new Insets(0, 0, 5, 5);
		gbc_lblRangoFechaInicial.gridx = 1;
		gbc_lblRangoFechaInicial.gridy = 1;
		panel.add(lblRangoFechaInicial, gbc_lblRangoFechaInicial);

		txtRangoInicial = new JTextField();
		TextPrompt phRangoInicial = new TextPrompt("Cantidad de días cercanos al viaje", txtRangoInicial);
		phRangoInicial.changeAlpha(0.75f);
		phRangoInicial.changeStyle(Font.ITALIC);
		GridBagConstraints gbc_txtRangoInicial = new GridBagConstraints();
		gbc_txtRangoInicial.gridwidth = 2;
		gbc_txtRangoInicial.insets = new Insets(0, 0, 5, 0);
		gbc_txtRangoInicial.fill = GridBagConstraints.BOTH;
		gbc_txtRangoInicial.gridx = 2;
		gbc_txtRangoInicial.gridy = 1;
		panel.add(txtRangoInicial, gbc_txtRangoInicial);
		txtRangoInicial.setColumns(10);

		JLabel lblRangoFechaFinal = new JLabel("Rango de fecha final");
		GridBagConstraints gbc_lblRangoFechaFinal = new GridBagConstraints();
		gbc_lblRangoFechaFinal.fill = GridBagConstraints.BOTH;
		gbc_lblRangoFechaFinal.insets = new Insets(0, 0, 5, 5);
		gbc_lblRangoFechaFinal.gridx = 1;
		gbc_lblRangoFechaFinal.gridy = 2;
		panel.add(lblRangoFechaFinal, gbc_lblRangoFechaFinal);

		txtRangoFinal = new JTextField();
		TextPrompt phRangoFinal = new TextPrompt("Cantidad de días alejados al viaje", txtRangoFinal);
		phRangoFinal.changeAlpha(0.75f);
		phRangoFinal.changeStyle(Font.ITALIC);
		GridBagConstraints gbc_txtRangoFinal = new GridBagConstraints();
		gbc_txtRangoFinal.gridwidth = 2;
		gbc_txtRangoFinal.insets = new Insets(0, 0, 5, 0);
		gbc_txtRangoFinal.fill = GridBagConstraints.BOTH;
		gbc_txtRangoFinal.gridx = 2;
		gbc_txtRangoFinal.gridy = 2;
		panel.add(txtRangoFinal, gbc_txtRangoFinal);
		txtRangoFinal.setColumns(10);

		JLabel lblPorcentajeDevolucion = new JLabel("Porcentaje devolución");
		GridBagConstraints gbc_lblPorcentajeDevolucion = new GridBagConstraints();
		gbc_lblPorcentajeDevolucion.insets = new Insets(0, 0, 5, 5);
		gbc_lblPorcentajeDevolucion.fill = GridBagConstraints.BOTH;
		gbc_lblPorcentajeDevolucion.gridx = 1;
		gbc_lblPorcentajeDevolucion.gridy = 3;
		panel.add(lblPorcentajeDevolucion, gbc_lblPorcentajeDevolucion);

		txtPorcentaje = new JTextField();
		TextPrompt phPorcentaje = new TextPrompt("Ingrese porcentaje de devolución", txtPorcentaje);
		phPorcentaje.changeAlpha(0.75f);
		phPorcentaje.changeStyle(Font.ITALIC);
		GridBagConstraints gbc_txtPorcentaje = new GridBagConstraints();
		gbc_txtPorcentaje.gridwidth = 2;
		gbc_txtPorcentaje.insets = new Insets(0, 0, 5, 0);
		gbc_txtPorcentaje.fill = GridBagConstraints.BOTH;
		gbc_txtPorcentaje.gridx = 2;
		gbc_txtPorcentaje.gridy = 3;
		panel.add(txtPorcentaje, gbc_txtPorcentaje);
		txtPorcentaje.setColumns(10);

		btnConfirmar = new JButton("Confirmar");
		GridBagConstraints gbc_btnConfirmar = new GridBagConstraints();
		gbc_btnConfirmar.fill = GridBagConstraints.BOTH;
		gbc_btnConfirmar.gridx = 3;
		gbc_btnConfirmar.gridy = 5;
		panel.add(btnConfirmar, gbc_btnConfirmar);

		this.frmAgregarConfigDevolucion.setVisible(false);
	}

	public void mostrarVentana() {
		this.frmAgregarConfigDevolucion.setVisible(true);
	}

	public JButton getBtnConfirmar() {
		return btnConfirmar;
	}

	public JTextField getTxtRangoInicial() {
		return txtRangoInicial;
	}

	public JTextField getTxtRangoFinal() {
		return txtRangoFinal;
	}

	public JTextField getTxtPorcentaje() {
		return txtPorcentaje;
	}

	public void cerrar() {
		this.txtRangoInicial.setText(null);
		this.txtRangoFinal.setText(null);
		this.txtPorcentaje.setText(null);
		this.frmAgregarConfigDevolucion.dispose();
	}

	private boolean camposVacios() {
		if (txtRangoInicial.getText().isEmpty() || txtPorcentaje.getText().isEmpty()) {
			JOptionPane.showMessageDialog(null,
					"Los campos Rango fecha inicial y Porcentaje de devolución son obligatorios.");
			return true;
		} else {
			return false;
		}
	}

	private boolean camposCorrectos() {
		ValidacionCampos validador = new ValidacionCampos();
		if (!validador.esNumerico(txtRangoInicial.getText()) || !validador.esNumerico(txtPorcentaje.getText())) {
			return false;
		} else {
			if (!txtRangoFinal.getText().equals("")) {
				if(txtRangoFinal.getText().equals("0")) {
					JOptionPane.showMessageDialog(null, "El Rango Final no puede ser 0.");
					return false;
				} else {
					return validador.esNumerico(txtRangoFinal.getText());
				}
			}
			int porcentaje = Integer.parseInt(txtPorcentaje.getText());
			if(porcentaje > 100) {
				JOptionPane.showMessageDialog(null, "El porcentaje no puede ser mayor al 100%.");
				return false;				
			}
			return true;
		}
	}

	public boolean verificarCampos() {
		if (!camposVacios() && camposCorrectos())
			return true;
		else
			return false;
	}
}