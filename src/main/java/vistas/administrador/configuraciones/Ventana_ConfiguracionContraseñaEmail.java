package vistas.administrador.configuraciones;

import java.awt.Dialog.ModalityType;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.border.EmptyBorder;

import vistas.ValidacionCampos;
import java.awt.Font;

public class Ventana_ConfiguracionContraseñaEmail {

	private static Ventana_ConfiguracionContraseñaEmail INSTANCE;
	private JDialog frmConfigEmail;
	private JPanel contentPane;
	private JPasswordField txtContraseñaActual, txtNuevaContraseña, txtConfirmarContraseña;
	private JButton btnConfirmar;

	public static Ventana_ConfiguracionContraseñaEmail getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new Ventana_ConfiguracionContraseñaEmail();
		}
		return INSTANCE;
	}

	public Ventana_ConfiguracionContraseñaEmail() {

		frmConfigEmail = new JDialog();
		frmConfigEmail.setIconImage(Toolkit.getDefaultToolkit()
				.getImage(Ventana_ConfiguracionContraseñaEmail.class.getResource("/imagenes/hermesicono.png")));
		frmConfigEmail.setModalityType(ModalityType.APPLICATION_MODAL);
		frmConfigEmail.setResizable(false);
		frmConfigEmail.setTitle("Hermes - Configurar contraseña");
		frmConfigEmail.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmConfigEmail.setBounds(100, 100, 450, 300);
		frmConfigEmail.setLocationRelativeTo(null);
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		frmConfigEmail.setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 444, 271);
		contentPane.add(panel);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{30, 0, 155, 0, 0};
		gbl_panel.rowHeights = new int[]{30, 30, 30, 30, 30, 30, 0, 0, 0};
		gbl_panel.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		panel.setLayout(gbl_panel);
		
		JLabel lblConfigEmail = new JLabel("Configuración de contraseña");
		lblConfigEmail.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_lblConfigEmail = new GridBagConstraints();
		gbc_lblConfigEmail.fill = GridBagConstraints.BOTH;
		gbc_lblConfigEmail.insets = new Insets(0, 0, 5, 5);
		gbc_lblConfigEmail.gridx = 1;
		gbc_lblConfigEmail.gridy = 1;
		panel.add(lblConfigEmail, gbc_lblConfigEmail);
		
		JLabel lblContraseñaActual = new JLabel("Contraseña actual");
		GridBagConstraints gbc_lblContraseñaActual = new GridBagConstraints();
		gbc_lblContraseñaActual.fill = GridBagConstraints.VERTICAL;
		gbc_lblContraseñaActual.insets = new Insets(0, 0, 5, 5);
		gbc_lblContraseñaActual.gridx = 1;
		gbc_lblContraseñaActual.gridy = 3;
		panel.add(lblContraseñaActual, gbc_lblContraseñaActual);
		
		txtContraseñaActual = new JPasswordField();
		GridBagConstraints gbc_txtContraseñaActual = new GridBagConstraints();
		gbc_txtContraseñaActual.gridwidth = 2;
		gbc_txtContraseñaActual.insets = new Insets(0, 0, 5, 5);
		gbc_txtContraseñaActual.fill = GridBagConstraints.BOTH;
		gbc_txtContraseñaActual.gridx = 2;
		gbc_txtContraseñaActual.gridy = 3;
		panel.add(txtContraseñaActual, gbc_txtContraseñaActual);
		txtContraseñaActual.setColumns(10);
		
		JLabel lblNuevaContraseña = new JLabel("Nueva contraseña");
		GridBagConstraints gbc_lblNuevaContraseña = new GridBagConstraints();
		gbc_lblNuevaContraseña.fill = GridBagConstraints.VERTICAL;
		gbc_lblNuevaContraseña.insets = new Insets(0, 0, 5, 5);
		gbc_lblNuevaContraseña.gridx = 1;
		gbc_lblNuevaContraseña.gridy = 4;
		panel.add(lblNuevaContraseña, gbc_lblNuevaContraseña);
		
		txtNuevaContraseña = new JPasswordField();
		GridBagConstraints gbc_txtNuevaContraseña = new GridBagConstraints();
		gbc_txtNuevaContraseña.gridwidth = 2;
		gbc_txtNuevaContraseña.insets = new Insets(0, 0, 5, 5);
		gbc_txtNuevaContraseña.fill = GridBagConstraints.BOTH;
		gbc_txtNuevaContraseña.gridx = 2;
		gbc_txtNuevaContraseña.gridy = 4;
		panel.add(txtNuevaContraseña, gbc_txtNuevaContraseña);
		txtNuevaContraseña.setColumns(10);
		
		JLabel lblConfirmarContraseña = new JLabel("Confirmar contraseña");
		GridBagConstraints gbc_lblConfirmarContraseña = new GridBagConstraints();
		gbc_lblConfirmarContraseña.insets = new Insets(0, 0, 5, 5);
		gbc_lblConfirmarContraseña.fill = GridBagConstraints.VERTICAL;
		gbc_lblConfirmarContraseña.gridx = 1;
		gbc_lblConfirmarContraseña.gridy = 5;
		panel.add(lblConfirmarContraseña, gbc_lblConfirmarContraseña);
		
		txtConfirmarContraseña = new JPasswordField();
		GridBagConstraints gbc_txtConfirmarContraseña = new GridBagConstraints();
		gbc_txtConfirmarContraseña.gridwidth = 2;
		gbc_txtConfirmarContraseña.insets = new Insets(0, 0, 5, 5);
		gbc_txtConfirmarContraseña.fill = GridBagConstraints.BOTH;
		gbc_txtConfirmarContraseña.gridx = 2;
		gbc_txtConfirmarContraseña.gridy = 5;
		panel.add(txtConfirmarContraseña, gbc_txtConfirmarContraseña);
		txtConfirmarContraseña.setColumns(10);

		btnConfirmar = new JButton("Confirmar");
		GridBagConstraints gbc_btnConfirmar = new GridBagConstraints();
		gbc_btnConfirmar.fill = GridBagConstraints.BOTH;
		gbc_btnConfirmar.insets = new Insets(0, 0, 0, 5);
		gbc_btnConfirmar.gridx = 3;
		gbc_btnConfirmar.gridy = 7;
		panel.add(btnConfirmar, gbc_btnConfirmar);
		
		this.frmConfigEmail.setVisible(false);
	}
	
	public void mostrarVentana() {
		this.frmConfigEmail.setVisible(true);
	}
	
	public JButton getBtnConfirmar() {
		return btnConfirmar;
	}
	
	public JPasswordField getTxtContraseñaActual() {
		return txtContraseñaActual;
	}
	
	public JPasswordField getTxtNuevaContraseña() {
		return txtNuevaContraseña;
	}
	
	public JPasswordField getTxtConfirmarContraseña() {
		return txtConfirmarContraseña;
	}
	
	public void cerrar() {
		this.frmConfigEmail.dispose();
	}
	
	private boolean camposVacios() {
		if (String.valueOf(getTxtNuevaContraseña().getPassword()).isEmpty() ||
				String.valueOf(getTxtConfirmarContraseña().getPassword()).isEmpty())
		{
			JOptionPane.showMessageDialog(null, "Los campos Nueva Contraseña y Confirmar Contraseña son obligatorios.");
			return true;
		}
		else
			return false;
	}
	
	private boolean camposCorrectos() {
		ValidacionCampos validador = new ValidacionCampos();
		if(!validador.esClave(String.valueOf(this.getTxtNuevaContraseña().getPassword()))
			|| !validador.esClave(String.valueOf(this.getTxtConfirmarContraseña().getPassword())))
			return false;
		
		else
			return true;
	}
	
	public boolean verificarCampo() {
		if (!camposVacios() && camposCorrectos())
			return true;
		else
			return false;
	}
	
}