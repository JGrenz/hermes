package vistas.administrador.configuraciones;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JSpinner;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import javax.swing.SpinnerNumberModel;

import vistas.ValidacionCampos;
import java.awt.Font;

public class Ventana_ConfiguracionPuntaje {

	private static Ventana_ConfiguracionPuntaje INSTANCE;
	private JPanel frmMainConfigPuntaje, panelPuntaje;
	private JTextField txtCantidadPlata, txtCantidadPuntos, txtPrecioUnitario;
	private JSpinner spAños, spMeses;
	private JButton btnConfirmar;

	public static Ventana_ConfiguracionPuntaje getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new Ventana_ConfiguracionPuntaje();
		}
		return INSTANCE;
	}

	public Ventana_ConfiguracionPuntaje() {
		frmMainConfigPuntaje = new JPanel();
		GridBagLayout gbl_frmMainConfigPuntaje = new GridBagLayout();
		gbl_frmMainConfigPuntaje.columnWidths = new int[] { 30, 165, 120, 30, 120, 120, 0 };
		gbl_frmMainConfigPuntaje.rowHeights = new int[] { 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30 };
		gbl_frmMainConfigPuntaje.columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		gbl_frmMainConfigPuntaje.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
				0.0, Double.MIN_VALUE };
		frmMainConfigPuntaje.setLayout(gbl_frmMainConfigPuntaje);

		panelPuntaje = new JPanel();
		GridBagConstraints gbc_panelPuntaje = new GridBagConstraints();
		gbc_panelPuntaje.insets = new Insets(0, 0, 5, 5);
		gbc_panelPuntaje.fill = GridBagConstraints.BOTH;
		gbc_panelPuntaje.gridx = 0;
		gbc_panelPuntaje.gridy = 0;
		frmMainConfigPuntaje.add(panelPuntaje, gbc_panelPuntaje);
		GridBagLayout gbl_panelPuntaje = new GridBagLayout();
		gbl_panelPuntaje.columnWidths = new int[] { 30 };
		gbl_panelPuntaje.rowHeights = new int[] { 30, 0 };
		gbl_panelPuntaje.columnWeights = new double[] { 0.0 };
		gbl_panelPuntaje.rowWeights = new double[] { 0.0, Double.MIN_VALUE };
		panelPuntaje.setLayout(gbl_panelPuntaje);

		JLabel lblConfiguracionDePuntos = new JLabel("Configuración de puntos");
		lblConfiguracionDePuntos.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblConfiguracionDePuntos.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_lblConfiguracionDePuntos = new GridBagConstraints();
		gbc_lblConfiguracionDePuntos.fill = GridBagConstraints.BOTH;
		gbc_lblConfiguracionDePuntos.insets = new Insets(0, 0, 5, 5);
		gbc_lblConfiguracionDePuntos.gridx = 1;
		gbc_lblConfiguracionDePuntos.gridy = 1;
		frmMainConfigPuntaje.add(lblConfiguracionDePuntos, gbc_lblConfiguracionDePuntos);
		lblConfiguracionDePuntos.setPreferredSize(new Dimension(117, 30));

		JLabel lblPuntosPorPlata = new JLabel("Puntos por plata");
		lblPuntosPorPlata.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblPuntosPorPlata.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_lblPuntosPorPlata = new GridBagConstraints();
		gbc_lblPuntosPorPlata.fill = GridBagConstraints.BOTH;
		gbc_lblPuntosPorPlata.insets = new Insets(0, 0, 5, 5);
		gbc_lblPuntosPorPlata.gridx = 1;
		gbc_lblPuntosPorPlata.gridy = 3;
		frmMainConfigPuntaje.add(lblPuntosPorPlata, gbc_lblPuntosPorPlata);

		JLabel lblCantidadDePlata = new JLabel("Cantidad de plata");
		lblCantidadDePlata.setHorizontalAlignment(SwingConstants.RIGHT);
		GridBagConstraints gbc_lblCantidadDePlata = new GridBagConstraints();
		gbc_lblCantidadDePlata.fill = GridBagConstraints.BOTH;
		gbc_lblCantidadDePlata.insets = new Insets(0, 0, 5, 5);
		gbc_lblCantidadDePlata.gridx = 1;
		gbc_lblCantidadDePlata.gridy = 4;
		frmMainConfigPuntaje.add(lblCantidadDePlata, gbc_lblCantidadDePlata);

		txtCantidadPlata = new JTextField();
		GridBagConstraints gbc_txtCantidadPlata = new GridBagConstraints();
		gbc_txtCantidadPlata.insets = new Insets(0, 0, 5, 5);
		gbc_txtCantidadPlata.fill = GridBagConstraints.BOTH;
		gbc_txtCantidadPlata.gridx = 2;
		gbc_txtCantidadPlata.gridy = 4;
		frmMainConfigPuntaje.add(txtCantidadPlata, gbc_txtCantidadPlata);
		txtCantidadPlata.setColumns(10);

		JLabel lblSignoIgual = new JLabel("=");
		lblSignoIgual.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_lblSignoIgual = new GridBagConstraints();
		gbc_lblSignoIgual.fill = GridBagConstraints.BOTH;
		gbc_lblSignoIgual.insets = new Insets(0, 0, 5, 5);
		gbc_lblSignoIgual.gridx = 3;
		gbc_lblSignoIgual.gridy = 4;
		frmMainConfigPuntaje.add(lblSignoIgual, gbc_lblSignoIgual);

		JLabel lblCantidadDePuntos = new JLabel("Cantidad de puntos");
		lblCantidadDePuntos.setHorizontalAlignment(SwingConstants.RIGHT);
		GridBagConstraints gbc_lblCantidadDePuntos = new GridBagConstraints();
		gbc_lblCantidadDePuntos.fill = GridBagConstraints.BOTH;
		gbc_lblCantidadDePuntos.insets = new Insets(0, 0, 5, 5);
		gbc_lblCantidadDePuntos.gridx = 4;
		gbc_lblCantidadDePuntos.gridy = 4;
		frmMainConfigPuntaje.add(lblCantidadDePuntos, gbc_lblCantidadDePuntos);

		txtCantidadPuntos = new JTextField();
		GridBagConstraints gbc_txtCantidadPuntos = new GridBagConstraints();
		gbc_txtCantidadPuntos.insets = new Insets(0, 0, 5, 0);
		gbc_txtCantidadPuntos.fill = GridBagConstraints.BOTH;
		gbc_txtCantidadPuntos.gridx = 5;
		gbc_txtCantidadPuntos.gridy = 4;
		frmMainConfigPuntaje.add(txtCantidadPuntos, gbc_txtCantidadPuntos);
		txtCantidadPuntos.setColumns(10);

		JLabel lblPrecioUnitarioDe = new JLabel("Precio unitario de punto");
		lblPrecioUnitarioDe.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblPrecioUnitarioDe.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_lblPrecioUnitarioDe = new GridBagConstraints();
		gbc_lblPrecioUnitarioDe.fill = GridBagConstraints.BOTH;
		gbc_lblPrecioUnitarioDe.insets = new Insets(0, 0, 5, 5);
		gbc_lblPrecioUnitarioDe.gridx = 1;
		gbc_lblPrecioUnitarioDe.gridy = 6;
		frmMainConfigPuntaje.add(lblPrecioUnitarioDe, gbc_lblPrecioUnitarioDe);

		JLabel lblPunto = new JLabel("1 punto = $");
		lblPunto.setHorizontalAlignment(SwingConstants.RIGHT);
		GridBagConstraints gbc_lblPunto = new GridBagConstraints();
		gbc_lblPunto.fill = GridBagConstraints.BOTH;
		gbc_lblPunto.insets = new Insets(0, 0, 5, 5);
		gbc_lblPunto.gridx = 1;
		gbc_lblPunto.gridy = 7;
		frmMainConfigPuntaje.add(lblPunto, gbc_lblPunto);

		txtPrecioUnitario = new JTextField();
		GridBagConstraints gbc_txtPrecioUnitario = new GridBagConstraints();
		gbc_txtPrecioUnitario.insets = new Insets(0, 0, 5, 5);
		gbc_txtPrecioUnitario.fill = GridBagConstraints.BOTH;
		gbc_txtPrecioUnitario.gridx = 2;
		gbc_txtPrecioUnitario.gridy = 7;
		frmMainConfigPuntaje.add(txtPrecioUnitario, gbc_txtPrecioUnitario);
		txtPrecioUnitario.setColumns(10);

		JLabel lblVencimiento = new JLabel("Vencimiento");
		lblVencimiento.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblVencimiento.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_lblVencimiento = new GridBagConstraints();
		gbc_lblVencimiento.fill = GridBagConstraints.BOTH;
		gbc_lblVencimiento.insets = new Insets(0, 0, 5, 5);
		gbc_lblVencimiento.gridx = 1;
		gbc_lblVencimiento.gridy = 9;
		frmMainConfigPuntaje.add(lblVencimiento, gbc_lblVencimiento);

		JLabel lblAños = new JLabel("Años");
		lblAños.setHorizontalAlignment(SwingConstants.RIGHT);
		GridBagConstraints gbc_lblAños = new GridBagConstraints();
		gbc_lblAños.fill = GridBagConstraints.BOTH;
		gbc_lblAños.insets = new Insets(0, 0, 5, 5);
		gbc_lblAños.gridx = 1;
		gbc_lblAños.gridy = 10;
		frmMainConfigPuntaje.add(lblAños, gbc_lblAños);

		spAños = new JSpinner();
		spAños.setModel(new SpinnerNumberModel(new Integer(0), new Integer(0), null, new Integer(1)));
		GridBagConstraints gbc_spAños = new GridBagConstraints();
		gbc_spAños.fill = GridBagConstraints.BOTH;
		gbc_spAños.insets = new Insets(0, 0, 5, 5);
		gbc_spAños.gridx = 2;
		gbc_spAños.gridy = 10;
		frmMainConfigPuntaje.add(spAños, gbc_spAños);

		JLabel lblMeses = new JLabel("Meses");
		lblMeses.setHorizontalAlignment(SwingConstants.RIGHT);
		GridBagConstraints gbc_lblMeses = new GridBagConstraints();
		gbc_lblMeses.fill = GridBagConstraints.BOTH;
		gbc_lblMeses.insets = new Insets(0, 0, 5, 5);
		gbc_lblMeses.gridx = 1;
		gbc_lblMeses.gridy = 11;
		frmMainConfigPuntaje.add(lblMeses, gbc_lblMeses);

		spMeses = new JSpinner();
		spMeses.setModel(new SpinnerNumberModel(0, 0, 11, 1));
		GridBagConstraints gbc_spMeses = new GridBagConstraints();
		gbc_spMeses.fill = GridBagConstraints.BOTH;
		gbc_spMeses.insets = new Insets(0, 0, 5, 5);
		gbc_spMeses.gridx = 2;
		gbc_spMeses.gridy = 11;
		frmMainConfigPuntaje.add(spMeses, gbc_spMeses);

		btnConfirmar = new JButton("Guardar configuraciones");
		GridBagConstraints gbc_btnConfirmar = new GridBagConstraints();
		gbc_btnConfirmar.fill = GridBagConstraints.BOTH;
		gbc_btnConfirmar.gridx = 5;
		gbc_btnConfirmar.gridy = 12;
		frmMainConfigPuntaje.add(btnConfirmar, gbc_btnConfirmar);

		this.frmMainConfigPuntaje.setVisible(true);
	}

	public void mostrarVentana() {
		this.frmMainConfigPuntaje.setVisible(true);
	}

	public JPanel getPanel() {
		return frmMainConfigPuntaje;
	}
	
	public JTextField getTxtCantidadPlata() {
		return txtCantidadPlata;
	}
	
	public JTextField getTxtCantidadPuntos() {
		return txtCantidadPuntos;
	}
	
	public JTextField getTxtPrecioUnitario() {
		return txtPrecioUnitario;
	}
	
	public JSpinner getSPAños() {
		return spAños;
	}
	
	public JSpinner getSPMeses() {
		return spMeses;
	}
	
	public JButton getBtnConfirmar() {
		return btnConfirmar;
	}
	private boolean camposVacios() {
		if (txtCantidadPlata.getText().isEmpty() || txtCantidadPuntos.getText().isEmpty() || txtPrecioUnitario.getText().isEmpty())
		{
			JOptionPane.showMessageDialog(null, "Los campos Cantidad de plata, Cantidad de puntos y Precio unitario de punto son obligatorios.");
			return true;
		}
		else
			return false;
	}
	
	private boolean camposCorrectos() {
		ValidacionCampos validador = new ValidacionCampos();
		if(!validador.esMonto(txtCantidadPlata.getText()) || !validador.esNumerico(txtCantidadPuntos.getText())
			|| !validador.esMonto(txtPrecioUnitario.getText()))
			return false;
		
		else
			return true;
	}
	
	public boolean verificarCampo() {
		if (!camposVacios() && camposCorrectos())
			return true;
		else
			return false;
	}
	
}