package vistas.administrador.configuraciones;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

public class Main_Configuracion {

	private static Main_Configuracion INSTANCE;
	private JPanel frmMainConfiguraciones;
	private JPanel panelBotones, panelCentro;
	private JButton btnConfiguracionesEmail, btnConfiguracionesPuntaje, btnConfiguracionesDevolucion;
	private JButton btnConfiguracionesBdd;

	public static Main_Configuracion getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new Main_Configuracion();
		}
		return INSTANCE;
	}

	public Main_Configuracion() {
		frmMainConfiguraciones = new JPanel();
		frmMainConfiguraciones.setLayout(new BorderLayout(0, 0));

		panelBotones = new JPanel();
		panelBotones.setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
		frmMainConfiguraciones.add(panelBotones, BorderLayout.NORTH);
		panelBotones.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		btnConfiguracionesEmail = new JButton("Configuraciones Email");
		panelBotones.add(btnConfiguracionesEmail);

		btnConfiguracionesPuntaje = new JButton("Configuraciones Puntaje");
		panelBotones.add(btnConfiguracionesPuntaje);

		btnConfiguracionesDevolucion = new JButton("Configuraciones Devolución");
		panelBotones.add(btnConfiguracionesDevolucion);
		
		btnConfiguracionesBdd = new JButton("Configuraciones BDD");
		panelBotones.add(btnConfiguracionesBdd);

		panelCentro = new JPanel();
		panelCentro.setBorder(new LineBorder(new Color(0, 0, 0)));
		frmMainConfiguraciones.add(panelCentro, BorderLayout.CENTER);
		panelCentro.setLayout(new CardLayout(0, 0));

		this.frmMainConfiguraciones.setVisible(true);
	}

	public void mostrarVentana() {
		this.frmMainConfiguraciones.setVisible(true);
	}

	public JButton getBtnConfigEmail() {
		return btnConfiguracionesEmail;
	}

	public JButton getBtnConfigPuntaje() {
		return btnConfiguracionesPuntaje;
	}

	public JButton getBtnConfigDevolucion() {
		return btnConfiguracionesDevolucion;
	}
	
	public JButton getBtnConfigBDD() {
		return btnConfiguracionesBdd;
	}
	
	public JPanel getPanel() {
		return frmMainConfiguraciones;
	}

	public JPanel getPanelCentro() {
		return panelCentro;
	}

}