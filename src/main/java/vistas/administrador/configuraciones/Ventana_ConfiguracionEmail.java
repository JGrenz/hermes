package vistas.administrador.configuraciones;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import vistas.ValidacionCampos;
import java.awt.Font;

public class Ventana_ConfiguracionEmail {

	private static Ventana_ConfiguracionEmail INSTANCE;
	private JPanel frmMainConfigEmail;
	private JPanel panelMail;
	private JTextField txtMail, txtRemitente;
	private JTextArea txtFirma;
	private JButton btnConfirmar;
	private JButton btnModificarContraseña;

	public static Ventana_ConfiguracionEmail getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new Ventana_ConfiguracionEmail();
		}
		return INSTANCE;
	}

	public Ventana_ConfiguracionEmail() {
		frmMainConfigEmail = new JPanel();
		
		GridBagLayout gbl_frmMainConfigEmail = new GridBagLayout();
		gbl_frmMainConfigEmail.columnWidths = new int[] { 0, 0 };
		gbl_frmMainConfigEmail.rowHeights = new int[] { 0, 0 };
		gbl_frmMainConfigEmail.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gbl_frmMainConfigEmail.rowWeights = new double[] { 1.0, Double.MIN_VALUE };
		frmMainConfigEmail.setLayout(gbl_frmMainConfigEmail);

		panelMail = new JPanel();
		GridBagConstraints gbc_panelMail = new GridBagConstraints();
		gbc_panelMail.fill = GridBagConstraints.BOTH;
		gbc_panelMail.gridx = 0;
		gbc_panelMail.gridy = 0;
		frmMainConfigEmail.add(panelMail, gbc_panelMail);
		GridBagLayout gbl_panelMail = new GridBagLayout();
		gbl_panelMail.columnWidths = new int[]{30, 200, 30, 250, 85, 0};
		gbl_panelMail.rowHeights = new int[]{30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 0};
		gbl_panelMail.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_panelMail.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		panelMail.setLayout(gbl_panelMail);
		
		JLabel lblConfiguracionDeEmail = new JLabel("Configuración de email");
		lblConfiguracionDeEmail.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblConfiguracionDeEmail.setHorizontalAlignment(SwingConstants.LEFT);
		lblConfiguracionDeEmail.setPreferredSize(new Dimension(200, 30));
		GridBagConstraints gbc_lblConfiguracionDeEmail = new GridBagConstraints();
		gbc_lblConfiguracionDeEmail.fill = GridBagConstraints.BOTH;
		gbc_lblConfiguracionDeEmail.insets = new Insets(0, 10, 5, 5);
		gbc_lblConfiguracionDeEmail.gridx = 1;
		gbc_lblConfiguracionDeEmail.gridy = 1;
		panelMail.add(lblConfiguracionDeEmail, gbc_lblConfiguracionDeEmail);
		
		JLabel lblMail = new JLabel("Mail de la empresa:");
		lblMail.setPreferredSize(new Dimension(200, 30));
		GridBagConstraints gbc_lblMail = new GridBagConstraints();
		gbc_lblMail.fill = GridBagConstraints.BOTH;
		gbc_lblMail.insets = new Insets(5, 5, 5, 5);
		gbc_lblMail.gridx = 1;
		gbc_lblMail.gridy = 2;
		panelMail.add(lblMail, gbc_lblMail);
		
		txtMail = new JTextField();
		txtMail.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_txtMail = new GridBagConstraints();
		gbc_txtMail.gridwidth = 2;
		gbc_txtMail.insets = new Insets(0, 0, 5, 0);
		gbc_txtMail.fill = GridBagConstraints.BOTH;
		gbc_txtMail.gridx = 3;
		gbc_txtMail.gridy = 2;
		panelMail.add(txtMail, gbc_txtMail);
		txtMail.setColumns(10);
		
		JLabel lblRemitente = new JLabel("Remitente:");
		lblRemitente.setPreferredSize(new Dimension(200, 30));
		GridBagConstraints gbc_lblRemitente = new GridBagConstraints();
		gbc_lblRemitente.fill = GridBagConstraints.BOTH;
		gbc_lblRemitente.insets = new Insets(5, 5, 5, 5);
		gbc_lblRemitente.gridx = 1;
		gbc_lblRemitente.gridy = 3;
		panelMail.add(lblRemitente, gbc_lblRemitente);
		
		txtRemitente = new JTextField();
		txtRemitente.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_txtRemitente = new GridBagConstraints();
		gbc_txtRemitente.gridwidth = 2;
		gbc_txtRemitente.insets = new Insets(0, 0, 5, 0);
		gbc_txtRemitente.fill = GridBagConstraints.BOTH;
		gbc_txtRemitente.gridx = 3;
		gbc_txtRemitente.gridy = 3;
		panelMail.add(txtRemitente, gbc_txtRemitente);
		txtRemitente.setColumns(10);
		
		JLabel lblFirma = new JLabel("Firma:");
		lblFirma.setEnabled(true);
		lblFirma.setPreferredSize(new Dimension(200, 30));
		GridBagConstraints gbc_lblFirma = new GridBagConstraints();
		gbc_lblFirma.fill = GridBagConstraints.BOTH;
		gbc_lblFirma.insets = new Insets(5, 5, 5, 5);
		gbc_lblFirma.gridx = 1;
		gbc_lblFirma.gridy = 4;
		panelMail.add(lblFirma, gbc_lblFirma);
		
		txtFirma = new JTextArea();
		txtFirma.setEnabled(true);
		txtFirma.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_txtFirma = new GridBagConstraints();
		gbc_txtFirma.gridheight = 3;
		gbc_txtFirma.gridwidth = 2;
		gbc_txtFirma.insets = new Insets(0, 0, 5, 0);
		gbc_txtFirma.fill = GridBagConstraints.BOTH;
		gbc_txtFirma.gridx = 3;
		gbc_txtFirma.gridy = 4;
		panelMail.add(txtFirma, gbc_txtFirma);
		txtFirma.setColumns(10);
		
		btnModificarContraseña = new JButton("Modificar contraseña");
		GridBagConstraints gbc_btnModificarContrasea = new GridBagConstraints();
		gbc_btnModificarContrasea.fill = GridBagConstraints.BOTH;
		gbc_btnModificarContrasea.insets = new Insets(0, 0, 5, 5);
		gbc_btnModificarContrasea.gridx = 1;
		gbc_btnModificarContrasea.gridy = 8;
		panelMail.add(btnModificarContraseña, gbc_btnModificarContrasea);
		
		btnConfirmar = new JButton("Guardar configuraciones");
		GridBagConstraints gbc_btnConfirmar = new GridBagConstraints();
		gbc_btnConfirmar.fill = GridBagConstraints.BOTH;
		gbc_btnConfirmar.gridx = 4;
		gbc_btnConfirmar.gridy = 9;
		panelMail.add(btnConfirmar, gbc_btnConfirmar);

		this.frmMainConfigEmail.setVisible(true);
	}

	public JButton getBtnConfirmar() {
		return btnConfirmar;
	}
	
	public JButton getBtnContraseña() {
		return btnModificarContraseña;
	}

	public void mostrarVentana() {
		this.frmMainConfigEmail.setVisible(true);
	}

	public JPanel getPanel() {
		return frmMainConfigEmail;
	}

	public JTextField getTxtMail() {
		return txtMail;
	}

	public JTextField getTxtRemitente() {
		return txtRemitente;
	}
	
	private boolean camposVacios() {
		if (txtMail.getText().isEmpty() || txtRemitente.getText().isEmpty()
			// || txtFirma.getText().isEmpty() 
			//|| String.valueOf(getTxtContraseña().getPassword()).isEmpty()
			//|| String.valueOf(getTxtConfirmarContraseña().getPassword()).isEmpty()
				)
		{
			JOptionPane.showMessageDialog(null, "Los campos Mail y remitente no pueden quedar vacíos.");
			return true;
		}
		else
			return false;
	}
	
	private boolean camposCorrectos() {
		ValidacionCampos validador = new ValidacionCampos();
		if(!validador.esMail(txtMail.getText()) || !validador.esPalabra(txtRemitente.getText())
			//|| !validador.esPalabra(txtFirma.getText())
			)
			return false;
		else
			return true;
	}
	
	public boolean verificarCampos() {
		if (!camposVacios() && camposCorrectos())
			return true;
		else
			return false;
	}

	public JTextArea getTxtFirma() {
		return txtFirma;
	}
}