package vistas.administrador.locales;

import java.awt.Dialog.ModalityType;
import java.awt.Toolkit;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import vistas.ValidacionCampos;

public class Ventana_EditarLocal {
	
	private static Ventana_EditarLocal INSTANCE;
	private JDialog frmEditarLocal;
	private JPanel contentPane;
	private JTextField txtNombre, txtDireccion, txtLocalidad, txtDetalle;
	private JComboBox<String> cbProvincia;
	private JButton btnConfirmar;

	public static Ventana_EditarLocal getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new Ventana_EditarLocal();
		}
		return INSTANCE;
	}

	private Ventana_EditarLocal() {

		frmEditarLocal = new JDialog();
		frmEditarLocal.setIconImage(Toolkit.getDefaultToolkit().getImage(Ventana_EditarLocal.class.getResource("/imagenes/hermesicono.png")));
		frmEditarLocal.setModalityType(ModalityType.APPLICATION_MODAL);
		frmEditarLocal.setResizable(false);
		frmEditarLocal.setTitle("Hermes - Editar local");
		frmEditarLocal.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmEditarLocal.setBounds(100, 100, 436, 408);
		frmEditarLocal.setLocationRelativeTo(null);
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		frmEditarLocal.setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 430, 379);
		contentPane.add(panel);
		panel.setLayout(null);

		JLabel lblNombre = new JLabel("Nombre:");
		lblNombre.setHorizontalAlignment(SwingConstants.CENTER);
		lblNombre.setBounds(10, 15, 133, 30);
		panel.add(lblNombre);

		JLabel lblDireccion = new JLabel("Dirección:");
		lblDireccion.setHorizontalAlignment(SwingConstants.CENTER);
		lblDireccion.setBounds(10, 50, 133, 30);
		panel.add(lblDireccion);

		JLabel lblLocalidad = new JLabel("Localidad:");
		lblLocalidad.setHorizontalAlignment(SwingConstants.CENTER);
		lblLocalidad.setBounds(10, 85, 133, 30);
		panel.add(lblLocalidad);

		JLabel lblProvincia = new JLabel("Provincia:");
		lblProvincia.setHorizontalAlignment(SwingConstants.CENTER);
		lblProvincia.setBounds(10, 120, 133, 30);
		panel.add(lblProvincia);

		JLabel lblDetalle = new JLabel("Detalle:");
		lblDetalle.setHorizontalAlignment(SwingConstants.CENTER);
		lblDetalle.setBounds(10, 155, 133, 30);
		panel.add(lblDetalle);
		
		txtNombre = new JTextField();
		txtNombre.setColumns(10);
		txtNombre.setBounds(192, 15, 228, 30);
		panel.add(txtNombre);

		txtDireccion = new JTextField();
		txtDireccion.setBounds(192, 50, 228, 30);
		panel.add(txtDireccion);
		txtDireccion.setColumns(10);

		txtLocalidad = new JTextField();
		txtLocalidad.setColumns(10);
		txtLocalidad.setBounds(192, 85, 228, 30);
		panel.add(txtLocalidad);

		cbProvincia = new JComboBox<String>();
		cbProvincia.setBounds(192, 120, 228, 30);
		panel.add(cbProvincia);

		txtDetalle = new JTextField();
		txtDetalle.setColumns(10);
		txtDetalle.setBounds(192, 155, 228, 30);
		panel.add(txtDetalle);

		btnConfirmar = new JButton("Confirmar");
		btnConfirmar.setBounds(294, 345, 126, 23);
		panel.add(btnConfirmar);

		this.frmEditarLocal.setVisible(false);
	}

	public void mostrarVentana() {
		this.frmEditarLocal.setVisible(true);
	}

	public JTextField getTxtNombre() {
		return txtNombre;
	}
	
	public JTextField getTxtDireccion() {
		return txtDireccion;
	}

	public JTextField getTxtLocalidad() {
		return txtLocalidad;
	}
	
	public JComboBox<String> getComboBoxProvincia() {
		return cbProvincia;
	}

	public JTextField getTxtDetalle() {
		return txtDetalle;
	}

	public JButton getBtnConfirmar() {
		return btnConfirmar;
	}

	private boolean camposVacios() {
		if (txtNombre.getText().isEmpty() || txtDireccion.getText().isEmpty()
			|| txtLocalidad.getText().isEmpty())
		{
			JOptionPane.showMessageDialog(null, "Los campos Nombre, Direccion, Localidad y Provincia no pueden quedar vacíos.");
			return true;
		}
		else
			return false;
	}
	
	private boolean camposCorrectos() {
		ValidacionCampos validador = new ValidacionCampos();
		if(!validador.esPalabra(txtNombre.getText()) || !validador.esPalabra(txtLocalidad.getText()))
			return false;
		else
			return true;
	}
	
	public boolean verificarCampos() {
		if (!camposVacios() && camposCorrectos())
			return true;
		else
			return false;
	}

	public void cerrar() {
		this.frmEditarLocal.dispose();
	}
}
