package vistas.administrador.locales;

import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;

import vistas.TextPrompt;

public class Main_Locales {

	private static Main_Locales INSTANCE;
	private JPanel frmMainLocales;
	private JTable tablaLocales;
	private DefaultTableModel modelLocales;
	private String[] nombreColumnas = { "Nombre ", "Dirección", "Localidad", "Provincia", "Detalle", "Estado" };
	private JButton btnAgregar, btnEditar, btnBorrar, btnRealizarBusqueda;
	private JTextField txtNombre, txtLocalidad, txtProvincia;
	private JRadioButton radioTodos, radioHabilitados, radioInhabilitados;
	private ButtonGroup filtro;
	private JPanel panelBusqueda, panelABM, panelTabla;

	public static Main_Locales getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new Main_Locales();
		}
		return INSTANCE;
	}

	public Main_Locales() {

		frmMainLocales = new JPanel();

		filtro = new ButtonGroup();
		
		GridBagLayout gbl_frmMainLocales = new GridBagLayout();
		gbl_frmMainLocales.columnWidths = new int[] { 999, 0 };
		gbl_frmMainLocales.rowHeights = new int[] { 69, 182, 427, 0 };
		gbl_frmMainLocales.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gbl_frmMainLocales.rowWeights = new double[] { 0.0, 0.0, 1.0, Double.MIN_VALUE };
		frmMainLocales.setLayout(gbl_frmMainLocales);

		panelABM = new JPanel();
		GridBagConstraints gbc_panelABM = new GridBagConstraints();
		gbc_panelABM.fill = GridBagConstraints.BOTH;
		gbc_panelABM.insets = new Insets(0, 0, 5, 0);
		gbc_panelABM.gridx = 0;
		gbc_panelABM.gridy = 0;
		frmMainLocales.add(panelABM, gbc_panelABM);
		panelABM.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		btnAgregar = new JButton("Agregar");
		btnAgregar.setIcon(new ImageIcon(Main_Locales.class.getResource("/imagenes/iconos/localAgregar.png")));
		panelABM.add(btnAgregar);

		btnBorrar = new JButton("Habilitar / Inhabilitar");
		btnBorrar.setIcon(new ImageIcon(Main_Locales.class.getResource("/imagenes/iconos/localHabInhab.png")));
		panelABM.add(btnBorrar);

		btnEditar = new JButton("Editar");
		btnEditar.setIcon(new ImageIcon(Main_Locales.class.getResource("/imagenes/iconos/localEditar.png")));
		panelABM.add(btnEditar);

		panelBusqueda = new JPanel();
		GridBagConstraints gbc_panelBusqueda = new GridBagConstraints();
		gbc_panelBusqueda.fill = GridBagConstraints.BOTH;
		gbc_panelBusqueda.insets = new Insets(0, 0, 5, 0);
		gbc_panelBusqueda.gridx = 0;
		gbc_panelBusqueda.gridy = 1;
		frmMainLocales.add(panelBusqueda, gbc_panelBusqueda);
		GridBagLayout gbl_panelBusqueda = new GridBagLayout();
		gbl_panelBusqueda.columnWidths = new int[] { 127, 58, 145, 122, 117, 0 };
		gbl_panelBusqueda.rowHeights = new int[] { 30, 25, 25, 26, 0 };
		gbl_panelBusqueda.columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		gbl_panelBusqueda.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		panelBusqueda.setLayout(gbl_panelBusqueda);

		JLabel lblFiltrarPor = new JLabel("Filtrar por:");
		lblFiltrarPor.setPreferredSize(new Dimension(51, 30));
		lblFiltrarPor.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_lblFiltrarPor = new GridBagConstraints();
		gbc_lblFiltrarPor.fill = GridBagConstraints.BOTH;
		gbc_lblFiltrarPor.insets = new Insets(0, 0, 5, 5);
		gbc_lblFiltrarPor.gridx = 0;
		gbc_lblFiltrarPor.gridy = 0;
		panelBusqueda.add(lblFiltrarPor, gbc_lblFiltrarPor);

		JLabel lblBusquedaPor = new JLabel("Búsqueda por:");
		lblBusquedaPor.setPreferredSize(new Dimension(70, 30));
		lblBusquedaPor.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_lblBusquedaPor = new GridBagConstraints();
		gbc_lblBusquedaPor.fill = GridBagConstraints.BOTH;
		gbc_lblBusquedaPor.insets = new Insets(0, 0, 5, 5);
		gbc_lblBusquedaPor.gridx = 2;
		gbc_lblBusquedaPor.gridy = 0;
		panelBusqueda.add(lblBusquedaPor, gbc_lblBusquedaPor);

		btnRealizarBusqueda = new JButton("Buscar");
		btnRealizarBusqueda.setPreferredSize(new Dimension(65, 30));
		GridBagConstraints gbc_btnRealizarBusqueda = new GridBagConstraints();
		gbc_btnRealizarBusqueda.fill = GridBagConstraints.BOTH;
		gbc_btnRealizarBusqueda.insets = new Insets(0, 0, 5, 5);
		gbc_btnRealizarBusqueda.gridx = 3;
		gbc_btnRealizarBusqueda.gridy = 0;
		panelBusqueda.add(btnRealizarBusqueda, gbc_btnRealizarBusqueda);

		radioTodos = new JRadioButton("Todos");
		radioTodos.setPreferredSize(new Dimension(55, 30));
		GridBagConstraints gbc_radioTodos = new GridBagConstraints();
		gbc_radioTodos.fill = GridBagConstraints.BOTH;
		gbc_radioTodos.insets = new Insets(0, 0, 5, 5);
		gbc_radioTodos.gridx = 0;
		gbc_radioTodos.gridy = 1;
		panelBusqueda.add(radioTodos, gbc_radioTodos);
		filtro.add(radioTodos);

		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setPreferredSize(new Dimension(63, 30));
		GridBagConstraints gbc_lblNombre = new GridBagConstraints();
		gbc_lblNombre.fill = GridBagConstraints.BOTH;
		gbc_lblNombre.insets = new Insets(0, 0, 5, 5);
		gbc_lblNombre.gridx = 2;
		gbc_lblNombre.gridy = 1;
		panelBusqueda.add(lblNombre, gbc_lblNombre);

		txtNombre = new JTextField();
		txtNombre.setPreferredSize(new Dimension(6, 30));
		GridBagConstraints gbc_txtNombre = new GridBagConstraints();
		gbc_txtNombre.gridwidth = 2;
		gbc_txtNombre.fill = GridBagConstraints.BOTH;
		gbc_txtNombre.insets = new Insets(0, 0, 5, 5);
		gbc_txtNombre.gridx = 3;
		gbc_txtNombre.gridy = 1;
		panelBusqueda.add(txtNombre, gbc_txtNombre);
		txtNombre.setColumns(10);
		TextPrompt phNombre = new TextPrompt("Ingrese nombre...", txtNombre);
		phNombre.setText("Ingrese nombre...");
		phNombre.changeAlpha(0.75f);
		phNombre.changeStyle(Font.ITALIC);

		radioHabilitados = new JRadioButton("Habilitados");
		radioHabilitados.setPreferredSize(new Dimension(77, 30));
		GridBagConstraints gbc_radioHabilitados = new GridBagConstraints();
		gbc_radioHabilitados.fill = GridBagConstraints.BOTH;
		gbc_radioHabilitados.insets = new Insets(0, 0, 5, 5);
		gbc_radioHabilitados.gridx = 0;
		gbc_radioHabilitados.gridy = 2;
		panelBusqueda.add(radioHabilitados, gbc_radioHabilitados);
		radioHabilitados.setSelected(true);
		filtro.add(radioHabilitados);

		JLabel lblLocalidad = new JLabel("Localidad");
		lblLocalidad.setPreferredSize(new Dimension(69, 30));
		GridBagConstraints gbc_lblLocalidad = new GridBagConstraints();
		gbc_lblLocalidad.fill = GridBagConstraints.BOTH;
		gbc_lblLocalidad.insets = new Insets(0, 0, 5, 5);
		gbc_lblLocalidad.gridx = 2;
		gbc_lblLocalidad.gridy = 2;
		panelBusqueda.add(lblLocalidad, gbc_lblLocalidad);

		txtLocalidad = new JTextField();
		txtLocalidad.setPreferredSize(new Dimension(6, 30));
		GridBagConstraints gbc_txtLocalidad = new GridBagConstraints();
		gbc_txtLocalidad.gridwidth = 2;
		gbc_txtLocalidad.fill = GridBagConstraints.BOTH;
		gbc_txtLocalidad.insets = new Insets(0, 0, 5, 5);
		gbc_txtLocalidad.gridx = 3;
		gbc_txtLocalidad.gridy = 2;
		panelBusqueda.add(txtLocalidad, gbc_txtLocalidad);
		txtLocalidad.setColumns(10);
		TextPrompt phUsuario = new TextPrompt("Ingrese usuario...", txtLocalidad);
		phUsuario.setText("Ingrese localidad...");
		phUsuario.changeAlpha(0.75f);
		phUsuario.changeStyle(Font.ITALIC);

		radioInhabilitados = new JRadioButton("Inhabilitados");
		radioInhabilitados.setPreferredSize(new Dimension(87, 30));
		GridBagConstraints gbc_radioInhabilitados = new GridBagConstraints();
		gbc_radioInhabilitados.fill = GridBagConstraints.BOTH;
		gbc_radioInhabilitados.insets = new Insets(0, 0, 0, 5);
		gbc_radioInhabilitados.gridx = 0;
		gbc_radioInhabilitados.gridy = 3;
		panelBusqueda.add(radioInhabilitados, gbc_radioInhabilitados);
		filtro.add(radioInhabilitados);

		JLabel lblProvincia = new JLabel("Provincia");
		lblProvincia.setPreferredSize(new Dimension(69, 30));
		GridBagConstraints gbc_lblProvincia = new GridBagConstraints();
		gbc_lblProvincia.fill = GridBagConstraints.BOTH;
		gbc_lblProvincia.insets = new Insets(0, 0, 0, 5);
		gbc_lblProvincia.gridx = 2;
		gbc_lblProvincia.gridy = 3;
		panelBusqueda.add(lblProvincia, gbc_lblProvincia);

		txtProvincia = new JTextField();
		txtProvincia.setPreferredSize(new Dimension(6, 30));
		GridBagConstraints gbc_txtProvincia = new GridBagConstraints();
		gbc_txtProvincia.gridwidth = 2;
		gbc_txtProvincia.insets = new Insets(0, 0, 0, 5);
		gbc_txtProvincia.fill = GridBagConstraints.BOTH;
		gbc_txtProvincia.gridx = 3;
		gbc_txtProvincia.gridy = 3;
		panelBusqueda.add(txtProvincia, gbc_txtProvincia);
		txtProvincia.setColumns(10);
		TextPrompt phProvincia = new TextPrompt("Ingrese provincia...", txtProvincia);
		phProvincia.setText("Ingrese provincia...");
		phProvincia.changeAlpha(0.75f);
		phProvincia.changeStyle(Font.ITALIC);

		panelTabla = new JPanel();
		GridBagConstraints gbc_panelTabla = new GridBagConstraints();
		gbc_panelTabla.fill = GridBagConstraints.BOTH;
		gbc_panelTabla.gridx = 0;
		gbc_panelTabla.gridy = 2;
		frmMainLocales.add(panelTabla, gbc_panelTabla);
		panelTabla.setLayout(new CardLayout(0, 0));

		JScrollPane spLocales = new JScrollPane();
		panelTabla.add(spLocales, "name_73104041868264");
		modelLocales = new DefaultTableModel(null, nombreColumnas) {
			private static final long serialVersionUID = 1L;

			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		tablaLocales = new JTable(modelLocales);
		tablaLocales.getTableHeader().setReorderingAllowed(false);
		spLocales.setViewportView(tablaLocales);

		this.frmMainLocales.setVisible(false);
	}

	public void mostrarVentana() {
		this.frmMainLocales.setVisible(true);
	}

	public JButton getBtnAgregar() {
		return btnAgregar;
	}

	public JButton getBtnBorrar() {
		return btnBorrar;
	}

	public JButton getBtnEditar() {
		return btnEditar;
	}

	public DefaultTableModel getModelLocales() {
		return modelLocales;
	}

	public JTable getTablaLocales() {
		return tablaLocales;
	}

	public String[] getNombreColumnas() {
		return nombreColumnas;
	}

	public JButton getBtnRealizarBusqueda() {
		return btnRealizarBusqueda;
	}

	public JTextField getBarraBusquedaLocaldiad() {
		return txtLocalidad;
	}

	public JTextField getBarraBusquedaProvincia() {
		return txtProvincia;
	}

	public JTextField getTxtNombre() {
		return txtNombre;
	}

	public JRadioButton getRadioTodos() {
		return radioTodos;
	}

	public JRadioButton getRadioHabilitados() {
		return radioHabilitados;
	}

	public JRadioButton getRadioInhabilitados() {
		return radioInhabilitados;
	}

	public JTextField getTxtLocalidad() {
		return txtLocalidad;
	}

	public JTextField getTxtProvincia() {
		return txtProvincia;
	}

	public void mostrarBarraBusquedaPorLocalidad() {
		btnRealizarBusqueda.setVisible(true);
		txtLocalidad.setVisible(true);

		txtNombre.setVisible(false);
		txtProvincia.setVisible(false);
		txtNombre.setText("");
		txtProvincia.setText("");
	}

	public void mostrarBusquedaPorNombre() {
		btnRealizarBusqueda.setVisible(true);
		txtNombre.setVisible(true);

		txtProvincia.setVisible(false);
		txtLocalidad.setVisible(false);
		txtLocalidad.setText("");
		txtProvincia.setText("");
	}

	public void mostrarBusquedaPorProvincia() {
		btnRealizarBusqueda.setVisible(true);
		txtProvincia.setVisible(true);

		txtLocalidad.setVisible(false);
		txtNombre.setVisible(false);
		txtNombre.setText("");
		txtLocalidad.setText("");
	}

	public JPanel getPanel() {
		return frmMainLocales;
	}
}