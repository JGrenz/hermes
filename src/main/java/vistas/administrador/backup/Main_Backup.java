package vistas.administrador.backup;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JPanel;
import javax.swing.JButton;
import java.awt.Insets;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

public class Main_Backup {

	private static Main_Backup INSTANCE;
	private JPanel frmMainBackup;
	private JPanel panelABM;
	private JTextField txtRutaExportar, txtRutaImportar;
	private JButton btnRutaExportar, btnExportar, btnRutaImportar, btnImportar;

	public static Main_Backup getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new Main_Backup();
		}
		return INSTANCE;
	}

	public Main_Backup() {

		frmMainBackup = new JPanel();

		GridBagLayout gbl_frmMainBackup = new GridBagLayout();
		gbl_frmMainBackup.columnWidths = new int[] { 0, 0 };
		gbl_frmMainBackup.rowHeights = new int[] { 0, 0 };
		gbl_frmMainBackup.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gbl_frmMainBackup.rowWeights = new double[] { 1.0, Double.MIN_VALUE };
		frmMainBackup.setLayout(gbl_frmMainBackup);

		panelABM = new JPanel();
		GridBagConstraints gbc_panelABM = new GridBagConstraints();
		gbc_panelABM.fill = GridBagConstraints.BOTH;
		gbc_panelABM.gridx = 0;
		gbc_panelABM.gridy = 0;
		frmMainBackup.add(panelABM, gbc_panelABM);
		GridBagLayout gbl_panelABM = new GridBagLayout();
		gbl_panelABM.columnWidths = new int[]{30, 100, 324, 30, 0, 0};
		gbl_panelABM.rowHeights = new int[]{30, 30, 0, 30, 30, 30, 0};
		gbl_panelABM.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_panelABM.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		panelABM.setLayout(gbl_panelABM);
		
		JLabel lblRutaExportar = new JLabel("Ruta exportar:");
		lblRutaExportar.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_lblRutaExportar = new GridBagConstraints();
		gbc_lblRutaExportar.fill = GridBagConstraints.BOTH;
		gbc_lblRutaExportar.insets = new Insets(0, 0, 5, 5);
		gbc_lblRutaExportar.gridx = 1;
		gbc_lblRutaExportar.gridy = 1;
		panelABM.add(lblRutaExportar, gbc_lblRutaExportar);
		
		txtRutaExportar = new JTextField();
		txtRutaExportar.setEditable(false);
		GridBagConstraints gbc_txtRutaExportar = new GridBagConstraints();
		gbc_txtRutaExportar.insets = new Insets(0, 0, 5, 5);
		gbc_txtRutaExportar.fill = GridBagConstraints.BOTH;
		gbc_txtRutaExportar.gridx = 2;
		gbc_txtRutaExportar.gridy = 1;
		panelABM.add(txtRutaExportar, gbc_txtRutaExportar);
		txtRutaExportar.setColumns(10);
		
		btnRutaExportar = new JButton("Elegir ruta para exportar");
		GridBagConstraints gbc_btnRutaExportar = new GridBagConstraints();
		gbc_btnRutaExportar.fill = GridBagConstraints.BOTH;
		gbc_btnRutaExportar.insets = new Insets(0, 0, 5, 0);
		gbc_btnRutaExportar.gridx = 4;
		gbc_btnRutaExportar.gridy = 1;
		panelABM.add(btnRutaExportar, gbc_btnRutaExportar);
		
		btnExportar = new JButton("Exportar back up");
		GridBagConstraints gbc_btnExportar = new GridBagConstraints();
		gbc_btnExportar.insets = new Insets(0, 0, 5, 0);
		gbc_btnExportar.fill = GridBagConstraints.BOTH;
		gbc_btnExportar.gridx = 4;
		gbc_btnExportar.gridy = 2;
		panelABM.add(btnExportar, gbc_btnExportar);
		
		JLabel lblRutaImportar = new JLabel("Ruta importar:");
		lblRutaImportar.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_lblRutaImportar = new GridBagConstraints();
		gbc_lblRutaImportar.fill = GridBagConstraints.BOTH;
		gbc_lblRutaImportar.insets = new Insets(0, 0, 5, 5);
		gbc_lblRutaImportar.gridx = 1;
		gbc_lblRutaImportar.gridy = 4;
		panelABM.add(lblRutaImportar, gbc_lblRutaImportar);
		
		txtRutaImportar = new JTextField();
		txtRutaImportar.setEditable(false);
		txtRutaImportar.setColumns(10);
		GridBagConstraints gbc_txtRutaImportar = new GridBagConstraints();
		gbc_txtRutaImportar.insets = new Insets(0, 0, 5, 5);
		gbc_txtRutaImportar.fill = GridBagConstraints.BOTH;
		gbc_txtRutaImportar.gridx = 2;
		gbc_txtRutaImportar.gridy = 4;
		panelABM.add(txtRutaImportar, gbc_txtRutaImportar);
		
		btnRutaImportar = new JButton("Elegir ruta para importar");
		GridBagConstraints gbc_btnRutaImportar = new GridBagConstraints();
		gbc_btnRutaImportar.fill = GridBagConstraints.BOTH;
		gbc_btnRutaImportar.insets = new Insets(0, 0, 5, 0);
		gbc_btnRutaImportar.gridx = 4;
		gbc_btnRutaImportar.gridy = 4;
		panelABM.add(btnRutaImportar, gbc_btnRutaImportar);
		
		btnImportar = new JButton("Importar back up");
		GridBagConstraints gbc_btnImportar = new GridBagConstraints();
		gbc_btnImportar.fill = GridBagConstraints.BOTH;
		gbc_btnImportar.gridx = 4;
		gbc_btnImportar.gridy = 5;
		panelABM.add(btnImportar, gbc_btnImportar);

		this.frmMainBackup.setVisible(false);
	}

	public void mostrarVentana() {
		this.frmMainBackup.setVisible(true);
	}

	public JPanel getPanel() {
		return frmMainBackup;
	}

	public JTextField getTxtRutaExportar() {
		return txtRutaExportar;
	}

	public JTextField getTxtRutaImportar() {
		return txtRutaImportar;
	}

	public JButton getBtnRutaExportar() {
		return btnRutaExportar;
	}

	public JButton getBtnExportar() {
		return btnExportar;
	}

	public JButton getBtnRutaImportar() {
		return btnRutaImportar;
	}

	public JButton getBtnImportar() {
		return btnImportar;
	}
}