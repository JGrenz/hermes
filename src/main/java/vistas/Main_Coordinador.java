package vistas;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Paint;
import java.awt.Toolkit;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

public class Main_Coordinador {

	private static Main_Coordinador INSTANCE;
	private JFrame frmMainCoordinador;
	private JButton btnUbicaciones, btnViajes, btnOfertas, btnEventos, btnGastos, btnTransporte, btnDesconectar,
			btnPerfilDeUsuario;
	private JPanel panelOeste, panelCentro;

	public static Main_Coordinador getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new Main_Coordinador();
		}
		return INSTANCE;
	}

	@SuppressWarnings("serial")
	public Main_Coordinador() {

		frmMainCoordinador = new JFrame();
		frmMainCoordinador.setIconImage(
				Toolkit.getDefaultToolkit().getImage(Main_Coordinador.class.getResource("/imagenes/hermesicono.png")));
		frmMainCoordinador.setTitle("Hermes - Coordinador");
		frmMainCoordinador.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmMainCoordinador.getContentPane().setLayout(new BorderLayout(0, 0));
		frmMainCoordinador.setExtendedState(frmMainCoordinador.getExtendedState() | JFrame.MAXIMIZED_BOTH);

		panelOeste = new JPanel() {
			protected void paintComponent(Graphics g) {
				if (g instanceof Graphics2D) {
					final int R = 153;
					final int G = 255;
					final int B = 153;
					Paint p = new GradientPaint(0.0f, 0.0f, new Color(R, G, B, 255), getWidth(), 0/*getHeight()*/,
							new Color(R, G, B, 0), true);
					Graphics2D g2d = (Graphics2D) g;
					g2d.setPaint(p);
					g2d.fillRect(0, 0, getWidth(), getHeight());
				} else {
					super.paintComponent(g);
				}
			}
		};
		panelOeste.setBackground(new Color(0,0,0,0));
		frmMainCoordinador.getContentPane().add(panelOeste, BorderLayout.WEST);
		GridBagLayout gbl_panelOeste = new GridBagLayout();
		gbl_panelOeste.columnWidths = new int[] { 1 };
		gbl_panelOeste.rowHeights = new int[] { 38, 23, 39, 23, 39, 39, 40, 0, 39, 0 };
		gbl_panelOeste.columnWeights = new double[] { 0.0 };
		gbl_panelOeste.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE };
		panelOeste.setLayout(gbl_panelOeste);

		btnUbicaciones = new JButton("Ubicaciones");
		btnUbicaciones.setHorizontalAlignment(SwingConstants.LEADING);
		btnUbicaciones.setIcon(new ImageIcon(Main_Coordinador.class.getResource("/imagenes/iconos/ubicacion.png")));
		GridBagConstraints gbc_btnUbicaciones = new GridBagConstraints();
		gbc_btnUbicaciones.fill = GridBagConstraints.BOTH;
		gbc_btnUbicaciones.insets = new Insets(10, 10, 5, 10);
		gbc_btnUbicaciones.gridx = 0;
		gbc_btnUbicaciones.gridy = 0;
		panelOeste.add(btnUbicaciones, gbc_btnUbicaciones);

		btnViajes = new JButton("Viajes");
		btnViajes.setHorizontalAlignment(SwingConstants.LEADING);
		btnViajes.setIcon(new ImageIcon(Main_Coordinador.class.getResource("/imagenes/iconos/viaje.png")));
		GridBagConstraints gbc_btnViajes = new GridBagConstraints();
		gbc_btnViajes.fill = GridBagConstraints.BOTH;
		gbc_btnViajes.insets = new Insets(5, 10, 5, 10);
		gbc_btnViajes.gridx = 0;
		gbc_btnViajes.gridy = 1;
		panelOeste.add(btnViajes, gbc_btnViajes);

		btnOfertas = new JButton("Ofertas");
		btnOfertas.setHorizontalAlignment(SwingConstants.LEADING);
		btnOfertas.setIcon(new ImageIcon(Main_Coordinador.class.getResource("/imagenes/iconos/oferta.png")));
		GridBagConstraints gbc_btnOfertas = new GridBagConstraints();
		gbc_btnOfertas.fill = GridBagConstraints.BOTH;
		gbc_btnOfertas.insets = new Insets(5, 10, 5, 10);
		gbc_btnOfertas.gridx = 0;
		gbc_btnOfertas.gridy = 2;
		panelOeste.add(btnOfertas, gbc_btnOfertas);

		btnEventos = new JButton("Eventos");
		btnEventos.setHorizontalAlignment(SwingConstants.LEADING);
		btnEventos.setIcon(new ImageIcon(Main_Coordinador.class.getResource("/imagenes/iconos/evento.png")));
		GridBagConstraints gbc_btnEventos = new GridBagConstraints();
		gbc_btnEventos.fill = GridBagConstraints.BOTH;
		gbc_btnEventos.insets = new Insets(5, 10, 5, 10);
		gbc_btnEventos.gridx = 0;
		gbc_btnEventos.gridy = 3;
		panelOeste.add(btnEventos, gbc_btnEventos);

		btnGastos = new JButton("Gastos");
		btnGastos.setIcon(new ImageIcon(Main_Coordinador.class.getResource("/imagenes/iconos/gastos.png")));
		btnGastos.setHorizontalAlignment(SwingConstants.LEADING);
		GridBagConstraints gbc_btnGastos = new GridBagConstraints();
		gbc_btnGastos.fill = GridBagConstraints.BOTH;
		gbc_btnGastos.insets = new Insets(5, 10, 5, 10);
		gbc_btnGastos.gridx = 0;
		gbc_btnGastos.gridy = 4;
		panelOeste.add(btnGastos, gbc_btnGastos);

		btnTransporte = new JButton("Transporte");
		btnTransporte.setHorizontalAlignment(SwingConstants.LEADING);
		btnTransporte.setIcon(new ImageIcon(Main_Coordinador.class.getResource("/imagenes/iconos/transporte.png")));
		GridBagConstraints gbc_btnTransporte = new GridBagConstraints();
		gbc_btnTransporte.fill = GridBagConstraints.BOTH;
		gbc_btnTransporte.insets = new Insets(5, 10, 5, 10);
		gbc_btnTransporte.gridx = 0;
		gbc_btnTransporte.gridy = 5;
		panelOeste.add(btnTransporte, gbc_btnTransporte);

		btnPerfilDeUsuario = new JButton("Perfil de usuario");
		btnPerfilDeUsuario.setHorizontalAlignment(SwingConstants.LEADING);
		btnPerfilDeUsuario.setIcon(new ImageIcon(Main_Coordinador.class.getResource("/imagenes/iconos/perfilUsuario.png")));
		GridBagConstraints gbc_btnPerfilDeUsuario = new GridBagConstraints();
		gbc_btnPerfilDeUsuario.fill = GridBagConstraints.BOTH;
		gbc_btnPerfilDeUsuario.insets = new Insets(5, 10, 5, 10);
		gbc_btnPerfilDeUsuario.gridx = 0;
		gbc_btnPerfilDeUsuario.gridy = 6;
		panelOeste.add(btnPerfilDeUsuario, gbc_btnPerfilDeUsuario);

		btnDesconectar = new JButton("Desconectar");
		btnDesconectar.setHorizontalAlignment(SwingConstants.LEADING);
		btnDesconectar.setIcon(new ImageIcon(Main_Coordinador.class.getResource("/imagenes/iconos/desconectar.png")));
		GridBagConstraints gbc_btnDesconectar = new GridBagConstraints();
		gbc_btnDesconectar.fill = GridBagConstraints.BOTH;
		gbc_btnDesconectar.insets = new Insets(0, 10, 10, 10);
		gbc_btnDesconectar.gridx = 0;
		gbc_btnDesconectar.gridy = 8;
		panelOeste.add(btnDesconectar, gbc_btnDesconectar);

		panelCentro = new JPanel();
		frmMainCoordinador.getContentPane().add(panelCentro, BorderLayout.CENTER);
		panelCentro.setLayout(new CardLayout(0, 0));

		this.frmMainCoordinador.setVisible(false);
	}

	public void mostrarVentana() {
		this.frmMainCoordinador.setVisible(true);
	}

	public JButton getBtnUbicaciones() {
		return btnUbicaciones;
	}

	public JButton getBtnViajes() {
		return btnViajes;
	}

	public JButton getBtnOfertas() {
		return btnOfertas;
	}

	public JButton getBtnEventos() {
		return btnEventos;
	}

	public JButton getBtnGastos() {
		return btnGastos;
	}

	public JButton getBtnTransporte() {
		return btnTransporte;
	}

	public JButton getBtnDesconectar() {
		return btnDesconectar;
	}
	
	public JButton getBtnPerfilDeUsuario() {
		return btnPerfilDeUsuario;
	}

	public void cerrarVentana() {
		this.frmMainCoordinador.dispose();
	}

	public JPanel getPanelCentro() {
		return panelCentro;
	}
}
