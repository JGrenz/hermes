package vistas;

import java.awt.Dialog.ModalityType;
import java.awt.Font;
import java.awt.Toolkit;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import vistas.TextPrompt;

public class Ventana_OlvidarContraseña {
	
	private static Ventana_OlvidarContraseña INSTANCE;
	private JDialog frmAgregarTransporte;
	private JPanel contentPane;
	private JTextField txtUsuario;
	private JButton btnConfirmar;

	public static Ventana_OlvidarContraseña getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new Ventana_OlvidarContraseña();
		}
		return INSTANCE;
	}

	private Ventana_OlvidarContraseña() {
		frmAgregarTransporte = new JDialog();
		frmAgregarTransporte.setIconImage(Toolkit.getDefaultToolkit().getImage(Ventana_OlvidarContraseña.class.getResource("/imagenes/hermesicono.png")));
		frmAgregarTransporte.setModalityType(ModalityType.APPLICATION_MODAL);
		frmAgregarTransporte.setResizable(false);
		frmAgregarTransporte.setTitle("Hermes - Recuperar contraseña");
		frmAgregarTransporte.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmAgregarTransporte.setBounds(100, 100, 436, 150);
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		frmAgregarTransporte.setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 430, 121);
		contentPane.add(panel);
		panel.setLayout(null);

		JLabel lbl = new JLabel("Ingrese su nombre de usuario");
		lbl.setBounds(10, 15, 228, 30);
		panel.add(lbl);

		txtUsuario = new JTextField();
		txtUsuario.setBounds(10, 56, 228, 30);
		TextPrompt phNombre = new TextPrompt("Ingrese nombre...", txtUsuario);
		phNombre.setText("Ingrese usuario...");
		phNombre.changeAlpha(0.75f);
		phNombre.changeStyle(Font.ITALIC);
		panel.add(txtUsuario);
		txtUsuario.setColumns(10);

		btnConfirmar = new JButton("Confirmar");
		btnConfirmar.setBounds(294, 80, 126, 30);
		panel.add(btnConfirmar);

		this.frmAgregarTransporte.setVisible(false);
	}

	public void mostrarVentana() {
		this.frmAgregarTransporte.setVisible(true);
	}

	public JTextField getTxtUsuario() {
		return txtUsuario;
	}

	public JButton getBtnConfirmar() {
		return btnConfirmar;
	}

	public void cerrar() {
		this.txtUsuario.setText(null);
		this.frmAgregarTransporte.dispose();
	}

}
