package vistas;

import java.awt.Color;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class Login extends JFrame {

	private static final long serialVersionUID = 1L;
	private JFrame frmLogin;
	private JButton btnConfirmar;
	private JTextField txtUser;
	private JPasswordField txtPass;
	private JLabel lblOlvidadoContraseña;

	public Login() {
		super();
		initialize();
	}

	private void initialize() {

		frmLogin = new JFrame();
		frmLogin.setIconImage(
				Toolkit.getDefaultToolkit().getImage(getClass().getResource("/imagenes/hermesicono.png")));
		frmLogin.setTitle("Hermes");
		frmLogin.setResizable(false);
		frmLogin.setBounds(100, 100, 575, 400);
		frmLogin.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmLogin.getContentPane().setLayout(null);
		frmLogin.setLocationRelativeTo(null);

		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 569, 371);
		frmLogin.getContentPane().add(panel);
		panel.setLayout(null);

		txtUser = new JTextField();
		txtUser.setBounds(387, 43, 172, 30);
		txtUser.setColumns(10);
		TextPrompt phUser = new TextPrompt("Ingrese usuario...", txtUser);
		phUser.changeAlpha(0.75f);
		phUser.changeStyle(Font.ITALIC);
		panel.add(txtUser);

		txtPass = new JPasswordField(10);
		txtPass.setBounds(387, 84, 172, 30);
		txtPass.setColumns(10);
		TextPrompt phPass = new TextPrompt("Ingrese contraseña...", txtPass);
		phPass.changeAlpha(0.75f);
		phPass.changeStyle(Font.ITALIC);
		panel.add(txtPass);

		JLabel lblUser = new JLabel("Usuario:");
		lblUser.setHorizontalAlignment(SwingConstants.RIGHT);
		lblUser.setBounds(285, 43, 91, 30);
		panel.add(lblUser);

		JLabel lblPass = new JLabel("Contraseña:");
		lblPass.setHorizontalAlignment(SwingConstants.RIGHT);
		lblPass.setBounds(285, 84, 91, 30);
		panel.add(lblPass);

		btnConfirmar = new JButton("Conectarse");
		btnConfirmar.setBounds(435, 156, 124, 30);
		panel.add(btnConfirmar);

		JLabel lblImagen = new JLabel();
		lblImagen.setIcon(new ImageIcon(Login.class.getResource("/imagenes/hermespersonaje.png")));
		lblImagen.setBounds(10, 11, 265, 300);
		panel.add(lblImagen);

		lblOlvidadoContraseña = new JLabel("He olvidado la contraseña");
		lblOlvidadoContraseña.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				lblOlvidadoContraseña.setForeground(Color.BLUE);
				lblOlvidadoContraseña.setFont(new Font("SansSerif", Font.BOLD, 12));
			}

			@Override
			public void mouseExited(MouseEvent e) {
				lblOlvidadoContraseña.setForeground(Color.BLACK);
				lblOlvidadoContraseña.setFont(new Font("SansSerif", Font.PLAIN, 12));
			}
		});
		lblOlvidadoContraseña.setBounds(406, 125, 153, 20);
		panel.add(lblOlvidadoContraseña);
	}

	public JButton getBtnConfirmar() {
		return btnConfirmar;
	}

	public JTextField getTxtUser() {
		return txtUser;
	}

	public JTextField getTxtPass() {
		return txtPass;
	}

	public void mostrarVentana() {
		this.frmLogin.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.frmLogin.setVisible(true);
	}

	public void cerrarVentana() {
		txtUser.setText(null);
		txtPass.setText(null);
		this.frmLogin.dispose();
	}

	public JLabel getOlvidadoContraseña() {
		return lblOlvidadoContraseña;
	}
}