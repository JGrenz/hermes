package vistas;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Paint;
import java.awt.Toolkit;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

public class Main_Administrador {

	private static Main_Administrador INSTANCE;
	private JFrame frmMainAdministrador;
	private JButton btnEmpleados, btnLocales, btnConfiguraciones, btnDesconectar, btnPerfilDeUsuario;
	private JPanel panelOeste, panelCentro;
	private JButton btnBackup;

	public static Main_Administrador getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new Main_Administrador();
		}
		return INSTANCE;
	}

	@SuppressWarnings("serial")
	public Main_Administrador() {

		frmMainAdministrador = new JFrame();
		frmMainAdministrador.setIconImage(Toolkit.getDefaultToolkit()
				.getImage(Main_Administrador.class.getResource("/imagenes/hermesicono.png")));
		frmMainAdministrador.setTitle("Hermes - Administrador");
		frmMainAdministrador.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmMainAdministrador.getContentPane().setLayout(new BorderLayout(0, 0));
		frmMainAdministrador.setExtendedState(frmMainAdministrador.getExtendedState() | JFrame.MAXIMIZED_BOTH);

		panelOeste = new JPanel() {
			protected void paintComponent(Graphics g) {
				if (g instanceof Graphics2D) {
					final int R = 102;
					final int G = 178;
					final int B = 255;
					Paint p = new GradientPaint(0.0f, 0.0f, new Color(R, G, B, 255), getWidth(), 0/*getHeight()*/,
							new Color(R, G, B, 0), true);
					Graphics2D g2d = (Graphics2D) g;
					g2d.setPaint(p);
					g2d.fillRect(0, 0, getWidth(), getHeight());
				} else {
					super.paintComponent(g);
				}
			}
		};
		panelOeste.setBackground(new Color(0,0,0,0));
		frmMainAdministrador.getContentPane().add(panelOeste, BorderLayout.WEST);
		GridBagLayout gbl_panelOeste = new GridBagLayout();
		gbl_panelOeste.columnWidths = new int[] { 83, 0 };
		gbl_panelOeste.rowHeights = new int[] { 23, 0, 0, 0, 0, 0 };
		gbl_panelOeste.columnWeights = new double[] { 0.0, Double.MIN_VALUE };
		gbl_panelOeste.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		panelOeste.setLayout(gbl_panelOeste);

		btnEmpleados = new JButton("Empleados");
		btnEmpleados.setHorizontalAlignment(SwingConstants.LEADING);
		btnEmpleados.setIcon(new ImageIcon(Main_Administrador.class.getResource("/imagenes/iconos/empleado.png")));
		GridBagConstraints gbc_btnEmpleados = new GridBagConstraints();
		gbc_btnEmpleados.fill = GridBagConstraints.BOTH;
		gbc_btnEmpleados.insets = new Insets(10, 10, 5, 10);
		gbc_btnEmpleados.gridx = 0;
		gbc_btnEmpleados.gridy = 0;
		panelOeste.add(btnEmpleados, gbc_btnEmpleados);

		btnLocales = new JButton("Locales");
		btnLocales.setHorizontalAlignment(SwingConstants.LEADING);
		btnLocales.setIcon(new ImageIcon(Main_Administrador.class.getResource("/imagenes/iconos/local.png")));
		GridBagConstraints gbc_btnLocales = new GridBagConstraints();
		gbc_btnLocales.fill = GridBagConstraints.BOTH;
		gbc_btnLocales.insets = new Insets(5, 10, 5, 10);
		gbc_btnLocales.gridx = 0;
		gbc_btnLocales.gridy = 1;
		panelOeste.add(btnLocales, gbc_btnLocales);

		btnConfiguraciones = new JButton("Configuraciones");
		btnConfiguraciones.setHorizontalAlignment(SwingConstants.LEADING);
		btnConfiguraciones
				.setIcon(new ImageIcon(Main_Administrador.class.getResource("/imagenes/iconos/configuracion.png")));
		GridBagConstraints gbc_btnConfiguraciones = new GridBagConstraints();
		gbc_btnConfiguraciones.fill = GridBagConstraints.BOTH;
		gbc_btnConfiguraciones.insets = new Insets(5, 10, 5, 10);
		gbc_btnConfiguraciones.gridx = 0;
		gbc_btnConfiguraciones.gridy = 2;
		panelOeste.add(btnConfiguraciones, gbc_btnConfiguraciones);

		btnPerfilDeUsuario = new JButton("Perfil de usuario");
		btnPerfilDeUsuario
				.setIcon(new ImageIcon(Main_Administrador.class.getResource("/imagenes/iconos/perfilUsuario.png")));
		btnPerfilDeUsuario.setHorizontalAlignment(SwingConstants.LEADING);
		GridBagConstraints gbc_btnPerfilDeUsuario = new GridBagConstraints();
		gbc_btnPerfilDeUsuario.fill = GridBagConstraints.BOTH;
		gbc_btnPerfilDeUsuario.insets = new Insets(5, 10, 5, 10);
		gbc_btnPerfilDeUsuario.gridx = 0;
		gbc_btnPerfilDeUsuario.gridy = 3;
		panelOeste.add(btnPerfilDeUsuario, gbc_btnPerfilDeUsuario);

		btnBackup = new JButton("Backup");
		btnBackup.setIcon(new ImageIcon(Main_Administrador.class.getResource("/imagenes/iconos/backup.png")));
		btnBackup.setHorizontalAlignment(SwingConstants.LEADING);
		GridBagConstraints gbc_btnBackup = new GridBagConstraints();
		gbc_btnBackup.fill = GridBagConstraints.BOTH;
		gbc_btnBackup.insets = new Insets(5, 10, 5, 10);
		gbc_btnBackup.gridx = 0;
		gbc_btnBackup.gridy = 4;
		panelOeste.add(btnBackup, gbc_btnBackup);

		btnDesconectar = new JButton("Desconectar");
		btnDesconectar.setIcon(new ImageIcon(Main_Administrador.class.getResource("/imagenes/iconos/desconectar.png")));
		btnDesconectar.setHorizontalAlignment(SwingConstants.LEADING);
		GridBagConstraints gbc_btnDesconectar = new GridBagConstraints();
		gbc_btnDesconectar.fill = GridBagConstraints.BOTH;
		gbc_btnDesconectar.insets = new Insets(5, 10, 10, 10);
		gbc_btnDesconectar.gridx = 0;
		gbc_btnDesconectar.gridy = 6;
		panelOeste.add(btnDesconectar, gbc_btnDesconectar);

		panelCentro = new JPanel();
		frmMainAdministrador.getContentPane().add(panelCentro);
		panelCentro.setLayout(new CardLayout(0, 0));

		this.frmMainAdministrador.setVisible(false);
	}

	public void mostrarVentana() {
		this.frmMainAdministrador.setVisible(true);
	}

	public JButton getBtnEmpleados() {
		return btnEmpleados;
	}

	public JButton getBtnLocales() {
		return btnLocales;
	}

	public JButton getBtnConfiguraciones() {
		return btnConfiguraciones;
	}

	public JButton getBtnDesconectar() {
		return btnDesconectar;
	}

	public JButton getBtnPerfilDeUsuario() {
		return btnPerfilDeUsuario;
	}

	public JButton getBtnBackup() {
		return btnBackup;
	}

	public void cerrarVentana() {
		this.frmMainAdministrador.dispose();
	}

	public JPanel getPanelCentro() {
		return panelCentro;
	}
}
