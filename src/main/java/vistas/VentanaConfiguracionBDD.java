package vistas;

import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import java.awt.Toolkit;

public class VentanaConfiguracionBDD extends JDialog {
	private static final long serialVersionUID = 1L;
	private JDialog frmConfigBDD;
	private JButton btnConfirmar;
	private JTextField txtHost, txtPuerto, txtBaseDeDatos, txtUser;
	private JPasswordField txtPass;

	public VentanaConfiguracionBDD() {
		super();
		initialize();
	}

	private void initialize() {
		frmConfigBDD = new JDialog();
		frmConfigBDD.setIconImage(Toolkit.getDefaultToolkit().getImage(VentanaConfiguracionBDD.class.getResource("/imagenes/hermesicono.png")));
		frmConfigBDD.setModalityType(ModalityType.APPLICATION_MODAL);
		frmConfigBDD.setTitle("Hermes");
		frmConfigBDD.setResizable(false);
		frmConfigBDD.setBounds(100, 100, 575, 400);
		frmConfigBDD.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmConfigBDD.setLocationRelativeTo(null);

		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 569, 371);
		frmConfigBDD.getContentPane().add(panel);
		panel.setLayout(null);

		txtHost = new JTextField();
		txtHost.setBounds(151, 43, 259, 30);
		txtHost.setColumns(10);
		TextPrompt phHost = new TextPrompt("Ingrese IP...", txtHost);
		phHost.changeAlpha(0.75f);
		phHost.changeStyle(Font.ITALIC);
		panel.add(txtHost);

		txtPuerto = new JTextField();
		txtPuerto.setBounds(151, 99, 259, 30);
		txtPuerto.setColumns(10);
		TextPrompt phPuerto = new TextPrompt("Ingrese el puerto...", txtPuerto);
		phPuerto.changeAlpha(0.75f);
		phPuerto.changeStyle(Font.ITALIC);
		panel.add(txtPuerto);

		txtBaseDeDatos = new JTextField();
		txtBaseDeDatos.setBounds(151, 152, 259, 30);
		txtBaseDeDatos.setColumns(10);
		TextPrompt phBDD = new TextPrompt("Ingrese el nombre de la base de datos...", txtBaseDeDatos);
		phBDD.changeAlpha(0.75f);
		phBDD.changeStyle(Font.ITALIC);
		panel.add(txtBaseDeDatos);

		txtUser = new JTextField();
		txtUser.setBounds(151, 205, 259, 30);
		txtUser.setColumns(10);
		TextPrompt phUser = new TextPrompt("Ingrese usuario...", txtUser);
		phUser.changeAlpha(0.75f);
		phUser.changeStyle(Font.ITALIC);
		panel.add(txtUser);

		txtPass = new JPasswordField(10);
		txtPass.setBounds(151, 261, 259, 30);
		txtPass.setColumns(10);
		TextPrompt phPass = new TextPrompt("Ingrese contraseña...", txtPass);
		phPass.changeAlpha(0.75f);
		phPass.changeStyle(Font.ITALIC);
		panel.add(txtPass);

		JLabel lblUser = new JLabel("Usuario:");
		lblUser.setHorizontalAlignment(SwingConstants.RIGHT);
		lblUser.setBounds(37, 205, 91, 30);
		panel.add(lblUser);

		JLabel lblPass = new JLabel("Contraseña:");
		lblPass.setHorizontalAlignment(SwingConstants.RIGHT);
		lblPass.setBounds(37, 261, 91, 30);
		panel.add(lblPass);

		JLabel lblBaseDeDatos = new JLabel("Base de datos:");
		lblBaseDeDatos.setBounds(37, 159, 100, 16);
		panel.add(lblBaseDeDatos);

		JLabel lblPuerto = new JLabel("Puerto:");
		lblPuerto.setBounds(83, 106, 56, 16);
		panel.add(lblPuerto);

		JLabel lblIp = new JLabel("IP:");
		lblIp.setBounds(105, 50, 56, 16);
		panel.add(lblIp);
		
		btnConfirmar = new JButton("Confirmar");
		btnConfirmar.setBounds(433, 316, 124, 30);
		panel.add(btnConfirmar);
	}

	public JButton getBtnConfirmar() {
		return btnConfirmar;
	}

	public JTextField getTxtUser() {
		return txtUser;
	}

	public JTextField getTxtContraseña() {
		return txtPass;
	}

	public void mostrarVentana() {
		this.frmConfigBDD.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.frmConfigBDD.setVisible(true);
	}

	public void cerrarVentana() {
		txtUser.setText(null);
		txtPass.setText(null);
		txtHost.setText(null);
		txtPuerto.setText(null);
		txtBaseDeDatos.setText(null);
		this.frmConfigBDD.dispose();
	}

	public JTextField getTxtHost() {
		return txtHost;
	}

	public JTextField getTxtPuerto() {
		return txtPuerto;
	}

	public JTextField getTxtBDD() {
		return txtBaseDeDatos;
	}
}
