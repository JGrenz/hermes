package vistas.eventos;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;

import vistas.TextPrompt;

public class Main_Eventos {

	private static Main_Eventos INSTANCE;
	private JPanel frmMainEventos, panelBusqueda, panelABM, panelTabla;
	private JTable tablaEventos, tablaHistorialEventos;
	private DefaultTableModel modelEventos, modelHistorialEventos;
	private String[] nombreColumnas = { "Titulo", "Cliente", "DNI", "Telefono", "Email", "Tipo", "Descripcion",
			"Estado", "Fecha creación", "Fecha estimada resolución", "Fecha real resolución" };
	private String[] nombreColumnasHistorial = { "Tipo", "Descripción", "Fecha estimada de resolución", "Fecha real de resolución" };
	private JButton btnAgregar, btnFinalizar, btnRealizarBusqueda, btnModificarResponsable, btnNuevoMotivo, btnExtenderEvento;
	private JRadioButton radioAbierto, radioFinalizado, radioTodos;
	private ButtonGroup filtro;
	private JTextField txtUsuario, txtDNI, txtNombre, txtApellido, txtResponsable;
	private JComboBox<String> cbTipoEvento;
	private JLabel lblResponsable;

	public static Main_Eventos getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new Main_Eventos();
		}
		return INSTANCE;
	}

	public Main_Eventos() {

		frmMainEventos = new JPanel();
		GridBagLayout gbl_frmMainEventos = new GridBagLayout();
		gbl_frmMainEventos.columnWidths = new int[] { 102, 0 };
		gbl_frmMainEventos.rowHeights = new int[] { 10, 177, 59, 0 };
		gbl_frmMainEventos.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gbl_frmMainEventos.rowWeights = new double[] { 0.0, 0.0, 1.0, Double.MIN_VALUE };
		frmMainEventos.setLayout(gbl_frmMainEventos);

		filtro = new ButtonGroup();

		panelABM = new JPanel();
		GridBagConstraints gbc_panelABM = new GridBagConstraints();
		gbc_panelABM.fill = GridBagConstraints.BOTH;
		gbc_panelABM.insets = new Insets(0, 0, 5, 0);
		gbc_panelABM.gridx = 0;
		gbc_panelABM.gridy = 0;
		frmMainEventos.add(panelABM, gbc_panelABM);

		btnAgregar = new JButton("Crear");
		panelABM.add(btnAgregar);
		btnAgregar.setIcon(new ImageIcon(Main_Eventos.class.getResource("/imagenes/iconos/eventoAgregar.png")));

		btnFinalizar = new JButton("Finalizar");
		panelABM.add(btnFinalizar);
		btnFinalizar.setIcon(new ImageIcon(Main_Eventos.class.getResource("/imagenes/iconos/eventoFinalizar.png")));

		btnNuevoMotivo = new JButton("Nuevo motivo");
		btnNuevoMotivo.setIcon(new ImageIcon(Main_Eventos.class.getResource("/imagenes/iconos/eventoNuevoMotivo.png")));
		panelABM.add(btnNuevoMotivo);

		btnExtenderEvento = new JButton("Extender fecha");
		btnExtenderEvento.setIcon(new ImageIcon(Main_Eventos.class.getResource("/imagenes/iconos/eventoExtender.png")));
		panelABM.add(btnExtenderEvento);

		btnModificarResponsable = new JButton("Modificar responsable");
		panelABM.add(btnModificarResponsable);
		btnModificarResponsable
				.setIcon(new ImageIcon(Main_Eventos.class.getResource("/imagenes/iconos/eventoResponsable.png")));

		panelBusqueda = new JPanel();
		panelBusqueda.setBounds(10, 51, 972, 191);
		GridBagConstraints gbc_panelBusqueda = new GridBagConstraints();
		gbc_panelBusqueda.fill = GridBagConstraints.BOTH;
		gbc_panelBusqueda.insets = new Insets(0, 0, 5, 0);
		gbc_panelBusqueda.gridx = 0;
		gbc_panelBusqueda.gridy = 1;
		frmMainEventos.add(panelBusqueda, gbc_panelBusqueda);
		GridBagLayout gbl_panelBusqueda = new GridBagLayout();
		gbl_panelBusqueda.columnWidths = new int[] { 127, 30, 157, 125, 125, 30, 157, 189, 0 };
		gbl_panelBusqueda.rowHeights = new int[] { 30, 33, 30, 30, 0 };
		gbl_panelBusqueda.columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		gbl_panelBusqueda.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		panelBusqueda.setLayout(gbl_panelBusqueda);

		JLabel lblFiltrarPor = new JLabel("Filtrar por:");
		lblFiltrarPor.setPreferredSize(new Dimension(0, 30));
		lblFiltrarPor.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_lblFiltrarPor = new GridBagConstraints();
		gbc_lblFiltrarPor.fill = GridBagConstraints.BOTH;
		gbc_lblFiltrarPor.insets = new Insets(0, 0, 5, 5);
		gbc_lblFiltrarPor.gridx = 0;
		gbc_lblFiltrarPor.gridy = 0;
		panelBusqueda.add(lblFiltrarPor, gbc_lblFiltrarPor);

		JLabel lblBusquedaPor = new JLabel("Búsqueda por:");
		lblBusquedaPor.setPreferredSize(new Dimension(0, 30));
		lblBusquedaPor.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_lblBusquedaPor = new GridBagConstraints();
		gbc_lblBusquedaPor.fill = GridBagConstraints.BOTH;
		gbc_lblBusquedaPor.insets = new Insets(0, 0, 5, 5);
		gbc_lblBusquedaPor.gridx = 2;
		gbc_lblBusquedaPor.gridy = 0;
		panelBusqueda.add(lblBusquedaPor, gbc_lblBusquedaPor);

		radioTodos = new JRadioButton("Todos");
		radioTodos.setSelected(true);
		radioTodos.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_radioTodos = new GridBagConstraints();
		gbc_radioTodos.fill = GridBagConstraints.BOTH;
		gbc_radioTodos.insets = new Insets(0, 0, 5, 5);
		gbc_radioTodos.gridx = 0;
		gbc_radioTodos.gridy = 1;
		panelBusqueda.add(radioTodos, gbc_radioTodos);
		filtro.add(radioTodos);

		JLabel radioUsuario = new JLabel("Usuario");
		radioUsuario.setHorizontalAlignment(SwingConstants.CENTER);
		radioUsuario.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_radioUsuario = new GridBagConstraints();
		gbc_radioUsuario.fill = GridBagConstraints.BOTH;
		gbc_radioUsuario.insets = new Insets(0, 0, 5, 5);
		gbc_radioUsuario.gridx = 2;
		gbc_radioUsuario.gridy = 1;
		panelBusqueda.add(radioUsuario, gbc_radioUsuario);

		txtUsuario = new JTextField();
		txtUsuario.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_txtUsuario = new GridBagConstraints();
		gbc_txtUsuario.fill = GridBagConstraints.BOTH;
		gbc_txtUsuario.insets = new Insets(0, 0, 5, 5);
		gbc_txtUsuario.gridwidth = 2;
		gbc_txtUsuario.gridx = 3;
		gbc_txtUsuario.gridy = 1;
		panelBusqueda.add(txtUsuario, gbc_txtUsuario);
		txtUsuario.setColumns(10);
		TextPrompt phUsuario = new TextPrompt("Ingrese usuario...", txtUsuario);
		phUsuario.setText("Ingrese usuario...");
		phUsuario.changeAlpha(0.75f);
		phUsuario.changeStyle(Font.ITALIC);

		JLabel radioTipo = new JLabel("Tipo de evento");
		radioTipo.setHorizontalAlignment(SwingConstants.CENTER);
		radioTipo.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_radioTipo = new GridBagConstraints();
		gbc_radioTipo.fill = GridBagConstraints.BOTH;
		gbc_radioTipo.insets = new Insets(0, 0, 5, 5);
		gbc_radioTipo.gridx = 6;
		gbc_radioTipo.gridy = 1;
		panelBusqueda.add(radioTipo, gbc_radioTipo);

		cbTipoEvento = new JComboBox<String>();
		cbTipoEvento.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_cbTipoEvento = new GridBagConstraints();
		gbc_cbTipoEvento.fill = GridBagConstraints.BOTH;
		gbc_cbTipoEvento.insets = new Insets(0, 0, 5, 0);
		gbc_cbTipoEvento.gridx = 7;
		gbc_cbTipoEvento.gridy = 1;
		panelBusqueda.add(cbTipoEvento, gbc_cbTipoEvento);

		radioAbierto = new JRadioButton("Abierto");
		radioAbierto.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_radioAbierto = new GridBagConstraints();
		gbc_radioAbierto.fill = GridBagConstraints.BOTH;
		gbc_radioAbierto.insets = new Insets(0, 0, 5, 5);
		gbc_radioAbierto.gridx = 0;
		gbc_radioAbierto.gridy = 2;
		panelBusqueda.add(radioAbierto, gbc_radioAbierto);
		filtro.add(radioAbierto);

		JLabel radioDni = new JLabel("DNI");
		radioDni.setHorizontalAlignment(SwingConstants.CENTER);
		radioDni.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_radioDni = new GridBagConstraints();
		gbc_radioDni.fill = GridBagConstraints.BOTH;
		gbc_radioDni.insets = new Insets(0, 0, 5, 5);
		gbc_radioDni.gridx = 2;
		gbc_radioDni.gridy = 2;
		panelBusqueda.add(radioDni, gbc_radioDni);

		txtDNI = new JTextField();
		txtDNI.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_txtDNI = new GridBagConstraints();
		gbc_txtDNI.fill = GridBagConstraints.BOTH;
		gbc_txtDNI.insets = new Insets(0, 0, 5, 5);
		gbc_txtDNI.gridwidth = 2;
		gbc_txtDNI.gridx = 3;
		gbc_txtDNI.gridy = 2;
		panelBusqueda.add(txtDNI, gbc_txtDNI);
		txtDNI.setColumns(10);
		TextPrompt phDni = new TextPrompt("Ingrese dni...", txtDNI);
		phDni.setText("Ingrese dni...");
		phDni.changeAlpha(0.75f);
		phDni.changeStyle(Font.ITALIC);

		lblResponsable = new JLabel("Responsable");
		lblResponsable.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_lblResponsable = new GridBagConstraints();
		gbc_lblResponsable.fill = GridBagConstraints.BOTH;
		gbc_lblResponsable.insets = new Insets(0, 0, 5, 5);
		gbc_lblResponsable.gridx = 6;
		gbc_lblResponsable.gridy = 2;
		panelBusqueda.add(lblResponsable, gbc_lblResponsable);

		txtResponsable = new JTextField();
		GridBagConstraints gbc_txtResponsable = new GridBagConstraints();
		gbc_txtResponsable.insets = new Insets(0, 0, 5, 0);
		gbc_txtResponsable.fill = GridBagConstraints.BOTH;
		gbc_txtResponsable.gridx = 7;
		gbc_txtResponsable.gridy = 2;
		panelBusqueda.add(txtResponsable, gbc_txtResponsable);
		TextPrompt phResponsable = new TextPrompt("Ingrese responsable...", txtResponsable);
		phResponsable.setText("Ingrese responsable...");
		phResponsable.changeAlpha(0.75f);
		txtResponsable.setColumns(10);

		radioFinalizado = new JRadioButton("Finalizado");
		radioFinalizado.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_radioFinalizado = new GridBagConstraints();
		gbc_radioFinalizado.fill = GridBagConstraints.BOTH;
		gbc_radioFinalizado.insets = new Insets(0, 0, 0, 5);
		gbc_radioFinalizado.gridx = 0;
		gbc_radioFinalizado.gridy = 3;
		panelBusqueda.add(radioFinalizado, gbc_radioFinalizado);
		radioFinalizado.setSelected(true);
		filtro.add(radioFinalizado);

		JLabel radioNombreYApellido = new JLabel("Nombre y Apellido");
		radioNombreYApellido.setHorizontalAlignment(SwingConstants.CENTER);
		radioNombreYApellido.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_radioNombreYApellido = new GridBagConstraints();
		gbc_radioNombreYApellido.fill = GridBagConstraints.BOTH;
		gbc_radioNombreYApellido.insets = new Insets(0, 0, 0, 5);
		gbc_radioNombreYApellido.gridx = 2;
		gbc_radioNombreYApellido.gridy = 3;
		panelBusqueda.add(radioNombreYApellido, gbc_radioNombreYApellido);

		txtNombre = new JTextField();
		txtNombre.setPreferredSize(new Dimension(0, 30));
		GridBagConstraints gbc_txtNombre = new GridBagConstraints();
		gbc_txtNombre.gridwidth = 2;
		gbc_txtNombre.fill = GridBagConstraints.BOTH;
		gbc_txtNombre.insets = new Insets(0, 0, 0, 5);
		gbc_txtNombre.gridx = 3;
		gbc_txtNombre.gridy = 3;
		panelBusqueda.add(txtNombre, gbc_txtNombre);
		txtNombre.setColumns(10);
		TextPrompt phNombre = new TextPrompt("Ingrese nombre...", txtNombre);
		phNombre.setText("Ingrese nombre...");
		phNombre.changeAlpha(0.75f);
		phNombre.changeStyle(Font.ITALIC);

		btnRealizarBusqueda = new JButton("Buscar");
		btnRealizarBusqueda.setPreferredSize(new Dimension(0, 30));
		btnRealizarBusqueda.setBounds(498, 0, 123, 29);
		GridBagConstraints gbc_btnRealizarBusqueda = new GridBagConstraints();
		gbc_btnRealizarBusqueda.insets = new Insets(0, 0, 5, 5);
		gbc_btnRealizarBusqueda.gridx = 3;
		gbc_btnRealizarBusqueda.gridy = 0;
		gbc_btnRealizarBusqueda.fill = GridBagConstraints.BOTH;
		panelBusqueda.add(btnRealizarBusqueda, gbc_btnRealizarBusqueda);

		panelTabla = new JPanel();
		GridBagConstraints gbc_panelTabla = new GridBagConstraints();
		gbc_panelTabla.fill = GridBagConstraints.BOTH;
		gbc_panelTabla.gridx = 0;
		gbc_panelTabla.gridy = 2;
		frmMainEventos.add(panelTabla, gbc_panelTabla);
		GridBagLayout gbl_panelTabla = new GridBagLayout();
		gbl_panelTabla.columnWidths = new int[] { 1283, 0 };
		gbl_panelTabla.rowHeights = new int[] { 120, 30, 300 };
		gbl_panelTabla.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gbl_panelTabla.rowWeights = new double[] { 1.0, 0.0, 4.9E-324 };
		panelTabla.setLayout(gbl_panelTabla);

		JScrollPane spEventos = new JScrollPane();
		modelEventos = new DefaultTableModel(null, nombreColumnas) {
			private static final long serialVersionUID = 1L;
			
			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		GridBagConstraints gbc_spEventos = new GridBagConstraints();
		gbc_spEventos.insets = new Insets(0, 0, 5, 0);
		gbc_spEventos.fill = GridBagConstraints.BOTH;
		gbc_spEventos.gridx = 0;
		gbc_spEventos.gridy = 0;
		panelTabla.add(spEventos, gbc_spEventos);
		tablaEventos = new JTable(modelEventos);
		tablaEventos.getTableHeader().setReorderingAllowed(false);
		spEventos.setViewportView(tablaEventos);

		JLabel lblHistorialDeEvento = new JLabel("Historial de evento");
		GridBagConstraints gbc_lblHistorialDeEvento = new GridBagConstraints();
		gbc_lblHistorialDeEvento.fill = GridBagConstraints.VERTICAL;
		gbc_lblHistorialDeEvento.anchor = GridBagConstraints.WEST;
		gbc_lblHistorialDeEvento.insets = new Insets(0, 5, 5, 0);
		gbc_lblHistorialDeEvento.gridx = 0;
		gbc_lblHistorialDeEvento.gridy = 1;
		panelTabla.add(lblHistorialDeEvento, gbc_lblHistorialDeEvento);
		
		JScrollPane spHistorial = new JScrollPane();
		modelHistorialEventos = new DefaultTableModel(null, nombreColumnasHistorial) {
			private static final long serialVersionUID = 1L;
			
			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		GridBagConstraints gbc_spHistorial = new GridBagConstraints();
		gbc_spHistorial.fill = GridBagConstraints.BOTH;
		gbc_spHistorial.gridx = 0;
		gbc_spHistorial.gridy = 2;
		panelTabla.add(spHistorial, gbc_spHistorial);
		tablaHistorialEventos = new JTable(modelHistorialEventos);
		tablaHistorialEventos.getTableHeader().setReorderingAllowed(false);
		spHistorial.setViewportView(tablaHistorialEventos);

		this.frmMainEventos.setVisible(false);
	}

	public void mostrarVentana() {
		this.frmMainEventos.setVisible(true);
	}

	public JButton getBtnAgregar() {
		return btnAgregar;
	}

	public JButton getBtnFinalizar() {
		return btnFinalizar;
	}

	public JButton getBtnResponsable() {
		return btnModificarResponsable;
	}

	public JButton getBtnRealizarBusqueda() {
		return btnRealizarBusqueda;
	}

	public JButton getBtnNuevoMotivo() {
		return btnNuevoMotivo;
	}

	public JButton getBtnExtenderEvento() {
		return btnExtenderEvento;
	}

	public DefaultTableModel getModelEventos() {
		return modelEventos;
	}

	public JTable getTablaEventos() {
		return tablaEventos;
	}

	public String[] getNombreColumnas() {
		return nombreColumnas;
	}

	public void setNombreColumnas(String[] columnas) {
		this.nombreColumnas = columnas;
	}

	public JTextField getTxtUsuario() {
		return txtUsuario;
	}

	public JTextField getTxtDni() {
		return txtDNI;
	}

	public JTextField getTxtNombre() {
		return txtNombre;
	}

	public JTextField getTxtApellido() {
		return txtApellido;
	}

	public JTextField getTxtResponsable() {
		return txtResponsable;
	}

	public JComboBox<String> getCBTipoEvento() {
		return cbTipoEvento;
	}

	public JRadioButton getRadioTodos() {
		return radioTodos;
	}

	public JRadioButton getRadioAbierto() {
		return radioAbierto;
	}

	public JRadioButton getRadioFinalizado() {
		return radioFinalizado;
	}

	public JPanel getPanel() {
		return frmMainEventos;
	}

	public void setCamposResponsable(boolean bool) {
		lblResponsable.setVisible(bool);
		txtResponsable.setVisible(bool);
		btnModificarResponsable.setEnabled(bool);
	}
	
	public DefaultTableModel getModelHistorialEventos() {
		return modelHistorialEventos;
	}

	public JTable getTablaHistorialEventos() {
		return tablaHistorialEventos;
	}

	public String[] getNombreColumnasHistorial() {
		return nombreColumnasHistorial;
	}
}
