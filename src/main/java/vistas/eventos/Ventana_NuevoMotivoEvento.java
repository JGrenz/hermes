package vistas.eventos;

import java.awt.Dialog.ModalityType;
import java.awt.Toolkit;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SpinnerDateModel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import com.toedter.calendar.JDateChooser;
import com.toedter.calendar.JTextFieldDateEditor;

public class Ventana_NuevoMotivoEvento {

	private static Ventana_NuevoMotivoEvento INSTANCE;
	private JDialog frmAdicionarEvento;
	private JPanel contentPane;
	private JButton btnConfirmar;
	private JDateChooser txtFechaResolucion;
	private JTextField txtInteresado, txtTitulo;
	private JTextArea txtMotivo;
	private JComboBox<String> cbTipoEvento;
	private JSpinner hora, minuto;

	public static Ventana_NuevoMotivoEvento getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new Ventana_NuevoMotivoEvento();
		}
		return INSTANCE;
	}

	private Ventana_NuevoMotivoEvento() {

		frmAdicionarEvento = new JDialog();
		frmAdicionarEvento.setIconImage(Toolkit.getDefaultToolkit()
				.getImage(Ventana_NuevoMotivoEvento.class.getResource("/imagenes/hermesicono.png")));
		frmAdicionarEvento.setModalityType(ModalityType.APPLICATION_MODAL);
		frmAdicionarEvento.setResizable(false);
		frmAdicionarEvento.setTitle("Hermes - Nuevo motivo de evento");
		frmAdicionarEvento.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmAdicionarEvento.setBounds(100, 100, 340, 472);
		frmAdicionarEvento.setLocationRelativeTo(null);

		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		frmAdicionarEvento.setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 334, 443);
		contentPane.add(panel);
		panel.setLayout(null);

		btnConfirmar = new JButton("Confirmar");
		btnConfirmar.setBounds(198, 403, 126, 30);
		panel.add(btnConfirmar);

		JLabel lblTipo = new JLabel("Tipo de evento");
		lblTipo.setHorizontalAlignment(SwingConstants.CENTER);
		lblTipo.setBounds(10, 10, 95, 30);
		panel.add(lblTipo);

		JLabel lblInteresado = new JLabel("Interesado");
		lblInteresado.setHorizontalAlignment(SwingConstants.CENTER);
		lblInteresado.setBounds(10, 45, 95, 30);
		panel.add(lblInteresado);

		JLabel lblTitulo = new JLabel("Titulo");
		lblTitulo.setHorizontalAlignment(SwingConstants.CENTER);
		lblTitulo.setBounds(10, 80, 95, 30);
		panel.add(lblTitulo);

		JLabel lblMotivo = new JLabel("Motivo");
		lblMotivo.setHorizontalAlignment(SwingConstants.CENTER);
		lblMotivo.setBounds(10, 115, 95, 30);
		panel.add(lblMotivo);

		cbTipoEvento = new JComboBox<String>();
		cbTipoEvento.setBounds(115, 10, 209, 30);
		cbTipoEvento.addItem("CONSULTA");
		cbTipoEvento.addItem("RECLAMO");
		cbTipoEvento.addItem("OTRO");
		cbTipoEvento.setSelectedIndex(-1);
		panel.add(cbTipoEvento);

		txtFechaResolucion = new JDateChooser();
		txtFechaResolucion.setMinSelectableDate(new Date());
		JTextFieldDateEditor editor = (JTextFieldDateEditor) txtFechaResolucion.getDateEditor();
		editor.setEditable(false);
		txtFechaResolucion.setBounds(115, 306, 209, 30);
		panel.add(txtFechaResolucion);

		txtInteresado = new JTextField();
		txtInteresado.setEditable(false);
		txtInteresado.setBounds(115, 45, 209, 30);
		panel.add(txtInteresado);

		txtTitulo = new JTextField();
		txtTitulo.setBounds(115, 80, 209, 30);
		panel.add(txtTitulo);

		txtMotivo = new JTextArea();
		txtMotivo.setLineWrap(true);
		txtMotivo.setBounds(10, 150, 314, 145);
		panel.add(txtMotivo);

		JLabel lblFechaResolucion = new JLabel("Fecha resolucion");
		lblFechaResolucion.setHorizontalAlignment(SwingConstants.CENTER);
		lblFechaResolucion.setBounds(10, 306, 95, 30);
		panel.add(lblFechaResolucion);

		hora = new JSpinner(new SpinnerDateModel());
		JSpinner.DateEditor horaSEditor = new JSpinner.DateEditor(hora, "HH");
		hora.setEditor(horaSEditor);
		hora.setBounds(115, 341, 100, 30);
		panel.add(hora);

		minuto = new JSpinner(new SpinnerDateModel());
		JSpinner.DateEditor minutoSEditor = new JSpinner.DateEditor(minuto, "mm");
		minuto.setEditor(minutoSEditor);
		minuto.setBounds(225, 341, 100, 30);
		panel.add(minuto);

		JLabel lblHoraResolucin = new JLabel("Hora resolución");
		lblHoraResolucin.setHorizontalAlignment(SwingConstants.CENTER);
		lblHoraResolucin.setBounds(10, 341, 95, 30);
		panel.add(lblHoraResolucin);

		this.frmAdicionarEvento.setVisible(false);
	}

	public void mostrarVentana() {
		this.frmAdicionarEvento.setVisible(true);
	}

	public JTextField getInteresado() {
		return txtInteresado;
	}

	public JButton getBtnConfirmar() {
		return btnConfirmar;
	}

	public void cerrar() {
		this.txtInteresado.setText(null);
		this.txtTitulo.setText(null);
		this.txtMotivo.setText(null);
		this.txtFechaResolucion.setDate(null);
		this.cbTipoEvento.setSelectedIndex(-1);
		this.frmAdicionarEvento.dispose();
	}

	public boolean camposVacios() {
		if (txtTitulo.getText().isEmpty() || txtFechaResolucion.getDate() == null || txtMotivo.getText().isEmpty()
				|| cbTipoEvento.getSelectedIndex() == -1) {
			JOptionPane.showMessageDialog(null, "No pueden quedar campos vacíos.", "¡Advertencia!", JOptionPane.WARNING_MESSAGE);
			return false;
		} else {
			return true;
		}
	}

//	private boolean camposCorrectos() {
//		ValidacionCampos validador = new ValidacionCampos();
//		if (!validador.esPalabra(txtTitulo.getText()) || !validador.esPalabra(txtMotivo.getText())) {
//			return false;
//		} else {
//			return true;
//		}
//	}

	public boolean verificarCampo() {
		if (camposVacios() /*&& camposCorrectos()*/)
			return true;
		else
			return false;
	}

	public String getTextTitulo() {
		return txtTitulo.getText();
	}

	public String getTextMotivo() {
		return txtMotivo.getText();
	}
	
	public JTextField getTxtInteresado() {
		return txtInteresado;
	}

	public LocalDateTime getTxtFechaResolucionEstimada() {
		Date date = txtFechaResolucion.getDate();
		return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
	}

	public String getTipoEvento() {
		return cbTipoEvento.getSelectedItem().toString();
	}

	public JComboBox<String> getCBTipo() {
		return cbTipoEvento;
	}

	public LocalTime getTxtHoraEvento() {
		String horaEvento = new SimpleDateFormat("HH").format(hora.getValue());
		String minutoEvento = new SimpleDateFormat("mm").format(minuto.getValue());
		LocalTime horarioEvento = LocalTime.parse(horaEvento + ":" + minutoEvento);
		return horarioEvento;
	}

    public JTextField getTxtTitulo() {
		return txtTitulo;
    }

	public JTextArea getTxtMotivo() {
		return txtMotivo;
	}
}
