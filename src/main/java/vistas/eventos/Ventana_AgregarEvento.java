package vistas.eventos;

import java.awt.Dialog.ModalityType;
import java.awt.Toolkit;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SpinnerDateModel;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import com.toedter.calendar.JDateChooser;
import com.toedter.calendar.JTextFieldDateEditor;

import vistas.ValidacionCampos;

public class Ventana_AgregarEvento {

	private static Ventana_AgregarEvento INSTANCE;
	private JDialog frmAgregarEvento;
	private JPanel contentPane, panelClientes;
	private JButton btnConfirmar, btnBuscarCliente, btnSeleccionarCliente, btnAbrirPanelCliente;
	private String[] columnasDeClientes = { "Nombre", "Apellido", "DNI", "Usuario" };
	private JDateChooser txtFechaResolucion;
	private DefaultTableModel modelClientes;
	private JTable tablaClientes;
	private JTextField txtNombre, txtApellido, txtDNI, txtUsuario;
	private JTextField txtInteresado, txtTelefono, txtEmail, txtTitulo;
	private JTextArea textMotivo;
	private JCheckBox checkCliente;
	private JComboBox<String> cbTipoEvento;
	private JSpinner hora, minuto;

	public static Ventana_AgregarEvento getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new Ventana_AgregarEvento();
		}
		return INSTANCE;
	}

	private Ventana_AgregarEvento() {

		frmAgregarEvento = new JDialog();
		frmAgregarEvento.setIconImage(Toolkit.getDefaultToolkit()
				.getImage(Ventana_AgregarEvento.class.getResource("/imagenes/hermesicono.png")));
		frmAgregarEvento.setModalityType(ModalityType.APPLICATION_MODAL);
		frmAgregarEvento.setResizable(false);
		frmAgregarEvento.setTitle("Hermes - Crear evento");
		frmAgregarEvento.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmAgregarEvento.setBounds(100, 100, 476, 586);
		frmAgregarEvento.setLocationRelativeTo(null);

		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		frmAgregarEvento.setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 470, 557);
		contentPane.add(panel);
		panel.setLayout(null);

		/*************************************
		 * PANEL DE BUSQUEDA DE CLIENTES
		 *************************************************/

		checkCliente = new JCheckBox("Asignar cliente?");
		checkCliente.setHorizontalAlignment(SwingConstants.CENTER);
		checkCliente.setBounds(330, 45, 130, 30);
		panel.add(checkCliente);

		panelClientes = new JPanel();
		panelClientes.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		panelClientes.setBounds(470, 115, 470, 442);
		panel.add(panelClientes);
		panelClientes.setLayout(null);

		JScrollPane spClientes = new JScrollPane();
		spClientes.setBounds(10, 182, 450, 192);
		panelClientes.add(spClientes);
		modelClientes = new DefaultTableModel(null, columnasDeClientes) {
			private static final long serialVersionUID = 1L;

			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		tablaClientes = new JTable(modelClientes);
		tablaClientes.getTableHeader().setReorderingAllowed(false);
		spClientes.setViewportView(tablaClientes);

		JLabel lblNombre = new JLabel("Nombre:");
		lblNombre.setHorizontalAlignment(SwingConstants.CENTER);
		lblNombre.setBounds(10, 15, 70, 30);
		panelClientes.add(lblNombre);

		JLabel lblApellido = new JLabel("Apellido:");
		lblApellido.setHorizontalAlignment(SwingConstants.CENTER);
		lblApellido.setBounds(10, 50, 70, 30);
		panelClientes.add(lblApellido);

		JLabel lblDNI = new JLabel("DNI:");
		lblDNI.setHorizontalAlignment(SwingConstants.CENTER);
		lblDNI.setBounds(10, 120, 70, 30);
		panelClientes.add(lblDNI);

		JLabel lblUsuario = new JLabel("Usuario:");
		lblUsuario.setHorizontalAlignment(SwingConstants.CENTER);
		lblUsuario.setBounds(10, 85, 70, 30);
		panelClientes.add(lblUsuario);

		txtNombre = new JTextField();
		txtNombre.setBounds(90, 15, 161, 30);
		panelClientes.add(txtNombre);
		txtNombre.setColumns(10);

		txtApellido = new JTextField();
		txtApellido.setBounds(90, 50, 161, 30);
		panelClientes.add(txtApellido);
		txtApellido.setColumns(10);

		txtDNI = new JTextField();
		txtDNI.setBounds(90, 120, 161, 30);
		panelClientes.add(txtDNI);
		txtDNI.setColumns(10);

		txtUsuario = new JTextField();
		txtUsuario.setBounds(90, 85, 161, 30);
		panelClientes.add(txtUsuario);
		txtUsuario.setColumns(10);

		btnBuscarCliente = new JButton("Buscar cliente");
		btnBuscarCliente.setBounds(307, 15, 153, 30);
		panelClientes.add(btnBuscarCliente);

		btnSeleccionarCliente = new JButton("Seleccionar cliente");
		btnSeleccionarCliente.setBounds(307, 385, 153, 30);
		panelClientes.add(btnSeleccionarCliente);

		btnConfirmar = new JButton("Confirmar");
		btnConfirmar.setBounds(334, 516, 126, 30);
		panel.add(btnConfirmar);

		JLabel lblTipo = new JLabel("Tipo de evento");
		lblTipo.setHorizontalAlignment(SwingConstants.CENTER);
		lblTipo.setBounds(10, 10, 95, 30);
		panel.add(lblTipo);

		JLabel lblInteresado = new JLabel("Interesado");
		lblInteresado.setHorizontalAlignment(SwingConstants.CENTER);
		lblInteresado.setBounds(10, 45, 95, 30);
		panel.add(lblInteresado);

		JLabel lblTelefono = new JLabel("Telefono");
		lblTelefono.setHorizontalAlignment(SwingConstants.CENTER);
		lblTelefono.setBounds(10, 115, 95, 30);
		panel.add(lblTelefono);

		JLabel lblEmail = new JLabel("Email");
		lblEmail.setHorizontalAlignment(SwingConstants.CENTER);
		lblEmail.setBounds(10, 80, 95, 30);
		panel.add(lblEmail);

		JLabel lblTitulo = new JLabel("Titulo");
		lblTitulo.setHorizontalAlignment(SwingConstants.CENTER);
		lblTitulo.setBounds(10, 150, 95, 30);
		panel.add(lblTitulo);

		JLabel lblMotivo = new JLabel("Motivo");
		lblMotivo.setHorizontalAlignment(SwingConstants.CENTER);
		lblMotivo.setBounds(10, 185, 95, 30);
		panel.add(lblMotivo);

		cbTipoEvento = new JComboBox<String>();
		cbTipoEvento.setBounds(115, 10, 209, 30);
		cbTipoEvento.addItem("CONSULTA");
		cbTipoEvento.addItem("RECLAMO");
		cbTipoEvento.addItem("OTRO");
		cbTipoEvento.setSelectedIndex(-1);
		panel.add(cbTipoEvento);

		txtFechaResolucion = new JDateChooser();
		txtFechaResolucion.setMinSelectableDate(new Date());
		JTextFieldDateEditor editor = (JTextFieldDateEditor) txtFechaResolucion.getDateEditor();
		editor.setEditable(false);
		txtFechaResolucion.setBounds(115, 335, 209, 30);
		panel.add(txtFechaResolucion);

		txtInteresado = new JTextField();
		txtInteresado.setBounds(115, 45, 209, 30);
		panel.add(txtInteresado);

		txtTelefono = new JTextField();
		txtTelefono.setBounds(115, 115, 209, 30);
		panel.add(txtTelefono);

		txtEmail = new JTextField();
		txtEmail.setBounds(115, 80, 209, 30);
		panel.add(txtEmail);

		txtTitulo = new JTextField();
		txtTitulo.setBounds(115, 150, 209, 30);
		panel.add(txtTitulo);

		textMotivo = new JTextArea();
		textMotivo.setLineWrap(true);
		textMotivo.setBounds(115, 185, 209, 145);
		panel.add(textMotivo);

		JLabel lblFechaResolucion = new JLabel("Fecha resolucion");
		lblFechaResolucion.setHorizontalAlignment(SwingConstants.CENTER);
		lblFechaResolucion.setBounds(10, 335, 95, 30);
		panel.add(lblFechaResolucion);
		
		hora = new JSpinner(new SpinnerDateModel());
		JSpinner.DateEditor horaSEditor = new JSpinner.DateEditor(hora, "HH");
		hora.setEditor(horaSEditor);
		hora.setBounds(115, 370, 100, 30);
		panel.add(hora);

		minuto = new JSpinner(new SpinnerDateModel());
		JSpinner.DateEditor minutoSEditor = new JSpinner.DateEditor(minuto, "mm");
		minuto.setEditor(minutoSEditor);
		minuto.setBounds(225, 370, 100, 30);
		panel.add(minuto);
		
		btnAbrirPanelCliente = new JButton("Buscar cliente");
		btnAbrirPanelCliente.setBounds(330, 82, 130, 30);
		btnAbrirPanelCliente.setEnabled(false);
		panel.add(btnAbrirPanelCliente);
		
		JLabel lblHoraResolucin = new JLabel("Hora resolución");
		lblHoraResolucin.setHorizontalAlignment(SwingConstants.CENTER);
		lblHoraResolucin.setBounds(10, 370, 95, 30);
		panel.add(lblHoraResolucin);
		
		this.frmAgregarEvento.setVisible(false);
	}

	public void mostrarVentana() {
		this.frmAgregarEvento.setVisible(true);
	}

	public JTextField getInteresado() {
		return txtInteresado;
	}

	public JButton getBtnConfirmar() {
		return btnConfirmar;
	}

	public void cerrar() {
		this.txtInteresado.setText(null);
		this.txtTitulo.setText(null);
		this.txtTelefono.setText(null);
		this.txtEmail.setText(null);
		this.textMotivo.setText(null);
		this.txtFechaResolucion.setDate(null);
		this.cbTipoEvento.setSelectedIndex(-1);
		this.frmAgregarEvento.dispose();
	}

	private boolean camposVacios() {
		if (!txtInteresado.getText().equals("") && !txtTitulo.getText().equals("") && txtFechaResolucion.getDate() != null
			&& (!txtEmail.getText().equals("") || !txtTelefono.getText().equals(""))
			&& !textMotivo.getText().equals("") && cbTipoEvento.getSelectedIndex() != -1) {
			return false;
		}
		JOptionPane.showMessageDialog(null, "Los campos interesado, titulo, fecha de resolucion, motivo y tipo de evento no pueden quedar vacíos.\n"
				+ "Además se debe completar al menos un campo de contacto(email o telefono).");
		return true;
	}
	
	private boolean camposCorrectos() {
		ValidacionCampos validador = new ValidacionCampos();
		if(txtEmail.getText().equals("")) {
			if(!validador.esTelefono(txtTelefono.getText())){
				return false;
			}
		} else {
			if(!validador.esTelefono(txtTelefono.getText()) || !validador.esMail(txtEmail.getText())) {
				return false;			
			}
		}
		return true;
	}
	
	public boolean verificarCampo() {
		if (!camposVacios() && camposCorrectos())
			return true;
		else
			return false;
	}

	public JTable getTablaClientes() {
		return tablaClientes;
	}

	public JCheckBox getCheckClientes() {
		return checkCliente;
	}

	public JPanel getPanelClientes() {
		return panelClientes;
	}
	
	public void habilitarPanelClientes(boolean bool) {
		panelClientes.setEnabled(bool);
	}

	public DefaultTableModel getModelClientes() {
		return modelClientes;
	}

	public String[] getColumnasDeClientes() {
		return columnasDeClientes;
	}

	public JTextField getTxtNombre() {
		return txtNombre;
	}

	public JTextField getTxtApellido() {
		return txtApellido;
	}

	public JTextField getTxtDNI() {
		return txtDNI;
	}

	public JTextField getTxtUsuario() {
		return txtUsuario;
	}

	public JButton getBtnBuscarCliente() {
		return btnBuscarCliente;
	}

	public JButton getBtnSeleccionarCliente() {
		return btnSeleccionarCliente;
	}

	public String getTextTitulo() {
		return txtTitulo.getText();
	}

	public String getTextMotivo() {
		return textMotivo.getText();
	}

	public LocalDate getTxtFechaResolucion() {
		Date date = txtFechaResolucion.getDate();
		return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
	}

	public String getTipoEvento() {
		return cbTipoEvento.getSelectedItem().toString();
	}
	
	public JComboBox<String> getCBTipo(){
		return cbTipoEvento;
	}

	public JTextField getTxtTelefono() {
		return txtTelefono;
	}

	public JTextField getTxtEmail() {
		return txtEmail;
	}
	
	public JButton getBtnPanelCliente() {
		return btnAbrirPanelCliente;
	}
	
	public LocalTime getTxtHoraEvento() {
		String horaEvento = new SimpleDateFormat("HH").format(hora.getValue());
		String minutoEvento = new SimpleDateFormat("mm").format(minuto.getValue());
		LocalTime horarioEvento = LocalTime.parse(horaEvento + ":" + minutoEvento);
		return horarioEvento;
	}
}
