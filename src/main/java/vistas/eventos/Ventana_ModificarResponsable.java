package vistas.eventos;

import java.awt.Toolkit;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

public class Ventana_ModificarResponsable extends JDialog {

	private static final long serialVersionUID = 1L;
	private static Ventana_ModificarResponsable INSTANCE;
	private JDialog frmModificarResponsable;
	private JPanel panel, panelEmpleados;
	private JButton btnConfirmar, btnBuscarEmpleado, btnSeleccionarEmpleado;
	private String[] columnasDeEmpleados = { "Nombre", "Apellido", "DNI", "Usuario" };
	private DefaultTableModel modelEmpleados;
	private JTable tablaEmpleados;
	private JTextField txtNombre, txtApellido, txtDNI, txtUsuario;
	private JTextField txtNuevo, txtOriginal;
	private JTextArea txtMotivo;

	public static Ventana_ModificarResponsable getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new Ventana_ModificarResponsable();
		}
		return INSTANCE;
	}

	private Ventana_ModificarResponsable() {

		frmModificarResponsable = new JDialog();
		frmModificarResponsable.setIconImage(Toolkit.getDefaultToolkit()
				.getImage(Ventana_ModificarResponsable.class.getResource("/imagenes/hermesicono.png")));
		frmModificarResponsable.setModalityType(ModalityType.APPLICATION_MODAL);
		frmModificarResponsable.setResizable(false);
		frmModificarResponsable.setTitle("Hermes - Modificar responsable de evento");
		frmModificarResponsable.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmModificarResponsable.setBounds(100, 100, 468, 683);
		frmModificarResponsable.setLocationRelativeTo(null);

		panel = new JPanel();
		panel.setBorder(new EmptyBorder(5, 5, 5, 5));
		frmModificarResponsable.setContentPane(panel);
		panel.setLayout(null);

		modelEmpleados = new DefaultTableModel(null, columnasDeEmpleados) {
			private static final long serialVersionUID = 1L;

			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};

		panelEmpleados = new JPanel();
		panelEmpleados.setBounds(10, 86, 442, 378);
		panel.add(panelEmpleados);
		panelEmpleados.setLayout(null);

		JScrollPane spEmpleados = new JScrollPane();
		spEmpleados.setBounds(10, 126, 422, 199);
		panelEmpleados.add(spEmpleados);
		tablaEmpleados = new JTable(modelEmpleados);
		tablaEmpleados.getTableHeader().setReorderingAllowed(false);
		spEmpleados.setViewportView(tablaEmpleados);

		JLabel lblNombre = new JLabel("Nombre:");
		lblNombre.setHorizontalAlignment(SwingConstants.CENTER);
		lblNombre.setBounds(10, 15, 70, 30);
		panelEmpleados.add(lblNombre);

		JLabel lblApellido = new JLabel("Apellido:");
		lblApellido.setHorizontalAlignment(SwingConstants.CENTER);
		lblApellido.setBounds(232, 15, 70, 30);
		panelEmpleados.add(lblApellido);

		JLabel lblDNI = new JLabel("DNI:");
		lblDNI.setHorizontalAlignment(SwingConstants.CENTER);
		lblDNI.setBounds(232, 50, 70, 30);
		panelEmpleados.add(lblDNI);

		JLabel lblUsuario = new JLabel("Usuario:");
		lblUsuario.setHorizontalAlignment(SwingConstants.CENTER);
		lblUsuario.setBounds(10, 50, 70, 30);
		panelEmpleados.add(lblUsuario);

		txtNombre = new JTextField();
		txtNombre.setBounds(90, 15, 120, 30);
		panelEmpleados.add(txtNombre);
		txtNombre.setColumns(10);

		txtApellido = new JTextField();
		txtApellido.setBounds(312, 15, 120, 30);
		panelEmpleados.add(txtApellido);
		txtApellido.setColumns(10);

		txtDNI = new JTextField();
		txtDNI.setBounds(312, 50, 120, 30);
		panelEmpleados.add(txtDNI);
		txtDNI.setColumns(10);

		txtUsuario = new JTextField();
		txtUsuario.setBounds(90, 50, 120, 30);
		panelEmpleados.add(txtUsuario);
		txtUsuario.setColumns(10);

		btnBuscarEmpleado = new JButton("Buscar empleado");
		btnBuscarEmpleado.setBounds(279, 85, 153, 30);
		panelEmpleados.add(btnBuscarEmpleado);

		btnSeleccionarEmpleado = new JButton("Seleccionar empleado");
		btnSeleccionarEmpleado.setBounds(279, 336, 153, 30);
		panelEmpleados.add(btnSeleccionarEmpleado);

		JLabel lblOriginal = new JLabel("Responsable original");
		lblOriginal.setHorizontalAlignment(SwingConstants.CENTER);
		lblOriginal.setBounds(10, 10, 163, 30);
		panel.add(lblOriginal);

		txtOriginal = new JTextField();
		txtOriginal.setEditable(false);
		txtOriginal.setBounds(183, 10, 188, 30);
		panel.add(txtOriginal);

		JLabel lblNuevo = new JLabel("Nuevo responsable");
		lblNuevo.setHorizontalAlignment(SwingConstants.CENTER);
		lblNuevo.setBounds(10, 45, 163, 30);
		panel.add(lblNuevo);

		txtNuevo = new JTextField();
		txtNuevo.setEditable(false);
		txtNuevo.setBounds(183, 45, 188, 30);
		panel.add(txtNuevo);

		JLabel lblMotivo = new JLabel("Motivo del cambio");
		lblMotivo.setHorizontalAlignment(SwingConstants.CENTER);
		lblMotivo.setBounds(10, 470, 163, 30);
		panel.add(lblMotivo);

		txtMotivo = new JTextArea();
		txtMotivo.setLineWrap(true);
		txtMotivo.setBounds(10, 505, 442, 97);
		panel.add(txtMotivo);

		btnConfirmar = new JButton("Confirmar");
		btnConfirmar.setBounds(326, 613, 126, 30);
		panel.add(btnConfirmar);

		this.frmModificarResponsable.setVisible(false);
	}

	public void mostrarVentana() {
		this.frmModificarResponsable.setVisible(true);
	}

	public JTextField getResponsableOriginal() {
		return txtOriginal;
	}

	public JTextField getResponsableNuevo() {
		return txtNuevo;
	}
	
	public JTextArea getMotivo() {
		return txtMotivo;
	}

	public JButton getBtnConfirmar() {
		return btnConfirmar;
	}

	public void cerrar() {
		this.txtNuevo.setText(null);
		this.txtMotivo.setText(null);
		this.frmModificarResponsable.dispose();
	}

	public boolean verificarCampo() {
		if (txtMotivo.getText().equals("")) {
			JOptionPane.showMessageDialog(null, "Tiene que especificar un motivo para el cambio de responsable.", "¡Advertencia!", JOptionPane.ERROR_MESSAGE);
			return false;
		} else {
			return true;
		}
	}

	public JTable getTablaEmpleados() {
		return tablaEmpleados;
	}

	public DefaultTableModel getModelEmpleados() {
		return modelEmpleados;
	}

	public String[] getColumnasDeEmpleados() {
		return columnasDeEmpleados;
	}

	public JTextField getTxtNombre() {
		return txtNombre;
	}

	public JTextField getTxtApellido() {
		return txtApellido;
	}

	public JTextField getTxtDNI() {
		return txtDNI;
	}

	public JTextField getTxtUsuario() {
		return txtUsuario;
	}

	public JButton getBtnBuscarEmpleado() {
		return btnBuscarEmpleado;
	}

	public JButton getBtnSeleccionarEmpleado() {
		return btnSeleccionarEmpleado;
	}
}
