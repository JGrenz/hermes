package vistas.eventos;

import java.awt.Dialog.ModalityType;
import java.awt.Toolkit;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SpinnerDateModel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import com.toedter.calendar.JDateChooser;
import com.toedter.calendar.JTextFieldDateEditor;

public class Ventana_ExtenderEvento {

	private static Ventana_ExtenderEvento INSTANCE;
	private JDialog frmExtenderEvento;
	private JPanel contentPane;
	private JButton btnConfirmar;
	private JDateChooser txtFechaResolucion;
	private JTextField txtInteresado, txtTitulo;
	private JTextArea txtMotivo;
	private JComboBox<String> cbTipoEvento;
	private JSpinner hora, minuto;

	public static Ventana_ExtenderEvento getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new Ventana_ExtenderEvento();
		}
		return INSTANCE;
	}

	private Ventana_ExtenderEvento() {

		frmExtenderEvento = new JDialog();
		frmExtenderEvento.setIconImage(Toolkit.getDefaultToolkit()
				.getImage(Ventana_ExtenderEvento.class.getResource("/imagenes/hermesicono.png")));
		frmExtenderEvento.setModalityType(ModalityType.APPLICATION_MODAL);
		frmExtenderEvento.setResizable(false);
		frmExtenderEvento.setTitle("Hermes - Extender fecha");
		frmExtenderEvento.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmExtenderEvento.setBounds(100, 100, 340, 472);
		frmExtenderEvento.setLocationRelativeTo(null);

		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		frmExtenderEvento.setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 334, 443);
		contentPane.add(panel);
		panel.setLayout(null);

		btnConfirmar = new JButton("Confirmar");
		btnConfirmar.setBounds(198, 403, 126, 30);
		panel.add(btnConfirmar);

		JLabel lblTipo = new JLabel("Tipo de evento");
		lblTipo.setEnabled(false);
		lblTipo.setHorizontalAlignment(SwingConstants.CENTER);
		lblTipo.setBounds(10, 10, 95, 30);
		panel.add(lblTipo);

		JLabel lblInteresado = new JLabel("Interesado");
		lblInteresado.setEnabled(false);
		lblInteresado.setHorizontalAlignment(SwingConstants.CENTER);
		lblInteresado.setBounds(10, 45, 95, 30);
		panel.add(lblInteresado);

		JLabel lblTitulo = new JLabel("Titulo");
		lblTitulo.setEnabled(false);
		lblTitulo.setHorizontalAlignment(SwingConstants.CENTER);
		lblTitulo.setBounds(10, 80, 95, 30);
		panel.add(lblTitulo);

		JLabel lblMotivo = new JLabel("Motivo");
		lblMotivo.setEnabled(false);
		lblMotivo.setHorizontalAlignment(SwingConstants.CENTER);
		lblMotivo.setBounds(10, 115, 95, 30);
		panel.add(lblMotivo);

		cbTipoEvento = new JComboBox<String>();
		cbTipoEvento.setEnabled(false);
		cbTipoEvento.setBounds(115, 10, 209, 30);
		cbTipoEvento.addItem("CONSULTA");
		cbTipoEvento.addItem("RECLAMO");
		cbTipoEvento.addItem("OTRO");
		cbTipoEvento.setSelectedIndex(-1);
		panel.add(cbTipoEvento);

		txtFechaResolucion = new JDateChooser();
		txtFechaResolucion.setMinSelectableDate(new Date());
		JTextFieldDateEditor editor = (JTextFieldDateEditor) txtFechaResolucion.getDateEditor();
		editor.setEditable(false);
		txtFechaResolucion.setBounds(115, 306, 209, 30);
		panel.add(txtFechaResolucion);

		txtInteresado = new JTextField();
		txtInteresado.setEnabled(false);
		txtInteresado.setEditable(false);
		txtInteresado.setBounds(115, 45, 209, 30);
		panel.add(txtInteresado);

		txtTitulo = new JTextField();
		txtTitulo.setEnabled(false);
		txtTitulo.setBounds(115, 80, 209, 30);
		panel.add(txtTitulo);

		txtMotivo = new JTextArea();
		txtMotivo.setEnabled(false);
		txtMotivo.setLineWrap(true);
		txtMotivo.setBounds(10, 150, 314, 145);
		panel.add(txtMotivo);

		JLabel lblFechaResolucion = new JLabel("Fecha resolucion");
		lblFechaResolucion.setHorizontalAlignment(SwingConstants.CENTER);
		lblFechaResolucion.setBounds(10, 306, 95, 30);
		panel.add(lblFechaResolucion);

		hora = new JSpinner(new SpinnerDateModel());
		JSpinner.DateEditor horaSEditor = new JSpinner.DateEditor(hora, "HH");
		hora.setEditor(horaSEditor);
		hora.setBounds(115, 341, 100, 30);
		panel.add(hora);

		minuto = new JSpinner(new SpinnerDateModel());
		JSpinner.DateEditor minutoSEditor = new JSpinner.DateEditor(minuto, "mm");
		minuto.setEditor(minutoSEditor);
		minuto.setBounds(225, 341, 100, 30);
		panel.add(minuto);

		JLabel lblHoraResolucin = new JLabel("Hora resolución");
		lblHoraResolucin.setHorizontalAlignment(SwingConstants.CENTER);
		lblHoraResolucin.setBounds(10, 341, 95, 30);
		panel.add(lblHoraResolucin);

		this.frmExtenderEvento.setVisible(false);
	}

	public void mostrarVentana() {
		this.frmExtenderEvento.setVisible(true);
	}

	public JButton getBtnConfirmar() {
		return btnConfirmar;
	}

	public void cerrar() {
		this.frmExtenderEvento.dispose();
	}

	public boolean camposVacios() {
		if (txtFechaResolucion.getDate() == null) {
			JOptionPane.showMessageDialog(null, "No pueden quedar campos vacíos.");
			return true;
		} else
			return false;
	}

	public LocalDate getTxtFechaSalida() {
		Date date = txtFechaResolucion.getDate();
		return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
	}

	public LocalTime getTxtHoraEvento() {
		String horaEvento = new SimpleDateFormat("HH").format(hora.getValue());
		String minutoEvento = new SimpleDateFormat("mm").format(minuto.getValue());
		LocalTime horarioEvento = LocalTime.parse(horaEvento + ":" + minutoEvento);
		return horarioEvento;
	}

	public JComboBox<String> getCBTipo() {
		return cbTipoEvento;
	}
	
    public JTextField getTxtTitulo() {
		return txtTitulo;
    }

	public JTextArea getTxtMotivo() {
		return txtMotivo;
	}
	
	public JTextField getTxtInteresado() {
		return txtInteresado;
	}
	
	public JDateChooser getFechaChooser() {
		return txtFechaResolucion;
	}
	
	public JSpinner getSpinnerHora() {
		return hora;
	}
	
	public JSpinner getSpinnerMinuto() {
		return minuto;
	}
}
