package vistas;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Paint;
import java.awt.Toolkit;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

public class Main_Cliente {

	private static Main_Cliente INSTANCE;
	private JFrame frmMainCliente;
	private JPanel panelOeste, panelCentro;
	private JButton btnDesconectar;
	private JButton btnPerfilDeUsuario;

	public static Main_Cliente getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new Main_Cliente();
		}
		return INSTANCE;
	}

	@SuppressWarnings("serial")
	public Main_Cliente() {

		frmMainCliente = new JFrame();
		frmMainCliente.setIconImage(
				Toolkit.getDefaultToolkit().getImage(Main_Cliente.class.getResource("/imagenes/hermesicono.png")));
		frmMainCliente.setTitle("Hermes - Cliente");
		frmMainCliente.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmMainCliente.setExtendedState(frmMainCliente.getExtendedState() | JFrame.MAXIMIZED_BOTH);
		frmMainCliente.getContentPane().setLayout(new BorderLayout(0, 0));

		panelOeste = new JPanel() {
			protected void paintComponent(Graphics g) {
				if (g instanceof Graphics2D) {
					final int R = 255;
					final int G = 255;
					final int B = 153;
					Paint p = new GradientPaint(0.0f, 0.0f, new Color(R, G, B, 255), getWidth(), 0/*getHeight()*/,
							new Color(R, G, B, 0), true);
					Graphics2D g2d = (Graphics2D) g;
					g2d.setPaint(p);
					g2d.fillRect(0, 0, getWidth(), getHeight());
				} else {
					super.paintComponent(g);
				}
			}
		};
		panelOeste.setBackground(new Color(0,0,0,0));
		frmMainCliente.getContentPane().add(panelOeste, BorderLayout.WEST);

		panelCentro = new JPanel();
		frmMainCliente.getContentPane().add(panelCentro);
		panelCentro.setLayout(new CardLayout(0, 0));
		GridBagLayout gbl_panelOeste = new GridBagLayout();
		gbl_panelOeste.columnWidths = new int[] { 71, 0 };
		gbl_panelOeste.rowHeights = new int[] { 0, 0 };
		gbl_panelOeste.columnWeights = new double[] { 0.0, Double.MIN_VALUE };
		gbl_panelOeste.rowWeights = new double[] { 0.0, Double.MIN_VALUE };
		panelOeste.setLayout(gbl_panelOeste);
		
		btnPerfilDeUsuario = new JButton("Perfil de usuario");
		btnPerfilDeUsuario.setIcon(new ImageIcon(Main_Cliente.class.getResource("/imagenes/iconos/perfilUsuario.png")));
		GridBagConstraints gbc_btnPerfilDeUsuario = new GridBagConstraints();
		gbc_btnPerfilDeUsuario.fill = GridBagConstraints.BOTH;
		gbc_btnPerfilDeUsuario.insets = new Insets(10, 5, 5, 5);
		gbc_btnPerfilDeUsuario.gridx = 0;
		gbc_btnPerfilDeUsuario.gridy = 0;
		panelOeste.add(btnPerfilDeUsuario, gbc_btnPerfilDeUsuario);
		
		btnDesconectar = new JButton("Desconectar");
		btnDesconectar.setHorizontalAlignment(SwingConstants.LEADING);
		btnDesconectar.setIcon(new ImageIcon(Main_Cliente.class.getResource("/imagenes/iconos/desconectar.png")));
		GridBagConstraints gbc_btnDesconectar = new GridBagConstraints();
		gbc_btnDesconectar.fill = GridBagConstraints.BOTH;
		gbc_btnDesconectar.insets = new Insets(5, 10, 10, 10);
		gbc_btnDesconectar.gridx = 0;
		gbc_btnDesconectar.gridy = 2;
		panelOeste.add(btnDesconectar, gbc_btnDesconectar);

		this.frmMainCliente.setVisible(false);
	}

	public void mostrarVentana() {
		this.frmMainCliente.setVisible(true);
	}
	
	public JButton getBtnDesconectar() {
		return btnDesconectar;
	}
	
	public JButton getBtnPerfilDeUsuario() {
		return btnPerfilDeUsuario;
	}

	public void cerrarVentana() {
		this.frmMainCliente.dispose();
	}

	public JPanel getPanelCentro() {
		return panelCentro;
	}
}
