package daoImplementacion;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import conexion.Conexion;
import dao.DevolucionDAO;
import dominio.Devolucion;
import dominio.DevolucionConfig;
import dominio.Usuario;

public class DevolucionDAOSQL implements DevolucionDAO {
	private final String insertDevolucion = "INSERT INTO devolucion(id_cliente, monto, fecha_creacion, porcentaje_devolucion) VALUES(?,?,NOW(),?)";
	private final String insertDevolucionConfig = "INSERT INTO devolucion_config(id_creador, desde, hasta, porcentaje, fecha_creacion) VALUES(?,?,?,?,?)";
	private final String update = "UPDATE devolucion_config SET desde = ?, hasta = ?, porcentaje = ?, estado = ? WHERE id = ?";
	private final String findAll = "SELECT * FROM devolucion_config";
	private final String findDevolucionConfig = "SELECT * FROM devolucion_config WHERE (hasta IS NULL AND ? >= desde) OR (desde <= ? AND ? <= hasta)";
	private UsuarioDAOSQL usuarioDAOSQL = new UsuarioDAOSQL();

	@Override
	public boolean insertDevolucion(Devolucion devolucion) {
		PreparedStatement statement;
		Conexion conexion = Conexion.getConexion();
		try {

			statement = conexion.getSQLConexion().prepareStatement(insertDevolucion, Statement.RETURN_GENERATED_KEYS);
			statement.setInt(1, devolucion.getCliente().getId());
			statement.setBigDecimal(2, devolucion.getMonto());
			statement.setInt(3, devolucion.getPorcentajeDevolucion());

			int affectedRows = statement.executeUpdate();

			if (affectedRows == 0) {
				throw new Exception("Creacion de Devolución fallida, no rows affected.");
			} else {
				 try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
	                    if (generatedKeys.next()) {
	                    	devolucion.setId(generatedKeys.getInt(1));
	        				return true;
	                    } else
	                        throw new Exception("Creacion de Devolución fallida, no se obtuvo id");
	                }
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean insertDevolucionConfig(DevolucionConfig devolucionConfig) {
		PreparedStatement statement;
		Conexion conexion = Conexion.getConexion();
		try {

			statement = conexion.getSQLConexion().prepareStatement(insertDevolucionConfig, Statement.RETURN_GENERATED_KEYS);
			statement.setInt(1, devolucionConfig.getCreador().getId());
			statement.setInt(2, devolucionConfig.getDesde());
			statement.setInt(3, devolucionConfig.getHasta());
			statement.setInt(4, devolucionConfig.getPorcentaje());
			Timestamp time = Timestamp.valueOf(devolucionConfig.getFechaCreacion());
			statement.setTimestamp(5, time);

			int affectedRows = statement.executeUpdate();

			if (affectedRows == 0) {
				throw new Exception("Creacion de devoluciónConfig fallida, no rows affected.");
			} else {
				 try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
	                    if (generatedKeys.next()) {
	                    	devolucionConfig.setId(generatedKeys.getInt(1));
	        				return true;
	                    } else
	                        throw new Exception("Creacion de DevoluciónConfig fallida, no se obtuvo id");
	                }
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public DevolucionConfig datosDevolucionConfig(int cantidadDias) {
		PreparedStatement statement;
		ResultSet resultSet;
		DevolucionConfig devolucionConfigurada = null;
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(findDevolucionConfig);
			statement.setInt(1, cantidadDias);
			statement.setInt(2, cantidadDias);
			statement.setInt(3, cantidadDias);
			resultSet = statement.executeQuery();

			if (resultSet.next()) {

				devolucionConfigurada = new DevolucionConfig(null, resultSet.getInt("porcentaje"),
						resultSet.getInt("desde"), resultSet.getInt("hasta"));
				devolucionConfigurada.setId(resultSet.getInt("id"));

				return devolucionConfigurada;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return devolucionConfigurada;
	}

	@Override
	public boolean update(DevolucionConfig devolucionConfig) {
		PreparedStatement statement;
		Conexion conexion = Conexion.getConexion();

		try {
			statement = conexion.getSQLConexion().prepareStatement(update);
			statement.setInt(1, devolucionConfig.getDesde());
			statement.setInt(2, devolucionConfig.getHasta());
			statement.setInt(3, devolucionConfig.getPorcentaje());
			statement.setString(4, devolucionConfig.getEstado());
			statement.setInt(5, devolucionConfig.getId());

			if (statement.executeUpdate() > 0) {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public List<DevolucionConfig> findAll() {
		PreparedStatement statement;
		ResultSet resultSet;
		List<DevolucionConfig> configsDevolucion = new ArrayList<>();
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(findAll);
			resultSet = statement.executeQuery();

			while (resultSet.next()) {
				Usuario creador = usuarioDAOSQL.findById(resultSet.getInt("id_creador"));
				DevolucionConfig devolucion = new DevolucionConfig(creador, resultSet.getInt("porcentaje"),
						resultSet.getInt("desde"), resultSet.getInt("hasta"));
				
				devolucion.setId(resultSet.getInt("id"));
				devolucion.setEstado(resultSet.getString("estado"));
				devolucion.setFechaCreacion(resultSet.getTimestamp("fecha_creacion").toLocalDateTime());
				
				configsDevolucion.add(devolucion);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return configsDevolucion;
	}
}
