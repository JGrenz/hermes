package daoImplementacion;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import conexion.Conexion;
import dao.DescripcionEventoDAO;
import dominio.DescripcionEvento;
import dominio.Evento;

public class DescripcionEventoDAOSQL implements DescripcionEventoDAO {
    private final String insert = "INSERT INTO motivo_evento(fecha_creacion, titulo, descripcion, fecha_resolucion_estimada, estado, tipo, id_evento) VALUES  (now(),?,?,?,?,?,?)";
    private final String findById = "SELECT * FROM motivo_evento WHERE id = ?";
    private final String update = "UPDATE motivo_evento SET estado = ?, fecha_resolucion_real = ?, fecha_resolucion_estimada = ? where id = ?";
    private final String findByEvento = "SELECT * FROM motivo_evento WHERE id_evento = ?";

    @Override
    public boolean insert(DescripcionEvento descripcion, int idEvento) {
        PreparedStatement statement;
        Conexion conexion = Conexion.getConexion();
        try {
            statement = conexion.getSQLConexion().prepareStatement(insert, Statement.RETURN_GENERATED_KEYS);

            Timestamp fechaResolucionEstimada = Timestamp.valueOf(descripcion.getFechaResolucionEstimada());

            statement.setString(1, descripcion.getTitulo());
            statement.setString(2, descripcion.getDescripcion());
            statement.setTimestamp(3, fechaResolucionEstimada);
            statement.setString(4, descripcion.getEstado());
            statement.setString(5, descripcion.getTipo());
            statement.setInt(6, idEvento);


            int affectedRows = statement.executeUpdate();

            if (affectedRows == 0)
                throw new Exception("Creacion de descripcion_evento fallida, no rows affected.");

            else {
                try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
                    if (generatedKeys.next()) {
                        descripcion.setId(generatedKeys.getInt(1));
                        return true;
                    } else
                        throw new Exception("Creacion de descripcion_evento fallida, no se obtuvo id");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean update(DescripcionEvento descripcion) {
        PreparedStatement statement;
        Conexion conexion = Conexion.getConexion();

        try {
            statement = conexion.getSQLConexion().prepareStatement(update);

            if (descripcion.getFechaResolucionReal() != null) {
                java.sql.Date fechaResolucionReal = java.sql.Date.valueOf(descripcion.getFechaResolucionReal());
                statement.setDate(2, fechaResolucionReal);
            } else {
                statement.setNull(2, 0);
            }

            statement.setString(1, descripcion.getEstado());
            statement.setTimestamp(3, Timestamp.valueOf(descripcion.getFechaResolucionEstimada()));
            statement.setInt(4, descripcion.getId());

            if (statement.executeUpdate() > 0) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public List<DescripcionEvento> findByEvento(Evento evento) {
        PreparedStatement statement;
        ResultSet resultSet;
        List<DescripcionEvento> descripcionesDeEventos = new ArrayList<>();
        Conexion conexion = Conexion.getConexion();
        try {
            statement = conexion.getSQLConexion().prepareStatement(findByEvento);
            statement.setInt(1, evento.getId());

            resultSet = statement.executeQuery();

            while(resultSet.next()){
                DescripcionEvento descripcionEvento = new DescripcionEvento(
                        resultSet.getString("titulo"),
                        resultSet.getString("descripcion"),
                        resultSet.getString("tipo"),
                        resultSet.getTimestamp("fecha_resolucion_estimada").toLocalDateTime()
                );

                descripcionEvento.setId(resultSet.getInt("id"));
                descripcionEvento.setFechaCreacion(resultSet.getTimestamp("fecha_creacion").toLocalDateTime());
                descripcionEvento.setEstado(resultSet.getString("estado"));



                if (resultSet.getDate("fecha_resolucion_real") != null) {
                    descripcionEvento.setFechaResolucionReal(resultSet.getDate("fecha_resolucion_real").toLocalDate());
                }

                descripcionesDeEventos.add(descripcionEvento);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return descripcionesDeEventos;
    }

    public DescripcionEvento findById(int id) {
        PreparedStatement statement;
        ResultSet resultSet;
        DescripcionEvento descripcionEvento = null;
        Conexion conexion = Conexion.getConexion();
        try {
            statement = conexion.getSQLConexion().prepareStatement(findById);
            statement.setInt(1, id);
            resultSet = statement.executeQuery();

            if (resultSet.next()) {
                String titulo = resultSet.getString("titulo");
                String descripcion = resultSet.getString("descripcion");
                LocalDateTime fechaResolucion = resultSet.getTimestamp("fecha_resolucion_estimada").toLocalDateTime();
                String tipo = resultSet.getString("tipo");

                descripcionEvento = new DescripcionEvento(titulo, descripcion, tipo, fechaResolucion); //TODO-FIJARSE SI TRAE TODO

                descripcionEvento.setEstado(resultSet.getString("estado"));
                if (resultSet.getDate("fecha_resolucion_real") != null) {
                    descripcionEvento.setFechaResolucionReal(resultSet.getDate("fecha_resolucion_real").toLocalDate());
                }
                descripcionEvento.setFechaCreacion(resultSet.getTimestamp("fecha_creacion").toLocalDateTime());
                descripcionEvento.setId(resultSet.getInt("id"));

                return descripcionEvento;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return descripcionEvento;
    }
}
