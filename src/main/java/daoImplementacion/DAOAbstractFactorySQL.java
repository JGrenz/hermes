package daoImplementacion;

import dao.*;

public class DAOAbstractFactorySQL implements DAOAbstractFactory{

	@Override
	public MedioTransporteDAO crearMedioTransporteDAO() {
		return new MedioTransporteDAOSQL();
	}

	@Override
	public OperacionDAO crearOperacionDAO() {
		return new OperacionDAOSQL();
	}

	@Override
	public PasajeDAO crearPasajeDAO() {
		return new PasajeDAOSQL();
	}

	@Override
	public UbicacionDAO crearUbicacionDAO() {
		return new UbicacionDAOSQL();
	}

	@Override
	public ViajeDAO crearViajeDAO() {
		return new ViajeDAOSQL();
	}

	@Override
	public UsuarioDAO crearUsuarioDAO() {
		return new UsuarioDAOSQL();
	}

    @Override
    public PagoDAO crearPagoDAO() {
        return new PagoDAOSQL();
    }

	@Override
	public LocalDAO crearLocalDAO() {
		return new LocalDAOSQL();
	}

	@Override
	public PaisContinenteDAO crearPaisContinenteDAO() {
		return new PaisContinenteDAOSQL();
	}
	
	@Override
	public AcompañanteDAO crearAcompañanteDAO() {
		return new AcompañanteDAOSQL();
	}

	@Override
	public MailDAO crearMailDAO() {
		return new MailDAOSQL();
	}

	@Override
	public PasajeroDAO crearPasajeroDAO() {
		return new PasajeroDAOSQL();
	}
	
	@Override
	public EventoDAO crearEventoDAO() {
		return new EventoDAOSQL();
	}
	
	@Override
	public PuntajeDAO crearPuntajeDAO() {
		return new PuntajeDAOSQL();
	}

	@Override
	public DevolucionDAO crearDevolucionDAO() {
		return new DevolucionDAOSQL();
	}

	@Override
	public OfertaDAO crearOfertaDAO() {
		return new OfertaDAOSQL();
	}

	@Override
	public NotificacionDAO crearNotificacionDAO() {
		return new NotificacionDAOSQL();
	}

	@Override
	public GastoDAO crearGastoDAO() {
		return new GastoDAOSQL();
	}

	@Override
	public DescripcionEventoDAO crearDescripcionDAO() {
		return new DescripcionEventoDAOSQL();
	}
}
