package daoImplementacion;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import conexion.Conexion;
import dao.EventoDAO;
import dominio.DescripcionEvento;
import dominio.Evento;

public class EventoDAOSQL implements EventoDAO {
    private static final String insertCliente = "INSERT INTO evento(id_creador,  motivo_cambio, id_responsable, fecha_creacion, id_cliente) VALUES(?,?,?,NOW(),?)";
    private static final String insertInteresado = "INSERT INTO evento(id_creador,  motivo_cambio, id_responsable, fecha_creacion, nombre, telefono, email) VALUES(?,?,?,NOW(),?,?,?)";
    private static final String update = "UPDATE evento set id_responsable = ? , motivo_cambio = ?, id_motivo_evento = ? WHERE id = ?";
//    	private static final String finalizar = "UPDATE evento set estado = 'FINALIZADO', fecha_resolucion_real = ? WHERE id = ?";
    private static final String findAll = "SELECT * FROM evento";
    private static final String findByResponsable = "SELECT * FROM evento where id_responsable = ?";
    //	private static final String findByNombreYApellido = "SELECT * FROM evento e INNER JOIN usuario u ON e.responsable = u.id WHERE u.nombre = ? and u.apellido = ?";
    private static final String findById = "SELECT * FROM evento WHERE id = ?";
    private UsuarioDAOSQL usuarioSQL = new UsuarioDAOSQL();
    private DescripcionEventoDAOSQL descripcionSQL = new DescripcionEventoDAOSQL();


    @Override
    public boolean insert(Evento evento, DescripcionEvento descripcion) {
        PreparedStatement statement;
        Conexion conexion = Conexion.getConexion();
        try {

            if (evento.getCliente() != null) {
                statement = conexion.getSQLConexion().prepareStatement(insertCliente, Statement.RETURN_GENERATED_KEYS);
                statement.setInt(4, evento.getCliente().getId());
            } else {
                statement = conexion.getSQLConexion().prepareStatement(insertInteresado, Statement.RETURN_GENERATED_KEYS);
                statement.setString(4, evento.getNombre());
                statement.setString(5, evento.getTelefono());
                statement.setString(6, evento.getMail());
            }

            statement.setInt(1, evento.getCreador().getId());
            statement.setString(2, evento.getMotivo());
            statement.setInt(3, evento.getResponsable().getId());

            conexion.getSQLConexion().setAutoCommit(false);

            int affectedRows = statement.executeUpdate();

            if (affectedRows == 0) {
                conexion.getSQLConexion().rollback();
                conexion.getSQLConexion().setAutoCommit(true);
                throw new Exception("Creacion de EVENTO fallida, no rows affected");
            } else {
                try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
                    if (generatedKeys.next()) {
                        evento.setId(generatedKeys.getInt(1));
                        conexion.getSQLConexion().commit();
                        conexion.getSQLConexion().setAutoCommit(true);
                        
                        
                        descripcionSQL.insert(descripcion, evento.getId());
                        
                        evento.setDescripcionEvento(descripcion);
                        update(evento);
                        return true;
                    } else
                        conexion.getSQLConexion().rollback();
                    conexion.getSQLConexion().setAutoCommit(true);
                    throw new Exception("Creacion de EVENTO fallida, no se obtuvo id");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            conexion.getSQLConexion().rollback();
            conexion.getSQLConexion().setAutoCommit(true);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean update(Evento evento) {
        PreparedStatement statement;
        Conexion conexion = Conexion.getConexion();
        try {
            statement = conexion.getSQLConexion().prepareStatement(update);

            statement.setInt(1, evento.getResponsable().getId());
            statement.setString(2, evento.getMotivo());
            statement.setInt(3, evento.getDescripcionEvento().getId());
            statement.setInt(4, evento.getId());

            conexion.getSQLConexion().setAutoCommit(false);

            if (statement.executeUpdate() > 0) {
                conexion.getSQLConexion().commit();
                conexion.getSQLConexion().setAutoCommit(true);
                return true;
            } else {
                conexion.getSQLConexion().rollback();
                conexion.getSQLConexion().setAutoCommit(true);
                throw new Exception("No pudo actualizarse el EVENTO");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override	
    public List<Evento> findAll() {
        PreparedStatement statement;
        ResultSet resultSet;
        List<Evento> lista_eventos = new ArrayList<>();
        Conexion conexion = Conexion.getConexion();
        try {
            statement = conexion.getSQLConexion().prepareStatement(findAll);
            resultSet = statement.executeQuery();

            while (resultSet.next()) {
	               Evento evento = new Evento(
                        usuarioSQL.findById((resultSet.getInt("id_creador"))),
                        usuarioSQL.findById((resultSet.getInt("id_responsable")))
                );
                int idDescripcion = resultSet.getInt("id_motivo_evento");
                evento.setId(resultSet.getInt("id"));
                DescripcionEvento descripcion = descripcionSQL.findById(idDescripcion);
                Timestamp fechacreacion = resultSet.getTimestamp("fecha_creacion");
                evento.setFecha_creacion(fechacreacion.toLocalDateTime());

                evento.setNombre(resultSet.getString("nombre"));
                evento.setMail(resultSet.getString("email"));
                evento.setTelefono(resultSet.getString("telefono"));
                evento.setCliente(usuarioSQL.findById((resultSet.getInt("id_cliente"))));
                evento.setMotivo(resultSet.getString("motivo_cambio"));

                evento.setDescripcionEvento(descripcion);
                
                lista_eventos.add(evento);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista_eventos;
    }

    @Override
    public List<Evento> findByResponsable(int id) {
        PreparedStatement statement;
        ResultSet resultSet;
        List<Evento> lista_eventos = new ArrayList<>();
        Conexion conexion = Conexion.getConexion();
        try {
            statement = conexion.getSQLConexion().prepareStatement(findByResponsable);
            statement.setInt(1, id);
            resultSet = statement.executeQuery();

            while (resultSet.next()) {
                Evento evento = new Evento(
                        usuarioSQL.findById((resultSet.getInt("id_creador"))),
                        usuarioSQL.findById((resultSet.getInt("id_responsable")))
                );
                int idDescripcion = resultSet.getInt("id_motivo_evento");
                evento.setId(resultSet.getInt("id"));
                DescripcionEvento descripcion = descripcionSQL.findById(idDescripcion);
                Timestamp fechacreacion = resultSet.getTimestamp("fecha_creacion");
                evento.setFecha_creacion(fechacreacion.toLocalDateTime());

                evento.setNombre(resultSet.getString("nombre"));
                evento.setMail(resultSet.getString("email"));
                evento.setTelefono(resultSet.getString("telefono"));
                evento.setCliente(usuarioSQL.findById((resultSet.getInt("id_cliente"))));
                evento.setMotivo(resultSet.getString("motivo_cambio"));

                evento.setDescripcionEvento(descripcion);

                lista_eventos.add(evento);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista_eventos;
    }

    @Override
    public List<Evento> findByTipo(String tipo) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<Evento> findByNombreYApellido(String nombre, String apellido) {
        // TODO Auto-generated method stub
        return null;
    }

//    @Override
//    public boolean finalizar(Evento evento) { //se finaliza una descripcion, no el evento
//        PreparedStatement statement;
//        Conexion conexion = Conexion.getConexion();
//        try {
//            statement = conexion.getSQLConexion().prepareStatement(finalizar);
//
//			java.sql.Date fecha_resolucion_real = java.sql.Date.valueOf(evento.getFecha_resolucion_real());
//
//			statement.setDate(1, fecha_resolucion_real);
//			statement.setInt(2, evento.getId());
//
//            if (statement.executeUpdate() > 0) {
//                return true;
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return false;
//    }

    @Override
    public Evento findEventoById(int id) {
        PreparedStatement statement;
        ResultSet resultSet;
        Evento evento = null;
        Conexion conexion = Conexion.getConexion();
        try {
            statement = conexion.getSQLConexion().prepareStatement(findById);
            statement.setInt(1, id);
            resultSet = statement.executeQuery();

            if (resultSet.next()) {
                evento = new Evento(
                        usuarioSQL.findById((resultSet.getInt("id_creador"))),
                        usuarioSQL.findById((resultSet.getInt("id_responsable")))
                );
                int idDescripcion = resultSet.getInt("id_motivo_evento");
                evento.setId(id);
                DescripcionEvento descripcion = descripcionSQL.findById(idDescripcion);
                Timestamp fechacreacion = resultSet.getTimestamp("fecha_creacion");
                evento.setFecha_creacion(fechacreacion.toLocalDateTime());
               
                evento.setNombre(resultSet.getString("nombre"));
                evento.setMail(resultSet.getString("email"));
                evento.setTelefono(resultSet.getString("telefono"));
                evento.setCliente(usuarioSQL.findById((resultSet.getInt("id_cliente"))));
                evento.setMotivo(resultSet.getString("motivo_cambio"));
              
                evento.setDescripcionEvento(descripcion);
                return evento;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return evento;
    }

}
