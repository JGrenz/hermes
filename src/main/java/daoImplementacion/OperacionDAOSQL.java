package daoImplementacion;

import conexion.Conexion;
import dao.OperacionDAO;
import dominio.Acompañante;
import dominio.ObjetoRankingViaje;
import dominio.ObjetoRankingUsuario;
import dominio.Operacion;
import dominio.Pasaje;
import dominio.Usuario;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class OperacionDAOSQL implements OperacionDAO {
	private final String insert = "INSERT INTO operacion(id_pasaje, id_comprador, id_pago, abonado, fecha_creacion, id_creador) VALUES (?,?,?,?,now(),?)";
	private final String findAll = "SELECT * FROM operacion";
	private final String findByPasaje = "SELECT * FROM operacion WHERE id_pasaje = ?";
	private final String findByCliente = "SELECT * FROM operacion WHERE id_comprador = ?";
	private final String findRankingEmpleados = "SELECT fk_responsable, SUM(abonado) abonado, COUNT(*) cantidad FROM "
			+ "(SELECT p.fk_responsable fk_responsable, SUM(abonado) abonado FROM operacion o, pasaje p "
			+ "WHERE (o.fecha_creacion BETWEEN ? AND ?) "
			+ "AND o.id_pasaje = p.id AND ( p.estado = 'RESERVADO' OR p.estado = 'PAGADO') GROUP BY o.id_pasaje) tabla "
			+ "GROUP BY fk_responsable";
	private final String findRankingViajes = "SELECT p.fk_viaje id_viaje, SUM(o.abonado) abonado "
			+ "FROM operacion o, pasaje p, viaje v "
			+ "WHERE (o.fecha_creacion BETWEEN '000-00-00' AND '2019-05-30 23:59:59') "
			+ "AND (p.estado = 'RESERVADO' OR p.estado = 'PAGADO') "
			+ "AND  p.fk_viaje = v.id "
			+ "AND o.id_pasaje = p.id "
			+ "GROUP BY id_viaje";
	private final String findRankingClientes = "SELECT id_comprador, SUM(abonado) abonado FROM operacion "
			+ "WHERE (fecha_creacion BETWEEN ? AND ?) GROUP BY id_comprador";
	private PasajeDAOSQL pasajeDAOSQL = new PasajeDAOSQL();
	private PagoDAOSQL pagoDAOSQL = new PagoDAOSQL();
	private UsuarioDAOSQL usuarioDAOSQL = new UsuarioDAOSQL();

	public boolean insert(Operacion operacion, List<Acompañante> lista_acompañantes) {
		Conexion conexion = Conexion.getConexion();
		try {
			conexion.getSQLConexion().setAutoCommit(false);

			if (insert(operacion)) {
				PasajeroDAOSQL pasajeroDAOSQL = new PasajeroDAOSQL();
				AcompañanteDAOSQL acompañanteDAOSQL = new AcompañanteDAOSQL();
				boolean seAgregaronTodos = true;
				for (Acompañante acompañante : lista_acompañantes) {
					if (acompañante.getId() == 0) {
						seAgregaronTodos = acompañanteDAOSQL.insert(acompañante)
								&& pasajeroDAOSQL.insert(operacion.getPasaje(), acompañante);
					} else {
						seAgregaronTodos = acompañanteDAOSQL.update(acompañante)
								&& pasajeroDAOSQL.insert(operacion.getPasaje(), acompañante);
					}
				}
				if (seAgregaronTodos) {
					conexion.getSQLConexion().commit();
					conexion.getSQLConexion().setAutoCommit(true);
					return true;
				}
			}
			conexion.getSQLConexion().rollback();
			conexion.getSQLConexion().setAutoCommit(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean insert(Operacion operacion) {
		PreparedStatement statement;
		Conexion conexion = Conexion.getConexion();
		boolean seGuardaPasaje = operacion.getPasaje().getId() == 0;
		try {
			statement = conexion.getSQLConexion().prepareStatement(insert, Statement.RETURN_GENERATED_KEYS);

			if (!seGuardaPasaje) {
				conexion.getSQLConexion().setAutoCommit(false);
			}

			int id_pasaje;
			if (seGuardaPasaje) {
				id_pasaje = pasajeDAOSQL.insert(operacion.getPasaje());
				if (id_pasaje == 0) {
					throw new Exception("Creacion de Operacion fallida, no se guardó el pasaje.");
				}
			} else {
				id_pasaje = operacion.getPasaje().getId();
				if (!pasajeDAOSQL.update(operacion.getPasaje())) {
					throw new Exception("No se pudo actualizar el estado del pasaje");
				}
			}

			int id_pago = pagoDAOSQL.insert(operacion.getPago());
			if (id_pago == 0) {
				throw new Exception("Creacion de Operacion fallida, no se guardó el pago.");
			}

			statement.setInt(1, id_pasaje);
			statement.setInt(2, operacion.getCliente().getId());
			statement.setInt(3, id_pago);
			statement.setBigDecimal(4, operacion.getMonto());
			statement.setInt(5, operacion.getResponsable().getId());

			int affectedRows = statement.executeUpdate();

			if (affectedRows == 0) {
				throw new Exception("Creacion de Operacion fallida, no rows affected.");
			} else {
				try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
					if (generatedKeys.next()) {
						operacion.setFechaCreacion(LocalDateTime.now());
						operacion.setId(generatedKeys.getInt(1));
						if (!seGuardaPasaje) {
							conexion.getSQLConexion().commit();
							conexion.getSQLConexion().setAutoCommit(true);
						}
						return true;
					} else
						throw new Exception("Creacion de operación fallida, no se obtuvo id");
				}
			}
		} catch (Exception e) {
			try {
				if (!seGuardaPasaje) {
					conexion.getSQLConexion().rollback();
					conexion.getSQLConexion().setAutoCommit(true);
				}
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public List<Operacion> findAll() {
		PreparedStatement statement;
		ResultSet resultSet;
		List<Operacion> lista_operaciones = new ArrayList<>();
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(findAll);
			resultSet = statement.executeQuery();

			while (resultSet.next()) {
				Operacion operacion = new Operacion(resultSet.getBigDecimal("abonado"),
						pagoDAOSQL.findById(resultSet.getInt("id_pago")),
						pasajeDAOSQL.findById(resultSet.getInt("id_pasaje")),
						usuarioDAOSQL.findById(resultSet.getInt("id_comprador")),
						usuarioDAOSQL.findById(resultSet.getInt("id_creador")));
				operacion.setId(resultSet.getInt("id"));
				operacion.setFechaCreacion(resultSet.getTimestamp("fecha_creacion").toLocalDateTime());
				lista_operaciones.add(operacion);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lista_operaciones;
	}

	@Override
	public List<Operacion> findByPasaje(Pasaje pasaje) {
		PreparedStatement statement;
		ResultSet resultSet;
		List<Operacion> operaciones = new ArrayList<>();
		Conexion conexion = Conexion.getConexion();
		try {

			statement = conexion.getSQLConexion().prepareStatement(findByPasaje);
			statement.setInt(1, pasaje.getId());

			resultSet = statement.executeQuery();

			while (resultSet.next()) {
				Operacion operacion = new Operacion(resultSet.getBigDecimal("abonado"),
						pagoDAOSQL.findById(resultSet.getInt("id_pago")), pasaje, pasaje.getCliente(),
						usuarioDAOSQL.findById(resultSet.getInt("id_creador")));
				operacion.setId(resultSet.getInt("id"));
				operacion.setFechaCreacion(resultSet.getTimestamp("fecha_creacion").toLocalDateTime());
				operaciones.add(operacion);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return operaciones;
	}

	public List<Operacion> findByCliente(Usuario cliente) {
		PreparedStatement statement;
		ResultSet resultSet;
		List<Operacion> operaciones = new ArrayList<>();
		Conexion conexion = Conexion.getConexion();
		try {

			statement = conexion.getSQLConexion().prepareStatement(findByCliente);
			statement.setInt(1, cliente.getId());

			resultSet = statement.executeQuery();

			while (resultSet.next()) {
				Operacion operacion = new Operacion(resultSet.getBigDecimal("abonado"),
						pagoDAOSQL.findById(resultSet.getInt("id_pago")),
						pasajeDAOSQL.findById(resultSet.getInt("id_pasaje")), cliente,
						usuarioDAOSQL.findById(resultSet.getInt("id_creador")));
				operacion.setId(resultSet.getInt("id"));
				operaciones.add(operacion);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return operaciones;
	}

	public List<ObjetoRankingUsuario> findRankingEmpleados(LocalDateTime desde, LocalDateTime hasta) {
		PreparedStatement statement;
		ResultSet resultSet;
		List<ObjetoRankingUsuario> lista_empleado = new ArrayList<>();
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(findRankingEmpleados);

			java.sql.Timestamp timeDesde = null;
			java.sql.Timestamp timeHasta = null;
			if(desde == null) {
				timeDesde = java.sql.Timestamp.valueOf(LocalDateTime.of(0, 1, 1, 0, 0));
			} else {
				timeDesde = java.sql.Timestamp.valueOf(desde);
			}
			if(hasta == null) {
				timeHasta = java.sql.Timestamp.valueOf(LocalDateTime.now().withHour(23).withMinute(59).withSecond(59));
			} else {
				timeHasta = java.sql.Timestamp.valueOf(hasta);
			}
			
			statement.setTimestamp(1, timeDesde);
			statement.setTimestamp(2, timeHasta);

			resultSet = statement.executeQuery();

			while (resultSet.next()) {
				ObjetoRankingUsuario empleado = new ObjetoRankingUsuario(
						usuarioDAOSQL.findById(resultSet.getInt("fk_responsable")), resultSet.getBigDecimal("abonado"),
						resultSet.getInt("cantidad"));
				lista_empleado.add(empleado);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lista_empleado;
	}

	@Override
	public List<ObjetoRankingViaje> findRankingViajes(LocalDateTime desde, LocalDateTime hasta) {
		PreparedStatement statement;
		ResultSet resultSet;
		List<ObjetoRankingViaje> lista_viaje = new ArrayList<>();
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(findRankingViajes);

			java.sql.Timestamp timeDesde = null;
			java.sql.Timestamp timeHasta = null;
			if(desde == null) {
				timeDesde = java.sql.Timestamp.valueOf(LocalDateTime.of(0, 1, 1, 0, 0));
			} else {
				timeDesde = java.sql.Timestamp.valueOf(desde);
			}
			if(hasta == null) {
				timeHasta = java.sql.Timestamp.valueOf(LocalDateTime.now().withHour(23).withMinute(59).withSecond(59));
			} else {
				timeHasta = java.sql.Timestamp.valueOf(hasta);
			}
			
			statement.setTimestamp(1, timeDesde);
			statement.setTimestamp(2, timeHasta);

			resultSet = statement.executeQuery();
			ViajeDAOSQL viajeDAOSQL = new ViajeDAOSQL();

			while (resultSet.next()) {
				ObjetoRankingViaje viaje = new ObjetoRankingViaje(
						viajeDAOSQL.findById(resultSet.getInt("id_viaje")), resultSet.getBigDecimal("abonado"));
				lista_viaje.add(viaje);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lista_viaje;
	}

	@Override
	public List<ObjetoRankingUsuario> findRankingClientes(LocalDateTime desde, LocalDateTime hasta) {
		PreparedStatement statement;
		ResultSet resultSet;
		List<ObjetoRankingUsuario> lista_cliente = new ArrayList<>();
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(findRankingClientes);

			java.sql.Timestamp timeDesde = null;
			java.sql.Timestamp timeHasta = null;
			if(desde == null) {
				timeDesde = java.sql.Timestamp.valueOf(LocalDateTime.of(0, 1, 1, 0, 0));
			} else {
				timeDesde = java.sql.Timestamp.valueOf(desde);
			}
			if(hasta == null) {
				timeHasta = java.sql.Timestamp.valueOf(LocalDateTime.now().withHour(23).withMinute(59).withSecond(59));
			} else {
				timeHasta = java.sql.Timestamp.valueOf(hasta);
			}
			
			statement.setTimestamp(1, timeDesde);
			statement.setTimestamp(2, timeHasta);

			resultSet = statement.executeQuery();

			while (resultSet.next()) {
				ObjetoRankingUsuario cliente = new ObjetoRankingUsuario(
						usuarioDAOSQL.findById(resultSet.getInt("id_comprador")), resultSet.getBigDecimal("abonado"),
						0);
				lista_cliente.add(cliente);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lista_cliente;
	}
}
