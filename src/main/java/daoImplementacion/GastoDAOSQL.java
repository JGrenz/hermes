package daoImplementacion;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import conexion.Conexion;
import dao.GastoDAO;
import dominio.Gasto;

public class GastoDAOSQL implements GastoDAO {
    private static final String insert = "INSERT INTO gasto(fecha_creacion, codigo_referencia, monto, tipoGasto, fk_local, estado, fecha_gasto) VALUES (NOW(),?,?,?,?,?,?)";
    private static final String update = "UPDATE gasto SET monto = ?, tipoGasto = ?, fk_local = ?, estado = ?, fecha_gasto = ? WHERE id = ?";
    private static final String inhabilitar = "UPDATE gasto SET estado = 'INHABILITADO' WHERE id = ?";
    private static final String findAll = "SELECT * FROM gasto";

    LocalDAOSQL localDAOSQL = new LocalDAOSQL();


    @Override
    public boolean insert(Gasto gasto) {
        PreparedStatement statement;
        Conexion conexion = Conexion.getConexion();
        try {
            statement = conexion.getSQLConexion().prepareStatement(insert, Statement.RETURN_GENERATED_KEYS);

            statement.setString(1, gasto.getCodigoReferencia());
            statement.setBigDecimal(2, gasto.getMonto());
            statement.setString(3, gasto.getTipoGasto());
            statement.setInt(4, gasto.getLocal().getId());
            statement.setString(5, gasto.getEstado());
            java.sql.Date fechaGasto = java.sql.Date.valueOf(gasto.getFechaGasto());
            statement.setDate(6, fechaGasto);


            int affectedRows = statement.executeUpdate();

            if (affectedRows == 0)
                throw new Exception("Creacion de gasto fallida, no rows affected.");

            else {
                try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
                    if (generatedKeys.next()) {
                        gasto.setId(generatedKeys.getInt(1));
                        return true;
                    } else
                        throw new Exception("Creacion de gasto fallida, no se obtuvo id");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean update(Gasto gasto) {
        PreparedStatement statement;
        Conexion conexion = Conexion.getConexion();

        try {
            statement = conexion.getSQLConexion().prepareStatement(update);
            statement.setBigDecimal(1, gasto.getMonto());
            statement.setString(2, gasto.getTipoGasto());
            statement.setInt(3, gasto.getLocal().getId());
            statement.setString(4, gasto.getEstado());
            java.sql.Date fechaGasto = java.sql.Date.valueOf(gasto.getFechaGasto());
            statement.setDate(5, fechaGasto);
            statement.setInt(6, gasto.getId());

            if (statement.executeUpdate() > 0) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
    
    @Override
    public boolean cancelarGasto(Gasto gasto) {
        PreparedStatement statement;
        Conexion conexion = Conexion.getConexion();

        try {
            statement = conexion.getSQLConexion().prepareStatement(inhabilitar);
            
            statement.setInt(1, gasto.getId());

            if (statement.executeUpdate() > 0) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public Gasto findByCodigoReferencia() {
        return null;
    }

    @Override
    public List<Gasto> findAll() {
        PreparedStatement statement;
        ResultSet resultSet;
        List<Gasto> gastos = new ArrayList<>();
        Conexion conexion = Conexion.getConexion();
        try {
            statement = conexion.getSQLConexion().prepareStatement(findAll);
            resultSet = statement.executeQuery();

            
            while (resultSet.next()) {
                Gasto gasto = new Gasto(
                        resultSet.getBigDecimal("monto"),
                        resultSet.getString("tipoGasto"),
                        localDAOSQL.findById(resultSet.getInt("fk_local")),
                        resultSet.getDate("fecha_gasto").toLocalDate()
                );
                gasto.setId(resultSet.getInt("id"));
                gasto.setCodigoReferencia(resultSet.getString("codigo_referencia"));
                gasto.setEstado(resultSet.getString("estado"));
                gasto.setFechaCreacion(resultSet.getTimestamp("fecha_creacion").toLocalDateTime());

                gastos.add(gasto);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return gastos;
    }
}
