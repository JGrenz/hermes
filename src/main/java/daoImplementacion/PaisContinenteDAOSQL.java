package daoImplementacion;

import conexion.Conexion;
import dao.PaisContinenteDAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class PaisContinenteDAOSQL implements PaisContinenteDAO {
    private final String findAll = "SELECT * FROM pais_continente WHERE codigo_continente = ?";
    private final String findAllContinentes = "SELECT * FROM continente";

    @Override
    public List<String> finAllContinentes() {
        PreparedStatement statement;
        ResultSet resultSet;
        List<String> continentes = new ArrayList<>();
        Conexion conexion = Conexion.getConexion();
        try {
            statement = conexion.getSQLConexion().prepareStatement(findAllContinentes);
            resultSet = statement.executeQuery();

            while(resultSet.next()){
                String continente = resultSet.getString("continente");
                continentes.add(continente);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return continentes;
    }


    @Override
    public List<Locale> findAllPaises(String continente) {
        PreparedStatement statement;
        ResultSet resultSet;
        List<Locale> paises = new ArrayList<>();
        Conexion conexion = Conexion.getConexion();
        try {
            statement = conexion.getSQLConexion().prepareStatement(findAll);

            statement.setString(1, continente);

            resultSet = statement.executeQuery();

            while(resultSet.next()){
                Locale pais = new Locale("",resultSet.getString("codigo_pais"));

                paises.add(pais);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return paises;
    }
}
