package daoImplementacion;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import conexion.Conexion;
import dao.PasajeDAO;
import dominio.Pasaje;
import dominio.Usuario;
import dominio.Viaje;

public class PasajeDAOSQL implements PasajeDAO {

	private final String insert = "INSERT INTO pasaje(codigo, fecha_creacion, estado, fk_cliente, fk_viaje, fecha_vencimiento, descuento, fk_responsable, fk_local) VALUES(?, now(), ?, ?, ?, ?, ?, ?, ?)";
	private final String update = "UPDATE pasaje SET estado = ? WHERE id = ?";
	private final String updateReserva = "UPDATE pasaje SET fecha_vencimiento = ? WHERE id = ?";
	private final String findAll = "SELECT * FROM pasaje";
	private final String findAllDisponibles = "SELECT * FROM pasaje where estado = 'PAGADO' or estado = 'RESERVADO'";
	private final String findById = "SELECT * FROM pasaje WHERE id = ?";
	private final String findByCodigoReferencia = "SELECT * FROM pasaje WHERE codigo = ?";
	private final String findByCliente = "SELECT * FROM pasaje WHERE fk_cliente = ?";
	private final String findByViaje = "SELECT * FROM pasaje WHERE fk_viaje = ?";
	private final String countByDisponibles = "SELECT * FROM pasaje WHERE fk_viaje = ? and (estado = 'PAGADO' or estado = 'RESERVADO')";
	private final String findByViajeDisponible = "SELECT p.id FROM pasaje p, viaje v WHERE p.fk_cliente = ? AND p.fk_viaje = v.id AND v.estado = 'ABIERTO'";
	private final String countByCliente = "SELECT COUNT(*) FROM pasaje WHERE fk_cliente = ? AND (estado = 'PAGADO' or estado = 'RESERVADO')";

	private UsuarioDAOSQL usuarioSQL = new UsuarioDAOSQL();
	private ViajeDAOSQL viajeSQL = new ViajeDAOSQL();
	private AcompañanteDAOSQL acompañanteSQL = new AcompañanteDAOSQL();

	public int insert(Pasaje pasaje) {
		PreparedStatement statement;
		Conexion conexion = Conexion.getConexion();

		try {
			statement = conexion.getSQLConexion().prepareStatement(insert, Statement.RETURN_GENERATED_KEYS);

			statement.setString(1, pasaje.getCodigo());
			statement.setString(2, pasaje.getEstadoPasaje());
			statement.setInt(3, pasaje.getCliente().getId());
			statement.setInt(4, pasaje.getViaje().getId());
			java.sql.Date fechaVtoReserva = java.sql.Date.valueOf(pasaje.getFecha_reserva());
			statement.setDate(5, fechaVtoReserva);
			statement.setInt(6, pasaje.getDescuento());
			statement.setInt(7, pasaje.getResponsable().getId());
			statement.setInt(8, pasaje.getLocal().getId());

			int affectedRows = statement.executeUpdate();

			if (affectedRows == 0)
				throw new Exception("Creacion de PASAJE fallida, no rows affected");
			else {
				try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
					if (generatedKeys.next()) {
						pasaje.setId(generatedKeys.getInt(1));
						return pasaje.getId();
					} else
						throw new Exception("Creacion de PASAJE fallida, no se obtuvo id");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	public boolean updateReserva(Pasaje pasaje) {
		PreparedStatement statement;
		Conexion conexion = Conexion.getConexion();

		try {
			statement = conexion.getSQLConexion().prepareStatement(updateReserva);

			java.sql.Date fechaVtoReserva = java.sql.Date.valueOf(pasaje.getFecha_reserva());
			statement.setDate(1, fechaVtoReserva);
			statement.setInt(2, pasaje.getId());

			if (statement.executeUpdate() > 0) {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public boolean update(Pasaje pasaje) {
		PreparedStatement statement;
		Conexion conexion = Conexion.getConexion();

		try {
			statement = conexion.getSQLConexion().prepareStatement(update);
			statement.setString(1, pasaje.getEstadoPasaje());
			statement.setInt(2, pasaje.getId());

			if (statement.executeUpdate() > 0) {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public List<Pasaje> findAll() {
		PreparedStatement statement;
		ResultSet resultSet;
		List<Pasaje> lista_pasajes = new ArrayList<>();
		Conexion conexion = Conexion.getConexion();

		try {
			statement = conexion.getSQLConexion().prepareStatement(findAll);
			resultSet = statement.executeQuery();

			while (resultSet.next()) {
				Usuario responsable = usuarioSQL.findById(resultSet.getInt("fk_responsable"));
				Pasaje pasaje = new Pasaje(viajeSQL.findById(resultSet.getInt("fk_viaje")),
						usuarioSQL.findById(resultSet.getInt("fk_cliente")), resultSet.getString("estado"),
						responsable, responsable.getLocal());
				pasaje.setCodigo(resultSet.getString("codigo"));
				pasaje.setId(resultSet.getInt("id"));
				pasaje.setDescuento(resultSet.getInt("descuento"));

				Date fechaReservaSQL = resultSet.getDate("fecha_vencimiento");
				pasaje.setFechaReserva(fechaReservaSQL.toLocalDate());

				lista_pasajes.add(pasaje);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lista_pasajes;
	}

	@Override
	public Pasaje findById(int id) {
		PreparedStatement statement;
		ResultSet resultSet;
		Pasaje pasaje = null;
		Conexion conexion = Conexion.getConexion();

		try {
			statement = conexion.getSQLConexion().prepareStatement(findById);
			statement.setInt(1, id);
			resultSet = statement.executeQuery();

			if (resultSet.next()) {
				Usuario responsable = usuarioSQL.findById(resultSet.getInt("fk_responsable"));
				pasaje = new Pasaje(viajeSQL.findById(resultSet.getInt("fk_viaje")),
						usuarioSQL.findById(resultSet.getInt("fk_cliente")), resultSet.getString("estado"),
						responsable, responsable.getLocal());
				pasaje.setId(id);
				pasaje.setCodigo(resultSet.getString("codigo"));
				pasaje.setDescuento(resultSet.getInt("descuento"));
				return pasaje;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return pasaje;
	}

	@Override
	public Pasaje findByCodigoReferencia(String codigoReferencia) {
		PreparedStatement statement;
		ResultSet resultSet;
		Pasaje pasaje = null;
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(findByCodigoReferencia);
			statement.setString(1, codigoReferencia);
			resultSet = statement.executeQuery();

			if (resultSet.next()) {
				Usuario responsable = usuarioSQL.findById(resultSet.getInt("fk_responsable"));
				pasaje = new Pasaje(viajeSQL.findById(resultSet.getInt("fk_viaje")),
						usuarioSQL.findById(resultSet.getInt("fk_cliente")), resultSet.getString("estado"),
						responsable, responsable.getLocal());
				pasaje.setId(resultSet.getInt("id"));
				pasaje.setCodigo(resultSet.getString("codigo"));
				pasaje.setDescuento(resultSet.getInt("descuento"));
				return pasaje;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return pasaje;
	}

	public List<Pasaje> findByCliente(Usuario cliente) {
		PreparedStatement statement;
		ResultSet resultSet;
		List<Pasaje> lista_pasajes = new ArrayList<>();
		Conexion conexion = Conexion.getConexion();

		try {
			statement = conexion.getSQLConexion().prepareStatement(findByCliente);
			statement.setInt(1, cliente.getId());
			resultSet = statement.executeQuery();

			while (resultSet.next()) {
				Usuario responsable = usuarioSQL.findById(resultSet.getInt("fk_responsable"));
				Pasaje pasaje = new Pasaje(viajeSQL.findById(resultSet.getInt("fk_viaje")),
						usuarioSQL.findById(resultSet.getInt("fk_cliente")), resultSet.getString("estado"),
						responsable, responsable.getLocal());
				pasaje.setId(resultSet.getInt("id"));
				pasaje.setCodigo(resultSet.getString("codigo"));
				pasaje.setDescuento(resultSet.getInt("descuento"));
				lista_pasajes.add(pasaje);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lista_pasajes;
	}

	public List<Pasaje> findByViaje(Viaje viaje) {
		PreparedStatement statement;
		ResultSet resultSet;
		List<Pasaje> lista_pasajes = new ArrayList<>();
		Conexion conexion = Conexion.getConexion();

		try {
			statement = conexion.getSQLConexion().prepareStatement(findByViaje);
			statement.setInt(1, viaje.getId());
			resultSet = statement.executeQuery();

			while (resultSet.next()) {
				Usuario responsable = usuarioSQL.findById(resultSet.getInt("fk_responsable"));
				Pasaje pasaje = new Pasaje(viajeSQL.findById(resultSet.getInt("fk_viaje")),
						usuarioSQL.findById(resultSet.getInt("fk_cliente")), resultSet.getString("estado"),
						responsable, responsable.getLocal());
				pasaje.setId(resultSet.getInt("id"));
				pasaje.setCodigo(resultSet.getString("codigo"));
				pasaje.setDescuento(resultSet.getInt("descuento"));
				lista_pasajes.add(pasaje);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lista_pasajes;
	}

	public int countByDisponibles(Viaje viaje) {
		PreparedStatement statement;
		ResultSet resultSet;
		int disponibles = 0;
		Conexion conexion = Conexion.getConexion();

		try {
			statement = conexion.getSQLConexion().prepareStatement(countByDisponibles);
			statement.setInt(1, viaje.getId());
			resultSet = statement.executeQuery();

			while (resultSet.next()) {
				Pasaje pasaje = new Pasaje(null, null, "PAGADO", null, null);

				pasaje.setId(resultSet.getInt("id"));

				disponibles += acompañanteSQL.findByPasaje(pasaje).size() + 1;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return disponibles;
	}

	@Override
	public List<Pasaje> findAllDisponibles() {
		PreparedStatement statement;
		ResultSet resultSet;
		List<Pasaje> lista_pasajes = new ArrayList<>();
		Conexion conexion = Conexion.getConexion();

		try {
			statement = conexion.getSQLConexion().prepareStatement(findAllDisponibles);
			resultSet = statement.executeQuery();

			while (resultSet.next()) {
				Usuario responsable = usuarioSQL.findById(resultSet.getInt("fk_responsable"));
				Pasaje pasaje = new Pasaje(viajeSQL.findById(resultSet.getInt("fk_viaje")),
						usuarioSQL.findById(resultSet.getInt("fk_cliente")), resultSet.getString("estado"),
						responsable, responsable.getLocal());
				pasaje.setCodigo(resultSet.getString("codigo"));
				pasaje.setId(resultSet.getInt("id"));
				pasaje.setFechaCreacion(resultSet.getTimestamp("fecha_creacion").toLocalDateTime());
				pasaje.setDescuento(resultSet.getInt("descuento"));

				lista_pasajes.add(pasaje);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lista_pasajes;
	}

	public boolean cancelarPasajesDelCliente(Usuario usuario) {
		PreparedStatement statement;
		ResultSet resultSet;
		List<Pasaje> lista_pasajes = new ArrayList<>();
		Conexion conexion = Conexion.getConexion();

		try {
			statement = conexion.getSQLConexion().prepareStatement(findByViajeDisponible);
			statement.setInt(1, usuario.getId());
			resultSet = statement.executeQuery();

			while (resultSet.next()) {
				Pasaje pasaje = new Pasaje(null, null, "CANCELADO", null, null);
				pasaje.setId(resultSet.getInt("p.id"));
				lista_pasajes.add(pasaje);
			}
			boolean flag = true;
			for (Pasaje pasaje : lista_pasajes) {
				flag = flag && update(pasaje);
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean tienePasajes(Usuario cliente) {
		PreparedStatement statement;
		ResultSet resultSet;
		Conexion conexion = Conexion.getConexion();

		try {
			statement = conexion.getSQLConexion().prepareStatement(countByCliente);
			statement.setInt(1, cliente.getId());
			resultSet = statement.executeQuery();

			if (resultSet.next()) {
				return resultSet.getInt(1) > 0;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
}
