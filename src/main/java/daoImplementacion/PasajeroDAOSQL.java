package daoImplementacion;

import dominio.Acompañante;
import dominio.Pasaje;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import conexion.Conexion;
import dao.PasajeroDAO;

public class PasajeroDAOSQL implements PasajeroDAO {

	private static final String insert = "INSERT INTO pasajero(id_acompañante, id_pasaje) VALUES(?,?)";
	private static final String countByPasaje = "SELECT COUNT(*) FROM pasajero WHERE id_pasaje = ?";

	@Override
	public boolean insert(Pasaje pasaje, Acompañante acompañante) {
		PreparedStatement statement;
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(insert, Statement.RETURN_GENERATED_KEYS);

			statement.setInt(1, acompañante.getId());
			statement.setInt(2, pasaje.getId());

			if (statement.executeUpdate() == 0) {
				throw new Exception("Creacion de PASAJERO fallida, no rows affected");
			} else {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public int countByPasaje(Pasaje pasaje) {
		PreparedStatement statement;
		Conexion conexion = Conexion.getConexion();
		ResultSet resultSet;
		try {
			statement = conexion.getSQLConexion().prepareStatement(countByPasaje);
			statement.setInt(1, pasaje.getId());
			
			resultSet = statement.executeQuery();
			
			if(resultSet.next()) {
				return resultSet.getInt(1);
			}
			
		}catch (Exception e) {
			e.printStackTrace();
		}

		return 0;
	}
	
	
	
	
}
