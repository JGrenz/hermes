package daoImplementacion;

import conexion.Conexion;
import dao.LocalDAO;
import dominio.Local;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class LocalDAOSQL implements LocalDAO {
	private final String insert = "INSERT INTO local(nombre, direccion, localidad, detalle, provincia) values (?, ?, ?, ?, ?)";
	private final String update = "UPDATE local SET nombre = ?, direccion = ?, localidad = ?, provincia = ?, detalle = ? WHERE id = ?";
	private final String updateEstado = "UPDATE local SET estado = ? WHERE id = ?";
	private final String findByNombre = "SELECT * FROM local WHERE nombre = ?";
	private final String findById = "Select * FROM LOCAL WHERE id = ?";
	private final String findAll = "SELECT * FROM local";

	@Override
	public boolean insert(Local local) {
		PreparedStatement statement;
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(insert, Statement.RETURN_GENERATED_KEYS);
			statement.setString(1, local.getNombre());
			statement.setString(2, local.getDireccion());
			statement.setString(3, local.getLocalidad());
			statement.setString(4, local.getDetalle());
			statement.setString(5, local.getProvincia());

			int affectedRows = statement.executeUpdate();

			if (affectedRows == 0)
				throw new Exception("Creacion de LOCAL fallida, no rows affected.");

			else {
				try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
					if (generatedKeys.next()) {
						local.setId(generatedKeys.getInt(1));
						return true;
					} else
						throw new Exception("Creacion de LOCAL fallida, no se obtuvo id");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean update(Local local) {
		PreparedStatement statement;
		Conexion conexion = Conexion.getConexion();

		try {
			statement = conexion.getSQLConexion().prepareStatement(update);

			statement.setString(1, local.getNombre());
			statement.setString(2, local.getDireccion());
			statement.setString(3, local.getLocalidad());
			statement.setString(4, local.getProvincia());
			statement.setString(5, local.getDetalle());
			statement.setInt(6, local.getId());

			if (statement.executeUpdate() > 0) {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public Local findByNombre(String nombre) {
		PreparedStatement statement;
		ResultSet resultSet;
		Local local = null;
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(findByNombre);
			statement.setString(1, nombre);
			resultSet = statement.executeQuery();

			if (resultSet.next()) {

				local = new Local(resultSet.getString("nombre"), resultSet.getString("direccion"),
						resultSet.getString("localidad"), resultSet.getString("provincia"),
						resultSet.getString("detalle"));
				local.setId(resultSet.getInt("id"));
				local.setEstado(resultSet.getString("estado"));
				return local;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return local;
	}

	@Override
	public Local findById(int id) {
		PreparedStatement statement;
		ResultSet resultSet;
		Local local = null;
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(findById);
			statement.setInt(1, id);
			resultSet = statement.executeQuery();

			if (resultSet.next()) {

				local = new Local(resultSet.getString("nombre"), resultSet.getString("direccion"),
						resultSet.getString("localidad"), resultSet.getString("provincia"),
						resultSet.getString("detalle"));
				local.setId(id);
				local.setEstado(resultSet.getString("estado"));
				return local;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return local;
	}

	@Override
	public List<Local> findAll() {
		PreparedStatement statement;
		ResultSet resultSet;
		List<Local> locales = new ArrayList<>();
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(findAll);
			resultSet = statement.executeQuery();

			while (resultSet.next()) {
				Local local = new Local(resultSet.getString("nombre"), resultSet.getString("direccion"), resultSet.getString("localidad"),
						resultSet.getString("provincia"), resultSet.getString("detalle"));
				local.setEstado(resultSet.getString("estado"));
				local.setId(resultSet.getInt("id"));

				locales.add(local);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return locales;
	}

	@Override
	public boolean update(Local local, String estado) {
		PreparedStatement statement;
		Conexion conexion = Conexion.getConexion();

		try {
			statement = conexion.getSQLConexion().prepareStatement(updateEstado);

			statement.setString(1, estado);
			statement.setInt(2, local.getId());

			if (statement.executeUpdate() > 0) {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

}
