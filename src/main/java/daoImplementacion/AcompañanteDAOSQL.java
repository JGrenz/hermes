package daoImplementacion;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import conexion.Conexion;
import dao.AcompañanteDAO;
import dominio.Acompañante;
import dominio.Contacto;
import dominio.Pasaje;

public class AcompañanteDAOSQL implements AcompañanteDAO {

	private static final String insert = "INSERT INTO acompañante(dni, nombre, apellido, fecha_nacimiento, email, id_contacto) VALUES(?,?,?,?,?,?)";
	private static final String update = "UPDATE acompañante set dni = ? , nombre = ? , apellido = ? , email = ? , fecha_nacimiento = ? WHERE id = ?";
	private static final String findAll = "SELECT * FROM acompañante a INNER JOIN contacto c ON a.id_contacto = c.id";
	private static final String findById = "SELECT * FROM acompañante a INNER JOIN contacto c ON a.id_contacto = c.id WHERE a.id = ?";
	private static final String findByDNI = "SELECT * FROM acompañante a INNER JOIN contacto c ON a.id_contacto = c.id WHERE a.dni = ?";
	private static final String findByPasaje = "SELECT * FROM pasajero WHERE id_pasaje = ?";
	private ContactoDAOSQL contactoDAOSQL = new ContactoDAOSQL();

	@Override
	public boolean insert(Acompañante acompañante) {
		PreparedStatement statement;
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(insert, Statement.RETURN_GENERATED_KEYS);

			statement.setString(1, acompañante.getDni());
			statement.setString(2, acompañante.getNombre());
			statement.setString(3, acompañante.getApellido());

			java.sql.Date fecha = Date.valueOf(acompañante.getFecha_nacimiento());
			statement.setDate(4, fecha);
			statement.setString(5, acompañante.getEmail());


			int id_contacto = contactoDAOSQL.insert(acompañante.getContacto());
			statement.setInt(6, id_contacto);

			int affectedRows = statement.executeUpdate();

			if (affectedRows == 0 || id_contacto == 0) {
				throw new Exception("Creacion de ACOMPAÑANTE fallida, no rows affected");
			} else {
				try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
					if (generatedKeys.next()) {
						acompañante.setId(generatedKeys.getInt(1));
						return true;
					} else
					throw new Exception("Creacion de ACOMPAÑANTE fallida, no se obtuvo id");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	 @Override
	    public boolean update(Acompañante acompañante) {
	        PreparedStatement statement;
	        Conexion conexion = Conexion.getConexion();
	        try {
	            statement = conexion.getSQLConexion().prepareStatement(update);

	            statement.setString(1, acompañante.getDni());
	            statement.setString(2, acompañante.getNombre());
	            statement.setString(3, acompañante.getApellido());
	            statement.setString(4, acompañante.getEmail());
	            java.sql.Date fechaNacimiento = java.sql.Date.valueOf(acompañante.getFecha_nacimiento());
	            statement.setDate(5, fechaNacimiento);
	            statement.setInt(6, acompañante.getId());

	            boolean contacto_modificado = contactoDAOSQL.update(acompañante.getContacto());

	            if (statement.executeUpdate() > 0 && contacto_modificado) {
	                return true;
	            } else {
	                throw new Exception("No pudo actualizarse el ACOMPAÑANTE");
	            }
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	        return false;
	    }

	@Override
	public List<Acompañante> findAll() {
		PreparedStatement statement;
		ResultSet resultSet;
		List<Acompañante> lista_acompañante = new ArrayList<Acompañante>();
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(findAll);
			resultSet = statement.executeQuery();

			while (resultSet.next()) {
				Acompañante acompañante = new Acompañante(resultSet.getString("a.dni"), resultSet.getString("a.nombre"),
						resultSet.getString("a.apellido"), resultSet.getDate("a.fecha_nacimiento").toLocalDate(),
						resultSet.getString("a.email"), null);
				acompañante.setId(resultSet.getInt("a.id"));
				Contacto contacto = new Contacto(resultSet.getString("c.telefono1"), null, null, null, null);
				contacto.setId(resultSet.getInt("c.id"));
				acompañante.setContacto(contacto);
				lista_acompañante.add(acompañante);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lista_acompañante;
	}

	@Override
	public Acompañante findById(int id) {
		PreparedStatement statement;
		ResultSet resultSet;
		Acompañante acompañante = null;
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(findById);
			statement.setInt(1, id);
			resultSet = statement.executeQuery();

			while (resultSet.next()) {
				acompañante = new Acompañante(resultSet.getString("a.dni"), resultSet.getString("a.nombre"),
						resultSet.getString("a.apellido"), resultSet.getDate("a.fecha_nacimiento").toLocalDate(),
						resultSet.getString("a.email"), null);
				acompañante.setId(resultSet.getInt("a.id"));
				Contacto contacto = new Contacto(resultSet.getString("c.telefono1"), null, null, null, null);
				contacto.setId(resultSet.getInt("c.id"));
				acompañante.setContacto(contacto);

				return acompañante;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return acompañante;
	}

	@Override
	public Acompañante findByDNI(String dni) {
		PreparedStatement statement;
		ResultSet resultSet;
		Acompañante acompañante = null;
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(findByDNI);
			statement.setString(1, dni);
			resultSet = statement.executeQuery();

			while (resultSet.next()) {
				acompañante = new Acompañante(resultSet.getString("a.dni"), resultSet.getString("a.nombre"),
						resultSet.getString("a.apellido"), resultSet.getDate("a.fecha_nacimiento").toLocalDate(),
						resultSet.getString("a.email"), null);
				acompañante.setId(resultSet.getInt("a.id"));
				Contacto contacto = new Contacto(resultSet.getString("c.telefono1"), null, null, null, null);
				contacto.setId(resultSet.getInt("c.id"));
				acompañante.setContacto(contacto);
				
				return acompañante;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return acompañante;
	}

	@Override
	public List<Acompañante> findByPasaje(Pasaje pasaje) {
		PreparedStatement statement;
		ResultSet resultSet;
		List<Acompañante> lista_acompañantes = new ArrayList<>();
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(findByPasaje);
			statement.setInt(1, pasaje.getId());

			resultSet = statement.executeQuery();

			while (resultSet.next()) {
				Acompañante acompañante = findById(resultSet.getInt("id_acompañante"));
				lista_acompañantes.add(acompañante);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return lista_acompañantes;
	}
}
