package daoImplementacion;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import conexion.Conexion;
import dao.MailDAO;
import dominio.Mail;

public class MailDAOSQL implements MailDAO {
    private static String insert = "INSERT INTO Mail(direccion, contraseña, remitente, firma) values (?,?,?,?)";
    private static String update = "UPDATE Mail SET direccion = ?, remitente = ?, firma = ? WHERE id = ?";
    private static String updateContraseña = "UPDATE Mail SET contraseña = ? WHERE id = ?";
    private final String findById = "SELECT * FROM Mail WHERE id = ?";
    private final String findAll = "SELECT * FROM Mail";

    @Override
    public boolean insert(Mail mail) {
        PreparedStatement statement;
        Conexion conexion = Conexion.getConexion();
        try {
            statement = conexion.getSQLConexion().prepareStatement(insert, Statement.RETURN_GENERATED_KEYS);
            
            statement.setString(1, mail.getDireccion());
            statement.setString(2, mail.getContraseña());
            statement.setString(3, mail.getRemitente());
            statement.setString(4, mail.getFirma());
            int affectedRows = statement.executeUpdate();

            if (affectedRows == 0)
                throw new Exception("Creacion de mail fallida, no rows affected.");

            else {
                try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
                    if (generatedKeys.next()) {
                        mail.setId(generatedKeys.getInt(1));
                        return true;
                    } else
                        throw new Exception("Creacion de mail fallida, no se obtuvo id");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean update(Mail mail) {
        PreparedStatement statement;
        Conexion conexion = Conexion.getConexion();

        try {
            statement = conexion.getSQLConexion().prepareStatement(update);
            statement.setString(1, mail.getDireccion());
            statement.setString(2, mail.getRemitente());
            statement.setString(3, mail.getFirma());

            statement.setInt(4, mail.getId());

            if (statement.executeUpdate() > 0) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
    
    @Override
    public boolean updateContraseña(Mail mail) {
        PreparedStatement statement;
        Conexion conexion = Conexion.getConexion();

        try {
            statement = conexion.getSQLConexion().prepareStatement(updateContraseña);
            
            statement.setString(1, mail.getContraseña());
            statement.setInt(2, mail.getId());

            if (statement.executeUpdate() > 0) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public Mail findById(int id) {
        PreparedStatement statement;
        ResultSet resultSet;
        Mail mail = null;
        Conexion conexion = Conexion.getConexion();
        try {
            statement = conexion.getSQLConexion().prepareStatement(findById);
            statement.setInt(1, id);
            resultSet = statement.executeQuery();

            if (resultSet.next()) {
                String direccion = resultSet.getString("direccion");
                String contraseña = resultSet.getString("contraseña");
                String remitente = resultSet.getString("remitente");
                String firma = resultSet.getString("firma");

                mail = new Mail("","","","");
                mail.setDireccion(direccion);
                mail.setContraseña(contraseña);
                mail.setRemitente(remitente);
                mail.setFirma(firma);
                mail.setId(id);
                return mail;
            }
        } catch (
                Exception e) {
            e.printStackTrace();
        }
        return mail;
    }

    @Override
    public List<Mail> findAll() {
        PreparedStatement statement;
        ResultSet resultSet;
        List<Mail> mails = new ArrayList<>();
        Conexion conexion = Conexion.getConexion();
        try {
            statement = conexion.getSQLConexion().prepareStatement(findAll);
            resultSet = statement.executeQuery();

            while(resultSet.next()){
                Mail mailNuevo = new Mail(resultSet.getString("direccion"),
                        resultSet.getString("contraseña"),
                        resultSet.getString("remitente"),
                        resultSet.getString("firma"));

                mailNuevo.setId(resultSet.getInt("id"));
                mails.add(mailNuevo);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mails;
    }

}
