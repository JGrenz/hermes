package daoImplementacion;

import conexion.Conexion;
import dao.PagoDAO;
import dominio.Efectivo;
import dominio.Pago;
import dominio.PagoPunto;
import dominio.Tarjeta;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

public class PagoDAOSQL implements PagoDAO {
    public final String insert = "INSERT INTO pago(FORMA, TIPO_TARJ, MARCA_TARJ, NUMERO_TARJ, VENCIMIENTO_TARJ) VALUES (?,?,?,?,?)";
    public final String findById = "SELECT * FROM pago WHERE id = ?";

    @Override
    public int insert(Pago pago) {
        PreparedStatement statement;
        Conexion conexion = Conexion.getConexion();
        try {
            statement = conexion.getSQLConexion().prepareStatement(insert, Statement.RETURN_GENERATED_KEYS);
            if (pago instanceof Tarjeta) {
                Tarjeta tarjeta = (Tarjeta) pago;

                java.sql.Date fechaVencimiento = java.sql.Date.valueOf(tarjeta.getFechaVencimiento());

                statement.setString(1, "Tarjeta");
                statement.setString(2, tarjeta.getTipo());
                statement.setString(3, tarjeta.getEmpresa());
                statement.setString(4, tarjeta.getNumero());
                statement.setDate(5, fechaVencimiento);
            } else if (pago instanceof Efectivo) {
                statement.setString(1, "Efectivo");
                statement.setString(2, null);
                statement.setString(3, null);
                statement.setString(4, null);
                statement.setDate(5, null);

            } else {
            	  statement.setString(1, "Puntaje");
                  statement.setString(2, null);
                  statement.setString(3, null);
                  statement.setString(4, null);
                  statement.setDate(5, null);
            }
            int affectedRows = statement.executeUpdate();

            if (affectedRows == 0)
                throw new Exception("Creacion de pago fallida, no rows affected.");

            else {
                try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
                    if (generatedKeys.next()) {
                        pago.setId(generatedKeys.getInt(1));
                        return pago.getId();
                    } else
                        throw new Exception("Creacion de pago fallida, no se obtuvo id");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

	@Override
	public Pago findById(int id) {
		PreparedStatement statement;
		ResultSet resultSet;
		Pago pago = null;
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(findById);
			statement.setInt(1, id);
			resultSet = statement.executeQuery();

			if (resultSet.next()) {
				if(resultSet.getString("forma").equals("Efectivo")) {
					pago = new Efectivo();
					pago.setId(id);
				}
				else if(resultSet.getString("forma").equals("Tarjeta")) {
					pago = new Tarjeta(resultSet.getDate("vencimiento_tarj").toLocalDate(),
							resultSet.getString("numero_tarj"),
							null,
							resultSet.getString("tipo_tarj"),
							resultSet.getString("marca_tarj")
							);
					pago.setId(id);
				}
				else {
					pago = new PagoPunto();
					pago.setId(id);
				}
				return pago;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return pago;
	}
    
    

}
