package daoImplementacion;

import conexion.Conexion;
import dao.UbicacionDAO;
import dominio.Ubicacion;

import java.lang.reflect.Type;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class UbicacionDAOSQL implements UbicacionDAO {
	private final String insert = "INSERT INTO ubicacion(ciudad, provincia, pais, continente, detalle, tags) VALUES (?,?,?,?,?,?)";

	private final String update = "UPDATE ubicacion SET ciudad = ?, provincia = ?, pais = ?, continente = ?, detalle = ?, tags = ?, estado = ?  WHERE id = ?";
	private final String updateEstado = "UPDATE ubicacion SET estado = ?  WHERE id = ?";

	private final String findAll = "SELECT * FROM ubicacion";
	private final String findById = "SELECT * FROM ubicacion WHERE id = ?";
	private final String findByCiudad = "SELECT * FROM ubicacion WHERE ciudad = ?";
	private final String findByProvincia = "SELECT * FROM ubicacion WHERE provincia = ?";
	private final String findByPais = "SELECT * FROM ubicacion WHERE pais = ?";
	private final String findByContinente = "SELECT * FROM ubicacion WHERE continente = ?";
	private final String findByTags = "SELECT * FROM ubicacion WHERE JSON_CONTAINS(tags, ? ) > 0";
	private final String findByEstado = "SELECT * FROM ubicacion WHERE estado = ?";
	private final String findByEnUso = "SELECT COUNT(*) FROM viaje WHERE (fk_origen = ? or fk_destino = ?) and estado = 'ABIERTO'";

	@Override
	public boolean insert(Ubicacion ubicacion) {
		PreparedStatement statement;
		Conexion conexion = Conexion.getConexion();

		try {
			statement = conexion.getSQLConexion().prepareStatement(insert, Statement.RETURN_GENERATED_KEYS);
			statement.setString(1, ubicacion.getCiudad());
			statement.setString(2, ubicacion.getProvincia());
			statement.setString(3, ubicacion.getPais());
			statement.setString(4, ubicacion.getContinente());
			statement.setString(5, ubicacion.getDetalle());

			Gson gson = new Gson();
			String Json = gson.toJson(ubicacion.getTags());
			statement.setString(6, Json);

			int affectedRows = statement.executeUpdate();

			if (affectedRows == 0)
				throw new Exception("Creacion de UBICACIÓN fallida, no rows affected");
			else {
				try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
					if (generatedKeys.next()) {
						ubicacion.setId(generatedKeys.getInt(1));
						return true;
					} else
						throw new Exception("Creacion de UBICACIÓN fallida, no se obtuvo id");
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean update(Ubicacion ubicacion) {
		PreparedStatement statement;
		Conexion conexion = Conexion.getConexion();

		try {
			statement = conexion.getSQLConexion().prepareStatement(update);
			statement.setString(1, ubicacion.getCiudad());
			statement.setString(2, ubicacion.getProvincia());
			statement.setString(3, ubicacion.getPais());
			statement.setString(4, ubicacion.getContinente());
			statement.setString(5, ubicacion.getDetalle());

			Gson gson = new Gson();
			String Json = gson.toJson(ubicacion.getTags());
			statement.setString(6, Json);

			statement.setString(7, ubicacion.getEstado());
			statement.setInt(8, ubicacion.getId());

			if (statement.executeUpdate() > 0) {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public List<Ubicacion> findAll() {
		PreparedStatement statement;
		ResultSet resultSet;
		List<Ubicacion> lista_ubicaciones = new ArrayList<>();
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(findAll);
			resultSet = statement.executeQuery();
			Gson gson = new Gson();

			while (resultSet.next()) {
				Ubicacion ubicacion = new Ubicacion(resultSet.getString("ciudad"), resultSet.getString("provincia"),
						resultSet.getString("pais"), resultSet.getString("continente"), resultSet.getString("detalle"));
				ubicacion.setId(resultSet.getInt("id"));

				String Json = resultSet.getString("tags");
				Type listType = new TypeToken<List<String>>() {
				}.getType();
				ubicacion.setTags(gson.fromJson(Json, listType));

				ubicacion.setEstado(resultSet.getString("estado"));

				lista_ubicaciones.add(ubicacion);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lista_ubicaciones;
	}

	@Override
	public Ubicacion findById(int id) {
		PreparedStatement statement;
		ResultSet resultSet;
		Ubicacion ubicacion = null;
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(findById);
			statement.setInt(1, id);
			resultSet = statement.executeQuery();

			if (resultSet.next()) {
				Gson gson = new Gson();

				ubicacion = new Ubicacion(resultSet.getString("ciudad"), resultSet.getString("provincia"),
						resultSet.getString("pais"), resultSet.getString("continente"), resultSet.getString("detalle"));
				ubicacion.setId(id);

				String Json = resultSet.getString("tags");
				Type listType = new TypeToken<List<String>>() {
				}.getType();
				ubicacion.setTags(gson.fromJson(Json, listType));

				ubicacion.setEstado(resultSet.getString("estado"));

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ubicacion;
	}

	@Override
	public List<Ubicacion> findByCiudad(String ciudad) {
		PreparedStatement statement;
		ResultSet resultSet;
		List<Ubicacion> lista_ubicaciones = new ArrayList<>();
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(findByCiudad);
			statement.setString(1, ciudad);
			resultSet = statement.executeQuery();

			while (resultSet.next()) {
				Gson gson = new Gson();

				Ubicacion ubicacion = new Ubicacion(ciudad, resultSet.getString("provincia"),
						resultSet.getString("pais"), resultSet.getString("continente"), resultSet.getString("detalle"));

				ubicacion.setId(resultSet.getInt("id"));

				String Json = resultSet.getString("tags");
				Type listType = new TypeToken<ArrayList<String>>() {
				}.getType();
				ubicacion.setTags(gson.fromJson(Json, listType));

				ubicacion.setEstado(resultSet.getString("estado"));

				lista_ubicaciones.add(ubicacion);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lista_ubicaciones;
	}

	@Override
	public List<Ubicacion> findByProvincia(String provincia) {
		PreparedStatement statement;
		ResultSet resultSet;
		List<Ubicacion> lista_ubicaciones = new ArrayList<>();
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(findByProvincia);
			statement.setString(1, provincia);
			resultSet = statement.executeQuery();

			while (resultSet.next()) {
				Gson gson = new Gson();

				Ubicacion ubicacion = new Ubicacion(resultSet.getString("ciudad"), provincia,
						resultSet.getString("pais"), resultSet.getString("continente"), resultSet.getString("detalle"));

				ubicacion.setId(resultSet.getInt("id"));

				String Json = resultSet.getString("tags");
				Type listType = new TypeToken<ArrayList<String>>() {
				}.getType();
				ubicacion.setTags(gson.fromJson(Json, listType));

				ubicacion.setEstado(resultSet.getString("estado"));

				lista_ubicaciones.add(ubicacion);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lista_ubicaciones;
	}

	@Override
	public List<Ubicacion> findByPais(String pais) {
		PreparedStatement statement;
		ResultSet resultSet;
		List<Ubicacion> lista_ubicaciones = new ArrayList<>();
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(findByPais);
			statement.setString(1, pais);
			resultSet = statement.executeQuery();

			while (resultSet.next()) {
				Gson gson = new Gson();

				Ubicacion ubicacion = new Ubicacion(resultSet.getString("ciudad"), resultSet.getString("provincia"),
						pais, resultSet.getString("continente"), resultSet.getString("detalle"));

				ubicacion.setId(resultSet.getInt("id"));

				String Json = resultSet.getString("tags");
				Type listType = new TypeToken<ArrayList<String>>() {
				}.getType();
				ubicacion.setTags(gson.fromJson(Json, listType));

				ubicacion.setEstado(resultSet.getString("estado"));

				lista_ubicaciones.add(ubicacion);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lista_ubicaciones;
	}

	@Override
	public List<Ubicacion> findByContinente(String continente) {
		PreparedStatement statement;
		ResultSet resultSet;
		List<Ubicacion> lista_ubicaciones = new ArrayList<>();
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(findByContinente);
			statement.setString(1, continente);
			resultSet = statement.executeQuery();

			while (resultSet.next()) {
				Gson gson = new Gson();

				Ubicacion ubicacion = new Ubicacion(resultSet.getString("ciudad"), resultSet.getString("provincia"),
						resultSet.getString("pais"), continente, resultSet.getString("detalle"));
				ubicacion.setId(resultSet.getInt("id"));

				String Json = resultSet.getString("tags");
				Type listType = new TypeToken<ArrayList<String>>() {
				}.getType();
				ubicacion.setTags(gson.fromJson(Json, listType));

				ubicacion.setEstado(resultSet.getString("estado"));

				lista_ubicaciones.add(ubicacion);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lista_ubicaciones;
	}

	@Override
	public List<Ubicacion> findByTags(List<String> tags) {
		PreparedStatement statement;
		ResultSet resultSet;
		List<Ubicacion> lista_ubicaciones = new ArrayList<>();
		Gson gson = new Gson();
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(findByTags);

			statement.setString(1, gson.toJson(tags));

			resultSet = statement.executeQuery();

			while (resultSet.next()) {

				Ubicacion ubicacion = new Ubicacion(resultSet.getString("ciudad"), resultSet.getString("provincia"),
						resultSet.getString("pais"), resultSet.getString("continente"), resultSet.getString("detalle"));

				ubicacion.setId(resultSet.getInt("id"));

				String Json = resultSet.getString("tags");
				Type listType = new TypeToken<ArrayList<String>>() {
				}.getType();
				ubicacion.setTags(gson.fromJson(Json, listType));

				ubicacion.setEstado(resultSet.getString("estado"));

				lista_ubicaciones.add(ubicacion);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lista_ubicaciones;
	}

	@Override
	public List<Ubicacion> findByEstado(String estado) {
		PreparedStatement statement;
		ResultSet resultSet;
		List<Ubicacion> lista_ubicaciones = new ArrayList<>();
		Gson gson = new Gson();
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(findByEstado);

			statement.setString(1, estado);

			resultSet = statement.executeQuery();

			while (resultSet.next()) {

				Ubicacion ubicacion = new Ubicacion(resultSet.getString("ciudad"), resultSet.getString("provincia"),
						resultSet.getString("pais"), resultSet.getString("continente"), resultSet.getString("detalle"));

				ubicacion.setId(resultSet.getInt("id"));

				String Json = resultSet.getString("tags");
				Type listType = new TypeToken<ArrayList<String>>() {
				}.getType();
				ubicacion.setTags(gson.fromJson(Json, listType));

				ubicacion.setEstado(estado);

				lista_ubicaciones.add(ubicacion);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lista_ubicaciones;
	}

	@Override
	public boolean enUso(Ubicacion ubicacion) {
		PreparedStatement statement;
		ResultSet resultSet;
		Conexion conexion = Conexion.getConexion();

		try {
			statement = conexion.getSQLConexion().prepareStatement(findByEnUso);
			statement.setInt(1, ubicacion.getId());
			statement.setInt(2, ubicacion.getId());
			resultSet = statement.executeQuery();

			if (resultSet.next()) {
				return resultSet.getInt(1) > 0;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean update(Ubicacion ubicacion, String estado) {
		PreparedStatement statement;
		Conexion conexion = Conexion.getConexion();

		try {
			statement = conexion.getSQLConexion().prepareStatement(updateEstado);
			statement.setString(1, estado);
			statement.setInt(2, ubicacion.getId());

			if (statement.executeUpdate() > 0) {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
}