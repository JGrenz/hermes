package daoImplementacion;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import conexion.Conexion;
import dao.MedioTransporteDAO;
import dominio.MedioTransporte;

public class MedioTransporteDAOSQL implements MedioTransporteDAO {
	private final String insert = "INSERT INTO medio_transporte(nombre) values (?)";
	private  final String update = "UPDATE medio_transporte SET nombre = ?, estado = ? WHERE id = ?";
	private final String findById = "SELECT * FROM medio_transporte WHERE id = ?";
	private  final String findByName = "SELECT * FROM medio_transporte WHERE nombre = ?";
	private final String findAll = "SELECT * FROM medio_transporte";
	private final String findByEnUso = "SELECT COUNT(*) FROM viaje WHERE (fk_medio_transporte = ?) and estado = 'ABIERTO'";
	
	@Override
	public boolean insert(MedioTransporte medioTransporte) {
		PreparedStatement statement;
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(insert, Statement.RETURN_GENERATED_KEYS);
			statement.setString(1,medioTransporte.getNombre());
			int affectedRows = statement.executeUpdate();

			if (affectedRows == 0)
				throw new Exception("Creacion de medioTransporte fallida, no rows affected.");

			 else {
				try(ResultSet generatedKeys = statement.getGeneratedKeys()){
					if (generatedKeys.next()) {
						medioTransporte.setId(generatedKeys.getInt(1));
						return true;
					} else
						throw new Exception("Creacion de medio de transporte fallida, no se obtuvo id");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean update(MedioTransporte transporte_a_editar){
		PreparedStatement statement;
		Conexion conexion = Conexion.getConexion();

		try {
			statement = conexion.getSQLConexion().prepareStatement(update);
			statement.setString(1, transporte_a_editar.getNombre());
			statement.setString(2, transporte_a_editar.getEstado());
			statement.setInt(3, transporte_a_editar.getId());

			if(statement.executeUpdate() > 0){
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public MedioTransporte findById(int id) {
		PreparedStatement statement;
		ResultSet resultSet;
		MedioTransporte medioTransporte = null;
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(findById);
			statement.setInt(1, id);
			resultSet = statement.executeQuery();

			if (resultSet.next()) {
				medioTransporte = new MedioTransporte(resultSet.getString("nombre"));
				medioTransporte.setId(id);
				medioTransporte.setEstado(resultSet.getString("estado"));
				return medioTransporte;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return medioTransporte;
	}

	@Override
	public MedioTransporte findByName(String nombre) {
		PreparedStatement statement;
		ResultSet resultSet;
		MedioTransporte medioTransporte = null;
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(findByName);
			statement.setString(1, nombre);
			resultSet = statement.executeQuery();
			if (resultSet.next()) {
				medioTransporte = new MedioTransporte(nombre);
				medioTransporte.setId(resultSet.getInt("id"));
				medioTransporte.setEstado(resultSet.getString("estado"));
				return medioTransporte;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return medioTransporte;
	}

	@Override
	public List<MedioTransporte> findAll() {
		PreparedStatement statement;
		ResultSet resultSet;
		List<MedioTransporte> mediosTransportes = new ArrayList<>();
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(findAll);
			resultSet = statement.executeQuery();

			while(resultSet.next()){
				MedioTransporte nuevo_medio = new MedioTransporte(resultSet.getString("nombre"));
				nuevo_medio.setId(resultSet.getInt("id"));
				nuevo_medio.setEstado(resultSet.getString("estado"));
				mediosTransportes.add(nuevo_medio);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mediosTransportes;
	}

	@Override
	public boolean enUso(MedioTransporte medioTranpsorte) {
		PreparedStatement statement;
		ResultSet resultSet;
		Conexion conexion = Conexion.getConexion();

		try {
			statement = conexion.getSQLConexion().prepareStatement(findByEnUso);
			statement.setInt(1, medioTranpsorte.getId());
			resultSet = statement.executeQuery();

			if (resultSet.next()) {
				return resultSet.getInt(1) > 0;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean update(MedioTransporte medioTransporte, String estado) {
		PreparedStatement statement;
		Conexion conexion = Conexion.getConexion();

		try {
			statement = conexion.getSQLConexion().prepareStatement(update);
			statement.setString(1, medioTransporte.getNombre());
			statement.setString(2, estado);
			statement.setInt(3, medioTransporte.getId());

			if(statement.executeUpdate() > 0){
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

}
