package daoImplementacion;

import conexion.Conexion;
import dao.ViajeDAO;
import dominio.Oferta;
import dominio.Viaje;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

public class ViajeDAOSQL implements ViajeDAO {
	private final String insert = "INSERT INTO viaje(fecha_creacion," + " codigo_referencia," + // 1
			"fk_origen, " + // 2
			"fk_destino," + // 3
			"fecha_salida, " + // 4
			"horario_salida," + // 5
			"horas_de_viaje," + // 6
			"capacidad_ascientos," + // 7
			"fk_medio_transporte," + // 8
			"precio," + // 9
			"impuesto," + // 10
			"actualizado)" + // 11
			" VALUES(NOW(),?,?,?,?,?,?,?,?,?,?,?)";
	private final String update = "UPDATE viaje SET fk_origen = ? , fk_destino = ? , fecha_salida = ?, horario_salida = ?, horas_de_viaje = ? , "
			+ "capacidad_ascientos = ? , fk_medio_transporte = ? , precio = ? , impuesto = ?, estado = ? WHERE id = ?";
	private final String findAll = "SELECT * FROM viaje";
	private final String findByEstado = "SELECT * FROM viaje WHERE estado = ?";
	private final String darDeBaja = "UPDATE viaje SET estado ='CANCELADO' WHERE id = ?";
	private static final String findById = "SELECT * FROM viaje WHERE id = ?";
	private static final String findByCodigoReferencia = "SELECT * FROM viaje WHERE codigo_referencia = ?";
	private static final String findByOferta = "SELECT * FROM viaje WHERE fk_descuento = ?";
	private static final String updateOferta = "UPDATE viaje SET fk_descuento = ? WHERE id = ?";

	private UbicacionDAOSQL ubicacionSQL = new UbicacionDAOSQL();
	private MedioTransporteDAOSQL medioTransporteSQL = new MedioTransporteDAOSQL();
	private OfertaDAOSQL ofertaSQL = new OfertaDAOSQL();

	@Override
	public boolean insert(Viaje viaje) {
		PreparedStatement statement;
		Conexion conexion = Conexion.getConexion();

		try {
			java.sql.Date fechaSalida = java.sql.Date.valueOf(viaje.getFechaSalida());
			java.sql.Time horaSalida = java.sql.Time.valueOf(viaje.getHoraSalida());

			statement = conexion.getSQLConexion().prepareStatement(insert, Statement.RETURN_GENERATED_KEYS);
			statement.setString(1, viaje.getCodigoReferencia());
			statement.setInt(2, viaje.getUbicacionOrigen().getId());
			statement.setInt(3, viaje.getUbicacionDestino().getId());
			statement.setDate(4, fechaSalida);
			statement.setTime(5, horaSalida);
			statement.setString(6, viaje.getHorasDeViaje().toString());
			statement.setInt(7, viaje.getCapacidad());
			statement.setInt(8, viaje.getMedioTransporte().getId());
			statement.setBigDecimal(9, viaje.getPrecio());
			statement.setBigDecimal(10, viaje.getImpuesto());
			statement.setBoolean(11, viaje.fueConfirmado());

			int affectedRows = statement.executeUpdate();

			if (affectedRows == 0)
				throw new Exception("Creacion de VIAJE fallida, no rows affected");
			else {
				try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
					if (generatedKeys.next()) {
						viaje.setId(generatedKeys.getInt(1));
						return true;
					} else
						throw new Exception("Creacion de VIAJE fallida, no se obtuvo id");
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean update(Viaje viaje) {
		PreparedStatement statement;
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(update);

			java.sql.Date fechaSalida = java.sql.Date.valueOf(viaje.getFechaSalida());
			java.sql.Time horaSalida = java.sql.Time.valueOf(viaje.getHoraSalida());

			statement.setInt(1, viaje.getUbicacionOrigen().getId());
			statement.setInt(2, viaje.getUbicacionDestino().getId());
			statement.setDate(3, fechaSalida);
			statement.setTime(4, horaSalida);
			statement.setString(5, viaje.getHorasDeViaje().toString());
			statement.setInt(6, viaje.getCapacidad());
			statement.setInt(7, viaje.getMedioTransporte().getId());
			statement.setBigDecimal(8, viaje.getPrecio());
			statement.setBigDecimal(9, viaje.getImpuesto());
			statement.setString(10, viaje.getEstadoViaje());
			statement.setInt(11, viaje.getId());

			if (statement.executeUpdate() > 0) {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean darDeBaja(Viaje viaje) {
		PreparedStatement statement;
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(darDeBaja);
			statement.setInt(1, viaje.getId());

			if (statement.executeUpdate() > 0) {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public List<Viaje> findAll() {
		PreparedStatement statement;
		ResultSet resultSet;
		List<Viaje> lista_viajes = new ArrayList<>();
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(findAll);
			resultSet = statement.executeQuery();

			while (resultSet.next()) {

				LocalDate fechaSalida = resultSet.getDate("fecha_salida").toLocalDate();
				LocalTime horaSalida = resultSet.getTime("horario_salida").toLocalTime();
				Duration horasDeViaje = Duration.parse(resultSet.getString("horas_de_viaje"));

				Viaje viaje = new Viaje(ubicacionSQL.findById(resultSet.getInt("fk_origen")),
						ubicacionSQL.findById(resultSet.getInt("fk_destino")), fechaSalida, horaSalida, horasDeViaje,
						resultSet.getInt("capacidad_ascientos"),
						medioTransporteSQL.findById(resultSet.getInt("fk_medio_transporte")),
						resultSet.getBigDecimal("precio"), resultSet.getBigDecimal("impuesto"));

				viaje.setId(resultSet.getInt("id"));
				viaje.setEstado(resultSet.getString("estado"));
				viaje.setCodigoReferencia(resultSet.getString("codigo_referencia"));

				if (resultSet.getBoolean("actualizado")){
					viaje.confirmar();
				}

				lista_viajes.add(viaje);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return lista_viajes;
	}

	@Override
	public Viaje findById(int id) {
		Viaje viaje = null;
		PreparedStatement statement;
		ResultSet resultSet;
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(findById);
			statement.setInt(1, id);

			resultSet = statement.executeQuery();
			if (resultSet.next()) {

				LocalDate fechaSalida = resultSet.getDate("fecha_salida").toLocalDate();
				LocalTime horaSalida = resultSet.getTime("horario_salida").toLocalTime();
				Duration horasDeViaje = Duration.parse(resultSet.getString("horas_de_viaje"));

				viaje = new Viaje(ubicacionSQL.findById(resultSet.getInt("fk_origen")),
						ubicacionSQL.findById(resultSet.getInt("fk_destino")), fechaSalida, horaSalida, horasDeViaje,
						resultSet.getInt("capacidad_ascientos"),
						medioTransporteSQL.findById(resultSet.getInt("fk_medio_transporte")),
						resultSet.getBigDecimal("precio"), resultSet.getBigDecimal("impuesto"));

				viaje.setId(id);
				viaje.setEstado(resultSet.getString("estado"));
				viaje.setCodigoReferencia(resultSet.getString("codigo_referencia"));

				if (resultSet.getBoolean("actualizado")){
					viaje.confirmar();
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return viaje;
	}

	@Override
	public Viaje findByCOdigoReferencia(String codigoReferencia) {

		Viaje viaje = null;
		PreparedStatement statement;
		ResultSet resultSet;
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(findByCodigoReferencia);
			statement.setString(1, codigoReferencia);

			resultSet = statement.executeQuery();
			if (resultSet.next()) {

				LocalDate fechaSalida = resultSet.getDate("fecha_salida").toLocalDate();
				LocalTime horaSalida = resultSet.getTime("horario_salida").toLocalTime();
				Duration horasDeViaje = Duration.parse(resultSet.getString("horas_de_viaje"));

				viaje = new Viaje(ubicacionSQL.findById(resultSet.getInt("fk_origen")),
						ubicacionSQL.findById(resultSet.getInt("fk_destino")), fechaSalida, horaSalida, horasDeViaje,
						resultSet.getInt("capacidad_ascientos"),
						medioTransporteSQL.findById(resultSet.getInt("fk_medio_transporte")),
						resultSet.getBigDecimal("precio"), resultSet.getBigDecimal("impuesto"));

				viaje.setId(resultSet.getInt("id"));
				viaje.setEstado(resultSet.getString("estado"));
				viaje.setCodigoReferencia(codigoReferencia);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return viaje;
	}

	@Override
	public List<Viaje> findByEstado(String estado) {
		List<Viaje> lista_viajes = new ArrayList<>();
		PreparedStatement statement;
		ResultSet resultSet;
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(findByEstado);
			statement.setString(1, estado);

			resultSet = statement.executeQuery();
			while (resultSet.next()) {

				LocalDate fechaSalida = resultSet.getDate("fecha_salida").toLocalDate();
				LocalTime horaSalida = resultSet.getTime("horario_salida").toLocalTime();
				Duration horasDeViaje = Duration.parse(resultSet.getString("horas_de_viaje"));

				Viaje viaje = new Viaje(ubicacionSQL.findById(resultSet.getInt("fk_origen")),
						ubicacionSQL.findById(resultSet.getInt("fk_destino")), fechaSalida, horaSalida, horasDeViaje,
						resultSet.getInt("capacidad_ascientos"),
						medioTransporteSQL.findById(resultSet.getInt("fk_medio_transporte")),
						resultSet.getBigDecimal("precio"), resultSet.getBigDecimal("impuesto"));

				viaje.setId(resultSet.getInt("id"));
				viaje.setEstado(estado);
				viaje.setCodigoReferencia(resultSet.getString("codigo_referencia"));

				lista_viajes.add(viaje);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return lista_viajes;
	}
	
	public List<Viaje> findByDisponibles(){
		PasajeDAOSQL pasajedSQL = new PasajeDAOSQL();
		PreparedStatement statement;
		ResultSet resultSet;
		List<Viaje> lista_viajes = new ArrayList<>();
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(findByEstado);
			statement.setString(1, "ABIERTO");
			resultSet = statement.executeQuery();

			while (resultSet.next()) {

				LocalDate fechaSalida = resultSet.getDate("fecha_salida").toLocalDate();
				LocalTime horaSalida = resultSet.getTime("horario_salida").toLocalTime();
				Duration horasDeViaje = Duration.parse(resultSet.getString("horas_de_viaje"));

				Viaje viaje = new Viaje(ubicacionSQL.findById(resultSet.getInt("fk_origen")),
						ubicacionSQL.findById(resultSet.getInt("fk_destino")), fechaSalida, horaSalida, horasDeViaje,
						resultSet.getInt("capacidad_ascientos"),
						medioTransporteSQL.findById(resultSet.getInt("fk_medio_transporte")),
						resultSet.getBigDecimal("precio"), resultSet.getBigDecimal("impuesto"));

				viaje.setId(resultSet.getInt("id"));
				viaje.setEstado(resultSet.getString("estado"));
				viaje.setCodigoReferencia(resultSet.getString("codigo_referencia"));
				viaje.setOferta(ofertaSQL.findById(resultSet.getInt("fk_descuento")));
				
				int cuposUsados = pasajedSQL.countByDisponibles(viaje);
				if( cuposUsados < viaje.getCapacidad()) {
					viaje.setCapacidad(viaje.getCapacidad()-cuposUsados);
					lista_viajes.add(viaje);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return lista_viajes;
	}
	
	public List<Viaje> findByOferta(Oferta oferta){
		PreparedStatement statement;
		ResultSet resultSet;
		List<Viaje> lista_viajes = new ArrayList<>();
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(findByOferta);
			statement.setInt(1, oferta.getId());
			resultSet = statement.executeQuery();

			while (resultSet.next()) {

				LocalDate fechaSalida = resultSet.getDate("fecha_salida").toLocalDate();
				LocalTime horaSalida = resultSet.getTime("horario_salida").toLocalTime();
				Duration horasDeViaje = Duration.parse(resultSet.getString("horas_de_viaje"));

				Viaje viaje = new Viaje(ubicacionSQL.findById(resultSet.getInt("fk_origen")),
						ubicacionSQL.findById(resultSet.getInt("fk_destino")), fechaSalida, horaSalida, horasDeViaje,
						resultSet.getInt("capacidad_ascientos"),
						medioTransporteSQL.findById(resultSet.getInt("fk_medio_transporte")),
						resultSet.getBigDecimal("precio"), resultSet.getBigDecimal("impuesto"));

				viaje.setId(resultSet.getInt("id"));
				viaje.setEstado(resultSet.getString("estado"));
				viaje.setCodigoReferencia(resultSet.getString("codigo_referencia"));
				viaje.setOferta(oferta);
				
				lista_viajes.add(viaje);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return lista_viajes;
	}
	
	public boolean updateOferta(Viaje viaje) {
		PreparedStatement statement;
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(updateOferta);
			
			if(viaje.getOferta() != null) {
				statement.setInt(1, viaje.getOferta().getId());	
			} else {
				statement.setNull(1, 0);
			}
			
			statement.setInt(2, viaje.getId());

			if (statement.executeUpdate() > 0) {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

}