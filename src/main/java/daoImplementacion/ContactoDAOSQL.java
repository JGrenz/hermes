package daoImplementacion;

import conexion.Conexion;
import dao.ContactoDAO;
import dominio.Contacto;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ContactoDAOSQL implements ContactoDAO {
    private static final String insert = "INSERT INTO contacto (telefono1, telefono2, telefono3, telefono4, telefono5) values (?,?,?,?,?)";
    private static final String update = "UPDATE contacto set telefono1 = ? , telefono2 = ? , telefono3 = ? , telefono4 = ? , telefono5 = ? WHERE id = ?";

    @Override
    public int insert(Contacto contacto) {
        PreparedStatement statement;
        Conexion conexion = Conexion.getConexion();
        try {
            statement = conexion.getSQLConexion().prepareStatement(insert, Statement.RETURN_GENERATED_KEYS);

            statement.setString(1, contacto.getTelefono1());
            statement.setString(2, contacto.getTelefono2());
            statement.setString(3, contacto.getTelefono3());
            statement.setString(4, contacto.getTelefono4());
            statement.setString(5, contacto.getTelefono5());

            int affectedRows = statement.executeUpdate();

            if (affectedRows == 0)
                throw new Exception("Creacion de CONTACTO fallida, no rows affected");
            else {
                try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
                    if (generatedKeys.next()) {
                        contacto.setId(generatedKeys.getInt(1));
                        return contacto.getId();
                    } else
                        throw new Exception("Creacion de CONTACTO fallida, no se obtuvo id");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public boolean update(Contacto contacto) {
        PreparedStatement statement;
        Conexion conexion = Conexion.getConexion();
        try {
            statement = conexion.getSQLConexion().prepareStatement(update);

            statement.setString(1, contacto.getTelefono1());
            statement.setString(2, contacto.getTelefono2());
            statement.setString(3, contacto.getTelefono3());
            statement.setString(4, contacto.getTelefono4());
            statement.setString(5, contacto.getTelefono5());
            statement.setInt(6, contacto.getId());

            if (statement.executeUpdate() > 0)
                return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
}
