package daoImplementacion;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import conexion.Conexion;
import dao.PuntajeDAO;
import dominio.Puntaje;
import dominio.PuntajeConfig;
import dominio.Usuario;

public class PuntajeDAOSQL implements PuntajeDAO {
	private final String insertPuntaje = "INSERT INTO puntaje(id_operacion, cantidad, fecha_creacion, fecha_vencimiento) VALUES (?,?,NOW(),?)";
	private final String updatePuntajeConfig = "UPDATE puntaje_config SET id_creador = ?, cantidad_precio = ?, cantidad_puntos = ?, valor_unitario = ?, fecha_creacion = NOW(), años_vencimiento = ?, meses_vencimiento = ? WHERE id = ?";
	private final String findByCliente = "SELECT p.id, p.cantidad, p.fecha_vencimiento FROM puntaje p, operacion o "
			+ "WHERE o.id_comprador = ? AND p.id_operacion = o.id AND p.fecha_vencimiento > NOW() AND p.cantidad > 0";
	private final String findPuntajeConfig = "SELECT * FROM puntaje_config";
	private final String restarPuntaje = "UPDATE puntaje SET cantidad = ? WHERE id = ?";
//	private UsuarioDAOSQL usuarioDAOSQL = new UsuarioDAOSQL();

	@Override
	public boolean insertPuntaje(Puntaje puntaje) {
		PreparedStatement statement;
		Conexion conexion = Conexion.getConexion();
		try {
			java.sql.Date fechaVencimiento = java.sql.Date.valueOf(puntaje.getVencimiento());

			statement = conexion.getSQLConexion().prepareStatement(insertPuntaje);
			statement.setInt(1, puntaje.getOperacion().getId());
			statement.setInt(2, puntaje.getCantidad());
			statement.setDate(3, fechaVencimiento);

			int affectedRows = statement.executeUpdate();

			if (affectedRows == 0) {
				throw new Exception("Creacion de puntaje fallida, no rows affected.");
			} else {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public List<Puntaje> buscarPorCliente(Usuario cliente) {
		PreparedStatement statement;
		ResultSet resultSet;
		Conexion conexion = Conexion.getConexion();
		List<Puntaje> lista_puntaje = new ArrayList<Puntaje>();
		try {
			statement = conexion.getSQLConexion().prepareStatement(findByCliente);
			statement.setInt(1, cliente.getId());

			resultSet = statement.executeQuery();

			while (resultSet.next()) {
				Puntaje puntaje = new Puntaje(resultSet.getInt("p.cantidad"),
						resultSet.getDate("p.fecha_vencimiento").toLocalDate(), null);
				puntaje.setId(resultSet.getInt("p.id"));
				lista_puntaje.add(puntaje);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return lista_puntaje;
	}

	public PuntajeConfig datosPuntajeConfig() {
		PreparedStatement statement;
		ResultSet resultSet;
		PuntajeConfig puntajeConfigurado = null;
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(findPuntajeConfig);
			resultSet = statement.executeQuery();

			if (resultSet.next()) {
				puntajeConfigurado = new PuntajeConfig(resultSet.getBigDecimal("cantidad_precio"),
						resultSet.getInt("cantidad_puntos"), resultSet.getBigDecimal("valor_unitario"),
						resultSet.getInt("años_vencimiento"), resultSet.getInt("meses_vencimiento"), null);
				puntajeConfigurado.setId(resultSet.getInt("id"));
//				usuarioDAOSQL.findById(resultSet.getInt("id_creador"));
//				Usuario creador = new Usuario(resultSet.getString("u.dni"),
//						resultSet.getString("u.nombre"),
//						resultSet.getString("u.apellido"),
//						resultSet.getString("u.usuario"),
//						null,
//						resultSet.getString("u.email"),
//						resultSet.getString("u.rol"));
//				puntajeConfigurado.setCreador(creador);
				return puntajeConfigurado;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return puntajeConfigurado;
	}

	@Override
	public boolean updatePuntajeConfig(PuntajeConfig puntajeConfiguracion) {
		PreparedStatement statement;
		Conexion conexion = Conexion.getConexion();
		try {

			statement = conexion.getSQLConexion().prepareStatement(updatePuntajeConfig);

			statement.setInt(1, puntajeConfiguracion.getCreador().getId());
			statement.setBigDecimal(2, puntajeConfiguracion.getValorEnPrecio());
			statement.setInt(3, puntajeConfiguracion.getValorEnPuntos());
			statement.setBigDecimal(4, puntajeConfiguracion.getValorUnitario());
			statement.setInt(5, puntajeConfiguracion.getAñosVencimiento());
			statement.setInt(6, puntajeConfiguracion.getMesesVencimiento());
			statement.setInt(7, puntajeConfiguracion.getId());

			int affectedRows = statement.executeUpdate();

			if (affectedRows == 0) {
				throw new Exception("Update de PuntajeConfig fallida, no rows affected.");
			} else {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	@Override
	public boolean restarPuntaje(Puntaje puntaje) {
		PreparedStatement statement;
		Conexion conexion = Conexion.getConexion();
		try {

			statement = conexion.getSQLConexion().prepareStatement(restarPuntaje);

			statement.setInt(1, puntaje.getCantidad());
			statement.setInt(2, puntaje.getId());

			int affectedRows = statement.executeUpdate();

			if (affectedRows == 0) {
				throw new Exception("Update de Puntaje fallida, no rows affected.");
			} else {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
}
