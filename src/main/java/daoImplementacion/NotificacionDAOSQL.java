package daoImplementacion;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import conexion.Conexion;
import dao.NotificacionDAO;
import dominio.Evento;
import dominio.Notificacion;

public class NotificacionDAOSQL implements NotificacionDAO {
    private String insert = "INSERT INTO notificacion(fecha_creacion,fecha_vencimiento, hora_vencimiento,titulo,contenido,estado, id_evento) VALUES (now(),?,?,?,?,?,?)";
    private String update = "UPDATE notificacion SET fecha_vencimiento = ?, hora_vencimiento = ?, titulo = ?, contenido = ?, estado = ?, id_evento = ? WHERE ID = ?";
    private String findAll = "SELECT * FROM notificacion INNER JOIN evento on notificacion.id_evento = evento.id";
    private String findByFkEvento = "SELECT * FROM notificacion WHERE id_evento = ?";

    private EventoDAOSQL eventoSQL = new EventoDAOSQL();

    @Override
    public boolean insert(Notificacion notificacion) {
        PreparedStatement statement;
        Conexion conexion = Conexion.getConexion();
        try {
            statement = conexion.getSQLConexion().prepareStatement(insert, Statement.RETURN_GENERATED_KEYS);

            Timestamp fechaVencimiento = Timestamp.valueOf(notificacion.getFechaVencimiento());
            java.sql.Time horaVencimeinto = java.sql.Time.valueOf(notificacion.getHoraVencimiento());

            statement.setTimestamp(1, fechaVencimiento);
            statement.setTime(2, horaVencimeinto);
            statement.setString(3, notificacion.getTitulo());
            statement.setString(4, notificacion.getContenido());
            statement.setString(5, notificacion.getEstado());
            statement.setInt(6, notificacion.getEvento().getId());

            int affectedRows = statement.executeUpdate();

            if (affectedRows == 0)
                throw new Exception("Creacion de notificacion fallida, no rows affected.");

            else {
                try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
                    if (generatedKeys.next()) {
                        notificacion.setId(generatedKeys.getInt(1));
                        return true;
                    } else
                        throw new Exception("Creacion de notificacion fallida, no se obtuvo id");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean update(Notificacion notificacion) {
        PreparedStatement statement;
        Conexion conexion = Conexion.getConexion();

        try {
            statement = conexion.getSQLConexion().prepareStatement(update);

            Timestamp fechaVencimiento = Timestamp.valueOf(notificacion.getFechaVencimiento());
            java.sql.Time horaVencimeinto = java.sql.Time.valueOf(notificacion.getHoraVencimiento());

            statement.setTimestamp(1, fechaVencimiento);
            statement.setTime(2, horaVencimeinto);
            statement.setString(3, notificacion.getTitulo());
            statement.setString(4, notificacion.getContenido());
            statement.setString(5, notificacion.getEstado());
            statement.setInt(6, notificacion.getEvento().getId());

            statement.setInt(7,notificacion.getId());

            if(statement.executeUpdate() > 0){
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public Notificacion findByFKEvento(Evento evento) {
        PreparedStatement statement;
        ResultSet resultSet;
        Notificacion notificacion = null;
        Conexion conexion = Conexion.getConexion();
        try {
            statement = conexion.getSQLConexion().prepareStatement(findByFkEvento);
            statement.setInt(1, evento.getId());
            resultSet = statement.executeQuery();

            if (resultSet.next()) {
                LocalDateTime fechaVencimiento = resultSet.getTimestamp("fecha_vencimiento").toLocalDateTime();
                LocalTime horaVencimiento = resultSet.getTime("hora_vencimiento").toLocalTime();
                String titulo = resultSet.getString("titulo");
                String contenido = resultSet.getString("contenido");

                notificacion = new Notificacion(horaVencimiento, titulo, contenido,evento);

                int id = resultSet.getInt("id");
                String estado = resultSet.getString("estado");
                LocalDate fechaCreacion = resultSet.getDate("fecha_creacion").toLocalDate();

                notificacion.setId(id);
                notificacion.setEstado(estado);
                notificacion.setFechaCreacion(fechaCreacion);
                notificacion.setFechaVencimiento(fechaVencimiento);

                return notificacion;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return notificacion;
    }

    @Override
    public List<Notificacion> findAll() {
        PreparedStatement statement;
        ResultSet resultSet;
        List<Notificacion> notificaciones = new ArrayList<>();
        Conexion conexion = Conexion.getConexion();
        try {
            statement = conexion.getSQLConexion().prepareStatement(findAll);
            resultSet = statement.executeQuery();

            while (resultSet.next()) {
                LocalDateTime fechaVencimiento = resultSet.getTimestamp("fecha_vencimiento").toLocalDateTime();
                LocalTime horaVencimiento = resultSet.getTime("hora_vencimiento").toLocalTime();
                String titulo = resultSet.getString("titulo");
                String contenido = resultSet.getString("contenido");

                Evento evento = eventoSQL.findEventoById(resultSet.getInt("id_evento"));
                
                Notificacion notificacion = new Notificacion(horaVencimiento, titulo, contenido,evento);

                int id = resultSet.getInt("id");
                String estado = resultSet.getString("estado");
                LocalDate fechaCreacion = resultSet.getDate("fecha_creacion").toLocalDate();

                notificacion.setId(id);
                notificacion.setEstado(estado);
                notificacion.setFechaCreacion(fechaCreacion);
                notificacion.setFechaVencimiento(fechaVencimiento);

                notificaciones.add(notificacion);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return notificaciones;
    }
}
