package daoImplementacion;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;

import dao.UsuarioDAO;
import conexion.Conexion;
import dominio.Contacto;
import dominio.Local;
import dominio.Usuario;

public class UsuarioDAOSQL implements UsuarioDAO {
    private static final String insert = "INSERT INTO usuario(dni, nombre, apellido, email, usuario, contraseña, id_contacto, fecha_creacion, rol, fecha_nacimiento, id_local) values (?, ?, ?, ?, ?, ?, ?, now(), ?, ?, ?)";
    private static final String update = "UPDATE usuario set dni = ? , nombre = ? , apellido = ? , email = ? , usuario = ? , contraseña = ?, rol = ? , fecha_nacimiento = ?, id_local = ? WHERE id = ?";
    private static final String updateEstado = "UPDATE usuario set estado = ? WHERE id = ?";
    private static final String findAll = "SELECT * FROM usuario u INNER JOIN contacto c ON u.id_contacto = c.id";
    private static final String findById = "SELECT * FROM usuario u INNER JOIN contacto c ON u.id_contacto = c.id WHERE u.id = ?";
    private static final String findByDNI = "SELECT * FROM usuario u INNER JOIN contacto c ON u.id_contacto = c.id WHERE u.DNI = ?";
    private static final String findByNombreUSuario = "SELECT * FROM usuario u INNER JOIN contacto c ON u.id_contacto = c.id WHERE u.usuario = ?";
    private static final String findByNombreYApellido = "SELECT * FROM usuario u INNER JOIN contacto c ON u.id_contacto = c.id WHERE u.nombre = ? and u.apellido = ?";
    private static final String contarUsuario = "SELECT COUNT(*) AS cantidadUsuario FROM usuario WHERE usuario.usuario LIKE ?";
    private ContactoDAOSQL contactoDAO = new ContactoDAOSQL();
    private LocalDAOSQL localDAO = new LocalDAOSQL();

    public boolean insert(Usuario usuario) {
        PreparedStatement statement;
        Conexion conexion = Conexion.getConexion();
        try {
            statement = conexion.getSQLConexion().prepareStatement(insert, Statement.RETURN_GENERATED_KEYS);
            String sha256hex = usuario.getContraseña();
            if(sha256hex.length()<64) {
                sha256hex = DigestUtils.sha256Hex(usuario.getContraseña());
            }

            statement.setString(1, usuario.getDni());
            statement.setString(2, usuario.getNombre());
            statement.setString(3, usuario.getApellido());
            statement.setString(4, usuario.getEmail());
            statement.setString(5, usuario.getUsuario());
            statement.setString(6, sha256hex);
            statement.setString(8, usuario.getRol());
            java.sql.Date fechaNacimiento = java.sql.Date.valueOf(usuario.getNacimiento());
            statement.setDate(9, fechaNacimiento);
            if (usuario.getLocal() == null) {
                statement.setNull(10, Types.INTEGER);
            } else {
                statement.setInt(10, usuario.getLocal().getId());
            }
            conexion.getSQLConexion().setAutoCommit(false);


            int id_contacto = contactoDAO.insert(usuario.getContacto());

            statement.setInt(7, id_contacto);

            int affectedRows = statement.executeUpdate();

            if (affectedRows == 0 && id_contacto == 0) {
                conexion.getSQLConexion().rollback();
                conexion.getSQLConexion().setAutoCommit(true);
                throw new Exception("Creacion de CLIENTE fallida, no rows affected");
            } else {
                try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
                    if (generatedKeys.next()) {
                        usuario.setId(generatedKeys.getInt(1));
                        conexion.getSQLConexion().commit();
                        conexion.getSQLConexion().setAutoCommit(true);
                        return true;
                    } else
                        conexion.getSQLConexion().rollback();
                    conexion.getSQLConexion().setAutoCommit(true);
                    throw new Exception("Creacion de CLIENTE fallida, no se obtuvo id");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            conexion.getSQLConexion().rollback();
            conexion.getSQLConexion().setAutoCommit(true);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean update(Usuario usuario) {
        PreparedStatement statement;
        Conexion conexion = Conexion.getConexion();
        try {
            statement = conexion.getSQLConexion().prepareStatement(update);
            String sha256hex = usuario.getContraseña();
            if(sha256hex.length()<64) {
                sha256hex = DigestUtils.sha256Hex(usuario.getContraseña());
            }

            statement.setString(1, usuario.getDni());
            statement.setString(2, usuario.getNombre());
            statement.setString(3, usuario.getApellido());
            statement.setString(4, usuario.getEmail());
            statement.setString(5, usuario.getUsuario());
            statement.setString(6, sha256hex);
            statement.setString(7, usuario.getRol());
            java.sql.Date fechaNacimiento = java.sql.Date.valueOf(usuario.getNacimiento());
            statement.setDate(8, fechaNacimiento);

            if (usuario.getLocal() == null) {
                statement.setNull(9, Types.INTEGER);
            } else {
                statement.setInt(9, usuario.getLocal().getId());
            }

            statement.setInt(10, usuario.getId());
            conexion.getSQLConexion().setAutoCommit(false);

            boolean contado_modificado = contactoDAO.update(usuario.getContacto());

            if (statement.executeUpdate() > 0 && contado_modificado) {
                conexion.getSQLConexion().commit();
                conexion.getSQLConexion().setAutoCommit(true);
                return true;
            } else {
                conexion.getSQLConexion().rollback();
                conexion.getSQLConexion().setAutoCommit(true);
                throw new Exception("No pudo actualizarse el CLIENTE");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public List<Usuario> findAll() {
        PreparedStatement statement;
        ResultSet resultSet;
        List<Usuario> lista_clientes = new ArrayList<>();
        Conexion conexion = Conexion.getConexion();
        try {
            statement = conexion.getSQLConexion().prepareStatement(findAll);
            resultSet = statement.executeQuery();

            while (resultSet.next()) {
                Local local = localDAO.findById(resultSet.getInt("id_local"));

                Usuario usuarioNuevo = new Usuario(
                        resultSet.getString("dni"),
                        resultSet.getString("nombre"),
                        resultSet.getString("apellido"),
                        resultSet.getString("usuario"),
                        resultSet.getDate("fecha_nacimiento").toLocalDate(),
                        resultSet.getString("email"),
                        resultSet.getString("rol")
                );
                usuarioNuevo.setContraseña(resultSet.getString("contraseña"));
                usuarioNuevo.setId(resultSet.getInt("u.id"));
                usuarioNuevo.setEstado(resultSet.getString("estado"));
                usuarioNuevo.setLocal(local);

                Date fechaCreacionSQL = resultSet.getDate("fecha_creacion");
                usuarioNuevo.setFechaCreacion(fechaCreacionSQL.toLocalDate());

                Contacto contacto = new Contacto(
                        resultSet.getString("telefono1"),
                        resultSet.getString("telefono2"),
                        resultSet.getString("telefono3"),
                        resultSet.getString("telefono4"),
                        resultSet.getString("telefono5")
                );
                contacto.setId(resultSet.getInt("c.id"));

                usuarioNuevo.setContacto(contacto);

                lista_clientes.add(usuarioNuevo);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista_clientes;
    }

    public Usuario findById(int id) {
        PreparedStatement statement;
        ResultSet resultSet;
        Usuario cliente = null;
        Conexion conexion = Conexion.getConexion();
        try {
            statement = conexion.getSQLConexion().prepareStatement(findById);
            statement.setInt(1, id);

            resultSet = statement.executeQuery();

            if (resultSet.next()) {
                Local local = localDAO.findById(resultSet.getInt("id_local"));

                cliente = new Usuario(
                        resultSet.getString("dni"),
                        resultSet.getString("nombre"),
                        resultSet.getString("apellido"),
                        resultSet.getString("usuario"),
                        resultSet.getDate("fecha_nacimiento").toLocalDate(),
                        resultSet.getString("email"),
                        resultSet.getString("rol")
                );
                cliente.setContraseña(resultSet.getString("contraseña"));
                cliente.setId(id);
                cliente.setFechaCreacion(resultSet.getDate("fecha_creacion").toLocalDate());
                cliente.setEstado(resultSet.getString("estado"));
                cliente.setLocal(local);

                Contacto contacto = new Contacto(
                        resultSet.getString("telefono1"),
                        resultSet.getString("telefono2"),
                        resultSet.getString("telefono3"),
                        resultSet.getString("telefono4"),
                        resultSet.getString("telefono5")
                );
                contacto.setId(resultSet.getInt("c.id"));

                cliente.setContacto(contacto);

                return cliente;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cliente;
    }

    @Override
    public Usuario findByDNI(int DNI) {
        PreparedStatement statement;
        ResultSet resultSet;
        Usuario cliente = null;
        Conexion conexion = Conexion.getConexion();
        try {
            statement = conexion.getSQLConexion().prepareStatement(findByDNI);
            statement.setInt(1, DNI);

            resultSet = statement.executeQuery();

            if (resultSet.next()) {
                Local local = localDAO.findById(resultSet.getInt("id_local"));

                cliente = new Usuario(
                        resultSet.getString("dni"),
                        resultSet.getString("nombre"),
                        resultSet.getString("apellido"),
                        resultSet.getString("usuario"),
                        resultSet.getDate("fecha_nacimiento").toLocalDate(),
                        resultSet.getString("email"),
                        resultSet.getString("rol")
                );
                cliente.setContraseña(resultSet.getString("contraseña"));
                cliente.setId(DNI);
                cliente.setFechaCreacion(resultSet.getDate("fecha_creacion").toLocalDate());
                cliente.setEstado(resultSet.getString("estado"));
                cliente.setLocal(local);

                Contacto contacto = new Contacto(
                        resultSet.getString("telefono1"),
                        resultSet.getString("telefono2"),
                        resultSet.getString("telefono3"),
                        resultSet.getString("telefono4"),
                        resultSet.getString("telefono5")
                );
                contacto.setId(resultSet.getInt("c.id"));

                cliente.setContacto(contacto);

                return cliente;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cliente;
    }

    @Override
    public Usuario findByNombreUSuario(String usuario) {
        PreparedStatement statement;
        ResultSet resultSet;
        Usuario usuarioRetorno = null;
        Conexion conexion = Conexion.getConexion();
        try {
            statement = conexion.getSQLConexion().prepareStatement(findByNombreUSuario);
            statement.setString(1, usuario);

            resultSet = statement.executeQuery();

            if (resultSet.next()) {
                Local local = localDAO.findById(resultSet.getInt("id_local"));

                usuarioRetorno = new Usuario(
                        resultSet.getString("dni"),
                        resultSet.getString("nombre"),
                        resultSet.getString("apellido"),
                        resultSet.getString("usuario"),
                        resultSet.getDate("fecha_nacimiento").toLocalDate(),
                        resultSet.getString("email"),
                        resultSet.getString("rol")
                );
                usuarioRetorno.setContraseña(resultSet.getString("contraseña"));
                usuarioRetorno.setId(resultSet.getInt("id"));
                usuarioRetorno.setFechaCreacion(resultSet.getDate("fecha_creacion").toLocalDate());
                usuarioRetorno.setEstado(resultSet.getString("estado"));
                usuarioRetorno.setLocal(local);

                Contacto contacto = new Contacto(
                        resultSet.getString("telefono1"),
                        resultSet.getString("telefono2"),
                        resultSet.getString("telefono3"),
                        resultSet.getString("telefono4"),
                        resultSet.getString("telefono5")
                );
                contacto.setId(resultSet.getInt("c.id"));

                usuarioRetorno.setContacto(contacto);

                return usuarioRetorno;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return usuarioRetorno;
    }

    @Override
    public List<Usuario> findByNombreYApellido(String nombre, String apellido) {
        PreparedStatement statement;
        ResultSet resultSet;
        List<Usuario> lista_clientes = new ArrayList<>();
        Conexion conexion = Conexion.getConexion();
        try {
            statement = conexion.getSQLConexion().prepareStatement(findByNombreYApellido);
            statement.setString(1, nombre);
            statement.setString(2, apellido);

            resultSet = statement.executeQuery();

            while (resultSet.next()) {
                Local local = localDAO.findById(resultSet.getInt("id_local"));

                Usuario usuarioNuevo = new Usuario(
                        resultSet.getString("dni"),
                        nombre,
                        apellido,
                        resultSet.getString("usuario"),
                        resultSet.getDate("fecha_nacimiento").toLocalDate(),
                        resultSet.getString("email"),
                        resultSet.getString("rol")
                );
                usuarioNuevo.setContraseña(resultSet.getString("contraseña"));
                usuarioNuevo.setId(resultSet.getInt("u.id"));
                usuarioNuevo.setEstado(resultSet.getString("estado"));
                usuarioNuevo.setLocal(local);

                Date fechaCreacionSQL = resultSet.getDate("fecha_creacion");
                usuarioNuevo.setFechaCreacion(fechaCreacionSQL.toLocalDate());

                Contacto contacto = new Contacto(
                        resultSet.getString("telefono1"),
                        resultSet.getString("telefono2"),
                        resultSet.getString("telefono3"),
                        resultSet.getString("telefono4"),
                        resultSet.getString("telefono5")
                );
                contacto.setId(resultSet.getInt("c.id"));

                usuarioNuevo.setContacto(contacto);

                lista_clientes.add(usuarioNuevo);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista_clientes;
    }

    @Override
    public int contarUsuariosSimilares(String nombreUsuario) {
        PreparedStatement statement;
        ResultSet resultSet;
        Conexion conexion = Conexion.getConexion();
        try {
            statement = conexion.getSQLConexion().prepareStatement(contarUsuario);
            statement.setString(1, nombreUsuario + "%");

            resultSet = statement.executeQuery();

            if (resultSet.next()) {
                int usuariosTotales = resultSet.getInt(1);
                return usuariosTotales;
            }
        } catch (
                Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public boolean verificarExisteUsuario(String nombreUsuario) {
        PreparedStatement statement;
        ResultSet resultSet;
        Conexion conexion = Conexion.getConexion();
        try {
            statement = conexion.getSQLConexion().prepareStatement(contarUsuario);
            statement.setString(1, nombreUsuario);

            resultSet = statement.executeQuery();

            if (resultSet.next()) {
                int usuariosTotales = resultSet.getInt(1);
                return usuariosTotales > 0;
            }
        } catch (
                Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean update(Usuario usuario, String estado) {
        PreparedStatement statement;
        Conexion conexion = Conexion.getConexion();
        try {
            statement = conexion.getSQLConexion().prepareStatement(updateEstado);

            statement.setString(1, estado);
            statement.setInt(2, usuario.getId());

            if (statement.executeUpdate() > 0) {
                return true;
            } else {
                throw new Exception("No pudo actualizarse el USUARIO");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
}
