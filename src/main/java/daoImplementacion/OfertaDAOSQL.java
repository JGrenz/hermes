package daoImplementacion;

import conexion.Conexion;
import dao.OfertaDAO;
import dominio.Oferta;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class OfertaDAOSQL implements OfertaDAO {
    private static String insert = "INSERT INTO oferta (fecha_creacion,fecha_vencimiento, codigo_referencia,titulo, descuento,estado) VALUES (NOW(),?,?,?,?,?)";
    private static String update = "UPDATE oferta SET fecha_vencimiento = ?, titulo = ?, descuento = ?,estado = ? WHERE ID = ?";
    private static String findById = "SELECT * FROM oferta where id = ?";
    private static String findAll = "SELECT * FROM oferta";

    @Override
    public boolean insert(Oferta oferta) {
        PreparedStatement statement;
        Conexion conexion = Conexion.getConexion();
        try {
            statement = conexion.getSQLConexion().prepareStatement(insert, Statement.RETURN_GENERATED_KEYS);

            java.sql.Date fechaVencimiento = java.sql.Date.valueOf(oferta.getFechaVencimiento());

            statement.setDate(1, fechaVencimiento);
            statement.setString(2, oferta.getCodigoReferencia());
            statement.setString(3, oferta.getTitulo());
            statement.setInt(4, oferta.getPorcentajeDescuento());
            statement.setString(5, oferta.getEstado());

            int affectedRows = statement.executeUpdate();

            if (affectedRows == 0)
                throw new Exception("Creacion de oferta fallida, no rows affected.");

            else {
                try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
                    if (generatedKeys.next()) {
                        oferta.setId(generatedKeys.getInt(1));
                        return true;
                    } else
                        throw new Exception("Creacion de oferta fallida, no se obtuvo id");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public Oferta findById(int id) {
        PreparedStatement statement;
        ResultSet resultSet;
        Oferta oferta = null;
        Conexion conexion = Conexion.getConexion();
        try {
            statement = conexion.getSQLConexion().prepareStatement(findById);
            statement.setInt(1, id);
            resultSet = statement.executeQuery();

            if (resultSet.next()) {
                oferta = new Oferta(resultSet.getString("titulo"),
                        resultSet.getInt("descuento"),
                        resultSet.getDate("fecha_vencimiento").toLocalDate()
                );
                oferta.setEstado(resultSet.getString("estado"));
                oferta.setCodigoReferencia(resultSet.getString("codigo_referencia"));
                oferta.setFechaCreacion(resultSet.getTimestamp("fecha_creacion").toLocalDateTime());
                oferta.setId(id);

                return oferta;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return oferta;
    }

    @Override
    public Oferta findByTItulo(String titulo) {
        return null;
    }

    @Override
    public Oferta findByCodigoReferencia(String codigoReferencia) {
        return null;
    }

    @Override
    public boolean update(Oferta oferta) {
        PreparedStatement statement;
        Conexion conexion = Conexion.getConexion();

        try {
            java.sql.Date fechaVencimiento = java.sql.Date.valueOf(oferta.getFechaVencimiento());

            statement = conexion.getSQLConexion().prepareStatement(update);
            statement.setDate(1, fechaVencimiento);
            statement.setString(2, oferta.getTitulo());
            statement.setInt(3, oferta.getPorcentajeDescuento());
            statement.setString(4, oferta.getEstado());
            statement.setInt(5, oferta.getId());

            if (statement.executeUpdate() > 0) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public List<Oferta> findAll() {
        PreparedStatement statement;
        ResultSet resultSet;
        List<Oferta> ofertas = new ArrayList<>();
        Conexion conexion = Conexion.getConexion();
        try {
            statement = conexion.getSQLConexion().prepareStatement(findAll);
            resultSet = statement.executeQuery();

            while (resultSet.next()) {
                Oferta oferta = new Oferta(resultSet.getString("titulo"),
                        resultSet.getInt("descuento"),
                        resultSet.getDate("fecha_vencimiento").toLocalDate()
                );
                oferta.setId(resultSet.getInt("id"));
                oferta.setEstado(resultSet.getString("estado"));
                oferta.setCodigoReferencia(resultSet.getString("codigo_referencia"));
                oferta.setFechaCreacion(resultSet.getTimestamp("fecha_creacion").toLocalDateTime());

                ofertas.add(oferta);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ofertas;
    }
}
