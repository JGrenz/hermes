package controladores.coordinador;

import java.lang.reflect.InvocationTargetException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import dominio.MetodosDominio;
import dominio.Oferta;
import dominio.Viaje;
import modelo.AccesoDatos;
import vistas.coordinador.ofertas.Main_Ofertas;
import vistas.coordinador.ofertas.Ventana_AgregarOferta;
import vistas.coordinador.ofertas.Ventana_GestionarOferta;
import vistas.coordinador.ofertas.Ventana_EditarOferta;

public class ControladorOfertas {

	private Main_Ofertas ventanaMainOfertas;
	private Ventana_AgregarOferta ventanaAgregarOferta;
	private Ventana_GestionarOferta ventanaAsignarOferta;
	private Ventana_EditarOferta ventanaEditarOferta;
	private AccesoDatos accesoDatos;
	private List<Oferta> ofertas_en_tabla;
	private Map<String, Viaje> viajesEnTabla;
	private Map<String, Viaje> viajesAsignados;

	public ControladorOfertas(Main_Ofertas ventanaMainOfertas, AccesoDatos acceso) {
		this.ventanaMainOfertas = ventanaMainOfertas;
		ventanaAgregarOferta = Ventana_AgregarOferta.getInstance();
		ventanaAsignarOferta = Ventana_GestionarOferta.getInstance();
		ventanaEditarOferta = Ventana_EditarOferta.getInstance();

		this.ventanaMainOfertas.getBtnAgregar().addActionListener(c -> ventanaAgregarOferta());
		this.ventanaMainOfertas.getBtnAsignar().addActionListener(c -> ventanaAsignarOferta());
		this.ventanaMainOfertas.getBtnEditar().addActionListener(e -> ventanaEditarOferta());
		this.ventanaMainOfertas.getBtnHabInhab().addActionListener(b -> HabilitarInhabilitar());

		this.ventanaMainOfertas.getRadioTodos().addActionListener(a -> realizarBusqueda(false));
		this.ventanaMainOfertas.getRadioHabilitado().addActionListener(a -> realizarBusqueda(false));
		this.ventanaMainOfertas.getRadioInhabilitado().addActionListener(a -> realizarBusqueda(false));
		this.ventanaMainOfertas.getBtnRealizarBusqueda().addActionListener(b -> realizarBusqueda(false));

		this.ventanaAgregarOferta.getBtnConfirmar().addActionListener(c -> {
			try {
				guardarOferta();
			} catch (InvocationTargetException | IllegalAccessException e) {
				e.printStackTrace();
			}
		});

		this.ventanaAsignarOferta.getBtnBuscarViaje().addActionListener(b -> buscarViajeAsignarOferta());
		this.ventanaAsignarOferta.getBtnAsignarOferta().addActionListener(a -> seleccionarViajeAsignar());
		this.ventanaAsignarOferta.getBtnDesasignarOferta().addActionListener(a -> seleccionarViajeDesasignar());
		this.ventanaAsignarOferta.getBtnConfirmar().addActionListener(a -> confirmarAsignarDesasignar());

		this.ventanaEditarOferta.getBtnConfirmar().addActionListener(e -> editarOferta());

		this.accesoDatos = acceso;
		this.ofertas_en_tabla = new ArrayList<>();
		this.viajesEnTabla = new HashMap<>();
		this.viajesAsignados = new HashMap<>();
	}

	private void editarOferta() {
		Oferta oferta = ofertaSeleccionadaEnTabla();
		String titulo = oferta.getTitulo();
		LocalDate fechaVencimiento = oferta.getFechaVencimiento();
		int porcentajeDescuento = oferta.getPorcentajeDescuento();

		setearCamposEditarOferta(oferta, ventanaEditarOferta.getTxtTitulo().getText(),
				ventanaEditarOferta.getTxtVencimiento(),
				Integer.parseInt(ventanaEditarOferta.getTxtDescuento().getText()));

		if (accesoDatos.updateOferta(oferta)) {
			actualizarOfertaEnArreglo(oferta);

			realizarBusqueda(true);
		} else {
			setearCamposEditarOferta(oferta, titulo, fechaVencimiento, porcentajeDescuento);

			JOptionPane.showMessageDialog(null, "No se pudo actualizar la oferta, intente nuevamente más tarde",
					"¡Advertencia!", JOptionPane.WARNING_MESSAGE);
		}
		ventanaEditarOferta.cerrar();
	}

	private void setearCamposEditarOferta(Oferta oferta, String titulo, LocalDate fechaVencimiento,
			int porcentajeDescuento) {
		oferta.setTitulo(titulo);
		oferta.setFechaVencimiento(fechaVencimiento);
		oferta.setPorcentajeDescuento(porcentajeDescuento);
	}

	private void actualizarOfertaEnArreglo(Oferta oferta) {
		for (Oferta ofertaEnTabla : ofertas_en_tabla) {
			if (ofertaEnTabla.equals(oferta)) {
				ofertaEnTabla = oferta;
			}
		}
	}

	public void inicializar() {
//		if (this.ofertas_en_tabla.size() == 0) {
			this.ofertas_en_tabla = this.accesoDatos.obtenerOfertas();
//		}
		llenarComboBoxPais();
		llenarComboBoxContinente();
		realizarBusqueda(true);
	}

	private void ventanaAgregarOferta() {
		ventanaAgregarOferta.mostrarVentana();
	}

	private void ventanaAsignarOferta() {
		try {
			Oferta oferta = ofertaSeleccionadaEnTabla();
			if (oferta.getEstado().equals("HABILITADO")) {
				limpiarTablaViaje(this.ventanaAsignarOferta.getModelViajes());
				limpiarTablaViaje(this.ventanaAsignarOferta.getModelViajesAsignados());
				this.viajesEnTabla.clear();
				this.viajesAsignados.clear();
				this.ventanaAsignarOferta.cerrar();
				llenarCamposAsignarDesasignarOferta(oferta);
				llenarTablaViajesAsignados(oferta);
				ventanaAsignarOferta.mostrarVentana();
			} else {
				JOptionPane.showMessageDialog(null, "Seleccione una oferta con estado HABILITADO para asignar.");
			}
		} catch (ArrayIndexOutOfBoundsException ex) {
			JOptionPane.showMessageDialog(null, "Seleccione una oferta para asignarla a un viaje.");
		}
	}

	private void llenarTablaViajesAsignados(Oferta oferta) {
		this.accesoDatos.obtenerViajesConOferta(oferta).forEach(viaje -> {
			mostrarViajes(viaje, this.ventanaAsignarOferta.getModelViajesAsignados());
			this.viajesAsignados.put(viaje.getCodigoReferencia(), viaje);
		});
	}

	private void llenarComboBoxPais() {
		if (this.ventanaAsignarOferta.getCBPais().getItemCount() == 0) {
			this.ventanaAsignarOferta.getCBPais().addItem("Seleccione un pais");
			String[] paises = Locale.getISOCountries();

			for (String pais : paises) {
				Locale obj = new Locale("", pais);

				this.ventanaAsignarOferta.getCBPais().addItem(obj.getDisplayCountry());
			}
			this.ventanaAsignarOferta.getCBPais().setSelectedIndex(0);
		}
	}

	private void llenarComboBoxContinente() {
		if (this.ventanaAsignarOferta.getCBContinente().getItemCount() == 0) {
			this.ventanaAsignarOferta.getCBContinente().addItem("Seleccione un continente");

			for (String continente : this.accesoDatos.obtenerContinentes()) {
				this.ventanaAsignarOferta.getCBContinente().addItem(continente);
			}
			this.ventanaAsignarOferta.getCBContinente().setSelectedIndex(0);
		}
	}

	private Oferta ofertaSeleccionadaEnTabla() throws ArrayIndexOutOfBoundsException {
		int index = this.ventanaMainOfertas.getTablaOfertas().getSelectedRow();

		return (Oferta) this.ventanaMainOfertas.getModelOfertas().getValueAt(index, 0);
	}

	private void llenarCamposAsignarDesasignarOferta(Oferta oferta) {
		this.ventanaAsignarOferta.getTxtTituloOferta().setText(oferta.getTitulo());
		this.ventanaAsignarOferta.getTxtDescuentoOferta().setText(Integer.toString(oferta.getPorcentajeDescuento()));
	}

	private void realizarBusqueda(boolean primeraEntrada) {
		limpiarTabla();
		Predicate<Oferta> filtro = filtrosOferta();
		if (filtro != null) {
			List<Oferta> lista_filtrada = this.ofertas_en_tabla.stream().filter(filtro).collect(Collectors.toList());

			if (lista_filtrada.size() > 0) {

				lista_filtrada.forEach(oferta -> agregarFilaATabla(oferta));
			} else {
				if (!primeraEntrada) {
					JOptionPane.showMessageDialog(null, "No se encontraron ofertas con los parámetros especificados.");
				}
			}
		} else {
			ofertas_en_tabla.forEach(oferta -> agregarFilaATabla(oferta));
		}
	}

	private Predicate<Oferta> filtrosOferta() {
		List<Predicate<Oferta>> lista_filtros = new ArrayList<Predicate<Oferta>>();
		String estado = radioButtonSeleccionado();
		if (!estado.equals("TODOS")) {
			Predicate<Oferta> filtroEstado = oferta -> oferta.getEstado().equals(estado);
			lista_filtros.add(filtroEstado);
		}

		String codigo = this.ventanaMainOfertas.getTxtCodigo().getText();
		if (!codigo.isEmpty()) {
			Predicate<Oferta> filtroCodigo = oferta -> oferta.getCodigoReferencia().equals(codigo);
			lista_filtros.add(filtroCodigo);
		}

		String titulo = this.ventanaMainOfertas.getTxtTitulo().getText().toLowerCase();
		if (!titulo.equals("")) {
			Predicate<Oferta> predicadoTitulo = oferta -> oferta.getTitulo().toLowerCase().indexOf(titulo) > -1;
			lista_filtros.add(predicadoTitulo);
		}

		if (lista_filtros.size() == 1) {
			return lista_filtros.get(0);
		} else if (lista_filtros.size() > 1) {
			Predicate<Oferta> acumulador = lista_filtros.get(0);
			lista_filtros.remove(0);
			for (Predicate<Oferta> predicado : lista_filtros) {
				acumulador = acumulador.and(predicado);
			}
			return acumulador;
		}
		return null;
	}

	public String radioButtonSeleccionado() {
		if (ventanaMainOfertas.getRadioHabilitado().isSelected()) {
			return "HABILITADO";
		}
		if (ventanaMainOfertas.getRadioInhabilitado().isSelected()) {
			return "INHABILITADO";
		}
		return "TODOS";
	}

	private void limpiarTabla() {
		this.ventanaMainOfertas.getModelOfertas().setRowCount(0);
		this.ventanaMainOfertas.getModelOfertas().setColumnCount(0);
		this.ventanaMainOfertas.getModelOfertas().setColumnIdentifiers(this.ventanaMainOfertas.getNombreColumnas());
	}

	private void agregarFilaATabla(Oferta oferta) {
		DateTimeFormatter formatoDeFechaHora = DateTimeFormatter.ofPattern("dd/MM/yyyy hh:mm");
		LocalDateTime fechaDeCreacion = oferta.getFechaCreacion();
		String fechaDeCreacionConFormato = fechaDeCreacion.format(formatoDeFechaHora);
		DateTimeFormatter formatoDeFecha = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		LocalDate fechaDeVencimiento = oferta.getFechaVencimiento();
		String fechaDeVencimientoConFormato = fechaDeVencimiento.format(formatoDeFecha);

		Object[] fila = { oferta, oferta.getTitulo(), oferta.getPorcentajeDescuento(), fechaDeCreacionConFormato,
				fechaDeVencimientoConFormato, oferta.getEstado() };
		this.ventanaMainOfertas.getModelOfertas().addRow(fila);
	}

	private void guardarOferta() throws InvocationTargetException, IllegalAccessException {
		if (this.ventanaAgregarOferta.verificarCampo()) {
			Oferta oferta = crearOferta();
			oferta.setCodigoReferencia(MetodosDominio.generarCodigoReferenia(ofertas_en_tabla));
			if (this.accesoDatos.agregarOferta(oferta)) {
				ofertas_en_tabla.add(oferta);
				realizarBusqueda(true);
				this.ventanaAgregarOferta.cerrar();
			} else {
				JOptionPane.showMessageDialog(null, "No se pudo agregar la oferta");
			}
		}
	}

	private Oferta crearOferta() {
		String descuento = ventanaAgregarOferta.getTxtDescuento().getText();
		int porcentajeDescuento = Integer.parseInt(descuento);
		String titulo = ventanaAgregarOferta.getTxtTitulo().getText();
		LocalDate vencimiento = ventanaAgregarOferta.getTxtVencimiento();

		Oferta oferta = new Oferta(titulo, porcentajeDescuento, vencimiento);

		return oferta;
	}

	private void ventanaEditarOferta() {
		try {
			Oferta oferta = ofertaSeleccionadaEnTabla();
			if (oferta.getEstado().equals("HABILITADO")) {
				llenarVentanaEditarOferta(oferta);
				this.ventanaEditarOferta.mostrarVentana();

			} else {
				JOptionPane.showMessageDialog(null, "Seleccione una oferta con estado HABILITADO para asignar.");
			}
		} catch (ArrayIndexOutOfBoundsException ex) {
			JOptionPane.showMessageDialog(null, "Seleccione una oferta para editarla.");
		}
	}

	private void llenarVentanaEditarOferta(Oferta oferta) {
		this.ventanaEditarOferta.setTxtTitulo(oferta.getTitulo());
		this.ventanaEditarOferta.setPorcentajeDescuento(oferta.getPorcentajeDescuento());
		this.ventanaEditarOferta.setFechaVencimiento(oferta.getFechaVencimiento());
	}

	private void HabilitarInhabilitar() {
		try {
			Oferta oferta = ofertaSeleccionadaEnTabla();

			if (oferta.getEstado().equals("HABILITADO")) {
				cambiarEstado(oferta, "INHABILITADO", "INHABILITAR");
			} else {
				cambiarEstado(oferta, "HABILITADO", "HABILITAR");
			}
		} catch (ArrayIndexOutOfBoundsException ex) {
			JOptionPane.showMessageDialog(null, "Seleccione una oferta.");
		}
	}

	private void cambiarEstado(Oferta oferta, String estado, String accion) {
		int confirm = JOptionPane.showOptionDialog(null, "¿Está seguro que quiere " + accion + " esta oferta?",
				"Confirmacion", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, null);
		if (confirm == 0) {
			String estadoOriginal = oferta.getEstado();
			oferta.setEstado(estado);
			if (this.accesoDatos.editarOferta(oferta)) {
				realizarBusqueda(true);
			} else {
				oferta.setEstado(estadoOriginal);
				JOptionPane.showMessageDialog(null, "No se pudo " + accion + " la oferta");
			}
		}
	}

	private void buscarViajeAsignarOferta() {
		limpiarTablaViaje(this.ventanaAsignarOferta.getModelViajes());
		this.viajesEnTabla.clear();
		Predicate<Viaje> filtro = filtroDeViaje();
		if (filtro != null) {
			this.accesoDatos.obtenerViajesDisponibles().stream().filter(filtro).forEach(viaje -> {
				if (!this.viajesAsignados.containsKey(viaje.getCodigoReferencia())) {
					this.viajesEnTabla.put(viaje.getCodigoReferencia(), viaje);
					mostrarViajes(viaje, this.ventanaAsignarOferta.getModelViajes());
				}
			});
			if (this.viajesEnTabla.isEmpty()) {
				JOptionPane.showMessageDialog(null, "No se encontraron viajes con los parametros especificados");
			}
		} else {
			this.accesoDatos.obtenerViajesDisponibles().forEach(viaje -> {
				if (!this.viajesAsignados.containsKey(viaje.getCodigoReferencia())) {
					this.viajesEnTabla.put(viaje.getCodigoReferencia(), viaje);
					mostrarViajes(viaje, this.ventanaAsignarOferta.getModelViajes());
				}
			});
		}
	}

	private void limpiarTablaViaje(DefaultTableModel tabla) {
		tabla.setRowCount(0);
		// tabla.setColumnCount(0);
		// tabla.setColumnIdentifiers(this.ventanaAsignarOferta.getColumnasDeViajes());
	}

	private Predicate<Viaje> filtroDeViaje() {
		List<Predicate<Viaje>> lista_predicados = new ArrayList<>();

		String ciudad = this.ventanaAsignarOferta.getTxtCiudad().getText().toLowerCase();
		if (!ciudad.equals("")) {
			Predicate<Viaje> predicadoCiudad = viaje -> viaje.getUbicacionDestino().getCiudad().toLowerCase()
					.indexOf(ciudad) > -1;
			lista_predicados.add(predicadoCiudad);
		}

		String provincia = this.ventanaAsignarOferta.getTxtProvincia().getText().toLowerCase();
		if (!provincia.equals("")) {
			Predicate<Viaje> predicadoProvincia = viaje -> viaje.getUbicacionDestino().getProvincia().toLowerCase()
					.indexOf(provincia) > -1;
			lista_predicados.add(predicadoProvincia);
		}

		if (this.ventanaAsignarOferta.getCBPais().getSelectedIndex() > 0) {
			String pais = this.ventanaAsignarOferta.getCBPais().getSelectedItem().toString();
			Predicate<Viaje> predicadoPais = viaje -> viaje.getUbicacionDestino().getPais().equals(pais);
			lista_predicados.add(predicadoPais);
		}

		if (this.ventanaAsignarOferta.getCBContinente().getSelectedIndex() > 0) {
			String continente = this.ventanaAsignarOferta.getCBContinente().getSelectedItem().toString();
			Predicate<Viaje> predicadoContinente = viaje -> viaje.getUbicacionDestino().getContinente()
					.equals(continente);
			lista_predicados.add(predicadoContinente);

		}

		if (lista_predicados.size() == 1) {
			return lista_predicados.get(0);
		} else if (lista_predicados.size() > 1) {
			Predicate<Viaje> acumulador = lista_predicados.get(0);
			lista_predicados.remove(0);
			for (Predicate<Viaje> predicado : lista_predicados) {
				acumulador = acumulador.and(predicado);
			}
			return acumulador;
		}
		return null;
	}

	private void mostrarViajes(Viaje viaje, DefaultTableModel tabla) {
		DateTimeFormatter formatoDeFecha = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		LocalDate fechaDeSalida = viaje.getFechaSalida();
		String fechaDeSalidaConFormato = fechaDeSalida.format(formatoDeFecha);

		Object[] fila = { viaje, viaje.getUbicacionOrigen(), viaje.getUbicacionDestino(), fechaDeSalidaConFormato,
				viaje.getMedioTransporte(), viaje.getPrecio(), viaje.getImpuesto() };
		tabla.addRow(fila);
	}

	private Viaje viajeSeleccionado(JTable tabla) throws ArrayIndexOutOfBoundsException {
		int fila = tabla.getSelectedRow();

		return (Viaje) tabla.getValueAt(fila, 0);
	}

	private void seleccionarViajeAsignar() {
		try {
			Viaje viaje = viajeSeleccionado(this.ventanaAsignarOferta.getTablaViajes());
			Oferta ofertaSeleccionada = ofertaSeleccionadaEnTabla();
			boolean viajeSinOfertaOInhabilitada = false;
			if (viaje.getOferta() == null) {
				viajeSinOfertaOInhabilitada = true;
			} else if(viaje.getOferta().getEstado().equals("INHABILITADO")) {
				viajeSinOfertaOInhabilitada = true;
			}
			else if(viaje.getOferta().getEstado().equals("HABILITADO")){
				int confirm = JOptionPane.showOptionDialog(null,
						"¿Este viaje ya tiene una oferta, desea asignarle otra?", "Confirmacion",
						JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, null);
				if (confirm == 0) {
					Oferta ofertaOriginal = viaje.getOferta();
					viaje.setOferta(ofertaSeleccionada);
					if (this.accesoDatos.editarOfertaDeViaje(viaje)) {
						mostrarViajes(viaje, this.ventanaAsignarOferta.getModelViajesAsignados());
						this.viajesAsignados.put(viaje.getCodigoReferencia(), viaje);
						this.viajesEnTabla.remove(viaje.getCodigoReferencia());
						this.ventanaAsignarOferta.getModelViajes()
								.removeRow(this.ventanaAsignarOferta.getTablaViajes().getSelectedRow());
					} else {
						viaje.setOferta(ofertaOriginal);
						JOptionPane.showMessageDialog(null, "No se pudo asignar la oferta al viaje.");
					}
				}
			}
			if(viajeSinOfertaOInhabilitada) {
				viaje.setOferta(ofertaSeleccionada);
				if (this.accesoDatos.editarOfertaDeViaje(viaje)) {
					mostrarViajes(viaje, this.ventanaAsignarOferta.getModelViajesAsignados());
					this.viajesAsignados.put(viaje.getCodigoReferencia(), viaje);
					this.viajesEnTabla.remove(viaje.getCodigoReferencia());
					this.ventanaAsignarOferta.getModelViajes()
							.removeRow(this.ventanaAsignarOferta.getTablaViajes().getSelectedRow());
				} else {
					viaje.setOferta(null);
					JOptionPane.showMessageDialog(null, "No se pudo asignar la oferta al viaje.");
				}
			}
		} catch (ArrayIndexOutOfBoundsException e) {
			JOptionPane.showMessageDialog(null, "Seleccione un viaje de la tabla superior.");
		}
	}

	private void seleccionarViajeDesasignar() {
		try {
			Viaje viaje = viajeSeleccionado(this.ventanaAsignarOferta.getTablaViajesAsignados());

			int confirm = JOptionPane.showOptionDialog(null, "¿Esta seguro de desasignar este viaje?", "Confirmacion",
					JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, null);
			if (confirm == 0) {
				viaje.setOferta(null);
				if (this.accesoDatos.editarOfertaDeViaje(viaje)) {
					mostrarViajes(viaje, this.ventanaAsignarOferta.getModelViajes());
					this.viajesEnTabla.put(viaje.getCodigoReferencia(), viaje);
					this.viajesAsignados.remove(viaje.getCodigoReferencia());
					this.ventanaAsignarOferta.getModelViajesAsignados()
							.removeRow(this.ventanaAsignarOferta.getTablaViajesAsignados().getSelectedRow());
				} else {
					viaje.setOferta(ofertaSeleccionadaEnTabla());
					JOptionPane.showMessageDialog(null, "No se pudo desasignar la oferta al viaje.");
				}
			}
		} catch (ArrayIndexOutOfBoundsException e) {
			JOptionPane.showMessageDialog(null, "Seleccione un viaje de la tabla inferior.");
		}
	}

	private void confirmarAsignarDesasignar() {
		this.ventanaAsignarOferta.cerrar();
	}
}