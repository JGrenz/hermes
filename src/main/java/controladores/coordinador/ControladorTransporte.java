package controladores.coordinador;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.swing.*;

import dominio.MedioTransporte;
import modelo.AccesoDatos;
import vistas.ValidacionCampos;
import vistas.coordinador.transporte.Main_Transporte;
import vistas.coordinador.transporte.Ventana_AgregarTransporte;
import vistas.coordinador.transporte.Ventana_EditarTransporte;

public class ControladorTransporte {
	private Main_Transporte ventanaMainTransporte;
	private Ventana_AgregarTransporte ventanaAgregarTransporte;
	private Ventana_EditarTransporte ventanaEditarTransporte;
	private AccesoDatos accesoDatos;
	private List<MedioTransporte> transporte_en_tabla;

	public ControladorTransporte(Main_Transporte ventanaMainTransporte, AccesoDatos accesoDatos) {
		this.ventanaMainTransporte = ventanaMainTransporte;
		this.ventanaAgregarTransporte = Ventana_AgregarTransporte.getInstance();
		this.ventanaEditarTransporte = Ventana_EditarTransporte.getInstance();

		this.ventanaMainTransporte.getBtnAgregar().addActionListener(t -> ventanaAgregarTransporte(t));
		this.ventanaMainTransporte.getBtnEditar().addActionListener(e -> ventanaEditarTransporte(e));
		this.ventanaMainTransporte.getBtnEstado().addActionListener(h -> HabilitarInhabilitar());

		this.ventanaMainTransporte.getBtnBuscar().addActionListener(h -> realizarBusqueda(false));

		this.ventanaMainTransporte.getRdbtnHabilitados().addActionListener(e -> realizarBusqueda(false));
		this.ventanaMainTransporte.getRdbtnInhabilitados().addActionListener(e -> realizarBusqueda(false));
		this.ventanaMainTransporte.getRdbtnTodos().addActionListener(e -> realizarBusqueda(false));

		this.ventanaAgregarTransporte.getBtnConfirmar().addActionListener(c -> guardarTransporte(c));
		this.ventanaEditarTransporte.getBtnConfirmar().addActionListener(e -> editarTransporte(e));

		this.accesoDatos = accesoDatos;
		this.transporte_en_tabla = new ArrayList<MedioTransporte>();
	}

	public void inicializar() {
//		if (this.transporte_en_tabla.size() == 0) {
			this.transporte_en_tabla = accesoDatos.obtenerTransportes();
//		}

		realizarBusqueda(true);
		ventanaMainTransporte.mostrarVentana();
	}

	private void realizarBusqueda(boolean primeraEntrada) {
		Predicate<MedioTransporte> filtro = filtrosTransporte();
		limpiarTabla();
		if (filtro != null) {
			List<MedioTransporte> lista_filtrada = transporte_en_tabla.stream().filter(filtro)
					.collect(Collectors.toList());
			if (lista_filtrada.size() > 0) {
				lista_filtrada.forEach(transporte -> agregarTransporteAFila(transporte));
			} else {
				if (!primeraEntrada) {
					JOptionPane.showMessageDialog(null, "No se encontraron medios de transportes.");
				}
			}
		} else {
			transporte_en_tabla.forEach(transporte -> agregarTransporteAFila(transporte));
		}
	}

	private Predicate<MedioTransporte> filtrosTransporte() {
		List<Predicate<MedioTransporte>> lista_filtros = new ArrayList<Predicate<MedioTransporte>>();
		String estado = radioButtonSeleccionado();
		if (!estado.equals("TODOS")) {
			Predicate<MedioTransporte> filtroEstado = transporte -> transporte.getEstado().equals(estado);
			lista_filtros.add(filtroEstado);
		}

		String nombreABuscar = ventanaMainTransporte.getTxtBarradebusqueda().getText().toLowerCase();
		if (!nombreABuscar.isEmpty()) {
			Predicate<MedioTransporte> filtroNombre = transporte -> transporte.getNombre().toLowerCase()
					.indexOf(nombreABuscar) > -1;
			lista_filtros.add(filtroNombre);
		}

		if (lista_filtros.size() == 1) {
			return lista_filtros.get(0);
		} else if (lista_filtros.size() > 1) {
			Predicate<MedioTransporte> acumulador = lista_filtros.get(0);
			lista_filtros.remove(0);
			for (Predicate<MedioTransporte> predicado : lista_filtros) {
				acumulador = acumulador.and(predicado);
			}
			return acumulador;
		}
		return null;
	}

	public String radioButtonSeleccionado() {
		if (ventanaMainTransporte.getRdbtnHabilitados().isSelected()) {
			return "HABILITADO";
		}
		if (ventanaMainTransporte.getRdbtnInhabilitados().isSelected()) {
			return "INHABILITADO";
		}
		return "TODOS";
	}

	private void limpiarTabla() {
		this.ventanaMainTransporte.getModelTransporte().setRowCount(0);
		this.ventanaMainTransporte.getModelTransporte().setColumnCount(0);
		this.ventanaMainTransporte.getModelTransporte()
				.setColumnIdentifiers(this.ventanaMainTransporte.getNombreColumnas());
	}

	private void agregarTransporteAFila(MedioTransporte transporte) {
		Object[] fila = { transporte, transporte.getEstado() };

		this.ventanaMainTransporte.getModelTransporte().addRow(fila);
	}

	private void ventanaAgregarTransporte(ActionEvent t) {
		ventanaAgregarTransporte.mostrarVentana();
	}

	private void ventanaEditarTransporte(ActionEvent e) {
		try {
			MedioTransporte transporteSeleccionado = transporteSeleccionado();
			if (transporteSeleccionado.getEstado().equals("HABILITADO")) {

				this.llenarCampos(transporteSeleccionado);
				this.ventanaEditarTransporte.mostrarVentana();
			} else {
				JOptionPane.showMessageDialog(null, "Seleccione un medio de transporte con estado HABILITADO.");
			}
		} catch (ArrayIndexOutOfBoundsException ex) {
			JOptionPane.showMessageDialog(null, "Seleccione un medio de transporte para editar.");
		}
	}

	private MedioTransporte transporteSeleccionado() {
		int fila = ventanaMainTransporte.getTabla().getSelectedRow();

		return (MedioTransporte) ventanaMainTransporte.getTabla().getValueAt(fila, 0);
	}

	private void llenarCampos(MedioTransporte transporte) {
		this.ventanaEditarTransporte.getTxtTransporte().setText(transporte.getNombre());
	}

	private void HabilitarInhabilitar() {
		try {
			MedioTransporte transporte = transporteSeleccionado();

			if (transporte.getEstado().equals("HABILITADO")) {
				cambiarEstado(transporte, "INHABILITADO", "INHABILITAR");
			} else {
				cambiarEstado(transporte, "HABILITADO", "HABILITAR");
			}
		} catch (ArrayIndexOutOfBoundsException ex) {
			JOptionPane.showMessageDialog(null, "Seleccione un medio de tranporte.");
		}
	}

	private void cambiarEstado(MedioTransporte transporte, String estado, String accion) {
		int confirm = JOptionPane.showOptionDialog(null,
				"¿Está seguro que quiere " + accion + " este medio de transporte?", "Confirmacion",
				JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, null);
		if (confirm == 0) {
			if (!this.accesoDatos.transporteEnUso(transporte)) {
				if (this.accesoDatos.cambiarEstadoTransporte(transporte, estado)) {
					transporte.setEstado(estado);
					realizarBusqueda(true);
				} else {
					JOptionPane.showMessageDialog(null, "No se pudo " + accion + " el medio de transporte");
				}
			} else {
				JOptionPane.showMessageDialog(null,
						"No se puede " + accion + " el medio de transporte porque se encuentra en uso");
			}
		}
	}

	private void guardarTransporte(ActionEvent c) {
		if (verificarCampoIngreso()) {
			MedioTransporte transporte = crearTransporte();
			if (!existeTransporte(transporte)) {
				if (this.accesoDatos.agregarTransporte(transporte)) {
					transporte_en_tabla.add(transporte);
					realizarBusqueda(true);
					this.ventanaAgregarTransporte.cerrar();
				} else {
					JOptionPane.showMessageDialog(null, "No se pudo guardar el medio de transporte");
				}
			} else {
				JOptionPane.showMessageDialog(null, "No se puede agregar un transporte repetido");
			}
		}
	}

	private boolean camposVacios() {
		if (ventanaAgregarTransporte.getTxtTransporte().getText().equals(""))
		{
			JOptionPane.showMessageDialog(null, "No pueden quedar campos vacíos.");
			return true;
		}
		else
			return false;
	}
	
	private boolean camposCorrectos() {
		ValidacionCampos validador = new ValidacionCampos();
		if(!validador.esPalabra(ventanaAgregarTransporte.getTxtTransporte().getText()))
			return false;
		else
			return true;
	}
	
	public boolean verificarCampoIngreso() {
		if (!camposVacios() && camposCorrectos())
			return true;
		else
			return false;
	}

	private MedioTransporte crearTransporte() {
		return new MedioTransporte(ventanaAgregarTransporte.getTxtTransporte().getText());
	}

	private boolean existeTransporte(MedioTransporte transporte) {
		for (MedioTransporte medioTransporte : this.accesoDatos.obtenerTransportes())
			if (transporte.equals(medioTransporte))
				return true;

		return false;
	}

	private void editarTransporte(ActionEvent e) {
		if (verificarCampoIngreso()) {
			MedioTransporte transporte = transporteSeleccionado();

			String nombreOriginal = transporte.getNombre();

			transporte.setNombre(this.ventanaEditarTransporte.getTxtTransporte().getText());
			if (!existeTransporte(transporte)) {
				if (this.accesoDatos.editarTransporte(transporte)) {
					realizarBusqueda(true);
					this.ventanaEditarTransporte.cerrar();
				} else {
					transporte.setNombre(nombreOriginal);
					JOptionPane.showMessageDialog(null, "No se pudo editar el medio de transporte");
				}
			} else {
				transporte.setNombre(nombreOriginal);
				JOptionPane.showMessageDialog(null, "El medio de transporte ya existe.");
			}
		}
	}
}