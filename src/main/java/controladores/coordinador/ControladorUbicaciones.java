package controladores.coordinador;

import dominio.Ubicacion;
import modelo.AccesoDatos;
import vistas.coordinador.ubicaciones.Main_Ubicaciones;
import vistas.coordinador.ubicaciones.Ventana_AgregarUbicacion;
import vistas.coordinador.ubicaciones.Ventana_EditarUbicacion;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class ControladorUbicaciones {

	private Main_Ubicaciones ventanaMainUbicaciones;
	private Ventana_AgregarUbicacion ventanaAgregarUbicacion;
	private Ventana_EditarUbicacion ventanaEditarUbicacion;
	private AccesoDatos acceso;
	private List<Ubicacion> ubicaciones_en_tabla;

	public ControladorUbicaciones(Main_Ubicaciones ventanaMainUbicacion, AccesoDatos acceso) {

		this.ventanaMainUbicaciones = ventanaMainUbicacion;
		ventanaAgregarUbicacion = Ventana_AgregarUbicacion.getInstance();
		ventanaEditarUbicacion = Ventana_EditarUbicacion.getInstance();

		this.ventanaMainUbicaciones.getRadioTodos().addActionListener(e -> realizarBusqueda(false));
		this.ventanaMainUbicaciones.getRadioHabilitados().addActionListener(e -> realizarBusqueda(false));
		this.ventanaMainUbicaciones.getRadioInhabilitados().addActionListener(e -> realizarBusqueda(false));

		this.ventanaMainUbicaciones.getBtnRealizarBusqueda().addActionListener(e -> realizarBusqueda(false));

		this.ventanaMainUbicaciones.getBtnAgregar().addActionListener(a -> ventanaAgregarUbicacion(a));
		this.ventanaMainUbicaciones.getBtnEditar().addActionListener(e -> ventanaEditarUbicacion(e));
		this.ventanaMainUbicaciones.getBtnEstado().addActionListener(i -> HabilitarInhabilitar(i));

		this.ventanaAgregarUbicacion.getBtnConfirmar().addActionListener(c -> guardarUbicacion(c));
		this.ventanaAgregarUbicacion.getBtnAgregarTag().addActionListener(a -> agregarTag(a));
		this.ventanaAgregarUbicacion.getBtnEliminarTag().addActionListener(a -> eliminarTag(a));

		this.ventanaEditarUbicacion.getBtnConfirmar().addActionListener(e -> editarUbicacion(e));
		this.ventanaEditarUbicacion.getBtnAgregarTag().addActionListener(a -> agregarTagEdit(a));
		this.ventanaEditarUbicacion.getBtnEliminarTag().addActionListener(a -> eliminarTagEdit(a));

		this.acceso = acceso;
		this.ubicaciones_en_tabla = new ArrayList<Ubicacion>();
	}

	public void inicializar() {
//		if (this.ubicaciones_en_tabla.size() == 0) {
			this.ubicaciones_en_tabla = acceso.obtenerUbicaciones();
//		}

		realizarBusqueda(true); // La primera vez que entra si no se encuentra nada no imprime un mensaje
								// avisando
		llenarComboBoxContinente();
	}

	private void realizarBusqueda(boolean primeraEntrada) {
		Predicate<Ubicacion> filtro = filtrosUbicacion();
		limpiarTabla();
		if (filtro != null) {
			List<Ubicacion> lista_filtrada = ubicaciones_en_tabla.stream().filter(filtro).collect(Collectors.toList());
			if (lista_filtrada.size() > 0) {
				lista_filtrada.forEach(ubicacion -> agregarFilaATabla(ubicacion));
			} else {
				if (!primeraEntrada) {
					JOptionPane.showMessageDialog(null, "No se encontraron ubicaciones.");
				}
			}
		} else {
			ubicaciones_en_tabla.forEach(ubicacion -> agregarFilaATabla(ubicacion));
		}
	}

	private Predicate<Ubicacion> filtrosUbicacion() {
		List<Predicate<Ubicacion>> lista_filtros = new ArrayList<Predicate<Ubicacion>>();

		String estado = radioButtonSeleccionado();
		if (!estado.equals("TODOS")) {
			Predicate<Ubicacion> filtroEstado = ubicacion -> ubicacion.getEstado().equals(estado);
			lista_filtros.add(filtroEstado);
		}

		String ciudad = ventanaMainUbicaciones.getTxtCiudad().getText().toLowerCase();
		if (!ciudad.isEmpty()) {
			Predicate<Ubicacion> filtroCiudad = ubicacion -> ubicacion.getCiudad().toLowerCase().indexOf(ciudad) > -1;
			lista_filtros.add(filtroCiudad);
		}

		String provincia = ventanaMainUbicaciones.getTxtProvincia().getText().toLowerCase();
		if (!provincia.isEmpty()) {
			Predicate<Ubicacion> filtroProvincia = ubicacion -> ubicacion.getProvincia().toLowerCase()
					.indexOf(provincia) > -1;
			lista_filtros.add(filtroProvincia);
		}

		String pais = ventanaMainUbicaciones.getTxtPais().getText().toLowerCase();
		if (!pais.isEmpty()) {
			Predicate<Ubicacion> filtroPais = ubicacion -> ubicacion.getPais().toLowerCase().indexOf(pais) > -1;
			lista_filtros.add(filtroPais);
		}

		String continente = ventanaMainUbicaciones.getTxtContinente().getText().toLowerCase();
		if (!continente.isEmpty()) {
			Predicate<Ubicacion> filtroContinente = ubicacion -> ubicacion.getContinente().toLowerCase()
					.indexOf(continente) > -1;
			lista_filtros.add(filtroContinente);
		}

		String tag = ventanaMainUbicaciones.getTxtTag().getText().toLowerCase();
		if (!tag.isEmpty()) {
			Predicate<Ubicacion> filtroTags = ubicacion -> ubicacion.getTags().stream()
					.filter(tags -> tags.toLowerCase().indexOf(tag) > -1).count() > 0;
			lista_filtros.add(filtroTags);
		}

		if (lista_filtros.size() == 1) {
			return lista_filtros.get(0);
		} else if (lista_filtros.size() > 1) {
			Predicate<Ubicacion> acumulador = lista_filtros.get(0);
			lista_filtros.remove(0);
			for (Predicate<Ubicacion> predicado : lista_filtros) {
				acumulador = acumulador.and(predicado);
			}
			return acumulador;
		}
		return null;
	}

	public String radioButtonSeleccionado() {
		if (ventanaMainUbicaciones.getRadioHabilitados().isSelected()) {
			return "HABILITADO";
		}
		if (ventanaMainUbicaciones.getRadioInhabilitados().isSelected()) {
			return "INHABILITADO";
		}
		return "TODOS";
	}

	private void limpiarTabla() {
		this.ventanaMainUbicaciones.getModelUbicaciones().setRowCount(0);
		this.ventanaMainUbicaciones.getModelUbicaciones().setColumnCount(0);
		this.ventanaMainUbicaciones.getModelUbicaciones()
				.setColumnIdentifiers(this.ventanaMainUbicaciones.getNombreColumnas());
	}

	private void agregarFilaATabla(Ubicacion ubicacion) {
		Object[] fila = { ubicacion, ubicacion.getProvincia(), ubicacion.getPais(), ubicacion.getContinente(),
				ubicacion.getDetalle(), ubicacion.getTags(), ubicacion.getEstado() };
		this.ventanaMainUbicaciones.getModelUbicaciones().addRow(fila);
	}

	private void llenarComboBoxContinente() {
		this.ventanaAgregarUbicacion.getContinentes().removeAllItems();
		this.ventanaEditarUbicacion.getContinentes().removeAllItems();

		for (String continente : this.acceso.obtenerContinentes()) {
			this.ventanaAgregarUbicacion.getContinentes().addItem(continente);
			this.ventanaEditarUbicacion.getContinentes().addItem(continente);
		}
		this.ventanaAgregarUbicacion.getContinentes().setSelectedIndex(-1);
		this.ventanaEditarUbicacion.getContinentes().setSelectedIndex(-1);

		this.ventanaAgregarUbicacion.getContinentes().addItemListener(c -> llenarCBPaisesAgregar(c));
		this.ventanaEditarUbicacion.getContinentes().addItemListener(c -> llenarCBPaisesEditar(c));
	}

	private void ventanaAgregarUbicacion(ActionEvent a) {
		reiniciarTagsAgregar();
		ventanaAgregarUbicacion.mostrarVentana();
	}

	private void llenarCBPaisesAgregar(ItemEvent c) {
		this.ventanaAgregarUbicacion.getPaises().removeAllItems();
		if (this.ventanaAgregarUbicacion.getContinentes().getSelectedIndex() != -1) {
			String continente = this.ventanaAgregarUbicacion.getContinentes().getSelectedItem().toString();
			llenarComboBoxPaises(continente);
		}
	}

	private void agregarTag(ActionEvent a) {
		if (this.ventanaAgregarUbicacion.getListadoTags().getSelectedIndex() != -1) {
			this.ventanaAgregarUbicacion.getModelTagsAgregados()
					.addElement(this.ventanaAgregarUbicacion.getListadoTags().getSelectedValue());
			this.ventanaAgregarUbicacion.getModelTags()
					.removeElement(this.ventanaAgregarUbicacion.getListadoTags().getSelectedValue());
		} else {
			JOptionPane.showMessageDialog(null, "Seleccione un tag para agregar");
		}
	}

	private void eliminarTag(ActionEvent a) {
		if (this.ventanaAgregarUbicacion.getListadoTagsAgregados().getSelectedIndex() != -1) {
			this.ventanaAgregarUbicacion.getModelTags()
					.addElement(this.ventanaAgregarUbicacion.getListadoTagsAgregados().getSelectedValue());
			this.ventanaAgregarUbicacion.getModelTagsAgregados()
					.removeElement(this.ventanaAgregarUbicacion.getListadoTagsAgregados().getSelectedValue());
		} else {
			JOptionPane.showMessageDialog(null, "Seleccione un tag para eliminar");
		}
	}

	private void reiniciarTagsAgregar() {
		for (int i = this.ventanaAgregarUbicacion.getModelTagsAgregados().size() - 1; i > -1; i--) {
			this.ventanaAgregarUbicacion.getModelTags()
					.addElement(this.ventanaAgregarUbicacion.getModelTagsAgregados().getElementAt(i));
			this.ventanaAgregarUbicacion.getModelTagsAgregados()
					.removeElement(this.ventanaAgregarUbicacion.getModelTagsAgregados().getElementAt(i));
		}
	}

	private void guardarUbicacion(ActionEvent c) {
		if (this.ventanaAgregarUbicacion.verificarCampo()) {

			Ubicacion ubicacion = crearUbicacion();
			if (!existeUbicacion(ubicacion)) {

				if (this.acceso.agregarUbicacion(ubicacion)) {
					this.ubicaciones_en_tabla.add(ubicacion);
					this.ventanaAgregarUbicacion.cerrar();
					realizarBusqueda(true);

				} else
					JOptionPane.showMessageDialog(null, "No se pudo guardar la ubicación");
			} else
				JOptionPane.showMessageDialog(null, "¡La ubicación ya existe!");
		}
	}

	private Ubicacion crearUbicacion() {
		Ubicacion ubicacion = new Ubicacion(ventanaAgregarUbicacion.getTxtDestino().getText(),
				ventanaAgregarUbicacion.getTxtProvincia().getText(),
				ventanaAgregarUbicacion.getPaises().getSelectedItem().toString(),
				ventanaAgregarUbicacion.getContinentes().getSelectedItem().toString(),
				ventanaAgregarUbicacion.getTxtDetalle().getText());

		List<String> tagsSeleccionados = new ArrayList<String>();
		for (int i = 0; i < this.ventanaAgregarUbicacion.getModelTagsAgregados().size(); i++) {
			tagsSeleccionados.add(this.ventanaAgregarUbicacion.getModelTagsAgregados().getElementAt(i));
		}
		ubicacion.setTags(tagsSeleccionados);

		return ubicacion;
	}

	private void ventanaEditarUbicacion(ActionEvent e) {
		try {
			Ubicacion ubicacionSeleccionada = datosDeFilaSeleccionada();
			if (ubicacionSeleccionada.getEstado().equals("HABILITADO")) {
				reiniciarTagsEditar();
				this.llenarCampos(ubicacionSeleccionada);
				this.ventanaEditarUbicacion.mostrarVentana();
			} else {
				JOptionPane.showMessageDialog(null, "Seleccione una ubicación con estado HABILITADO.");
			}
		} catch (ArrayIndexOutOfBoundsException ex) {
			JOptionPane.showMessageDialog(null, "Seleccione una ubicación para editar.");
		}
	}

	private void llenarCampos(Ubicacion ubicacion) {
		this.ventanaEditarUbicacion.getTxtDestino().setText(ubicacion.getCiudad());
		this.ventanaEditarUbicacion.getTxtProvincia().setText(ubicacion.getProvincia());
		this.ventanaEditarUbicacion.getContinentes().setSelectedItem(ubicacion.getContinente());
		this.ventanaEditarUbicacion.getPaises().setSelectedItem(ubicacion.getPais());
		this.ventanaEditarUbicacion.getTxtDetalle().setText(ubicacion.getDetalle());
		for (String tag : ubicacion.getTags()) {
			for (int i = 0; i < this.ventanaEditarUbicacion.getModelTags().size(); i++) {
				if (tag.equals(this.ventanaEditarUbicacion.getModelTags().getElementAt(i))) {
					this.ventanaEditarUbicacion.getModelTagsAgregados()
							.addElement(this.ventanaEditarUbicacion.getModelTags().getElementAt(i));
					this.ventanaEditarUbicacion.getModelTags()
							.removeElement(this.ventanaEditarUbicacion.getModelTags().getElementAt(i));
				}
			}
		}
	}

	private void agregarTagEdit(ActionEvent a) {
		if (this.ventanaEditarUbicacion.getListadoTags().getSelectedIndex() != -1) {
			this.ventanaEditarUbicacion.getModelTagsAgregados()
					.addElement(this.ventanaEditarUbicacion.getListadoTags().getSelectedValue());
			this.ventanaEditarUbicacion.getModelTags()
					.removeElement(this.ventanaEditarUbicacion.getListadoTags().getSelectedValue());
		} else {
			JOptionPane.showMessageDialog(null, "Seleccione un tag para agregar");
		}
	}

	private void eliminarTagEdit(ActionEvent a) {
		if (this.ventanaEditarUbicacion.getListadoTagsAgregados().getSelectedIndex() != -1) {
			this.ventanaEditarUbicacion.getModelTags()
					.addElement(this.ventanaEditarUbicacion.getListadoTagsAgregados().getSelectedValue());
			this.ventanaEditarUbicacion.getModelTagsAgregados()
					.removeElement(this.ventanaEditarUbicacion.getListadoTagsAgregados().getSelectedValue());
		} else {
			JOptionPane.showMessageDialog(null, "Seleccione un tag para eliminar");
		}
	}

	private void reiniciarTagsEditar() {
		for (int i = this.ventanaEditarUbicacion.getModelTagsAgregados().size() - 1; i > -1; i--) {
			this.ventanaEditarUbicacion.getModelTags()
					.addElement(this.ventanaEditarUbicacion.getModelTagsAgregados().getElementAt(i));
			this.ventanaEditarUbicacion.getModelTagsAgregados()
					.removeElement(this.ventanaEditarUbicacion.getModelTagsAgregados().getElementAt(i));
		}
	}

	private void llenarCBPaisesEditar(ItemEvent c) {
		this.ventanaEditarUbicacion.getPaises().removeAllItems();
		if (this.ventanaEditarUbicacion.getContinentes().getSelectedIndex() != -1) {
			String continente = this.ventanaEditarUbicacion.getContinentes().getSelectedItem().toString();
			llenarComboBoxPaises(continente);
		}
	}

	private void editarUbicacion(ActionEvent e) {
		if (this.ventanaEditarUbicacion.verificarCampo()) {
			try {
				Ubicacion ubicacionDeTabla = datosDeFilaSeleccionada();
				Ubicacion ubicacionConDatosDeVentana = ubicacionConDatosDeVentanaEditar();
				ubicacionConDatosDeVentana.setId(ubicacionDeTabla.getId());
				if (!existeUbicacion(ubicacionConDatosDeVentana)) {
					if (this.acceso.editarUbicacion(ubicacionConDatosDeVentana)) {
						setearUbicacion(ubicacionDeTabla, ubicacionConDatosDeVentana);
						realizarBusqueda(true);
						this.ventanaEditarUbicacion.cerrar();
					} else {
						setearUbicacion(ubicacionConDatosDeVentana, ubicacionDeTabla);
						JOptionPane.showMessageDialog(null, "No se pudo editar la ubicación");
					}
				} else {
					setearUbicacion(ubicacionConDatosDeVentana, ubicacionDeTabla);
					JOptionPane.showMessageDialog(null, "La ubicación ya existe");
				}
			} catch (ArrayIndexOutOfBoundsException ex) {
				JOptionPane.showMessageDialog(null, "Seleccione una ubicación.");
			}
		}
	}

	private Ubicacion datosDeFilaSeleccionada() throws ArrayIndexOutOfBoundsException {
		int index = this.ventanaMainUbicaciones.getTablaUbicaciones().getSelectedRow();
		return (Ubicacion) this.ventanaMainUbicaciones.getModelUbicaciones().getValueAt(index, 0);
	}

	private Ubicacion ubicacionConDatosDeVentanaEditar() {
		Ubicacion nuevaUbicacion = new Ubicacion(this.ventanaEditarUbicacion.getTxtDestino().getText(),
				this.ventanaEditarUbicacion.getTxtProvincia().getText(),
				this.ventanaEditarUbicacion.getPaises().getSelectedItem().toString(),
				this.ventanaEditarUbicacion.getContinentes().getSelectedItem().toString(),
				this.ventanaEditarUbicacion.getTxtDetalle().getText());

		List<String> tagsSeleccionados = new ArrayList<String>();
		for (int i = 0; i < this.ventanaEditarUbicacion.getModelTagsAgregados().size(); i++) {
			tagsSeleccionados.add(this.ventanaEditarUbicacion.getModelTagsAgregados().getElementAt(i));
		}
		nuevaUbicacion.setTags(tagsSeleccionados);

		return nuevaUbicacion;
	}

	private void setearUbicacion(Ubicacion ubicacionAeditar, Ubicacion datosNuevos) {
		ubicacionAeditar.setCiudad(datosNuevos.getCiudad());
		ubicacionAeditar.setProvincia(datosNuevos.getProvincia());
		ubicacionAeditar.setPais(datosNuevos.getPais());
		ubicacionAeditar.setContinente(datosNuevos.getContinente());
		ubicacionAeditar.setDetalle(datosNuevos.getDetalle());
		ubicacionAeditar.setTags(datosNuevos.getTags());
	}

	private void llenarComboBoxPaises(String continente) {
		switch (continente) {
		case "África":
			cargarPaises("AF");
			break;
		case "Antártida":
			cargarPaises("AN");
			break;
		case "Asia":
			cargarPaises("AS");
			break;
		case "Europa":
			cargarPaises("EU");
			break;
		case "Norte América":
			cargarPaises("NA");
			break;
		case "Oceania":
			cargarPaises("OC");
			break;
		case "Sudamérica":
			cargarPaises("SA");
			break;
		}
	}

	private void cargarPaises(String codigoContinente) {
		List<Locale> paises = this.acceso.obtenerPaisesDeContinente(codigoContinente);
		for (Locale pais : paises) {
			this.ventanaAgregarUbicacion.getPaises().addItem(pais.getDisplayCountry());
			this.ventanaEditarUbicacion.getPaises().addItem(pais.getDisplayCountry());
		}
	}

	private boolean existeUbicacion(Ubicacion ubicacion) {
		for (Ubicacion ubicacionEnTabla : ubicaciones_en_tabla)
			if (ubicacion.equals(ubicacionEnTabla) && ubicacion.getId() != ubicacionEnTabla.getId())
				return true;

		return false;
	}

	private void HabilitarInhabilitar(ActionEvent h) {
		try {
			Ubicacion ubicacion = datosDeFilaSeleccionada();
			if (ubicacion.getEstado().equals("HABILITADO")) {
				cambiarEstado(ubicacion, "INHABILITADO", "INHABILITAR");
			} else {
				cambiarEstado(ubicacion, "HABILITADO", "HABILITAR");
			}
		} catch (ArrayIndexOutOfBoundsException ex) {
			JOptionPane.showMessageDialog(null, "Seleccione una ubicación.");
		}
	}

	private void cambiarEstado(Ubicacion ubicacion, String estado, String accion) {
		int confirm = JOptionPane.showOptionDialog(null, "¿Está seguro que quiere " + accion + " esta ubicación?",
				"Confirmacion", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, null);
		if (confirm == 0) {
			if (!this.acceso.ubicacionEnUso(ubicacion)) {
				if (this.acceso.cambiarEstadoUbicacion(ubicacion, estado)) {
					ubicacion.setEstado(estado);
					realizarBusqueda(true);
				} else {
					JOptionPane.showMessageDialog(null, "No se pudo " + accion + " la ubicación");
				}
			} else {
				JOptionPane.showMessageDialog(null,
						"No se puede " + accion + " la ubicación porque se encuentra en uso");
			}
		}
	}

//	private void actualizarVentana() {
//		ventanaMainUbicaciones.getPanel().revalidate();
//		ventanaMainUbicaciones.getPanel().repaint();
//	}
}
