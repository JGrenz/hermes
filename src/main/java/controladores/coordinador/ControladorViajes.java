package controladores.coordinador;

import java.awt.event.ActionEvent;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.swing.JOptionPane;

import daoImplementacion.PasajeDAOSQL;
import dominio.Devolucion;
import dominio.Gasto;
import dominio.Local;
import dominio.Mail;
import dominio.MedioTransporte;
import dominio.MetodosDominio;
import dominio.Operacion;
import dominio.Pasaje;
import dominio.Ubicacion;
import dominio.Viaje;
import modelo.AccesoDatos;
import vistas.coordinador.viajes.Main_Viajes;
import vistas.coordinador.viajes.Ventana_AgregarViaje;
import vistas.coordinador.viajes.Ventana_EditarViaje;

public class ControladorViajes {

	private Main_Viajes ventanaMainViajes;
	private Ventana_AgregarViaje ventanaAgregarViaje;
	private Ventana_EditarViaje ventanaEditarViaje;
	private AccesoDatos accesoDatos;
	private List<Viaje> viajes_en_tabla;
	private Mail mail;
	private List<Ubicacion> ubicaciones_en_tabla;
	private Ubicacion ubicacionOrigenAgregar;
	private Ubicacion ubicacionDestinoAgregar;
	private Ubicacion ubicacionOrigenEditar;
	private Ubicacion ubicacionDestinoEditar;

	public ControladorViajes(Main_Viajes ventanaMainViajes, AccesoDatos acceso) {
		this.ventanaMainViajes = ventanaMainViajes;
		ventanaAgregarViaje = Ventana_AgregarViaje.getInstance();
		ventanaEditarViaje = Ventana_EditarViaje.getInstance();

		this.ventanaMainViajes.getBtnAgregar().addActionListener(c -> ventanaAgregarViaje(c));
		this.ventanaMainViajes.getBtnEditar().addActionListener(e -> ventanaEditarViaje(e));
		this.ventanaMainViajes.getBtnBorrar().addActionListener(b -> {
			try {
				cancelarViaje();
			} catch (InvocationTargetException | IllegalAccessException e1) {
				e1.printStackTrace();
			}
		});

		this.ventanaMainViajes.getRadioAbierto().addActionListener(a -> realizarBusqueda(false));
		this.ventanaMainViajes.getRadioCancelado().addActionListener(a -> realizarBusqueda(false));
		this.ventanaMainViajes.getRadioFinalizado().addActionListener(a -> realizarBusqueda(false));
		this.ventanaMainViajes.getRadioTodos().addActionListener(a -> realizarBusqueda(false));
		this.ventanaMainViajes.getBtnRealizarBusqueda().addActionListener(b -> realizarBusqueda(false));

		this.ventanaAgregarViaje.getBtnConfirmar().addActionListener(c -> {
			try {
				guardarViaje(c);
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		});
		this.ventanaEditarViaje.getBtnConfirmar().addActionListener(e -> editarViaje(e));

		this.ventanaAgregarViaje.getBtnBuscarUbicacion().addActionListener(c -> buscarUbicacionAgregar());
		this.ventanaAgregarViaje.getBtnSeleccionarOrigen().addActionListener(o -> seleccionarOrigenAgregar());
		this.ventanaAgregarViaje.getBtnSeleccionarDestino().addActionListener(o -> seleccionarDestinoAgregar());
		this.ventanaEditarViaje.getBtnBuscarUbicacion().addActionListener(c -> buscarUbicacionEditar());
		this.ventanaEditarViaje.getBtnSeleccionarOrigen().addActionListener(o -> seleccionarOrigenEditar());
		this.ventanaEditarViaje.getBtnSeleccionarDestino().addActionListener(o -> seleccionarDestinoEditar());

		this.accesoDatos = acceso;
		this.mail = Mail.getInstance(accesoDatos);
		this.viajes_en_tabla = new ArrayList<Viaje>();
		this.ubicaciones_en_tabla = new ArrayList<>();
	}

	public void inicializar() {
//		if (this.viajes_en_tabla.size() == 0) {
			this.viajes_en_tabla = this.accesoDatos.obtenerViajes();
//		}
		llenarComboBoxPais();
		llenarComboBoxContinente();
		llenarComboBoxTransporte();
		realizarBusqueda(true);
	}

	private void ventanaAgregarViaje(ActionEvent c) {
		ventanaAgregarViaje.mostrarVentana();
	}

	private void ventanaEditarViaje(ActionEvent e) {
		try {
			Viaje viaje = viajeSeleccionado();
			if (viaje.getEstadoViaje().equals("ABIERTO")) {
				List<Pasaje> pasajesAsociados = recuperarViajesAsociados(viaje);
				llenarCampos(viaje);
				this.ubicacionOrigenEditar = viaje.getUbicacionOrigen();
				this.ubicacionDestinoEditar = viaje.getUbicacionDestino();

				if (tienePasajesAsociados(pasajesAsociados)) {
					if (estanTodosCancelados(pasajesAsociados)) {
						this.ventanaEditarViaje.mostrarEdicionTotal();
						this.ventanaEditarViaje.mostrarVentana();
					} else {
						this.ventanaEditarViaje.mostrarEdicionParcial();
						this.ventanaEditarViaje.mostrarVentana();
					}
				} else {
					this.ventanaEditarViaje.mostrarEdicionTotal();
					this.ventanaEditarViaje.mostrarVentana();
				}
			} else {
				JOptionPane.showMessageDialog(null, "Seleccione un viaje con estado ABIERTO para editar.");
			}
		} catch (ArrayIndexOutOfBoundsException ex) {
			JOptionPane.showMessageDialog(null, "Seleccione un viaje para editar.");
		}
	}

	private void llenarCampos(Viaje viaje) {
		String capacidad = "" + viaje.getCapacidad();
		java.util.Date fechaSalida = java.sql.Date.valueOf(viaje.getFechaSalida());

		Duration duracion = viaje.getHorasDeViaje();
		Duration duracionMinutos = duracion.minusHours(duracion.toHours());
		long minutos = duracionMinutos.toMinutes();

		BigDecimal precioTotal = viaje.getPrecio();
		precioTotal = precioTotal.add(viaje.getImpuesto());

		LocalTime horaSalida = viaje.getHoraSalida();
		Calendar calendar = Calendar.getInstance();
		calendar.clear();
		calendar.set(0, 0, 0, horaSalida.getHour(), horaSalida.getMinute(), horaSalida.getSecond());

		this.ventanaEditarViaje.getTxtOrigen().setText(viaje.getUbicacionOrigen().getCiudad());
		this.ventanaEditarViaje.getTxtDestino().setText(viaje.getUbicacionDestino().getCiudad());
		this.ventanaEditarViaje.getComboBoxTransporte().setSelectedItem(viaje.getMedioTransporte());
		this.ventanaEditarViaje.getTxtCapacidad().setText(capacidad);
		this.ventanaEditarViaje.getChooserFechaSalida().setDate(fechaSalida);
		this.ventanaEditarViaje.getSpinnerHoraSalida().setValue(calendar.getTime());
		this.ventanaEditarViaje.getSpinnerMinutoSalida().setValue(calendar.getTime());
		this.ventanaEditarViaje.getMinutoViaje().setValue(minutos);
		this.ventanaEditarViaje.getHoraViaje().setValue(viaje.getHorasDeViaje().toHours());
		this.ventanaEditarViaje.getTxtPrecio().setText(viaje.getPrecio().toString());
		this.ventanaEditarViaje.getTxtImpuesto().setText(viaje.getImpuesto().toString());
		this.ventanaEditarViaje.getTxtPrecioTotal().setText(precioTotal.toString());
	}

	private boolean tienePasajesAsociados(List<Pasaje> pasajesAsociados) {
		return pasajesAsociados.size() != 0;
	}

	private boolean estanTodosCancelados(List<Pasaje> pasajesAsociados) {
		boolean estanTodosCancelados = true;
		for (Pasaje pasajes : pasajesAsociados) {
			estanTodosCancelados = estanTodosCancelados && pasajes.getEstadoPasaje().equals("CANCELADO");
		}
		return estanTodosCancelados;
	}

	private List<Pasaje> recuperarViajesAsociados(Viaje viaje) {
		PasajeDAOSQL pasajeDAOSQL = new PasajeDAOSQL();

		return pasajeDAOSQL.findByViaje(viaje);
	}

	private Viaje addViaje() throws InvocationTargetException, IllegalAccessException {
		Viaje viaje = new Viaje(ubicacionOrigenAgregar, ubicacionDestinoAgregar,
				this.ventanaAgregarViaje.getTxtFechaSalida(), this.ventanaAgregarViaje.getTxtHoraSalida(),
				this.ventanaAgregarViaje.getCantidadHorasViaje(), this.ventanaAgregarViaje.getTxtCapacidad(),
				this.ventanaAgregarViaje.getTxtTransporte(), this.ventanaAgregarViaje.getTxtPrecio(),
				this.ventanaAgregarViaje.getTxtImpuesto());

		viaje.setCodigoReferencia(MetodosDominio.generarCodigoReferenia(viajes_en_tabla));
		return viaje;
	}

	private void guardarViaje(ActionEvent p) throws InvocationTargetException, IllegalAccessException {
		if (this.ventanaAgregarViaje.verificarCampo()) {
			Viaje viaje = addViaje();
			if (!viaje.getUbicacionOrigen().equals(viaje.getUbicacionDestino())) {
				if (this.accesoDatos.agregarViaje(viaje)) {
					viajes_en_tabla.add(viaje);
					realizarBusqueda(true);
					this.ventanaAgregarViaje.cerrar();
				} else {
					JOptionPane.showMessageDialog(null, "No se pudo agregar el viaje");
				}
			} else {
				JOptionPane.showMessageDialog(null, "Origen y destino no pueden ser iguales.");
			}
		}
	}

	private Viaje viajeConDatosDeVentanaEditar() {
		return new Viaje(ubicacionOrigenEditar, ubicacionDestinoEditar, this.ventanaEditarViaje.getFechaSalida(),
				this.ventanaEditarViaje.getHoraSalida(), this.ventanaEditarViaje.getCantidadHorasViaje(),
				this.ventanaEditarViaje.getCapacidad(), this.ventanaEditarViaje.getTxtTransporte(),
				this.ventanaEditarViaje.getPrecio(), this.ventanaEditarViaje.getImpuesto());
	}

	private void editarViaje(ActionEvent e) {
		Viaje viajeEnTabla = viajeSeleccionado();
		Viaje viaje = viajeConDatosDeVentanaEditar();
		viaje.setId(viajeEnTabla.getId());
		if (this.accesoDatos.editarViaje(viaje)) {
			setearViajePorOtro(viajeEnTabla, viaje);
			notificarEdicionAClientes(viaje);
			this.ventanaEditarViaje.cerrar();
			realizarBusqueda(true);
		} else {
			JOptionPane.showMessageDialog(null, "No se pudo editar el viaje");
		}
	}

	private void setearViajePorOtro(Viaje viajaAcambiar, Viaje viaje) {
		viajaAcambiar.setUbicacionOrigen(viaje.getUbicacionOrigen());
		viajaAcambiar.setUbicacionDestino(viaje.getUbicacionDestino());
		viajaAcambiar.setMedioTransporte(viaje.getMedioTransporte());
		viajaAcambiar.setCapacidad(viaje.getCapacidad());
		viajaAcambiar.setFechaSalida(viaje.getFechaSalida());
		viajaAcambiar.setHoraSalida(viaje.getHoraSalida());
		viajaAcambiar.setHorasDeViaje(viaje.getHorasDeViaje());
		viajaAcambiar.setPrecio(viaje.getPrecio());
		viajaAcambiar.setImpuesto(viaje.getImpuesto());
	}

	private void notificarEdicionAClientes(Viaje viaje) {
		List<Pasaje> pasajes = accesoDatos.findByViaje(viaje);

		DateTimeFormatter formatoDeFecha = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		LocalDate fechaDeSalida = viaje.getFechaSalida();
		String fechaDeSalidaConFormato = fechaDeSalida.format(formatoDeFecha);

		for (Pasaje pasaje : pasajes) {
			String cuerpoMail = "Estimade " + pasaje.getCliente().getApellido() + " " + pasaje.getCliente().getNombre()
					+ " , se le informa que el viaje con destino a " + viaje.getUbicacionDestino().getCiudad() + ", "
					+ viaje.getUbicacionDestino().getProvincia() + ", " + viaje.getUbicacionDestino().getPais()
					+ " fue actualizado.\n\n" + "La nueva fecha y hora de salida es " + fechaDeSalidaConFormato
					+ " a las " + viaje.getHoraSalida() + "hs.\n\n"
					+ "Si tiene algun problema, le pedimos que se acerque a alguno de nuestros locales.\n\n"
					+ "Muchas gracias.\n" + "Equipo Hermes.";

			mail = Mail.getInstance(accesoDatos);

			mail.enviarConGMail(pasaje.getCliente().getEmail(), "Viaje actualizado", cuerpoMail);
		}
	}

	private void cancelarViaje() throws InvocationTargetException, IllegalAccessException {
		try {
			Viaje viaje = viajeSeleccionado();
			if (viaje.getEstadoViaje().equals("ABIERTO")) {
				int confirm = JOptionPane.showOptionDialog(null, "¿Está seguro que quiere cancelar este viaje?",
						"Confirmacion", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, null);
				if (confirm == 0) {
					if (this.accesoDatos.cancelarViaje(viaje)) {
						notificarCancelacionAClientes(viaje);

						viaje.setEstado("CANCELADO");
						List<Pasaje> pasajesDeViaje = accesoDatos.findByViaje(viaje);
						for (Pasaje pasaje : pasajesDeViaje) {
							pasaje.setEstadoPasaje("CANCELADO");
							accesoDatos.updatePasaje(pasaje);
							ingresarGastoPorDevolucion(pasaje);
						}
						realizarBusqueda(true);
					} else {
						JOptionPane.showMessageDialog(null, "No se pudo cancelar el viaje");
					}
				}
			} else {
				JOptionPane.showMessageDialog(null, "Debe elegir un viaje con estado abierto.");
			}
		} catch (ArrayIndexOutOfBoundsException ex) {
			JOptionPane.showMessageDialog(null, "Seleccione un viaje.");
		}
	}

	private void ingresarGastoPorDevolucion(Pasaje pasaje) throws InvocationTargetException, IllegalAccessException {
		List<Operacion> operacionesDelPasaje = accesoDatos.findOperacionesByPasaje(pasaje);
		BigDecimal montoPagado = new BigDecimal(0);
		Local primerLocal = null;
		for (Operacion operacion : operacionesDelPasaje) {
			if (primerLocal == null) {
				primerLocal = operacion.getResponsable().getLocal();
			}
			montoPagado = montoPagado.add(operacion.getMonto());
		}
		if (montoPagado.compareTo(new BigDecimal(0)) > 0) {
			Gasto gasto = new Gasto(montoPagado, "DEVOLUCION", primerLocal, LocalDate.now());
			gasto.setCodigoReferencia(MetodosDominio.generarCodigoReferenia(accesoDatos.findAllGastos()));
			if (!accesoDatos.insertGasto(gasto)) {
				JOptionPane.showMessageDialog(null, "No se pudo guardar el gasto.");
			}
			Devolucion devolucion = new Devolucion(pasaje.getCliente(), montoPagado, 100);
			if (!accesoDatos.insertDevolucion(devolucion)) {
				JOptionPane.showMessageDialog(null, "No se pudo guardar la devolución.");
			}
		}
	}

	private Viaje viajeSeleccionado() throws ArrayIndexOutOfBoundsException {
		int fila = this.ventanaMainViajes.getTablaViajes().getSelectedRow();
		return (Viaje) this.ventanaMainViajes.getModelViajes().getValueAt(fila, 0);
	}

	private void notificarCancelacionAClientes(Viaje viaje) {
		List<Pasaje> pasajes = accesoDatos.findByViaje(viaje);

		DateTimeFormatter formatoDeFecha = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		LocalDate fechaDeSalida = viaje.getFechaSalida();
		String fechaDeSalidaConFormato = fechaDeSalida.format(formatoDeFecha);

		for (Pasaje pasaje : pasajes) {
			String cuerpoMail = "Estimade " + pasaje.getCliente().getApellido() + " " + pasaje.getCliente().getNombre()
					+ " , se le informa que el viaje con destino a " + viaje.getUbicacionDestino().getCiudad() + ", "
					+ viaje.getUbicacionDestino().getProvincia() + ", " + viaje.getUbicacionDestino().getPais()
					+ " y fecha de salida " + fechaDeSalidaConFormato + " ha sido cancelado.\n\n"
					+ "Si lo desea, puede acercarse a una de nuestras sucursales a retirar la devolución de su dinero."
					+ "Le pedimos disculpas por los inconvenientes.\n\n" + "Muchas gracias.\n" + "Equipo Hermes.";

			mail.enviarConGMail(pasaje.getCliente().getEmail(), "Viaje cancelado", cuerpoMail);
		}
	}

	private void llenarComboBoxTransporte() {
		this.ventanaAgregarViaje.getComboBoxTransporte().removeAllItems();
		this.ventanaEditarViaje.getComboBoxTransporte().removeAllItems();

		for (MedioTransporte transporte : this.accesoDatos.obtenerTransportes()) {
			if (transporte.getEstado().equals("HABILITADO")) {
				this.ventanaAgregarViaje.getComboBoxTransporte().addItem(transporte);
				this.ventanaEditarViaje.getComboBoxTransporte().addItem(transporte);
			}
		}
		this.ventanaAgregarViaje.getComboBoxTransporte().setSelectedIndex(-1);
	}

	private void llenarComboBoxPais() {
		if (this.ventanaMainViajes.getCbPaisOrigen().getItemCount() == 0
				|| this.ventanaMainViajes.getCbPaisDestino().getItemCount() == 0) {
			this.ventanaMainViajes.getCbPaisOrigen().removeAllItems();
			this.ventanaMainViajes.getCbPaisDestino().removeAllItems();
			String primero = "Seleccione un pais";
			this.ventanaMainViajes.getCbPaisOrigen().addItem(primero);
			this.ventanaMainViajes.getCbPaisDestino().addItem(primero);

			String[] paises = Locale.getISOCountries();

			for (String pais : paises) {
				Locale obj = new Locale("", pais);

				this.ventanaMainViajes.getCbPaisOrigen().addItem(obj.getDisplayCountry());
				this.ventanaMainViajes.getCbPaisDestino().addItem(obj.getDisplayCountry());
			}
			this.ventanaMainViajes.getCbPaisOrigen().setSelectedIndex(0);
			this.ventanaMainViajes.getCbPaisDestino().setSelectedIndex(0);
		}
	}

	private void llenarComboBoxContinente() {
		if (this.ventanaMainViajes.getCbContinenteOrigen().getItemCount() == 0
				|| this.ventanaMainViajes.getCbContinenteDestino().getItemCount() == 0) {
			this.ventanaMainViajes.getCbContinenteOrigen().removeAllItems();
			this.ventanaMainViajes.getCbContinenteDestino().removeAllItems();
			String primero = "Seleccione un continente";
			this.ventanaMainViajes.getCbContinenteOrigen().addItem(primero);
			this.ventanaMainViajes.getCbContinenteDestino().addItem(primero);

			for (String continente : this.accesoDatos.obtenerContinentes()) {
				this.ventanaMainViajes.getCbContinenteOrigen().addItem(continente);
				this.ventanaMainViajes.getCbContinenteDestino().addItem(continente);
			}
			this.ventanaMainViajes.getCbContinenteOrigen().setSelectedIndex(0);
			this.ventanaMainViajes.getCbContinenteDestino().setSelectedIndex(0);
		}
	}

	private void limpiarTabla() {
		this.ventanaMainViajes.getModelViajes().setRowCount(0);
		this.ventanaMainViajes.getModelViajes().setColumnCount(0);
		this.ventanaMainViajes.getModelViajes().setColumnIdentifiers(this.ventanaMainViajes.getNombreColumnas());
	}

	private void agregarFilaATabla(Viaje viaje) {
		DateTimeFormatter formatoDeFecha = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		LocalDate fechaDeSalida = viaje.getFechaSalida();
		String fechaDeSalidaConFormato = fechaDeSalida.format(formatoDeFecha);

		Object[] fila = { viaje, viaje.getUbicacionOrigen(), viaje.getUbicacionDestino(), fechaDeSalidaConFormato,
				viaje.getHoraSalida(), mostrarHoras(viaje), viaje.getCapacidad(), viaje.getMedioTransporte(),
				viaje.getPrecio(), viaje.getImpuesto(), viaje.getEstadoViaje() };

		this.ventanaMainViajes.getModelViajes().addRow(fila);
	}

	private String mostrarHoras(Viaje viaje) {
		Duration duracion = viaje.getHorasDeViaje();
		long horas = duracion.toHours();
		Duration duracionMinutos = duracion.minusHours(duracion.toHours());
		long minutos = duracionMinutos.toMinutes();
		String cantidadHoras = horas + " horas y " + minutos + " minutos";
		return cantidadHoras;
	}

	private void realizarBusqueda(boolean primeraEntrada) {
		Predicate<Viaje> filtro = filtroDeViaje();
		limpiarTabla();
		if (filtro != null) {
			List<Viaje> lista_filtrada = viajes_en_tabla.stream().filter(filtro).collect(Collectors.toList());
			if (lista_filtrada.size() > 0) {
				lista_filtrada.forEach(ubicacion -> agregarFilaATabla(ubicacion));
			} else {
				if (!primeraEntrada) {
					JOptionPane.showMessageDialog(null, "No se encontraron viajes.");
				}
			}
		} else {
			viajes_en_tabla.forEach(ubicacion -> agregarFilaATabla(ubicacion));
		}
	}

	private String radioButtonSeleccionado() {
		if (ventanaMainViajes.getRadioAbierto().isSelected()) {
			return "ABIERTO";
		}
		if (ventanaMainViajes.getRadioCancelado().isSelected()) {
			return "CANCELADO";
		}
		if (ventanaMainViajes.getRadioFinalizado().isSelected()) {
			return "FINALIZADO";
		}
		return "TODOS";
	}

	private Predicate<Viaje> filtroDeViaje() {
		List<Predicate<Viaje>> lista_predicados = new ArrayList<>();

		String estado = radioButtonSeleccionado();
		if (!estado.equals("TODOS")) {
			Predicate<Viaje> predicadoEstado = viaje -> viaje.getEstadoViaje().equals(estado);
			lista_predicados.add(predicadoEstado);
		}

		String codigoDeReferencia = this.ventanaMainViajes.getTxtBusquedaCodigo().getText();
		if (!codigoDeReferencia.isEmpty()) {
			Predicate<Viaje> filtroCodigo = viaje -> viaje.getCodigoReferencia().equals(codigoDeReferencia);
			lista_predicados.add(filtroCodigo);
		}

		String ciudadDestino = this.ventanaMainViajes.getTxtCiudadDestino().getText().toLowerCase();
		if (!ciudadDestino.equals("")) {
			Predicate<Viaje> predicadoCiudad = viaje -> viaje.getUbicacionDestino().getCiudad().toLowerCase()
					.indexOf(ciudadDestino) > -1;
			lista_predicados.add(predicadoCiudad);
		}

		String provinciaDestino = this.ventanaMainViajes.getTxtProvinciaDestino().getText().toLowerCase();
		if (!provinciaDestino.equals("")) {
			Predicate<Viaje> predicadoProvincia = viaje -> viaje.getUbicacionDestino().getProvincia().toLowerCase()
					.indexOf(provinciaDestino) > -1;
			lista_predicados.add(predicadoProvincia);
		}

		if (this.ventanaMainViajes.getCbPaisDestino().getSelectedIndex() > 0) {
			String paisDestino = this.ventanaMainViajes.getCbPaisDestino().getSelectedItem().toString();
			Predicate<Viaje> predicadoPais = viaje -> viaje.getUbicacionDestino().getPais().equals(paisDestino);
			lista_predicados.add(predicadoPais);
		}

		if (this.ventanaMainViajes.getCbContinenteDestino().getSelectedIndex() > 0) {
			String continenteDestino = this.ventanaMainViajes.getCbContinenteDestino().getSelectedItem().toString();
			Predicate<Viaje> predicadoContinente = viaje -> viaje.getUbicacionDestino().getContinente()
					.equals(continenteDestino);
			lista_predicados.add(predicadoContinente);
		}

		String ciudadOrigen = this.ventanaMainViajes.getTxtCiudadOrigen().getText().toLowerCase();
		if (!ciudadOrigen.equals("")) {
			Predicate<Viaje> predicadoCiudad = viaje -> viaje.getUbicacionOrigen().getCiudad().toLowerCase()
					.indexOf(ciudadOrigen) > -1;
			lista_predicados.add(predicadoCiudad);
		}

		String provinciaOrigen = this.ventanaMainViajes.getTxtProvinciaOrigen().getText().toLowerCase();
		if (!provinciaOrigen.equals("")) {
			Predicate<Viaje> predicadoProvincia = viaje -> viaje.getUbicacionOrigen().getProvincia().toLowerCase()
					.indexOf(provinciaOrigen) > -1;
			lista_predicados.add(predicadoProvincia);
		}

		if (this.ventanaMainViajes.getCbPaisOrigen().getSelectedIndex() > 0) {
			String paisOrigen = this.ventanaMainViajes.getCbPaisOrigen().getSelectedItem().toString();
			Predicate<Viaje> predicadoPais = viaje -> viaje.getUbicacionOrigen().getPais().equals(paisOrigen);
			lista_predicados.add(predicadoPais);
		}

		if (this.ventanaMainViajes.getCbContinenteOrigen().getSelectedIndex() > 0) {
			String continenteOrigen = this.ventanaMainViajes.getCbContinenteOrigen().getSelectedItem().toString();
			Predicate<Viaje> predicadoContinente = viaje -> viaje.getUbicacionOrigen().getContinente()
					.equals(continenteOrigen);
			lista_predicados.add(predicadoContinente);
		}

		if (lista_predicados.size() == 1) {
			return lista_predicados.get(0);
		} else if (lista_predicados.size() > 1) {
			Predicate<Viaje> acumulador = lista_predicados.get(0);
			lista_predicados.remove(0);
			for (Predicate<Viaje> predicado : lista_predicados) {
				acumulador = acumulador.and(predicado);
			}
			return acumulador;
		}
		return null;
	}

	private void buscarUbicacionAgregar() {
		this.ubicaciones_en_tabla.clear();
		limpiarTablaUbicacionesAgregar();

		String ciudad = this.ventanaAgregarViaje.getTxtBuscarCiudad().getText().toLowerCase();
		String provincia = this.ventanaAgregarViaje.getTxtBuscarProvincia().getText().toLowerCase();
		String pais = this.ventanaAgregarViaje.getTxtBuscarPais().getText().toLowerCase();
		String continente = this.ventanaAgregarViaje.getTxtBuscarContinente().getText().toLowerCase();
		String tag = this.ventanaAgregarViaje.getTxtBuscarTag().getText().toLowerCase();

		Predicate<Ubicacion> filtro = filtroDeUbicacion(ciudad, provincia, pais, continente, tag);
		this.accesoDatos.obtenerUbicaciones().stream()
				.filter(ubicacion -> (filtro.test(ubicacion) && ubicacion.getEstado().equals("HABILITADO")))
				.forEach(ubicacion -> {
					Object[] fila = armarFila(ubicacion);
					this.ventanaAgregarViaje.getModelUbicacion().addRow(fila);
					this.ubicaciones_en_tabla.add(ubicacion);
				});
		if (this.ubicaciones_en_tabla.isEmpty()) {
			JOptionPane.showMessageDialog(null, "No se encontraron ubicaciones con los parametros especificados");
		}
	}

	private void limpiarTablaUbicacionesAgregar() {
		this.ventanaAgregarViaje.getModelUbicacion().setRowCount(0);
		this.ventanaAgregarViaje.getModelUbicacion().setColumnCount(0);
		this.ventanaAgregarViaje.getModelUbicacion()
				.setColumnIdentifiers(this.ventanaAgregarViaje.getColumnasDeUbicacion());
	}

	private void buscarUbicacionEditar() {
		this.ubicaciones_en_tabla.clear();
		limpiarTablaUbicacionesEditar();

		String ciudad = this.ventanaEditarViaje.getTxtBuscarCiudad().getText().toLowerCase();
		String provincia = this.ventanaEditarViaje.getTxtBuscarProvincia().getText().toLowerCase();
		String pais = this.ventanaEditarViaje.getTxtBuscarPais().getText().toLowerCase();
		String continente = this.ventanaEditarViaje.getTxtBuscarContinente().getText().toLowerCase();
		String tag = this.ventanaEditarViaje.getTxtBuscarTag().getText().toLowerCase();

		Predicate<Ubicacion> filtro = filtroDeUbicacion(ciudad, provincia, pais, continente, tag);
		this.accesoDatos.obtenerUbicaciones().stream()
				.filter(ubicacion -> (filtro.test(ubicacion) && ubicacion.getEstado().equals("HABILITADO")))
				.forEach(ubicacion -> {
					Object[] fila = armarFila(ubicacion);
					this.ventanaEditarViaje.getModelUbicacion().addRow(fila);
					this.ubicaciones_en_tabla.add(ubicacion);
				});
		if (this.ubicaciones_en_tabla.isEmpty()) {
			JOptionPane.showMessageDialog(null, "No se encontraron ubicaciones con los parametros especificados");
		}
	}

	private void limpiarTablaUbicacionesEditar() {
		this.ventanaEditarViaje.getModelUbicacion().setRowCount(0);
		this.ventanaEditarViaje.getModelUbicacion().setColumnCount(0);
		this.ventanaEditarViaje.getModelUbicacion()
				.setColumnIdentifiers(this.ventanaEditarViaje.getColumnasDeUbicacion());
	}

	private Predicate<Ubicacion> filtroDeUbicacion(String ciudad, String provincia, String pais, String continente,
			String tag) {
		List<Predicate<Ubicacion>> lista_filtros = new ArrayList<>();

		if (!ciudad.equals("")) {
			Predicate<Ubicacion> predicadoCiudad = ubicacion -> ubicacion.getCiudad().toLowerCase()
					.indexOf(ciudad) > -1;
			lista_filtros.add(predicadoCiudad);
		}

		if (!provincia.equals("")) {
			Predicate<Ubicacion> predicadoProvincia = ubicacion -> ubicacion.getProvincia().toLowerCase()
					.indexOf(provincia) > -1;
			lista_filtros.add(predicadoProvincia);
		}

		if (!pais.equals("")) {
			Predicate<Ubicacion> predicadoPais = ubicacion -> ubicacion.getPais().toLowerCase().indexOf(pais) > -1;
			lista_filtros.add(predicadoPais);
		}

		if (!continente.equals("")) {
			Predicate<Ubicacion> predicadoContinente = ubicacion -> ubicacion.getContinente().toLowerCase()
					.indexOf(continente) > -1;
			lista_filtros.add(predicadoContinente);
		}

		if (!tag.isEmpty()) {
			Predicate<Ubicacion> filtroTags = ubicacion -> ubicacion.getTags().stream()
					.filter(tags -> tags.toLowerCase().indexOf(tag) > -1).count() > 0;
			lista_filtros.add(filtroTags);
		}

		if (lista_filtros.size() == 1) {
			return lista_filtros.get(0);
		} else if (lista_filtros.size() > 1) {
			Predicate<Ubicacion> acumulador = lista_filtros.get(0);
			lista_filtros.remove(0);
			for (Predicate<Ubicacion> predicado : lista_filtros) {
				acumulador = acumulador.and(predicado);
			}
			return acumulador;
		}
		return (ubicacion -> ubicacion != null);
	}

	private Object[] armarFila(Ubicacion ubicacion) {
		Object[] fila = { ubicacion, ubicacion.getProvincia(), ubicacion.getPais(), ubicacion.getContinente(),
				ubicacion.getTags() };
		return fila;
	}

	private void seleccionarOrigenAgregar() {
		try {
			Ubicacion ubicacionSeleccionada = ubicacionSeleccionada("agregar");
			this.ubicacionOrigenAgregar = ubicacionSeleccionada;
			this.ventanaAgregarViaje.getTxtOrigen().setText(ubicacionSeleccionada.getCiudad());
		} catch (ArrayIndexOutOfBoundsException ex) {
			JOptionPane.showMessageDialog(null, "Se debe seleccionar una ubicacion!", "¡Error!",
					JOptionPane.ERROR_MESSAGE);
		}
	}

	private void seleccionarDestinoAgregar() {
		try {
			Ubicacion ubicacionSeleccionada = ubicacionSeleccionada("agregar");
			this.ubicacionDestinoAgregar = ubicacionSeleccionada;
			this.ventanaAgregarViaje.getTxtDestino().setText(ubicacionSeleccionada.getCiudad());
		} catch (ArrayIndexOutOfBoundsException ex) {
			JOptionPane.showMessageDialog(null, "Se debe seleccionar una ubicacion!", "¡Error!",
					JOptionPane.ERROR_MESSAGE);
		}
	}

	private void seleccionarOrigenEditar() {
		try {
			Ubicacion ubicacionSeleccionada = ubicacionSeleccionada("editar");
			this.ubicacionOrigenEditar = ubicacionSeleccionada;
			this.ventanaEditarViaje.getTxtOrigen().setText(ubicacionSeleccionada.getCiudad());
		} catch (ArrayIndexOutOfBoundsException ex) {
			JOptionPane.showMessageDialog(null, "Se debe seleccionar una ubicacion!", "¡Error!",
					JOptionPane.ERROR_MESSAGE);
		}
	}

	private void seleccionarDestinoEditar() {
		try {
			Ubicacion ubicacionSeleccionada = ubicacionSeleccionada("editar");
			this.ubicacionDestinoEditar = ubicacionSeleccionada;
			this.ventanaEditarViaje.getTxtDestino().setText(ubicacionSeleccionada.getCiudad());
		} catch (ArrayIndexOutOfBoundsException ex) {
			JOptionPane.showMessageDialog(null, "Se debe seleccionar una ubicacion!", "¡Error!",
					JOptionPane.ERROR_MESSAGE);
		}
	}

	private Ubicacion ubicacionSeleccionada(String ventana) throws ArrayIndexOutOfBoundsException {
		if (ventana.equals("agregar")) {
			int fila = this.ventanaAgregarViaje.getTablaUbicaciones().getSelectedRow();
			return (Ubicacion) this.ventanaAgregarViaje.getModelUbicacion().getValueAt(fila, 0);
		} else {
			int fila = this.ventanaEditarViaje.getTablaUbicaciones().getSelectedRow();
			return (Ubicacion) this.ventanaEditarViaje.getModelUbicacion().getValueAt(fila, 0);

		}
	}
}
