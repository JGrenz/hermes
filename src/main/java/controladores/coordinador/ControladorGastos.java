package controladores.coordinador;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.swing.JOptionPane;

import controladores.ControladorLogin;
import dominio.Gasto;
import dominio.Local;
import dominio.MetodosDominio;
import modelo.AccesoDatos;
import vistas.coordinador.gastos.Main_Gastos;
import vistas.coordinador.gastos.Ventana_AgregarGasto;
import vistas.coordinador.gastos.Ventana_EditarGasto;

public class ControladorGastos {

	private Main_Gastos ventanaMainGastos;
	private Ventana_AgregarGasto ventanaAgregarGasto;
	private Ventana_EditarGasto ventanaEditarGasto;
	private AccesoDatos accesoDatos;
	private List<Gasto> gastos_en_tabla;

	public ControladorGastos(Main_Gastos ventanaMainGastos, AccesoDatos acceso) {
		this.ventanaMainGastos = ventanaMainGastos;
		ventanaAgregarGasto = Ventana_AgregarGasto.getInstance();
		ventanaEditarGasto = Ventana_EditarGasto.getInstance();

		this.ventanaMainGastos.getBtnAgregar().addActionListener(c -> ventanaAgregarGasto());
		this.ventanaMainGastos.getBtnEditar().addActionListener(e -> ventanaEditarGasto());
		this.ventanaMainGastos.getBtnInhabilitar().addActionListener(b -> InhabilitarGasto());

		this.ventanaMainGastos.getRadioTodos().addActionListener(a -> realizarBusqueda(false));
		this.ventanaMainGastos.getRadioHabilitados().addActionListener(a -> realizarBusqueda(false));
		this.ventanaMainGastos.getRadioInhabilitados().addActionListener(a -> realizarBusqueda(false));
		this.ventanaMainGastos.getBtnRealizarBusqueda().addActionListener(b -> realizarBusqueda(false));

		this.ventanaAgregarGasto.getBtnConfirmar().addActionListener(c -> {
			try {
				guardarGasto();
			} catch (InvocationTargetException | IllegalAccessException e) {
				e.printStackTrace();
			}
		});

		this.ventanaEditarGasto.getBtnConfirmar().addActionListener(e -> editarGasto());

		this.accesoDatos = acceso;

		this.gastos_en_tabla = new ArrayList<>();
	}

	public void inicializar() {
//		if (this.gastos_en_tabla.size() == 0) {
			this.gastos_en_tabla = this.accesoDatos.findAllGastos();
//		}
		cargarLocales();
		realizarBusqueda(true);
	}

	private void InhabilitarGasto() {
		try {

			Gasto gasto = datosDeFilaSeleccionada();
			if (gasto.getEstado().equals("HABILITADO")) {
				cambiarEstado(gasto);
			} else {
				JOptionPane.showMessageDialog(null, "No se puede volver a habilitar un gasto", "¡Advertencia!",
						JOptionPane.WARNING_MESSAGE);
			}
		} catch (ArrayIndexOutOfBoundsException ex) {
			JOptionPane.showMessageDialog(null, "Seleccione un gasto para cancelar.");
		}

	}

	private void cambiarEstado(Gasto gasto) {
		int confirm = JOptionPane.showOptionDialog(null, "¿Está seguro que quiere cancelar este gasto?", "Confirmacion",
				JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, null);
		if (confirm == 0) {
			if (this.accesoDatos.cancelarGasto(gasto)) {
				gasto.setEstado("INHABILITADO");
				realizarBusqueda(true);
			} else {
				JOptionPane.showMessageDialog(null, "No se pudo cancelar el gasto.");
			}
		}
	}

	private void cargarLocales() {
		if (ventanaMainGastos.getCBLocal().getItemCount() == 0) {
			ventanaMainGastos.getCBLocal().addItem("Seleccionar local");
			List<Local> locales = accesoDatos.obtenerLocales();
			for (Local local : locales) {
				ventanaMainGastos.getCBLocal().addItem(local.getNombre());
			}
			ventanaMainGastos.getCBLocal().setSelectedIndex(0);
		}
	}

	private void ventanaAgregarGasto() {
		ventanaAgregarGasto.mostrarVentana();
	}

	private void ventanaEditarGasto() {
		try {
			Gasto gasto = datosDeFilaSeleccionada();
			if (gasto.getEstado().equals("HABILITADO")) {
				llenarVentanaEditarGasto(gasto);
				this.ventanaEditarGasto.mostrarVentana();
			} else {
				JOptionPane.showMessageDialog(null, "Seleccione un gasto con estado HABILITADO para editar.");
			}
		} catch (ArrayIndexOutOfBoundsException ex) {
			JOptionPane.showMessageDialog(null, "Seleccione un gasto para editar");
		}
	}

	private void llenarVentanaEditarGasto(Gasto gasto) {
		java.util.Date fechaGasto = java.sql.Date.valueOf(gasto.getFechaGasto());
		
		this.ventanaEditarGasto.getTxtMonto().setText(gasto.getMonto().toString());
		this.ventanaEditarGasto.getCBTipo().setSelectedItem(gasto.getTipoGasto());
		this.ventanaEditarGasto.getChooserFechaPago().setDate(fechaGasto);
	}

	private void guardarGasto() throws InvocationTargetException, IllegalAccessException {
		if (this.ventanaAgregarGasto.verificarCampo()) {
			Gasto gasto = crearGasto();
			gasto.setCodigoReferencia(MetodosDominio.generarCodigoReferenia(gastos_en_tabla));
			if (this.accesoDatos.insertGasto(gasto)) {
				gastos_en_tabla.add(gasto);
				agregarFilaATabla(gasto);
				this.ventanaAgregarGasto.cerrar();
			} else {
				JOptionPane.showMessageDialog(null, "No se pudo agregar el gasto");
			}
		}
	}

	private Gasto crearGasto() {
		BigDecimal monto = new BigDecimal(ventanaAgregarGasto.getTxtMonto().getText().toString());
		String tipoGasto = ventanaAgregarGasto.getCBTipo().getSelectedItem().toString().toUpperCase();
		LocalDate fechaGasto = ventanaAgregarGasto.getTxtFechaPago();
		Local local = ControladorLogin.id_logeado().getLocal();

		Gasto gasto = new Gasto(monto, tipoGasto, local, fechaGasto);

		return gasto;
	}

	private void editarGasto() {
		Gasto gasto = datosDeFilaSeleccionada();

		BigDecimal monto = new BigDecimal(ventanaEditarGasto.getTxtMonto().getText().toString());
		gasto.setMonto(monto);
		gasto.setTipoGasto(ventanaEditarGasto.getCBTipo().getSelectedItem().toString().toUpperCase());
		gasto.setFechaGasto(ventanaEditarGasto.getTxtFechaGasto());

		if (accesoDatos.updateGasto(gasto)) {
			actualizarGastoEnArreglo(gasto);
			realizarBusqueda(true);
			ventanaEditarGasto.cerrar();
		} else {
			JOptionPane.showMessageDialog(null, "No se pudo actualizar el gasto.");
		}
	}

	private void actualizarGastoEnArreglo(Gasto gasto) {
		for (Gasto gastoEnTabla : gastos_en_tabla) {
			if (gastoEnTabla.equals(gasto)) {
				gastoEnTabla = gasto;
			}
		}
	}

	private void realizarBusqueda(boolean primeraEntrada) {
		limpiarTabla();
		Predicate<Gasto> filtro = filtrosGastos();
		if (filtro != null) {
			List<Gasto> lista_filtrada = this.gastos_en_tabla.stream().filter(filtro).collect(Collectors.toList());

			if (lista_filtrada.size() > 0) {

				lista_filtrada.forEach(oferta -> agregarFilaATabla(oferta));
			} else {
				if (!primeraEntrada) {
					JOptionPane.showMessageDialog(null, "No se encontraron gastos con los parámetros especificados.");
				}
			}
		} else {
			gastos_en_tabla.forEach(gasto -> agregarFilaATabla(gasto));
		}
	}

	private Predicate<Gasto> filtrosGastos() {
		List<Predicate<Gasto>> lista_filtros = new ArrayList<Predicate<Gasto>>();

		String estado = radioButtonSeleccionado();
		if (!estado.equals("TODOS")) {
			Predicate<Gasto> filtroEstado = gasto -> gasto.getEstado().equals(estado);
			lista_filtros.add(filtroEstado);
		}

		String codigo = this.ventanaMainGastos.getTxtCodigoReferencia().getText();
		if (!codigo.equals("")) {
			Predicate<Gasto> filtroCodigo = gasto -> gasto.getCodigoReferencia().indexOf(codigo) > -1;
			lista_filtros.add(filtroCodigo);
		}

		String tipo = this.ventanaMainGastos.getCBTipoEvento().getSelectedItem().toString().toUpperCase();
		if (!tipo.equals("SELECCIONAR TIPO")) {
			Predicate<Gasto> predicadoTipo = gasto -> gasto.getTipoGasto().indexOf(tipo) > -1;
			lista_filtros.add(predicadoTipo);
		}

		String local = this.ventanaMainGastos.getCBLocal().getSelectedItem().toString();
		if (!local.equals("Seleccionar local")) {
			Predicate<Gasto> predicadoLocal = gasto -> gasto.getLocal().getNombre().indexOf(local) > -1;
			lista_filtros.add(predicadoLocal);
		}

		try {
			LocalDate fechaDesde = this.ventanaMainGastos.getTxtFechaDesde().getDate().toInstant()
					.atZone(ZoneId.systemDefault()).toLocalDate();
			Predicate<Gasto> fechaBase = gasto -> (gasto.getFechaCreacion().toLocalDate().isEqual(fechaDesde)
					|| gasto.getFechaCreacion().toLocalDate().isAfter(fechaDesde));
			lista_filtros.add(fechaBase);
		} catch (NullPointerException ex) {
		}

		try {
			LocalDate fechaHasta = this.ventanaMainGastos.getTxtFechaHasta().getDate().toInstant()
					.atZone(ZoneId.systemDefault()).toLocalDate();
			if (fechaHasta != null) {
				Predicate<Gasto> fechaLimite = gasto -> (gasto.getFechaCreacion().toLocalDate().isEqual(fechaHasta)
						|| gasto.getFechaCreacion().toLocalDate().isBefore(fechaHasta));
				lista_filtros.add(fechaLimite);
			}
		} catch (IllegalArgumentException | NullPointerException ex) {
		}

		String precioMinimo = this.ventanaMainGastos.getTxtImporteDesde().getText();
		if (!precioMinimo.equals("")) {
			try {
				BigDecimal minimo = new BigDecimal(precioMinimo);
				Predicate<Gasto> precioBase = gasto -> gasto.getMonto().compareTo(minimo) >= 0;
				lista_filtros.add(precioBase);
			} catch (IllegalArgumentException il) {
				JOptionPane.showMessageDialog(null, "Lo ingresado en el precio minimo no es válido");
				return null;
			}
		}

		String precioMaximo = this.ventanaMainGastos.getTxtImporteHasta().getText();
		if (!precioMaximo.equals("")) {
			try {
				BigDecimal maximo = new BigDecimal(precioMaximo);
				Predicate<Gasto> precioLimite = gasto -> gasto.getMonto().compareTo(maximo) <= 0;
				lista_filtros.add(precioLimite);
			} catch (IllegalArgumentException il) {
				JOptionPane.showMessageDialog(null, "Lo ingresado en el precio maximo no es válido");
				return null;
			}
		}

		if (lista_filtros.size() == 1) {
			return lista_filtros.get(0);
		} else if (lista_filtros.size() > 1) {
			Predicate<Gasto> acumulador = lista_filtros.get(0);
			lista_filtros.remove(0);
			for (Predicate<Gasto> predicado : lista_filtros) {
				acumulador = acumulador.and(predicado);
			}
			return acumulador;
		}
		return null;
	}

	public String radioButtonSeleccionado() {
		if (ventanaMainGastos.getRadioHabilitados().isSelected()) {
			return "HABILITADO";
		}
		if (ventanaMainGastos.getRadioInhabilitados().isSelected()) {
			return "INHABILITADO";
		}
		return "TODOS";
	}

	private void agregarFilaATabla(Gasto gasto) {
		DateTimeFormatter formatoDeFechaHora = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
		DateTimeFormatter formatoDeFecha = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		LocalDateTime fechaDeCreacion = gasto.getFechaCreacion();
		String fechaDeCreacionConFormato = fechaDeCreacion.format(formatoDeFechaHora);
		LocalDate fechaGasto = gasto.getFechaGasto();
		String fechaGastoConFormato = fechaGasto.format(formatoDeFecha);

		Object[] fila = { gasto, gasto.getMonto(), gasto.getTipoGasto(), gasto.getEstado(), gasto.getLocal().getNombre(),
				fechaDeCreacionConFormato, fechaGastoConFormato };
		this.ventanaMainGastos.getModelGastos().addRow(fila);
	}

	private Gasto datosDeFilaSeleccionada() throws ArrayIndexOutOfBoundsException {
		int index = this.ventanaMainGastos.getTablaGastos().getSelectedRow();
		return (Gasto) this.ventanaMainGastos.getModelGastos().getValueAt(index, 0);
	}

	private void limpiarTabla() {
		this.ventanaMainGastos.getModelGastos().setRowCount(0);
		this.ventanaMainGastos.getModelGastos().setColumnCount(0);
		this.ventanaMainGastos.getModelGastos().setColumnIdentifiers(this.ventanaMainGastos.getNombreColumnas());
	}
}