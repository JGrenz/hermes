package controladores.administrador;

import static javax.swing.JFileChooser.APPROVE_OPTION;

import java.io.*;
import java.util.Properties;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;

import modelo.AccesoDatos;
import vistas.administrador.backup.*;

public class ControladorBackup {
	private Main_Backup ventanaMainBackup;
	private String ip, puerto, bdd, usuario, contraseña;
	// private AccesoDatos accesoDatos;

	public ControladorBackup(Main_Backup ventanaMainBackup, AccesoDatos accesoDatos) {
		this.ventanaMainBackup = ventanaMainBackup;
		// this.accesoDatos = accesoDatos;
		this.ventanaMainBackup.getBtnRutaExportar().addActionListener(i -> {
			try {
				guardarBackup();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (UnsupportedLookAndFeelException e) {
				e.printStackTrace();
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		});
		this.ventanaMainBackup.getBtnExportar().addActionListener(i -> backup());
		this.ventanaMainBackup.getBtnRutaImportar().addActionListener(i -> cargarBackup());
		this.ventanaMainBackup.getBtnImportar().addActionListener(i -> restore());

	}

	public void inicializar() {
		leerProperties();
	}

//	private void actualizarVentana() {
//		.revalidate();
//		.repaint();
//	}

	public void guardarBackup() throws ClassNotFoundException, UnsupportedLookAndFeelException, InstantiationException,
			IllegalAccessException {
		UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		javax.swing.JFileChooser fileChooser = new javax.swing.JFileChooser();
		fileChooser.showSaveDialog(null);
//		fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		String ruta = "";
		try {
//			if (fileChooser.showSaveDialog(null) == APPROVE_OPTION) {
			ruta = fileChooser.getSelectedFile().getAbsolutePath();
			// Aqui ya tiens la ruta ahora puedes crear un fichero con esa ruta y escribir
			// lo que quieras..
			this.ventanaMainBackup.getTxtRutaExportar().setText(ruta);
//			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private void leerProperties() {
		Properties properties = new Properties();
		try {
			FileReader fileReader = new FileReader("BDD.properties");
			properties.load(fileReader);
		} catch (IOException e) {
			e.printStackTrace();
		}

		this.ip = properties.getProperty("host");
		this.puerto = properties.getProperty("puerto");
		this.bdd = properties.getProperty("baseDatos");
		this.usuario = properties.getProperty("usuario");
		this.contraseña = properties.getProperty("contrasenia");
	}

	private void backup() {
		String ruta = this.ventanaMainBackup.getTxtRutaExportar().getText();
		if (!ruta.equals("")) {
			try {
				Process p = Runtime.getRuntime().exec(
						new String[] { "mysqldump", "-u", usuario, "-p" + contraseña, bdd, "-h", ip, "-P", puerto });

				InputStream is = p.getInputStream();
				FileOutputStream fos = new FileOutputStream(ruta + ".sql");
				byte[] buffer = new byte[1000];

				int leido = is.read(buffer);
				while (leido > 0) {
					fos.write(buffer, 0, leido);
					leido = is.read(buffer);
				}
				fos.close();
				JOptionPane.showMessageDialog(null, "Backup realizado con éxito.", "¡Aviso!",
						JOptionPane.INFORMATION_MESSAGE);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			JOptionPane.showMessageDialog(null, "Seleccione la ruta en donde se guardará.", "¡Advertencia!",
					JOptionPane.WARNING_MESSAGE);
		}
	}

	private void cargarBackup() {
		javax.swing.JFileChooser fileChooser = new javax.swing.JFileChooser();
		FileNameExtensionFilter filter = new FileNameExtensionFilter("SQL", "sql");
		fileChooser.setFileFilter(filter);
		String archivo = "";
		try {
			if (fileChooser.showOpenDialog(null) == APPROVE_OPTION) {
				archivo = fileChooser.getSelectedFile().getPath();
				this.ventanaMainBackup.getTxtRutaImportar().setText(archivo);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private void restore() {
		String archivo = this.ventanaMainBackup.getTxtRutaImportar().getText();
		if (!archivo.equals("")) {
			try {
				Process p = Runtime.getRuntime()
						.exec(new String[] { "mysql", "-u", usuario, "-p" + contraseña, bdd, "-h", ip, "-P", puerto });

				OutputStream os = p.getOutputStream();
				FileInputStream fis = new FileInputStream(archivo);
				byte[] buffer = new byte[1000];

				int leido = fis.read(buffer);
				while (leido > 0) {
					os.write(buffer, 0, leido);
					leido = fis.read(buffer);
				}
				os.flush();
				os.close();
				fis.close();
				JOptionPane.showMessageDialog(null, "Carga de backup realizada con éxito.", "¡Aviso!",
						JOptionPane.INFORMATION_MESSAGE);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			JOptionPane.showMessageDialog(null, "Seleccione la ruta donde se encuentra el archivo.", "¡Advertencia!",
					JOptionPane.WARNING_MESSAGE);
		}
	}
}
