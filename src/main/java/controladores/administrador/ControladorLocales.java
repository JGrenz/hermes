package controladores.administrador;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import dominio.Local;
import modelo.AccesoDatos;
import vistas.administrador.locales.Main_Locales;
import vistas.administrador.locales.Ventana_AgregarLocal;
import vistas.administrador.locales.Ventana_EditarLocal;

import javax.swing.*;

public class ControladorLocales {
	private Main_Locales ventanaMainLocales;
	private Ventana_AgregarLocal ventanaAgregarLocal;
	private Ventana_EditarLocal ventanaEditarLocal;
	private AccesoDatos accesoDatos;
	private List<Local> locales_en_tabla;

	public ControladorLocales(Main_Locales ventanaMainLocales, AccesoDatos accesoDatos) {
		this.ventanaMainLocales = ventanaMainLocales;
		this.ventanaAgregarLocal = Ventana_AgregarLocal.getInstance();
		this.ventanaEditarLocal = Ventana_EditarLocal.getInstance();

		this.ventanaMainLocales.getBtnAgregar().addActionListener(a -> ventanaAgregarLocal(a));
		this.ventanaMainLocales.getBtnEditar().addActionListener(e -> ventanaEditarLocal(e));
		this.ventanaMainLocales.getBtnBorrar().addActionListener(e -> HabilitarInhabilitar());

		this.ventanaMainLocales.getRadioTodos().addActionListener(e -> realizarBusqueda(false));
		this.ventanaMainLocales.getRadioHabilitados().addActionListener(e -> realizarBusqueda(false));
		this.ventanaMainLocales.getRadioInhabilitados().addActionListener(e -> realizarBusqueda(false));

		this.ventanaMainLocales.getBtnRealizarBusqueda().addActionListener(e -> realizarBusqueda(false));

		this.ventanaAgregarLocal.getBtnConfirmar().addActionListener(a -> guardarLocal());
		this.ventanaEditarLocal.getBtnConfirmar().addActionListener(e -> editarLocal());

		this.accesoDatos = accesoDatos;
		this.locales_en_tabla = new ArrayList<Local>();
	}

	public void inicializar() {
		llenarComboBoxProvincia();
		this.locales_en_tabla = accesoDatos.obtenerLocales();
//		cargarArreglo();
		realizarBusqueda(true);
	}

//	private void cargarArreglo() {
//		if (this.locales_en_tabla.size() == 0) {
//			this.locales_en_tabla = accesoDatos.obtenerLocales();
//		}
//	}

	private void realizarBusqueda(boolean primeraEntrada) {
		Predicate<Local> filtro = filtrosSeleccionados();
		limpiarTabla();
		if (filtro != null) {
			List<Local> lista_filtrada = this.locales_en_tabla.stream().filter(filtro).collect(Collectors.toList());

			if (lista_filtrada.size() > 0) {
				lista_filtrada.forEach(local -> agregarFilaATabla(local));
			} else {
				if (!primeraEntrada) {
					JOptionPane.showMessageDialog(null, "No se encontraron locales.", "¡Aviso!",
							JOptionPane.WARNING_MESSAGE);
				}
			}
		} else {
			locales_en_tabla.forEach(local -> agregarFilaATabla(local));
		}
	}

	private Predicate<Local> filtrosSeleccionados() {
		List<Predicate<Local>> lista_filtros = new ArrayList<>();

		String estado = radioButtonSeleccionado();
		if (!estado.equals("TODOS")) {
			Predicate<Local> filtroEstado = local -> local.getEstado().equals(estado);
			lista_filtros.add(filtroEstado);
		}

		String nombre = this.ventanaMainLocales.getTxtNombre().getText().toLowerCase();
		if (!nombre.isEmpty()) {
			Predicate<Local> filtroNombre = local -> local.getNombre().toLowerCase().indexOf(nombre) > -1;
			lista_filtros.add(filtroNombre);
		}

		String provincia = this.ventanaMainLocales.getTxtProvincia().getText().toLowerCase();
		if (!provincia.isEmpty()) {
			Predicate<Local> filtroProvincia = local -> local.getProvincia().toLowerCase().indexOf(provincia) > -1;
			lista_filtros.add(filtroProvincia);
		}

		String localidad = this.ventanaMainLocales.getTxtLocalidad().getText();
		if (!localidad.isEmpty()) {
			Predicate<Local> filtroLocalidad = local -> local.getLocalidad().indexOf(localidad) > -1;
			lista_filtros.add(filtroLocalidad);
		}

		if (lista_filtros.size() == 1) {
			return lista_filtros.get(0);
		} else if (lista_filtros.size() > 1) {
			Predicate<Local> acumulador = lista_filtros.get(0);
			lista_filtros.remove(0);
			for (Predicate<Local> predicado : lista_filtros) {
				acumulador = acumulador.and(predicado);
			}
			return acumulador;
		}
		return null;
	}

	private String radioButtonSeleccionado() {
		if (ventanaMainLocales.getRadioHabilitados().isSelected()) {
			return "HABILITADO";
		}
		if (ventanaMainLocales.getRadioInhabilitados().isSelected()) {
			return "INHABILITADO";
		}
		return "TODOS";
	}

	private void ventanaAgregarLocal(ActionEvent a) {
		this.ventanaAgregarLocal.mostrarVentana();
	}

	private void llenarComboBoxProvincia() {
		this.ventanaAgregarLocal.getComboBoxProvincia().removeAllItems();
		this.ventanaEditarLocal.getComboBoxProvincia().removeAllItems();

		List<String> provincias = Arrays.asList("Buenos Aires", "Ciudad Autónoma de Buenos Aires", "Catamarca", "Chaco",
				"Chubut", "Córdoba", "Corrientes", "Entre Ríos", "Formosa", "Jujuy", "La Pampa", "La Rioja", "Mendoza",
				"Misiones", "Neuquén", "Río Negro", "Salta", "San Juan", "San Luis", "Santa Cruz", "Santa Fe",
				"Santiago del Estero", "Tierra del Fuego", "Tucumán");

		for (String provincia : provincias) {
			this.ventanaAgregarLocal.getComboBoxProvincia().addItem(provincia);
			this.ventanaEditarLocal.getComboBoxProvincia().addItem(provincia);
		}
		this.ventanaAgregarLocal.getComboBoxProvincia().setSelectedIndex(-1);
		this.ventanaEditarLocal.getComboBoxProvincia().setSelectedIndex(-1);

	}

	private void ventanaEditarLocal(ActionEvent e) {
		try {
			Local localSeleccionado = localSeleccionado();
			if (localSeleccionado.getEstado().equals("HABILITADO")) {
				llenarVentanaLocal(localSeleccionado);
				this.ventanaEditarLocal.mostrarVentana();
			} else {
				JOptionPane.showMessageDialog(null, "Seleccione un local con estado HABILITADO.", "¡Error!",
						JOptionPane.ERROR_MESSAGE);
			}
		} catch (NullPointerException ex) {
			JOptionPane.showMessageDialog(null, "Seleccione un local para editar.", "¡Error!",
					JOptionPane.ERROR_MESSAGE);
		}
	}

	private void llenarVentanaLocal(Local local) {
		ventanaEditarLocal.getTxtNombre().setText(local.getNombre());
		ventanaEditarLocal.getTxtDetalle().setText(local.getDetalle());
		ventanaEditarLocal.getTxtDireccion().setText(local.getDireccion());
		ventanaEditarLocal.getTxtLocalidad().setText(local.getLocalidad());
		ventanaEditarLocal.getComboBoxProvincia().setSelectedItem(local.getProvincia());
	}

	private void limpiarTabla() {
		this.ventanaMainLocales.getModelLocales().setRowCount(0);
		this.ventanaMainLocales.getModelLocales().setColumnCount(0);
		this.ventanaMainLocales.getModelLocales().setColumnIdentifiers(this.ventanaMainLocales.getNombreColumnas());
	}

	private void agregarFilaATabla(Local local) {
		Object[] fila = { local, local.getDireccion(), local.getLocalidad(), local.getProvincia(), local.getDetalle(),
				local.getEstado() };

		this.ventanaMainLocales.getModelLocales().addRow(fila);
	}

	private Local localSeleccionado() throws ArrayIndexOutOfBoundsException {
		int fila = ventanaMainLocales.getTablaLocales().getSelectedRow();
		return (Local) ventanaMainLocales.getModelLocales().getValueAt(fila, 0);
	}

	private void HabilitarInhabilitar() {
		try {
			Local local = localSeleccionado();
			if (local.getEstado().equals("HABILITADO")) {
				cambiarEstado(local, "INHABILITADO", "INHABILITAR");
			} else {
				cambiarEstado(local, "HABILITADO", "HABILITAR");
			}
		} catch (ArrayIndexOutOfBoundsException ex) {
			JOptionPane.showMessageDialog(null, "Seleccione un local.", "¡Error!", JOptionPane.ERROR_MESSAGE);
		}
	}

	private void cambiarEstado(Local local, String estado, String accion) {
		int confirm = JOptionPane.showOptionDialog(null, "¿Está seguro que quiere " + accion + " este local?",
				"Confirmacion", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, null);
		if (confirm == 0) {

			if (this.accesoDatos.cambiarEstadoLocal(local, estado)) {
				local.setEstado(estado);
				realizarBusqueda(true);
			} else {
				JOptionPane.showMessageDialog(null, "No se pudo " + accion + " el local", "¡Error!",
						JOptionPane.ERROR_MESSAGE);
			}
		}
	}

	private boolean existeLocal(Local local) {
		Local localEnBdd = this.accesoDatos.buscarLocalPorNombre(local.getNombre());
		if (local.equals(localEnBdd) && local.getId() != localEnBdd.getId()) {
			return true;
		}

		return false;
	}

	private void guardarLocal() {
		if (this.ventanaAgregarLocal.verificarCampos()) {
			Local local = crearLocal();
			if (!existeLocal(local)) {
				if (this.accesoDatos.agregarLocal(local)) {
					locales_en_tabla.add(local);
					realizarBusqueda(true);
					ventanaAgregarLocal.cerrar();
				} else {
					JOptionPane.showMessageDialog(null, "No se pudo guardar el local", "¡Error!",
							JOptionPane.ERROR_MESSAGE);
				}
			} else {
				JOptionPane.showMessageDialog(null, "El nombre del local ya existe en la base de datos.",
						"¡Advertencia!", JOptionPane.WARNING_MESSAGE);
			}
		}
	}

	private Local crearLocal() {
		String nombre = ventanaAgregarLocal.getTxtNombre().getText();
		String direccion = ventanaAgregarLocal.getTxtDireccion().getText();
		String localidad = ventanaAgregarLocal.getTxtLocalidad().getText();
		String provincia = ventanaAgregarLocal.getComboBoxProvincia().getSelectedItem().toString();
		String detalle = ventanaAgregarLocal.getTxtDetalle().getText();

		return new Local(nombre, direccion, localidad, provincia, detalle);
	}

	private void editarLocal() {
		if (this.ventanaEditarLocal.verificarCampos()) {
			Local localAeditar = editarLocalConCamposDeVentana();
			if (!existeLocal(localAeditar)) {

				if (!accesoDatos.updateLocal(localAeditar)) {
					JOptionPane.showMessageDialog(null, "No se pudo editar el local", "¡Error!",
							JOptionPane.ERROR_MESSAGE);
				} else {
					realizarBusqueda(true);
					this.ventanaEditarLocal.cerrar();
				}
			}
			JOptionPane.showMessageDialog(null, "El nombre del local ya existe en la base de datos.", "¡Advertencia!",
					JOptionPane.WARNING_MESSAGE);
		}
	}

	private Local editarLocalConCamposDeVentana() {
		Local localSeleccionado = localSeleccionado();

		localSeleccionado.setNombre(ventanaEditarLocal.getTxtNombre().getText());
		localSeleccionado.setDetalle(ventanaEditarLocal.getTxtDetalle().getText());
		localSeleccionado.setDireccion(ventanaEditarLocal.getTxtDireccion().getText());
		localSeleccionado.setLocalidad(ventanaEditarLocal.getTxtLocalidad().getText());
		localSeleccionado.setProvincia(ventanaEditarLocal.getComboBoxProvincia().getSelectedItem().toString());

		return localSeleccionado;
	}

//	private void actualizarVentana() {
//		ventanaMainLocales.getPanel().revalidate();
//		ventanaMainLocales.getPanel().repaint();
//	}
}
