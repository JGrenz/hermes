package controladores.administrador;

import java.awt.event.ActionEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.swing.JOptionPane;

import org.apache.commons.codec.digest.DigestUtils;

import daoImplementacion.UsuarioDAOSQL;
import dominio.Contacto;
import dominio.Local;
import dominio.Mail;
import dominio.Usuario;
import dominio.UsuarioRol;
import modelo.AccesoDatos;
import vistas.administrador.empleados.Main_Empleados;
import vistas.administrador.empleados.Ventana_AgregarEmpleado;
import vistas.administrador.empleados.Ventana_EditarEmpleado;

public class ControladorEmpleado {

	private Main_Empleados ventanaMainEmpleados;
	private Ventana_AgregarEmpleado ventanaAgregarEmpleado;
	private Ventana_EditarEmpleado ventanaEditarEmpleado;
	private AccesoDatos accesoDatos;
	private List<Usuario> empleados_en_tabla;
	private Mail mail;
	

	public ControladorEmpleado(Main_Empleados ventanaMainEmpleados, AccesoDatos accesoDatos) {
		this.ventanaMainEmpleados = ventanaMainEmpleados;
		this.ventanaAgregarEmpleado = Ventana_AgregarEmpleado.getInstance();
		this.ventanaEditarEmpleado = Ventana_EditarEmpleado.getInstance();

		this.ventanaMainEmpleados.getBtnAgregar().addActionListener(a -> ventanaAgregarEmpleado(a));
		this.ventanaMainEmpleados.getBtnEditar().addActionListener(e -> ventanaEditarEmpleado(e));
		this.ventanaMainEmpleados.getBtnBorrar().addActionListener(e -> HabilitarInhabilitar());
		this.ventanaMainEmpleados.getTablaEmpleado().addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				mostrarTelefonos();
			}
		});

		this.ventanaMainEmpleados.getBtnBuscarEmpleado().addActionListener(e -> realizarBusqueda(false));

//        this.ventanaMainEmpleados.getCboxRol().addActionListener(e -> realizarBusqueda(false));

		this.ventanaMainEmpleados.getRdbtnHabilitados().addActionListener(e -> realizarBusqueda(false));
		this.ventanaMainEmpleados.getRdbtnInhabilitados().addActionListener(e -> realizarBusqueda(false));
		this.ventanaMainEmpleados.getRdbtnTodos().addActionListener(e -> realizarBusqueda(false));

		this.ventanaAgregarEmpleado.getBtnConfirmar().addActionListener(c -> guardarEmpleado(c));
		this.ventanaAgregarEmpleado.getTxtNombre().addFocusListener(metodoUsuario());
		this.ventanaAgregarEmpleado.getTxtApellido().addFocusListener(metodoUsuario());

		this.ventanaEditarEmpleado.getBtnConfirmar().addActionListener(e -> editarEmpleado(e));

		this.accesoDatos = accesoDatos;
		this.mail = Mail.getInstance(accesoDatos);
		this.empleados_en_tabla = new ArrayList<Usuario>();
	}

	public void inicializar() {
//		if (empleados_en_tabla.size() == 0) {
			empleados_en_tabla = this.accesoDatos.obtenerEmpleados();
//		}
		llenarCboxRol();
		llenarComboBoxAgregarEmpleado();
		llenarComboBoxEditarEmpleado();
		llenarComboBoxLocales();

		realizarBusqueda(true);
	}

	private void realizarBusqueda(boolean primeraEntrada) {
		Predicate<Usuario> filtro = filtrosSeleccionados();
		limpiarTabla();
		limpiarTelefonos();
		if (filtro != null) {
			List<Usuario> lista_filtrada = this.empleados_en_tabla.stream().filter(filtro).collect(Collectors.toList());

			if (lista_filtrada.size() > 0) {
				lista_filtrada.forEach(empleado -> agregarEmpleadoATabla(empleado));
			} else {
				if (!primeraEntrada) {
					JOptionPane.showMessageDialog(null, "No se encontraron empleados.", "¡Advertencia!", JOptionPane.WARNING_MESSAGE);
				}
			}
		} else {
			empleados_en_tabla.forEach(empleado -> agregarEmpleadoATabla(empleado));
		}
	}

	private Predicate<Usuario> filtrosSeleccionados() {
		List<Predicate<Usuario>> lista_filtros = new ArrayList<>();

		String estado = radioButtonSeleccionado();
		if (!estado.equals("TODOS")) {
			Predicate<Usuario> filtroEstado = empleado -> empleado.getEstado().equals(estado);
			lista_filtros.add(filtroEstado);
		}

		String rol = rolSeleccionado();
		if (!rol.equals("TODOS")) {
			Predicate<Usuario> filtroEstado = empleado -> empleado.getRol().equals(rol);
			lista_filtros.add(filtroEstado);
		}

		String nombre = this.ventanaMainEmpleados.getTxtNombre().getText().toLowerCase();
		if (!nombre.isEmpty()) {
			Predicate<Usuario> filtroNombre = empleado -> empleado.getNombre().toLowerCase().indexOf(nombre) > -1;
			lista_filtros.add(filtroNombre);
		}

		String apellido = this.ventanaMainEmpleados.getTxtApellido().getText().toLowerCase();
		if (!apellido.isEmpty()) {
			Predicate<Usuario> filtroApellido = empleado -> empleado.getApellido().toLowerCase().indexOf(apellido) > -1;
			lista_filtros.add(filtroApellido);
		}

		String dni = this.ventanaMainEmpleados.getTxtDNI().getText();
		if (!dni.isEmpty()) {
			Predicate<Usuario> filtroDni = empleado -> empleado.getDni().indexOf(dni) > -1;
			lista_filtros.add(filtroDni);
		}

		String usuario = this.ventanaMainEmpleados.getTxtUsuario().getText();
		if (!usuario.isEmpty()) {
			Predicate<Usuario> filtroUsuario = empleado -> empleado.getUsuario().indexOf(usuario) > -1;
			lista_filtros.add(filtroUsuario);
		}

		if (lista_filtros.size() == 1) {
			return lista_filtros.get(0);
		} else if (lista_filtros.size() > 1) {
			Predicate<Usuario> acumulador = lista_filtros.get(0);
			lista_filtros.remove(0);
			for (Predicate<Usuario> predicado : lista_filtros) {
				acumulador = acumulador.and(predicado);
			}
			return acumulador;
		}
		return null;
	}

	public String radioButtonSeleccionado() {
		if (ventanaMainEmpleados.getRdbtnHabilitados().isSelected()) {
			return "HABILITADO";
		}
		if (ventanaMainEmpleados.getRdbtnInhabilitados().isSelected()) {
			return "INHABILITADO";
		}
		return "TODOS";
	}

	public String rolSeleccionado() {
		return this.ventanaMainEmpleados.getCboxRol().getSelectedItem().toString();
	}

	private void HabilitarInhabilitar() {
		try {

			Usuario empleado = empleadoSeleccionado();
			if (empleado.getEstado().equals("HABILITADO")) {
				cambiarEstado(empleado, "INHABILITADO", "INHABILITAR");
			} else {
				cambiarEstado(empleado, "HABILITADO", "HABILITAR");
			}
		} catch (ArrayIndexOutOfBoundsException ex) {
			JOptionPane.showMessageDialog(null, "Seleccione un empleado.", "¡Advertencia!", JOptionPane.WARNING_MESSAGE);
		}
	}

	private void cambiarEstado(Usuario empleado, String estado, String accion) {
		int confirm = JOptionPane.showOptionDialog(null, "¿Está seguro que quiere " + accion + " este empleado?",
				"Confirmacion", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, null);
		if (confirm == 0) {

			if (this.accesoDatos.cambiarEstadoUsuario(empleado, estado)) {
				empleado.setEstado(estado);
				realizarBusqueda(true);
			} else {
				JOptionPane.showMessageDialog(null, "No se pudo " + accion + " el empleado", "¡Error!", JOptionPane.ERROR_MESSAGE);
			}
		}
	}

	private void llenarCboxRol() {
		if (ventanaMainEmpleados.getCboxRol().getItemCount() == 0) {
			for (UsuarioRol value : UsuarioRol.values()) {
				if (value.toString() != "CLIENTE") {
					ventanaMainEmpleados.getCboxRol().addItem(value.toString());
				}
			}
			ventanaMainEmpleados.getCboxRol().addItem("TODOS");
		}
		ventanaMainEmpleados.getCboxRol().setSelectedItem("TODOS");
	}

	private void ventanaAgregarEmpleado(ActionEvent a) {
		ventanaAgregarEmpleado.mostrarVentana();
	}

	private void llenarComboBoxAgregarEmpleado() {
		if (ventanaAgregarEmpleado.getCboxRol().getItemCount() == 0) {
			ventanaAgregarEmpleado.agregarItemCboxEmpleado(UsuarioRol.ADMINISTRADOR);
			ventanaAgregarEmpleado.agregarItemCboxEmpleado(UsuarioRol.COORDINADOR);
			ventanaAgregarEmpleado.agregarItemCboxEmpleado(UsuarioRol.DUEÑO);
			ventanaAgregarEmpleado.agregarItemCboxEmpleado(UsuarioRol.PERSONAL_ADMINISTRATIVO);
		}
		ventanaAgregarEmpleado.getCboxRol().setSelectedIndex(-1);
	}

	private void llenarComboBoxLocales() {
		if (ventanaAgregarEmpleado.getCboxLocal().getItemCount() == 0) {
			List<Local> locales = this.accesoDatos.obtenerLocales();
			for (Local local : locales) {
				ventanaAgregarEmpleado.agregarItemCboxLocal(local);
				ventanaEditarEmpleado.agregarItemCboxLocal(local);
			}
			ventanaAgregarEmpleado.getCboxLocal().setSelectedIndex(-1);
			ventanaEditarEmpleado.getCboxLocal().setSelectedIndex(-1);
		}
	}

	private void ventanaEditarEmpleado(ActionEvent e) {
		try {

			Usuario empleado = empleadoSeleccionado();
			if (empleado.getEstado().equals("HABILITADO")) {
				this.llenarCampos(empleado);
				this.ventanaEditarEmpleado.mostrarVentana();
			} else {
				JOptionPane.showMessageDialog(null, "Seleccione un empleado con estado HABILITADO.", "¡Advertencia!", JOptionPane.WARNING_MESSAGE);
			}
		} catch (ArrayIndexOutOfBoundsException ex) {
			JOptionPane.showMessageDialog(null, "Seleccione un empleado para editar.", "¡Advertencia!", JOptionPane.WARNING_MESSAGE);
		}
	}

	private Usuario empleadoSeleccionado() throws ArrayIndexOutOfBoundsException {

		int fila = ventanaMainEmpleados.getTablaEmpleado().getSelectedRow();

		int columna = ventanaMainEmpleados.getTablaEmpleado().getColumnModel().getColumnIndex("Usuario");

		return (Usuario) ventanaMainEmpleados.getTablaEmpleado().getValueAt(fila, columna);
	}

	private void llenarComboBoxEditarEmpleado() {
		if (ventanaEditarEmpleado.getCboxRol().getItemCount() == 0) {
			ventanaEditarEmpleado.agregarItemCboxEmpleado(UsuarioRol.ADMINISTRADOR);
			ventanaEditarEmpleado.agregarItemCboxEmpleado(UsuarioRol.COORDINADOR);
			ventanaEditarEmpleado.agregarItemCboxEmpleado(UsuarioRol.DUEÑO);
			ventanaEditarEmpleado.agregarItemCboxEmpleado(UsuarioRol.PERSONAL_ADMINISTRATIVO);
		}
	}

	private Usuario crearEmpleado() {
		String rol = this.ventanaAgregarEmpleado.getCboxRol().getSelectedItem().toString();
		if (rol.equals("Personal administrativo")) {
			rol = "Personal_administrativo";
		}

		Usuario empleado = new Usuario(ventanaAgregarEmpleado.getTxtDni().getText(),
				ventanaAgregarEmpleado.getTxtNombre().getText(), ventanaAgregarEmpleado.getTxtApellido().getText(),
				ventanaAgregarEmpleado.getTxtUsuario().getText(), ventanaAgregarEmpleado.getTxtNacimiento(),
				ventanaAgregarEmpleado.getTxtEmail().getText(), rol.toUpperCase());

		Local local = (Local) ventanaAgregarEmpleado.getCboxLocal().getSelectedItem();

		empleado.setLocal(local);
		empleado.setUsuario(ventanaAgregarEmpleado.getTxtUsuario().getText());
		empleado.setContacto(crearContacto());
		return empleado;
	}

	private void guardarEmpleado(ActionEvent c) {
		if (this.ventanaAgregarEmpleado.verificarCampo()
				&& !existeUsuario(ventanaAgregarEmpleado.getTxtUsuario().getText())) {
			Usuario empleado = crearEmpleado();
			if (this.accesoDatos.agregarUsuario(empleado)) {
				enviarNombreUsuario(empleado);
				enviarContraseña(empleado);

				String sha256hex = DigestUtils.sha256Hex(empleado.getContraseña());
				empleado.setContraseña(sha256hex);

				empleados_en_tabla.add(empleado);
				realizarBusqueda(true);
				this.ventanaAgregarEmpleado.cerrar();
			}
		}
	}

	private void enviarNombreUsuario(Usuario empleado) {
		String asunto = "¡Nuevo usuario en Hermes!";
		String cuerpo = "Bienvenide " + empleado.getApellido() + " " + empleado.getNombre()
				+ " al sistema Hermes, !Muchas gracias por incorporarse!.\n\n"
				+ "Para poder ingresar al sistema deberas hacerlo con el siguiente nombre de usuario: \n\n"
				+ empleado.getUsuario()
				+ "\n\nEn breve recibirás un mail con tu contraseña. \nEl equpo de cuentas de Hermes.";
		this.mail.enviarConGMail(empleado.getEmail(), asunto, cuerpo);
	}

	private void enviarContraseña(Usuario empleado) {
		String asunto = "Contraseña para ingresar en Hermes";
		String cuerpo = "Estimade " + empleado.getApellido() + " " + empleado.getNombre()
				+ ": Fuiste registrado exitosamente en el sistema Hermes. Para poder ingresar "
				+ "con tu nombre de usuario tendrás que hacerlo con la siguiente contraseña: \n\n "
				+ empleado.getContraseña()
				+ "\n\nNo olvide que al ingresar puede cambiarla. Gracias,\nEl equipo de cuentas de Hermes.";
		this.mail.enviarConGMail(empleado.getEmail(), asunto, cuerpo);
	}

	private boolean existeUsuario(String usuario) {
		if (this.accesoDatos.existeNombreUsuario(usuario)) {
			JOptionPane.showMessageDialog(null, "El nombre de usuario está en uso", "¡Error!", JOptionPane.ERROR_MESSAGE);
			return true;
		}
		return false;
	}

	private Usuario editarEmpleadoConDatosDeVentana() {

		Usuario empleado = empleadoSeleccionado();

		empleado.setNombre(this.ventanaEditarEmpleado.getTxtNombre().getText());
		empleado.setApellido(this.ventanaEditarEmpleado.getTxtApellido().getText());
		empleado.setDni(this.ventanaEditarEmpleado.getTxtDni().getText());
		empleado.setEmail(this.ventanaEditarEmpleado.getTxtEmail().getText());
		empleado.setUsuario(this.ventanaEditarEmpleado.getTxtUsuario().getText());
		empleado.setNacimiento(this.ventanaEditarEmpleado.getTxtNacimiento());
		empleado.setContacto(editarContactoConDatosDeVentana());

		String rol = this.ventanaEditarEmpleado.getCboxRol().getSelectedItem().toString();
		if (rol.equals("Personal administrativo")) {
			rol = "Personal_administrativo";
		}
		empleado.setRol(rol.toUpperCase());
		Local local = (Local) ventanaEditarEmpleado.getCboxLocal().getSelectedItem();
		empleado.setLocal(local);

		return empleado;
	}

	private void editarEmpleado(ActionEvent e) {
		if (this.ventanaEditarEmpleado.verificarCampo()) {
			Usuario empleado = editarEmpleadoConDatosDeVentana();
			if (this.accesoDatos.editarUsuario(empleado)) {
				this.ventanaEditarEmpleado.cerrar();
				realizarBusqueda(true);
			} else {
				JOptionPane.showMessageDialog(null, "No se pudo editar el empleado", "¡Error!", JOptionPane.ERROR_MESSAGE);
			}
		}
	}

	private Contacto crearContacto() {
		Contacto contacto = new Contacto(ventanaAgregarEmpleado.getTelefono1().getText(),
				ventanaAgregarEmpleado.getTelefono2().getText(), ventanaAgregarEmpleado.getTelefono3().getText(),
				ventanaAgregarEmpleado.getTelefono4().getText(), ventanaAgregarEmpleado.getTelefono5().getText());
		return contacto;
	}

	private Contacto editarContactoConDatosDeVentana() {
		Contacto contacto = this.empleados_en_tabla.get(this.ventanaMainEmpleados.getTablaEmpleado().getSelectedRow())
				.getContacto();
		contacto.setTelefono1(this.ventanaEditarEmpleado.getTelefono1().getText());
		contacto.setTelefono2(this.ventanaEditarEmpleado.getTelefono2().getText());
		contacto.setTelefono3(this.ventanaEditarEmpleado.getTelefono3().getText());
		contacto.setTelefono4(this.ventanaEditarEmpleado.getTelefono4().getText());
		contacto.setTelefono5(this.ventanaEditarEmpleado.getTelefono5().getText());

		return contacto;
	}

	private void llenarCampos(Usuario empleado) {
		java.util.Date date = java.sql.Date.valueOf(empleado.getNacimiento());

		this.ventanaEditarEmpleado.getTxtNombre().setText(empleado.getNombre());
		this.ventanaEditarEmpleado.getTxtApellido().setText(empleado.getApellido());
		this.ventanaEditarEmpleado.getTxtDni().setText(empleado.getDni());
		this.ventanaEditarEmpleado.getTxtEmail().setText(empleado.getEmail());
		this.ventanaEditarEmpleado.getTxtUsuario().setText(empleado.getUsuario());
		this.ventanaEditarEmpleado.getNacimientoChooser().setDate(date);
		this.ventanaEditarEmpleado.getCboxRol().setSelectedItem(UsuarioRol.valueOf(empleado.getRol()));
		this.ventanaEditarEmpleado.getCboxLocal().setSelectedItem(empleado.getLocal());
		this.ventanaEditarEmpleado.getTelefono1().setText(empleado.getContacto().getTelefono1());
		this.ventanaEditarEmpleado.getTelefono2().setText(empleado.getContacto().getTelefono2());
		this.ventanaEditarEmpleado.getTelefono3().setText(empleado.getContacto().getTelefono3());
		this.ventanaEditarEmpleado.getTelefono4().setText(empleado.getContacto().getTelefono4());
		this.ventanaEditarEmpleado.getTelefono5().setText(empleado.getContacto().getTelefono5());
	}

	private void limpiarTabla() {
		this.ventanaMainEmpleados.getModelEmpleado().setRowCount(0);
		this.ventanaMainEmpleados.getModelEmpleado().setColumnCount(0);
		this.ventanaMainEmpleados.getModelEmpleado()
				.setColumnIdentifiers(this.ventanaMainEmpleados.getNombreColumnas());
	}

	private void agregarEmpleadoATabla(Usuario empleado) {
		DateTimeFormatter formatoDeFecha = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		LocalDate fechaDeNacimiento = empleado.getNacimiento();
		String fechaDeNacimientoConFormato = fechaDeNacimiento.format(formatoDeFecha);

		Object[] fila = { empleado.getNombre(), empleado.getApellido(), empleado.getDni(), fechaDeNacimientoConFormato,
				empleado, empleado.getEmail(), empleado.getContacto().getTelefono1(), empleado.getRol(),
				empleado.getEstado() };
		this.ventanaMainEmpleados.getModelEmpleado().addRow(fila);
	}

	private void mostrarTelefonos() {
		Usuario empleado = this.empleados_en_tabla.get(this.ventanaMainEmpleados.getTablaEmpleado().getSelectedRow());
		this.ventanaMainEmpleados.getTelefono1().setText(empleado.getContacto().getTelefono1());
		this.ventanaMainEmpleados.getTelefono2().setText(empleado.getContacto().getTelefono2());
		this.ventanaMainEmpleados.getTelefono3().setText(empleado.getContacto().getTelefono3());
		this.ventanaMainEmpleados.getTelefono4().setText(empleado.getContacto().getTelefono4());
		this.ventanaMainEmpleados.getTelefono5().setText(empleado.getContacto().getTelefono5());
	}

	private void limpiarTelefonos() {
		this.ventanaMainEmpleados.getTelefono1().setText("");
		this.ventanaMainEmpleados.getTelefono2().setText("");
		this.ventanaMainEmpleados.getTelefono3().setText("");
		this.ventanaMainEmpleados.getTelefono4().setText("");
		this.ventanaMainEmpleados.getTelefono5().setText("");
	}

	private FocusAdapter metodoUsuario() {
		return new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent arg0) {
				String usuarioAgregar = setNumeroUsuarioAgregar();
				ventanaAgregarEmpleado.getTxtUsuario().setText(usuarioAgregar);
			}
		};
	}

	private String setNumeroUsuarioAgregar() {
		UsuarioDAOSQL clienteSQL = new UsuarioDAOSQL();
		String usuario = this.ventanaAgregarEmpleado.getTxtUsuario().getText();

		return comprobacionUsuario(clienteSQL, usuario);
	}

	private String comprobacionUsuario(UsuarioDAOSQL clienteSQL, String usuario) {
		int indice = 1;
		String elegido = usuario;

		while (true) {
			if (clienteSQL.verificarExisteUsuario(usuario)) {
				usuario = elegido + indice;
			} else {
				return usuario;
			}
			indice = indice + 1;
		}
	}

//	private void actualizarVentana() {
//		ventanaMainEmpleados.getPanel().revalidate();
//		ventanaMainEmpleados.getPanel().repaint();
//	}
}
