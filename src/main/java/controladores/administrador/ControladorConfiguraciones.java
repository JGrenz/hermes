package controladores.administrador;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.JOptionPane;

import org.apache.commons.codec.digest.DigestUtils;

import controladores.ControladorConfiguracionBDD;
import controladores.ControladorLogin;
import dominio.DevolucionConfig;
import dominio.Mail;
import dominio.PuntajeConfig;
import dominio.Usuario;
import modelo.AccesoDatos;
import vistas.VentanaConfiguracionBDD;
import vistas.administrador.configuraciones.Main_Configuracion;
import vistas.administrador.configuraciones.Ventana_AgregarConfigDevolucion;
import vistas.administrador.configuraciones.Ventana_ConfiguracionContraseñaEmail;
import vistas.administrador.configuraciones.Ventana_ConfiguracionDevolucion;
import vistas.administrador.configuraciones.Ventana_ConfiguracionEmail;
import vistas.administrador.configuraciones.Ventana_ConfiguracionPuntaje;
import vistas.administrador.configuraciones.Ventana_EditarConfigDevolucion;

public class ControladorConfiguraciones {
	private Main_Configuracion ventanaMainConfiguraciones;
	private Ventana_ConfiguracionEmail ventanaConfigEmail;
	private Ventana_ConfiguracionContraseñaEmail ventanaConfigContraseñaEmail;
	private Ventana_ConfiguracionPuntaje ventanaConfigPuntaje;
	private Ventana_ConfiguracionDevolucion ventanaConfigDevolucion;
	private Ventana_AgregarConfigDevolucion ventanaAgregarConfigDevolucion;
	private Ventana_EditarConfigDevolucion ventanaEditarConfigDevolucion;
	private VentanaConfiguracionBDD ventanaConfigBDD;
	private AccesoDatos accesoDatos;
	private Mail mail;
	private PuntajeConfig configuracionPuntaje;
	private List<DevolucionConfig> configDevolucion_en_tabla;
	private ControladorConfiguracionBDD configuracionBDD;

	public ControladorConfiguraciones(Main_Configuracion ventanaMainConfiguraciones, AccesoDatos accesoDatos) {
		this.ventanaMainConfiguraciones = ventanaMainConfiguraciones;
		this.ventanaConfigEmail = Ventana_ConfiguracionEmail.getInstance();
		this.ventanaConfigContraseñaEmail = Ventana_ConfiguracionContraseñaEmail.getInstance();
		this.ventanaConfigPuntaje = Ventana_ConfiguracionPuntaje.getInstance();
		this.ventanaConfigDevolucion = Ventana_ConfiguracionDevolucion.getInstance();
		this.ventanaAgregarConfigDevolucion = Ventana_AgregarConfigDevolucion.getInstance();
		this.ventanaEditarConfigDevolucion = Ventana_EditarConfigDevolucion.getInstance();
		this.ventanaConfigBDD = new VentanaConfiguracionBDD();

		this.ventanaMainConfiguraciones.getBtnConfigEmail().addActionListener(e -> mostrarVentanaConfigEmail());
		this.ventanaMainConfiguraciones.getBtnConfigPuntaje().addActionListener(e -> mostrarVentanaConfigPuntaje());
		this.ventanaMainConfiguraciones.getBtnConfigDevolucion()
				.addActionListener(e -> mostrarVentanaConfigDevolucion());
		this.ventanaMainConfiguraciones.getBtnConfigBDD().addActionListener(e -> mostrarVentanaConfigBDD());

		this.ventanaConfigEmail.getBtnConfirmar().addActionListener(e -> confirmarConfiguracionesEmail());
		this.ventanaConfigEmail.getBtnContraseña().addActionListener(e -> mostrarVentanaCambiarContraseña());
		this.ventanaConfigContraseñaEmail.getBtnConfirmar()
				.addActionListener(c -> confirmarConfiguracionesContraseñaEmail());

		this.ventanaConfigPuntaje.getBtnConfirmar().addActionListener(p -> confirmarConfiguracionesPuntaje());

		this.ventanaConfigDevolucion.getBtnAgregar().addActionListener(e -> mostrarVentanaAgregarConfigDevolucion());
		this.ventanaConfigDevolucion.getBtnEditar().addActionListener(e -> mostrarVentanaEditarConfigDevolucion());
		this.ventanaConfigDevolucion.getBtnHabInhab().addActionListener(e -> habInhabConfigDevolucion());

		this.ventanaAgregarConfigDevolucion.getBtnConfirmar().addActionListener(e -> agregarConfig());
		this.ventanaEditarConfigDevolucion.getBtnConfirmar().addActionListener(e -> editarConfig());

		this.accesoDatos = accesoDatos;
		mail = Mail.getInstance(accesoDatos);
		configuracionPuntaje = this.accesoDatos.obtenerConfiguracionDePuntaje();
		this.configDevolucion_en_tabla = new ArrayList<>();
	}

	public void inicializar() {
//        if (this.config_en_tabla.size() == 0) {
		this.configDevolucion_en_tabla = accesoDatos.findAllDevolucionConfig();
//        }

		realizarBusqueda(true);
	}

	private void mostrarVentanaConfigEmail() {
		ventanaMainConfiguraciones.getPanelCentro().removeAll();
		ventanaMainConfiguraciones.getPanelCentro().add(ventanaConfigEmail.getPanel());
		llenarCamposEmailConfig();
		actualizarVentana();
	}

	private void llenarCamposEmailConfig() {
		ventanaConfigEmail.getTxtMail().setText(mail.getDireccion());
		ventanaConfigEmail.getTxtRemitente().setText(mail.getRemitente());
		ventanaConfigEmail.getTxtFirma().setText(mail.getFirma());
	}

	private void mostrarVentanaConfigPuntaje() {
		ventanaMainConfiguraciones.getPanelCentro().removeAll();
		ventanaMainConfiguraciones.getPanelCentro().add(ventanaConfigPuntaje.getPanel());
		llenarCamposPuntajeConfig();
		actualizarVentana();
	}

	private void mostrarVentanaConfigBDD() {
		this.configuracionBDD = new ControladorConfiguracionBDD(true);
	}

	private void llenarCamposPuntajeConfig() {
		ventanaConfigPuntaje.getTxtCantidadPlata().setText(configuracionPuntaje.getValorEnPrecio().toString());
		ventanaConfigPuntaje.getTxtCantidadPuntos().setText("" + configuracionPuntaje.getValorEnPuntos());
		ventanaConfigPuntaje.getTxtPrecioUnitario().setText(configuracionPuntaje.getValorUnitario().toString());
		ventanaConfigPuntaje.getSPMeses().setValue(configuracionPuntaje.getMesesVencimiento());
		ventanaConfigPuntaje.getSPAños().setValue(configuracionPuntaje.getAñosVencimiento());
	}

	private void mostrarVentanaConfigDevolucion() {
		ventanaMainConfiguraciones.getPanelCentro().removeAll();
		ventanaMainConfiguraciones.getPanelCentro().add(ventanaConfigDevolucion.getPanel());
		actualizarVentana();
	}

	private void actualizarVentana() {
		ventanaMainConfiguraciones.getPanelCentro().revalidate();
		ventanaMainConfiguraciones.getPanelCentro().repaint();
	}

	private void confirmarConfiguracionesEmail() {
		if (ventanaConfigEmail.verificarCampos()) {

			String direccion = ventanaConfigEmail.getTxtMail().getText();
			String remitente = ventanaConfigEmail.getTxtRemitente().getText();
			String firma = ventanaConfigEmail.getTxtFirma().getText();

			if (mail.getDireccion().equals("")) {
				mail.setDireccion(direccion);
				mail.setRemitente(remitente);
				mail.setFirma(firma);

				if (accesoDatos.insertMail(mail)) {
					JOptionPane.showMessageDialog(null, "El mail fue guardado satisfactoriamente", "¡Aviso!", JOptionPane.INFORMATION_MESSAGE);
				} else {
					JOptionPane.showMessageDialog(null, "No se pudo guardar la información de mail.", "¡Error!", JOptionPane.ERROR_MESSAGE);
				}
			} else {
				mail.setDireccion(direccion);
				mail.setRemitente(remitente);
				mail.setFirma(firma);

				if (accesoDatos.updateMail(mail)) {
					JOptionPane.showMessageDialog(null, "La información fue actualizada satisfactoriamente", "¡Aviso!", JOptionPane.INFORMATION_MESSAGE);
				} else {
					JOptionPane.showMessageDialog(null, "No se pudo guardar la información de mail.", "¡Error!", JOptionPane.ERROR_MESSAGE);
				}
			}
		}
	}

	private void mostrarVentanaCambiarContraseña() {
		ventanaConfigContraseñaEmail.mostrarVentana();
	}

	private void confirmarConfiguracionesContraseñaEmail() {
		char[] contraseñaActual = ventanaConfigContraseñaEmail.getTxtContraseñaActual().getPassword();
		String contraseña = String.valueOf(contraseñaActual);
		String sha256hex = DigestUtils.sha256Hex(contraseña);
		if (sha256hex.equals(mail.getContraseña())) {
			char[] contraseñaNueva = ventanaConfigContraseñaEmail.getTxtNuevaContraseña().getPassword();
			char[] confirmarContraseña = ventanaConfigContraseñaEmail.getTxtConfirmarContraseña().getPassword();
			if (ventanaConfigContraseñaEmail.verificarCampo()) {
				if (mismaContraseña(contraseñaNueva, confirmarContraseña)) {
					mail.setContraseña(String.valueOf(contraseñaNueva));
					if (accesoDatos.updateContraseña(mail)) {
						JOptionPane.showMessageDialog(null, "La contraseña fue actualizada satisfactoriamente", "¡Aviso!", JOptionPane.INFORMATION_MESSAGE);
						limpiarCamposContraseña();
						ventanaConfigContraseñaEmail.cerrar();
					} else {
						JOptionPane.showMessageDialog(null, "No se pudo actualizar la contraseña del mail.", "¡Advertencia!", JOptionPane.WARNING_MESSAGE);
					}
				} else {
					JOptionPane.showMessageDialog(null, "Las contraseñas NUEVA y CONFIRMADA no coinciden.", "¡Error!", JOptionPane.ERROR_MESSAGE);
				}
			}
		} else {
			JOptionPane.showMessageDialog(null, "La contraseña actual no es la correcta.", "¡Error!", JOptionPane.ERROR_MESSAGE);
		}
	}

	private boolean mismaContraseña(char[] contraseña1, char[] contraseña0) {
		if (Arrays.equals(contraseña1, contraseña0)) {
			return true;
		}
		return false;
	}

	private void limpiarCamposContraseña() {
		ventanaConfigContraseñaEmail.getTxtContraseñaActual().setText("");
		ventanaConfigContraseñaEmail.getTxtConfirmarContraseña().setText("");
		ventanaConfigContraseñaEmail.getTxtNuevaContraseña().setText("");
	}

	private void confirmarConfiguracionesPuntaje() {
		if (ventanaConfigPuntaje.verificarCampo()) {
			BigDecimal cantidadPrecio = new BigDecimal(ventanaConfigPuntaje.getTxtCantidadPlata().getText());
			int cantidadPuntos = Integer.parseInt(ventanaConfigPuntaje.getTxtCantidadPuntos().getText());
			BigDecimal valorUnitario = new BigDecimal(ventanaConfigPuntaje.getTxtPrecioUnitario().getText());
			int añosVencimiento = Integer.parseInt(ventanaConfigPuntaje.getSPAños().getValue().toString());
			int mesesVencimiento = Integer.parseInt(ventanaConfigPuntaje.getSPMeses().getValue().toString());
			Usuario creador = ControladorLogin.id_logeado();

			PuntajeConfig nuevaConfig = new PuntajeConfig(cantidadPrecio, cantidadPuntos, valorUnitario,
					añosVencimiento, mesesVencimiento, creador);
			nuevaConfig.setId(configuracionPuntaje.getId());

			if (accesoDatos.editarPuntajeConfig(nuevaConfig)) {
				JOptionPane.showMessageDialog(null, "La información fue actualizada satisfactoriamente", "¡Aviso!", JOptionPane.INFORMATION_MESSAGE);
			} else {
				JOptionPane.showMessageDialog(null, "No se pudo guardar la configuración del puntaje.", "¡Advertencia!", JOptionPane.WARNING_MESSAGE);
			}
		}
	}

	private void mostrarVentanaAgregarConfigDevolucion() {
		ventanaAgregarConfigDevolucion.mostrarVentana();
	}

	private void mostrarVentanaEditarConfigDevolucion() {
		try {
			DevolucionConfig devolucionConfig = configSeleccionada();
			llenarCamposDevolucionConfig(devolucionConfig);
			ventanaEditarConfigDevolucion.mostrarVentana();
		} catch (ArrayIndexOutOfBoundsException e) {
			JOptionPane.showMessageDialog(null, "Seleccione una configuracion de devolución.", "¡Advertencia!", JOptionPane.WARNING_MESSAGE);
		}
	}

	private void llenarCamposDevolucionConfig(DevolucionConfig devolucionConfig) {
		this.ventanaEditarConfigDevolucion.getTxtRangoInicial().setText("" + devolucionConfig.getDesde());
		String hasta = "";
		if (devolucionConfig.getHasta() != 0) {
			hasta = "" + devolucionConfig.getHasta();
		}
		this.ventanaEditarConfigDevolucion.getTxtRangoFinal().setText(hasta);
		this.ventanaEditarConfigDevolucion.getTxtPorcentaje().setText("" + devolucionConfig.getPorcentaje());
	}

	private void agregarConfig() {
		if (ventanaAgregarConfigDevolucion.verificarCampos()) {
			int porcentaje = Integer.parseInt(ventanaAgregarConfigDevolucion.getTxtPorcentaje().getText());
			int rangoInicial = Integer.parseInt(ventanaAgregarConfigDevolucion.getTxtRangoInicial().getText());
			int rangoFinal = 0;
			if (!ventanaAgregarConfigDevolucion.getTxtRangoFinal().getText().equals("")) {
				rangoFinal = Integer.parseInt(ventanaAgregarConfigDevolucion.getTxtRangoFinal().getText());
			}
			if (!seSolapanConfigDevoluciones(rangoInicial, rangoFinal, 0)) {
				DevolucionConfig configuracionDevolucion = new DevolucionConfig(ControladorLogin.id_logeado(),
						porcentaje, rangoInicial, rangoFinal);
				if (this.accesoDatos.insertDevolucionConfig(configuracionDevolucion)) {
					configDevolucion_en_tabla.add(configuracionDevolucion);
					realizarBusqueda(true);
					ventanaAgregarConfigDevolucion.cerrar();
				} else {
					JOptionPane.showMessageDialog(null, "No se pudo insertar la configuración de devolución.", "¡Advertencia!", JOptionPane.WARNING_MESSAGE);
				}
			} else {
				JOptionPane.showMessageDialog(null,
						"El rango ingresado se solapa con el de otra configuración de devolución.", "¡Error!", JOptionPane.ERROR_MESSAGE);
			}
		}
	}

	private void editarConfig() {
		if (ventanaEditarConfigDevolucion.verificarCampos()) {

			DevolucionConfig configuracionDevolucion = configSeleccionada();
			int rangoInicial = Integer.parseInt(ventanaEditarConfigDevolucion.getTxtRangoInicial().getText());
			int rangoFinal = 0;
			if (!ventanaEditarConfigDevolucion.getTxtRangoFinal().getText().equals("")) {
				rangoFinal = Integer.parseInt(ventanaEditarConfigDevolucion.getTxtRangoFinal().getText());
			}
			if (!seSolapanConfigDevoluciones(rangoInicial, rangoFinal, configuracionDevolucion.getId())) {
				int porcentaje = configuracionDevolucion.getPorcentaje();
				int desdeOriginal = configuracionDevolucion.getDesde();
				int hastaOriginal = configuracionDevolucion.getHasta();
				configuracionDevolucion.setDesde(rangoInicial);
				configuracionDevolucion.setHasta(rangoFinal);
				configuracionDevolucion
						.setPorcentaje(Integer.parseInt(ventanaEditarConfigDevolucion.getTxtPorcentaje().getText()));

				if (this.accesoDatos.updateDevolucionConfig(configuracionDevolucion)) {
					realizarBusqueda(true);
					ventanaEditarConfigDevolucion.cerrar();
				} else {
					configuracionDevolucion.setDesde(desdeOriginal);
					configuracionDevolucion.setHasta(hastaOriginal);
					configuracionDevolucion.setPorcentaje(porcentaje);
					JOptionPane.showMessageDialog(null, "No se pudo editar la configuración de devolución.", "¡Advertencia!", JOptionPane.WARNING_MESSAGE);
				}
			} else {
				JOptionPane.showMessageDialog(null,
						"El rango ingresado se solapa con el de otra configuración de devolución.", "¡Error!", JOptionPane.ERROR_MESSAGE);
			}

		}
	}

	private boolean seSolapanConfigDevoluciones(int rangoInicial, int rangoFinal, int id) {
		for (DevolucionConfig devolucion : configDevolucion_en_tabla) {
			if (devolucion.getHasta() == 0 && rangoFinal == 0) {
				if (id != devolucion.getId()) {
					return true;
				}
			} else if (devolucion.getHasta() == 0 && rangoFinal != 0) {
				if ((devolucion.getDesde() <= rangoInicial || rangoFinal >= devolucion.getDesde())) {
					if (devolucion.getId() != id) {
						return true;
					}
				}
			} else if (devolucion.getHasta() != 0 && rangoFinal == 0) {
				if (devolucion.getDesde() <= rangoInicial && rangoInicial <= devolucion.getHasta()) {
					if (devolucion.getId() != id) {
						return true;
					}
				}
			} else {
				if ((devolucion.getDesde() <= rangoInicial && rangoInicial <= devolucion.getHasta())
						|| (devolucion.getDesde() <= rangoFinal && rangoFinal <= devolucion.getHasta())) {
					if (devolucion.getId() != id) {
						return true;
					}
				}
			}
		}
		return false;
	}

	private void realizarBusqueda(boolean primeraEntrada) {
		limpiarTabla();
//		Predicate<DevolucionConfig> filtro = filtrosDevolucionConfig();
//		if (filtro != null) {
//			List<DevolucionConfig> lista_filtrada = config_en_tabla.stream().filter(filtro)
//					.collect(Collectors.toList());
//			if (lista_filtrada.size() > 0) {
//				lista_filtrada.forEach(devolucionConfig -> agregarDevolucionAFila(devolucionConfig));
//			} else {
//				if (!primeraEntrada) {
//					JOptionPane.showMessageDialog(null, "No se encontraron configuraciones de devolucion.");
//				}
//			}
//		} else {
		configDevolucion_en_tabla.forEach(devolucionConfig -> agregarDevolucionAFila(devolucionConfig));
//		}
	}

	private void agregarDevolucionAFila(DevolucionConfig devolucion) {
		DateTimeFormatter formatoDeFecha = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
		LocalDateTime fechaDeCreacion = devolucion.getFechaCreacion();
		String fechaDeCreacionConFormato = fechaDeCreacion.format(formatoDeFecha);

		String devolucionHasta = "";
		if (devolucion.getHasta() == 0) {
			devolucionHasta = "∞";
		} else {
			devolucionHasta = "" + devolucion.getHasta();
		}

		Object[] fila = { devolucion, devolucion.getDesde(), devolucionHasta, devolucion.getPorcentaje(),
				fechaDeCreacionConFormato, devolucion.getCreador(), devolucion.getEstado() };

		this.ventanaConfigDevolucion.getModelConfig().addRow(fila);
	}

	private void limpiarTabla() {
		this.ventanaConfigDevolucion.getModelConfig().setRowCount(0);
//		this.ventanaConfigDevolucion.getModelConfig().setColumnCount(0);
//		this.ventanaConfigDevolucion.getModelConfig()
//				.setColumnIdentifiers(this.ventanaConfigDevolucion.getNombreColumnas());
	}

	private DevolucionConfig configSeleccionada() throws ArrayIndexOutOfBoundsException {

		int fila = ventanaConfigDevolucion.getTablaConfig().getSelectedRow();

		return (DevolucionConfig) ventanaConfigDevolucion.getModelConfig().getValueAt(fila, 0);
	}

	private void habInhabConfigDevolucion() {
		try {

			DevolucionConfig devolucion = configSeleccionada();
			if (devolucion.getEstado().equals("HABILITADO")) {
				cambiarEstado(devolucion, "INHABILITADO", "INHABILITAR");
			} else {
				cambiarEstado(devolucion, "HABILITADO", "HABILITAR");
			}
		} catch (ArrayIndexOutOfBoundsException ex) {
			JOptionPane.showMessageDialog(null, "Seleccione una configuración de devolución.");
		}
	}

	private void cambiarEstado(DevolucionConfig devolucion, String estado, String accion) {
		int confirm = JOptionPane.showOptionDialog(null,
				"¿Está seguro que quiere " + accion + " esta configuración de devolución?", "Confirmacion",
				JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, null);
		if (confirm == 0) {
			String estadoOriginal = devolucion.getEstado();
			devolucion.setEstado(estado);
			if (this.accesoDatos.updateDevolucionConfig(devolucion)) {
				realizarBusqueda(true);
			} else {
				devolucion.setEstado(estadoOriginal);
				JOptionPane.showMessageDialog(null, "No se pudo " + accion + " el devolucion", "¡Advertencia!", JOptionPane.WARNING_MESSAGE);
			}
		}
	}
}
