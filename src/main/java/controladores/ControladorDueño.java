package controladores;

import controladores.dueño.ControladorReportes;
import modelo.AccesoDatos;
import vistas.Main_Dueño;
import vistas.dueño.reportes.Main_Reportes;
import vistas.perfil.Main_PerfilDeUsuario;

public class ControladorDueño {
	
    private Main_Dueño ventanaMainDueño;
    private Main_PerfilDeUsuario ventanaMainPerfilDeUsuario;
    private Main_Reportes ventanaMainReportes;
    private AccesoDatos accesoDatos;
    private ControladorPerfilDeUsuario controladorPerfilDeUsuario;
    private ControladorReportes controladorReportes;
    

    public ControladorDueño(Main_Dueño VentanaMainDueño, AccesoDatos acceso) {
        this.ventanaMainDueño = VentanaMainDueño;
        this.ventanaMainPerfilDeUsuario = Main_PerfilDeUsuario.getInstance();
        this.ventanaMainReportes = Main_Reportes.getInstance();
        
        this.ventanaMainDueño.getBtnDesconectar().addActionListener(d -> desconectar());
        this.ventanaMainDueño.getBtnPerfilDeUsuario().addActionListener(p -> mostrarMainPerfilDeUsuario());
        this.ventanaMainDueño.getBtnReportes().addActionListener(r -> mostrarMainReportes());
        
        this.accesoDatos = acceso;
        
        controladorReportes = new ControladorReportes(ventanaMainReportes, accesoDatos);
    }

    public void inicializar(ControladorPerfilDeUsuario controladorPerfilDeUsuario) {
    	this.controladorPerfilDeUsuario = controladorPerfilDeUsuario;
    	mostrarMainReportes();
        ventanaMainDueño.mostrarVentana();
    }
    
    private void actualizarVentana() {
		ventanaMainDueño.getPanelCentro().revalidate();
		ventanaMainDueño.getPanelCentro().repaint();
	}
    
    private void mostrarMainPerfilDeUsuario() {
    	ventanaMainDueño.getPanelCentro().removeAll();
    	ventanaMainDueño.getPanelCentro().add(ventanaMainPerfilDeUsuario.getPanel());
    	actualizarVentana();
    	controladorPerfilDeUsuario.inicializar();
    }
    
    private void mostrarMainReportes() {
    	ventanaMainDueño.getPanelCentro().removeAll();
    	ventanaMainDueño.getPanelCentro().add(ventanaMainReportes.getPanel());
    	actualizarVentana();
    	controladorReportes.inicializar();
    }
    
    private void desconectar() {
    	ventanaMainDueño.cerrarVentana();
    	ControladorLogin.desconectar(ControladorLogin.id_logeado());
	}
}