package controladores;

import vistas.Main_Cliente;
import vistas.perfil.Main_PerfilDeUsuario;

public class ControladorCliente {
	
    private Main_Cliente ventanaMainCliente;
    private Main_PerfilDeUsuario ventanaMainPerfilDeUsuario;
    private ControladorPerfilDeUsuario controladorPerfilDeUsuario;
   

    public ControladorCliente(Main_Cliente VentanaMainCliente) {
        this.ventanaMainCliente = VentanaMainCliente;
        this.ventanaMainPerfilDeUsuario = Main_PerfilDeUsuario.getInstance();
        
        this.ventanaMainCliente.getBtnDesconectar().addActionListener(d -> desconectar());
        this.ventanaMainCliente.getBtnPerfilDeUsuario().addActionListener(p -> mostrarMainPerfilDeUsuario());
    }

    public void inicializar(ControladorPerfilDeUsuario controladorPerfilDeUsuario) {
    	this.controladorPerfilDeUsuario = controladorPerfilDeUsuario;
    	mostrarMainPerfilDeUsuario();
        ventanaMainCliente.mostrarVentana();
    }
    
    private void actualizarVentana() {
		ventanaMainCliente.getPanelCentro().revalidate();
		ventanaMainCliente.getPanelCentro().repaint();
	}
    
    private void desconectar() {
    	ventanaMainCliente.cerrarVentana();
    	ControladorLogin.desconectar(ControladorLogin.id_logeado());
	}
    
    private void mostrarMainPerfilDeUsuario() {
		ventanaMainCliente.getPanelCentro().removeAll();
		ventanaMainCliente.getPanelCentro().add(ventanaMainPerfilDeUsuario.getPanel());
		actualizarVentana();
		controladorPerfilDeUsuario.inicializar();
	}
}