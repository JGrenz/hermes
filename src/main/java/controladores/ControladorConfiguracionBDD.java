package controladores;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

import javax.swing.JOptionPane;

import conexion.Conexion;
import modelo.AccesoDatos;
import vistas.VentanaConfiguracionBDD;

public class ControladorConfiguracionBDD {
	private VentanaConfiguracionBDD ventanaConfiguracionBDD;
	private Properties properties;
	private boolean estado = true;

	public ControladorConfiguracionBDD(boolean llamadaDesdeVentana) {
		ventanaConfiguracionBDD = new VentanaConfiguracionBDD();
		properties = new Properties();

		this.ventanaConfiguracionBDD.getBtnConfirmar().addActionListener(e -> confirmarConfiguracion());
		ventanaConfiguracionBDD.getTxtBDD().addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				if (arg0.getKeyCode() == KeyEvent.VK_ENTER) {
					confirmarConfiguracion();
				}
			}
		});
		ventanaConfiguracionBDD.getTxtContraseña().addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				if (arg0.getKeyCode() == KeyEvent.VK_ENTER) {
					confirmarConfiguracion();
				}
			}
		});
		ventanaConfiguracionBDD.getTxtHost().addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				if (arg0.getKeyCode() == KeyEvent.VK_ENTER) {
					confirmarConfiguracion();
				}
			}
		});
		ventanaConfiguracionBDD.getTxtPuerto().addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				if (arg0.getKeyCode() == KeyEvent.VK_ENTER) {
					confirmarConfiguracion();
				}
			}
		});
		ventanaConfiguracionBDD.getTxtUser().addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				if (arg0.getKeyCode() == KeyEvent.VK_ENTER) {
					confirmarConfiguracion();
				}
			}
		});

		inicializar(llamadaDesdeVentana);
	}

	public void inicializar(boolean llamadaDesdeVentana) {
		if (!AccesoDatos.hayConexion() || llamadaDesdeVentana) {
			if(llamadaDesdeVentana) {
				llenarCamposBDD();
			}
			ventanaConfiguracionBDD.mostrarVentana();
		} else {
			estado = false;
		}
	}
	
	private void llenarCamposBDD(){
		Properties properties = new Properties();
		try {
			FileReader fileReader = new FileReader("BDD.properties");
			properties.load(fileReader);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		ventanaConfiguracionBDD.getTxtHost().setText(properties.getProperty("host"));
		ventanaConfiguracionBDD.getTxtPuerto().setText(properties.getProperty("puerto"));
		ventanaConfiguracionBDD.getTxtBDD().setText(properties.getProperty("baseDatos"));
		ventanaConfiguracionBDD.getTxtUser().setText(properties.getProperty("usuario"));
	}

	private void confirmarConfiguracion() {
		if (camposValidos()) {

			Conexion conexion = Conexion.getConexion();

			String usuario = ventanaConfiguracionBDD.getTxtUser().getText();
			String contraseña = ventanaConfiguracionBDD.getTxtContraseña().getText();
			String puerto = ventanaConfiguracionBDD.getTxtPuerto().getText();
			String ip = ventanaConfiguracionBDD.getTxtHost().getText();
			String bdd = ventanaConfiguracionBDD.getTxtBDD().getText();

			if (conexion.probarConexion(ip, puerto, bdd, usuario, contraseña)) {
				tomarInformacionDeConfiguracion();
				ventanaConfiguracionBDD.cerrarVentana();
				this.estado = false;
			} else{
				JOptionPane.showMessageDialog(null, "No se pudo realizar la conexion, verifique los datos",
						"¡Error de conexion!", JOptionPane.ERROR_MESSAGE);
			}

//			tomarInformacionDeConfiguracion();
//			if (AccesoDatos.hayConexion()) {
//				ventanaConfiguracionBDD.cerrarVentana();
//				this.estado = false;
//			} else {
//				JOptionPane.showMessageDialog(null, "No se pudo realizar la conexion, verifique los datos",
//						"¡Error de conexion!", JOptionPane.ERROR_MESSAGE);
//			}
		} else {
			JOptionPane.showMessageDialog(null, "Debe completar todos los campos!", "¡Error!", JOptionPane.WARNING_MESSAGE);
		}
	}

	private boolean camposValidos() {
		// TODO RODREEE - metodo momentaneo por juan
		if (ventanaConfiguracionBDD.getTxtUser().getText().equals("")
				|| ventanaConfiguracionBDD.getTxtContraseña().getText().equals("")
				|| ventanaConfiguracionBDD.getTxtPuerto().getText().equals("")
				|| ventanaConfiguracionBDD.getTxtHost().getText().equals("")
				|| ventanaConfiguracionBDD.getTxtBDD().getText().equals("")) {
			return false;
		} else {
			return true;
		}
	}

	private void tomarInformacionDeConfiguracion() {
		String usuario = ventanaConfiguracionBDD.getTxtUser().getText();
		String contraseña = ventanaConfiguracionBDD.getTxtContraseña().getText();
		String puerto = ventanaConfiguracionBDD.getTxtPuerto().getText();
		String ip = ventanaConfiguracionBDD.getTxtHost().getText();
		String bdd = ventanaConfiguracionBDD.getTxtBDD().getText();

		crearArchivoProperties(ip, puerto, bdd, usuario, contraseña);
	}

	private void crearArchivoProperties(String ip, String puerto, String bdd, String usuario, String contraseña) {

		File file = new File("BDD.properties");
		FileWriter fileWriter = null;
		BufferedWriter bw;

		try {
			fileWriter = new FileWriter("BDD.properties");

			if (!file.exists()) {
				bw = new BufferedWriter(fileWriter);
				bw.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		properties.setProperty("host", ip);
		properties.setProperty("puerto", puerto);
		properties.setProperty("baseDatos", bdd);
		properties.setProperty("usuario", usuario);
		properties.setProperty("contrasenia", contraseña);

		try {
			properties.store(fileWriter,
					"En este archivo se almacenan las configuraciones para la correcta conexión con la base de datos.");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public boolean getEstado() {
		return this.estado;
	}
}
