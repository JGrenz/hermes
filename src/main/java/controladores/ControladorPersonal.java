package controladores;

import java.awt.event.ActionEvent;

import controladores.personaladm.ControladorCliente;
import controladores.personaladm.ControladorPasajes;
import modelo.AccesoDatos;
import vistas.Main_PersonalAdm;
import vistas.eventos.Main_Eventos;
import vistas.perfil.Main_PerfilDeUsuario;
import vistas.personaladm.clientes.Main_Clientes;
import vistas.personaladm.pasajes.Main_Pasajes;

public class ControladorPersonal {
	
    private Main_PersonalAdm ventanaMainPersonalAdm;
    private Main_Clientes ventanaMainClientes;
    private Main_Pasajes ventanaMainPasajes;
    private Main_Eventos ventanaMainEventos;
    private Main_PerfilDeUsuario ventanaMainPerfilDeUsuario;
    private AccesoDatos accesoDatos;
    private ControladorCliente controladorClientes;
    private ControladorPasajes controladorPasajes;
    private ControladorEventos controladorEventos;
    private ControladorPerfilDeUsuario controladorPerfilDeUsuario;

    public ControladorPersonal(Main_PersonalAdm VentanaMainPersonalAdm, AccesoDatos acceso) {
        this.ventanaMainPersonalAdm = VentanaMainPersonalAdm;
        this.ventanaMainClientes = Main_Clientes.getInstance();
        this.ventanaMainPasajes = Main_Pasajes.getInstance();
        this.ventanaMainEventos = Main_Eventos.getInstance();
        this.ventanaMainPerfilDeUsuario = Main_PerfilDeUsuario.getInstance();
        
        this.ventanaMainPersonalAdm.getBtnClientes().addActionListener(c -> mostrarMainClientes());
        this.ventanaMainPersonalAdm.getBtnVentas().addActionListener(v -> mostrarMainPasajes());
        this.ventanaMainPersonalAdm.getBtnEventos().addActionListener(v -> mostrarMainEventos(v));
        this.ventanaMainPersonalAdm.getBtnDesconectar().addActionListener(d -> desconectar());
        this.ventanaMainPersonalAdm.getBtnPerfilDeUsuario().addActionListener(p -> mostrarMainPerfilDeUsuario());

        this.accesoDatos = acceso;

        controladorClientes = new ControladorCliente(ventanaMainClientes, accesoDatos);
        controladorPasajes = new ControladorPasajes(ventanaMainPasajes, accesoDatos);
        //controladorEventos = new ControladorEventos(ventanaMainEventos, accesoDatos);
    }

	public void inicializar(ControladorEventos controladorEventos, ControladorPerfilDeUsuario controladorPerfilDeUsuario) {
	    this.controladorPerfilDeUsuario = controladorPerfilDeUsuario;
		this.controladorEventos = controladorEventos;
		mostrarMainPasajes();
        ventanaMainPersonalAdm.mostrarVentana();
    }

    private void mostrarMainClientes() {
    	ventanaMainPersonalAdm.getPanelCentro().removeAll();
        ventanaMainPersonalAdm.getPanelCentro().add(ventanaMainClientes.getPanel());
        actualizarVentana();
    	controladorClientes.inicializar();
    }

    private void mostrarMainPasajes() {
    	ventanaMainPersonalAdm.getPanelCentro().removeAll();
        ventanaMainPersonalAdm.getPanelCentro().add(ventanaMainPasajes.getPanel());
        actualizarVentana();
        controladorPasajes.inicializar();
    }
    
    private void mostrarMainEventos(ActionEvent t) {
    	ventanaMainPersonalAdm.getPanelCentro().removeAll();
    	ventanaMainPersonalAdm.getPanelCentro().add(ventanaMainEventos.getPanel());
    	actualizarVentana();
    	controladorEventos.inicializar();
    }
    
    private void mostrarMainPerfilDeUsuario() {
    	ventanaMainPersonalAdm.getPanelCentro().removeAll();
    	ventanaMainPersonalAdm.getPanelCentro().add(ventanaMainPerfilDeUsuario.getPanel());
    	actualizarVentana();
    	controladorPerfilDeUsuario.inicializar();
    }
    
    private void actualizarVentana() {
		ventanaMainPersonalAdm.getPanelCentro().revalidate();
		ventanaMainPersonalAdm.getPanelCentro().repaint();
	}

    private void desconectar() {
    	ventanaMainPersonalAdm.cerrarVentana();
    	ControladorLogin.desconectar(ControladorLogin.id_logeado());
	}
}