package controladores;

import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

import dominio.DescripcionEvento;
import dominio.Evento;
import dominio.Notificacion;
import dominio.Usuario;
import modelo.AccesoDatos;
import vistas.eventos.Main_Eventos;
import vistas.eventos.Ventana_AgregarEvento;
import vistas.eventos.Ventana_ExtenderEvento;
import vistas.eventos.Ventana_ModificarResponsable;
import vistas.eventos.Ventana_NuevoMotivoEvento;

public class ControladorEventos {

	private Main_Eventos ventanaMainEvento;
	private Ventana_AgregarEvento ventanaAgregarEvento;
	private Ventana_ModificarResponsable ventanaModificarResponsable;
	private Ventana_NuevoMotivoEvento ventanaNuevoMotivoEvento;
	private Ventana_ExtenderEvento ventanaExtenderEvento;
	private AccesoDatos accesoDatos;
	private List<Evento> eventos_en_tabla;
	private List<Usuario> clientes_en_tabla, empleados_en_tabla;
	private boolean checkClientes = false;
	private JPanel panelClientes;
	private DateTimeFormatter formatoDeFechaHora = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
	private DateTimeFormatter formatoDeFecha = DateTimeFormatter.ofPattern("dd/MM/yyyy");
	private DateTimeFormatter formatoDeHora = DateTimeFormatter.ofPattern("HH:mm");

	public ControladorEventos(Main_Eventos ventEventos, AccesoDatos accDatos) {
		this.ventanaMainEvento = ventEventos;
		this.ventanaAgregarEvento = Ventana_AgregarEvento.getInstance();
		this.ventanaModificarResponsable = Ventana_ModificarResponsable.getInstance();
		this.ventanaNuevoMotivoEvento = Ventana_NuevoMotivoEvento.getInstance();
		this.ventanaExtenderEvento = Ventana_ExtenderEvento.getInstance();

		this.ventanaMainEvento.getBtnAgregar().addActionListener(t -> mostrarVentanaAgregar(t));
		this.ventanaMainEvento.getBtnFinalizar().addActionListener(e -> finalizarEvento(e));
		this.ventanaMainEvento.getBtnResponsable().addActionListener(e -> mostrarVentanaModificarResponsable(e));
		this.ventanaMainEvento.getBtnNuevoMotivo().addActionListener(e -> mostrarVentanaNuevoEvento());
		this.ventanaMainEvento.getBtnExtenderEvento().addActionListener(e -> extenderEvento());

		this.ventanaMainEvento.getTablaEventos().addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Evento evento = datosDeEventoSeleccionado();
				mostrarHistorialEventos(evento);
			}
		});

		this.ventanaMainEvento.getBtnRealizarBusqueda().addActionListener(h -> realizarBusqueda(false));
		this.ventanaMainEvento.getRadioAbierto().addActionListener(h -> realizarBusqueda(false));
		this.ventanaMainEvento.getRadioFinalizado().addActionListener(h -> realizarBusqueda(false));
		this.ventanaMainEvento.getRadioTodos().addActionListener(h -> realizarBusqueda(false));

		this.ventanaAgregarEvento.getBtnConfirmar().addActionListener(a -> agregarEvento(a));
		this.ventanaAgregarEvento.getCheckClientes().addActionListener(a -> habilitarBotonClientes());
		this.ventanaAgregarEvento.getBtnPanelCliente().addActionListener(a -> mostrarVentanaClientes());
		this.ventanaAgregarEvento.getBtnBuscarCliente().addActionListener(a -> buscarCliente());
		this.ventanaAgregarEvento.getBtnSeleccionarCliente().addActionListener(a -> seleccionarCliente());

		this.ventanaModificarResponsable.getBtnConfirmar().addActionListener(l -> modificarResponsable());
		this.ventanaModificarResponsable.getBtnBuscarEmpleado().addActionListener(l -> buscarEmpleado());
		this.ventanaModificarResponsable.getBtnSeleccionarEmpleado().addActionListener(a -> seleccionarEmpleado());

		this.ventanaNuevoMotivoEvento.getBtnConfirmar().addActionListener(e -> confirmarMotivo());

		this.ventanaExtenderEvento.getBtnConfirmar().addActionListener(e -> confimarExtensionDeEvento());

		this.accesoDatos = accDatos;
		this.eventos_en_tabla = new ArrayList<>();
		this.clientes_en_tabla = new ArrayList<>();
		this.empleados_en_tabla = new ArrayList<>();
		this.panelClientes = this.ventanaAgregarEvento.getPanelClientes();
	}

	private void confimarExtensionDeEvento() {
		Evento evento = datosDeEventoSeleccionado();
		DescripcionEvento descripcionEvento = evento.getDescripcionEvento();
		LocalDateTime fechaYHoraActualizada = TomarHoraYFechaVentanaExtender();

		descripcionEvento.setFechaResolucionEstimada(fechaYHoraActualizada);

		if (accesoDatos.updateDescripcionEvento(descripcionEvento)) {
			realizarBusqueda(true);
			Notificacion notificacion = accesoDatos.findNotificacionBy(evento);
			
			notificacion.setHoraVencimiento(descripcionEvento.getFechaResolucionEstimada().toLocalTime());
			notificacion.setFechaVencimiento(descripcionEvento.getFechaResolucionEstimada());

			accesoDatos.updateNotificacion(notificacion);

			ventanaExtenderEvento.cerrar();
		} else {
			accesoDatos.updateDescripcionEvento(descripcionEvento);
			realizarBusqueda(true);
			JOptionPane.showMessageDialog(null, "No se pudo actualizar el evento, intente nuevamente más tarde.",
					"¡Error!", JOptionPane.WARNING_MESSAGE);
		}
	}

	private LocalDateTime TomarHoraYFechaVentanaExtender() {
		LocalDate fechaActualizada = ventanaExtenderEvento.getFechaChooser().getDate().toInstant()
				.atZone(ZoneId.systemDefault()).toLocalDate();

		String horaEvento = new SimpleDateFormat("HH").format(ventanaExtenderEvento.getSpinnerHora().getValue());
		String minutoEvento = new SimpleDateFormat("mm").format(ventanaExtenderEvento.getSpinnerMinuto().getValue());
		LocalTime horarioEvento = LocalTime.parse(horaEvento + ":" + minutoEvento);

		return LocalDateTime.of(fechaActualizada, horarioEvento);
	}

	public void inicializar() {
		llenarCBMainTipo();
		inicializarTablaEvento();
		limpiarTablaDescripcionEvento();
		realizarBusqueda(true);
	}

	private void inicializarTablaEvento() {
		if (ControladorLogin.id_logeado().getRol() == "PERSONAL_ADMINISTRATIVO") {
			String[] nombreColumnasNuevo = { "Titulo", "Cliente", "DNI", "Telefono", "Email", "Tipo", "Descripcion",
					"Estado", "Fecha creación", "Fecha estimada resolución", "Fecha real resolución" };
			ventanaMainEvento.setNombreColumnas(nombreColumnasNuevo);

			this.eventos_en_tabla = accesoDatos.obtenerEventosResponsable(ControladorLogin.id_logeado());
			ventanaMainEvento.setCamposResponsable(false);
		} else {
			String[] nombreColumnasNuevo = { "Titulo", "Cliente", "DNI", "Telefono", "Email", "Tipo", "Descripcion",
					"Estado", "Fecha creación", "Fecha estimada resolución", "Fecha real resolución", "Responsable" };
			ventanaMainEvento.setNombreColumnas(nombreColumnasNuevo);

			this.eventos_en_tabla = accesoDatos.obtenerEventos();
			ventanaMainEvento.setCamposResponsable(true);
		}
		
	}

	private void mostrarVentanaNuevoEvento() {
		try {
			Evento eventoSeleccionado = datosDeEventoSeleccionado();
			if (ControladorLogin.id_logeado().getId() == eventoSeleccionado.getResponsable().getId()) {
				if (eventoSeleccionado.getDescripcionEvento().getEstado().equals("ABIERTO")) {

					prepararVentanaMotivoEvento(eventoSeleccionado);
					ventanaNuevoMotivoEvento.mostrarVentana();
				} else {
					JOptionPane.showMessageDialog(null, "Seleccione un evento con estado Abierto.");
				}
			} else {
				JOptionPane.showMessageDialog(null,
						"No puede agregar un nuevo motivo al evento si no es el responsable de este.");
			}
		} catch (ArrayIndexOutOfBoundsException ex) {
			JOptionPane.showMessageDialog(null, "Seleccione un evento para modificar su responsable.");
		}
	}

	private void prepararVentanaMotivoEvento(Evento eventoSeleccionado) {
		if (eventoSeleccionado.getNombre() == null) {
			ventanaNuevoMotivoEvento.getTxtInteresado().setText(eventoSeleccionado.getCliente().getNombre());
		} else {
			ventanaNuevoMotivoEvento.getTxtInteresado().setText(eventoSeleccionado.getNombre());
		}
		ventanaNuevoMotivoEvento.getTxtTitulo().setText("");
		ventanaNuevoMotivoEvento.getTxtMotivo().setText("");
	}

	private void extenderEvento() {
		try {
			Evento eventoSeleccionado = datosDeEventoSeleccionado();
			if (ControladorLogin.id_logeado().getId() == eventoSeleccionado.getResponsable().getId()) {
				if (eventoSeleccionado.getDescripcionEvento().getEstado().equals("ABIERTO")) {
					cargarDatosVentanaExtenderEvento(eventoSeleccionado);

					ventanaExtenderEvento.mostrarVentana();

				} else {
					JOptionPane.showMessageDialog(null, "Seleccione un evento con estado Abierto.");
				}
			} else {
				JOptionPane.showMessageDialog(null,
						"No puede extender la fecha de resolución al evento si no es el responsable de este.");
			}
		} catch (ArrayIndexOutOfBoundsException ex) {
			JOptionPane.showMessageDialog(null, "Seleccione un evento para modificar su responsable.");
		}
	}

	private void cargarDatosVentanaExtenderEvento(Evento evento) {
		if (evento.getCliente() == null) {
			ventanaExtenderEvento.getTxtInteresado().setText(evento.getNombre());
		} else {
			ventanaExtenderEvento.getTxtInteresado().setText(evento.getCliente().toString());
		}
		ventanaExtenderEvento.getCBTipo().setSelectedItem(evento.getDescripcionEvento().getTipo());
		ventanaExtenderEvento.getTxtTitulo().setText(evento.getDescripcionEvento().getTitulo());
		ventanaExtenderEvento.getTxtMotivo().setText(evento.getDescripcionEvento().getDescripcion());

		LocalDateTime fechaResolucionEstimada = evento.getDescripcionEvento().getFechaResolucionEstimada();
		java.sql.Date fechaVencimientoEstimadaDate = java.sql.Date.valueOf(fechaResolucionEstimada.toLocalDate());

		ventanaExtenderEvento.getFechaChooser().setDate(fechaVencimientoEstimadaDate);

		cargarSpinnerHora(fechaResolucionEstimada);
	}

	private void cargarSpinnerHora(LocalDateTime fechaResolucionEstimada) {
		Calendar calendar = Calendar.getInstance();
		calendar.clear();
		calendar.set(0, 0, 0, fechaResolucionEstimada.getHour(), fechaResolucionEstimada.getMinute(),
				fechaResolucionEstimada.getSecond());

		ventanaExtenderEvento.getSpinnerHora().setValue(calendar.getTime());
		ventanaExtenderEvento.getSpinnerMinuto().setValue(calendar.getTime());
	}

	private void mostrarHistorialEventos(Evento evento) {
		List<DescripcionEvento> descripcionesEventos = accesoDatos.findDescripcionesEventosBy(evento);

		agregarATabla(descripcionesEventos);
	}

	public void agregarATabla(List<DescripcionEvento> descripcionesEventos) {
		limpiarTablaDescripcionEvento();
		DateTimeFormatter formatoDeFechaHora = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
		DateTimeFormatter formatoDeFecha = DateTimeFormatter.ofPattern("dd/MM/yyyy");

		for (DescripcionEvento descripcionesEvento : descripcionesEventos) {

			String fechaDeResolucionReal = "";
			if (descripcionesEvento.getFechaResolucionReal() != null) {
				fechaDeResolucionReal = descripcionesEvento.getFechaResolucionReal().toString();
				fechaDeResolucionReal = descripcionesEvento.getFechaResolucionReal().format(formatoDeFecha);
			}
			Object[] fila = { descripcionesEvento.getTipo(), descripcionesEvento.getDescripcion(),
					formatoDeFechaHora.format(descripcionesEvento.getFechaResolucionEstimada()),
					fechaDeResolucionReal };

			this.ventanaMainEvento.getModelHistorialEventos().addRow(fila);
		}
	}

	private void limpiarTablaDescripcionEvento() {
		this.ventanaMainEvento.getModelHistorialEventos().setRowCount(0);
		this.ventanaMainEvento.getModelHistorialEventos().setColumnCount(0);
		this.ventanaMainEvento.getModelHistorialEventos()
				.setColumnIdentifiers(this.ventanaMainEvento.getNombreColumnasHistorial());
	}

	private void realizarBusqueda(boolean primeraEntrada) {
		Predicate<Evento> filtro = filtrosEvento();
		if (filtro != null) {
			List<Evento> lista_filtrada = this.eventos_en_tabla.stream().filter(filtro).collect(Collectors.toList());

			if (lista_filtrada.size() > 0) {
				limpiarTablaEventos();
				lista_filtrada.forEach(evento -> agregarATabla(evento));
			} else {
				limpiarTablaEventos();
				if (!primeraEntrada) {
					JOptionPane.showMessageDialog(null, "No se encontraron eventos.");
				}
			}
		} else {
			limpiarTablaEventos();
			eventos_en_tabla.forEach(evento -> agregarATabla(evento));
		}
	}

	private Predicate<Evento> filtrosEvento() {
		List<Predicate<Evento>> lista_filtros = new ArrayList<>();

		String estado = radioButtonSeleccionado();
		if (!estado.equals("TODOS")) {
			Predicate<Evento> filtroEstado = Evento -> Evento.getDescripcionEvento().getEstado().equals(estado);
			lista_filtros.add(filtroEstado);
		}

		String usuario = ventanaMainEvento.getTxtUsuario().getText().toLowerCase();
		if (!usuario.isEmpty()) {
			Predicate<Evento> filtroUsuario = new Predicate<Evento>() {
				@Override
				public boolean test(Evento e) {
					if (e.getCliente() != null) {
						return e.getCliente().getUsuario().toLowerCase().indexOf(usuario) > -1;
					} else {
						return false;
					}
				}
			};
			lista_filtros.add(filtroUsuario);
		}

		String dni = ventanaMainEvento.getTxtDni().getText();
		if (!dni.isEmpty()) {
			Predicate<Evento> filtroDni = new Predicate<Evento>() {
				@Override
				public boolean test(Evento e) {
					if (e.getCliente() != null) {
						return e.getCliente().getDni().indexOf(dni) > -1;
					} else {
						return false;
					}
				}
			};
			lista_filtros.add(filtroDni);
		}

		String nombreYapellido = ventanaMainEvento.getTxtNombre().getText().toLowerCase();
		if (!nombreYapellido.isEmpty()) {
			Predicate<Evento> filtroNombre = new Predicate<Evento>() {
				@Override
				public boolean test(Evento e) {
					if (e.getCliente() != null) {
						return e.getCliente().getNombre().concat(" ").concat(e.getCliente().getApellido()).toLowerCase()
								.indexOf(nombreYapellido) > -1;
					} else {
						return e.getNombre().toLowerCase().indexOf(nombreYapellido) > -1;
					}
				}
			};
			lista_filtros.add(filtroNombre);
		}

		String tipo = (String) ventanaMainEvento.getCBTipoEvento().getSelectedItem();
		if (!tipo.equals("TODOS")) {
			Predicate<Evento> filtroTipo = evento -> evento.getDescripcionEvento().getTipo().equals(tipo);
			lista_filtros.add(filtroTipo);
		}

		String usuarioResponsable = ventanaMainEvento.getTxtResponsable().getText().toLowerCase();
		if (!usuarioResponsable.isEmpty()) {
			Predicate<Evento> filtroUsuario = evento -> evento.getResponsable().getUsuario().toLowerCase()
					.indexOf(usuarioResponsable) > -1;
			lista_filtros.add(filtroUsuario);
		}

		if (lista_filtros.size() == 1) {
			return lista_filtros.get(0);
		} else if (lista_filtros.size() > 1) {
			Predicate<Evento> acumulador = lista_filtros.get(0);
			lista_filtros.remove(0);
			for (Predicate<Evento> predicado : lista_filtros) {
				acumulador = acumulador.and(predicado);
			}
			return acumulador;
		}
		return null;
	}

	private String radioButtonSeleccionado() {
		if (this.ventanaMainEvento.getRadioAbierto().isSelected()) {
			return "ABIERTO";
		} else if (this.ventanaMainEvento.getRadioFinalizado().isSelected()) {
			return "FINALIZADO";
		}
		return "TODOS";
	}

	private void llenarCBMainTipo() {
		if (this.ventanaMainEvento.getCBTipoEvento().getItemCount() == 0) {
			this.ventanaMainEvento.getCBTipoEvento().addItem("TODOS");
			this.ventanaMainEvento.getCBTipoEvento().addItem("RECLAMO");
			this.ventanaMainEvento.getCBTipoEvento().addItem("CONSULTA");
			this.ventanaMainEvento.getCBTipoEvento().addItem("OTRO");
		}
	}

	private void limpiarTablaEventos() {
		this.ventanaMainEvento.getModelEventos().setRowCount(0);
		this.ventanaMainEvento.getModelEventos().setColumnCount(0);
		this.ventanaMainEvento.getModelEventos().setColumnIdentifiers(this.ventanaMainEvento.getNombreColumnas());
	}

	public void agregarATabla(Evento evento) {
		String nombre = "";
		String email = "";
		String telefono = "";
		String dni = "";
		if (evento.getCliente() != null) {
			nombre = evento.getCliente().getNombre() + " " + evento.getCliente().getApellido();
			email = evento.getCliente().getEmail();
			telefono = evento.getCliente().getContacto().getTelefono1();
			dni = evento.getCliente().getDni();
		} else {
			nombre = evento.getNombre();
			email = evento.getMail();
			telefono = evento.getTelefono();
		}

		LocalDateTime fechaDeCreacion = evento.getDescripcionEvento().getFechaCreacion();
		String fechaDeCreacionConFormato = fechaDeCreacion.format(formatoDeFechaHora);
		LocalDateTime fechaDeResolucionEstimada = evento.getDescripcionEvento().getFechaResolucionEstimada();
		String fechaDeResolucionEstimaConFormato = fechaDeResolucionEstimada.format(formatoDeFechaHora);
		LocalDate fechaDeResolucionReal = evento.getDescripcionEvento().getFechaResolucionReal();
		String fechaDeResolucionRealConFormato = "";
		if (fechaDeResolucionReal != null) {
			fechaDeResolucionRealConFormato = fechaDeResolucionReal.format(formatoDeFecha);
		}

		if (ControladorLogin.id_logeado().getRol() == "PERSONAL_ADMINISTRATIVO") {
			Object[] fila = { evento, nombre, dni, telefono, email, evento.getDescripcionEvento().getTipo(),
					evento.getDescripcionEvento().getDescripcion(), evento.getDescripcionEvento().getEstado(),
					fechaDeCreacionConFormato, fechaDeResolucionEstimaConFormato, fechaDeResolucionRealConFormato };
			this.ventanaMainEvento.getModelEventos().addRow(fila);
		} else {
			Object[] fila = { evento, nombre, dni, telefono, email, evento.getDescripcionEvento().getTipo(),
					evento.getDescripcionEvento().getDescripcion(), evento.getDescripcionEvento().getEstado(),
					fechaDeCreacionConFormato, fechaDeResolucionEstimaConFormato, fechaDeResolucionRealConFormato,
					evento.getResponsable().getUsuario() };
			this.ventanaMainEvento.getModelEventos().addRow(fila);
		}
	}

	private void mostrarVentanaAgregar(ActionEvent t) {
		this.ventanaAgregarEvento.mostrarVentana();
	}

	private void agregarEvento(ActionEvent e) {
		if (this.ventanaAgregarEvento.verificarCampo()) { // TODO HACER BIEN ESTO
			Evento evento = crearEvento();
			DescripcionEvento descripcion = crearDescripcionEvento();
			evento.setDescripcionEvento(descripcion);
			Notificacion notificacion = crearNotificacion(evento);

			if (this.accesoDatos.agregarEvento(evento, descripcion)
					&& this.accesoDatos.agregarNotificacion(notificacion)) {
				this.eventos_en_tabla.add(evento);

				agregarATabla(evento);
				realizarBusqueda(true);

				this.ventanaAgregarEvento.cerrar();
			} else {
				JOptionPane.showMessageDialog(null, "No se pudo agregar el evento");
			}
		}
	}

	private DescripcionEvento crearDescripcionEvento() {
		LocalDate fecha = this.ventanaAgregarEvento.getTxtFechaResolucion();

		DescripcionEvento descripcion = new DescripcionEvento(this.ventanaAgregarEvento.getTextTitulo(),
				this.ventanaAgregarEvento.getTextMotivo(), this.ventanaAgregarEvento.getTipoEvento(),
				LocalDateTime.of(fecha, this.ventanaAgregarEvento.getTxtHoraEvento()));

		return descripcion;
	}

	private Evento crearEvento() {
		Evento evento = new Evento(ControladorLogin.id_logeado(), ControladorLogin.id_logeado());

		verificarDestinatario(evento);

		return evento;
	}

	private Notificacion crearNotificacion(Evento evento) {
		LocalTime horaVencimiento = this.ventanaAgregarEvento.getTxtHoraEvento();

		String tituloEvento = evento.getDescripcionEvento().getTitulo();
		String interesado = "";
		if (evento.getCliente() != null) {
			interesado = "Cliente " + evento.getCliente().getUsuario();
		} else {
			interesado = evento.getNombre();
		}

		LocalDateTime fechaDeResolucion = evento.getDescripcionEvento().getFechaResolucionEstimada();
		String fecha = fechaDeResolucion.format(formatoDeFecha);
		String hora = fechaDeResolucion.format(formatoDeHora);

		String contenido = "Recuerde que para este momento (" + fecha + " a las " + hora
				+ "hs.) ya debe haber resuelto el evento " + tituloEvento + "." + "\nTipo del evento: "
				+ evento.getDescripcionEvento().getTipo() + "\nInteresado: " + interesado + "\nDescripción: "
				+ evento.getDescripcionEvento().getDescripcion()
				+ "\nDe ya haberlo completado, marquelo como finalizado.";

		return new Notificacion(horaVencimiento, "Recordatorio de evento", contenido, evento);
	}

	private void habilitarBotonClientes() {
		if (ventanaAgregarEvento.getCheckClientes().isSelected()) {
			ventanaAgregarEvento.getBtnPanelCliente().setEnabled(true);
			editarCampos(false);
		} else {
			ventanaAgregarEvento.getBtnPanelCliente().setEnabled(false);
			ventanaAgregarEvento.getBtnPanelCliente().setText("Buscar cliente");
			checkClientes = false;
			acomodarPanel();
			editarCampos(true);
			limpiarCampos();
		}
	}

	private void editarCampos(boolean bool) {
		ventanaAgregarEvento.getInteresado().setEditable(bool);
		ventanaAgregarEvento.getTxtTelefono().setEditable(bool);
		ventanaAgregarEvento.getTxtEmail().setEditable(bool);
	}

	private void limpiarCampos() {
		ventanaAgregarEvento.getInteresado().setText("");
		ventanaAgregarEvento.getTxtTelefono().setText("");
		ventanaAgregarEvento.getTxtEmail().setText("");
	}

	private void mostrarVentanaClientes() {
		if (checkClientes == false) {
			checkClientes = true;
			ventanaAgregarEvento.getBtnPanelCliente().setText("Cerrar panel");
			Thread th = new Thread() {
				@Override
				public void run() {
					try {
						while (panelClientes.getX() > 0) {
							Thread.sleep((long) 0.99);
							ventanaAgregarEvento.getBtnPanelCliente().setEnabled(false);
							panelClientes.setBounds(panelClientes.getX() - 1, panelClientes.getY(),
									panelClientes.getWidth(), panelClientes.getHeight());
						}
						ventanaAgregarEvento.getBtnPanelCliente().setEnabled(true);
					} catch (Exception ex) {
						System.out.println(ex);
					}
				}
			};
			th.start();
		} else {
			checkClientes = false;
			ventanaAgregarEvento.getBtnPanelCliente().setText("Buscar cliente");
			Thread th = new Thread() {

				@Override
				public void run() {
					try {
						while (panelClientes.getX() < panelClientes.getWidth()) {
							Thread.sleep((long) 0.99);
							ventanaAgregarEvento.getBtnPanelCliente().setEnabled(false);
							panelClientes.setBounds(panelClientes.getX() + 1, panelClientes.getY(),
									panelClientes.getWidth(), panelClientes.getHeight());
						}
						ventanaAgregarEvento.getBtnPanelCliente().setEnabled(true);
					} catch (Exception ex) {
						System.out.println(ex);
					}
				}

			};
			th.start();
		}
	}

	private void verificarDestinatario(Evento evento) {
		if (ventanaAgregarEvento.getCheckClientes().isSelected()) {
			Usuario cliente = accesoDatos.findByNombreUSuario(ventanaAgregarEvento.getInteresado().getText());
			evento.setCliente(cliente);
		} else {
			evento.setNombre(ventanaAgregarEvento.getInteresado().getText());
			evento.setTelefono(ventanaAgregarEvento.getTxtTelefono().getText());
			evento.setMail(ventanaAgregarEvento.getTxtEmail().getText());
		}
	}

	private void seleccionarCliente() {
		try {
			Usuario clienteSeleccionado = this.clientes_en_tabla
					.get(this.ventanaAgregarEvento.getTablaClientes().getSelectedRow());

			ventanaAgregarEvento.getInteresado().setText(clienteSeleccionado.getUsuario());
			ventanaAgregarEvento.getTxtEmail().setText(clienteSeleccionado.getEmail());
			ventanaAgregarEvento.getTxtTelefono().setText(clienteSeleccionado.getContacto().getTelefono1());

		} catch (ArrayIndexOutOfBoundsException ex) {
			JOptionPane.showMessageDialog(null, "Se debe seleccionar un cliente", "¡Error!", JOptionPane.ERROR_MESSAGE);
		}

		ventanaAgregarEvento.getBtnPanelCliente().setText("Buscar cliente");
		checkClientes = false;
		acomodarPanel();
	}

	private void mostrarVentanaModificarResponsable(ActionEvent e) {
		try {
			Evento eventoSeleccionado = datosDeEventoSeleccionado();
			if (eventoSeleccionado.getDescripcionEvento().getEstado().equals("ABIERTO")) {
				this.llenarCamposModificarEvento(eventoSeleccionado);
				this.ventanaModificarResponsable.mostrarVentana();
			} else {
				JOptionPane.showMessageDialog(null, "Seleccione un evento con estado Abierto.");
			}
		} catch (ArrayIndexOutOfBoundsException ex) {
			JOptionPane.showMessageDialog(null, "Seleccione un evento para modificar su responsable.");
		}
	}

	private void modificarResponsable() {
		try {
			if (ventanaModificarResponsable.verificarCampo()) {

				Evento evento = datosDeEventoSeleccionado();

				Usuario nuevoResponsable = empleadoSeleccionado();
				Usuario responsableActual = evento.getResponsable();
				String motivoActual = evento.getMotivo();

				evento.setResponsable(nuevoResponsable);
				evento.setMotivo(ventanaModificarResponsable.getMotivo().getText());

				if (!this.accesoDatos.modificarResponsable(evento)) {
					evento.setResponsable(responsableActual);
					evento.setMotivo(motivoActual);
					JOptionPane.showMessageDialog(null, "No se pudo modificar el responsable del evento");
				} else {
					realizarBusqueda(true);
					ventanaModificarResponsable.cerrar();
				}
			}
		} catch (ArrayIndexOutOfBoundsException ex) {
			JOptionPane.showMessageDialog(null, "Seleccione un evento.");
		}
	}

	private void llenarCamposModificarEvento(Evento evento) {
		this.ventanaModificarResponsable.getResponsableOriginal().setText(evento.getResponsable().toString());
	}

	private void acomodarPanel() {
		panelClientes.setBounds(470, panelClientes.getY(), panelClientes.getWidth(), panelClientes.getHeight());
	}

	private void buscarCliente() {
		this.clientes_en_tabla.clear();
		limpiarTablaCliente();
		Predicate<Usuario> filtro = filtroDeCliente();
		this.accesoDatos.obtenerClientes().stream()
				.filter(cliente -> (filtro.test(cliente) && cliente.getEstado().equals("HABILITADO")))
				.forEach(cliente -> {
					mostrarClientes(cliente);
					this.clientes_en_tabla.add(cliente);
				});
		if (this.clientes_en_tabla.isEmpty()) {
			JOptionPane.showMessageDialog(null, "No se encontraron clientes con los parametros especificados");
		}
	}

	private void limpiarTablaCliente() {
		this.ventanaAgregarEvento.getModelClientes().setRowCount(0);
		this.ventanaAgregarEvento.getModelClientes().setColumnCount(0);
		this.ventanaAgregarEvento.getModelClientes()
				.setColumnIdentifiers(this.ventanaAgregarEvento.getColumnasDeClientes());
	}

	private Predicate<Usuario> filtroDeCliente() {
		List<Predicate<Usuario>> lista_filtros = new ArrayList<>();

		String nombre = this.ventanaAgregarEvento.getTxtNombre().getText().toLowerCase();
		if (!nombre.equals("")) {
			Predicate<Usuario> predicadoNombre = cliente -> cliente.getNombre().toLowerCase().indexOf(nombre) > -1;
			lista_filtros.add(predicadoNombre);
		}

		String apellido = this.ventanaAgregarEvento.getTxtApellido().getText().toLowerCase();
		if (!apellido.equals("")) {
			Predicate<Usuario> predicadoApellido = cliente -> cliente.getApellido().toLowerCase()
					.indexOf(apellido) > -1;
			lista_filtros.add(predicadoApellido);
		}

		String dni = this.ventanaAgregarEvento.getTxtDNI().getText();
		if (!dni.equals("")) {
			Predicate<Usuario> predicadoDni = cliente -> cliente.getDni().equals(dni);
			lista_filtros.add(predicadoDni);
		}

		String usuario = this.ventanaAgregarEvento.getTxtUsuario().getText();
		if (!usuario.equals("")) {
			Predicate<Usuario> predicadoUsuario = cliente -> cliente.getUsuario().indexOf(usuario) > -1;
			lista_filtros.add(predicadoUsuario);
		}

		if (lista_filtros.size() == 1) {
			return lista_filtros.get(0);
		} else if (lista_filtros.size() > 1) {
			Predicate<Usuario> acumulador = lista_filtros.get(0);
			lista_filtros.remove(0);
			for (Predicate<Usuario> predicado : lista_filtros) {
				acumulador = acumulador.and(predicado);
			}
			return acumulador;
		}
		return (cliente -> cliente != null);
	}

	private void mostrarClientes(Usuario cliente) {
		String[] fila = { cliente.getNombre(), cliente.getApellido(), cliente.getDni(), cliente.getUsuario() };
		this.ventanaAgregarEvento.getModelClientes().addRow(fila);
	}

	private void finalizarEvento(ActionEvent h) {
		try {
			Evento evento = datosDeEventoSeleccionado();
			if (evento.getDescripcionEvento().getEstado().equals("ABIERTO")) {
				finalizarEvento(evento, "FINALIZADO");
			} else {
				JOptionPane.showMessageDialog(null, "Este evento ya se encuentra finalizado.");
			}
		} catch (ArrayIndexOutOfBoundsException ex) {
			JOptionPane.showMessageDialog(null, "Seleccione un evento.");
		}
	}

	private void finalizarEvento(Evento evento, String estado) {
		try {
			int confirm = JOptionPane.showOptionDialog(null, "¿Está seguro que quiere finalizar este evento?",
					"Confirmacion", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, null);
			if (confirm == 0) {
				String estadoActual = evento.getDescripcionEvento().getEstado();
				LocalDate resolucionActual = evento.getDescripcionEvento().getFechaResolucionReal();
				evento.getDescripcionEvento().setEstado("FINALIZADO");
				evento.getDescripcionEvento().setFechaResolucionReal(LocalDate.now());
				if (!this.accesoDatos.finalizarEvento(evento)) {
					evento.getDescripcionEvento().setEstado(estadoActual);
					evento.getDescripcionEvento().setFechaResolucionReal(resolucionActual);
					JOptionPane.showMessageDialog(null, "No se pudo finalizar el evento");
				} else {
					realizarBusqueda(true);
				}
			}
		} catch (Exception e2) {
			JOptionPane.showMessageDialog(null, "Seleccione un evento que quiera finalizar");
		}
	}

	private Evento datosDeEventoSeleccionado() throws ArrayIndexOutOfBoundsException {
		int index = this.ventanaMainEvento.getTablaEventos().getSelectedRow();
		return (Evento) this.ventanaMainEvento.getModelEventos().getValueAt(index, 0);
	}

	private void buscarEmpleado() {
		this.empleados_en_tabla.clear();
		limpiarTablaEmpleados();
		Predicate<Usuario> filtro = filtroDeEmpleados();
		this.accesoDatos.obtenerEmpleados().stream().filter(empleado -> (filtro.test(empleado)
				&& empleado.getEstado().equals("HABILITADO")
				&& (empleado.getRol().equals("PERSONAL_ADMINISTRATIVO") || empleado.getRol().equals("COORDINADOR"))))
				.forEach(empleado -> {
					mostrarEmpleado(empleado);
					this.empleados_en_tabla.add(empleado);
				});
		if (this.empleados_en_tabla.isEmpty()) {
			JOptionPane.showMessageDialog(null, "No se encontraron empleados con los parametros especificados");
		}
	}

	private void limpiarTablaEmpleados() {
		this.ventanaModificarResponsable.getModelEmpleados().setRowCount(0);
		this.ventanaModificarResponsable.getModelEmpleados().setColumnCount(0);
		this.ventanaModificarResponsable.getModelEmpleados()
				.setColumnIdentifiers(this.ventanaModificarResponsable.getColumnasDeEmpleados());
	}

	private Predicate<Usuario> filtroDeEmpleados() {
		List<Predicate<Usuario>> lista_filtros = new ArrayList<>();

		String nombre = this.ventanaModificarResponsable.getTxtNombre().getText().toLowerCase();
		if (!nombre.equals("")) {
			Predicate<Usuario> predicadoNombre = empleado -> empleado.getNombre().toLowerCase().indexOf(nombre) > -1;
			lista_filtros.add(predicadoNombre);
		}

		String apellido = this.ventanaModificarResponsable.getTxtApellido().getText().toLowerCase();
		if (!apellido.equals("")) {
			Predicate<Usuario> predicadoApellido = empleado -> empleado.getApellido().toLowerCase()
					.indexOf(apellido) > -1;
			lista_filtros.add(predicadoApellido);
		}

		String dni = this.ventanaModificarResponsable.getTxtDNI().getText();
		if (!dni.equals("")) {
			Predicate<Usuario> predicadoDni = empleado -> empleado.getDni().equals(dni);
			lista_filtros.add(predicadoDni);
		}

		String usuario = this.ventanaModificarResponsable.getTxtUsuario().getText();
		if (!usuario.equals("")) {
			Predicate<Usuario> predicadoUsuario = empleado -> empleado.getUsuario().indexOf(usuario) > -1;
			lista_filtros.add(predicadoUsuario);
		}

		if (lista_filtros.size() == 1) {
			return lista_filtros.get(0);
		} else if (lista_filtros.size() > 1) {
			Predicate<Usuario> acumulador = lista_filtros.get(0);
			lista_filtros.remove(0);
			for (Predicate<Usuario> predicado : lista_filtros) {
				acumulador = acumulador.and(predicado);
			}
			return acumulador;
		}
		return (empleado -> empleado != null);
	}

	private void mostrarEmpleado(Usuario empleado) {
		String[] fila = { empleado.getNombre(), empleado.getApellido(), empleado.getDni(), empleado.getUsuario() };
		this.ventanaModificarResponsable.getModelEmpleados().addRow(fila);
	}

	private void seleccionarEmpleado() {
		try {
			Usuario empleadoSeleccionado = empleadoSeleccionado();
			ventanaModificarResponsable.getResponsableNuevo().setText(empleadoSeleccionado.getUsuario());

		} catch (ArrayIndexOutOfBoundsException ex) {
			JOptionPane.showMessageDialog(null, "Se debe seleccionar un empleado", "¡Error!",
					JOptionPane.ERROR_MESSAGE);
		}
	}

	private Usuario empleadoSeleccionado() {
		Usuario empleadoSeleccionado = this.empleados_en_tabla
				.get(this.ventanaModificarResponsable.getTablaEmpleados().getSelectedRow());
		return empleadoSeleccionado;
	}

	private void confirmarMotivo() {
		if (ventanaNuevoMotivoEvento.camposVacios()) {

			Evento evento = datosDeEventoSeleccionado();
			DescripcionEvento descripcionEvento = crearDescripcionEventoConVentana();

			Notificacion notificacion = accesoDatos.findNotificacionBy(evento);

			notificacion.setFechaVencimiento(descripcionEvento.getFechaResolucionEstimada());
			notificacion.setHoraVencimiento(descripcionEvento.getFechaResolucionEstimada().toLocalTime());
			notificacion.setContenido(contenidoDeNuevoMotivo(evento, descripcionEvento));

			accesoDatos.updateNotificacion(notificacion);

			reemplazarDescripcionActual(evento, descripcionEvento);

			ventanaNuevoMotivoEvento.cerrar();
		}
	}

	private DescripcionEvento crearDescripcionEventoConVentana() {
		
		LocalDate fechaResolucion = ventanaNuevoMotivoEvento.getTxtFechaResolucionEstimada().toLocalDate();
		LocalTime horaResolucion = ventanaNuevoMotivoEvento.getTxtHoraEvento();
		LocalDateTime fechaYHoraResolucion = LocalDateTime.of(fechaResolucion, horaResolucion);
		
		DescripcionEvento nuevaDescripcion = new DescripcionEvento(ventanaNuevoMotivoEvento.getTxtTitulo().getText(),
				ventanaNuevoMotivoEvento.getTextMotivo(), ventanaNuevoMotivoEvento.getTipoEvento(),
				fechaYHoraResolucion);
		
		return nuevaDescripcion;
	}

	private String contenidoDeNuevoMotivo(Evento evento, DescripcionEvento descripcionEvento) {

		String tituloEvento = descripcionEvento.getTitulo();
		String interesado = "";
		if (evento.getCliente() != null) {
			interesado = "Cliente " + evento.getCliente().getUsuario();
		} else {
			interesado = evento.getNombre();
		}

		LocalDateTime fechaDeResolucion = descripcionEvento.getFechaResolucionEstimada();
		String fecha = fechaDeResolucion.format(formatoDeFecha);
		String hora = fechaDeResolucion.format(formatoDeHora);

		String contenido = "Recuerde que para este momento (" + fecha + " a las " + hora
				+ "hs.) ya debe haber resuelto el evento " + tituloEvento + "." + "\nTipo del evento: "
				+ descripcionEvento.getTipo() + "\nInteresado: " + interesado + "\nDescripción: "
				+ descripcionEvento.getDescripcion() + "\nDe ya haberlo completado, marquelo como finalizado.";

		return contenido;
	}

	private void reemplazarDescripcionActual(Evento evento, DescripcionEvento descripcionEvento) {
		boolean pudoGuardarse = true;
		if (!accesoDatos.insertDescripcionEvento(descripcionEvento, evento.getId())) {
			JOptionPane.showMessageDialog(null,
					"Lamentablemente no se pudo guardar la nueva descripcion del evento, intente nuevamente más tarde",
					"¡Advertencia!", JOptionPane.WARNING_MESSAGE);
			pudoGuardarse = false;
		}

		finalizarDescripcionAnterior(evento.getDescripcionEvento());

		evento.setDescripcionEvento(descripcionEvento);

		if (!accesoDatos.updateEvento(evento)) {
			JOptionPane.showMessageDialog(null,
					"Lamentablemente no se pudo asociar la nueva descripción al evento, intente nuevamente más tarde",
					"¡Advertencia!", JOptionPane.WARNING_MESSAGE);
			pudoGuardarse = false;
		}

		if (pudoGuardarse) {
			for (Evento eventoEnTabla : eventos_en_tabla) {
				if (eventoEnTabla.equals(evento)) {
					eventoEnTabla = evento;
				}
			}
			realizarBusqueda(true);
		}
	}

	private void finalizarDescripcionAnterior(DescripcionEvento descripcionEvento) {
		descripcionEvento.setFechaResolucionReal(LocalDate.now());
		descripcionEvento.setEstado("FINALIZADO");
		if (!accesoDatos.updateDescripcionEvento(descripcionEvento)) {
			JOptionPane.showMessageDialog(null, "Lamentablemente no se pudo actualizar la descripción anterior.",
					"¡Advertencia!", JOptionPane.WARNING_MESSAGE);
		}
	}

}