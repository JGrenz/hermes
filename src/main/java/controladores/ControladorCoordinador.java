package controladores;

import controladores.coordinador.ControladorGastos;
import controladores.coordinador.ControladorOfertas;
import controladores.coordinador.ControladorTransporte;
import controladores.coordinador.ControladorUbicaciones;
import controladores.coordinador.ControladorViajes;
import modelo.AccesoDatos;
import vistas.Main_Coordinador;
import vistas.coordinador.gastos.Main_Gastos;
import vistas.coordinador.ofertas.Main_Ofertas;
import vistas.coordinador.transporte.Main_Transporte;
import vistas.coordinador.ubicaciones.Main_Ubicaciones;
import vistas.coordinador.viajes.Main_Viajes;
import vistas.eventos.Main_Eventos;
import vistas.perfil.Main_PerfilDeUsuario;

public class ControladorCoordinador {
    private Main_Coordinador ventanaMainCoordinador;
    private Main_Ubicaciones ventanaMainUbicaciones;
    private Main_Viajes ventanaMainViajes;
    private Main_Transporte ventanaMainTransportes;
    private Main_Ofertas ventanaMainOfertas;
    private Main_Eventos ventanaMainEventos;
    private Main_Gastos ventanaMainGastos;
    private Main_PerfilDeUsuario ventanaMainPerfilDeUsuario;
    private AccesoDatos accesoDatos;
    private ControladorTransporte controladorTransporte;
    private ControladorOfertas controladorOfertas;
    private ControladorViajes controladorViajes;
    private ControladorUbicaciones controladorUbicaciones;
    private ControladorEventos controladorEventos;
    private ControladorGastos controladorGastos;
    private ControladorPerfilDeUsuario controladorPerfilDeUsuario;


    public ControladorCoordinador(Main_Coordinador ventanaMainCoordinador, AccesoDatos acceso) {
        this.ventanaMainCoordinador = ventanaMainCoordinador;
        this.ventanaMainUbicaciones = Main_Ubicaciones.getInstance();
        this.ventanaMainViajes = Main_Viajes.getInstance();
        this.ventanaMainTransportes = Main_Transporte.getInstance();
        this.ventanaMainEventos = Main_Eventos.getInstance();
        this.ventanaMainOfertas = Main_Ofertas.getInstance();
        this.ventanaMainGastos = Main_Gastos.getInstance();
        this.ventanaMainPerfilDeUsuario = Main_PerfilDeUsuario.getInstance();

        this.ventanaMainCoordinador.getBtnUbicaciones().addActionListener(u -> mostrarMainUbicaciones());
        this.ventanaMainCoordinador.getBtnViajes().addActionListener(v -> mostrarMainViajes());
        this.ventanaMainCoordinador.getBtnTransporte().addActionListener(t -> mostrarMainTransporte());
        this.ventanaMainCoordinador.getBtnOfertas().addActionListener(o -> mostrarMainOfertas());
        this.ventanaMainCoordinador.getBtnEventos().addActionListener(v -> mostrarMainEventos());
        this.ventanaMainCoordinador.getBtnPerfilDeUsuario().addActionListener(p -> mostrarMainPerfilDeUsuario());
        this.ventanaMainCoordinador.getBtnGastos().addActionListener(g -> mostrarMainGastos());

        this.ventanaMainCoordinador.getBtnDesconectar().addActionListener(d -> desconectar());
        
        this.accesoDatos = acceso;

        controladorTransporte = new ControladorTransporte(ventanaMainTransportes, accesoDatos);
        controladorViajes = new ControladorViajes(ventanaMainViajes, accesoDatos);
        controladorUbicaciones = new ControladorUbicaciones(ventanaMainUbicaciones, accesoDatos);
        controladorOfertas = new ControladorOfertas(ventanaMainOfertas, accesoDatos);
        controladorGastos = new ControladorGastos(ventanaMainGastos, accesoDatos);
        //controladorEventos = new ControladorEventos(ventanaMainEventos, accesoDatos);

    }

	public void inicializar(ControladorEventos controladorEventos, ControladorPerfilDeUsuario controladorPerfilDeUsuario) {
		this.controladorPerfilDeUsuario = controladorPerfilDeUsuario;
		this.controladorEventos = controladorEventos;
		mostrarMainViajes();
        ventanaMainCoordinador.mostrarVentana();
    }

    private void mostrarMainUbicaciones() {
        ventanaMainCoordinador.getPanelCentro().removeAll();
        ventanaMainCoordinador.getPanelCentro().add(ventanaMainUbicaciones.getPanel());
        actualizarVentana();
    	controladorUbicaciones.inicializar();
    }

    private void mostrarMainViajes() {
    	ventanaMainCoordinador.getPanelCentro().removeAll();
        ventanaMainCoordinador.getPanelCentro().add(ventanaMainViajes.getPanel());
        actualizarVentana();
        controladorViajes.inicializar();
    }

    private void mostrarMainTransporte() {
    	ventanaMainCoordinador.getPanelCentro().removeAll();
        ventanaMainCoordinador.getPanelCentro().add(ventanaMainTransportes.getPanel());
        actualizarVentana();
        controladorTransporte.inicializar();
    }
    
    private void mostrarMainOfertas() {
    	ventanaMainCoordinador.getPanelCentro().removeAll();
        ventanaMainCoordinador.getPanelCentro().add(ventanaMainOfertas.getPanel());
        actualizarVentana();
        controladorOfertas.inicializar();
    }
    
    private void mostrarMainEventos() {
    	ventanaMainCoordinador.getPanelCentro().removeAll();
    	ventanaMainCoordinador.getPanelCentro().add(ventanaMainEventos.getPanel());
    	actualizarVentana();
    	controladorEventos.inicializar();
    }
    
    private void mostrarMainGastos() {
    	ventanaMainCoordinador.getPanelCentro().removeAll();
        ventanaMainCoordinador.getPanelCentro().add(ventanaMainGastos.getPanel());
        actualizarVentana();
        controladorGastos.inicializar();
    }
    
    private void mostrarMainPerfilDeUsuario() {
    	ventanaMainCoordinador.getPanelCentro().removeAll();
    	ventanaMainCoordinador.getPanelCentro().add(ventanaMainPerfilDeUsuario.getPanel());
    	actualizarVentana();
    	controladorPerfilDeUsuario.inicializar();
    }
    
    private void actualizarVentana() {
		ventanaMainCoordinador.getPanelCentro().revalidate();
		ventanaMainCoordinador.getPanelCentro().repaint();
	}

    private void desconectar() {
    	ventanaMainCoordinador.cerrarVentana();
    	ControladorLogin.desconectar(ControladorLogin.id_logeado());
	}
    
}
