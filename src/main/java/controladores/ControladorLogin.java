package controladores;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JOptionPane;

import org.apache.commons.codec.digest.DigestUtils;

import dominio.Estado;
import dominio.Mail;
import dominio.Usuario;
import modelo.AccesoDatos;
import vistas.Login;
import vistas.Main_Administrador;
import vistas.Main_Cliente;
import vistas.Main_Coordinador;
import vistas.Main_Dueño;
import vistas.Main_PersonalAdm;
import vistas.Ventana_OlvidarContraseña;
import vistas.eventos.Main_Eventos;
import vistas.perfil.Main_PerfilDeUsuario;

public class ControladorLogin {

	private static Login login;
	private Main_Dueño main_dueño;
	private Main_Cliente main_cliente;
	private Main_Coordinador main_coordinador;
	private Main_PersonalAdm main_personaladm;
	private Main_Administrador main_administrador;
	private Main_Eventos main_eventos;
	private Main_PerfilDeUsuario main_perfilDeUsuario;
	private Ventana_OlvidarContraseña ventanaOlvidarContraseña;
	private AccesoDatos accesoDatos;
	private static ControladorDueño controladorDueño;
	private static ControladorCliente controladorCliente;
	private static ControladorCoordinador controladorCoord;
	private static ControladorPersonal controladorPersonal;
	private static ControladorAdministrador controladorAdministrador;
	private ControladorEventos controladorEventos;
	private ControladorPerfilDeUsuario controladorPerfilDeUsuario;

	private static Usuario user_logged;

	public ControladorLogin(AccesoDatos acceso) {
		login = new Login();

		this.main_coordinador = Main_Coordinador.getInstance();
		this.main_personaladm = Main_PersonalAdm.getInstance();
		this.main_administrador = Main_Administrador.getInstance();
		this.main_dueño = Main_Dueño.getInstance();
		this.main_cliente = Main_Cliente.getInstance();
		this.ventanaOlvidarContraseña = Ventana_OlvidarContraseña.getInstance();

		this.main_eventos = Main_Eventos.getInstance();
		this.main_perfilDeUsuario = Main_PerfilDeUsuario.getInstance();
		login.getBtnConfirmar().addActionListener(o -> confirmarUser());
		login.getTxtUser().addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				if (arg0.getKeyCode() == KeyEvent.VK_ENTER) {
					confirmarUser();
				}
			}
		});
		login.getTxtPass().addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				if (arg0.getKeyCode() == KeyEvent.VK_ENTER) {
					confirmarUser();
				}
			}
		});
		login.getOlvidadoContraseña().addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				ventanaOlvidarContraseña.mostrarVentana();
			}
		});
		ventanaOlvidarContraseña.getBtnConfirmar().addActionListener(e -> enviarNuevaContraseña());

		this.accesoDatos = acceso;

		controladorEventos = new ControladorEventos(main_eventos, accesoDatos);
		controladorPerfilDeUsuario = new ControladorPerfilDeUsuario(main_perfilDeUsuario, accesoDatos);

		controladorCoord = new ControladorCoordinador(main_coordinador, accesoDatos);
		controladorPersonal = new ControladorPersonal(main_personaladm, accesoDatos);
		controladorAdministrador = new ControladorAdministrador(main_administrador, accesoDatos);
		controladorDueño = new ControladorDueño(main_dueño, accesoDatos);
		controladorCliente = new ControladorCliente(main_cliente);

	}

	private void enviarNuevaContraseña() {
		String usuarioBuscado = ventanaOlvidarContraseña.getTxtUsuario().getText();
		if (!usuarioBuscado.equals("")) {
			if (accesoDatos.existeNombreUsuario(usuarioBuscado)) {

				Usuario usuario = accesoDatos.findByNombreUSuario(usuarioBuscado);
				usuario.generarContraseña();

				if (accesoDatos.editarUsuario(usuario)) {
					redactarMailCambioContraseña(usuario);
					String sha256hex = DigestUtils.sha256Hex(usuario.getContraseña());
					usuario.setContraseña(sha256hex);
					ventanaOlvidarContraseña.cerrar();
					JOptionPane.showMessageDialog(null,
							"La nueva contraseña ha sido enviada exitosamente a su casilla de correo.", "¡Aviso!",
							JOptionPane.INFORMATION_MESSAGE);
				} else {
					JOptionPane.showMessageDialog(null,
							"No se ha podido recuperar la contraseña, intente nuevamente más tarde.", "¡Advertencia!",
							JOptionPane.WARNING_MESSAGE);
				}
			} else {
				JOptionPane.showMessageDialog(null, "El nombre de usuario no existe.", "¡Error!",
						JOptionPane.ERROR_MESSAGE);
			}
		} else {
			JOptionPane.showMessageDialog(null, "Se debe completar el usuario", "¡Advertencia!",
					JOptionPane.WARNING_MESSAGE);
		}

	}

	private void redactarMailCambioContraseña(Usuario usuario) {
		Mail mail = Mail.getInstance(accesoDatos);
		String asunto = "Cambio de contraseña de la cuenta de Hermes";
		String cuerpoMail = "La contraseña de la cuenta" + usuario.getUsuario() + " acaba de cambiar: \n\n"
				+ usuario.getContraseña()
				+ "\n\n Con esta nueva contraseña ya puede ingresar nuevamente al sistema y si lo desea, puede modificarla desde su perfil."
				+ "\n\n Si no has sido tú la seguridad de tu cuenta corre peligro, ponte en contacto con el equipo de seguridad informática.";
		mail.enviarConGMail(usuario.getEmail(), asunto, cuerpoMail);
	}

	public void inicializar() {
		login.mostrarVentana();
	}

	private void confirmarUser() {
		if (ingresoUsuarioContraseña()) {
			String nombreUsuario = login.getTxtUser().getText();
			if (accesoDatos.existeNombreUsuario(nombreUsuario)) {
				Usuario usuario = accesoDatos.findByNombreUSuario(nombreUsuario);
				if (!usuario.getEstado().equals(Estado.INHABILITADO.toString())) {
					String contraseña = login.getTxtPass().getText();

					String sha256hex = DigestUtils.sha256Hex(contraseña);

					if (sha256hex.equals(usuario.getContraseña())) {
						user_logged = usuario;
						mostrarVentanaUsuario(usuario);

					} else {
						JOptionPane.showMessageDialog(null, "La contraseña no es válida", "¡Error!",
								JOptionPane.ERROR_MESSAGE);
					}
				} else {
					JOptionPane.showMessageDialog(null, "El usuario no existe", "¡Advertencia!",
							JOptionPane.WARNING_MESSAGE);
				}
			} else {
				JOptionPane.showMessageDialog(null, "El usuario no existe", "¡Advertencia!",
						JOptionPane.WARNING_MESSAGE);

			}
		}
	}

	private void mostrarVentanaUsuario(Usuario usuario) {
		String rol = usuario.getRol();

		switch (rol) {
		case "ADMINISTRADOR":
			mostrarMainAdministrador();
			break;
		case "CLIENTE":
			mostrarMainCliente();
			break;
		case "PERSONAL_ADMINISTRATIVO":
			mostrarMainPersonalAdm();
			break;
		case "COORDINADOR":
			mostrarMainCoordinador();
			break;
		case "DUEÑO":
			mostrarMainDueño();
			break;
		}
	}

	public static void desconectar(Usuario usuario) {
		String rol = usuario.getRol();

		switch (rol) {
		case "ADMINISTRADOR":
			cerrarMainAdministrador();
			break;
		case "CLIENTE":
			cerrarMainCliente();
			break;
		case "PERSONAL_ADMINISTRATIVO":
			cerrarMainPersonalAdm();
			break;
		case "COORDINADOR":
			cerrarMainCoordinador();
			break;
		case "DUEÑO":
			cerrarMainDueño();
			break;
		}
	}

	private boolean ingresoUsuarioContraseña() {
		if (login.getTxtUser().getText().isEmpty() || login.getTxtPass().getText().isEmpty()) {
			JOptionPane.showMessageDialog(null, "Los campos usuario y contraseña no pueden estar vacios.",
					"¡Advertencia!", JOptionPane.WARNING_MESSAGE);
			return false;
		}
		return true;
	}

	private void mostrarMainCoordinador() {
		controladorCoord.inicializar(controladorEventos, controladorPerfilDeUsuario);
		login.cerrarVentana();
	}

	private void mostrarMainPersonalAdm() {
		controladorPersonal.inicializar(controladorEventos, controladorPerfilDeUsuario);
		login.cerrarVentana();
	}

	private void mostrarMainAdministrador() {
		controladorAdministrador.inicializar(controladorPerfilDeUsuario);
		login.cerrarVentana();
	}

	private void mostrarMainDueño() {
		controladorDueño.inicializar(controladorPerfilDeUsuario);
		login.cerrarVentana();
	}

	private void mostrarMainCliente() {
		controladorCliente.inicializar(controladorPerfilDeUsuario);
		login.cerrarVentana();
	}

	private static void cerrarMainCoordinador() {
		login.mostrarVentana();
	}

	private static void cerrarMainPersonalAdm() {
		login.mostrarVentana();
	}

	private static void cerrarMainAdministrador() {
		login.mostrarVentana();
	}

	private static void cerrarMainDueño() {
		login.mostrarVentana();
	}

	private static void cerrarMainCliente() {
		login.mostrarVentana();
	}

	public static Usuario id_logeado() {
		return user_logged;
	}
}
