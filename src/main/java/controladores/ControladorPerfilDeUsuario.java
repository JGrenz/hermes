package controladores;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.JOptionPane;

import org.apache.commons.codec.digest.DigestUtils;

import dominio.Contacto;
import dominio.Operacion;
import dominio.Pasaje;
import dominio.Puntaje;
import dominio.Usuario;
import dominio.Viaje;
import modelo.AccesoDatos;
import vistas.perfil.Main_PerfilDeUsuario;
import vistas.perfil.Ventana_ConfiguracionContraseñaUsuario;
import vistas.perfil.Ventana_EditarUsuario;

public class ControladorPerfilDeUsuario {

	private Main_PerfilDeUsuario ventanaMainPerfilDeUsuario;
	private Ventana_EditarUsuario ventanaEditarPerfilDeUsuario;
	private Ventana_ConfiguracionContraseñaUsuario ventanaConfigurarContraseñaUsuario;
	private AccesoDatos accesoDatos;
	private Usuario usuario;
	private List<Pasaje> pasajes_en_tabla;

	public ControladorPerfilDeUsuario(Main_PerfilDeUsuario VentanaMainPerfilDeUsuario, AccesoDatos acceso) {
		this.ventanaMainPerfilDeUsuario = VentanaMainPerfilDeUsuario;
		this.ventanaEditarPerfilDeUsuario = Ventana_EditarUsuario.getInstance();
		this.ventanaConfigurarContraseñaUsuario = Ventana_ConfiguracionContraseñaUsuario.getInstance();

		this.accesoDatos = acceso;

		this.ventanaMainPerfilDeUsuario.getBtnEditar().addActionListener(e -> mostrarVentanaEditarUsuario());
		this.ventanaEditarPerfilDeUsuario.getBtnConfirmar().addActionListener(e -> editarUsuario());
		this.ventanaEditarPerfilDeUsuario.getBtnContraseña().addActionListener(e -> mostrarVentanaConfigurarContraseña());
		this.ventanaConfigurarContraseñaUsuario.getBtnConfirmar().addActionListener(e -> configurarContraseña());

		this.pasajes_en_tabla = new ArrayList<>();
	}

	public void inicializar() {
		this.usuario = ControladorLogin.id_logeado();
		cargarComponentesCliente();
		cargarDatosVentanaMain();
	}

	public void mostrarVentanaEditarUsuario() {
		cargarDatosVentanaEditar();
		this.ventanaEditarPerfilDeUsuario.mostrarVentana();
	}
	
	public void mostrarVentanaConfigurarContraseña() {
		this.ventanaConfigurarContraseñaUsuario.mostrarVentana();
	}

	private void cargarDatosVentanaMain() {
		LocalDate date = this.usuario.getNacimiento();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		String fechaNacimiento = date.format(formatter);
		String puntos = cargarPuntos();

		ventanaMainPerfilDeUsuario.getTxtNombre().setText(this.usuario.getNombre());
		ventanaMainPerfilDeUsuario.getTxtApellido().setText(this.usuario.getApellido());
		ventanaMainPerfilDeUsuario.getTxtDni().setText(this.usuario.getDni());
		ventanaMainPerfilDeUsuario.getTxtEmail().setText(this.usuario.getEmail());
		ventanaMainPerfilDeUsuario.getTxtUsuario().setText(this.usuario.getUsuario());
		ventanaMainPerfilDeUsuario.getNacimiento().setText(fechaNacimiento);
		ventanaMainPerfilDeUsuario.getTxtPuntos().setText(puntos);
		ventanaMainPerfilDeUsuario.getTelefono1().setText(this.usuario.getContacto().getTelefono1());
		ventanaMainPerfilDeUsuario.getTelefono2().setText(this.usuario.getContacto().getTelefono2());
		ventanaMainPerfilDeUsuario.getTelefono3().setText(this.usuario.getContacto().getTelefono3());
		ventanaMainPerfilDeUsuario.getTelefono4().setText(this.usuario.getContacto().getTelefono4());
		ventanaMainPerfilDeUsuario.getTelefono5().setText(this.usuario.getContacto().getTelefono5());
	}

	private String cargarPuntos() {
		int puntaje = 0;
		List<Puntaje> listaDePuntos = this.accesoDatos.obtenerPuntajeDeCliente(usuario);
		for (Puntaje punto : listaDePuntos) {
			puntaje = puntaje + punto.getCantidad();
		}
		return ""+puntaje;
	}

	private void cargarComponentesCliente() {
		if (usuario.getRol().equals("CLIENTE")) {
			limpiarTabla();
			ventanaMainPerfilDeUsuario.getLblHistorial().setVisible(true);
			ventanaMainPerfilDeUsuario.getPanelPasajes().setVisible(true);
			cargarPasajesDelCliente();
			ventanaMainPerfilDeUsuario.getLblPuntos().setVisible(true);
			ventanaMainPerfilDeUsuario.getTxtPuntos().setVisible(true);
		}
	}

	private void cargarDatosVentanaEditar() {
		java.util.Date date = java.sql.Date.valueOf(this.usuario.getNacimiento());

		ventanaEditarPerfilDeUsuario.getTxtNombre().setText(this.usuario.getNombre());
		ventanaEditarPerfilDeUsuario.getTxtApellido().setText(this.usuario.getApellido());
		ventanaEditarPerfilDeUsuario.getTxtDni().setText(this.usuario.getDni());
		ventanaEditarPerfilDeUsuario.getTxtEmail().setText(this.usuario.getEmail());
		ventanaEditarPerfilDeUsuario.getTxtUsuario().setText(this.usuario.getUsuario());
		ventanaEditarPerfilDeUsuario.getNacimientoChooser().setDate(date);
		ventanaEditarPerfilDeUsuario.getTelefono1().setText(this.usuario.getContacto().getTelefono1());
		ventanaEditarPerfilDeUsuario.getTelefono2().setText(this.usuario.getContacto().getTelefono2());
		ventanaEditarPerfilDeUsuario.getTelefono3().setText(this.usuario.getContacto().getTelefono3());
		ventanaEditarPerfilDeUsuario.getTelefono4().setText(this.usuario.getContacto().getTelefono4());
		ventanaEditarPerfilDeUsuario.getTelefono5().setText(this.usuario.getContacto().getTelefono5());
	}

	private void editarUsuario() {
		if (this.ventanaEditarPerfilDeUsuario.verificarCampo()) {
			Usuario usuario = editarUsuarioConDatosDeVentana(); // Se setea el cliente antes de hacer el update en la
																// base de datos (CORREGIR)
			if (this.accesoDatos.editarUsuario(usuario)) {
				cargarDatosVentanaMain();
				this.ventanaEditarPerfilDeUsuario.cerrar();
			} else {
				JOptionPane.showMessageDialog(null, "No se pudo editar el cliente", "¡Error!", JOptionPane.ERROR_MESSAGE);
			}
		}
	}

	private Usuario editarUsuarioConDatosDeVentana() {
		Usuario usuario = ControladorLogin.id_logeado();

		usuario.setNombre(this.ventanaEditarPerfilDeUsuario.getTxtNombre().getText());
		usuario.setApellido(this.ventanaEditarPerfilDeUsuario.getTxtApellido().getText());
		usuario.setDni(this.ventanaEditarPerfilDeUsuario.getTxtDni().getText());
		usuario.setEmail(this.ventanaEditarPerfilDeUsuario.getTxtEmail().getText());
		usuario.setUsuario(this.ventanaEditarPerfilDeUsuario.getTxtUsuario().getText());
		usuario.setNacimiento(this.ventanaEditarPerfilDeUsuario.getTxtNacimiento());
		usuario.setContacto(editarContactoConDatosDeVentana(usuario));

		return usuario;
	}

	private Contacto editarContactoConDatosDeVentana(Usuario usuario) {
		Contacto contacto = usuario.getContacto();
		contacto.setTelefono1(this.ventanaEditarPerfilDeUsuario.getTelefono1().getText());
		contacto.setTelefono2(this.ventanaEditarPerfilDeUsuario.getTelefono2().getText());
		contacto.setTelefono3(this.ventanaEditarPerfilDeUsuario.getTelefono3().getText());
		contacto.setTelefono4(this.ventanaEditarPerfilDeUsuario.getTelefono4().getText());
		contacto.setTelefono5(this.ventanaEditarPerfilDeUsuario.getTelefono5().getText());

		return contacto;
	}

	private void limpiarTabla() {
		pasajes_en_tabla.clear();
		this.ventanaMainPerfilDeUsuario.getModelPasaje().setRowCount(0);
		this.ventanaMainPerfilDeUsuario.getModelPasaje().setColumnCount(0);
		this.ventanaMainPerfilDeUsuario.getModelPasaje().setColumnIdentifiers(this.ventanaMainPerfilDeUsuario.getNombreColumnasPasajes());
	}
	
	private void cargarPasajesDelCliente() {
		if (this.pasajes_en_tabla.size() == 0) {
			this.pasajes_en_tabla = accesoDatos.findPasajeByCliente(usuario);
			pasajes_en_tabla.forEach(pasaje -> agregarATabla(pasaje));
		}
	}

	private void agregarATabla(Pasaje pasaje) {
		BigDecimal precio = pasaje.getViaje().getPrecio();
		precio = precio.add(pasaje.getViaje().getImpuesto());

		String fechaYHoraLlegada = calculadorfechaViaje(pasaje.getViaje());

		Object[] fila = { pasaje, pasaje.getViaje().getUbicacionOrigen(), pasaje.getViaje().getUbicacionDestino(),
				pasaje.getViaje().getFechaSalida() + " " + pasaje.getViaje().getHoraSalida(), fechaYHoraLlegada,
				pasaje.getViaje().getMedioTransporte(), precio.toString(), "$" + montoTotalPagado(pasaje),
				pasaje.getEstadoPasaje() };

		this.ventanaMainPerfilDeUsuario.getModelPasaje().addRow(fila);
	}

	public BigDecimal montoTotalPagado(Pasaje pasaje) {
		List<Operacion> operaciones = accesoDatos.findOperacionesByPasaje(pasaje);
		BigDecimal montoRetorno = new BigDecimal(0);

		for (Operacion operacion : operaciones) {
			montoRetorno = montoRetorno.add(operacion.getMonto());
		}

		montoRetorno = montoRetorno.setScale(2, RoundingMode.HALF_UP);
		return montoRetorno;
	}

	private String calculadorfechaViaje(Viaje viaje) {
		LocalTime hora = viaje.getHoraSalida();
		LocalDate fecha = viaje.getFechaSalida();

		Duration duracionViaje = viaje.getHorasDeViaje();

		LocalDateTime fechasalida = fecha.atTime(hora);

		fechasalida = fechasalida.plus(duracionViaje.getSeconds(), ChronoUnit.SECONDS);

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

		String formatDateTime = fechasalida.format(formatter);
		return formatDateTime;
	}
	
	private void configurarContraseña() {
		Usuario usuario = ControladorLogin.id_logeado();
		char[] contraseñaActual = this.ventanaConfigurarContraseñaUsuario.getTxtContraseñaActual().getPassword();
		String contraseña = String.valueOf(contraseñaActual);
		String sha256hex = DigestUtils.sha256Hex(contraseña);
		if(sha256hex.equals(usuario.getContraseña())) {
			char[] contraseñaNueva = ventanaConfigurarContraseñaUsuario.getTxtNuevaContraseña().getPassword();
			char[] confirmarContraseña = ventanaConfigurarContraseñaUsuario.getTxtConfirmarContraseña().getPassword();
			if(ventanaConfigurarContraseñaUsuario.verificarCampo()){
				if(Arrays.equals(contraseñaNueva, confirmarContraseña)) {
					usuario.setContraseña(String.valueOf(contraseñaNueva));
					if(accesoDatos.editarUsuario(usuario)) {
						JOptionPane.showMessageDialog(null, "La contraseña fue actualizada satisfactoriamente", "¡Aviso!", JOptionPane.INFORMATION_MESSAGE);
						limpiarCamposContraseña();
						ventanaConfigurarContraseñaUsuario.cerrar();
					}  else {
						JOptionPane.showMessageDialog(null, "No se pudo actualizar la contraseña del usuario.", "¡Error!", JOptionPane.ERROR_MESSAGE);
					}
				} else {
					JOptionPane.showMessageDialog(null, "Las contraseñas NUEVA y CONFIRMADA no coinciden.", "¡Error!", JOptionPane.ERROR_MESSAGE);
				}
			}
		}else {
			JOptionPane.showMessageDialog(null, "La contraseña actual no es la correcta.", "¡Error!", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	private void limpiarCamposContraseña() {
		ventanaConfigurarContraseñaUsuario.getTxtContraseñaActual().setText("");
		ventanaConfigurarContraseñaUsuario.getTxtConfirmarContraseña().setText("");
		ventanaConfigurarContraseñaUsuario.getTxtNuevaContraseña().setText("");
	}
}