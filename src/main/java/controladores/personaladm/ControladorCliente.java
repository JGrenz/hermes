package controladores.personaladm;

import java.awt.event.ActionEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.swing.*;

import org.apache.commons.codec.digest.DigestUtils;

import daoImplementacion.UsuarioDAOSQL;
import dominio.*;
import modelo.AccesoDatos;
import vistas.personaladm.clientes.Main_Clientes;
import vistas.personaladm.clientes.Ventana_AgregarCliente;
import vistas.personaladm.clientes.Ventana_EditarCliente;

public class 	ControladorCliente {

	private Main_Clientes ventanaMainClientes;
	private Ventana_AgregarCliente ventanaAgregarCliente;
	private Ventana_EditarCliente ventanaEditarCliente;
	private AccesoDatos accesoDatos;
	private List<Usuario> clientes_en_tabla;
	private Mail mail;
	private DateTimeFormatter formatoDeFecha = DateTimeFormatter.ofPattern("dd/MM/yyyy");
	private DateTimeFormatter formatoDeFechaHora = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");

	public ControladorCliente(Main_Clientes ventanaMainClientes, AccesoDatos accesoDatos) {
		this.ventanaMainClientes = ventanaMainClientes;
		this.ventanaAgregarCliente = Ventana_AgregarCliente.getInstance();
		this.ventanaEditarCliente = Ventana_EditarCliente.getInstance();

		this.ventanaMainClientes.getBtnAgregar().addActionListener(a -> ventanaAgregarCliente(a));
		this.ventanaMainClientes.getBtnEditar().addActionListener(e -> ventanaEditarCliente(e));
		this.ventanaMainClientes.getBtnBorrar().addActionListener(e -> HabilitarInhabilitar());
		this.ventanaMainClientes.getBtnBuscarCliente().addActionListener(e -> realizarBusqueda(false));
		this.ventanaMainClientes.getTablaClientes().addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				mostrarPasajesDeClienteSeleccionado();
				mostrarTelefonos();
			}
		});

		this.ventanaMainClientes.getRdbtnHabilitados().addActionListener(e -> realizarBusqueda(false));
		this.ventanaMainClientes.getRdbtnInhabilitados().addActionListener(e -> realizarBusqueda(false));
		this.ventanaMainClientes.getRdbtnTodos().addActionListener(e -> realizarBusqueda(false));

		this.ventanaAgregarCliente.getBtnConfirmar().addActionListener(c -> guardarCliente(c));
		this.ventanaAgregarCliente.getTxtNombre().addFocusListener(metodoUsuario());
		this.ventanaAgregarCliente.getTxtApellido().addFocusListener(metodoUsuario());

		this.ventanaEditarCliente.getBtnConfirmar().addActionListener(e -> editarCliente(e));

		this.accesoDatos = accesoDatos;
		this.mail = Mail.getInstance(accesoDatos);
		this.clientes_en_tabla = new ArrayList<Usuario>();
	}

	public void inicializar() {
		this.clientes_en_tabla = accesoDatos.obtenerClientes();
//		cargarArreglo();
		limpiarTelefonos();
		realizarBusqueda(true);
	}

//	private void cargarArreglo() {
//		if (this.clientes_en_tabla.size() == 0) {
//			this.clientes_en_tabla = accesoDatos.obtenerClientes();
//		}
//	}

	private void realizarBusqueda(boolean primeraEntrada) {
		Predicate<Usuario> filtro = filtrosSeleccionados();
		limpiarTablaClientes();
		limpiarTelefonos();
		if (filtro != null) {
			List<Usuario> lista_filtrada = this.clientes_en_tabla.stream().filter(filtro).collect(Collectors.toList());

			if (lista_filtrada.size() > 0) {
				lista_filtrada.forEach(cliente -> agregarATabla(cliente));
			} else {
				if (!primeraEntrada) {
					JOptionPane.showMessageDialog(null, "No se encontraron clientes.", "¡Advertencia!", JOptionPane.WARNING_MESSAGE);
				}
			}
		} else {
			clientes_en_tabla.forEach(cliente -> agregarATabla(cliente));
		}
	}

	private Predicate<Usuario> filtrosSeleccionados() {
		List<Predicate<Usuario>> lista_filtros = new ArrayList<>();

		String estado = radioButtonSeleccionado();
		if (!estado.equals("TODOS")) {
			Predicate<Usuario> filtroEstado = cliente -> cliente.getEstado().equals(estado);
			lista_filtros.add(filtroEstado);
		}

		String nombre = this.ventanaMainClientes.getTxtNombre().getText().toLowerCase();
		if (!nombre.isEmpty()) {
			Predicate<Usuario> filtroNombre = cliente -> cliente.getNombre().toLowerCase().indexOf(nombre) > -1;
			lista_filtros.add(filtroNombre);
		}

		String apellido = this.ventanaMainClientes.getTxtApellido().getText().toLowerCase();
		if (!apellido.isEmpty()) {
			Predicate<Usuario> filtroApellido = cliente -> cliente.getApellido().toLowerCase().indexOf(apellido) > -1;
			lista_filtros.add(filtroApellido);
		}

		String dni = this.ventanaMainClientes.getTxtDNI().getText();
		if (!dni.isEmpty()) {
			Predicate<Usuario> filtroDni = cliente -> cliente.getDni().indexOf(dni) > -1;
			lista_filtros.add(filtroDni);
		}

		String usuario = this.ventanaMainClientes.getTxtUsuario().getText();
		if (!usuario.isEmpty()) {
			Predicate<Usuario> filtroUsuario = cliente -> cliente.getUsuario().indexOf(usuario) > -1;
			lista_filtros.add(filtroUsuario);
		}

		if (lista_filtros.size() == 1) {
			return lista_filtros.get(0);
		} else if (lista_filtros.size() > 1) {
			Predicate<Usuario> acumulador = lista_filtros.get(0);
			lista_filtros.remove(0);
			for (Predicate<Usuario> predicado : lista_filtros) {
				acumulador = acumulador.and(predicado);
			}
			return acumulador;
		}
		return null;
	}

	public String radioButtonSeleccionado() {
		if (ventanaMainClientes.getRdbtnHabilitados().isSelected()) {
			return "HABILITADO";
		}
		if (ventanaMainClientes.getRdbtnInhabilitados().isSelected()) {
			return "INHABILITADO";
		}
		return "TODOS";
	}

	private void ventanaAgregarCliente(ActionEvent a) {
		ventanaAgregarCliente.mostrarVentana();
	}

	private void HabilitarInhabilitar() {
		try {
			Usuario cliente = clienteSeleccionadoEnTabla();
			if (cliente.getEstado().equals("HABILITADO")) {
				cambiarEstado(cliente, "INHABILITADO", "INHABILITAR");
			} else {
				cambiarEstado(cliente, "HABILITADO", "HABILITAR");
			}
		} catch (ArrayIndexOutOfBoundsException ex) {
			JOptionPane.showMessageDialog(null, "Seleccione un cliente.", "¡Aviso!", JOptionPane.INFORMATION_MESSAGE);
		}
	}

	private void cambiarEstado(Usuario cliente, String estado, String accion) {
		int confirm = JOptionPane.showOptionDialog(null, "¿Está seguro que quiere " + accion + " este cliente?",
				"Confirmacion", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, null);
		if (confirm == 0) {
			if (this.accesoDatos.tienePasajesActivos(cliente)) {
				JOptionPane.showMessageDialog(null,
						"No se pudo " + accion + " el cliente porque tiene pasajes activos.", "¡Advertencia!", JOptionPane.WARNING_MESSAGE);
			} else {
				if (this.accesoDatos.cambiarEstadoUsuario(cliente, estado)) {
					cliente.setEstado(estado);
					realizarBusqueda(true);
				} else {
					JOptionPane.showMessageDialog(null, "No se pudo " + accion + " el cliente", "¡Error!", JOptionPane.ERROR_MESSAGE);
				}
			}
		}
	}

	private void ventanaEditarCliente(ActionEvent e) {
		try {
			Usuario cliente = clienteSeleccionadoEnTabla();

			if (cliente.getEstado().equals("HABILITADO")) {
				this.llenarCampos(cliente);
				this.ventanaEditarCliente.mostrarVentana();
			} else {
				JOptionPane.showMessageDialog(null, "Seleccione un cliente con estado HABILITADO.", "¡Advertencia!", JOptionPane.WARNING_MESSAGE);
			}
		} catch (ArrayIndexOutOfBoundsException ex) {
			JOptionPane.showMessageDialog(null, "Seleccione un cliente para editar.", "¡Advertencia!", JOptionPane.WARNING_MESSAGE);
		}
	}

	private Usuario clienteSeleccionadoEnTabla() throws ArrayIndexOutOfBoundsException {
		int fila = ventanaMainClientes.getTablaClientes().getSelectedRow();

		int columna = ventanaMainClientes.getTablaClientes().getColumnModel().getColumnIndex("Usuario");

		return (Usuario) ventanaMainClientes.getTablaClientes().getValueAt(fila, columna);
	}

	private Usuario crearCliente() {
		Usuario cliente = new Usuario(ventanaAgregarCliente.getTxtDni().getText(),
				ventanaAgregarCliente.getTxtNombre().getText(), ventanaAgregarCliente.getTxtApellido().getText(),
				ventanaAgregarCliente.getTxtUsuario().getText(), ventanaAgregarCliente.getTxtNacimiento(),
				ventanaAgregarCliente.getTxtEmail().getText(), UsuarioRol.CLIENTE.name());
		cliente.setUsuario(ventanaAgregarCliente.getTxtUsuario().getText());
		cliente.setContacto(crearContacto());
		return cliente;
	}

	private void guardarCliente(ActionEvent c) {
		if (this.ventanaAgregarCliente.verificarCampo()
				&& !existeUsuario(ventanaAgregarCliente.getTxtUsuario().getText())) {
			Usuario cliente = crearCliente();
			if (this.accesoDatos.agregarUsuario(cliente)) {
				enviarNombreUsuario(cliente);
				enviarContraseña(cliente);

				String sha256hex = DigestUtils.sha256Hex(cliente.getContraseña());
				cliente.setContraseña(sha256hex);
				clientes_en_tabla.add(cliente);
				realizarBusqueda(true);
				this.ventanaAgregarCliente.cerrar();
			} else {
				JOptionPane.showMessageDialog(null, "No se pudo agregar el cliente", "¡Advertencia!", JOptionPane.WARNING_MESSAGE);
			}
		}
	}

	private void enviarNombreUsuario(Usuario cliente) {
		String asunto = "¡Nuevo usuario en Hermes!";
		String cuerpo = "Bienvenide " + cliente.getApellido() + " " + cliente.getNombre()
				+ " al sistema Hermes, !Muchas gracias por incorporarse!.\n\n"
				+ "Para poder ingresar al sistema deberas hacerlo con el siguiente nombre de usuario: \n\n"
				+ cliente.getUsuario()
				+ "\n\nEn breve recibirás un mail con tu contraseña. \nEl equpo de cuentas de Hermes.";
		this.mail.enviarConGMail(cliente.getEmail(), asunto, cuerpo);
	}

	private void enviarContraseña(Usuario cliente) {
		String asunto = "Contraseña para ingresar en Hermes";
		String cuerpo = "Estimade " + cliente.getApellido() + " " + cliente.getNombre()
				+ ": Fuiste registrado exitosamente en el sistema Hermes. Para poder ingresar "
				+ "con tu nombre de usuario tendrás que hacerlo con la siguiente contraseña: \n\n "
				+ cliente.getContraseña()
				+ "\n\nNo olvide que al ingresar puede cambiarla. Gracias,\nEl equipo de cuentas de Hermes.";
		this.mail.enviarConGMail(cliente.getEmail(), asunto, cuerpo);
	}

	private boolean existeUsuario(String usuario) {
		if (this.accesoDatos.existeNombreUsuario(usuario)) {
			JOptionPane.showMessageDialog(null, "El nombre de usuario está en uso", "¡Advertencia!", JOptionPane.WARNING_MESSAGE);
			return true;
		}
		return false;
	}

	private Usuario editarClienteConDatosDeVentana() {
		Usuario cliente = clienteSeleccionadoEnTabla();

		cliente.setNombre(this.ventanaEditarCliente.getTxtNombre().getText());
		cliente.setApellido(this.ventanaEditarCliente.getTxtApellido().getText());
		cliente.setDni(this.ventanaEditarCliente.getTxtDni().getText());
		cliente.setEmail(this.ventanaEditarCliente.getTxtEmail().getText());
		cliente.setUsuario(this.ventanaEditarCliente.getTxtUsuario().getText());
		cliente.setNacimiento(this.ventanaEditarCliente.getTxtNacimiento());
		cliente.setContacto(editarContactoConDatosDeVentana(cliente));

		return cliente;
	}

	private void editarCliente(ActionEvent e) {
		if (this.ventanaEditarCliente.verificarCampo()) {
			Usuario usuario = editarClienteConDatosDeVentana(); // Se setea el cliente antes de hacer el update en la
																// base de datos (CORREGIR)
			if (this.accesoDatos.editarUsuario(usuario)) {
				this.ventanaEditarCliente.cerrar();
				realizarBusqueda(true);
			} else {
				JOptionPane.showMessageDialog(null, "No se pudo editar el cliente", "¡Error!", JOptionPane.ERROR_MESSAGE);
			}
		}
	}

	private Contacto crearContacto() {
		Contacto contacto = new Contacto(ventanaAgregarCliente.getTelefono1().getText(),
				ventanaAgregarCliente.getTelefono2().getText(), ventanaAgregarCliente.getTelefono3().getText(),
				ventanaAgregarCliente.getTelefono4().getText(), ventanaAgregarCliente.getTelefono5().getText());
		return contacto;
	}

	private Contacto editarContactoConDatosDeVentana(Usuario cliente) {
		Contacto contacto = cliente.getContacto();
		contacto.setTelefono1(this.ventanaEditarCliente.getTelefono1().getText());
		contacto.setTelefono2(this.ventanaEditarCliente.getTelefono2().getText());
		contacto.setTelefono3(this.ventanaEditarCliente.getTelefono3().getText());
		contacto.setTelefono4(this.ventanaEditarCliente.getTelefono4().getText());
		contacto.setTelefono5(this.ventanaEditarCliente.getTelefono5().getText());

		return contacto;
	}

	private void llenarCampos(Usuario cliente) {
		java.util.Date date = java.sql.Date.valueOf(cliente.getNacimiento());

		this.ventanaEditarCliente.getTxtNombre().setText(cliente.getNombre());
		this.ventanaEditarCliente.getTxtApellido().setText(cliente.getApellido());
		this.ventanaEditarCliente.getTxtDni().setText(cliente.getDni());
		this.ventanaEditarCliente.getTxtEmail().setText(cliente.getEmail());
		this.ventanaEditarCliente.getTxtUsuario().setText(cliente.getUsuario());
		this.ventanaEditarCliente.getNacimientoChooser().setDate(date);
		this.ventanaEditarCliente.getTelefono1().setText(cliente.getContacto().getTelefono1());
		this.ventanaEditarCliente.getTelefono2().setText(cliente.getContacto().getTelefono2());
		this.ventanaEditarCliente.getTelefono3().setText(cliente.getContacto().getTelefono3());
		this.ventanaEditarCliente.getTelefono4().setText(cliente.getContacto().getTelefono4());
		this.ventanaEditarCliente.getTelefono5().setText(cliente.getContacto().getTelefono5());
	}

	private void limpiarTablaClientes() {
		this.ventanaMainClientes.getModelCliente().setRowCount(0);
		this.ventanaMainClientes.getModelCliente().setColumnCount(0);
		this.ventanaMainClientes.getModelCliente()
				.setColumnIdentifiers(this.ventanaMainClientes.getNombreColumnasClientes());
	}

	private void agregarATabla(Usuario cliente) {
		LocalDate fechaDeNacimiento = cliente.getNacimiento();
		String fechaDeNacimientoConFormato = fechaDeNacimiento.format(formatoDeFecha);
		Object[] fila = { cliente.getNombre(), cliente.getApellido(), cliente.getDni(), fechaDeNacimientoConFormato,
				cliente, cliente.getEmail(), cliente.getContacto().getTelefono1(), cliente.getEstado() };
		this.ventanaMainClientes.getModelCliente().addRow(fila);
	}

	private void mostrarTelefonos() {
		Usuario cliente = clienteSeleccionadoEnTabla();
		this.ventanaMainClientes.getTelefono1().setText(cliente.getContacto().getTelefono1());
		this.ventanaMainClientes.getTelefono2().setText(cliente.getContacto().getTelefono2());
		this.ventanaMainClientes.getTelefono3().setText(cliente.getContacto().getTelefono3());
		this.ventanaMainClientes.getTelefono4().setText(cliente.getContacto().getTelefono4());
		this.ventanaMainClientes.getTelefono5().setText(cliente.getContacto().getTelefono5());
	}

	private void mostrarPasajesDeClienteSeleccionado() {
		limpiarTablaPasajes();

		ArrayList<Pasaje> pasajesAsociadosconCliente = agregarPasajesAsociados();

		pasajesAsociadosconCliente.forEach(pasaje -> agregarFilaTablaPasajes(pasaje));
	}

	private ArrayList<Pasaje> agregarPasajesAsociados() {
		List<Pasaje> pasajes = accesoDatos.findAllPasajesDisponibles();
		Usuario clienteSeleccionado = clienteSeleccionadoEnTabla();
		ArrayList<Pasaje> pasajesAsociados = new ArrayList<>();

		for (Pasaje pasaje : pasajes) {
			if (clienteSeleccionado.equals(pasaje.getCliente())) {
				pasajesAsociados.add(pasaje);
			}
		}
		return pasajesAsociados;
	}

	private void limpiarTablaPasajes() {
		this.ventanaMainClientes.getModelPasaje().setRowCount(0);
		this.ventanaMainClientes.getModelPasaje().setColumnCount(0);
		this.ventanaMainClientes.getModelPasaje()
				.setColumnIdentifiers(this.ventanaMainClientes.getNombreColumnasPasajes());
	}

	private void agregarFilaTablaPasajes(Pasaje pasaje) {
		BigDecimal montoAbonado = montoTotal(pasaje);
		LocalDateTime fechaDeCreacion = pasaje.getFecha_creacion();
		String fechaDeCreacionConFormato = fechaDeCreacion.format(formatoDeFechaHora);
		LocalDate fechaDeSalida = pasaje.getViaje().getFechaSalida();
		String fechaDeSalidaConFormato = fechaDeSalida.format(formatoDeFecha);
		LocalDateTime fechaSalidaLDT = crearLocalDateTimeLlegada(pasaje);
		LocalDateTime fechaLlegada = sumarHorasAFecha(fechaSalidaLDT, pasaje.getViaje().getHorasDeViaje());
		String fechaLlegadaConFormato = fechaLlegada.format(formatoDeFechaHora);

		Object[] fila = { fechaDeCreacionConFormato, pasaje,
				pasaje.getViaje().getUbicacionOrigen().getCiudad() + ", "
						+ pasaje.getViaje().getUbicacionOrigen().getProvincia() + ", "
						+ pasaje.getViaje().getUbicacionOrigen().getPais(),
				pasaje.getViaje().getUbicacionDestino().getCiudad() + ", "
						+ pasaje.getViaje().getUbicacionDestino().getProvincia() + ", "
						+ pasaje.getViaje().getUbicacionDestino().getPais(),
				fechaDeSalidaConFormato + " " + pasaje.getViaje().getHoraSalida(), fechaLlegadaConFormato,
				pasaje.getViaje().getMedioTransporte(),
				"$ " + pasaje.getViaje().getPrecio().add(pasaje.getViaje().getImpuesto()), "$ " + montoAbonado,
				pasaje.getEstadoPasaje()

		};
		this.ventanaMainClientes.getModelPasaje().addRow(fila);
	}

	public BigDecimal montoTotal(Pasaje pasaje) {
		List<Operacion> operaciones = accesoDatos.findOperacionesByPasaje(pasaje);
		BigDecimal montoRetorno = new BigDecimal(0);

		for (Operacion operacion : operaciones)
			montoRetorno = montoRetorno.add(operacion.getMonto());

		montoRetorno = montoRetorno.setScale(2, RoundingMode.HALF_UP);
		return montoRetorno;
	}

	private LocalDateTime crearLocalDateTimeLlegada(Pasaje pasaje) {
		LocalDate fechaSalida = pasaje.getViaje().getFechaSalida();
		LocalTime horaSalida = pasaje.getViaje().getHoraSalida();
		return LocalDateTime.of(fechaSalida, horaSalida);
	}

	private LocalDateTime sumarHorasAFecha(LocalDateTime fechaSalida, Duration horasDeViaje) {
		return fechaSalida.plus(horasDeViaje);
	}

	private void limpiarTelefonos() {
		this.ventanaMainClientes.getTelefono1().setText("");
		this.ventanaMainClientes.getTelefono2().setText("");
		this.ventanaMainClientes.getTelefono3().setText("");
		this.ventanaMainClientes.getTelefono4().setText("");
		this.ventanaMainClientes.getTelefono5().setText("");
	}

	private FocusAdapter metodoUsuario() {
		return new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent arg0) {
				String usuarioAgregar = setNumeroUsuarioAgregar();
				ventanaAgregarCliente.getTxtUsuario().setText(usuarioAgregar);
			}
		};
	}

	private String setNumeroUsuarioAgregar() {
		UsuarioDAOSQL clienteSQL = new UsuarioDAOSQL();
		String usuario = this.ventanaAgregarCliente.getTxtUsuario().getText();

		return comprobacionUsuario(clienteSQL, usuario);
	}

	private String comprobacionUsuario(UsuarioDAOSQL clienteSQL, String usuario) {
		int indice = 1;
		String elegido = usuario;

		while (true) {
			if (clienteSQL.verificarExisteUsuario(usuario)) {
				usuario = elegido + indice;
			} else {
				return usuario;
			}
			indice = indice + 1;
		}
	}

//	private void actualizarVentana() {
//		ventanaMainClientes.getPanel().revalidate();
//		ventanaMainClientes.getPanel().repaint();
//	}

}
