package controladores.personaladm;

import static java.time.temporal.ChronoUnit.DAYS;

import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.mail.MessagingException;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import com.itextpdf.text.DocumentException;

import controladores.ControladorLogin;
import dominio.*;
import modelo.AccesoDatos;
import vistas.personaladm.pasajes.Main_Pasajes;
import vistas.personaladm.pasajes.Ventana_AgregarPasaje;
import vistas.personaladm.pasajes.Ventana_ExtenderReserva;
import vistas.personaladm.pasajes.Ventana_RealizarPago;

public class ControladorPasajes {

    private Main_Pasajes ventanaMainPasajes;
    private Ventana_AgregarPasaje ventanaAgregarPasaje;
    private Ventana_RealizarPago ventanaRealizarPago;
    private Ventana_ExtenderReserva ventanaExtenderReserva;
    private AccesoDatos accesoDatos;
    private List<Pasaje> pasajes_en_tabla;
    private List<Viaje> viajes_en_tabla;
    private List<Usuario> clientes_en_tabla;
    private List<Acompañante> listaAcompañantes;
    private boolean checkClientes = false;
    private boolean checkViajes = false;
    private boolean checkAgregarAcompañante = false;
    private JPanel panelClientes, panelViajes, panelAgregarAcompañante;
    private Mail mail;
    private int idAcompañante;
    private int idContactoAcompañante;
    private boolean checkBuscar = false;
    private PuntajeConfig puntajeConfiguracion;
    private List<Puntaje> listaDePuntos;
    private Viaje viajeAgregarPasaje;
    private Usuario clienteAgregarPasaje;
    private DateTimeFormatter formatoDeFecha = DateTimeFormatter.ofPattern("dd/MM/yyyy");

    public ControladorPasajes(Main_Pasajes ventanaPrincipalPasajes, AccesoDatos acceso) {

        this.ventanaMainPasajes = ventanaPrincipalPasajes;
        ventanaAgregarPasaje = Ventana_AgregarPasaje.getInstance();
        ventanaRealizarPago = Ventana_RealizarPago.getInstance();
        ventanaExtenderReserva = Ventana_ExtenderReserva.getInstance();

        this.ventanaMainPasajes.getBtnAgregar().addActionListener(t -> ventanaEmitirPasaje(t));
        this.ventanaMainPasajes.getBtnPago().addActionListener(e -> ventanaRealizarPago(e));
        this.ventanaMainPasajes.getBtnCancelar().addActionListener(b -> cancelarPasaje(b));
        this.ventanaMainPasajes.getBtnRealizarBusqueda().addActionListener(r -> realizarBusqueda(false));
        this.ventanaMainPasajes.getBtnReserva().addActionListener(r -> ventanaExtenderReserva());
        this.ventanaMainPasajes.getTablaPasajes().addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent arg0) {
                mostrarOperacionesDePasajeSeleccionado();
            }
        });

        this.ventanaMainPasajes.getRadioTodos().addActionListener(b -> realizarBusqueda(false));
        this.ventanaMainPasajes.getRadioPagado().addActionListener(b -> realizarBusqueda(false));
        this.ventanaMainPasajes.getRadioReservado().addActionListener(b -> realizarBusqueda(false));
        this.ventanaMainPasajes.getRadioCancelado().addActionListener(b -> realizarBusqueda(false));
        this.ventanaMainPasajes.getRadioVencido().addActionListener(b -> realizarBusqueda(false));
        this.ventanaMainPasajes.getBtnGenerarComprobante().addActionListener(e -> {
            try {
                generarComprobante();
            } catch (IOException ex) {
                ex.printStackTrace();
            } catch (DocumentException ex) {
                ex.printStackTrace();
            }
        });

        this.ventanaAgregarPasaje.getBtnPanelViajes().addActionListener(v -> mostrarPanelViaje());
        this.ventanaAgregarPasaje.getBtnPanelClientes().addActionListener(v -> mostrarPanelCliente());
        this.ventanaAgregarPasaje.getBtnBuscarViaje().addActionListener(b -> buscarViaje());
        this.ventanaAgregarPasaje.getBtnBuscarCliente().addActionListener(b -> buscarCliente());
        this.ventanaAgregarPasaje.getBtnSeleccionarViaje().addActionListener(v -> seleccionarViaje());
        this.ventanaAgregarPasaje.getBtnSeleccionarCliente().addActionListener(v -> seleccionarCliente());

        this.ventanaAgregarPasaje.getBtnPanelAgregarAcompañante()
                .addActionListener(v -> mostrarPanelAgregarAcompañante());
        this.ventanaAgregarPasaje.getBtnBuscarAcompañante().addActionListener(b -> buscarAcompañante());
        this.ventanaAgregarPasaje.getBtnLimpiarCampos().addActionListener(b -> limpiarCamposAcompañante());
        this.ventanaAgregarPasaje.getBtnQuitarAcompañante().addActionListener(b -> quitarAcompañante());
        this.ventanaAgregarPasaje.getBtnConfirmar().addActionListener(l -> {
            try {
                agregarPasaje(l);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (DocumentException e) {
                e.printStackTrace();
            } catch (MessagingException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        });
        this.ventanaRealizarPago.getBtnConfirmar().addActionListener(r -> {
            try {
                realizarPago(r);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (DocumentException e) {
                e.printStackTrace();
            } catch (MessagingException e) {
                e.printStackTrace();
            }
        });

        this.ventanaAgregarPasaje.getBtnAgregarAcompañante().addActionListener(e -> agregarAcompañante());
        this.ventanaExtenderReserva.getBtnConfirmar().addActionListener(r -> extenderReserva());
        this.ventanaAgregarPasaje.getTxtPuntosAUtilizar().addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                puntosAPlataAgregar();
            }
        });
        this.ventanaRealizarPago.getTxtPuntosAUtilizar().addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                puntosAPlataRealizar();
            }
        });

        this.accesoDatos = acceso;
        this.pasajes_en_tabla = new ArrayList<>();
        this.viajes_en_tabla = new ArrayList<>();
        this.clientes_en_tabla = new ArrayList<>();
        this.listaAcompañantes = new ArrayList<>();

        this.panelClientes = this.ventanaAgregarPasaje.getPanelClientes();
        this.panelViajes = this.ventanaAgregarPasaje.getPanelViajes();
        this.panelAgregarAcompañante = this.ventanaAgregarPasaje.getPanelAgregarAcompañante();
        this.mail = Mail.getInstance(accesoDatos);
        this.puntajeConfiguracion = this.accesoDatos.obtenerConfiguracionDePuntaje();
    }

    public void inicializar() {
        llenarComboBoxPais();
        llenarComboBoxContinente();
        this.pasajes_en_tabla = accesoDatos.obtenerPasajes();
//        cargarArregloPasajes();

        realizarBusqueda(true);
    }

    private void mostrarPanelViaje() {
        mostrarPanelIzquierda(panelViajes, ventanaAgregarPasaje.getBtnPanelViajes(), checkViajes);
    }

    private void mostrarPanelCliente() {
        mostrarPanelIzquierda(panelClientes, ventanaAgregarPasaje.getBtnPanelClientes(), checkClientes);
    }

    private void llenarComboBoxPais() {
        if (this.ventanaAgregarPasaje.getCBPais().getItemCount() == 0) {
            this.ventanaAgregarPasaje.getCBPais().addItem("Seleccione un pais");
            String[] paises = Locale.getISOCountries();

            for (String pais : paises) {
                Locale obj = new Locale("", pais);

                this.ventanaAgregarPasaje.getCBPais().addItem(obj.getDisplayCountry());
            }
            this.ventanaAgregarPasaje.getCBPais().setSelectedIndex(0);
        }
    }

    private void llenarComboBoxContinente() {
        if (this.ventanaAgregarPasaje.getCBContinente().getItemCount() == 0) {
            this.ventanaAgregarPasaje.getCBContinente().addItem("Seleccione un continente");
            for (String continente : this.accesoDatos.obtenerContinentes()) {
                this.ventanaAgregarPasaje.getCBContinente().addItem(continente);
            }
            this.ventanaAgregarPasaje.getCBContinente().setSelectedIndex(0);
        }
    }

//    private void cargarArregloPasajes() {
//        if (this.pasajes_en_tabla.size() == 0) {
//            this.pasajes_en_tabla = accesoDatos.obtenerPasajes();
//        }
//    }

    private void realizarBusqueda(boolean primeraEntrada) {
        Predicate<Pasaje> filtro = filtrosPasaje();
        if (filtro != null) {
            List<Pasaje> lista_filtrada = this.pasajes_en_tabla.stream().filter(filtro).collect(Collectors.toList());

            if (lista_filtrada.size() > 0) {
                limpiarTabla();
                lista_filtrada.forEach(pasaje -> agregarATabla(pasaje));
            } else {
                limpiarTabla();
                if (!primeraEntrada) {
                    JOptionPane.showMessageDialog(null, "No se encontraron pasajes.", "¡Advertencia!", JOptionPane.WARNING_MESSAGE);
                }
            }
        } else {
            limpiarTabla();
            pasajes_en_tabla.forEach(pasaje -> agregarATabla(pasaje));
        }
    }

    private Predicate<Pasaje> filtrosPasaje() {
        List<Predicate<Pasaje>> lista_filtros = new ArrayList<>();

        String estado = radioButtonSeleccionado();
        if (!estado.equals("TODOS")) {
            Predicate<Pasaje> filtroEstado = pasaje -> pasaje.getEstadoPasaje().equals(estado);
            lista_filtros.add(filtroEstado);
        }

        String codigoDeReferencia = ventanaMainPasajes.getTxtCodigo().getText();
        if (!codigoDeReferencia.isEmpty()) {
            Predicate<Pasaje> filtroCodigo = pasaje -> pasaje.getCodigo().equals(codigoDeReferencia);
            lista_filtros.add(filtroCodigo);
        }

        String usuario = ventanaMainPasajes.getTxtUsuario().getText().toLowerCase();
        if (!usuario.isEmpty()) {
            Predicate<Pasaje> filtroUsuario = pasaje -> pasaje.getCliente().getUsuario().toLowerCase()
                    .indexOf(usuario) > -1;
            lista_filtros.add(filtroUsuario);
        }

        String dni = ventanaMainPasajes.getTxtDNI().getText();
        if (!dni.isEmpty()) {
            Predicate<Pasaje> filtroDni = pasaje -> pasaje.getCliente().getDni().equals(dni);
            lista_filtros.add(filtroDni);
        }

        String nombre = ventanaMainPasajes.getTxtNombre().getText();
        if (!nombre.isEmpty()) {
            Predicate<Pasaje> filtroNombre = pasaje -> pasaje.getCliente().getNombre().toLowerCase()
                    .indexOf(nombre) > -1;
            lista_filtros.add(filtroNombre);
        }

        String apellido = ventanaMainPasajes.getTxtApellido().getText();
        if (!apellido.isEmpty()) {
            Predicate<Pasaje> filtroApellido = pasaje -> pasaje.getCliente().getApellido().toLowerCase()
                    .indexOf(apellido) > -1;
            lista_filtros.add(filtroApellido);
        }

        if (lista_filtros.size() == 1) {
            return lista_filtros.get(0);
        } else if (lista_filtros.size() > 1) {
            Predicate<Pasaje> acumulador = lista_filtros.get(0);
            lista_filtros.remove(0);
            for (Predicate<Pasaje> predicado : lista_filtros) {
                acumulador = acumulador.and(predicado);
            }
            return acumulador;
        }

        return null;
    }

    private String radioButtonSeleccionado() {
        if (ventanaMainPasajes.getRadioCancelado().isSelected()) {
            return "CANCELADO";
        } else if (ventanaMainPasajes.getRadioVencido().isSelected()) {
            return "VENCIDO";
        } else if (ventanaMainPasajes.getRadioPagado().isSelected()) {
            return "PAGADO";
        } else if (ventanaMainPasajes.getRadioReservado().isSelected()) {
            return "RESERVADO";
        }
        return "TODOS";
    }

    private void generarComprobante() throws IOException, DocumentException {
        Operacion operacion = operacionSeleccionada();

        Usuario cliente = operacion.getCliente();
        Pasaje pasaje = operacion.getPasaje();
        Viaje viaje = pasaje.getViaje();
        Pago pago = operacion.getPago();

        LocalDateTime fechaSalida = viaje.getFechaSalida().atTime(viaje.getHoraSalida());
        LocalDateTime fechaLlegada = fechaSalida.plus(viaje.getHorasDeViaje().getSeconds(), ChronoUnit.SECONDS);
        DateTimeFormatter formatoFecha = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:MM");

        BigDecimal montoPasaje = null;
        BigDecimal montoRestante = new BigDecimal(0);

        List<Acompañante> listaAcompañantes = null;

        if (pasaje.getId() != 0) {
            listaAcompañantes = this.accesoDatos.obtenerAcompañantesDePasaje(pasaje);
            montoPasaje = viaje.getPrecio().add(viaje.getImpuesto())
                    .multiply(new BigDecimal(listaAcompañantes.size() + 1));
            BigDecimal montoPagado = montoAcumulado(pasaje);
            montoRestante = montoPasaje.subtract(montoPagado);
        }

        String fechaDeOperacion = formatoFechaOperacion(operacion);
        String fechaDePago = "   Fecha de pago: " + fechaDeOperacion + "\n";

        String comprobante = getMensajePopUp(operacion, cliente, viaje, pago, fechaSalida, fechaLlegada, formatoFecha,
                montoPasaje, montoRestante, fechaDePago, listaAcompañantes);


        GeneradorPDF.crear("operacion", comprobante);
        GeneradorPDF.abrir("operacion");
    }

    private String formatoFechaOperacion(Operacion operacion) {
        DateTimeFormatter formatoDeFecha = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
        LocalDateTime fechaCreacionOperacion = operacion.getFechaCreacion();
        return fechaCreacionOperacion.format(formatoDeFecha);
    }

    private String getMensajePopUp(Operacion operacion, Usuario cliente, Viaje viaje, Pago pago,
                                   LocalDateTime fechaSalida, LocalDateTime fechaLlegada, DateTimeFormatter formatoFecha,
                                   BigDecimal montoPasaje, BigDecimal montoRestante, String fechaDePago, List<Acompañante> listaAcompañantes) {

        String metodoDePago = pago.getClass().getSimpleName();
        if (metodoDePago.equals("PagoPunto")) {
            metodoDePago = "Puntos";

        }

        String comprobante = "CLIENTE:" + "\n" + "   Nombre y apellido: " + cliente.getNombre() + " "
                + cliente.getApellido() + "\n" + "   DNI: " + cliente.getDni() + "\n" + "   Usuario: "
                + cliente.getUsuario() + "\n\n" + "VIAJE:" + "\n" + "   Origen: " + viaje.getUbicacionOrigen() + ", "
                + viaje.getUbicacionOrigen().getProvincia() + ", " + viaje.getUbicacionOrigen().getPais() + "\n"
                + "   Destino: " + viaje.getUbicacionDestino() + ", " + viaje.getUbicacionDestino().getProvincia()
                + ", " + viaje.getUbicacionDestino().getPais() + "\n" + "   Medio de Transporte: "
                + viaje.getMedioTransporte() + "\n" + "   Fecha y hora de salida: " + fechaSalida.format(formatoFecha)
                + "\n" + "   Fecha y hora de llegada: " + fechaLlegada.format(formatoFecha) + "\n\n" + "PAGO:" + "\n"
                + "   Precio del pasaje: " + montoPasaje + "\n" + "   Monto a pagar: " + operacion.getMonto() + "\n"
                + "   Monto restante: " + montoRestante + "\n" + "   Metodo de pago: " + metodoDePago + "\n"
                + fechaDePago;

        if (metodoDePago.equals("Puntos")) {
            comprobante = comprobante + "   Puntos utilizados: " + calcularPuntosUsados(operacion.getMonto()) + "\n";
        }

        if (listaAcompañantes.size() > 0) {
            comprobante += "\n\nACOMPAÑANTES:" + "\n";
            for (int i = 0; i < listaAcompañantes.size(); i++) {
                Acompañante acompañante = listaAcompañantes.get(i);
                comprobante = comprobante + "   Nombre y apellido: " + acompañante.getNombre() + " "
                        + acompañante.getApellido() + "\n" + "   DNI: " + acompañante.getDni() + "\n";
            }
        }
        return comprobante;
    }

    private BigDecimal calcularPuntosUsados(BigDecimal monto) {
        return monto.divide(puntajeConfiguracion.getValorUnitario());
    }

    private Operacion operacionSeleccionada() {
        int fila = this.ventanaMainPasajes.getTablaOperaciones().getSelectedRow();
        return (Operacion) this.ventanaMainPasajes.getModelOperaciones().getValueAt(fila, 1);
    }

    public void agregarATabla(Pasaje pasaje) {
        LocalDate fechaDeSalida = pasaje.getViaje().getFechaSalida();
        LocalDate fechaDeReserva = pasaje.getFecha_reserva();
        String fechaDeSalidaConFormato = fechaDeSalida.format(formatoDeFecha);
        String fechaDeReservaConFormato = fechaDeReserva.format(formatoDeFecha);

        Object[] fila = {pasaje, pasaje.getCliente().getNombre() + " " + pasaje.getCliente().getApellido(),
                pasaje.getCliente().getDni(), pasaje.getViaje(), pasaje.getViaje().getUbicacionDestino(),
                fechaDeSalidaConFormato, "$" + montoTotalPagado(pasaje), pasaje.getEstadoPasaje(),
                fechaDeReservaConFormato};

        this.ventanaMainPasajes.getModelPasajes().addRow(fila);
    }

    private void limpiarTabla() {
        this.ventanaMainPasajes.getModelPasajes().setRowCount(0);
        this.ventanaMainPasajes.getModelPasajes().setColumnCount(0);
        this.ventanaMainPasajes.getModelPasajes().setColumnIdentifiers(this.ventanaMainPasajes.getNombreColumnas());
    }

    private void ventanaEmitirPasaje(ActionEvent t) {
        limpiarTablaAcompañante();
        listaAcompañantes.clear();
        ventanaAgregarPasaje.limpiarCamposVentanaPasaje();
        ventanaAgregarPasaje.habilitarBotonesAcompañantes(true);
        limpiarTablaCliente();
        limpiarTablaViaje();
        ventanaAgregarPasaje.mostrarVentana();
    }

    private void extenderReserva() {
        Pasaje pasaje = pasajeEnTabla();

        LocalDate nuevaFecha = ventanaExtenderReserva.getTxtFechaReserva();
        LocalDate fechaOriginal = pasaje.getFecha_reserva();

        pasaje.setFechaReserva(nuevaFecha);
        if (this.accesoDatos.cambiarReservaPasaje(pasaje) == false) {
            pasaje.setFechaReserva(fechaOriginal);
            JOptionPane.showMessageDialog(null, "No se pudo editar la fecha de reserva", "¡Advertencia!", JOptionPane.WARNING_MESSAGE);
        } else {
            realizarBusqueda(true);
            this.ventanaExtenderReserva.cerrar();
        }
    }

    private void ventanaExtenderReserva() {
        try {
            Pasaje pasaje = pasajeEnTabla();
            double porcentajeAbonado = calcularPorcentajeAbonado(pasaje);

            if (pasaje.getEstadoPasaje().equals("RESERVADO")) {

                if (porcentajeAbonado < 30) {
                    this.llenarCampos(pasaje);
                    ventanaExtenderReserva.mostrarVentana();
                } else {
                    JOptionPane.showMessageDialog(null, "Este pasaje ya abonó más del 30%", "¡Advertencia!", JOptionPane.WARNING_MESSAGE);
                }
            } else {
                JOptionPane.showMessageDialog(null, "Seleccione un pasaje con estado RESERVADO", "¡Error!", JOptionPane.ERROR_MESSAGE);
            }
        } catch (ArrayIndexOutOfBoundsException ex) {
            JOptionPane.showMessageDialog(null, "Seleccione un pasaje para extender la reserva.", "¡Error!", JOptionPane.ERROR_MESSAGE);
        }
    }

    private double calcularPorcentajeAbonado(Pasaje pasaje) {
        BigDecimal precioViajeConImpuesto = pasaje.getViaje().getPrecio().add(pasaje.getViaje().getImpuesto());
        BigDecimal montoAcumulado = montoAcumulado(pasaje);
        BigDecimal precioPorCien = montoAcumulado.multiply(new BigDecimal(100));
        BigDecimal divididoTotal = precioPorCien.divide(precioViajeConImpuesto, 2, RoundingMode.HALF_UP);

        double porcentajeAbonado = Double.parseDouble(divididoTotal.toString());

        return porcentajeAbonado;
    }

    private BigDecimal montoAcumulado(Pasaje pasaje) {
        List<Operacion> operaciones = accesoDatos.findOperacionesByPasaje(pasaje);
        BigDecimal montoRetorno = new BigDecimal(0);

        for (Operacion operacion : operaciones)
            montoRetorno = montoRetorno.add(operacion.getMonto());

        montoRetorno = montoRetorno.setScale(2, RoundingMode.HALF_UP);
        return montoRetorno;
    }

    private void llenarCampos(Pasaje pasaje) {
        java.util.Date date = java.sql.Date.valueOf(pasaje.getFecha_reserva());

        this.ventanaExtenderReserva.getReservaChooser().setDate(date);
        this.ventanaExtenderReserva.getReservaChooser().setMinSelectableDate(date);
    }

    private void ventanaRealizarPago(ActionEvent e) {
        ventanaRealizarPago.limpiarCampos();
        try {
            Pasaje pasaje = pasajeEnTabla();
            Usuario clienteSeleccionado = pasaje.getCliente();
            String puntos = cargarPuntos(clienteSeleccionado);
            this.ventanaRealizarPago.getTxtPuntosAcumulados().setText(puntos);
            if (pasaje.getEstadoPasaje().equals("RESERVADO")) {
                ventanaRealizarPago.getTxtPrecioViaje().setText(calcularPrecio(pasaje).toString());

                int fila = this.ventanaMainPasajes.getTablaPasajes().getSelectedRow();
                int columna = this.ventanaMainPasajes.getTablaPasajes().getColumnModel().getColumnIndex("Abonado");
                String abonado = (String) this.ventanaMainPasajes.getModelPasajes().getValueAt(fila, columna);
                BigDecimal montoPagadoEnTabla = new BigDecimal(abonado.substring(1));

                ventanaRealizarPago.getMontoAbonado().setText(montoPagadoEnTabla.toString());
                ventanaRealizarPago.mostrarVentana();
            } else {
                JOptionPane.showMessageDialog(null, "Seleccione un pasaje con estado reservado para realizar el pago.", "¡Error!", JOptionPane.ERROR_MESSAGE);
            }
        } catch (ArrayIndexOutOfBoundsException ex) {
            JOptionPane.showMessageDialog(null, "Seleccione un pasaje para realizar el pago.", "¡Error!", JOptionPane.ERROR_MESSAGE);
        }
    }

    private Pasaje pasajeEnTabla() throws ArrayIndexOutOfBoundsException {
        int fila = this.ventanaMainPasajes.getTablaPasajes().getSelectedRow();

        return (Pasaje) this.ventanaMainPasajes.getModelPasajes().getValueAt(fila, 0);
    }

    private void cancelarPasaje(ActionEvent e) {
        try {
            Pasaje pasaje = pasajeEnTabla();
            if (pasaje.getEstadoPasaje().equals("CANCELADO") || pasaje.getEstadoPasaje().equals("VENCIDO")) {
                JOptionPane.showMessageDialog(null, "Debe elegir un pasaje con estado PAGADO O RESERVADO.", "¡Advertencia!", JOptionPane.WARNING_MESSAGE);
            } else {
                int confirm = JOptionPane.showOptionDialog(null, "¿Está seguro que quiere cancelar este pasaje?",
                        "Confirmacion", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, null);
                if (confirm == 0) {
                    String estadoActual = pasaje.getEstadoPasaje();
                    pasaje.setEstadoPasaje("CANCELADO");

                    Usuario cliente = pasaje.getCliente();
                    Ubicacion ubicacionDestino = pasaje.getViaje().getUbicacionDestino();

                    Mail mail = Mail.getInstance(accesoDatos);

                    mail.enviarConGMail(cliente.getEmail(), "Cancelación de pasaje", "Estimadx " + cliente.getApellido() + " " + cliente.getNombre() + ", con este mail dejamos constancia de que su pasaje a "
                            + ubicacionDestino.getCiudad() + ", " + ubicacionDestino.getProvincia() + ", " + ubicacionDestino.getPais() + " con fecha para el " + formatoDeFecha.format(pasaje.getViaje().getFechaSalida()) + " se ha cancelado.");

                    if (!this.accesoDatos.updatePasaje(pasaje)) {
                        pasaje.setEstadoPasaje(estadoActual);
                        JOptionPane.showMessageDialog(null, "No se pudo cancelar el pasaje.", "¡Error!", JOptionPane.ERROR_MESSAGE);
                    } else {
                        ingresarGastoPorDevolucion(pasaje);
                        realizarBusqueda(true);
                    }
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            JOptionPane.showMessageDialog(null, "Seleccione el pasaje que quiera cancelar.", "¡Error!", JOptionPane.ERROR_MESSAGE);
        }
    }

    public String primerEstado() {
        try {
            BigDecimal montoPagado = new BigDecimal(this.ventanaAgregarPasaje.getMonto().getText());
            BigDecimal precioTotal = new BigDecimal(this.ventanaAgregarPasaje.getTxtPrecioDescuento().getText());
            BigDecimal precioConAcompañante = precioTotal.multiply(new BigDecimal(listaAcompañantes.size() + 1));

            if (montoPagado.compareTo(precioConAcompañante) < 0) {
                return "RESERVADO";
            } else if (montoPagado.compareTo(precioConAcompañante) == 0) {
                return "PAGADO";
            } else {
                JOptionPane.showMessageDialog(null, "El monto ingresado es superior al precio del viaje.", "¡Error!", JOptionPane.ERROR_MESSAGE);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Lo ingresado en el monto al crear un pasaje es inválido", "¡Error!", JOptionPane.ERROR_MESSAGE);
        }
        return "";
    }

    public String estadoPasaje(Pasaje pasaje, BigDecimal monto) {
        try {
            BigDecimal montoPagado = new BigDecimal(ventanaRealizarPago.getMontoAbonado().getText());
            BigDecimal precioTotal = new BigDecimal(ventanaRealizarPago.getTxtPrecioViaje().getText());
            montoPagado = montoPagado.add(monto);

            if (montoPagado.compareTo(precioTotal) < 0) {
                return "RESERVADO";
            } else if (montoPagado.compareTo(precioTotal) == 0) {
                return "PAGADO";
            } else {
                JOptionPane.showMessageDialog(null, "El monto ingresado es superior al precio del viaje.", "¡Advertencia!", JOptionPane.WARNING_MESSAGE);
            }
        } catch (Exception e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null,
                    "Lo ingresado en el monto al realizar el pago de un pasaje es inválido", "¡Error!", JOptionPane.ERROR_MESSAGE);
        }
        return "";
    }

    public BigDecimal montoTotalPagado(Pasaje pasaje) {
        List<Operacion> operaciones = accesoDatos.findOperacionesByPasaje(pasaje);
        BigDecimal montoRetorno = new BigDecimal(0);

        for (Operacion operacion : operaciones) {
            montoRetorno = montoRetorno.add(operacion.getMonto());
        }

        montoRetorno = montoRetorno.setScale(2, RoundingMode.HALF_UP);
        return montoRetorno;
    }

    private BigDecimal calcularPrecio(Pasaje pasaje) {
        BigDecimal precioViaje = pasaje.getViaje().getPrecio();
        BigDecimal impuestoViaje = pasaje.getViaje().getImpuesto();
        BigDecimal precioTotal = precioViaje.add(impuestoViaje);
        int porcentaje = pasaje.getDescuento();
        BigDecimal precioConDescuento = calcularPorcentajeAbonado(porcentaje, precioTotal);
        int cantidadAcompañante = this.accesoDatos.cantidadDeAcompañantes(pasaje);
        BigDecimal precioConAcompañante = precioConDescuento.multiply(new BigDecimal(cantidadAcompañante + 1));

        return precioConAcompañante;
    }

    private String comprobante(Operacion operacion) {
        Usuario cliente = operacion.getCliente();
        Pasaje pasaje = operacion.getPasaje();
        Viaje viaje = pasaje.getViaje();
        Pago pago = operacion.getPago();

        LocalDateTime fechaSalida = viaje.getFechaSalida().atTime(viaje.getHoraSalida());
        LocalDateTime fechaLlegada = fechaSalida.plus(viaje.getHorasDeViaje().getSeconds(), ChronoUnit.SECONDS);
        DateTimeFormatter formatoFecha = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:MM");

        BigDecimal montoPasaje = viaje.getPrecio().add(viaje.getImpuesto());
        BigDecimal montoRestante = new BigDecimal(0);

        List<Acompañante> listaAcompañantes;

        if (pasaje.getId() != 0) {
            listaAcompañantes = this.accesoDatos.obtenerAcompañantesDePasaje(pasaje);
            if (pasaje.getDescuento() > 0) {
                montoPasaje = calcularPorcentajeAbonado(pasaje.getDescuento(), montoPasaje);
            }
            montoPasaje = montoPasaje.multiply(new BigDecimal(listaAcompañantes.size() + 1));
            BigDecimal montoPagado = new BigDecimal(this.ventanaRealizarPago.getMontoAbonado().getText());
            montoRestante = montoPasaje.subtract(montoPagado.add(operacion.getMonto()));
        } else {
            listaAcompañantes = this.listaAcompañantes;
            montoPasaje = viaje.getPrecio().add(viaje.getImpuesto());
            if (pasaje.getDescuento() > 0) {
                montoPasaje = calcularPorcentajeAbonado(pasaje.getDescuento(), montoPasaje);
            }
            montoPasaje = montoPasaje.multiply(new BigDecimal(listaAcompañantes.size() + 1));
            montoRestante = montoPasaje.subtract(operacion.getMonto());
        }

        String fechaDePago = "";

        String comprobante = getMensajePopUp(operacion, cliente, viaje, pago, fechaSalida, fechaLlegada, formatoFecha,
                montoPasaje, montoRestante, fechaDePago, listaAcompañantes);
        return comprobante;
    }

    private void agregarPasaje(ActionEvent l) throws IOException, DocumentException, MessagingException,
            InvocationTargetException, IllegalAccessException {
        if (this.ventanaAgregarPasaje.verificarCampo()) {
            if (this.viajeAgregarPasaje.getCapacidad() >= this.listaAcompañantes.size() + 1) {
                boolean puedeUtilizarSusPuntos = true;
                if (this.ventanaAgregarPasaje.getBtnPunto().isSelected()) {
                    int puntosAcumulados = Integer
                            .parseInt(this.ventanaAgregarPasaje.getTxtPuntosAcumulados().getText());
                    int puntosAUtilizar = Integer.parseInt(this.ventanaAgregarPasaje.getTxtPuntosAUtilizar().getText());
                    puedeUtilizarSusPuntos = puntosAcumulados >= puntosAUtilizar;
                }
                if (puedeUtilizarSusPuntos) {
                    String estado = primerEstado();
                    if (!estado.equals("")) {
                        Pasaje pasaje = crearPasaje(estado);
                        pasaje.setCodigo(MetodosDominio.generarCodigoReferenia(pasajes_en_tabla));

                        Pago pago = crearFormaPago(this.ventanaAgregarPasaje, pasaje);

                        BigDecimal monto = new BigDecimal(ventanaAgregarPasaje.getMonto().getText());
                        Operacion operacion = new Operacion(monto, pago, pasaje, pasaje.getCliente(),
                                ControladorLogin.id_logeado());

                        String comprobante = comprobante(operacion);
                        int confirm = JOptionPane.showOptionDialog(null, comprobante, "Confirmacion",
                                JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, null);
                        if (confirm == 0) {

                            if (this.accesoDatos.agregarOperacionYAcompañantes(operacion, listaAcompañantes)) {

                                String carpetaTempURL = System.getProperty("java.io.tmpdir");
                                GeneradorPDF.crear(carpetaTempURL + "operacion", comprobante);

                                mail = Mail.getInstance(accesoDatos);
                                mail.enviarConGMail(pasaje.getCliente().getEmail(), "Comprobante de compra",
                                        "En el presente mail se le adjunta el comprobante de compra.", "operacion.pdf");



                                listaAcompañantes.clear();
                                pasajes_en_tabla.add(pasaje);
                                realizarBusqueda(true);
                                this.viajeAgregarPasaje = null;
                                this.clienteAgregarPasaje = null;
                                this.idAcompañante = 0;
                                this.idContactoAcompañante = 0;
                                if (!ventanaAgregarPasaje.getBtnPunto().isSelected()) {
                                    agregarPuntaje(operacion);
                                } else {
                                    int puntosUtilizados = Integer
                                            .parseInt(ventanaAgregarPasaje.getTxtPuntosAUtilizar().getText());
                                    restarPuntaje(puntosUtilizados);
                                }
                                this.ventanaAgregarPasaje.cerrar();
                            } else {
                                JOptionPane.showMessageDialog(null, "No se pudo guardar la operación", "¡Error!", JOptionPane.ERROR_MESSAGE);
                            }
                        }
                    }
                } else {
                    JOptionPane.showMessageDialog(null,
                            "Los puntos a utilizar no deben superar los puntos acumulados.", "¡Error!", JOptionPane.ERROR_MESSAGE);
                }
            } else {
                JOptionPane.showMessageDialog(null,
                        "La cantidad de acompañantes además del cliente no pueden ser mayor a la capacidad disponible del viaje", "¡Advertencia!", JOptionPane.WARNING_MESSAGE);
            }
        }
    }

    private Pasaje crearPasaje(String estado) {
        Pasaje pasaje = new Pasaje(this.viajeAgregarPasaje, this.clienteAgregarPasaje, estado,
                ControladorLogin.id_logeado(), ControladorLogin.id_logeado().getLocal());
        LocalDate fechaReserva = ventanaAgregarPasaje.getTxtFechaReserva();
        pasaje.setFechaReserva(fechaReserva);
        pasaje.setDescuento(Integer.parseInt(this.ventanaAgregarPasaje.getTxtDescuento().getText()));

        return pasaje;
    }

    public Pago crearFormaPago(JDialog ventana, Pasaje pasaje) {
        if (ventana instanceof Ventana_AgregarPasaje) {
            if (((Ventana_AgregarPasaje) ventana).getBtnEfectivo().isSelected()) {
                return new Efectivo();
            } else if (((Ventana_AgregarPasaje) ventana).getBtnTarjeta().isSelected()) {
                Usuario clienteSeleccionado = this.accesoDatos
                        .findByNombreUSuario(this.ventanaAgregarPasaje.getCliente().getText());
                return new Tarjeta(ventanaAgregarPasaje.getVtoTarjeta(),
                        ventanaAgregarPasaje.getNumeroTarjeta().getText(), clienteSeleccionado.getNombre(),
                        ventanaAgregarPasaje.getTipoTarjeta(), ventanaAgregarPasaje.getEmpresaTarjeta());
            } else {
                return new PagoPunto();
            }
        }
        if (ventana instanceof Ventana_RealizarPago) {
            if (((Ventana_RealizarPago) ventana).getBtnEfectivo().isSelected()) {
                return new Efectivo();
            } else if (((Ventana_RealizarPago) ventana).getBtnTarjeta().isSelected()) {
                return new Tarjeta(ventanaRealizarPago.getVtoTarjeta(),
                        ventanaRealizarPago.getNumeroTarjeta().getText(), pasaje.getCliente().getNombre(),
                        ventanaRealizarPago.getTipoTarjeta(), ventanaRealizarPago.getEmpresaTarjeta());
            } else {
                return new PagoPunto();
            }
        }
        return null;
    }

    private void realizarPago(ActionEvent r) throws IOException, DocumentException, MessagingException {
        if (this.ventanaRealizarPago.verificarCampo()) {
            BigDecimal monto = new BigDecimal(ventanaRealizarPago.getMonto().getText());
            if (!monto.equals(new BigDecimal(0))) {
                boolean bool = true;
                if (this.ventanaRealizarPago.getBtnPunto().isSelected()) {
                    int puntosAcumulados = Integer
                            .parseInt(this.ventanaRealizarPago.getTxtPuntosAcumulados().getText());
                    int puntosAUtilizar = Integer.parseInt(this.ventanaRealizarPago.getTxtPuntosAUtilizar().getText());
                    bool = puntosAcumulados >= puntosAUtilizar;
                }
                if (bool) {
                    Pasaje pasaje = pasajeEnTabla();

                    String estadoOriginal = pasaje.getEstadoPasaje();
                    String estadoNuevo = estadoPasaje(pasaje, monto);
                    if (!estadoNuevo.equals("")) {
                        Pago pago = crearFormaPago(this.ventanaRealizarPago, pasaje);

                        Operacion operacion = new Operacion(monto, pago, pasaje, pasaje.getCliente(),
                                ControladorLogin.id_logeado());

                        String comprobante = comprobante(operacion);
                        int confirm = JOptionPane.showOptionDialog(null, comprobante, "Confirmacion",
                                JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, null);
                        if (confirm == 0) {

                            String carpetaTempURL = System.getProperty("java.io.tmpdir");

                            GeneradorPDF.crear(carpetaTempURL + "operacion", comprobante);

                            mail = Mail.getInstance(accesoDatos);
                            mail.enviarConGMail(pasaje.getCliente().getEmail(), "Comprobante de pago",
                                    "En el presente mail se le adjunta el comprobante de pago.", "operacion.pdf");

                            pasaje.setEstadoPasaje(estadoNuevo);
                            if (this.accesoDatos.agregarOperacion(operacion) == false) {
                                pasaje.setEstadoPasaje(estadoOriginal);
                                JOptionPane.showMessageDialog(null, "No se pudo guardar la operación", "¡Error!", JOptionPane.ERROR_MESSAGE);
                            } else {
                                agregarFilaTablaOperaciones(operacion);
                                realizarBusqueda(true);
                                if (!ventanaRealizarPago.getBtnPunto().isSelected()) {
                                    agregarPuntaje(operacion);
                                } else {
                                    int puntosUtilizados = Integer
                                            .parseInt(ventanaRealizarPago.getTxtPuntosAUtilizar().getText());
                                    restarPuntaje(puntosUtilizados);
                                }
                                this.ventanaRealizarPago.cerrar();
                            }
                        }
                    }
                }
            } else {
                JOptionPane.showMessageDialog(null, "Lo abonado no puede ser 0", "¡Advertencia!", JOptionPane.WARNING_MESSAGE);
            }
        }
    }

    private void agregarPuntaje(Operacion operacion) {
        BigDecimal cociente = operacion.getMonto().divide(puntajeConfiguracion.getValorEnPrecio());
        int cantidadDePuntos = cociente.intValue() * puntajeConfiguracion.getValorEnPuntos();
        if (cantidadDePuntos > 0) {
            int añosVencimiento = puntajeConfiguracion.getAñosVencimiento();
            int mesesVencimiento = puntajeConfiguracion.getMesesVencimiento();
            LocalDate fechaVencimiento = LocalDate.now().plusYears(añosVencimiento).plusMonths(mesesVencimiento);
            Puntaje puntaje = new Puntaje(cantidadDePuntos, fechaVencimiento, operacion);
            this.accesoDatos.agregarPuntaje(puntaje);
        }
    }

    private void restarPuntaje(int puntosUtilizados) {
        int puntosRestantes = puntosUtilizados;
        for (int i = 0; puntosRestantes > 0; i++) {
            puntosRestantes = puntosRestantes - listaDePuntos.get(i).getCantidad();
            if (puntosRestantes >= 0) {
                listaDePuntos.get(i).setCantidad(0);
                this.accesoDatos.restarPuntaje(listaDePuntos.get(i));
            } else {
                listaDePuntos.get(i).setCantidad(puntosRestantes * (-1));
                this.accesoDatos.restarPuntaje(listaDePuntos.get(i));
            }
        }
    }

    private void buscarViaje() {
        limpiarTablaViaje();
        Predicate<Viaje> filtro = filtroDeViaje();
        if (filtro != null) {
            this.viajes_en_tabla.clear();
            this.accesoDatos.obtenerViajesDisponibles().stream().filter(filtro).forEach(viaje -> {
                mostrarViajes(viaje);
                this.viajes_en_tabla.add(viaje);
            });
            if (this.viajes_en_tabla.isEmpty()) {
                JOptionPane.showMessageDialog(null, "No se encontraron viajes con los parámetros especificados", "¡Advertencia!", JOptionPane.WARNING_MESSAGE);
            }
        } else {
            this.viajes_en_tabla.clear();
        }
    }

    private void limpiarTablaViaje() {
        this.ventanaAgregarPasaje.getModelViajes().setRowCount(0);
        this.ventanaAgregarPasaje.getModelViajes().setColumnCount(0);
        this.ventanaAgregarPasaje.getModelViajes()
                .setColumnIdentifiers(this.ventanaAgregarPasaje.getColumnasDeViajes());
    }

    private Predicate<Viaje> filtroDeViaje() {
        List<Predicate<Viaje>> lista_predicados = new ArrayList<>();

        String ciudad = this.ventanaAgregarPasaje.getTxtCiudad().getText().toLowerCase();
        if (!ciudad.equals("")) {
            Predicate<Viaje> predicadoCiudad = viaje -> viaje.getUbicacionDestino().getCiudad().toLowerCase()
                    .indexOf(ciudad) > -1;
            lista_predicados.add(predicadoCiudad);
        }

        String provincia = this.ventanaAgregarPasaje.getTxtProvincia().getText().toLowerCase();
        if (!provincia.equals("")) {
            Predicate<Viaje> predicadoProvincia = viaje -> viaje.getUbicacionDestino().getProvincia().toLowerCase()
                    .indexOf(provincia) > -1;
            lista_predicados.add(predicadoProvincia);
        }

        if (this.ventanaAgregarPasaje.getCBPais().getSelectedIndex() > 0) {
            String pais = this.ventanaAgregarPasaje.getCBPais().getSelectedItem().toString();
            Predicate<Viaje> predicadoPais = viaje -> viaje.getUbicacionDestino().getPais().equals(pais);
            lista_predicados.add(predicadoPais);
        }

        if (this.ventanaAgregarPasaje.getCBContinente().getSelectedIndex() > 0) {
            String continente = this.ventanaAgregarPasaje.getCBContinente().getSelectedItem().toString();
            Predicate<Viaje> predicadoContinente = viaje -> viaje.getUbicacionDestino().getContinente()
                    .equals(continente);
            lista_predicados.add(predicadoContinente);
        }

        LocalDate fechaDesde = this.ventanaAgregarPasaje.getTxtFechaDesde().getDate().toInstant()
                .atZone(ZoneId.systemDefault()).toLocalDate();
        if (fechaDesde != null) {
            Predicate<Viaje> fechaBase = viaje -> (viaje.getFechaSalida().isEqual(fechaDesde)
                    || viaje.getFechaSalida().isAfter(fechaDesde));
            lista_predicados.add(fechaBase);
        }

        try {
            LocalDate fechaHasta = this.ventanaAgregarPasaje.getTxtFechaHasta().getDate().toInstant()
                    .atZone(ZoneId.systemDefault()).toLocalDate();
            if (fechaHasta != null) {
                Predicate<Viaje> fechaLimite = viaje -> (viaje.getFechaSalida().isEqual(fechaHasta)
                        || viaje.getFechaSalida().isBefore(fechaHasta));
                lista_predicados.add(fechaLimite);
            }
        } catch (IllegalArgumentException il) {
            JOptionPane.showMessageDialog(null, "Lo ingresado en la fecha límite no es válido", "¡Error!", JOptionPane.ERROR_MESSAGE);
            return null;
        } catch (NullPointerException ex) {
        }

        String precioMinimo = this.ventanaAgregarPasaje.getTxtPrecioMinimo().getText();
        if (!precioMinimo.equals("")) {
            try {
                BigDecimal minimo = new BigDecimal(precioMinimo);
                Predicate<Viaje> precioBase = viaje -> viaje.getPrecio().add(viaje.getImpuesto())
                        .compareTo(minimo) >= 0;
                lista_predicados.add(precioBase);
            } catch (IllegalArgumentException il) {
                JOptionPane.showMessageDialog(null, "Lo ingresado en el precio mínimo no es válido", "¡Error!", JOptionPane.ERROR_MESSAGE);
                return null;
            }
        }

        String precioMaximo = this.ventanaAgregarPasaje.getTxtPrecioMaximo().getText();
        if (!precioMaximo.equals("")) {
            try {
                BigDecimal maximo = new BigDecimal(precioMaximo);
                Predicate<Viaje> precioLimite = viaje -> viaje.getPrecio().add(viaje.getImpuesto())
                        .compareTo(maximo) <= 0;
                lista_predicados.add(precioLimite);
            } catch (IllegalArgumentException il) {
                JOptionPane.showMessageDialog(null, "Lo ingresado en el precio máximo no es válido", "¡Error!", JOptionPane.ERROR_MESSAGE);
                return null;
            }
        }

        if (lista_predicados.size() == 1) {
            return lista_predicados.get(0);
        } else if (lista_predicados.size() > 1) {
            Predicate<Viaje> acumulador = lista_predicados.get(0);
            lista_predicados.remove(0);
            for (Predicate<Viaje> predicado : lista_predicados) {
                acumulador = acumulador.and(predicado);
            }
            return acumulador;
        }
        return null;
    }

    private void mostrarViajes(Viaje viaje) {
        BigDecimal precio = viaje.getPrecio().add(viaje.getImpuesto());
        LocalDate fechaDeSalida = viaje.getFechaSalida();
        String fechaDeSalidaConFormato = fechaDeSalida.format(formatoDeFecha);

        Duration duracion = viaje.getHorasDeViaje();
        long horas = duracion.toHours();
        Duration duracionMinutos = duracion.minusHours(duracion.toHours());
        long minutos = duracionMinutos.toMinutes();
        String cantidadHoras = horas + " horas y " + minutos + " minutos";

        Object[] fila = {viaje, viaje.getUbicacionOrigen(), viaje.getUbicacionDestino(), fechaDeSalidaConFormato,
                viaje.getHoraSalida(), cantidadHoras, viaje.getMedioTransporte(), precio.toString(),
                viaje.getCapacidad()};
        this.ventanaAgregarPasaje.getModelViajes().addRow(fila);
    }

    private void buscarCliente() {
        this.clientes_en_tabla.clear();
        limpiarTablaCliente();
        Predicate<Usuario> filtro = filtroDeCliente();
        this.accesoDatos.obtenerClientes().stream()
                .filter(cliente -> (filtro.test(cliente) && cliente.getEstado().equals("HABILITADO")))
                .forEach(cliente -> {
                    mostrarClientes(cliente);
                    this.clientes_en_tabla.add(cliente);
                });
        if (this.clientes_en_tabla.isEmpty()) {
            JOptionPane.showMessageDialog(null, "No se encontraron clientes con los parametros especificados");
        }
    }

    private void limpiarTablaCliente() {
        this.ventanaAgregarPasaje.getModelClientes().setRowCount(0);
        this.ventanaAgregarPasaje.getModelClientes().setColumnCount(0);
        this.ventanaAgregarPasaje.getModelClientes()
                .setColumnIdentifiers(this.ventanaAgregarPasaje.getColumnasDeClientes());
    }

    private Predicate<Usuario> filtroDeCliente() {
        List<Predicate<Usuario>> lista_filtros = new ArrayList<>();

        String nombre = this.ventanaAgregarPasaje.getTxtNombre().getText().toLowerCase();
        if (!nombre.equals("")) {
            Predicate<Usuario> predicadoNombre = cliente -> cliente.getNombre().toLowerCase().indexOf(nombre) > -1;
            lista_filtros.add(predicadoNombre);
        }

        String apellido = this.ventanaAgregarPasaje.getTxtApellido().getText().toLowerCase();
        if (!apellido.equals("")) {
            Predicate<Usuario> predicadoApellido = cliente -> cliente.getApellido().toLowerCase()
                    .indexOf(apellido) > -1;
            lista_filtros.add(predicadoApellido);
        }

        String dni = this.ventanaAgregarPasaje.getTxtDNI().getText();
        if (!dni.equals("")) {
            Predicate<Usuario> predicadoDni = cliente -> cliente.getDni().equals(dni);
            lista_filtros.add(predicadoDni);
        }

        String usuario = this.ventanaAgregarPasaje.getTxtUsuario().getText();
        if (!usuario.equals("")) {
            Predicate<Usuario> predicadoUsuario = cliente -> cliente.getUsuario().indexOf(usuario) > -1;
            lista_filtros.add(predicadoUsuario);
        }

        if (lista_filtros.size() == 1) {
            return lista_filtros.get(0);
        } else if (lista_filtros.size() > 1) {
            Predicate<Usuario> acumulador = lista_filtros.get(0);
            lista_filtros.remove(0);
            for (Predicate<Usuario> predicado : lista_filtros) {
                acumulador = acumulador.and(predicado);
            }
            return acumulador;
        }
        return (cliente -> cliente != null);
    }

    private void mostrarClientes(Usuario cliente) {
        Object[] fila = {cliente, cliente.getNombre(), cliente.getApellido(), cliente.getDni()};
        this.ventanaAgregarPasaje.getModelClientes().addRow(fila);
    }

    private Viaje viajeSeleccionado() throws ArrayIndexOutOfBoundsException {
        int fila = this.ventanaAgregarPasaje.getTablaViajes().getSelectedRow();
        return (Viaje) this.ventanaAgregarPasaje.getModelViajes().getValueAt(fila, 0);
    }

    private void seleccionarViaje() {
        try {
            Viaje viajeSeleccionado = viajeSeleccionado();
            this.viajeAgregarPasaje = viajeSeleccionado;
            this.ventanaAgregarPasaje.getViaje().setText(viajeSeleccionado.getCodigoReferencia());
            this.ventanaAgregarPasaje.getPrecioViaje()
                    .setText(viajeSeleccionado.getPrecio().add(viajeSeleccionado.getImpuesto()).toString());
            if (viajeSeleccionado.getOferta() == null) {
                this.ventanaAgregarPasaje.getTxtDescuento().setText("0");
            } else if (viajeSeleccionado.getOferta().getEstado().equals("INHABILITADO")) {
                this.ventanaAgregarPasaje.getTxtDescuento().setText("0");
            } else {
                this.ventanaAgregarPasaje.getTxtDescuento()
                        .setText("" + viajeSeleccionado.getOferta().getPorcentajeDescuento());
            }

            int porcentajeDescuento = Integer.parseInt(this.ventanaAgregarPasaje.getTxtDescuento().getText());
            BigDecimal precioViaje = new BigDecimal(this.ventanaAgregarPasaje.getPrecioViaje().getText());
            if (porcentajeDescuento != 0) {
                this.ventanaAgregarPasaje.getTxtPrecioDescuento()
                        .setText(calcularPorcentajeAbonado(porcentajeDescuento, precioViaje).toString());
            } else {
                this.ventanaAgregarPasaje.getTxtPrecioDescuento().setText(precioViaje.toString());
            }

            calcularPrecioConAcompañante();
            ventanaAgregarPasaje.getBtnPanelViajes().setText("Seleccionar viaje");
            checkViajes = false;
            acomodarPanel(panelViajes);
            ventanaAgregarPasaje.getBtnPanelClientes().setEnabled(true);
        } catch (ArrayIndexOutOfBoundsException ex) {
            JOptionPane.showMessageDialog(null, "Se debe seleccionar un viaje", "¡Error!", JOptionPane.ERROR_MESSAGE);
        }
    }

    private Usuario clienteSeleccionado() throws ArrayIndexOutOfBoundsException {
        int fila = this.ventanaAgregarPasaje.getTablaClientes().getSelectedRow();
        return (Usuario) this.ventanaAgregarPasaje.getModelClientes().getValueAt(fila, 0);
    }

    private void seleccionarCliente() {
        try {
            Usuario clienteSeleccionado = clienteSeleccionado();
            this.clienteAgregarPasaje = clienteSeleccionado;
            String puntos = cargarPuntos(clienteSeleccionado);
            this.ventanaAgregarPasaje.getCliente().setText(clienteSeleccionado.getUsuario());
            this.ventanaAgregarPasaje.getTxtPuntosAcumulados().setText(puntos);
            ventanaAgregarPasaje.getBtnPanelClientes().setText("Seleccionar cliente");
            checkClientes = false;
            acomodarPanel(panelClientes);
            ventanaAgregarPasaje.getBtnPanelViajes().setEnabled(true);
        } catch (ArrayIndexOutOfBoundsException ex) {
            JOptionPane.showMessageDialog(null, "Se debe seleccionar un cliente", "¡Error!", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void acomodarPanel(JPanel panel) {
        panel.setBounds(-887, panel.getY(), panel.getWidth(), panel.getHeight());
    }

    private Contacto crearContactoAcompañante(String telefono) {
        return new Contacto(telefono, "", "", "", "");
    }

    private void agregarAcompañante() {
        Acompañante acompañante = null;
//        if (!this.ventanaAgregarPasaje.verificarCampoAcompañante()) {
        acompañante = crearAcompañante();
        acompañante.setId(idAcompañante);
        acompañante.getContacto().setId(idContactoAcompañante);
        boolean agregarALista = false;

        if (!existeAcompañante(acompañante)) {
            Acompañante acompañanteDeBdd = this.accesoDatos.obtenerAcompañantePorDNI(acompañante.getDni());
            if (checkBuscar) {
                if (acompañanteDeBdd != null) {
                    if (acompañanteDeBdd.getId() == idAcompañante) {
                        agregarALista = true;
                    } else {
                        JOptionPane.showMessageDialog(null,
                                "El dni del acompañante que intenta guardar ya se encuentra en la base de datos.", "¡Advertencia!", JOptionPane.WARNING_MESSAGE);
                    }
                } else {
                    agregarALista = true;
                }
            } else if (acompañanteDeBdd != null) {
                if (!existeAcompañante(acompañanteDeBdd)) {
                    int confirm = JOptionPane.showOptionDialog(null,
                            "El dni que está ingresando ya se encuentra en la base de datos, ¿Desea sobreescribir el acompañante?",
                            "Confirmacion", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, null);
                    if (confirm == 0) {
                        acompañante.setId(acompañanteDeBdd.getId());
                        acompañante.getContacto().setId(acompañanteDeBdd.getContacto().getId());
                        agregarALista = true;
                    }
                }
            } else {
                agregarALista = true;
            }
        }
        if (agregarALista) {
            limpiarCamposAcompañante();
            listaAcompañantes.add(acompañante);
            agregarAcompañanteATabla(acompañante);
            calcularPrecioConAcompañante();
        }
//        }
    }

    private Acompañante crearAcompañante() {
        String nombre = ventanaAgregarPasaje.getTxtNombreAcompañante().getText();
        String apellido = ventanaAgregarPasaje.getTxtApellidoAcompañante().getText();
        String dni = ventanaAgregarPasaje.getTxtDNIAcompañante().getText();
        String telefono = ventanaAgregarPasaje.getTxtTelefonoAcompañante().getText();
        String email = ventanaAgregarPasaje.getTxtEmailAcompañante().getText();

        LocalDate cumpleaños = ventanaAgregarPasaje.getTxtNacimiento();

        return new Acompañante(dni, nombre, apellido, cumpleaños, email, crearContactoAcompañante(telefono));
    }

    private void calcularPrecioConAcompañante() {
        BigDecimal precioViaje = new BigDecimal(ventanaAgregarPasaje.getTxtPrecioDescuento().getText());
        BigDecimal cantidadAcompañante = new BigDecimal(listaAcompañantes.size() + 1); //+1 para contar el cliente
        BigDecimal montoFinal = new BigDecimal(0);

        if (listaAcompañantes.size() > 0) {
            montoFinal = cantidadAcompañante.multiply(precioViaje);
        }
        this.ventanaAgregarPasaje.getPrecioAcompañantes().setText(montoFinal.toString());
    }

    private boolean existeAcompañante(Acompañante acompañanteNuevo) {
        for (Acompañante acompañante : listaAcompañantes) {

            if (acompañante.equals(acompañanteNuevo)
                    || (acompañanteNuevo.getId() != 0 && acompañante.getId() == acompañanteNuevo.getId())) {
                JOptionPane.showMessageDialog(null,
                        "El acompañante que está intentando ingresar ya se encuentra en la lista de acompañantes actuales.", "¡Advertencia!", JOptionPane.WARNING_MESSAGE);
                return true;
            }
        }
        return false;
    }

    private void mostrarPanelIzquierda(JPanel panel, JButton boton, boolean check) {
        if (check == false) {
            if (boton == ventanaAgregarPasaje.getBtnPanelClientes()) {
                checkClientes = true;
                ventanaAgregarPasaje.getBtnPanelViajes().setEnabled(false);
            } else {
                checkViajes = true;
                ventanaAgregarPasaje.getBtnPanelClientes().setEnabled(false);
            }
            boton.setText("Cerrar panel");
            Thread th = new Thread() {
                @Override
                public void run() {
                    try {
                        while (panel.getX() < 0) {
                            Thread.sleep((long) 0.99);
                            boton.setEnabled(false);
                            panel.setBounds(panel.getX() + 1, panel.getY(), panel.getWidth(), panel.getHeight());
                        }
                        boton.setEnabled(true);
                    } catch (Exception ex) {
                        System.out.println(ex);
                    }
                }
            };
            th.start();
        } else {
            if (boton == ventanaAgregarPasaje.getBtnPanelClientes()) {
                boton.setText("Seleccionar cliente");
                checkClientes = false;
            } else {
                boton.setText("Seleccionar viaje");
                checkViajes = false;
            }
            Thread th = new Thread() {

                @Override
                public void run() {
                    try {
                        while (panel.getX() > (0 - panel.getWidth())) {
                            Thread.sleep((long) 0.99);
                            boton.setEnabled(false);
                            panel.setBounds(panel.getX() - 1, panel.getY(), panel.getWidth(), panel.getHeight());
                        }
                        boton.setEnabled(true);
                        if (boton == ventanaAgregarPasaje.getBtnPanelClientes()) {
                            ventanaAgregarPasaje.getBtnPanelViajes().setEnabled(true);
                        } else {
                            ventanaAgregarPasaje.getBtnPanelClientes().setEnabled(true);
                        }
                    } catch (Exception ex) {
                        System.out.println(ex);
                    }
                }

            };
            th.start();
        }
    }

    private void mostrarPanelAgregarAcompañante() {
        if (checkAgregarAcompañante == false) {
            checkAgregarAcompañante = true;
            ventanaAgregarPasaje.getBtnPanelAgregarAcompañante().setText("Cerrar panel");
            Thread th = new Thread() {
                @Override
                public void run() {
                    try {
                        while ((panelAgregarAcompañante.getX()
                                + panelAgregarAcompañante.getWidth()) > ventanaAgregarPasaje.getPanel().getWidth()) {
                            Thread.sleep((long) 0.99);
                            ventanaAgregarPasaje.getBtnPanelAgregarAcompañante().setEnabled(false);
                            panelAgregarAcompañante.setBounds(panelAgregarAcompañante.getX() - 1,
                                    panelAgregarAcompañante.getY(), panelAgregarAcompañante.getWidth(),
                                    panelAgregarAcompañante.getHeight());
                        }
                        ventanaAgregarPasaje.getBtnPanelAgregarAcompañante().setEnabled(true);
                    } catch (Exception ex) {
                        System.out.println(ex);
                    }
                }
            };
            th.start();
        } else {
            ventanaAgregarPasaje.getBtnPanelAgregarAcompañante().setText("Agregar acompañante");
            checkAgregarAcompañante = false;
            Thread th = new Thread() {

                @Override
                public void run() {
                    try {
                        while (panelAgregarAcompañante.getX() < ventanaAgregarPasaje.getPanel().getWidth()) {
                            Thread.sleep((long) 0.99);
                            ventanaAgregarPasaje.getBtnPanelAgregarAcompañante().setEnabled(false);
                            panelAgregarAcompañante.setBounds(panelAgregarAcompañante.getX() + 1,
                                    panelAgregarAcompañante.getY(), panelAgregarAcompañante.getWidth(),
                                    panelAgregarAcompañante.getHeight());
                        }
                        ventanaAgregarPasaje.getBtnPanelAgregarAcompañante().setEnabled(true);
                        ventanaAgregarPasaje.getBtnPanelAgregarAcompañante().setEnabled(true);
                    } catch (Exception ex) {
                        System.out.println(ex);
                    }
                }

            };
            th.start();
        }
    }

    private void buscarAcompañante() {
        String dniAcompañante = ventanaAgregarPasaje.getTxtDNIAcompañanteBuscar().getText();
        Acompañante acompañante = this.accesoDatos.obtenerAcompañantePorDNI(dniAcompañante);

        if (acompañante != null) {
            java.util.Date date = java.sql.Date.valueOf(acompañante.getFecha_nacimiento());

            this.ventanaAgregarPasaje.getTxtNombreAcompañante().setText(acompañante.getNombre());
            this.ventanaAgregarPasaje.getTxtApellidoAcompañante().setText(acompañante.getApellido());
            this.ventanaAgregarPasaje.getTxtDNIAcompañante().setText(acompañante.getDni());
            this.ventanaAgregarPasaje.getNacimiento().setDate(date);
            this.ventanaAgregarPasaje.getTxtTelefonoAcompañante().setText(acompañante.getContacto().getTelefono1());
            this.ventanaAgregarPasaje.getTxtEmailAcompañante().setText(acompañante.getEmail());

            idAcompañante = acompañante.getId();
            idContactoAcompañante = acompañante.getContacto().getId();
            checkBuscar = true;
        } else {
            JOptionPane.showMessageDialog(null, "No se encontraron acompañantes con los parámetros especificados", "¡Advertencia!", JOptionPane.WARNING_MESSAGE);
        }
    }

    private void limpiarCamposAcompañante() {
        this.ventanaAgregarPasaje.getTxtDNIAcompañanteBuscar().setText("");
        this.ventanaAgregarPasaje.getTxtNombreAcompañante().setText("");
        this.ventanaAgregarPasaje.getTxtApellidoAcompañante().setText("");
        this.ventanaAgregarPasaje.getTxtDNIAcompañante().setText("");
        this.ventanaAgregarPasaje.getNacimiento().setDate(null);
        this.ventanaAgregarPasaje.getTxtTelefonoAcompañante().setText("");
        this.ventanaAgregarPasaje.getTxtEmailAcompañante().setText("");

        idAcompañante = 0;
        idContactoAcompañante = 0;
        checkBuscar = false;
    }

    private void limpiarTablaAcompañante() {
        this.ventanaAgregarPasaje.getModelAcompañante().setRowCount(0);
        this.ventanaAgregarPasaje.getModelAcompañante().setColumnCount(0);
        this.ventanaAgregarPasaje.getModelAcompañante()
                .setColumnIdentifiers(this.ventanaAgregarPasaje.getColumnasDeAcompañantes());
    }

    private void agregarAcompañanteATabla(Acompañante acompañante) {
        LocalDate fechaDeNacimiento = acompañante.getFecha_nacimiento();
        String fechaDeNacimientoConFormato = fechaDeNacimiento.format(formatoDeFecha);

        Object[] fila = {acompañante, acompañante.getApellido(), acompañante.getDni(), fechaDeNacimientoConFormato};
        this.ventanaAgregarPasaje.getModelAcompañante().addRow(fila);
    }

    private void quitarAcompañante() {
        try {
            Acompañante acompañanteSeleccionado = acompañanteSeleccionado();
            int confirm = JOptionPane.showOptionDialog(null, "¿Está seguro que quiere quitar el acompañante?",
                    "Confirmacion", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, null);
            if (confirm == 0) {
                listaAcompañantes.remove(acompañanteSeleccionado);
                int fila = this.ventanaAgregarPasaje.getTablaAcompañantes().getSelectedRow();
                this.ventanaAgregarPasaje.getModelAcompañante().removeRow(fila);
                calcularPrecioConAcompañante();
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            JOptionPane.showMessageDialog(null, "Seleccione el acompañante que quiera quitar.", "¡Advertencia!", JOptionPane.WARNING_MESSAGE);
        }
    }

    private Acompañante acompañanteSeleccionado() throws ArrayIndexOutOfBoundsException {
        int fila = this.ventanaAgregarPasaje.getTablaAcompañantes().getSelectedRow();
        return (Acompañante) this.ventanaAgregarPasaje.getModelAcompañante().getValueAt(fila, 0);
    }

    private void mostrarOperacionesDePasajeSeleccionado() {
        limpiarTablaOperaciones();

        Pasaje pasajeSeleccionado = pasajeEnTabla();
        ArrayList<Operacion> operacionesAsociadasConPasaje = (ArrayList<Operacion>) accesoDatos
                .findOperacionesByPasaje(pasajeSeleccionado);

        operacionesAsociadasConPasaje.forEach(operacion -> agregarFilaTablaOperaciones(operacion));
    }

    private void limpiarTablaOperaciones() {
        this.ventanaMainPasajes.getModelOperaciones().setRowCount(0);
        this.ventanaMainPasajes.getModelOperaciones().setColumnCount(0);
        this.ventanaMainPasajes.getModelOperaciones()
                .setColumnIdentifiers(this.ventanaMainPasajes.getNombreColumnasOperaciones());
    }

    private void agregarFilaTablaOperaciones(Operacion operacion) {
        String tipoOperacon = operacion.getPago().getClass().getSimpleName();

        if (tipoOperacon.equals("PagoPunto")) {
            tipoOperacon = "Puntos";
        }

        Object[] fila = {tipoOperacon, operacion, /* fechaDeCreacionConFormato, */
                operacion.getMonto(), operacion.getResponsable()};

        this.ventanaMainPasajes.getModelOperaciones().addRow(fila);
    }

    private String cargarPuntos(Usuario cliente) {
        int puntaje = 0;
        listaDePuntos = this.accesoDatos.obtenerPuntajeDeCliente(cliente);
        for (Puntaje punto : listaDePuntos) {
            puntaje = puntaje + punto.getCantidad();
        }
        return "" + puntaje;
    }

    private void puntosAPlataAgregar() {
        String puntosEnCampo = ventanaAgregarPasaje.getTxtPuntosAUtilizar().getText();

        BigDecimal puntosAUsar = new BigDecimal(puntosEnCampo);

        String montoPlata = puntosAUsar.multiply(puntajeConfiguracion.getValorUnitario()).toString();

        ventanaAgregarPasaje.getMonto().setText(montoPlata);
    }

    private void puntosAPlataRealizar() {
        String puntosEnCampo = ventanaRealizarPago.getTxtPuntosAUtilizar().getText();

        BigDecimal puntosAUsar = new BigDecimal(puntosEnCampo);

        String montoPlata = puntosAUsar.multiply(puntajeConfiguracion.getValorUnitario()).toString();

        ventanaRealizarPago.getMonto().setText(montoPlata);
    }

    private void ingresarGastoPorDevolucion(Pasaje pasaje) throws InvocationTargetException, IllegalAccessException {
        List<Operacion> operacionesDelPasaje = accesoDatos.findOperacionesByPasaje(pasaje);
        BigDecimal montoPagado = new BigDecimal(0);
        for (Operacion operacion : operacionesDelPasaje) {
            montoPagado = montoPagado.add(operacion.getMonto());
        }
        if (montoPagado.compareTo(new BigDecimal(0)) > 0) {
            long dias = diasQueFaltan(pasaje.getViaje().getFechaSalida());
            DevolucionConfig devolucionConfig = accesoDatos.findDevolucionConfigByRango((int) dias);
            int porcentaje = (devolucionConfig.getPorcentaje() - 100) * (-1);
            BigDecimal montoDevolver = calcularPorcentajeAbonado(porcentaje, montoPagado);

            Gasto gasto = new Gasto(montoDevolver, "DEVOLUCION", pasaje.getLocal(), LocalDate.now());
            gasto.setCodigoReferencia(MetodosDominio.generarCodigoReferenia(accesoDatos.findAllGastos()));

            if (!accesoDatos.insertGasto(gasto)) {
                JOptionPane.showMessageDialog(null, "No se pudo guardar el gasto.", "¡Error!", JOptionPane.ERROR_MESSAGE);
            }
            Devolucion devolucion = new Devolucion(pasaje.getCliente(), montoDevolver, porcentaje);
            if (!accesoDatos.insertDevolucion(devolucion)) {
                JOptionPane.showMessageDialog(null, "No se pudo guardar la devolución.", "¡Error!", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    private long diasQueFaltan(LocalDate fecha) {
        return DAYS.between(LocalDate.now(), fecha);
    }

    private BigDecimal calcularPorcentajeAbonado(int porcentaje, BigDecimal montoTotal) {
        BigDecimal precioPorCien = montoTotal.multiply(new BigDecimal(porcentaje));
        BigDecimal descuento = precioPorCien.divide(new BigDecimal(100), 2, RoundingMode.HALF_UP);
        BigDecimal montoDevolver = montoTotal.subtract(descuento);

        return montoDevolver;
    }
}