package controladores.dueño;

import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.imageio.ImageIO;
import javax.swing.JOptionPane;

import dominio.Gasto;
import dominio.Local;
import dominio.ObjetoRankingUsuario;
import dominio.Operacion;
import dominio.Pasaje;
import dominio.Usuario;
import dominio.reportes.ReporteEmpleadoGeneral;
import dominio.reportes.ReporteEmpleadoLocal;
import dominio.reportes.ReporteGastosGeneral;
import dominio.reportes.ReporteGastosLocal;
import dominio.reportes.ReporteIngresosGeneral;
import dominio.reportes.ReporteIngresosLocal;
import dominio.reportes.ReportePasajeGeneral;
import dominio.reportes.ReportePasajeLocal;
import dominio.reportes.ReporteRankingGeneral;
import dominio.reportes.ReporteRankingLocal;
import modelo.AccesoDatos;
import vistas.dueño.reportes.Main_Reportes;

public class ControladorReportes {

	private Main_Reportes ventanaMainReportes;
	private AccesoDatos accesoDatos;
	private List<Local> listaLocales;

	public ControladorReportes(Main_Reportes ventanaMainReportes, AccesoDatos accesoDatos) {
		this.ventanaMainReportes = ventanaMainReportes;

		this.ventanaMainReportes.getBtnConfirmar().addActionListener(e -> generarReporte());

		this.accesoDatos = accesoDatos;
	}

	public void inicializar() {
		llenarCBLocales();
	}

	private void llenarCBLocales() {
		if (ventanaMainReportes.getCBLocales().getItemCount() == 0) {
			listaLocales = accesoDatos.obtenerLocales();
			for (Local local : listaLocales) {
				if (local.getEstado().equals("HABILITADO")) {
					ventanaMainReportes.getCBLocales().addItem(local.getNombre());
				}
			}
		}
	}

	private void generarReporte() {
		try {
			String radioTipoReporte = radioBotonReporteSeleccionado();
			String radioAgrupacion = radioBotonAgrupacionSeleccionado();
			String rangoFechaDesde = getFechaDesde();
			String rangoFechaHasta = getFechaHasta();
//			Image imagen = Toolkit.getDefaultToolkit().getImage(getClass().getResource("/imagenes/hermespersonajereporte.png"));
			BufferedImage imagen = null;
			try {
				imagen = ImageIO.read(getClass().getResource("/imagenes/hermespersonajereporte.png"));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			switch (radioTipoReporte) {
			case "EMPLEADO":
				switch (radioAgrupacion) {
				case "LOCAL":
					String local = ventanaMainReportes.getCBLocales().getSelectedItem().toString();
					List<Usuario> listaEmpleados = accesoDatos.obtenerEmpleados();
					Predicate<Usuario> empleadosDeLocal = busquedaEmpleadosLocal(local);
					List<Usuario> empleadosParaReporte = listaEmpleados.stream().filter(empleadosDeLocal)
							.collect(Collectors.toList());
					ReporteEmpleadoLocal reporteEmpleadoLocal = new ReporteEmpleadoLocal(empleadosParaReporte, local, rangoFechaDesde, rangoFechaHasta, imagen);
					reporteEmpleadoLocal.mostrar();
					break;
				case "GENERAL":
					ReporteEmpleadoGeneral reporteEmpleadoGeneral = new ReporteEmpleadoGeneral(
							accesoDatos.obtenerEmpleados(), rangoFechaDesde, rangoFechaHasta, imagen);
					reporteEmpleadoGeneral.mostrar();
					break;
				}
				break;
			case "RANKING VENTAS":
				LocalDateTime desdeVentas;
				if (ventanaMainReportes.getTxtDesde().getDate() != null) {
					desdeVentas = ventanaMainReportes.getTxtDesde().getDate().toInstant().atZone(ZoneId.systemDefault())
							.toLocalDateTime();
				} else {
					desdeVentas = null;
				}
				LocalDateTime hastaVentas = ventanaMainReportes.getTxtHasta().getDate().toInstant()
						.atZone(ZoneId.systemDefault()).toLocalDateTime();

				hastaVentas = hastaVentas.withHour(23).withMinute(59).withSecond(59);

				switch (radioAgrupacion) {
				case "LOCAL":
					String local = ventanaMainReportes.getCBLocales().getSelectedItem().toString();

					List<ObjetoRankingUsuario> rankingVentas = accesoDatos.obtenerRankingEmpleados(desdeVentas,
							hastaVentas);
					Predicate<ObjetoRankingUsuario> rankingDeLocal = busquedaRankingLocal(local);
					List<ObjetoRankingUsuario> rankingParaReporte = rankingVentas.stream().filter(rankingDeLocal)
							.collect(Collectors.toList());
					
					ReporteRankingLocal reporteRankingLocal = new ReporteRankingLocal(rankingParaReporte, local, rangoFechaDesde, rangoFechaHasta, imagen);
					reporteRankingLocal.mostrar();
					break;
				case "GENERAL":
					ReporteRankingGeneral reporteRankingGeneral = new ReporteRankingGeneral(
							accesoDatos.obtenerRankingEmpleados(desdeVentas, hastaVentas), rangoFechaDesde, rangoFechaHasta, imagen);
					reporteRankingGeneral.mostrar();
					break;
				}
				break;
			case "EGRESOS":
				List<Gasto> listaGastos = accesoDatos.findAllGastos();
				Predicate<Gasto> gastos = busquedaGastos();
				List<Gasto> gastosParaReporte = listaGastos.stream().filter(gastos).collect(Collectors.toList());
				switch (radioAgrupacion) {
				case "LOCAL":
					ReporteGastosLocal reporteGastosLocal = new ReporteGastosLocal(gastosParaReporte, rangoFechaDesde, rangoFechaHasta, imagen);
					reporteGastosLocal.mostrar();
					break;
				case "GENERAL":
					ReporteGastosGeneral reporteGastosGeneral = new ReporteGastosGeneral(gastosParaReporte, rangoFechaDesde, rangoFechaHasta, imagen);
					reporteGastosGeneral.mostrar();
					break;
				}
				break;
			case "INGRESOS":
				List<Operacion> listaOperaciones = accesoDatos.obtenerOperaciones();
				Predicate<Operacion> operaciones = busquedaIngresos();
				List<Operacion> operacionesParaReporte = listaOperaciones.stream().filter(operaciones)
						.collect(Collectors.toList());
				String local = ventanaMainReportes.getCBLocales().getSelectedItem().toString();
				switch (radioAgrupacion) {
				case "LOCAL":
					if (ventanaMainReportes.getCBTipoIngresos().getSelectedItem().toString().equals("Viaje")) {
						ReporteIngresosLocal reporteIngresosLocal = new ReporteIngresosLocal(operacionesParaReporte,
								"viaje", local, rangoFechaDesde, rangoFechaHasta, imagen);
						reporteIngresosLocal.mostrar();
					} else {
						ReporteIngresosLocal reporteIngresosLocal = new ReporteIngresosLocal(operacionesParaReporte,
								"cliente", local, rangoFechaDesde, rangoFechaHasta, imagen);
						reporteIngresosLocal.mostrar();
					}
					break;
				case "GENERAL":
					if (ventanaMainReportes.getCBTipoIngresos().getSelectedItem().toString().equals("Viaje")) {
						ReporteIngresosGeneral reporteIngresosGeneral = new ReporteIngresosGeneral(
								operacionesParaReporte, "viaje", rangoFechaDesde, rangoFechaHasta, imagen);
						reporteIngresosGeneral.mostrar();
					} else {
						ReporteIngresosGeneral reporteIngresosGeneral = new ReporteIngresosGeneral(
								operacionesParaReporte, "cliente", rangoFechaDesde, rangoFechaHasta, imagen);
						reporteIngresosGeneral.mostrar();
					}
					break;
				}
				break;
			case "PASAJES":
				List<Pasaje> listaPasajes = accesoDatos.findAllPasajesDisponibles();
				Predicate<Pasaje> pasajes = busquedaPasajes();
				List<Pasaje> pasajesParaReporte = listaPasajes.stream().filter(pasajes).collect(Collectors.toList());
				switch (radioAgrupacion) {
				case "LOCAL":
					ReportePasajeLocal reportePasajeLocal = new ReportePasajeLocal(pasajesParaReporte, rangoFechaDesde, rangoFechaHasta, imagen);
					reportePasajeLocal.mostrar();
					break;
				case "GENERAL":
					ReportePasajeGeneral reportePasajeGeneral = new ReportePasajeGeneral(pasajesParaReporte, rangoFechaDesde, rangoFechaHasta, imagen);
					reportePasajeGeneral.mostrar();
					break;
				}
				break;
			}
		} catch (NullPointerException ex) {
			JOptionPane.showMessageDialog(null, "No se encontraron resultados para este reporte.", "¡Advertencia!",
					JOptionPane.WARNING_MESSAGE);
		}
	}

	private String radioBotonReporteSeleccionado() {
		if (ventanaMainReportes.getRadioEmpleados().isSelected()) {
			return "EMPLEADO";
		} else if (ventanaMainReportes.getRadioRankingDeVentas().isSelected()) {
			return "RANKING VENTAS";
		} else if (ventanaMainReportes.getRadioEgresos().isSelected()) {
			return "EGRESOS";
		} else if (ventanaMainReportes.getRadioIngresos().isSelected()) {
			return "INGRESOS";
		} else {
			return "PASAJES";
		}
	}

	private String radioBotonAgrupacionSeleccionado() {
		if (ventanaMainReportes.getRadioLocal().isSelected()) {
			return "LOCAL";
		} else {
			return "GENERAL";
		}
	}

	private Predicate<Gasto> busquedaGastos() {
		List<Predicate<Gasto>> lista_filtros = new ArrayList<>();

		try {
			LocalDate fechaDesde = this.ventanaMainReportes.getTxtDesde().getDate().toInstant()
					.atZone(ZoneId.systemDefault()).toLocalDate();
			Predicate<Gasto> fechaBase = gasto -> (gasto.getFechaCreacion().toLocalDate().isEqual(fechaDesde)
					|| gasto.getFechaCreacion().toLocalDate().isAfter(fechaDesde));
			lista_filtros.add(fechaBase);
		} catch (NullPointerException ex) {
		}

		try {
			LocalDate fechaHasta = this.ventanaMainReportes.getTxtHasta().getDate().toInstant()
					.atZone(ZoneId.systemDefault()).toLocalDate();
			if (fechaHasta != null) {
				Predicate<Gasto> fechaLimite = gasto -> (gasto.getFechaCreacion().toLocalDate().isEqual(fechaHasta)
						|| gasto.getFechaCreacion().toLocalDate().isBefore(fechaHasta));
				lista_filtros.add(fechaLimite);
			}
		} catch (IllegalArgumentException | NullPointerException ex) {
			JOptionPane.showMessageDialog(null, "Lo ingresado en la fecha límite no es válido", "¡Error!", JOptionPane.ERROR_MESSAGE);
		}

		if (this.ventanaMainReportes.getRadioLocal().isSelected()) {
			String local = ventanaMainReportes.getCBLocales().getSelectedItem().toString();
			Predicate<Gasto> filtroLocal = gasto -> gasto.getLocal().getNombre().equals(local);
			lista_filtros.add(filtroLocal);
		}

		String tipoGasto = ventanaMainReportes.getCBTipoEgresos().getSelectedItem().toString();
		if (!tipoGasto.equals("Todos")) {
			Predicate<Gasto> filtroTipo = gasto -> gasto.getTipoGasto().equals(tipoGasto.toUpperCase());
			lista_filtros.add(filtroTipo);
		}

		if (lista_filtros.size() == 1) {
			return lista_filtros.get(0);
		} else if (lista_filtros.size() > 1) {
			Predicate<Gasto> acumulador = lista_filtros.get(0);
			lista_filtros.remove(0);
			for (Predicate<Gasto> predicado : lista_filtros) {
				acumulador = acumulador.and(predicado);
			}
			return acumulador;
		}
		return null;
	}

	private Predicate<Usuario> busquedaEmpleadosLocal(String local) {
		List<Predicate<Usuario>> lista_filtros = new ArrayList<>();

		if (!local.equals("")) {
			Predicate<Usuario> filtroUsuario = empleado -> empleado.getLocal().getNombre().equals(local);
			lista_filtros.add(filtroUsuario);
		}

		if (lista_filtros.size() == 1) {
			return lista_filtros.get(0);
		} else if (lista_filtros.size() > 1) {
			Predicate<Usuario> acumulador = lista_filtros.get(0);
			lista_filtros.remove(0);
			for (Predicate<Usuario> predicado : lista_filtros) {
				acumulador = acumulador.and(predicado);
			}
			return acumulador;
		}
		return null;
	}

	private Predicate<ObjetoRankingUsuario> busquedaRankingLocal(String local) {
		List<Predicate<ObjetoRankingUsuario>> lista_filtros = new ArrayList<>();

		if (!local.equals("")) {
			Predicate<ObjetoRankingUsuario> filtroObjeto = objeto -> objeto.getCreador().getLocal().getNombre()
					.equals(local);
			lista_filtros.add(filtroObjeto);
		}

		if (lista_filtros.size() == 1) {
			return lista_filtros.get(0);
		} else if (lista_filtros.size() > 1) {
			Predicate<ObjetoRankingUsuario> acumulador = lista_filtros.get(0);
			lista_filtros.remove(0);
			for (Predicate<ObjetoRankingUsuario> predicado : lista_filtros) {
				acumulador = acumulador.and(predicado);
			}
			return acumulador;
		}
		return null;
	}

	private Predicate<Pasaje> busquedaPasajes() {
		List<Predicate<Pasaje>> lista_filtros = new ArrayList<>();

		try {
			LocalDate fechaDesde = this.ventanaMainReportes.getTxtDesde().getDate().toInstant()
					.atZone(ZoneId.systemDefault()).toLocalDate();
			Predicate<Pasaje> fechaBase = pasaje -> (pasaje.getFecha_creacion().toLocalDate().isEqual(fechaDesde)
					|| pasaje.getFecha_creacion().toLocalDate().isAfter(fechaDesde));
			lista_filtros.add(fechaBase);
		} catch (NullPointerException ex) {
		}

		try {
			LocalDate fechaHasta = this.ventanaMainReportes.getTxtHasta().getDate().toInstant()
					.atZone(ZoneId.systemDefault()).toLocalDate();
			if (fechaHasta != null) {
				Predicate<Pasaje> fechaLimite = pasaje -> (pasaje.getFecha_creacion().toLocalDate().isEqual(fechaHasta)
						|| pasaje.getFecha_creacion().toLocalDate().isBefore(fechaHasta));
				lista_filtros.add(fechaLimite);
			}
		} catch (IllegalArgumentException | NullPointerException ex) {
			JOptionPane.showMessageDialog(null, "Lo ingresado en la fecha límite no es válido", "¡Error!", JOptionPane.ERROR_MESSAGE);
		}

		if (this.ventanaMainReportes.getRadioLocal().isSelected()) {
			String local = ventanaMainReportes.getCBLocales().getSelectedItem().toString();
			Predicate<Pasaje> filtroLocal = pasaje -> pasaje.getLocal().getNombre().equals(local);
			lista_filtros.add(filtroLocal);
		}

		String estadoPasaje = ventanaMainReportes.getCBTipoPasajes().getSelectedItem().toString();
		if (!estadoPasaje.equals("Ambos")) {
			Predicate<Pasaje> filtroEstado = pasaje -> pasaje.getEstadoPasaje().equals(estadoPasaje.toUpperCase());
			lista_filtros.add(filtroEstado);
		}

		if (lista_filtros.size() == 1) {
			return lista_filtros.get(0);
		} else if (lista_filtros.size() > 1) {
			Predicate<Pasaje> acumulador = lista_filtros.get(0);
			lista_filtros.remove(0);
			for (Predicate<Pasaje> predicado : lista_filtros) {
				acumulador = acumulador.and(predicado);
			}
			return acumulador;
		}
		return null;
	}

	private Predicate<Operacion> busquedaIngresos() {
		List<Predicate<Operacion>> lista_filtros = new ArrayList<>();

		try {
			LocalDate fechaDesde = this.ventanaMainReportes.getTxtDesde().getDate().toInstant()
					.atZone(ZoneId.systemDefault()).toLocalDate();
			Predicate<Operacion> fechaBase = operacion -> (operacion.getFechaCreacion().toLocalDate()
					.isEqual(fechaDesde) || operacion.getFechaCreacion().toLocalDate().isAfter(fechaDesde));
			lista_filtros.add(fechaBase);
		} catch (NullPointerException ex) {
		}

		try {
			LocalDate fechaHasta = this.ventanaMainReportes.getTxtHasta().getDate().toInstant()
					.atZone(ZoneId.systemDefault()).toLocalDate();
			if (fechaHasta != null) {
				Predicate<Operacion> fechaLimite = operacion -> (operacion.getFechaCreacion().toLocalDate()
						.isEqual(fechaHasta) || operacion.getFechaCreacion().toLocalDate().isBefore(fechaHasta));
				lista_filtros.add(fechaLimite);
			}
		} catch (IllegalArgumentException | NullPointerException ex) {
			JOptionPane.showMessageDialog(null, "Lo ingresado en la fecha límite no es válido", "¡Error!", JOptionPane.ERROR_MESSAGE);
		}

		if (this.ventanaMainReportes.getRadioLocal().isSelected()) {
			String local = ventanaMainReportes.getCBLocales().getSelectedItem().toString();
			Predicate<Operacion> filtroLocal = operacion -> operacion.getResponsable().getLocal().getNombre()
					.equals(local);
			lista_filtros.add(filtroLocal);
		}

		Predicate<Operacion> filtroMonto = operacion -> operacion.getMonto().compareTo(BigDecimal.ZERO) > 0;
		lista_filtros.add(filtroMonto);

		if (lista_filtros.size() == 1) {
			return lista_filtros.get(0);
		} else if (lista_filtros.size() > 1) {
			Predicate<Operacion> acumulador = lista_filtros.get(0);
			lista_filtros.remove(0);
			for (Predicate<Operacion> predicado : lista_filtros) {
				acumulador = acumulador.and(predicado);
			}
			return acumulador;
		}
		return null;
	}
	
	private String getFechaDesde() {
		if (ventanaMainReportes.getTxtDesde().getDate() == null) {
			return "Principios del sistema";
		} else {
			Format formato = new SimpleDateFormat("dd/MM/yyyy");
			String fechaDesde = formato.format(ventanaMainReportes.getTxtDesde().getDate());
			return fechaDesde;
		}
	}
	
	private String getFechaHasta() {
		Format formato = new SimpleDateFormat("dd/MM/yyyy");
		String fechaHasta = formato.format(ventanaMainReportes.getTxtHasta().getDate());
		return fechaHasta;
	}
}
