package controladores;

import controladores.administrador.ControladorBackup;
import controladores.administrador.ControladorConfiguraciones;
import controladores.administrador.ControladorEmpleado;
import controladores.administrador.ControladorLocales;
import modelo.AccesoDatos;
import vistas.Main_Administrador;
import vistas.administrador.backup.Main_Backup;
import vistas.administrador.configuraciones.Main_Configuracion;
import vistas.administrador.empleados.Main_Empleados;
import vistas.administrador.locales.Main_Locales;
import vistas.perfil.Main_PerfilDeUsuario;

public class ControladorAdministrador {
	
	private Main_Administrador ventanaMainAdministrador;
	private Main_Locales ventanaMainLocales;
	private Main_Empleados ventanaMainEmpleados;
	private Main_Configuracion ventanaMainConfiguraciones;
	private Main_PerfilDeUsuario ventanaMainPerfilDeUsuario;
	private Main_Backup ventanaMainBackup;
	private AccesoDatos accesoDatos;
	private ControladorLocales controladorLocales;
	private ControladorEmpleado controladorEmpleados;
	private ControladorConfiguraciones controladorConfiguraciones;
	private ControladorPerfilDeUsuario controladorPerfilDeUsuario;
	private ControladorBackup controladorBackup;
    
	
	public ControladorAdministrador(Main_Administrador ventanaMainAdministrador, AccesoDatos acceso) {
		this.ventanaMainAdministrador = ventanaMainAdministrador;
		this.ventanaMainLocales = Main_Locales.getInstance();
		this.ventanaMainEmpleados = Main_Empleados.getInstance();
		this.ventanaMainConfiguraciones = Main_Configuracion.getInstance();
		this.ventanaMainPerfilDeUsuario = Main_PerfilDeUsuario.getInstance();
		this.ventanaMainBackup = Main_Backup.getInstance();
		
		this.ventanaMainAdministrador.getBtnLocales().addActionListener(l -> mostrarMainLocales());
		this.ventanaMainAdministrador.getBtnEmpleados().addActionListener(e -> mostrarMainEmpleados());
		this.ventanaMainAdministrador.getBtnConfiguraciones().addActionListener(e -> mostrarMainConfiguraciones());
		this.ventanaMainAdministrador.getBtnDesconectar().addActionListener(d -> desconectar());
		this.ventanaMainAdministrador.getBtnPerfilDeUsuario().addActionListener(p -> mostrarMainPerfilDeUsuario());
		this.ventanaMainAdministrador.getBtnBackup().addActionListener(b -> mostrarMainBackup());
		
		this.accesoDatos = acceso;
		
		controladorLocales = new ControladorLocales(ventanaMainLocales, accesoDatos);
		controladorEmpleados = new ControladorEmpleado(ventanaMainEmpleados, accesoDatos);
		controladorConfiguraciones = new ControladorConfiguraciones(ventanaMainConfiguraciones, accesoDatos);
		controladorBackup = new ControladorBackup(ventanaMainBackup, accesoDatos);
	}
	
	public void inicializar(ControladorPerfilDeUsuario controladorPerfilDeUsuario) {
		this.controladorPerfilDeUsuario = controladorPerfilDeUsuario;
		mostrarMainEmpleados();
		ventanaMainAdministrador.mostrarVentana();
	}
	
	private void mostrarMainLocales() {
		ventanaMainAdministrador.getPanelCentro().removeAll();
		ventanaMainAdministrador.getPanelCentro().add(ventanaMainLocales.getPanel());
		actualizarVentana();
		controladorLocales.inicializar();
	}
	
	private void mostrarMainEmpleados() {
		ventanaMainAdministrador.getPanelCentro().removeAll();
		ventanaMainAdministrador.getPanelCentro().add(ventanaMainEmpleados.getPanel());
		actualizarVentana();
		controladorEmpleados.inicializar();
	}
	
	private void mostrarMainConfiguraciones() {
		ventanaMainAdministrador.getPanelCentro().removeAll();
		ventanaMainAdministrador.getPanelCentro().add(ventanaMainConfiguraciones.getPanel());
		actualizarVentana();
		controladorConfiguraciones.inicializar();
	}
	
	private void mostrarMainPerfilDeUsuario() {
		ventanaMainAdministrador.getPanelCentro().removeAll();
		ventanaMainAdministrador.getPanelCentro().add(ventanaMainPerfilDeUsuario.getPanel());
		actualizarVentana();
		controladorPerfilDeUsuario.inicializar();
	}
	
	private void mostrarMainBackup() {
		ventanaMainAdministrador.getPanelCentro().removeAll();
		ventanaMainAdministrador.getPanelCentro().add(ventanaMainBackup.getPanel());
		actualizarVentana();
		controladorBackup.inicializar();
	}

	private void actualizarVentana() {
		ventanaMainAdministrador.getPanelCentro().revalidate();
		ventanaMainAdministrador.getPanelCentro().repaint();
	}
	
	 private void desconectar() {
		 ventanaMainAdministrador.cerrarVentana();
	    ControladorLogin.desconectar(ControladorLogin.id_logeado());
	}
}
