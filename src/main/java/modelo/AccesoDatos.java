package modelo;

import dao.*;
import dominio.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import conexion.Conexion;

public class AccesoDatos {

	private EventoDAO evento;
	private UsuarioDAO usuario;
	private MedioTransporteDAO transporte;
	private OperacionDAO operacion;
	private PasajeDAO pasaje;
	private UbicacionDAO ubicacion;
	private ViajeDAO viaje;
	private LocalDAO local;
	private PaisContinenteDAO paisContinente;
	private AcompañanteDAO acompañante;
	private MailDAO mail;
	private PasajeroDAO pasajero;
	private PuntajeDAO puntaje;
	private OfertaDAO oferta;
	private NotificacionDAO notificacion;
	private GastoDAO gasto;
	private DevolucionDAO devolucion;
	private DescripcionEventoDAO descripcionEvento;

	public AccesoDatos(DAOAbstractFactory metodo_persistencia) {
		this.usuario = metodo_persistencia.crearUsuarioDAO();
		this.transporte = metodo_persistencia.crearMedioTransporteDAO();
		this.operacion = metodo_persistencia.crearOperacionDAO();
		this.pasaje = metodo_persistencia.crearPasajeDAO();
		this.ubicacion = metodo_persistencia.crearUbicacionDAO();
		this.viaje = metodo_persistencia.crearViajeDAO();
		this.local = metodo_persistencia.crearLocalDAO();
		this.paisContinente = metodo_persistencia.crearPaisContinenteDAO();
		this.acompañante = metodo_persistencia.crearAcompañanteDAO();
		this.mail = metodo_persistencia.crearMailDAO();
		this.pasajero = metodo_persistencia.crearPasajeroDAO();
		this.evento = metodo_persistencia.crearEventoDAO();
		this.puntaje = metodo_persistencia.crearPuntajeDAO();
		this.oferta = metodo_persistencia.crearOfertaDAO();
		this.notificacion = metodo_persistencia.crearNotificacionDAO();
		this.gasto = metodo_persistencia.crearGastoDAO();
		this.devolucion = metodo_persistencia.crearDevolucionDAO();
		this.descripcionEvento = metodo_persistencia.crearDescripcionDAO();
	}

	public boolean agregarUsuario(Usuario usuario_a_agregar) {
		return this.usuario.insert(usuario_a_agregar);
	}

	public boolean editarUsuario(Usuario usuario_a_editar) {
		return this.usuario.update(usuario_a_editar);
	}

	public boolean cambiarEstadoUsuario(Usuario usuario_a_editar, String estado) {
		return this.usuario.update(usuario_a_editar, estado);
	}

	public List<Usuario> obtenerUsuarios() {
		return this.usuario.findAll();
	}

	public List<Usuario> obtenerClientes() {
		List<Usuario> usuarios = new ArrayList<>();

		for (Usuario empleado : obtenerUsuarios()) {
			if (empleado.getRol().equals("CLIENTE")) {
				usuarios.add(empleado);
			}
		}

		return usuarios;
	}

	public List<Usuario> obtenerEmpleados() {
		List<Usuario> usuarios = new ArrayList<>();

		for (Usuario empleado : obtenerUsuarios()) {
			if (!empleado.getRol().equals("CLIENTE")) {
				usuarios.add(empleado);
			}
		}

		return usuarios;
	}

	public Usuario findByNombreUSuario(String nombreUsuario) {
		return this.usuario.findByNombreUSuario(nombreUsuario);
	}

	public Usuario findUsuarioById(int id) {
		return this.usuario.findById(id);
	}

	public boolean existeNombreUsuario(String usuario) {
		return this.usuario.verificarExisteUsuario(usuario);
	}

	public boolean agregarTransporte(MedioTransporte transporte_a_agregar) {
		return this.transporte.insert(transporte_a_agregar);
	}

	public boolean editarTransporte(MedioTransporte transporte_a_editar) {
		return this.transporte.update(transporte_a_editar);
	}

	public List<MedioTransporte> obtenerTransportes() {
		return this.transporte.findAll();
	}

	public boolean transporteEnUso(MedioTransporte transporte) {
		return this.transporte.enUso(transporte);
	}

	public boolean cambiarEstadoTransporte(MedioTransporte transporte, String estado) {
		return this.transporte.update(transporte, estado);
	}

	public boolean agregarOperacion(Operacion operacion_a_agregar) {
		return this.operacion.insert(operacion_a_agregar);
	}
	
	public boolean agregarOperacionYAcompañantes(Operacion operacion, List<Acompañante> lista_acompañantes) {
		return this.operacion.insert(operacion, lista_acompañantes);
	}

	public List<Operacion> findOperacionesByPasaje(Pasaje pasaje) {
		return this.operacion.findByPasaje(pasaje);
	}
	
	public List<Operacion> obtenerOperaciones(){
		return this.operacion.findAll();
	}

	public List<ObjetoRankingUsuario> obtenerRankingEmpleados(LocalDateTime desde, LocalDateTime hasta){
		return this.operacion.findRankingEmpleados(desde, hasta);
	}
	
	public List<ObjetoRankingViaje> obtenerRankingViaje(LocalDateTime desde, LocalDateTime hasta){
		return this.operacion.findRankingViajes(desde, hasta);
	}
	
	public List<ObjetoRankingUsuario> obtenerRankingCliente(LocalDateTime desde, LocalDateTime hasta){
		return this.operacion.findRankingClientes(desde, hasta);
	}
	
	public List<Pasaje> findAllPasajesDisponibles() {
		return this.pasaje.findAllDisponibles();
	}

	public List<Pasaje> obtenerPasajes() {
		return this.pasaje.findAll();
	}

	public boolean updatePasaje(Pasaje pasaje) {
		return this.pasaje.update(pasaje);
	}
	
	public boolean cambiarReservaPasaje(Pasaje pasaje) {
		return this.pasaje.updateReserva(pasaje);
	}

	public boolean cancelarPasajesDeCliente(Usuario usuario) {
		return this.pasaje.cancelarPasajesDelCliente(usuario);
	}

	public int verificadorCapacidad(Viaje viaje) {
		return this.pasaje.countByDisponibles(viaje);
	}

	public List<Pasaje> findByViaje(Viaje viaje) {
		return this.pasaje.findByViaje(viaje);
	}
	
	public boolean tienePasajesActivos(Usuario cliente) {
		return this.pasaje.tienePasajes(cliente);
	}
	
	public List<Pasaje> findPasajeByCliente(Usuario cliente){
		return this.pasaje.findByCliente(cliente);
	}

	public boolean agregarUbicacion(Ubicacion ubicacion_a_agregar) {
		return this.ubicacion.insert(ubicacion_a_agregar);
	}

	public boolean editarUbicacion(Ubicacion ubicacion_a_editar) {
		return this.ubicacion.update(ubicacion_a_editar);
	}

	public List<Ubicacion> obtenerUbicaciones() {
		return this.ubicacion.findAll();
	}

	public boolean ubicacionEnUso(Ubicacion ubicacion) {
		return this.ubicacion.enUso(ubicacion);
	}

	public boolean cambiarEstadoUbicacion(Ubicacion ubicacion_a_editar, String estado) {
		return this.ubicacion.update(ubicacion_a_editar, estado);
	}

	public boolean agregarViaje(Viaje viaje_a_agregar) {
		return this.viaje.insert(viaje_a_agregar);
	}

	public Viaje obtenerViaje(String codigoReferencia) {
		return this.viaje.findByCOdigoReferencia(codigoReferencia);
	}

	public List<Viaje> obtenerViajes() {
		return this.viaje.findAll();
	}

	public List<Viaje> obtenerViajesConEstado(String estado) {
		return this.viaje.findByEstado(estado);
	}

	public List<Viaje> obtenerViajesDisponibles() {
		return this.viaje.findByDisponibles();
	}

	public boolean editarViaje(Viaje viaje_a_editar) {
		return this.viaje.update(viaje_a_editar);
	}

	public boolean cancelarViaje(Viaje viaje_a_cancelar) {
		return this.viaje.darDeBaja(viaje_a_cancelar);
	}

	public List<Viaje> obtenerViajesConOferta(Oferta oferta){
		return this.viaje.findByOferta(oferta);
	}
	
	public boolean editarOfertaDeViaje(Viaje viaje) {
		return this.viaje.updateOferta(viaje);
	}

	public boolean agregarLocal(Local local) {
		return this.local.insert(local);
	}

	public List<Local> obtenerLocales() {
		return this.local.findAll();
	}

	public Local buscarLocalPorNombre(String nombre) {
		return this.local.findByNombre(nombre);
	}

	public boolean updateLocal(Local local) {
		return this.local.update(local);
	}

	public boolean cambiarEstadoLocal(Local local, String estado) {
		return this.local.update(local, estado);
	}

	public List<Locale> obtenerPaisesDeContinente(String continente) {
		return this.paisContinente.findAllPaises(continente);
	}

	public List<String> obtenerContinentes() {
		return this.paisContinente.finAllContinentes();
	}

	public boolean agregarAcompañante(Acompañante acompañante) {
		return this.acompañante.insert(acompañante);
	}
	
	public boolean editarAcompañante(Acompañante acompañante) {
		return this.acompañante.update(acompañante);
	}

	public List<Acompañante> obtenerAcompañantes() {
		return this.acompañante.findAll();
	}

	public List<Acompañante> obtenerAcompañantesDePasaje(Pasaje pasaje){
		return this.acompañante.findByPasaje(pasaje);
	}
	
	public Acompañante obtenerAcompañantePorID(int id) {
		return this.acompañante.findById(id);
	}
	
	public Acompañante obtenerAcompañantePorDNI(String dni) {
		return this.acompañante.findByDNI(dni);
	}

	public Mail findMailById(int id) {
		return mail.findById(id);
	}

	public boolean insertMail(Mail mail) {
		return this.mail.insert(mail);
	}

	public boolean updateMail(Mail mail) {
		return this.mail.update(mail);
	}
	
	public boolean updateContraseña(Mail mail) {
		return this.mail.updateContraseña(mail);
	}

	public boolean agregarPasajero(Pasaje pasaje, Acompañante acompañante) {
		return this.pasajero.insert(pasaje, acompañante);
	}

	public int cantidadDeAcompañantes(Pasaje pasaje) {
		return this.pasajero.countByPasaje(pasaje);
	}

	public boolean agregarEvento(Evento evento, DescripcionEvento descripcion ) {
		return this.evento.insert(evento, descripcion);
	}

//	public boolean finalizarEvento(Evento evento_a_editar) {
//		return this.evento.finalizar(evento_a_editar);
//	}
	public boolean finalizarEvento(Evento evento_a_editar) {
		return this.descripcionEvento.update(evento_a_editar.getDescripcionEvento());
	}

	public boolean modificarResponsable(Evento evento_a_editar) {
		return this.evento.update(evento_a_editar);
	}

	public List<Evento> obtenerEventos() {
		return this.evento.findAll();
	}

	public List<Evento> obtenerEventosResponsable(Usuario responsable) {
		return this.evento.findByResponsable(responsable.getId());
	}

	public List<Evento> obtenerEventosResponsableNombre(Usuario responsable) {
		return this.evento.findByNombreYApellido(responsable.getNombre(), responsable.getApellido());
	}

    public List<Mail> findAllMails() {
		return this.mail.findAll();
	}

    public boolean agregarPuntaje(Puntaje puntaje) {
		return this.puntaje.insertPuntaje(puntaje);
	}

	public List<Puntaje> obtenerPuntajeDeCliente(Usuario cliente) {
		return this.puntaje.buscarPorCliente(cliente);
	}

	public PuntajeConfig obtenerConfiguracionDePuntaje() {
		return this.puntaje.datosPuntajeConfig();
	}

	public boolean editarPuntajeConfig(PuntajeConfig puntajeConfiguracion) {
		return this.puntaje.updatePuntajeConfig(puntajeConfiguracion);
	}
	
	public boolean restarPuntaje(Puntaje puntaje) {
		return this.puntaje.restarPuntaje(puntaje);
	}
	
	public boolean agregarOferta(Oferta oferta) {
		return this.oferta.insert(oferta);
	}
	
	public List<Oferta> obtenerOfertas(){
		return this.oferta.findAll();
	}
	
	public boolean editarOferta(Oferta oferta) {
		return this.oferta.update(oferta);
	}

    public List<Notificacion> findAllNotificaciones() {
		return notificacion.findAll();
    }

	public boolean agregarNotificacion(Notificacion notificacion) {
		return this.notificacion.insert(notificacion);
	}

	public Notificacion findNotificacionBy(Evento evento) {
		return this.notificacion.findByFKEvento(evento);
	}

    public boolean insertGasto(Gasto gasto) {
		return this.gasto.insert(gasto);
    }

	public List<Gasto> findAllGastos() {
		return this.gasto.findAll();
	}

	public boolean updateGasto(Gasto gasto) {
		return this.gasto.update(gasto);
	}
	
	public boolean cancelarGasto(Gasto gasto) {
		return this.gasto.cancelarGasto(gasto);
	}

    public boolean updateOferta(Oferta oferta) {
		return this.oferta.update(oferta);
    }
    
    public boolean insertDevolucionConfig(DevolucionConfig devolucionConfig) {
    	return this.devolucion.insertDevolucionConfig(devolucionConfig);
    }
    
    public boolean updateDevolucionConfig(DevolucionConfig devolucionConfig) {
    	return this.devolucion.update(devolucionConfig);
    }
    
    public List<DevolucionConfig> findAllDevolucionConfig(){
    	return this.devolucion.findAll();
    }
    
    public boolean insertDevolucion(Devolucion devolucion) {
    	return this.devolucion.insertDevolucion(devolucion);
    }
    
    public DevolucionConfig findDevolucionConfigByRango(int rango) {
    	return this.devolucion.datosDevolucionConfig(rango);
    }

    public boolean insertDescripcionEvento(DescripcionEvento descripcion, int idEvento){
		return descripcionEvento.insert(descripcion, idEvento);
	}

	public boolean updateEvento(Evento evento) {
		return this.evento.update(evento);
	}

	public boolean updateDescripcionEvento(DescripcionEvento descripcionEvento) {
		return this.descripcionEvento.update(descripcionEvento);
	}
	
	public static boolean hayConexion() {
		return Conexion.comprobarConexion();
	}

    public boolean updateNotificacion(Notificacion notificacion) {
		return this.notificacion.update(notificacion);
    }

    public List<DescripcionEvento> findDescripcionesEventosBy(Evento evento) {
		return descripcionEvento.findByEvento(evento);
    }
}
