package main;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.UIManager;

import controladores.ControladorConfiguracionBDD;
import controladores.ControladorLogin;
import daoImplementacion.DAOAbstractFactorySQL;
import dominio.Notificador;
import modelo.AccesoDatos;

public class Main 
{
    public static void main( String[] args )
    {
    	try {
    		JFrame.setDefaultLookAndFeelDecorated(true);
    		JDialog.setDefaultLookAndFeelDecorated(true);
    		UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
    	}
    	catch (Exception e) {
    		e.printStackTrace();
    	}

    	ControladorConfiguracionBDD configuracionBDD = new ControladorConfiguracionBDD(false);
    	configuracionBDD.inicializar(false);
    	while(configuracionBDD.getEstado()) {
			try {
				Thread.sleep((long) 1);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}
       	AccesoDatos acceso = new AccesoDatos(new DAOAbstractFactorySQL());
    	
    	ControladorLogin controladorLogin = new ControladorLogin(acceso);
    	controladorLogin.inicializar();
    	
		Notificador notificador = new Notificador(acceso);
	}
}

