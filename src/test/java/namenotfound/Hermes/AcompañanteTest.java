package namenotfound.Hermes;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import conexion.Conexion;
import daoImplementacion.AcompañanteDAOSQL;
import dominio.Acompañante;
import dominio.Contacto;

public class AcompañanteTest {
	AcompañanteDAOSQL acompañanteDAO;
	Acompañante acompañante;
	Contacto contacto;
	
	@Before
    public void inicializar() {
		acompañanteDAO = new AcompañanteDAOSQL();
		contacto = new Contacto("123", null, null, null, null);

		acompañante = new Acompañante("37368951", "Cacho", "nadie", LocalDate.now(),"cliente@gmail.com", contacto);

		acompañante.setContacto(contacto);
	}
	
	@After
	public void cerrarConexion() {
		Conexion.getConexion().cerrarConexion();
	}

	@Test
	public void insertTest_conDatosValidos_retornaTrue() {
		assertTrue(acompañanteDAO.insert(acompañante));
	}

	@Test
	public void findAll_conTablaCargada_retornaLista(){
		ArrayList<Acompañante> lista_acompañantes = (ArrayList<Acompañante>) acompañanteDAO.findAll();

		for (Acompañante acompañante : lista_acompañantes) {
			System.out.println("acompañante = " + acompañante.getNombre());
		}

		assertNotNull(lista_acompañantes.get(0));
	}

	@Test
	public void findById_conTablaCargada_retornaUnaLista(){
		Acompañante acompañante = acompañanteDAO.findById(1);

		System.out.println("acompañante = " + acompañante.getNombre());

		assertNotNull(acompañante);
	}
}
