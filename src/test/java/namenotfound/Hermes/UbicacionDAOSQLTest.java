package namenotfound.Hermes;

import daoImplementacion.DAOAbstractFactorySQL;
import daoImplementacion.UbicacionDAOSQL;
import dominio.Ubicacion;
import org.junit.*;

import java.util.ArrayList;

import static junit.framework.TestCase.*;

public class UbicacionDAOSQLTest {
    private DAOAbstractFactorySQL DAOFactory;
    private UbicacionDAOSQL ubicacionSQL;

    @Before
    public void instanciar(){
        DAOFactory = new DAOAbstractFactorySQL();
        ubicacionSQL = (UbicacionDAOSQL) DAOFactory.crearUbicacionDAO();
    }

    @Test
    public void insert_conParametroValido_retornaTrue(){
        Ubicacion ubicacion = new Ubicacion("jcp","Buenos Aires","Argentina","America","Hola Mundo");
        ArrayList<String> tags = new ArrayList<String>();
        tags.add("Playa"); tags.add("Frio");
        ubicacion.setTags(tags);

        assertTrue(ubicacionSQL.insert(ubicacion));
    }

    @Test
    public void update_conParametroInsertado_retornaTrue(){
        Ubicacion ubicacion = new Ubicacion("moreno","Buenos Aires","Argentina","wakanda","HM");
        ubicacion.setId(1);

        assertTrue(ubicacionSQL.update(ubicacion));
    }

    @Test
    public void findAll_conDatosTesteadosDelInsert_retornaLista(){
        ArrayList<Ubicacion> ubicacion = (ArrayList<Ubicacion>) ubicacionSQL.findAll();
        
        String pais = ubicacion.get(0).getPais();
        String ciudad = ubicacion.get(0).getCiudad();


        assertNotNull(ubicacion);
        assertEquals(pais, ubicacion.get(0).getPais());
        assertEquals(ciudad, ubicacion.get(0).getCiudad());
    }
    
    @Test
    public void findByTags_conDatosTesteadosDelInsert_retornaLista(){
    	ArrayList<String> tags = new ArrayList<String>();
        tags.add("Playa");
        ArrayList<Ubicacion> lista_ubicaciones = (ArrayList<Ubicacion>) ubicacionSQL.findByTags(tags);
        
        for(Ubicacion ubicacion : lista_ubicaciones) {
        	System.out.println(ubicacion.toString());
        }

        assertNotNull(lista_ubicaciones);
    }
}
