package namenotfound.Hermes;

import dao.DAOAbstractFactory;
import daoImplementacion.DAOAbstractFactorySQL;
import daoImplementacion.OperacionDAOSQL;
import dominio.Operacion;
import org.junit.Test;

import java.util.ArrayList;

import static junit.framework.TestCase.*;

public class OperacionDAOSQLTest {
    DAOAbstractFactory DAOFactory = new DAOAbstractFactorySQL();
    OperacionDAOSQL operacionSQL = (OperacionDAOSQL) DAOFactory.crearOperacionDAO();

    @Test
    public void insert_ConDatosValidos_retornaTrue(){
//        Operacion operacion = new Operacion(new BigDecimal(11),1,1,1);
//        assertTrue(operacionSQL.insert(operacion));
    }

    @Test
    public void readAll_conDatosEnTabla_retornaLista(){
        ArrayList<Operacion> operaciones = (ArrayList<Operacion>) operacionSQL.findAll();

        assertNotNull(operaciones);
    }

}
