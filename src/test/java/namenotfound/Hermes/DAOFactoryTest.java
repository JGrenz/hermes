package namenotfound.Hermes;

import daoImplementacion.*;
import org.junit.*;

import static org.junit.Assert.assertNotNull;

public class DAOFactoryTest {
    private DAOAbstractFactorySQL DAOFactory;

    @Before
    public void inicializacion() {
        DAOFactory = new DAOAbstractFactorySQL();
    }

    @Test
    public void crearMedioTransporteDAO_instanciaValida_retornaMedioTransporteDAOSQL(){
        MedioTransporteDAOSQL medioTrasnporteSQL= (MedioTransporteDAOSQL) DAOFactory.crearMedioTransporteDAO();

        assertNotNull(medioTrasnporteSQL);
    }

    @Test
    public void crearOperacionDAO_instanciaValida_retornaOperacionDAO(){
        OperacionDAOSQL operacionSQL = (OperacionDAOSQL) DAOFactory.crearOperacionDAO();

        assertNotNull(operacionSQL);
    }

    @Test
    public void crearPasajeDAO_InstanciaValida_retornaPasajeDAO(){
        PasajeDAOSQL pasajeSQL = (PasajeDAOSQL) DAOFactory.crearPasajeDAO();

        assertNotNull(pasajeSQL);
    }

    @Test
    public void crearUbicacionDAO_InstanciaValida_retornaUbicacionDAO(){
        UbicacionDAOSQL ubicacionSQL = (UbicacionDAOSQL) DAOFactory.crearUbicacionDAO();

        assertNotNull(ubicacionSQL);
    }

    @Test
    public void crearViajeDAO_instanciaValida_retornaViajeDAO(){
        ViajeDAOSQL viajeSQL = (ViajeDAOSQL) DAOFactory.crearViajeDAO();

        assertNotNull(viajeSQL);
    }

    @Test
    public void crearClienteDAO_instanciaValida_retornaClienteDAO(){
        UsuarioDAOSQL clienteSQL = (UsuarioDAOSQL) DAOFactory.crearUsuarioDAO();

        assertNotNull(clienteSQL);
    }
}
