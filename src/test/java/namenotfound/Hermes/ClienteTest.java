package namenotfound.Hermes;

import static org.junit.Assert.*;

import dominio.Contacto;

import dominio.Usuario;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import conexion.Conexion;
import daoImplementacion.UsuarioDAOSQL;

import java.time.LocalDate;
import java.util.ArrayList;

public class ClienteTest {
	UsuarioDAOSQL clienteDAO;
	Usuario cliente;
	Contacto contacto;
	
	@Before
    public void inicializar() {
		clienteDAO = new UsuarioDAOSQL();
		contacto = new Contacto("123", "1234", "12345", "123456", "1234567");

		cliente = new Usuario("37368951", "Cacho", "nadie", "cnadie", LocalDate.now(), "cliente@gmail.com","CLIENTE");

		cliente.setContacto(contacto);
	}
	
	@After
	public void cerrarConexion() {
		Conexion.getConexion().cerrarConexion();
	}

	@Test
	public void insertTest_conDatosValidos_retornaTrue() {
		assertTrue(clienteDAO.insert(cliente));
	}

	@Test
	public void updateTest_conDatosValidos_retornaTrue(){
		cliente.setNombre("Nahuel");
		cliente.setApellido("BF");
		cliente.setId(1);
		cliente.getContacto().setTelefono1("000");
		cliente.getContacto().setTelefono2("0000");
		cliente.getContacto().setId(5);

		assertTrue(clienteDAO.update(cliente));
	}

	@Test
	public void findAll_conTablaCargada_retornaLista(){
		ArrayList<Usuario> lista_clientes = (ArrayList<Usuario>) clienteDAO.findAll();

		for (Usuario cliente : lista_clientes) {
			System.out.println("cliente = " + cliente.getNombre());
		}

		assertNotNull(lista_clientes.get(0));
	}

	@Test
	public void findById_conTablaCargada_retornaUnaLista(){
		Usuario cliente = clienteDAO.findById(1);

		System.out.println("cliente = " + cliente.getNombre());

		assertNotNull(cliente);
	}

    @Test
    public void contarUsuarios_conUsuariosEnTabla_retornaEntero(){
	    int cantidadUsuarios = clienteDAO.contarUsuariosSimilares("cburgos");

	    assertEquals(3,cantidadUsuarios);
    }

}
