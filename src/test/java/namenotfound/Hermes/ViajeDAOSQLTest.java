package namenotfound.Hermes;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import daoImplementacion.DAOAbstractFactorySQL;
import daoImplementacion.ViajeDAOSQL;
import dominio.MedioTransporte;
import dominio.Oferta;
import dominio.Ubicacion;
import dominio.Viaje;

public class ViajeDAOSQLTest {
    DAOAbstractFactorySQL DAOFactory;
    ViajeDAOSQL viajeSQL;
    Ubicacion ubicacion;
    MedioTransporte medioTransporte;
    String estadoViaje;
    Viaje viaje;

    @Before
    public void inicializar() {
        DAOFactory = new DAOAbstractFactorySQL();
        viajeSQL = (ViajeDAOSQL) DAOFactory.crearViajeDAO();

        ubicacion = new Ubicacion("prueba1", "prueba1", "prueba1", "prueba1", "prueba1");
        ubicacion.setId(1);
        
        ubicacion = new Ubicacion("prueba1", "prueba1", "prueba1", "prueba1", "prueba1");
        ubicacion.setId(2);

        medioTransporte = new MedioTransporte("prueba1");
        medioTransporte.setId(1);

        estadoViaje = "CANCELADO";
        
        

        viaje = new Viaje(ubicacion,ubicacion, LocalDate.now(), LocalTime.now(),Duration.parse("PT20H59M5S"),22,medioTransporte,new BigDecimal(22.2), new BigDecimal(10.2));
    }

    @Test
    public void insert_conParametroValido_retornaTrue() {
        assertTrue(viajeSQL.insert(viaje));
    }

    @Test
    public void update_conObjetoEnTabla_retornaTrue() {
        LocalDate fechaSalida = LocalDate.of(2018,12,24);
        LocalTime horaSalida = LocalTime.now();
        Duration dur = Duration.parse("PT2H2M2S");
        
        viaje.setId(3);
        viaje.setHorasDeViaje(dur);
        viaje.setFechaSalida(fechaSalida);
        viaje.setHoraSalida(horaSalida);

        assertTrue(viajeSQL.update(viaje));
    }

    @Test
    public void readAll_conDatosEnTabla_retornaLista(){
        ArrayList<Viaje> viajes = (ArrayList<Viaje>) viajeSQL.findAll();

        for(Viaje viaje : viajes) {
        	System.out.println("id :"+ viaje.getId() + " Horas: " + viaje.getHorasDeViaje());
        }

        assertNotNull(viajes);
    }

    @Test
    public void findByIdTest_conViajeEnTabla_retornaViaje(){
        Viaje viaje = viajeSQL.findById(2); //chequear que el id exista en tabla

        assertNotNull(viaje);
    }

    @Test
    public void darDeBajaTest_conViajeEnTabla_retornaTrue(){
        viaje.setId(4);

        assertTrue(viajeSQL.darDeBaja(viaje));
        assertEquals("cancelado",viaje.getEstadoViaje());
    }

    @Test //chequear que exista el codigo de referencia
    public void findByCodigoReferencia_conViajeEnTabla_retornaViaje(){
        Viaje viaje = viajeSQL.findByCOdigoReferencia("VER COMO HACER EL CODIGO DE REFERENCIA");

        assertNotNull(viaje);
//        assertEquals(2,viaje.getId());
    }

    @Test
    public void quitarOferta_conViajeEnTabla_retornaTrue(){
        viaje.setId(4);
        
        Oferta oferta = new Oferta(null, 0, null);
        oferta.setId(6);
        viaje.setOferta(oferta);
        assertTrue(viajeSQL.updateOferta(viaje));
    }    
}
