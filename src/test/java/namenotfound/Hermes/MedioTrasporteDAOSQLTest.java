package namenotfound.Hermes;

import daoImplementacion.DAOAbstractFactorySQL;
import daoImplementacion.MedioTransporteDAOSQL;
import dominio.MedioTransporte;
import org.junit.*;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class MedioTrasporteDAOSQLTest {
    DAOAbstractFactorySQL DAOFactory;
    MedioTransporteDAOSQL medioTransporteSQL;

    @Before
    public void inicializar(){
        DAOFactory = new DAOAbstractFactorySQL();
        medioTransporteSQL = (MedioTransporteDAOSQL) DAOFactory.crearMedioTransporteDAO();
    }

    @Test
    public void findAll_ConDatosEnBDD_retornaLista(){
        ArrayList<MedioTransporte> mediosTransporte;

        mediosTransporte = (ArrayList<MedioTransporte>) medioTransporteSQL.findAll();

        assertNotNull(mediosTransporte);
//        assertEquals(2,mediosTransporte.size());
        assertEquals("avion",mediosTransporte.get(0).getNombre());
        assertEquals(0,mediosTransporte.get(0).getId());
    }

    @Test
    public void insert_conParametroValido_retornaTrue(){
       MedioTransporte tren = new MedioTransporte("Tren");

        assertTrue(medioTransporteSQL.insert(tren));
    }

    @Test
    public void update_conParametroValido_retornaTrue(){
        MedioTransporte medioT = new MedioTransporte("camion");
        medioT.setId(2);

        assertTrue(medioTransporteSQL.update(medioT));
    }

    @Test
    public void findById_ConDatosEnBDD_retornaUnaFila(){
        MedioTransporte medioTransporte = medioTransporteSQL.findById(1);

        assertNotNull(medioTransporte);
        assertEquals("avion", medioTransporte.getNombre());
        assertEquals(1,medioTransporte.getId());
    }

    @Test
    public void findByName_ConDatosEnBDD_retornaTrue(){

        assertEquals("camion",medioTransporteSQL.findByName("camion").getNombre());

    }


}
