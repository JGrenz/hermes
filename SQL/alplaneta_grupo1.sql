-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.3.14-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win32
-- HeidiSQL Versión:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para alplaneta_grupo1
DROP DATABASE IF EXISTS `alplaneta_grupo1`;
CREATE DATABASE IF NOT EXISTS `alplaneta_grupo1` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `alplaneta_grupo1`;

-- Volcando estructura para tabla alplaneta_grupo1.acompañante
CREATE TABLE IF NOT EXISTS `acompañante` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dni` varchar(50) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `apellido` varchar(50) NOT NULL,
  `fecha_nacimiento` datetime NOT NULL,
  `email` varchar(50) NOT NULL,
  `id_contacto` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `dni` (`dni`),
  KEY `FK_contacto_acompañante` (`id_contacto`),
  CONSTRAINT `FK_contacto_acompañante` FOREIGN KEY (`id_contacto`) REFERENCES `contacto` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla alplaneta_grupo1.acompañante: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `acompañante` DISABLE KEYS */;
INSERT INTO `acompañante` (`id`, `dni`, `nombre`, `apellido`, `fecha_nacimiento`, `email`, `id_contacto`) VALUES
	(1, '13094283', 'Javier', 'Marenco', '1956-05-28 00:00:00', 'correo@correo.com', 13),
	(2, '23459010', 'Santiago', 'Montiel', '1980-03-16 00:00:00', 'mail@mail.com', 12);
/*!40000 ALTER TABLE `acompañante` ENABLE KEYS */;

-- Volcando estructura para tabla alplaneta_grupo1.contacto
CREATE TABLE IF NOT EXISTS `contacto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `telefono1` varchar(25) DEFAULT NULL,
  `telefono2` varchar(25) DEFAULT NULL,
  `telefono3` varchar(25) DEFAULT NULL,
  `telefono4` varchar(25) DEFAULT NULL,
  `telefono5` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla alplaneta_grupo1.contacto: ~21 rows (aproximadamente)
/*!40000 ALTER TABLE `contacto` DISABLE KEYS */;
INSERT INTO `contacto` (`id`, `telefono1`, `telefono2`, `telefono3`, `telefono4`, `telefono5`) VALUES
	(1, '02320430377', '1122322222', '', '', ''),
	(2, '47471717', '', '', '', ''),
	(3, '1188902627', '', '', '', ''),
	(4, '1109195502', '43417900', '', '', ''),
	(5, '1178584508', '', '', '', ''),
	(6, '1134563760', '', '', '', ''),
	(7, '1194859092', '', '', '', ''),
	(8, '1194785888', '', '', '', ''),
	(9, '1231231788', '48482982', '', '', ''),
	(10, '1158799325', '', '', '', ''),
	(11, '1178934568', '1181296984', '', '', ''),
	(12, '', '', '', '', ''),
	(13, '44919827', '', '', '', ''),
	(14, '44919589', '1138473892', '', '', ''),
	(15, '1174829998', '', '', '', ''),
	(16, '74747192', '1165467487', '', '', ''),
	(17, '95910022', '75895954', '1156754389', '', ''),
	(18, '', '', '', '', ''),
	(19, '49941299', '', '', '', ''),
	(20, '1123142233', '', '', '', ''),
	(21, '48419239', '', '', '', ''),
	(22, '', '', '', '', ''),
	(23, '', '', '', '', '');
/*!40000 ALTER TABLE `contacto` ENABLE KEYS */;

-- Volcando estructura para tabla alplaneta_grupo1.continente
CREATE TABLE IF NOT EXISTS `continente` (
  `codigo` varchar(50) NOT NULL,
  `continente` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`codigo`),
  UNIQUE KEY `continente` (`continente`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla alplaneta_grupo1.continente: ~7 rows (aproximadamente)
/*!40000 ALTER TABLE `continente` DISABLE KEYS */;
INSERT INTO `continente` (`codigo`, `continente`) VALUES
	('AF', 'África'),
	('AN', 'Antártida'),
	('AS', 'Asia'),
	('EU', 'Europa'),
	('NA', 'Norte América'),
	('OC', 'Oceania'),
	('SA', 'Sudamérica');
/*!40000 ALTER TABLE `continente` ENABLE KEYS */;

-- Volcando estructura para tabla alplaneta_grupo1.devolucion
CREATE TABLE IF NOT EXISTS `devolucion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_cliente` int(11) NOT NULL,
  `monto` decimal(19,2) NOT NULL,
  `fecha_creacion` datetime NOT NULL,
  `porcentaje_devolucion` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_devolucion_cliente` (`id_cliente`),
  CONSTRAINT `FK_devolucion_cliente` FOREIGN KEY (`id_cliente`) REFERENCES `usuario` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla alplaneta_grupo1.devolucion: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `devolucion` DISABLE KEYS */;
INSERT INTO `devolucion` (`id`, `id_cliente`, `monto`, `fecha_creacion`, `porcentaje_devolucion`) VALUES
	(1, 11, 300.00, '2019-06-26 02:23:57', '75');
/*!40000 ALTER TABLE `devolucion` ENABLE KEYS */;

-- Volcando estructura para tabla alplaneta_grupo1.devolucion_config
CREATE TABLE IF NOT EXISTS `devolucion_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_creador` int(11) NOT NULL,
  `desde` int(11) NOT NULL,
  `hasta` int(11) DEFAULT NULL,
  `porcentaje` int(11) NOT NULL,
  `fecha_creacion` datetime NOT NULL DEFAULT current_timestamp(),
  `estado` enum('HABILITADO','INHABILITADO') NOT NULL DEFAULT 'HABILITADO',
  PRIMARY KEY (`id`),
  KEY `FK_devolucion_creador` (`id_creador`),
  CONSTRAINT `FK_devolucion_creador` FOREIGN KEY (`id_creador`) REFERENCES `usuario` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla alplaneta_grupo1.devolucion_config: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `devolucion_config` DISABLE KEYS */;
INSERT INTO `devolucion_config` (`id`, `id_creador`, `desde`, `hasta`, `porcentaje`, `fecha_creacion`, `estado`) VALUES
	(1, 1, 0, 15, 100, '2019-06-16 01:18:12', 'HABILITADO'),
	(2, 1, 16, 20, 75, '2019-06-16 01:19:48', 'HABILITADO'),
	(3, 1, 21, 30, 50, '2019-06-16 01:20:46', 'HABILITADO'),
	(4, 1, 30, NULL, 25, '2019-06-16 01:22:37', 'HABILITADO');
/*!40000 ALTER TABLE `devolucion_config` ENABLE KEYS */;

-- Volcando estructura para tabla alplaneta_grupo1.evento
CREATE TABLE IF NOT EXISTS `evento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_creador` int(11) NOT NULL,
  `motivo_cambio` varchar(150) DEFAULT NULL,
  `id_cliente` int(11) DEFAULT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `telefono` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `id_responsable` int(11) NOT NULL,
  `fecha_creacion` datetime NOT NULL,
  `id_motivo_evento` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_id_cliente` (`id_cliente`),
  KEY `FK_id_responsable` (`id_responsable`),
  KEY `FK_id_creador` (`id_creador`),
  KEY `FK_evento` (`id_motivo_evento`),
  CONSTRAINT `FK_id_cliente` FOREIGN KEY (`id_cliente`) REFERENCES `usuario` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_id_creador` FOREIGN KEY (`id_creador`) REFERENCES `usuario` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_id_responsable` FOREIGN KEY (`id_responsable`) REFERENCES `usuario` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla alplaneta_grupo1.evento: ~8 rows (aproximadamente)
/*!40000 ALTER TABLE `evento` DISABLE KEYS */;
INSERT INTO `evento` (`id`, `id_creador`, `motivo_cambio`, `id_cliente`, `nombre`, `telefono`, `email`, `id_responsable`, `fecha_creacion`, `id_motivo_evento`) VALUES
	(1, 4, '', NULL, 'Mariano Vargas', '', 'mariano@gmail.com', 4, '2019-06-26 02:47:37', 1),
	(2, 4, '', NULL, 'Daniel Alvarez', '1159592303', 'daniel@hotmail.com', 4, '2019-06-26 02:51:15', 2),
	(3, 4, '', 11, NULL, NULL, NULL, 4, '2019-06-26 02:53:01', 4),
	(4, 4, '', 7, NULL, NULL, NULL, 4, '2019-06-26 02:55:42', 5),
	(5, 6, '', NULL, 'Javier Marenco', '', 'javier@correo.com', 6, '2019-06-26 03:04:41', 6),
	(6, 6, '', 9, NULL, NULL, NULL, 6, '2019-06-26 03:06:48', 7),
	(7, 6, '', NULL, 'Gaston Alles', '1156739198', '', 6, '2019-06-26 03:16:09', 8),
	(8, 3, '', NULL, 'Ivo Koch', '1167894232', 'ikoch@gmail.com', 3, '2019-07-05 01:20:01', 9);
/*!40000 ALTER TABLE `evento` ENABLE KEYS */;

-- Volcando estructura para tabla alplaneta_grupo1.gasto
CREATE TABLE IF NOT EXISTS `gasto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha_creacion` datetime NOT NULL,
  `codigo_referencia` varchar(50) NOT NULL,
  `monto` varchar(30) NOT NULL,
  `tipoGasto` enum('SERVICIO','SUELDO','CONSUMOS_VARIOS','DEVOLUCION') NOT NULL DEFAULT 'CONSUMOS_VARIOS',
  `fk_local` int(11) NOT NULL,
  `estado` enum('HABILITADO','INHABILITADO') NOT NULL,
  `fecha_gasto` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `codigo_referencia` (`codigo_referencia`),
  KEY `fk_local` (`fk_local`),
  CONSTRAINT `fk_local` FOREIGN KEY (`fk_local`) REFERENCES `local` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla alplaneta_grupo1.gasto: ~8 rows (aproximadamente)
/*!40000 ALTER TABLE `gasto` DISABLE KEYS */;
INSERT INTO `gasto` (`id`, `fecha_creacion`, `codigo_referencia`, `monto`, `tipoGasto`, `fk_local`, `estado`, `fecha_gasto`) VALUES
	(1, '2019-06-26 02:08:16', 'ac522', '15050', 'SERVICIO', 1, 'HABILITADO', '2019-06-26 00:00:00'),
	(2, '2019-06-26 02:08:35', '46e70', '53000', 'SUELDO', 1, 'HABILITADO', '2019-06-26 00:00:00'),
	(3, '2019-06-26 02:09:07', '9cfbf', '570', 'CONSUMOS_VARIOS', 1, 'HABILITADO', '2019-06-26 00:00:00'),
	(4, '2019-06-26 02:09:21', 'b2626', '37000', 'SUELDO', 2, 'HABILITADO', '2019-06-26 00:00:00'),
	(5, '2019-06-26 02:09:34', 'fea0a', '1600', 'CONSUMOS_VARIOS', 2, 'HABILITADO', '2019-04-12 00:00:00'),
	(6, '2019-06-26 02:09:50', 'f13fd', '12500', 'SERVICIO', 2, 'HABILITADO', '2019-05-14 00:00:00'),
	(7, '2019-06-26 02:13:06', '5805c', '37000', 'SUELDO', 2, 'HABILITADO', '2019-05-17 00:00:00'),
	(8, '2019-06-26 02:23:57', '1b61d', '300.00', 'DEVOLUCION', 1, 'HABILITADO', '2019-06-26 00:00:00');
/*!40000 ALTER TABLE `gasto` ENABLE KEYS */;

-- Volcando estructura para tabla alplaneta_grupo1.local
CREATE TABLE IF NOT EXISTS `local` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `direccion` varchar(50) NOT NULL,
  `localidad` varchar(50) NOT NULL,
  `detalle` varchar(150) DEFAULT NULL,
  `nombre` varchar(50) NOT NULL,
  `estado` enum('HABILITADO','INHABILITADO') NOT NULL DEFAULT 'HABILITADO',
  `provincia` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `CUIT` (`nombre`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla alplaneta_grupo1.local: ~7 rows (aproximadamente)
/*!40000 ALTER TABLE `local` DISABLE KEYS */;
INSERT INTO `local` (`id`, `direccion`, `localidad`, `detalle`, `nombre`, `estado`, `provincia`) VALUES
	(1, 'Av. San Martin 123', 'San Miguel', 'Horario: 8hs - 18hs, Lu - Vi', 'Local 1', 'HABILITADO', 'Buenos Aires'),
	(2, 'Av. Santa Fe 2400', 'Recoleta', 'Horario: 8hs - 12hs, 13hs - 19hs, Lu - Sa', 'Local 2', 'HABILITADO', 'Ciudad Autónoma de Buenos Aires'),
	(3, 'Ruta 202 3850', 'Don Torcuato', 'Horario: 10hs - 14hs, 16hs - 20hs, Lu - Vi', 'Local 3', 'INHABILITADO', 'Buenos Aires'),
	(4, 'Calle Falsa 123', 'Don Torcuato', 'Horario: 9hs - 12hs, Lu - Ma', 'Localsito', 'HABILITADO', 'Buenos Aires'),
	(5, 'Av. Rivadavia 5200', ' San Isidro', '', 'Alplaneta A', 'HABILITADO', 'San Luis'),
	(6, 'Corrientes 60', 'Palermo', '', 'Alplaneta B', 'HABILITADO', 'Tucumán'),
	(7, 'Ruta 202', 'Bahia Blanca', 'Horario: 8hs - 18hs, Lu - Ju', 'SuperLocal', 'HABILITADO', 'Córdoba');
/*!40000 ALTER TABLE `local` ENABLE KEYS */;

-- Volcando estructura para tabla alplaneta_grupo1.mail
CREATE TABLE IF NOT EXISTS `mail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `direccion` varchar(30) NOT NULL DEFAULT '0',
  `contraseña` varchar(65) NOT NULL DEFAULT '0',
  `remitente` varchar(50) NOT NULL DEFAULT '0',
  `firma` varchar(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `direccion` (`direccion`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla alplaneta_grupo1.mail: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `mail` DISABLE KEYS */;
INSERT INTO `mail` (`id`, `direccion`, `contraseña`, `remitente`, `firma`) VALUES
	(1, 'hermespp1@gmail.com', 'projecthermes123', 'Proyecto Hermes', 'Atentamente, empresa Hermes.');
/*!40000 ALTER TABLE `mail` ENABLE KEYS */;

-- Volcando estructura para tabla alplaneta_grupo1.medio_transporte
CREATE TABLE IF NOT EXISTS `medio_transporte` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(30) NOT NULL DEFAULT '0',
  `estado` enum('HABILITADO','INHABILITADO') DEFAULT 'HABILITADO',
  PRIMARY KEY (`id`),
  UNIQUE KEY `nombre` (`nombre`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla alplaneta_grupo1.medio_transporte: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `medio_transporte` DISABLE KEYS */;
INSERT INTO `medio_transporte` (`id`, `nombre`, `estado`) VALUES
	(1, 'Avion', 'HABILITADO'),
	(2, 'Barco', 'HABILITADO'),
	(3, 'Micro', 'HABILITADO'),
	(4, 'Tren', 'INHABILITADO');
/*!40000 ALTER TABLE `medio_transporte` ENABLE KEYS */;

-- Volcando estructura para tabla alplaneta_grupo1.motivo_evento
CREATE TABLE IF NOT EXISTS `motivo_evento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha_creacion` datetime NOT NULL,
  `titulo` varchar(50) NOT NULL,
  `descripcion` varchar(300) NOT NULL,
  `fecha_resolucion_estimada` datetime DEFAULT NULL,
  `estado` enum('ABIERTO','FINALIZADO','VENCIDO') NOT NULL DEFAULT 'ABIERTO',
  `tipo` enum('CONSULTA','RECLAMO','OTRO') NOT NULL DEFAULT 'CONSULTA',
  `fecha_resolucion_real` datetime DEFAULT NULL,
  `id_evento` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_evento` (`id_evento`),
  CONSTRAINT `FK_evento` FOREIGN KEY (`id_evento`) REFERENCES `evento` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla alplaneta_grupo1.motivo_evento: ~9 rows (aproximadamente)
/*!40000 ALTER TABLE `motivo_evento` DISABLE KEYS */;
INSERT INTO `motivo_evento` (`id`, `fecha_creacion`, `titulo`, `descripcion`, `fecha_resolucion_estimada`, `estado`, `tipo`, `fecha_resolucion_real`, `id_evento`) VALUES
	(1, '2019-06-26 02:47:37', 'Estacionamiento para motos', 'El interesado Mariano Vargas consulta si en el hotel "UNGS" hay estacionamiento para su moto.', '2019-06-28 15:30:00', 'ABIERTO', 'CONSULTA', NULL, 1),
	(2, '2019-06-26 02:51:15', 'Servicio a la habitación', 'El interesado Daniel Alvarez consulta si en el hotel "UNGS" se realiza servicio a la habitación.', '2019-07-08 12:00:00', 'ABIERTO', 'CONSULTA', NULL, 2),
	(3, '2019-06-26 02:53:01', 'Pago de reintegro', 'El usuario Juan Grenz reclama que no se le hizo el reintegro correspondiente a lo que abonó en el viaje\n5d0d0.', '2019-06-28 18:10:00', 'FINALIZADO', 'RECLAMO', '2019-06-26 00:00:00', 3),
	(4, '2019-06-26 02:53:54', 'Pago de reintegro', 'El usuario Juan Grenz vuelve a reclamar el reintegro del pasaje cancelado para el viaje 5d0d0.', '2019-07-03 02:53:38', 'ABIERTO', 'RECLAMO', NULL, 3),
	(5, '2019-06-26 02:55:42', 'Postulación para puesto laboral', 'El cliente Martin Bornholdt entregó su CV en recepción para solicitar trabajo en la empresa. Enviar CV al jefe.', '2019-07-01 14:10:00', 'ABIERTO', 'OTRO', NULL, 4),
	(6, '2019-06-26 03:04:41', 'Mochila perdida', 'Javier Marenco consulta si en el Avión del viaje 3db25 no se encontró una mochila que se olvido. Revisar avión.', '2019-07-10 11:00:00', 'ABIERTO', 'CONSULTA', NULL, 5),
	(7, '2019-06-26 03:06:48', 'Mala atención', 'El cliente Nahuel Burgos reclama que durante el viaje 16aa7 los asistentes de abordo no le prestaron atención a sus pedidos.', '2019-06-29 12:00:00', 'ABIERTO', 'RECLAMO', NULL, 6),
	(8, '2019-06-26 03:16:09', 'Rampa para silla de ruedas', 'El interesado Gaston Alles consulta que hoteles en Misiones, Argentina tienen rampa para sillas de ruedas en su entrada.', '2019-06-29 14:35:00', 'ABIERTO', 'CONSULTA', NULL, 7),
	(9, '2019-07-05 01:20:01', 'Hoteles cerca del mar', 'El interesado Ivo Koch consulta que hoteles se encuentran cerca del mar en mar de las pompas.', '2019-11-15 11:30:00', 'ABIERTO', 'CONSULTA', NULL, 8);
/*!40000 ALTER TABLE `motivo_evento` ENABLE KEYS */;

-- Volcando estructura para tabla alplaneta_grupo1.notificacion
CREATE TABLE IF NOT EXISTS `notificacion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha_creacion` datetime NOT NULL,
  `fecha_vencimiento` datetime NOT NULL,
  `hora_vencimiento` time NOT NULL,
  `titulo` varchar(30) NOT NULL,
  `contenido` varchar(700) NOT NULL,
  `estado` enum('ABIERTO','FINALIZADO') NOT NULL DEFAULT 'ABIERTO',
  `id_evento` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_eventoNotificacion` (`id_evento`),
  CONSTRAINT `fk_eventoNotificacion` FOREIGN KEY (`id_evento`) REFERENCES `evento` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla alplaneta_grupo1.notificacion: ~8 rows (aproximadamente)
/*!40000 ALTER TABLE `notificacion` DISABLE KEYS */;
INSERT INTO `notificacion` (`id`, `fecha_creacion`, `fecha_vencimiento`, `hora_vencimiento`, `titulo`, `contenido`, `estado`, `id_evento`) VALUES
	(1, '2019-06-26 02:47:37', '2019-06-28 15:30:00', '15:30:00', 'Recordatorio de evento', 'Recuerde que para este momento (28/06/2019 a las 15:30hs.) ya debe haber resuelto el evento Estacionamiento para motos.\nTipo del evento: CONSULTA\nInteresado: Mariano Vargas\nDescripción: El interesado Mariano Vargas consulta si en el hotel "UNGS" hay estacionamiento para su moto.\nDe ya haberlo completado, marquelo como finalizado.', 'ABIERTO', 1),
	(2, '2019-06-26 02:51:15', '2019-07-08 12:00:00', '12:00:00', 'Recordatorio de evento', 'Recuerde que para este momento (05/07/2019 a las 10:15hs.) ya debe haber resuelto el evento Servicio a la habitación.\nTipo del evento: CONSULTA\nInteresado: Daniel Alvarez\nDescripción: El interesado Daniel Alvarez consulta si en el hotel "UNGS" se realiza servicio a la habitación.\nDe ya haberlo completado, marquelo como finalizado.', 'ABIERTO', 2),
	(3, '2019-06-26 02:53:01', '2019-06-28 18:10:00', '02:53:38', 'Recordatorio de evento', 'Recuerde que para este momento (03/07/2019 a las 02:53hs.) ya debe haber resuelto el evento Pago de reintegro.\nTipo del evento: RECLAMO\nInteresado: Cliente jgrenz\nDescripción: El usuario Juan Grenz vuelve a reclamar el reintegro del pasaje cancelado para el viaje 5d0d0.\nDe ya haberlo completado, marquelo como finalizado.', 'ABIERTO', 3),
	(4, '2019-06-26 02:53:01', '2019-06-27 00:21:00', '00:25:00', 'Recordatorio de evento', 'Recuerde que para este momento (04/07/2019 a las 14:10hs.) ya debe haber resuelto el evento Postulación para puesto laboral.\nTipo del evento: OTRO\nInteresado: Cliente mbornholdt\nDescripción: El cliente Martin Bornholdt entregó su CV en recepción para solicitar trabajo en la empresa. Enviar CV al jefe.\nDe ya haberlo completado, marquelo como finalizado.', 'ABIERTO', 4),
	(5, '2019-06-26 03:04:41', '2019-07-10 11:00:00', '11:00:00', 'Recordatorio de evento', 'Recuerde que para este momento (10/07/2019 a las 11:00hs.) ya debe haber resuelto el evento Mochila perdida.\nTipo del evento: CONSULTA\nInteresado: Javier Marenco\nDescripción: Javier Marenco consulta si en el Avión del viaje 3db25 no se encontró una mochila que se olvido. Revisar avión.\nDe ya haberlo completado, marquelo como finalizado.', 'ABIERTO', 5),
	(6, '2019-06-26 03:06:48', '2019-06-29 12:00:00', '12:00:00', 'Recordatorio de evento', 'Recuerde que para este momento (29/06/2019 a las 12:00hs.) ya debe haber resuelto el evento Mala atención.\nTipo del evento: RECLAMO\nInteresado: Cliente nburgos\nDescripción: El cliente Nahuel Burgos reclama que durante el viaje 16aa7 los asistentes de abordo no le prestaron atención a sus pedidos.\nDe ya haberlo completado, marquelo como finalizado.', 'ABIERTO', 6),
	(7, '2019-06-26 03:16:09', '2019-06-29 14:35:00', '14:35:00', 'Recordatorio de evento', 'Recuerde que para este momento (29/06/2019 a las 14:35hs.) ya debe haber resuelto el evento Rampa para silla de ruedas.\nTipo del evento: CONSULTA\nInteresado: Gaston Alles\nDescripción: El interesado Gaston Alles consulta que hoteles en Misiones, Argentina tienen rampa para sillas de ruedas en su entrada.\nDe ya haberlo completado, marquelo como finalizado.', 'ABIERTO', 7),
	(8, '2019-07-05 01:42:02', '2019-07-05 01:43:00', '01:43:00', 'Recordatorio de evento', 'Recuerde que para este momento (05/07/2019 a las 01:43hs.) ya debe haber resuelto el evento TITULO DEL EVENTO.\nTipo del evento: RECLAMO\nInteresado: Cliente lskywalker\nDescripción: MOTIVO DEL EVENTO\nDe ya haberlo completado, marquelo como finalizado.', 'ABIERTO', 8);
/*!40000 ALTER TABLE `notificacion` ENABLE KEYS */;

-- Volcando estructura para tabla alplaneta_grupo1.oferta
CREATE TABLE IF NOT EXISTS `oferta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha_creacion` datetime NOT NULL,
  `fecha_vencimiento` datetime NOT NULL,
  `codigo_referencia` varchar(50) NOT NULL,
  `titulo` varchar(30) NOT NULL,
  `descuento` int(11) NOT NULL,
  `estado` enum('HABILITADO','INHABILITADO') NOT NULL DEFAULT 'INHABILITADO',
  PRIMARY KEY (`id`),
  UNIQUE KEY `codigo_referencia` (`codigo_referencia`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla alplaneta_grupo1.oferta: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `oferta` DISABLE KEYS */;
INSERT INTO `oferta` (`id`, `fecha_creacion`, `fecha_vencimiento`, `codigo_referencia`, `titulo`, `descuento`, `estado`) VALUES
	(1, '2019-06-26 02:05:52', '2019-07-11 00:00:00', '6f866', 'Black monday', 50, 'HABILITADO'),
	(2, '2019-06-26 02:06:09', '2019-08-02 00:00:00', '943fd', 'Cyber Friday', 25, 'HABILITADO');
/*!40000 ALTER TABLE `oferta` ENABLE KEYS */;

-- Volcando estructura para tabla alplaneta_grupo1.operacion
CREATE TABLE IF NOT EXISTS `operacion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_pasaje` int(11) NOT NULL DEFAULT 0,
  `id_comprador` int(11) NOT NULL DEFAULT 0,
  `id_pago` int(11) NOT NULL DEFAULT 0,
  `abonado` decimal(19,2) unsigned NOT NULL DEFAULT 0.00,
  `fecha_creacion` datetime NOT NULL,
  `id_creador` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `FK_pasaje` (`id_pasaje`),
  KEY `FK_comprador` (`id_comprador`),
  KEY `FK_pago` (`id_pago`),
  KEY `FK_operacion_creador` (`id_creador`),
  CONSTRAINT `FK_comprador` FOREIGN KEY (`id_comprador`) REFERENCES `usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_operacion_creador` FOREIGN KEY (`id_creador`) REFERENCES `usuario` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_pago` FOREIGN KEY (`id_pago`) REFERENCES `pago` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_pasaje` FOREIGN KEY (`id_pasaje`) REFERENCES `pasaje` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla alplaneta_grupo1.operacion: ~22 rows (aproximadamente)
/*!40000 ALTER TABLE `operacion` DISABLE KEYS */;
INSERT INTO `operacion` (`id`, `id_pasaje`, `id_comprador`, `id_pago`, `abonado`, `fecha_creacion`, `id_creador`) VALUES
	(1, 1, 11, 1, 1200.00, '2019-06-26 02:23:41', 4),
	(2, 2, 11, 2, 3000.00, '2019-06-26 02:25:10', 4),
	(3, 3, 10, 3, 5200.00, '2019-06-26 02:25:49', 4),
	(4, 4, 7, 4, 0.00, '2019-06-26 02:27:10', 4),
	(5, 5, 9, 5, 5600.00, '2019-06-26 02:27:52', 4),
	(6, 6, 8, 6, 3400.34, '2019-06-26 02:28:27', 4),
	(7, 7, 17, 7, 7000.00, '2019-06-29 18:28:10', 5),
	(8, 8, 16, 8, 1686.99, '2019-06-30 18:31:06', 5),
	(9, 9, 13, 9, 16420.00, '2019-06-30 18:31:55', 6),
	(10, 10, 14, 10, 0.00, '2019-06-30 18:32:10', 6),
	(11, 11, 17, 11, 13500.00, '2019-07-02 18:32:42', 19),
	(12, 12, 16, 12, 18001.50, '2019-07-02 18:32:56', 19),
	(13, 13, 14, 13, 10344.00, '2019-07-01 18:33:12', 19),
	(14, 14, 13, 14, 0.00, '2019-07-03 18:33:32', 20),
	(15, 15, 14, 15, 0.00, '2019-07-03 18:33:44', 20),
	(16, 16, 15, 16, 12036.11, '2019-07-03 18:34:22', 21),
	(17, 17, 14, 17, 8600.00, '2019-07-02 18:34:39', 21),
	(18, 18, 15, 18, 29152.00, '2019-07-02 18:40:07', 23),
	(19, 19, 15, 19, 1200.34, '2019-06-30 18:40:23', 23),
	(20, 20, 16, 20, 11354.99, '2019-06-30 18:40:41', 23),
	(21, 21, 13, 21, 750.00, '2019-07-02 18:41:05', 22),
	(22, 22, 17, 22, 23122.00, '2019-07-01 18:41:25', 22);
/*!40000 ALTER TABLE `operacion` ENABLE KEYS */;

-- Volcando estructura para tabla alplaneta_grupo1.pago
CREATE TABLE IF NOT EXISTS `pago` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `forma` varchar(10) NOT NULL COMMENT 'Efectivo, Tarjeta, Puntos',
  `tipo_tarj` varchar(10) DEFAULT NULL COMMENT 'Debito, Credito',
  `marca_tarj` varchar(20) DEFAULT NULL COMMENT 'Visa, MasterCard, AmericanExpress',
  `numero_tarj` varchar(16) DEFAULT NULL,
  `vencimiento_tarj` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla alplaneta_grupo1.pago: ~22 rows (aproximadamente)
/*!40000 ALTER TABLE `pago` DISABLE KEYS */;
INSERT INTO `pago` (`id`, `forma`, `tipo_tarj`, `marca_tarj`, `numero_tarj`, `vencimiento_tarj`) VALUES
	(1, 'Tarjeta', 'Crédito', 'Visa', '1234567890123456', '2021-06-01 00:00:00'),
	(2, 'Efectivo', NULL, NULL, NULL, NULL),
	(3, 'Tarjeta', 'Crédito', 'AmericanExpress', '7823462783468765', '2021-05-01 00:00:00'),
	(4, 'Tarjeta', 'Débito', 'MasterCard', '8721564782468767', '2020-08-01 00:00:00'),
	(5, 'Efectivo', NULL, NULL, NULL, NULL),
	(6, 'Efectivo', NULL, NULL, NULL, NULL),
	(7, 'Efectivo', NULL, NULL, NULL, NULL),
	(8, 'Efectivo', NULL, NULL, NULL, NULL),
	(9, 'Efectivo', NULL, NULL, NULL, NULL),
	(10, 'Efectivo', NULL, NULL, NULL, NULL),
	(11, 'Efectivo', NULL, NULL, NULL, NULL),
	(12, 'Efectivo', NULL, NULL, NULL, NULL),
	(13, 'Efectivo', NULL, NULL, NULL, NULL),
	(14, 'Efectivo', NULL, NULL, NULL, NULL),
	(15, 'Efectivo', NULL, NULL, NULL, NULL),
	(16, 'Efectivo', NULL, NULL, NULL, NULL),
	(17, 'Efectivo', NULL, NULL, NULL, NULL),
	(18, 'Efectivo', NULL, NULL, NULL, NULL),
	(19, 'Efectivo', NULL, NULL, NULL, NULL),
	(20, 'Efectivo', NULL, NULL, NULL, NULL),
	(21, 'Efectivo', NULL, NULL, NULL, NULL),
	(22, 'Efectivo', NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `pago` ENABLE KEYS */;

-- Volcando estructura para tabla alplaneta_grupo1.pais_continente
CREATE TABLE IF NOT EXISTS `pais_continente` (
  `codigo_pais` varchar(50) NOT NULL,
  `codigo_continente` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`codigo_pais`),
  KEY `FK_pais_continente_continente` (`codigo_continente`),
  CONSTRAINT `FK_pais_continente_continente` FOREIGN KEY (`codigo_continente`) REFERENCES `continente` (`codigo`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla alplaneta_grupo1.pais_continente: ~249 rows (aproximadamente)
/*!40000 ALTER TABLE `pais_continente` DISABLE KEYS */;
INSERT INTO `pais_continente` (`codigo_pais`, `codigo_continente`) VALUES
	('AO', 'AF'),
	('BF', 'AF'),
	('BI', 'AF'),
	('BJ', 'AF'),
	('BW', 'AF'),
	('CD', 'AF'),
	('CF', 'AF'),
	('CG', 'AF'),
	('CI', 'AF'),
	('CM', 'AF'),
	('CV', 'AF'),
	('DJ', 'AF'),
	('DZ', 'AF'),
	('EG', 'AF'),
	('EH', 'AF'),
	('ER', 'AF'),
	('ET', 'AF'),
	('GA', 'AF'),
	('GH', 'AF'),
	('GM', 'AF'),
	('GN', 'AF'),
	('GQ', 'AF'),
	('GW', 'AF'),
	('KE', 'AF'),
	('KM', 'AF'),
	('LR', 'AF'),
	('LS', 'AF'),
	('LY', 'AF'),
	('MA', 'AF'),
	('MG', 'AF'),
	('ML', 'AF'),
	('MR', 'AF'),
	('MU', 'AF'),
	('MW', 'AF'),
	('MZ', 'AF'),
	('NA', 'AF'),
	('NE', 'AF'),
	('NG', 'AF'),
	('RE', 'AF'),
	('RW', 'AF'),
	('SC', 'AF'),
	('SD', 'AF'),
	('SH', 'AF'),
	('SL', 'AF'),
	('SN', 'AF'),
	('SO', 'AF'),
	('ST', 'AF'),
	('SZ', 'AF'),
	('TD', 'AF'),
	('TG', 'AF'),
	('TN', 'AF'),
	('TZ', 'AF'),
	('UG', 'AF'),
	('YT', 'AF'),
	('ZA', 'AF'),
	('ZM', 'AF'),
	('ZW', 'AF'),
	('AQ', 'AN'),
	('BV', 'AN'),
	('GS', 'AN'),
	('HM', 'AN'),
	('TF', 'AN'),
	('AE', 'AS'),
	('AF', 'AS'),
	('AM', 'AS'),
	('AP', 'AS'),
	('AZ', 'AS'),
	('BD', 'AS'),
	('BH', 'AS'),
	('BN', 'AS'),
	('BT', 'AS'),
	('CC', 'AS'),
	('CN', 'AS'),
	('CX', 'AS'),
	('CY', 'AS'),
	('GE', 'AS'),
	('HK', 'AS'),
	('ID', 'AS'),
	('IL', 'AS'),
	('IN', 'AS'),
	('IO', 'AS'),
	('IQ', 'AS'),
	('IR', 'AS'),
	('JO', 'AS'),
	('JP', 'AS'),
	('KG', 'AS'),
	('KH', 'AS'),
	('KP', 'AS'),
	('KR', 'AS'),
	('KW', 'AS'),
	('KZ', 'AS'),
	('LA', 'AS'),
	('LB', 'AS'),
	('LK', 'AS'),
	('MM', 'AS'),
	('MN', 'AS'),
	('MO', 'AS'),
	('MV', 'AS'),
	('MY', 'AS'),
	('NP', 'AS'),
	('OM', 'AS'),
	('PH', 'AS'),
	('PK', 'AS'),
	('PS', 'AS'),
	('QA', 'AS'),
	('SA', 'AS'),
	('SG', 'AS'),
	('SY', 'AS'),
	('TH', 'AS'),
	('TJ', 'AS'),
	('TL', 'AS'),
	('TM', 'AS'),
	('TW', 'AS'),
	('UZ', 'AS'),
	('VN', 'AS'),
	('YE', 'AS'),
	('AD', 'EU'),
	('AL', 'EU'),
	('AT', 'EU'),
	('AX', 'EU'),
	('BA', 'EU'),
	('BE', 'EU'),
	('BG', 'EU'),
	('BY', 'EU'),
	('CH', 'EU'),
	('CZ', 'EU'),
	('DE', 'EU'),
	('DK', 'EU'),
	('EE', 'EU'),
	('ES', 'EU'),
	('EU', 'EU'),
	('FI', 'EU'),
	('FO', 'EU'),
	('FR', 'EU'),
	('FX', 'EU'),
	('GB', 'EU'),
	('GG', 'EU'),
	('GI', 'EU'),
	('GR', 'EU'),
	('HR', 'EU'),
	('HU', 'EU'),
	('IE', 'EU'),
	('IM', 'EU'),
	('IS', 'EU'),
	('IT', 'EU'),
	('JE', 'EU'),
	('LI', 'EU'),
	('LT', 'EU'),
	('LU', 'EU'),
	('LV', 'EU'),
	('MC', 'EU'),
	('MD', 'EU'),
	('ME', 'EU'),
	('MK', 'EU'),
	('MT', 'EU'),
	('NL', 'EU'),
	('NO', 'EU'),
	('PL', 'EU'),
	('PT', 'EU'),
	('RO', 'EU'),
	('RS', 'EU'),
	('RU', 'EU'),
	('SE', 'EU'),
	('SI', 'EU'),
	('SJ', 'EU'),
	('SK', 'EU'),
	('SM', 'EU'),
	('TR', 'EU'),
	('UA', 'EU'),
	('VA', 'EU'),
	('AG', 'NA'),
	('AI', 'NA'),
	('AN', 'NA'),
	('AW', 'NA'),
	('BB', 'NA'),
	('BL', 'NA'),
	('BM', 'NA'),
	('BS', 'NA'),
	('BZ', 'NA'),
	('CA', 'NA'),
	('CR', 'NA'),
	('CU', 'NA'),
	('DM', 'NA'),
	('DO', 'NA'),
	('GD', 'NA'),
	('GL', 'NA'),
	('GP', 'NA'),
	('GT', 'NA'),
	('HN', 'NA'),
	('HT', 'NA'),
	('JM', 'NA'),
	('KN', 'NA'),
	('KY', 'NA'),
	('LC', 'NA'),
	('MF', 'NA'),
	('MQ', 'NA'),
	('MS', 'NA'),
	('MX', 'NA'),
	('NI', 'NA'),
	('PA', 'NA'),
	('PM', 'NA'),
	('PR', 'NA'),
	('SV', 'NA'),
	('TC', 'NA'),
	('TT', 'NA'),
	('US', 'NA'),
	('VC', 'NA'),
	('VG', 'NA'),
	('VI', 'NA'),
	('AS', 'OC'),
	('AU', 'OC'),
	('CK', 'OC'),
	('FJ', 'OC'),
	('FM', 'OC'),
	('GU', 'OC'),
	('KI', 'OC'),
	('MH', 'OC'),
	('MP', 'OC'),
	('NC', 'OC'),
	('NF', 'OC'),
	('NR', 'OC'),
	('NU', 'OC'),
	('NZ', 'OC'),
	('PF', 'OC'),
	('PG', 'OC'),
	('PN', 'OC'),
	('PW', 'OC'),
	('SB', 'OC'),
	('TK', 'OC'),
	('TO', 'OC'),
	('TV', 'OC'),
	('UM', 'OC'),
	('VU', 'OC'),
	('WF', 'OC'),
	('WS', 'OC'),
	('AR', 'SA'),
	('BO', 'SA'),
	('BR', 'SA'),
	('CL', 'SA'),
	('CO', 'SA'),
	('EC', 'SA'),
	('FK', 'SA'),
	('GF', 'SA'),
	('GY', 'SA'),
	('PE', 'SA'),
	('PY', 'SA'),
	('SR', 'SA'),
	('UY', 'SA'),
	('VE', 'SA');
/*!40000 ALTER TABLE `pais_continente` ENABLE KEYS */;

-- Volcando estructura para tabla alplaneta_grupo1.pasaje
CREATE TABLE IF NOT EXISTS `pasaje` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `codigo` varchar(50) NOT NULL,
  `fecha_creacion` datetime NOT NULL,
  `estado` enum('PAGADO','RESERVADO','CANCELADO','VENCIDO','FINALIZADO') NOT NULL,
  `fk_cliente` int(11) NOT NULL,
  `fk_viaje` int(11) NOT NULL,
  `fecha_vencimiento` datetime NOT NULL,
  `descuento` int(11) DEFAULT NULL,
  `fk_responsable` int(11) NOT NULL,
  `fk_local` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `codigo` (`codigo`),
  KEY `FK_pasaje_cliente` (`fk_cliente`),
  KEY `FK_pasaje_viaje` (`fk_viaje`),
  KEY `FK_pasaje_responsable` (`fk_responsable`),
  KEY `FK_pasaje_local` (`fk_local`),
  CONSTRAINT `FK_pasaje_cliente` FOREIGN KEY (`fk_cliente`) REFERENCES `usuario` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_pasaje_local` FOREIGN KEY (`fk_local`) REFERENCES `local` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_pasaje_responsable` FOREIGN KEY (`fk_responsable`) REFERENCES `usuario` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_pasaje_viaje` FOREIGN KEY (`fk_viaje`) REFERENCES `viaje` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla alplaneta_grupo1.pasaje: ~22 rows (aproximadamente)
/*!40000 ALTER TABLE `pasaje` DISABLE KEYS */;
INSERT INTO `pasaje` (`id`, `codigo`, `fecha_creacion`, `estado`, `fk_cliente`, `fk_viaje`, `fecha_vencimiento`, `descuento`, `fk_responsable`, `fk_local`) VALUES
	(1, '099cc', '2019-06-26 02:23:41', 'CANCELADO', 11, 1, '2019-08-03 00:00:00', 50, 4, 1),
	(2, 'd8cd1', '2019-06-26 02:25:10', 'PAGADO', 11, 3, '2019-08-08 00:00:00', 0, 4, 1),
	(3, 'de2de', '2019-06-26 02:25:49', 'PAGADO', 10, 6, '2019-07-08 00:00:00', 50, 4, 1),
	(4, '9b9a8', '2019-06-26 02:27:10', 'PAGADO', 7, 3, '2019-07-12 00:00:00', 0, 4, 1),
	(5, 'efccb', '2019-06-26 02:27:52', 'PAGADO', 9, 5, '2019-07-15 00:00:00', 25, 4, 1),
	(6, '398bc', '2019-06-26 02:28:27', 'PAGADO', 8, 2, '2019-08-04 00:00:00', 0, 4, 1),
	(7, 'bbfbf', '2019-06-29 18:28:10', 'PAGADO', 17, 5, '2019-07-09 00:00:00', 25, 5, 1),
	(8, 'e2aa9', '2019-06-29 18:31:06', 'PAGADO', 16, 1, '2019-07-19 00:00:00', 50, 5, 1),
	(9, '2d7a8', '2019-06-30 17:31:55', 'PAGADO', 13, 7, '2019-07-19 00:00:00', 25, 6, 2),
	(10, 'b423a', '2019-06-27 17:32:10', 'VENCIDO', 14, 5, '2019-08-27 00:00:00', 25, 6, 2),
	(11, 'c144f', '2019-07-02 18:32:42', 'PAGADO', 17, 7, '2019-07-19 00:00:00', 25, 19, 4),
	(12, '7b2e2', '2019-07-02 18:32:56', 'PAGADO', 16, 7, '2019-07-19 00:00:00', 25, 19, 4),
	(13, '0b1a4', '2019-07-04 18:33:12', 'PAGADO', 14, 5, '2019-08-27 00:00:00', 25, 19, 4),
	(14, '30fa8', '2019-07-04 18:33:32', 'PAGADO', 13, 6, '2019-07-19 00:00:00', 50, 20, 7),
	(15, '247a8', '2019-07-03 18:33:44', 'PAGADO', 14, 4, '2019-08-27 00:00:00', 0, 20, 7),
	(16, '9813a', '2019-07-03 18:34:22', 'PAGADO', 15, 5, '2019-07-19 00:00:00', 25, 21, 6),
	(17, '81306', '2019-07-04 18:34:39', 'PAGADO', 14, 2, '2019-08-27 00:00:00', 0, 21, 6),
	(18, 'fa500', '2019-07-02 18:40:07', 'PAGADO', 15, 4, '2019-07-09 00:00:00', 0, 23, 2),
	(19, 'ce42d', '2019-07-03 18:40:23', 'PAGADO', 15, 2, '2019-07-09 00:00:00', 0, 23, 2),
	(20, 'c11af', '2019-07-02 18:40:41', 'PAGADO', 16, 4, '2019-07-09 00:00:00', 0, 23, 2),
	(21, 'c0be7', '2019-07-04 18:41:05', 'PAGADO', 13, 4, '2019-07-09 00:00:00', 0, 22, 2),
	(22, '31226', '2019-07-04 19:41:25', 'PAGADO', 17, 4, '2019-07-09 00:00:00', 0, 22, 2);
/*!40000 ALTER TABLE `pasaje` ENABLE KEYS */;

-- Volcando estructura para tabla alplaneta_grupo1.pasajero
CREATE TABLE IF NOT EXISTS `pasajero` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_acompañante` int(11) NOT NULL,
  `id_pasaje` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_acompañante_pasajero` (`id_acompañante`),
  KEY `FK_pasaje_pasajero` (`id_pasaje`),
  CONSTRAINT `FK_acompañante_pasajero` FOREIGN KEY (`id_acompañante`) REFERENCES `acompañante` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_pasaje_pasajero` FOREIGN KEY (`id_pasaje`) REFERENCES `pasaje` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla alplaneta_grupo1.pasajero: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `pasajero` DISABLE KEYS */;
INSERT INTO `pasajero` (`id`, `id_acompañante`, `id_pasaje`) VALUES
	(1, 1, 4),
	(2, 2, 5);
/*!40000 ALTER TABLE `pasajero` ENABLE KEYS */;

-- Volcando estructura para tabla alplaneta_grupo1.puntaje
CREATE TABLE IF NOT EXISTS `puntaje` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_operacion` int(11) NOT NULL,
  `cantidad` varchar(50) NOT NULL,
  `fecha_creacion` datetime NOT NULL DEFAULT current_timestamp(),
  `fecha_vencimiento` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_puntaje_operacion` (`id_operacion`),
  CONSTRAINT `FK_puntaje_operacion` FOREIGN KEY (`id_operacion`) REFERENCES `operacion` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla alplaneta_grupo1.puntaje: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `puntaje` DISABLE KEYS */;
INSERT INTO `puntaje` (`id`, `id_operacion`, `cantidad`, `fecha_creacion`, `fecha_vencimiento`) VALUES
	(1, 1, '100', '2019-06-26 02:23:45', '2023-06-26 00:00:00'),
	(2, 2, '300', '2019-06-26 02:25:14', '2023-06-26 00:00:00'),
	(3, 3, '500', '2019-06-26 02:25:53', '2023-06-26 00:00:00'),
	(4, 5, '500', '2019-06-26 02:27:56', '2023-06-26 00:00:00'),
	(5, 6, '300', '2019-06-26 02:28:30', '2023-06-26 00:00:00'),
	(6, 7, '700', '2019-07-02 18:28:13', '2023-07-02 00:00:00'),
	(7, 8, '100', '2019-07-02 18:31:09', '2023-07-02 00:00:00'),
	(8, 9, '1600', '2019-07-02 18:31:57', '2023-07-02 00:00:00'),
	(9, 11, '1300', '2019-07-02 18:32:43', '2023-07-02 00:00:00'),
	(10, 12, '1800', '2019-07-02 18:32:58', '2023-07-02 00:00:00'),
	(11, 13, '1000', '2019-07-02 18:33:13', '2023-07-02 00:00:00'),
	(12, 16, '1200', '2019-07-02 18:34:23', '2023-07-02 00:00:00'),
	(13, 17, '800', '2019-07-02 18:34:41', '2023-07-02 00:00:00'),
	(14, 18, '2900', '2019-07-02 18:40:09', '2023-07-02 00:00:00'),
	(15, 19, '100', '2019-07-02 18:40:24', '2023-07-02 00:00:00'),
	(16, 20, '1100', '2019-07-02 18:40:42', '2023-07-02 00:00:00'),
	(17, 22, '2300', '2019-07-02 18:41:26', '2023-07-02 00:00:00');
/*!40000 ALTER TABLE `puntaje` ENABLE KEYS */;

-- Volcando estructura para tabla alplaneta_grupo1.puntaje_config
CREATE TABLE IF NOT EXISTS `puntaje_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_creador` int(11) DEFAULT NULL,
  `cantidad_precio` decimal(19,2) NOT NULL,
  `cantidad_puntos` int(11) NOT NULL,
  `valor_unitario` decimal(19,2) NOT NULL,
  `fecha_creacion` datetime NOT NULL DEFAULT current_timestamp(),
  `años_vencimiento` int(11) unsigned NOT NULL DEFAULT 0,
  `meses_vencimiento` int(11) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `FK_puntaje_creador` (`id_creador`),
  CONSTRAINT `FK_puntaje_creador` FOREIGN KEY (`id_creador`) REFERENCES `usuario` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla alplaneta_grupo1.puntaje_config: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `puntaje_config` DISABLE KEYS */;
INSERT INTO `puntaje_config` (`id`, `id_creador`, `cantidad_precio`, `cantidad_puntos`, `valor_unitario`, `fecha_creacion`, `años_vencimiento`, `meses_vencimiento`) VALUES
	(1, 1, 1000.00, 100, 0.20, '2019-06-03 07:57:23', 4, 0);
/*!40000 ALTER TABLE `puntaje_config` ENABLE KEYS */;

-- Volcando estructura para tabla alplaneta_grupo1.ubicacion
CREATE TABLE IF NOT EXISTS `ubicacion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ciudad` varchar(50) NOT NULL,
  `provincia` varchar(50) NOT NULL,
  `pais` varchar(50) NOT NULL,
  `continente` varchar(50) NOT NULL,
  `detalle` varchar(50) DEFAULT NULL,
  `tags` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `estado` enum('HABILITADO','INHABILITADO') DEFAULT 'HABILITADO',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla alplaneta_grupo1.ubicacion: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `ubicacion` DISABLE KEYS */;
INSERT INTO `ubicacion` (`id`, `ciudad`, `provincia`, `pais`, `continente`, `detalle`, `tags`, `estado`) VALUES
	(1, 'Municipio Roma XIII', 'Roma', 'Italia', 'Europa', 'Cd. del Vaticano', '["Ciudad","Monumento"]', 'INHABILITADO'),
	(2, 'Plaza San Pietro', 'Unica', 'Ciudad del Vaticano', 'Europa', '', '["Monumento","Ciudad"]', 'HABILITADO'),
	(3, 'Rio IV', 'Cordoba', 'Argentina', 'Sudamérica', '', '["Playa","Ciudad"]', 'HABILITADO'),
	(4, 'Manhattan', 'Nueva York', 'Estados Unidos', 'Norte América', 'Capital', '["Ciudad","Comercial","Bar"]', 'HABILITADO'),
	(5, 'Fuji', 'Prefectura de Shizouka', 'Japón', 'Asia', '', '["Montaña","Bosque","Ciudad","Bar","Nieve"]', 'HABILITADO'),
	(6, 'Valparaiso', 'Valparaiso', 'Chile', 'Sudamérica', '', '["Ciudad","Playa","Monumento","Montaña"]', 'HABILITADO'),
	(7, 'La Boca', 'Buenos Aires', 'Argentina', 'Sudamérica', 'Aeropuerto', '["Ciudad"]', 'HABILITADO'),
	(8, 'Rosario', 'Santa Fe', 'Argentina', 'Sudamérica', 'Aeropuerto', '["Ciudad"]', 'HABILITADO'),
	(9, 'Cerro Murarata', 'La Paz', 'Bolivia', 'Sudamérica', '', '["Montaña","Ciudad","Bosque"]', 'HABILITADO');
/*!40000 ALTER TABLE `ubicacion` ENABLE KEYS */;

-- Volcando estructura para tabla alplaneta_grupo1.usuario
CREATE TABLE IF NOT EXISTS `usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dni` varchar(50) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `apellido` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `usuario` varchar(50) NOT NULL,
  `contraseña` varchar(65) NOT NULL,
  `id_contacto` int(11) NOT NULL,
  `fecha_creacion` datetime NOT NULL,
  `rol` enum('CLIENTE','COORDINADOR','DUEÑO','PERSONAL_ADMINISTRATIVO','ADMINISTRADOR') DEFAULT 'CLIENTE',
  `estado` enum('HABILITADO','INHABILITADO') DEFAULT 'HABILITADO',
  `fecha_nacimiento` datetime NOT NULL,
  `id_local` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `usuario` (`usuario`),
  KEY `FK_contacto` (`id_contacto`),
  KEY `fk_localEnUsuario` (`id_local`),
  CONSTRAINT `FK_contacto` FOREIGN KEY (`id_contacto`) REFERENCES `contacto` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_localEnUsuario` FOREIGN KEY (`id_local`) REFERENCES `local` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla alplaneta_grupo1.usuario: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` (`id`, `dni`, `nombre`, `apellido`, `email`, `usuario`, `contraseña`, `id_contacto`, `fecha_creacion`, `rol`, `estado`, `fecha_nacimiento`, `id_local`) VALUES
	(1, '0', 'Administrador', 'General', 'correo@correo.com', 'admin', '5fd924625f6ab16a19cc9807c7c506ae1813490e4ba675f843d5a10e0baacdb8', 1, '2019-06-01 01:14:45', 'ADMINISTRADOR', 'HABILITADO', '2019-06-01 00:00:00', 1),
	(2, '30131453', 'Javier', 'Godoy', 'correo@correo.com', 'jgodoy', '5fd924625f6ab16a19cc9807c7c506ae1813490e4ba675f843d5a10e0baacdb8', 2, '2019-06-01 01:33:50', 'DUEÑO', 'HABILITADO', '2008-06-01 00:00:00', 1),
	(3, '30123456', 'Juan Carlos', 'Monteros', 'correo@correo.com', 'jmonteros', '5fd924625f6ab16a19cc9807c7c506ae1813490e4ba675f843d5a10e0baacdb8', 3, '2019-06-01 01:34:37', 'COORDINADOR', 'HABILITADO', '1999-06-04 00:00:00', 1),
	(4, '30654321', 'Fulano', 'Gomez', 'correo@correo.com', 'fgomez', '5fd924625f6ab16a19cc9807c7c506ae1813490e4ba675f843d5a10e0baacdb8', 4, '2019-06-01 01:35:38', 'PERSONAL_ADMINISTRATIVO', 'HABILITADO', '2019-06-01 00:00:00', 1),
	(5, '20123456', 'Mengano', 'Perez', 'correo@correo.com', 'mperez', '5fd924625f6ab16a19cc9807c7c506ae1813490e4ba675f843d5a10e0baacdb8', 5, '2019-06-01 02:50:46', 'PERSONAL_ADMINISTRATIVO', 'HABILITADO', '1994-06-01 00:00:00', 1),
	(6, '30654321', 'Cosme', 'Fulanito', 'correo@correo.com', 'cfulanito', '5fd924625f6ab16a19cc9807c7c506ae1813490e4ba675f843d5a10e0baacdb8', 6, '2019-06-01 01:35:38', 'PERSONAL_ADMINISTRATIVO', 'HABILITADO', '2019-06-01 00:00:00', 2),
	(7, '36123456', 'Martin', 'Bornholdt', 'mbornholdt3@gmail.com', 'mbornholdt', '5fd924625f6ab16a19cc9807c7c506ae1813490e4ba675f843d5a10e0baacdb8', 7, '2019-06-01 02:44:17', 'CLIENTE', 'HABILITADO', '1994-10-26 00:00:00', NULL),
	(8, '33123456', 'Rodrigo', 'Zordan', 'zordan.rodrigo97@gmail.com', 'rzordan', '5fd924625f6ab16a19cc9807c7c506ae1813490e4ba675f843d5a10e0baacdb8', 8, '2019-06-01 02:45:12', 'CLIENTE', 'HABILITADO', '1997-11-17 00:00:00', NULL),
	(9, '30123456', 'Nahuel', 'Burgos Fuentes', 'burgosfuentesn@gmail.com', 'nburgos', '5fd924625f6ab16a19cc9807c7c506ae1813490e4ba675f843d5a10e0baacdb8', 9, '2019-06-01 02:48:00', 'CLIENTE', 'HABILITADO', '1992-12-24 00:00:00', NULL),
	(10, '38123456', 'Matias', 'Dutra', 'matydu98@gmail.com', 'mdutra', '5fd924625f6ab16a19cc9807c7c506ae1813490e4ba675f843d5a10e0baacdb8', 10, '2019-06-01 02:49:00', 'CLIENTE', 'HABILITADO', '1998-06-13 00:00:00', NULL),
	(11, '38267785', 'Juan', 'Grenz', 'j.grenz@hotmail.com', 'jgrenz', '5fd924625f6ab16a19cc9807c7c506ae1813490e4ba675f843d5a10e0baacdb8', 11, '2019-06-21 17:24:46', 'CLIENTE', 'HABILITADO', '1994-07-03 00:00:00', NULL),
	(12, '20573842', 'Pepe', 'Luis', 'correo@correo.com', 'pluis', '5fd924625f6ab16a19cc9807c7c506ae1813490e4ba675f843d5a10e0baacdb8', 14, '2019-06-26 01:44:47', 'COORDINADOR', 'HABILITADO', '2003-10-20 01:44:51', 2),
	(13, '18492052', 'Homero', 'Simpson', 'hjsimpson@correo.com', 'hsimpson', '5fd924625f6ab16a19cc9807c7c506ae1813490e4ba675f843d5a10e0baacdb8', 15, '2019-06-27 15:41:14', 'CLIENTE', 'HABILITADO', '1982-05-06 00:00:00', NULL),
	(14, '11584802', 'Han', 'Solo', 'millenniumfalcon03@hotmail.com', 'hsolo', '5fd924625f6ab16a19cc9807c7c506ae1813490e4ba675f843d5a10e0baacdb8', 16, '2019-06-27 15:43:23', 'CLIENTE', 'HABILITADO', '1977-12-25 00:00:00', NULL),
	(15, '23059822', 'Luke', 'Skywalker', 'anewhope@gmail.com', 'lskywalker', '5fd924625f6ab16a19cc9807c7c506ae1813490e4ba675f843d5a10e0baacdb8', 17, '2019-06-27 15:44:33', 'CLIENTE', 'HABILITADO', '1977-12-25 00:00:00', NULL),
	(16, '20895450', 'Leia', 'Organa', 'maytheforce@bewithyou.com', 'lorgana', '5fd924625f6ab16a19cc9807c7c506ae1813490e4ba675f843d5a10e0baacdb8', 18, '2019-06-27 15:46:11', 'CLIENTE', 'HABILITADO', '1977-12-25 00:00:00', NULL),
	(17, '10820020', 'ObiWan', 'Kenobi', 'hellothere@correo.com', 'okenobi', '5fd924625f6ab16a19cc9807c7c506ae1813490e4ba675f843d5a10e0baacdb8', 19, '2019-06-27 15:47:46', 'CLIENTE', 'HABILITADO', '1980-11-04 00:00:00', NULL),
	(18, '1023847', 'Bill', 'Gatos', 'billgatos@microsoft.com', 'bgatos', '5fd924625f6ab16a19cc9807c7c506ae1813490e4ba675f843d5a10e0baacdb8', 20, '2019-06-27 15:53:11', 'COORDINADOR', 'HABILITADO', '1995-02-03 00:00:00', 2),
	(19, '11289368', 'Steve', 'Jons', 'manzana@correo.com', 'sjons', '5fd924625f6ab16a19cc9807c7c506ae1813490e4ba675f843d5a10e0baacdb8', 21, '2019-06-27 16:01:56', 'PERSONAL_ADMINISTRATIVO', 'HABILITADO', '1965-06-08 00:00:00', 4),
	(20, '43202098', 'Bart', 'Simpson', 'elbarto@yahoo.com', 'bsimpson', '5fd924625f6ab16a19cc9807c7c506ae1813490e4ba675f843d5a10e0baacdb8', 22, '2019-06-27 16:15:09', 'PERSONAL_ADMINISTRATIVO', 'HABILITADO', '2002-07-03 00:00:00', 7),
	(21, '17812347', 'Tony', 'Stark', 'iamironman@gmail.com', 'ironman99', '5fd924625f6ab16a19cc9807c7c506ae1813490e4ba675f843d5a10e0baacdb8', 23, '2019-06-27 16:16:48', 'PERSONAL_ADMINISTRATIVO', 'HABILITADO', '1980-09-11 00:00:00', 6),
	(22, '30654121', 'Steve', 'Rogers', 'correo@correo.com', 'srogers', '5fd924625f6ab16a19cc9807c7c506ae1813490e4ba675f843d5a10e0baacdb8', 6, '2019-06-01 01:35:38', 'PERSONAL_ADMINISTRATIVO', 'HABILITADO', '2019-06-01 00:00:00', 2),
	(23, '30655921', 'Thor', 'Odinson', 'correo@correo.com', 'thor88', '5fd924625f6ab16a19cc9807c7c506ae1813490e4ba675f843d5a10e0baacdb8', 6, '2019-06-01 01:35:38', 'PERSONAL_ADMINISTRATIVO', 'HABILITADO', '2019-06-01 00:00:00', 2);
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;

-- Volcando estructura para tabla alplaneta_grupo1.viaje
CREATE TABLE IF NOT EXISTS `viaje` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha_creacion` datetime NOT NULL,
  `codigo_referencia` varchar(50) NOT NULL,
  `fk_origen` int(11) NOT NULL,
  `fk_destino` int(11) NOT NULL,
  `fecha_salida` date NOT NULL,
  `horario_salida` time NOT NULL,
  `horas_de_viaje` varchar(50) NOT NULL,
  `capacidad_ascientos` int(11) NOT NULL,
  `fk_medio_transporte` int(11) NOT NULL,
  `precio` decimal(19,2) NOT NULL,
  `estado` enum('ABIERTO','CANCELADO','FINALIZADO') NOT NULL DEFAULT 'ABIERTO',
  `actualizado` tinyint(1) NOT NULL,
  `impuesto` decimal(19,2) NOT NULL,
  `fk_descuento` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `codigo_referencia` (`codigo_referencia`),
  KEY `fk_medio_transporte` (`fk_medio_transporte`),
  KEY `FK_origen` (`fk_origen`),
  KEY `FK_destino` (`fk_destino`),
  KEY `fk_descuento` (`fk_descuento`),
  CONSTRAINT `FK_destino` FOREIGN KEY (`fk_destino`) REFERENCES `ubicacion` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_origen` FOREIGN KEY (`fk_origen`) REFERENCES `ubicacion` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_viaje_medio_transporte` FOREIGN KEY (`fk_medio_transporte`) REFERENCES `medio_transporte` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `fk_descuento` FOREIGN KEY (`fk_descuento`) REFERENCES `oferta` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla alplaneta_grupo1.viaje: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `viaje` DISABLE KEYS */;
INSERT INTO `viaje` (`id`, `fecha_creacion`, `codigo_referencia`, `fk_origen`, `fk_destino`, `fecha_salida`, `horario_salida`, `horas_de_viaje`, `capacidad_ascientos`, `fk_medio_transporte`, `precio`, `estado`, `actualizado`, `impuesto`, `fk_descuento`) VALUES
	(1, '2019-06-01 02:03:52', '5d0d0', 7, 6, '2019-12-06', '15:30:00', 'PT8H', 36, 1, 3298.99, 'ABIERTO', 0, 75.00, 1),
	(2, '2019-06-01 02:05:38', '3845a', 7, 6, '2019-11-29', '14:35:00', 'PT4H30M', 17, 1, 8454.00, 'ABIERTO', 0, 155.34, NULL),
	(3, '2019-05-01 02:09:11', '3db25', 6, 7, '2019-06-30', '22:30:00', 'PT5H24M', 23, 1, 6355.00, 'FINALIZADO', 1, 93.75, NULL),
	(4, '2019-05-30 02:15:16', 'fabbb', 7, 4, '2019-07-25', '15:15:00', 'PT144H', 20, 2, 28699.00, 'ABIERTO', 0, 453.99, NULL),
	(5, '2019-05-28 02:19:16', '16aa7', 6, 9, '2020-01-01', '20:20:00', 'PT70H', 16, 2, 15789.00, 'ABIERTO', 1, 259.29, 2),
	(6, '2019-06-15 22:47:46', '3a363', 3, 5, '2019-10-26', '22:47:00', 'PT3H30M', 20, 2, 11250.00, 'ABIERTO', 1, 150.00, 1),
	(7, '2019-06-25 23:11:18', '097e9', 7, 4, '2019-12-24', '23:10:00', 'PT2H45M', 50, 1, 23450.00, 'ABIERTO', 0, 552.00, 2);
/*!40000 ALTER TABLE `viaje` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
